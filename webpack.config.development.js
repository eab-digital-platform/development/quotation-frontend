/* eslint max-len: 0 */
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var config = {
  entry: [
    'webpack-hot-middleware/client?path=http://localhost:3000/__webpack_hmr',
    './app/index'
  ],

  devtool: 'source-map',

  output: {
    path: __dirname + 'dist',
    filename: 'bundle.js',
    // libraryTarget: 'commonjs2',
    publicPath: '/'
    // publicPath: 'http://localhost:3000/dist/'
  },

  module: {
    loaders: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['react', 'es2015', 'stage-0']
      }
    },
    {
      test: /\.global\.css$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: 'css-loader'
      })
    },
    {
      test: /^((?!\.global).)*\.css$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
      })
    },
    {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: "css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!sass-loader"
      })
    },
    {
      test: /\.json$/,
      loader: 'json-loader'
    }, {
      test: /\.(woff|woff2|eot|ttf)$/,
      loader: 'url-loader?limit=8192'
    },
    {
      test: /\.html$/,
      loader: 'html-loader?attrs[]=video:src'
    }, {
      test: /\.mp4$/,
      loader: 'url-loader?limit=100000&mimetype=video/mp4'
    }, {
        test: /\.(jpe?g|png|gif|svg|ico)$/i,
        loader: 'url-loader?limit=8192'
    }]
  },

  resolve: {
    extensions: ['.js', '.jsx']
    // packageMains: ['webpack', 'browser', 'web', 'browserify', ['jam', 'main'], 'main']
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      __DEV__: true,
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),
    new webpack.LoaderOptionsPlugin({
       debug: true
    }),
    new ExtractTextPlugin({
      filename: 'style.css', allChunks: true
    }),
    new HtmlWebpackPlugin({
      template: `${__dirname}/app/index.ejs`,
      filename: 'index.html',
      inject: 'body',
      title: "EASE - AXA Singapore hot"
    })
  ]
};
//
// config.target = webpackTargetElectronRenderer(config);

module.exports = config;
