var http = require('http');
var fs = require('fs');
var _ = require('lodash');
const beautify = require('js-beautify');
const cssBeautify = beautify.css_beautify;
const jsBeautify = beautify.js_beautify;
const htmlBeautify = beautify.html_beautify;

const config = require('../conf/extConfig.json');
const REPORT_TEMPLATE = '../report_template/';

global.config = config;

const basicPlanList = ['SAV', 'IND', 'BAA', 'ASIM', 'FPX', 'FSX', 'TPX', 'TPPX', 'AWT', 'PUL', 'RHP', 'ESP', 'LMP', 'HIM', 'HER', 'AWICA', 'AWICP', 'AWI_CPF', 'LITE', 'NPE', 'PNP', 'PNP2', 'PNPP', 'PNPP2', 'DTS', 'DTJ', 'PVTVUL', 'PVLVUL', 'ART2', 'SPEN', "AWTR", "MPC", "CSP", "FX2","RHP2"];

const main = function () {
  const paras = process.argv;
  if (paras) {
    // getBasicPlanList().then(basicPlanList => {
      for (var i = 1; i < paras.length; i++) {
        if (paras[i] === '-path') {
          global.config.sg.path = paras[++i];
        } else if (paras[i] === '-port') {
          global.config.sg.port = paras[++i];
        } else if (paras[i] === '-name') {
          global.config.sg.name = paras[++i];
        } else if (paras[i] === 'basicPlan') {
          return exportProduct([paras[++i]], true, true, false);
        } else if (paras[i] === 'plan') {
          return exportProduct([paras[++i]], false, true, false);
        } else if (paras[i] === 'list') {
          return exportProduct(basicPlanList, true, false, true);
        } else if (paras[i] === 'all') {
          return exportProduct(basicPlanList, true, true, true);
        }
      }
    // });
  }
  console.log(
    'Usage: node product_exporter.js [OPTION] basicPlan COV_CODE\n' +
    '  or:  node product_exporter.js [OPTION] plan COV_CODE\n' +
    '  or:  node product_exporter.js [OPTION] list\n' +
    '  or:  node product_exporter.js [OPTION] all'
  );
};

// by binary
const getAttachmentByBinary = function(name, cb) {
  var options = {
    hostname: global.config.sg.path,
    port: global.config.sg.port,
    path: '/' + global.config.sg.name + '/' + name,
    method: 'GET',
  };
  console.log('INFO: getAttachmentByBinary: Options: ',options.path);
  var req = http.request(options, (res) => {
    res.on('data', (chunk) => {
      cb(chunk, true);
    });
    res.on('end', () => {
      cb(false);
    });
  }).on('error', (e)=> {
    console.log('SG Error: ', e);
  });
  req.end();
};

const getFromSG = function(method, name, cb) {
  if (name && typeof name !== 'string') {
    console.log("ERROR: invalid doc name:", name);
    cb(false);
    return;
  }
  var options = {
    hostname: global.config.sg.path,
    port: global.config.sg.port,
    path: '/' + global.config.sg.name + '/' + encodeURIComponent(name),
    method: method
  };
  console.log('INFO: getFromSG Options: ',options.path);
  var req = http.request(options, (res) => {
    var resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      console.log('INFO: getFromSG End:', (resp.length < 100 && resp) || resp.length);
      if (typeof cb === 'function') {
        if (resp) {
          resp = JSON.parse(resp);
          cb(resp);
        } else {
          console.log('Error: getFromSG non-string resp?', resp);
          cb(false);
        }
      }
    });
  }).on('error', (e)=> {
    console.log('SG Error: ', e);
  });
  req.end();
};

const getDoc = function(docId, callback) {
  getFromSG('GET', docId, callback);
};

const queryFromSG = function(method, name, params, updateIndex, cb) {
  var paramStr = '';
  if (params) {
    for (var p in params) {
      if (params[p]) {
        paramStr += (paramStr ? "&" : "?") + p + '=' + params[p];
      }
    }
  }
  //set stale to false, The index is updated before the query is executed
  paramStr += (paramStr ? "&" : "?") + (updateIndex ? "stale=false" : "stale=ok");

  var options = {
    hostname: global.config.sg.path,
    port: global.config.sg.port,
    path: '/' + global.config.sg.name + '/' + name + (paramStr ? paramStr : ''),
    method: method,
    params: {
      params
    },
    header: {
      'Content-type': 'application/json'
    }
  };
  console.log('INFO: queryFromSG: Options: ', options.path);

  var req = http.request(options, (res) => {
    var resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      console.log('INFO: queryFromSG End:', (resp.length < 100 && resp) || resp.length);
      if (cb && typeof cb === 'function') {
        if (resp) {
          resp = JSON.parse(resp);
          cb(resp);
        } else {
          console.log('ERROR: queryFromSG:', resp);
          cb(false);
        }
      }
    });
  }).on('error', (e)=> {
    console.log('SG Error: ', e);
  });
  req.end();
};

const getViewRange = function(ddname, vname, start, end, params, callback) {
  var query = {
    startkey: start ? start : null,
    endkey: end ? end : null
  };

  if (params) {
    for (var p in params) {
      query[p] = params[p];
    }
  }

  // get session
  try {
    var updateIndex = true;
    queryFromSG('GET', '_design/' + ddname + '/_view/' + vname, query, updateIndex, function(res) {
        console.log('remoteCB + res: ', res);
        if (typeof callback === 'function') {
            callback(res);
        }
    });
  } catch (ex) {
      callback(false);
  }
};

function getBasicPlanList () {
  return new Promise(resolve => {
    getDoc('productSuitability', prodSuitDoc => resolve(_.keys(prodSuitDoc.products)));
  })
}

const exportProduct = function (covCodes, isBasicPlan, writeJson, writeList) {
  let importFile = null;
  if (writeList) {
    importFile = fs.createWriteStream('products.txt');
  }
  getViewRange('main', 'products', '["01","0","0"]', '["01","Z","ZZZ"]', '', function(list) {
    if (list && list.total_rows > 0) {
      let promise = Promise.resolve();
      _.each(list.rows, (row) => {
        if (!covCodes.length || covCodes.indexOf(getCovCodeFromProductId(row.id)) > -1) {
          promise = promise.then(() => {
            return new Promise((resolve) => {
              getDoc(row.id, resolve);
            }).then((planDetail) => {
              return saveProduct(planDetail, writeJson, importFile).then(() => {
                if (!isBasicPlan) {
                  return;
                }
                return saveTemplates(planDetail, writeJson, importFile).then(() => {
                  let p = Promise.resolve();
                  _.each(planDetail.riderList, r => {
                    p = p.then(() => getRiders(r.covCode).then(riders => Promise.all(_.map(riders, rider => saveProduct(rider, writeJson, importFile)))));
                  });
                  return p;
                });
              });
            });
          });
        }
      });
    }
  });
};

const getRiders = function (covCode) {
  return new Promise((resolve) => {
    getViewRange('main', 'products', '["01","R","' + covCode + '"]', '["01","R","' + covCode + '"]', '', resolve);
  }).then((list) => {
    return Promise.all(_.map(list && list.rows, row => new Promise(resolve => {
      getDoc(row.id, resolve);
    })));
  });
};

const saveProduct = function (planDetail, writeJson, importFile) {
  if (!planDetail) {
    return null;
  }
  console.log('Exporting product: ' + planDetail._id);
  if (writeJson) {
    const productJson = getCleanDoc(planDetail);
    updateFormula(planDetail);
    fs.writeFileSync('./files/product/' + planDetail._id + '.json', JSON.stringify(productJson, null, 4));
  }
  if (importFile) {
    importFile.write('D\t' + planDetail._id + '\t' + planDetail._id + '.json\n');
  }
  return saveAttachments(planDetail, writeJson, importFile);
};

function getPdfDir (template) {
  const { _id: id } = template;
  const folderPath = REPORT_TEMPLATE + id;

  // create template folder if not exist
  if (!fs.existsSync(folderPath)) {
    fs.mkdirSync(folderPath);
  }

  return folderPath;
}

function updateStyle (template) {
  try {
    const { style } = template;
    const folderPath = getPdfDir(template);

    fs.writeFileSync(folderPath + '/style.css', cssBeautify(style, { indent_size: 2 }));
    return true;
  } catch (ex) {
    console.log('fail to write style: ', template.id, ex);
    return false;
  }
}

function updateFormula (template) {
  try {
    const { formulas } = template;
    const folderPath = getPdfDir(template);

    // handle data format
    for (const funcName in formulas) {
      const content = formulas[funcName];
      if (content) {
        fs.writeFileSync(`${folderPath}/formula_${funcName}.js`, jsBeautify(content, { indent_size: 2 }));
      }
    }
    return true;
  } catch (ex) {
    console.log('fail to write formula: ', template.id, ex);
    return false;
  }
}

function updateHtml(filePath, data) {
  fs.writeFileSync(filePath, htmlBeautify(data, { indent_size: 2 }));
}

function updateTemplateHtml (template) {
  try {
    const { template: pages } = template;
    const folderPath = getPdfDir(template);

    // write footer header first
    _.forEach(['footer', 'header'], key => {
      const content = _.get(template, `${key}.en`);
      // update html if exist content and content is not default value
      if (content && content != '<p></p>') {
        updateHtml(folderPath + `/${key}.html`, content);
      }
    });

    _.forEach(pages.en, (page, index) => {
      updateHtml(folderPath + `/page_${index}.html`, page);
    });

    return true;
  } catch (ex) {
    console.log('fail to write formula: ', template.id, ex);
    return false;
  }
}

function updateTemplate (template) {
  updateStyle(template);
  updateFormula(template);
  updateTemplateHtml(template);
}

const saveTemplates = function (planDetail, writeJson, importFile) {
  let p = Promise.resolve();
  var pdfCodes = new Set();
  _.each(planDetail.reportTemplate, (reportTemplate) => {
    if (pdfCodes.has(reportTemplate.pdfCode)) {
      return;
    }
    pdfCodes.add(reportTemplate.pdfCode);
    p = p.then(() => {
      return new Promise((resolve) => {
        getViewRange('main', 'pdfTemplates',
          '["01","QUOT","' + reportTemplate.pdfCode + '"]',
          '["01","QUOT","' + reportTemplate.pdfCode + '"]',
          null,
          resolve
        );
      }).then((list) => {
        let p = Promise.resolve();
        _.each(list.rows, row => {
          p = p.then(() => {
            return new Promise((resolve) => {
              getDoc(row.id, (template) => {
                if (writeJson) {
                  const json = getCleanDoc(template);
                  fs.writeFileSync('./files/product/' + template._id + '.json', JSON.stringify(json, null, 4));
                  updateTemplate(template);
                }
                if (importFile) {
                  importFile.write('D\t' + template._id + '\t' + template._id + '.json\n');
                }
                resolve();
              });
            });
          });
        });
        return p;
      });
    });
  });
  return p;
};

const saveAttachments = function (planDetail, writeJson, importFile) {
  let p = Promise.resolve();
  _.each(planDetail._attachments, (att, key) => {
    p = p.then(() => {
      return new Promise((resolve) => {
        const type = att.content_type;
        let extension;
        if (type.indexOf('pdf') > 0) {
          extension = '.pdf';
        } else if (type.indexOf('jpg') > 0 || type.indexOf('jpeg') > 0) {
          extension = '.jpg';
        } else {
          extension = '.png';
        }
        if (importFile) {
          importFile.write('A\t' + planDetail._id + '\t' + key + '\t' + planDetail.covCode + '_' + planDetail.version + '_' + key + extension + '\n');
        }
        if (writeJson) {
          const ws = fs.createWriteStream('./files/product/' + planDetail.covCode + '_' + planDetail.version + '_' + key + extension);
          getAttachmentByBinary(planDetail._id + '/' + key, (chunk, isData) => {
            if (isData) {
              ws.write(chunk);
            } else {
              ws.end();
              resolve();
            }
          });
        } else {
          resolve();
        }
      });
    });
  });
  return p;
};

const getCleanDoc = function (doc) {
  const json = {};
  _.each(doc, (val, key) => {
    if (key !== '_id' && key !== '_attachments' && key !== '_rev') {
      json[key] = val;
    }
  });
  return json;
};

const getCovCodeFromProductId = (productId) => {
  if (!productId) {
    return null;
  }
  let m = productId.match(/_product_(.*)_[0-9]+$/);
  if (m && m.length > 1) {
    return m[1];
  }
};

main();
