"use strict";
var fs = require('fs');
var lib = require('./prod/bz.js');

const rootDir = './CB_resources/';
const exportDir = rootDir + 'files/';
const configFile = rootDir + 'updateView.txt';
const VIEW_PATH = '_design/';
const VIEW_PREFIX = 'view_';
const FILE_TYPE = '.json';

function _getFileName (name) {
    return VIEW_PREFIX + name + FILE_TYPE;
}

function _getFilePath (name) {
    return exportDir + _getFileName(name);
}

function _getDocName (name) {
    return VIEW_PATH + name;
}

function _getConfigLine (name) {
    const fileName = _getFileName(name);
    const docName = _getDocName(name);
    const tabChar = '\t';
    const newLineChar = '\n';

    return 'D' + tabChar + docName + tabChar + fileName + newLineChar;
}

function _writeFile (filePath, data) {
    try {
        fs.writeFileSync(filePath, data);
        console.log('Views export done.', filePath);
        return true;
    } catch (ex) {
        console.log('Views generate failure.', filePath, ex);
        return false;
    }
}

try {
    let views = lib.exportViews();
    if (views) {
        let configData = '';
        for (const viewName in views) {
            // export view file
            const filePath = _getFilePath(viewName);
            const writeResult = _writeFile(filePath, views[viewName]);

            // input config file if export view successfully
            if (writeResult) {
                configData += _getConfigLine(viewName);
            }
        }
        // export config file
        _writeFile(configFile, configData);
    } else {
        console.log('Views generate failure 1.');
    }
} catch (ex) {
    console.log('Views generate failure.', ex);
}
