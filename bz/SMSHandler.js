var {
  callApi
} = require('./utils/RemoteUtils.js');

module.exports.sendSMS = (data, session, cb) => {
  if (global.config.disableSMS){
    cb({});
  } else {
    callApi('/sms/sendSMS',
      data.sms,
      cb);
  }
};
