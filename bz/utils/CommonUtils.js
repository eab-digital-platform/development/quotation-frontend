"use strict"
var _ = require('lodash');
const dao = require('../cbDaoFactory').create();



// call server with path and para in object or string
module.exports.callServer = function(actionPath, data, callback) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) { // done
      var action = null;
      
      if (xhttp.status == 200 && xhttp.responseText) {
        var res = JSON.parse(xhttp.responseText);
      }
      
      if (callback) {
        callback(res);
      }
    }
  };

  if (!data) {
    data = "";
  } else if (typeof(data) == 'object') {
    // data = convertObj2ParaStr(data);
    data = JSON.stringify(data);
  }

  xhttp.open("POST", global.config.remote + actionPath, true);
  xhttp.setRequestHeader("Content-type", "application/json");
  // xhttp.withCredentials = true;
  // xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(data);

  ///TODO: may need to call up loading panel
};

// Function to Sort the Data by given Property
module.exports.sortByProperty = function (property) {
    return function (a, b) {
        var sortStatus = 0,
            aProp = a[property].toLowerCase(),
            bProp = b[property].toLowerCase();
        if (aProp < bProp) {
            sortStatus = -1;
        } else if (aProp > bProp) {
            sortStatus = 1;
        }
        return sortStatus;
    };
};

module.exports.generateSortFn = function (props) {
    return function (a, b) {
        for (var i = 0; i < props.length; i++) {
            var prop = props[i];
            var name = prop.name;
            var reverse = prop.reverse;
            if (a[name] < b[name])
                return reverse ? 1 : -1;
            if (a[name] > b[name])
                return reverse ? -1 : 1;
        }
        return 0;
    };
};

var addZero = function(a, num){
  for(let i = 0; i < num; i++){
    a = "0" + a;
  }
  return a;
}

module.exports.getAgentIdForDoc = function(agentNumber){
  agentNumber  = agentNumber + ""
  agentNumber = addZero(agentNumber, 6 - agentNumber.length);
  return agentNumber;
}

module.exports.getSeqForDoc = function(seq){
  seq = seq + "";
  seq = addZero(seq, 5 - seq.length);
  return seq;
}

const getAgentNumber = (agentCode) => {
  const docId = 'agentNumberMap';
  return new Promise((resolve) => {
    dao.getDoc(docId, (doc) => {
      let agentNum = doc && doc.map && doc.map[agentCode];
      if (agentNum) {
        resolve(agentNum);
      } else {
        doc = doc.map ? doc : {};
        agentNum = doc.nextSeq || 1001;
        doc.map = doc.map || {};
        doc.map[agentCode] = agentNum;
        doc.nextSeq = agentNum + 1;
        dao.updDoc(docId, doc, () => {
          resolve(agentNum);
        });
      }
    });
  });
};

module.exports.getDocNumbers = function(agentCode, prefix, numOfIds){
  return getAgentNumber(agentCode).then((agentNumber) => {
    let seqDocId = prefix + 'id-' + agentNumber + '-seq';
    let seqNos = [];
    let _getSeq = function(index, callback) {
      dao.getDoc(seqDocId,
        function(seqDoc){
          let noOfSeq;
          //new doc
          if (seqDoc.error) {
            for (noOfSeq = 0; noOfSeq < numOfIds; noOfSeq++) {
              seqNos.push(noOfSeq + 1);
            }
            let newSeqDoc = {seq: numOfIds + 1};
            dao.updDoc(
              seqDocId,
              newSeqDoc,
              function(){
                callback({agentNumber, seqNos});
              }
            );
          } else {
            let seqNo = seqDoc.seq;
            if (!seqNo) {
              seqNo = 1;
            }

            seqDoc.seq = seqNo + numOfIds;

            for (noOfSeq = seqNo; noOfSeq < seqNo + numOfIds; noOfSeq++) {
              seqNos.push(noOfSeq);
            }

            dao.updDoc(
              seqDocId,
              seqDoc,
              function(){
                callback({agentNumber, seqNos});
              }
            );
          }
        }
      );
    };

    return new Promise((resolve) => {
      _getSeq(0, resolve);
    });
  });
};

module.exports.getDocNumber = function(agentCode, prefix, callback){
  getAgentNumber(agentCode).then((agentNumber) => {
    let seqDocId = prefix + "id-" + agentNumber + "-seq";
    dao.getDoc(seqDocId,
      function(seqDoc){
        //new doc
        if (seqDoc.error) {
          let newSeqDoc = {seq: 2};
          dao.updDoc(
            seqDocId,
            newSeqDoc,
            function(){
              callback(agentNumber, 1)
            }
          );
        } else {
          let seqNo = seqDoc.seq;
          if(!seqNo)
            seqNo = 1;

          seqDoc.seq = seqNo + 1;

          dao.updDoc(
            seqDocId,
            seqDoc,
            function(){
              callback(agentNumber, seqNo)
            }
          )
        }
      }
    );
  });
}

module.exports.getByLang = function (obj, lang) {
  if (!obj) {
    return obj;
  }
  if (obj && lang && obj[lang]) {
    return obj[lang];
  }
  for (let key in obj) {
    return obj[key];
  }
};


module.exports.calcAge = function(birthday) {
  let today = new Date();
  let birthDate = new Date(birthday);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
  }
  return age;
};

/**
 * Deep diff between two object, using lodash
 * @param  {Object} object Object compared
 * @param  {Object} base   Object to compare with
 * @return {Object}        Return a new object who represent the diff
 */
module.exports.difference = function(object, base) {
	function changes(object, base) {
		return _.transform(object, function(result, value, key) {
			if (!_.isEqual(value, base[key])) {
				result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
			}
		});
	}
	return changes(object, base);
}

module.exports.getFromRedis = function(key, cb) {
  global.redisClient.get(key, function(err, data) {
    if (err) {
      logger.info('re-initial pool:' + key);
      cb({})
    }
    if (data) {
      let obj = false;
      logger.debug('DEBUG: getFromRedis data', data);
      try {
        obj = JSON.parse(data);
      } catch(e) {
        logger.error('EXCEPTION: get from redis', e);
      }
      if (obj) {
        cb(obj);        
      } else {
        logger.info('re-initial pool:' + key);
        cb({})
      }
    } else {
      logger.info('Initial pool:' + key);
      cb({})
    };
  })
};

module.exports.setToRedis = function(key, obj) {
  logger.debug('DEBUG: setToRedis obj', obj);
  if (obj) {
    global.redisClient.set(key, JSON.stringify(obj));
  } else {
    logger.error('ERROR: invalid object due to setToRedis.');
  }
}

module.exports.getCurrency = function(value, sign, decimals) {
  if (!_.isNumber(value)) {
      return '-';
  }
  if (!_.isNumber(decimals)) {
      decimals = 2;
  }
  var text = parseFloat(value).toFixed(decimals);
  var parts = text.split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  parts[0] = (sign && sign.trim().length > 0 ? sign : '') + parts[0];
  return parts.join(".");
};

module.exports.getCurrencyByCcy = function(value, ccy, decimals) {
  var sign = '$';
  if (ccy == 'SGD') {
    sign = 'S$ ';
  } else if (ccy === 'USD') {
    sign = 'US$ ';
  } else if (ccy === 'GBP') {
    sign = '£ ';
  } else if (ccy === 'EUR') {
    sign = '€ ';
  } else if (ccy === 'AUD') {
    sign = 'A$ ';
  }
  return this.getCurrency(value, sign, decimals);
}

module.exports.getCurrencySign = function (compCode, ccy, optionsMap) {
  const currency = _.find(optionsMap.ccy.currencies, c => c.compCode === compCode);
  if (currency) {
    const option = _.find(currency.options, opt => opt.value === ccy);
    if (option) {
      return option.symbol;
    }
  }
  return 'S$';
};

module.exports.escapeHtml = function(html) {
  if (html) {
    return html
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
  } else {
    return html;
  }
}