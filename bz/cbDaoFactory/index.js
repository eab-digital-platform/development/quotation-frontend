var db = null;

module.exports.create = function() {
  if (db == null) {
    if (process.env.NODE_TYPE == 'LOCAL' || process.env.NODE_TYPE == 'DEV') {
      db = require('./localCB');
    } else {
      db = require('./remoteCB');
    }
  }
  return db;
}
