const cDao = require("./cbDao/client");
const nDao = require("./cbDao/needs");
const bDao = require("./cbDao/bundle");
const dao = require('./cbDaoFactory').create();
var logger = global.logger || console;

module.exports.getProfileLayout = function(data, session, cb) {
  cDao.getProfileLayout({},function(doc) {
    var ret = {
      success: true,
      template: doc
    }
    if (ret) {
      cb(ret)
    } else {
      cb({})
    }
  })
}

module.exports.saveTrustedIndividual = function(data, session, cb){
  var {cid, tiInfo, tiPhoto, confirm} = data;
  //check change would affect current profile or not
  //also check which module will have effort
  bDao.onSaveTrustedIndividual(cid, tiInfo).then((result)=>{
    //if no effort or user clicked confirm at ui
    if(confirm || result.code === bDao.CHECK_CODE.VALID){
      //invalidate client if neccessary
      var save = function(){
        cDao.saveTrustedIndividual(cid, tiInfo, tiPhoto).then((resp)=>{
          bDao.updateApplicationTrustedIndividual(cid, false, tiInfo).then(()=>{
            nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
              cb({
                success: resp.profile? true: false, 
                profile: resp.profile,
                showFnaInvalidFlag
              });
            });
          }).catch((error)=>{
            logger.error("Error in saveTrustedIndividual->updateApplicationTrustedIndividual: ", error);
          });
        }).catch((error)=>{
          logger.error("Error in saveTrustedIndividual->saveTrustedIndividual: ", error);
        });
      }
      if(result.code === bDao.CHECK_CODE.VALID){
        save();
      }
      else {
        bDao.invalidTrustedIndividual(cid).then(save).catch((error)=>{
          logger.error("Error in saveTrustedIndividual->invalidTrustedIndividual: ", error);
        });
      }
    }
    else {
      cb({success: true, code: result.code})
    }
  }).catch((error)=>{
      logger.error("Error in saveTrustedIndividual->onSaveTrustedIndividual: ", error);
  });
}

module.exports.deleteTrustIndividual = function(data, session, cb){
  var {cid} = data;
  bDao.invalidTrustedIndividual(cid).then(()=>{
    //save profile after invalidate
    cDao.saveTrustedIndividual(cid, {}, null).then((resp)=>{
      bDao.updateApplicationTrustedIndividual(cid, false, {}).then(()=>{
        nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
          cb({
            success: resp.profile? true: false, 
            profile: resp.profile,
            showFnaInvalidFlag
          });
        });
      }).catch((error)=>{
          logger.error("Error in deleteTrustIndividual->updateApplicationTrustedIndividual: ", error);
      });
    }).catch((error)=>{
      logger.error("Error in deleteTrustIndividual->saveTrustedIndividual: ", error);
    });
  }).catch((error)=>{
      logger.error("Error in deleteTrustIndividual->invalidTrustedIndividual: ", error);
  });
}


module.exports.unlinkRelationship = function(data, session, cb){
  let {cid, fid} = data;
  let errObj = {
    fna: true,
    quotation: true,
    application: true,
    removeDependant: true,
    removeApplicant: true
  };

  bDao.invalidFamilyMember(cid, session.agent, fid, errObj).then((resp)=>{
    cDao.unlinkRelationship(data.cid, data.fid, function(doc){
      if(!doc.error){
        nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
          cb({
            success: true,
            profile: doc,
            showFnaInvalidFlag
          })
        });
      }else{
        cb({success: false})
      }
    })
  }).catch((error)=>{
      logger.error("Error in unlinkRelationship->:invalidFamilyMember ", error);
  });
}

module.exports.getTrustIndividualLayout = function(data, session, cb) {
  cDao.getTrustIndividualLayout({},function(doc) {
    if(!doc.error){
      cb({
        success: true,
        template: doc
      })
    }else{
      cb({success: false})
    }
  })
}

module.exports.getProfileDisplayLayout = function(data, session, cb) {
  cDao.getProfileDisplayLayout({},function(doc) {
    if(!doc.error){
      cb({
        success: true,
        template: doc
      })
    }else{
      cb({success: false})
    }
  })
}

module.exports.getFamilProfileLayout = function(data, session, cb) {
  cDao.getFamilProfileLayout({},function(doc) {
    if(!doc.error){
      cb({
        success: true,
        template: doc
      })
    }else{
      cb({success: false})
    }
  })
}

module.exports.saveFamilyMember = function(data, session, cb) {
  let {fid, cid, profile, photo, confirm} = data;
  let {agentCode: aid, features= ""} = session.agent;
  bDao.onSaveClient(cid, (features.indexOf("FNA")>-1? true: false), profile).then((result)=>{
    if (result) {
      if(confirm || result.code === bDao.CHECK_CODE.VALID){
        bDao.invalidFamilyMember(cid, session.agent, fid, result).then((resp = {})=>{
          //update bundle if there is new bundle
          if (resp && resp.bundle) {
            profile.bundle = resp.bundle;
          }
          return nDao.unlinkMappedPlans(cid).then(
            cDao.saveFamilyMember(fid, cid, session.agent, profile, photo, function(doc, fDoc, fid) {
              if(!doc.error){
                bDao.updateApplicationClientProfiles(fDoc).then(()=>{
                  nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
                    cb({
                      success: true,
                      profile: doc,
                      fProfile: fDoc,
                      invalid: resp.invalid,
                      fid: fid,
                      showFnaInvalidFlag
                    });
                  });
                }).catch((error)=>{
                  logger.error("Error in saveFamilyMember->updateApplicationClientProfile: ", error);
                });
              }else{
                cb({success: false})
              }
            })
          )
        }).catch((error)=>{
            logger.error("Error in saveFamilyMember->invalidFamilyMember: ", error);
        });
      }
      else {
        cb({success: true, code: result.code});
      }
    } else {
      logger.error("Error in saveFamilyMember->:onSaveClient [#POS 2]");
      cb({success: false});
    }
  }).catch((error)=>{
      logger.error("Error in saveFamilyMember->:onSaveClient ", error);
  });
}

module.exports.getProfile = function(data, session, cb){
  var cid = data.cid || data.docId;
  cDao.getProfile(cid).then((profile)=>{
    nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
      cb({
        success: profile ? true: false, 
        profile,
        showFnaInvalidFlag
      });
    });
  }).catch((error)=>{
      logger.error("Error in getProfile->getProfile: ", error);
  });
}

const getContactList = function(data, session, cb){
  logger.log("start get contact list");
  cDao.getContactList("01", session.agent.agentCode, function(pList){
    cb({
      success: true,
      contactList: pList
    })
  });
}

module.exports.getContactList = getContactList;

module.exports.addProfile = function(data, session, cb){
  var profile = data.profile,
    photo = data.photo,
    aid = session.agent.agentCode;
  cDao.addProfile(profile, session.agent, photo, function(doc){
    if(!doc.error){
      cb({
        success: true,
        showFnaInvalidFlag: false,
        profile: doc
      })
    }else{
      cb({success: false});
    }
  })
}

module.exports.deleteProfile = function(data, session, cb){
  cDao.deleteProfile(data.cid, session, function(result){
    logger.log("delte profile", result);
    if(!result.error){
        cb({success: true});
    }else{
      cb({success: false});
    }
  })
}

module.exports.saveProfile = function(data, session, cb){
  let {profile = {}, photo, tiPhoto, confirm} = data;
  let {cid} = profile;
  let {agentCode: aid, features= ""} = session.agent;
  //check change would affect current profile or not
  //also check which module will have effort
  bDao.onSaveClient(cid, (features.indexOf("FNA")>-1? true: false), profile).then((result)=>{
    //if no effort or user clicked confirm at ui
    if(confirm || result.code === bDao.CHECK_CODE.VALID){
      //invalidate client if neccessary
      bDao.invalidFamilyMember(cid, session.agent, cid, result).then((resp)=>{
        //update bundle if there is new bundle
        if(resp && resp.bundle){
          profile.bundle = resp.bundle
        }
        //save profile after invalidate
        cDao.saveProfile(profile, photo, tiPhoto, aid, function(doc){
          if(!doc.error){
            bDao.updateApplicationClientProfiles(profile).then(()=>{
              cDao.getAllDependantsProfile(cid).then((dependantProfiles)=>{
                nDao.showFnaInvalidFlag(cid).then(showFnaInvalidFlag => {
                  cb({
                    success: true,
                    dependantProfiles: dependantProfiles,
                    profile: doc,
                    invalid: resp.invalid,
                    showFnaInvalidFlag
                  })
                });
              }).catch((error)=>{
                  logger.error("Error in saveProfile->getAllDependantsProfile: ", error);
              });
            }).catch((error)=>{
                logger.error("Error in saveProfile->updateApplicationClientProfile: ", error);
            });
          }else{
            cb({success: false});
          }
        })
      }).catch((error)=>{
          logger.error("Error in saveProfile->invalidClient: ", error);
      });
    }
    else {
      cb({success: true, code: result.code})
    }
  }).catch((error)=>{
      logger.error("Error in saveProfile->onSaveClient: ", error);
  });
  
}

module.exports.getAllDependantsProfile = function(data, session, cb){
  var cid = data.cid;
  cDao.getAllDependantsProfile(cid).then((dependantProfiles)=>{
    cb({success: true, dependantProfiles});
  });
}

module.exports.saveClientProfile = function(data, session, cb) {
  logger.log('call login');
  cb(true)
}

module.exports.getAddressByPostalCode = function(data, session, cb){
  let postalCode = data.pC;
  let fileName = data.fileName;
  cDao.getAddressByPostalCode(postalCode, fileName, cb);
}