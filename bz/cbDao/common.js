const dao = require('../cbDaoFactory').create();
var logger = global.logger || console;
var _ = require('lodash');
const moment = require('moment');
const AUDIT_LOG_CLIENTTYPE_FIELD = 'clientId';
const ConfigConstant = require('../../app/constants/ConfigConstants');

const createAuditLogRecord = function(fieldName, deletedId, auditDoc) {
  const auditLog =  {
    deletedTime: moment().utcOffset(ConfigConstant.MOMENT_TIME_ZONE).valueOf()
  };
  auditLog[fieldName] = deletedId;

  if (auditDoc.deletedIds) {
    auditDoc.deletedIds.push(auditLog);
  } else {
    auditDoc.deletedIds = [auditLog];
  }

  return auditDoc;
};

const addDeleteLog = function(docName, fieldName, deletedId, callback){
  dao.getDoc(docName, function(doc){
    if (doc) {
      let auditDoc = {
        type: 'maintenance'
      };
      if (doc.error !== 'not_found') {
        auditDoc = doc;
      }
      auditDoc = createAuditLogRecord(fieldName, deletedId, auditDoc);
      dao.updDoc(docName, auditDoc, callback);
    } else {
      callback({success: false});
    }
  });
};

module.exports.addIdsInAuditLog = function(clientId, agentProfile, callback){
  const profileId = _.get(agentProfile, 'profileId');
  if (profileId) {
    addDeleteLog(`U_${profileId}_DELETEDID`, AUDIT_LOG_CLIENTTYPE_FIELD, clientId, callback);
  } else {
    logger.error(`No Profile Id in agentProfile ${clientId}`);
    callback({success: false});
  }
};
