const _c = require('./common');
const _ = require('lodash');

const dao = require('../../cbDaoFactory').create();
const quotDao = require('../quotation');
const appDao = require('../application');
const nDao = require('../needs');
const cDao = require('../client');
const _commonApplication = require('../../handler/application/common');

const {
  callApiComplete
} = require('../../utils/RemoteUtils');

const logger = global.logger || console;

var _invalidateApplicationByClientId = function(cid){
  return new Promise((resolve, reject)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      let promises = [];
      _.forEach(bundle.applications, (application)=>{
        let {appStatus, applicationDocId, quotationDocId} = application;
        if(appStatus === 'APPLYING' || !applicationDocId){
          _invalidateApplication(bundle, applicationDocId || quotationDocId);
        }
      })
      bundle.isFnaReportSigned = false;
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; rollback:true; fn:_invalidateApplicationByClientId]`);
      _c.updateBundle(bundle, ()=>{ resolve(bundle.applications); });
    }).catch((error)=>{
      logger.error("Error in _invalidateApplicationByClientId->getCurrentBundle: ", error);
    });
  })

};
module.exports.invalidateApplicationByClientId = _invalidateApplicationByClientId;

var _invalidateApplicationByFamilyClientId = function(cid, fid){
  return new Promise((resolve, reject)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      let promises = [];
      _.forEach(bundle.applications, (application)=>{
        promises.push(new Promise((resolve2)=>{
          let {appStatus, applicationDocId, quotationDocId} = application;
          dao.getDoc(quotationDocId, (quotation)=>{
            let familyClientIdIsLa = _.get(quotation, 'quotType') === 'SHIELD' ? (_.indexOf(_.keys(quotation.insureds), fid) >= 0) : quotation.iCid === fid;
            let hasInvalidBI = familyClientIdIsLa && (appStatus === 'APPLYING' || !applicationDocId);
            if(hasInvalidBI){
              _invalidateApplication(bundle, applicationDocId || quotationDocId);
            }
            resolve2(hasInvalidBI);
          });
        }));
      })
      Promise.all(promises).then((args)=>{
        let hasInvalidateBI = _.find(args, arg => arg === true);
        if (hasInvalidateBI){
          bundle.isFnaReportSigned = false;
        }
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fid:${fid}; rollback:true; fn:_invalidateApplicationByFamilyClientId]`);
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fid:${fid}; isFnaReportSigned:${_.get(bundle, 'isFnaReportSigned')}; status:${_.get(bundle, 'status')}; fn:_invalidateApplicationByFamilyClientId] ;checking:fnaSignature]`);
        _c.updateBundle(bundle, ()=>{
          //return true if there is an invalidate BI
          resolve(hasInvalidateBI);
        });
      }).catch((error)=>{
        logger.error("Error in _invalidateApplicationByFamilyClientId->Promise.all: ", error);
      });
    }).catch((error)=>{
      logger.error("Error in _invalidateApplicationByFamilyClientId->getCurrentBundle: ", error);
    });
  })

};
module.exports.invalidateApplicationByFamilyClientId = _invalidateApplicationByFamilyClientId;

module.exports.onDeleteInvalidateApplications = function (cid, deleteIds, isFaChannel, bundleId, session) {
  logger.log('INFO: onDeleteInvalidateApplications', cid, deleteIds, isFaChannel);
  return new Promise((resolve) => {
    _c.getBundle(cid, bundleId, (bundle) => {
      let hasInvalidatedCase = false;
      if (!bundle) {
        resolve();
        return;
      }

      let applications = [];
      if (!deleteIds) {
        applications = bundle.applications;
      } else {
        _.each(bundle.applications, app => {
          if ((deleteIds.indexOf(app.applicationDocId) > -1 || deleteIds.indexOf(app.quotationDocId) > -1) && app.appStatus === 'INVALIDATED') {
            hasInvalidatedCase = true;
            // When on deleting the invalidated applicaitons, not push the object to bundle
            // Delete the quotation
            logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; deleteIds:${deleteIds}; fn:onDeleteInvalidateApplications : BundleUpdate(before)]`);
            logger.log(`INFO: onDeleteInvalidateApplications: BundleUpdate(before) - [cid:${cid}; bundleId:${_.get(bundle, 'id')}]; deleteIds:${deleteIds}; quotationDocId:${app.quotationDocId}; Delete invalidated quotation]`);
            quotDao.getQuotation(app.quotationDocId, (quot) => {
              if (quot.id && quot._rev) {
                quotDao.deleteQuotation(quot, false, session);
              } else {
                logger.error(`Failed to get quotation: ${app.quotationDocId}`);
              }
            });
          } else {
            applications.push(app);
          }
        });
      }
      if (hasInvalidatedCase) {
        bundle.applications = applications;
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; deleteIds:${deleteIds}; rollback:true; fn:onDeleteInvalidateApplications]`);
        _c.updateBundle(bundle, () => {
          resolve(bundle.id);
        });
      } else {
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; deleteIds:${deleteIds}; No Invalidated Case; fn:onDeleteInvalidateApplications]`);
        resolve(bundle.id);
      }
    });
  });
};

module.exports.onDeleteApplications = function (cid, deleteIds, isFaChannel) {
  logger.log('INFO: onDeleteApplications', cid, deleteIds, isFaChannel);
  const proms = [];
  const toBedeleted = [];
  return new Promise((resolve) => {
    _c.getCurrentBundle(cid).then((bundle) => {
      if (!bundle) {
        resolve();
        return;
      }

      if (!isFaChannel && bundle.status >= _c.BUNDLE_STATUS.FULL_SIGN) {
        resolve(bundle.id);
        return;
      }

      let applications = [];
      let deleteIdsWithSPplanChecking = deleteIds;
      if (!deleteIds) {
        applications = bundle.applications;
      } else {
        _.each(bundle.applications, app => {
          // Remove linking for spPlan/rpPlan
            if (app.linkedCaseMapping && (deleteIds.indexOf(app.linkedCaseMapping.rpPlanQuotationId) > -1 || deleteIds.indexOf(app.linkedCaseMapping.spPlanQuotationId) > -1)) {
              const rpLinked = app.quotationDocId !== app.linkedCaseMapping.rpPlanQuotationId && app.quotationDocId === app.linkedCaseMapping.spPlanQuotationId;
              const spLinked = app.quotationDocId !== app.linkedCaseMapping.spPlanQuotationId && app.quotationDocId === app.linkedCaseMapping.rpPlanQuotationId;
              if (rpLinked || spLinked) {
                if (app.appStatus === _c.APPSTATUS.INPROGRESS_STATUS.APPLYING && rpLinked) {
                  // SP Plan that RP has been deleted
                  deleteIdsWithSPplanChecking.push(app.applicationDocId);
                }
                proms.push(
                  new Promise(resl => {
                    dao.getDoc(app.quotationDocId, (application) => {
                      if (application.clientChoice) {
                        application.clientChoice.isClientChoiceSelected = false;
                        application.clientChoice.isClientChoiceDisabled = false;
                      } else {
                        application.clientChoice = {};
                        application.clientChoice.isClientChoiceSelected = false;
                        application.clientChoice.isClientChoiceDisabled = false;
                      }

                      dao.updDoc(app.quotationDocId, application, (res) => {
                        if (!res.error || res.success) {
                          application._rev = res.rev;
                          resl();
                        }
                      });
                    });
                  })
                  );
              }
              delete app.linkedCaseMapping;
            }
        });

        _.each(bundle.applications, app => {
          if (deleteIdsWithSPplanChecking.indexOf(app.applicationDocId) > -1) {
            //rollback application to proposal
            logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; deleteIds:${deleteIdsWithSPplanChecking}; rollback:true; fn:onDeleteApplications]`);
            logger.log(`INFO: BundleUpdate(before) - [cid:${cid}; bundleId:${_.get(bundle, 'id')}]; deleteIds:${deleteIdsWithSPplanChecking}; quotationDocId:${app.quotationDocId}; rollback:true; fn:onDeleteApplications; action: removeSignedProposal]`);
            quotDao.removeSignedProposal(app.quotationDocId);
            delete app.applicationDocId;
            delete app.appStatus;
            applications.push(app);
          } else if (deleteIdsWithSPplanChecking.indexOf(app.quotationDocId) === -1) {
            //delete quotation
            applications.push(app);
          }
        });
      }

      bundle.applications = applications;
      bundle.status = _c.BUNDLE_STATUS.HAVE_BI;

      if (!isFaChannel) {
        bundle.isFnaReportSigned = false;
        bundle.clientChoice.isAppListChanged = true;
        bundle.clientChoice.isClientChoiceCompleted = false;
        bundle.clientChoice.isBudgetCompleted = false;
        bundle.clientChoice.isAcceptanceCompleted = false;
        bundle.clientChoice.clientChoiceStep = bundle.clientChoice.clientChoiceStep > 0 ? 1 : 0;
        //budget will be recalculated if isAppListChanged = true
      }

      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; deleteIds:${deleteIdsWithSPplanChecking}; rollback:true; fn:onDeleteApplications]`);
      _c.updateBundle(bundle, () => {
        Promise.all(proms).then(() => {
          dao.updateViewIndex('main', 'summaryQuots');
          resolve({ bundleId : bundle.id, toBedeleted });
        }
        );
      });
    });
  });
};

module.exports.onCreateApplication = function (cid, quotId, applicationId) {
  logger.log('INFO: onCreateApplication', cid);
  return new Promise((resolve) => {
    _c.getCurrentBundle(cid).then((bundle) => {
      if (!bundle) {
        resolve();
        return;
      }
      let application = _.find(bundle.applications, app => app.quotationDocId === quotId);
      application.applicationDocId = applicationId;
      application.appStatus = 'APPLYING';
      if (bundle.status < _c.BUNDLE_STATUS.START_APPLY){
        bundle.status = _c.BUNDLE_STATUS.START_APPLY;
      }
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; quotId:${quotId}; applicationId:${applicationId}; fn:onCreateApplication]`);
      _c.updateBundle(bundle, resolve);
    }).catch((error)=>{
      logger.error("Error in onCreateApplication->getCurrentBundle: ", error);
    });
  });
};

module.exports.onSubmitApplication = function (cid, applicationId) {
  return new Promise((resolve) => {
    _c.getCurrentBundle(cid).then((bundle) => {
      if (!bundle) {
        resolve();
        return;
      }
      let application = _.find(bundle.applications, app => app.applicationDocId === applicationId);
      application.appStatus = 'SUBMITTED';
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; applicationId:${applicationId}; fn:onSubmitApplication]`);
      _c.updateBundle(bundle, resolve);
    }).catch((error)=>{
      logger.error("Error in onSubmitApplication->getCurrentBundle: ", error);
    })
  });
};

module.exports.onApplyApplication = function (cid, applicationId) {
  return new Promise((resolve) => {
    _c.getCurrentBundle(cid).then((bundle) => {
      if (!bundle) {
        resolve();
        return;
      }
      let application = _.find(bundle.applications, app => app.applicationDocId === applicationId);
      application.appStatus = 'APPLYING';
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; applicationId:${applicationId}; fn:onApplyApplication]`);
      _c.updateBundle(bundle, resolve);
    }).catch((error)=>{
      logger.error("Error in onApplyApplication->getCurrentBundle: ", error);
    })
  });
};

var _invalidateApplication = function(bundle, docId, reason){
  let application = _.find(bundle.applications, app => app.applicationDocId === docId);
  // If it is not appId, then search as quotationId
  if (!application) {
    application = _.find(bundle.applications, app => app.quotationDocId === docId);
  }
  if (application && (application.appStatus === 'APPLYING' || !application.applicationDocId)) {
    application.appStatus = 'INVALIDATED' + (application.isFullySigned ? '_SIGNED' : '');
    application.invalidateDate = new Date().getTime(); //Rollback to use (UTC + 8), align with the production data in release 1 and 2
    if (reason) {
      application.invalidateReason = reason;
    }
    bundle.clientChoice.isAppListChanged = true;
    bundle.clientChoice.isClientChoiceCompleted = false;

    if (application.isFullySigned && application.applicationDocId) {
      appDao.getApplication(application.applicationDocId, function(app) {
        let invalidateIds = [];
        if (_.get(app, 'quotation.quotType') === 'SHIELD') {
          invalidateIds.push(application.applicationDocId);
          let childIds = _.get(app, 'childIds');
          invalidateIds = [...invalidateIds, ...childIds];
        } else {
          invalidateIds.push(application.applicationDocId);
        }

        if (app.isInitialPaymentCompleted) {
          // submit
          logger.debug('DEBUG: Submit invalidated paid application:', application.applicationDocId);
          _.each(invalidateIds, invalidateId => {
            appDao.getApplication(invalidateId, function(invalidateApp) {
              invalidateApp.isInvalidated = true;
              logger.log(`Payment Data Checking :: ${invalidateApp.id}, --bundle/applications --_invalidateApplication Previous Payment Policy Number :: ${_.get(invalidateApp, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(invalidateApp, 'payment.proposalNo')}, Current Policy Number :: ${_.get(invalidateApp, 'policyNumber')}`);
              appDao.updApplication(invalidateApp.id, invalidateApp, (saveRes) => {
                if (_.get(invalidateApp, 'type') === 'application') {
                  callApiComplete("/submitInvalidApp/" + invalidateApp.id, 'GET', {}, {}, true, (resp) => {
                    logger.log('INFO: Submit invalidated paid application:', invalidateApp.id, resp);
                    if (resp && resp.success) {
                      invalidateApp._rev = saveRes.rev;
                      invalidateApp.isSubmittedStatus = true;
                      invalidateApp.applicationSubmittedDate = (new Date()).getTime();
                      logger.log(`Payment Data Checking :: ${invalidateApp.id}, --bundle/applications --_invalidateApplication --After Call Api -- Previous Payment Policy Number :: ${_.get(invalidateApp, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(invalidateApp, 'payment.proposalNo')}, Current Policy Number :: ${_.get(invalidateApp, 'policyNumber')}`);
                      appDao.updApplication(invalidateApp.id, invalidateApp);
                    }
                  });
                }
              })
            });
          })
        } else {
          _.each(invalidateIds, invalidateId => {
            appDao.getApplication(invalidateId, function(invalidateApp) {
              invalidateApp.isInvalidated = true;
              logger.log(`Payment Data Checking :: ${invalidateApp.id}, --bundle/applications --_invalidateApplication -- isInitialPaymentCompleted (false) -- Previous Payment Policy Number :: ${_.get(invalidateApp, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(invalidateApp, 'payment.proposalNo')}, Current Policy Number :: ${_.get(invalidateApp, 'policyNumber')}`);
              appDao.updApplication(invalidateApp.id, invalidateApp);
            });
          });
        }
      });
    }
  }
};
module.exports.invalidateApplication = _invalidateApplication;

var _onInvalidateApplicationById = function (cid, docId) {
  return new Promise((resolve) => {
    _c.getCurrentBundle(cid).then((bundle) => {
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; docId:${docId};fn:_onInvalidateApplicationById]`);
      if (!bundle) {
        resolve();
        return;
      }
      //if no docId, invalidate all application
      if(_.isString(docId)){
        _invalidateApplication(bundle, docId);
      }
      else if(_.isArray(docId)){
        _.forEach(docId, (_id)=>{
          _invalidateApplication(bundle, _id);
        })
      }
      else {
        _.forEach(bundle.applications, (application)=>{
          _invalidateApplication(bundle, application.applicationDocId || application.quotationDocId);
        });
      }
      _c.updateBundle(bundle, resolve);
    }).catch((error)=>{
      logger.error("Error in _onInvalidateApplicationById->getCurrentBundle: ", error);
    })
  });
};
module.exports.onInvalidateApplicationById = _onInvalidateApplicationById;

var _onInvalidateApplicationByProductLine = function (cid, productLine){
  return new Promise((resolve) => {
    _c.getCurrentBundle(cid).then((bundle) => {
      let promises = [];
      _.forEach(bundle.applications, (application)=>{
        promises.push(new Promise((resolve2)=>{
          dao.getDoc(application.quotationDocId, (quotation)=>{
            if (quotation.productLine === productLine) {
              _invalidateApplication(bundle, application.quotationDocId);
            }
            resolve2();
          });
        }));
      });
      Promise.all(promises).then((args)=>{
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}]; productLine:${productLine};fn:_onInvalidateApplicationByProductLine`);
        _c.updateBundle(bundle, resolve);
      }).catch((error)=>{
        logger.error("Error in _onInvalidateApplicationByProductLine->Promise.all: ", error);
      })
    }).catch((error)=>{
      logger.error("Error in _onInvalidateApplicationByProductLine->getCurrentBundle: ", error);
    })
  });
};

module.exports.onInvalidateApplicationByProductLine = _onInvalidateApplicationByProductLine;

var _onInvalidateApplicationByCovCode = function (cid, covCode){
  return new Promise((resolve) => {
    _c.getCurrentBundle(cid).then((bundle) => {
      let promises = [];
      _.forEach(bundle.applications, (application)=>{
        promises.push(new Promise((resolve2)=>{
          dao.getDoc(application.quotationDocId, (quotation)=>{
            let _covCode = _.get(quotation, "plans[0].covCode") || quotation.baseProductCode;
            if((_.isString(covCode) && _covCode == covCode) || (_.isArray(covCode) && _.split(covCode, ",").indexOf(_covCode) > -1)){
              _invalidateApplication(bundle, application.quotationDocId);
            }
            resolve2();
          });
        }));
      });
      Promise.all(promises).then((args)=>{
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}]; covCode:${covCode}; fn:_onInvalidateApplicationByCovCode`);
        _c.updateBundle(bundle, resolve);
      }).catch((error)=>{
        logger.error("Error in _onInvalidateApplicationByCovCode->Promise.all: ", error);
      })
    }).catch((error)=>{
      logger.error("Error in _onInvalidateApplicationByCovCode->getCurrentBundle: ", error);
    })
  });
};

module.exports.onInvalidateApplicationByCovCode = _onInvalidateApplicationByCovCode;

module.exports.getApplicationByBundleId = function (bundleId, appId) {
  return new Promise((resolve) => {
    dao.getDoc(bundleId, (bundle) => {
      if (bundle) {
        let application = _.find(bundle.applications, app => app.applicationDocId === appId);
        resolve(application);
      } else {
        resolve();
      }
    });
  });
};

module.exports.getApplicationIdsByBundle = function (bundleId) {
  return new Promise((resolve) => {
    dao.getDoc(bundleId, (bundle) => {
      let applications = [];
      if (bundle) {
        applications = _.map(bundle.applications, (application) => {
          if (!application.applicationDocId) {
            return {
              type: 'quotation',
              id: application.quotationDocId,
              status: application.appStatus,
              invalidateReason: application.invalidateReason
            };
          } else {
            return {
              type: 'application',
              id: application.applicationDocId,
              status: application.appStatus,
              invalidateReason: application.invalidateReason
            };
          }
        });
      }
      resolve(applications);
    });
  });
};

module.exports.getApplication = function(cid, applicationId){
  return new Promise((resolve) => {
    _c.getCurrentBundle(cid).then((bundle) => {
      if (!bundle) {
        resolve();
        return;
      }
      let application = _.find(bundle.applications, app => app.applicationDocId === applicationId);
      resolve(application);
    }).catch((error)=>{
      logger.error("Error in getApplication->Promise.all: ", error);
    });
  });
};

module.exports.getDocByQuotId = function(cid, quotId) {
  logger.log('INFO: bundle getDocByQuotId starts');
  return new Promise((resolve)=>{
    _c.getCurrentBundle(cid).then((bundle) => {
      if (!bundle) {
        resolve();
      }
      let document = _.find(bundle.applications, doc => doc.quotationDocId === quotId);
      resolve(document);
    }).catch((error)=>{
      logger.error('Error in getDocByQuotId', error);
    });
  });
};

module.exports.rollbackApplication2Proposal = function(cid, fid, hasFNA){
  return new Promise((resolve)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      let promises = [];
      let invalidated = [];
      _.forEach(bundle.applications, (application)=>{
        if(!hasFNA && application.isFullySigned){
          promises.push(_invalidateApplication(bundle, application.applicationDocId));
        }
        else{
          promises.push(
            new Promise((resolve2)=>{
              dao.getDoc(application.quotationDocId, (quot)=>{
                let familyClientIdIsLa = _.get(quot, 'quotType') === 'SHIELD' ? (_.indexOf(_.keys(quot.insureds), fid) >= 0) : quot.iCid === fid;
                if (familyClientIdIsLa || !fid){
                  logger.log(`INFO: BundleUpdate(before) - [cid:${cid}; bundleId:${_.get(bundle, 'id')}]; fid:${fid}; hasFNA:${hasFNA}; quotationDocId:${application.quotationDocId}; rollback:true; fn:rollbackApplication2Proposal; action: removeSignedProposal]`);
                  quotDao.removeSignedProposal(application.quotationDocId);
                  if (application.appStatus === 'APPLYING'){
                    invalidated.push(application.applicationDocId);
                    _updateApplicationAppStatus2Delete(application.applicationDocId);
                    delete application.applicationDocId;
                    delete application.appStatus;
                  }
                }
                resolve2();
              })
            })
          )
        }
      })

      Promise.all(promises).then(()=>{
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}]; fid:${fid}; hasFNA:${hasFNA}; rollback:true; fn:rollbackApplication2Proposal]`);
        _c.updateBundle(bundle, () => { resolve({applications: bundle.applications, invalidated}) });
      }).catch((error)=>{
        logger.error("Error in rollbackApplication2Proposal->Promise.all: ", error);
      });
    }).catch((error)=>{
      logger.error("Error in rollbackApplication2Proposal->getCurrentBundle: ", error);
    });
  });
};

module.exports.initClientChoice = function(cid){
  return new Promise((resolve)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      bundle.clientChoice.isAppListChanged = true;
      bundle.clientChoice.isClientChoiceCompleted = false;
      bundle.clientChoice.clientChoiceStep = 0;
      bundle.clientChoice.isAcceptanceCompleted = false;
      bundle.clientChoice.isRecommendationCompleted = false;
      bundle.clientChoice.isBudgetCompleted = false;
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; rollback:true; fn:initClientChoice]`);
      _c.updateBundle(bundle, resolve);
    }).catch((error)=>{
      logger.error("Error in initClientChoice->getCurrentBundle: ", error);
    });
  })
};

var _rollbackApplication2Step1 = function(cid){
  return _c.getCurrentBundle(cid).then((bundle)=>{
    if (!bundle || bundle.status > _c.BUNDLE_STATUS.SIGN_FNA){
      return;
    }
    logger.log("INFO: roll back application:", cid);
    bundle.isFnaReportSigned = false;
    if(bundle.status > _c.BUNDLE_STATUS.START_APPLY){
      bundle.status = _c.BUNDLE_STATUS.START_APPLY;
    }
    return new Promise((resolve) => {
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; rollback:true; fn:_rollbackApplication2Step1]`);
      _c.updateBundle(bundle, resolve);
    }).then(() => {
      let promises = [];
      //if only 1 bundle and not fully sign, update have sign doc flag for delete client
      promises.push(new Promise((resolve2)=>{
        cDao.getProfile(cid, true).then((profile)=>{
          if (profile.bundle.length === 1 && profile.haveSignDoc) {
            profile.haveSignDoc = _.find(bundle.applications, app => app.isFullySigned);
            cDao.updateProfile(cid, profile).then(resolve2).catch((error)=>{
              logger.error("Error in _rollbackApplication2Step1->updateProfile: ", error);
            });
          }
          else {
            resolve2();
          }
        }).catch((error)=>{
          logger.error("Error in _rollbackApplication2Step1->getProfile: ", error);
        });
      }))
      _.forEach(bundle.applications, (application)=>{
        let {quotationDocId, applicationDocId, appStatus} = application;
        logger.log(`INFO: BundleUpdate(after) - [appStatus: ${appStatus}; cid:${cid}; bundleId:${_.get(bundle, 'id')}; quotationDocId:${quotationDocId}; rollback:true; fn:_rollbackApplication2Step1; action:removeSignedProposal]`);
        if (!_.includes(_c.APPSTATUS.VALID_PROPOSAL_SIGNATURE_STATUS, appStatus)) {
          quotDao.removeSignedProposal(quotationDocId);
          if(applicationDocId){
            promises.push(new Promise((resolve2)=>{
              dao.getDoc(applicationDocId, (applicationDoc)=>{
                applicationDoc.appStep = 0;
                applicationDoc.isStartSignature = false;

                if (_.get(applicationDoc, 'quotation.quotType') === 'SHIELD') {
                  //For Shield
                  applicationDoc.appCompletedStep = -1;
                  applicationDoc.isAppFormProposerSigned = false;
                  applicationDoc.isAppFormInsuredSigned = _.times(applicationDoc.isAppFormInsuredSigned.length, _.constant(false));
                } else {
                  //For Normal product
                  applicationDoc.isApplicationSigned = false;
                }
                applicationDoc.isProposalSigned = false;
                applicationDoc.isFullySigned = false;
                logger.log(`Payment Data Checking :: ${applicationDoc.id}, --bundle/applications --_rollbackApplication2Step1 -- Previous Payment Policy Number :: ${_.get(applicationDoc, 'payment.proposalNo')}, Next Payment Policy Number :: ${_.get(applicationDoc, 'payment.proposalNo')}, Current Policy Number :: ${_.get(applicationDoc, 'policyNumber')}`);
                dao.updDoc(applicationDocId, applicationDoc, resolve2);
              })
            }))
          }
        }
      })
      return Promise.all(promises);
    }).catch((error)=>{
      logger.error("Error in _rollbackApplication2Step1->new Promise: ", error);
    });
  }).catch((error)=>{
    logger.error("Error in _rollbackApplication2Step1->getCurrentBundle: ", error);
  });
};
module.exports.rollbackApplication2Step1 = _rollbackApplication2Step1;

module.exports.getApplyingApplicationsCount = function(bundleId) {
  logger.log('INFO -- getApplyingApplicationsCount');
  return new Promise((resolve)=>{
    dao.getDoc(bundleId, (bundle)=>{
      let applyingApps = _.filter( _.get(bundle, 'applications'), (bApp)=>{
        return _.get(bApp, 'appStatus') === 'APPLYING';
      });
      resolve(_.size(applyingApps));
    });
  });
};

var _getApplicationsByBundle = function(bundle) {
  let promises = [];
  for (let i = 0; i < bundle.applications.length; i ++) {
    if (bundle.applications[i].appStatus === 'APPLYING') {
      promises.push(
        new Promise(appResolve => {
          appDao.getApplication(bundle.applications[i].applicationDocId, function(app) {
            appResolve(app);
          });
        })
      );
    }
  }

  logger.log('INFO: getApplicationsByBundle', promises.length);
  return new Promise((resolve, reject) => {
    Promise.all(promises).then((results) => {
      resolve(results);
    }).catch((error) => {
      logger.error('ERROR: getApplicationsByBundle', error);
    });
  });
};
module.exports.getApplicationsByBundle = _getApplicationsByBundle;

module.exports.updateApplicationClientProfiles = function(profile) {
  return new Promise((resolve, reject) => {
    if (!profile.cid) {
      resolve();
    } else {
      const cids = _.concat(_.map(profile.dependants, 'cid'), [profile.cid]);
      let promises = [];
      _.forEach(cids, cid => {
        promises.push(_updateApplicationClientProfile(cid, profile));
      });
      Promise.all(promises).then(resolve);
    }
  })
}

//cid = proposer cid, profile = client profile (proposer / life assured)
const _updateApplicationClientProfile = function(cid, profile){
  logger.log('INFO: updateApplicationClientProfile', cid);

  // removeInvalidProfileProp
  if (profile._rev) delete profile._rev;
  if (profile._attachments) delete profile._attachments;

  return new Promise((resolve, reject)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      if (bundle.formValues) {
        if (bundle.formValues[profile.cid]) {
          let { personalInfo, insurability } = bundle.formValues[profile.cid];
          let branchInfo = {};
          if (personalInfo && personalInfo.branchInfo) {
            branchInfo = _.cloneDeep(personalInfo.branchInfo);
          }

          bundle.formValues[profile.cid].personalInfo = Object.assign(profile, (branchInfo ? {branchInfo:branchInfo} : {}));

          //repopulate client profile to insurability question LIFESTYLE01
          if (_.get(bundle.formValues[profile.cid], 'insurability.LIFESTYLE01')) {
            insurability.LIFESTYLE01 = profile.isSmoker;
            if (profile.isSmoker === 'N') {
              insurability.LIFESTYLE01a = '';
              insurability.LIFESTYLE01a_OTH = '';
              insurability.LIFESTYLE01b = '';
              insurability.LIFESTYLE01c = '';
            }
          }
        }

        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:_updateApplicationClientProfile]`);
        _c.updateBundle(bundle, (bResult) => {
          if (bResult && !bResult.error) {
            _getApplicationsByBundle(bundle).then((apps) => {
              let upPromise = [];
              for (let i = 0; i < apps.length; i ++) {
                let app = apps[i];
                if (app && !app.error) {
                  let { proposer, insured, insurability } = app.applicationForm.values;
                  let isUpdate = false;

                  //update proposer personalInfo
                  if (proposer.personalInfo.cid == profile.cid) {
                    let branchInfo = {};
                    if (proposer.personalInfo.branchInfo) {
                      branchInfo = _.cloneDeep(proposer.personalInfo.branchInfo);
                    }
                    proposer.personalInfo = Object.assign(profile, (branchInfo ? {branchInfo:branchInfo} : {}));

                    //no need to repopulate client profile isSmoker to insurability question LIFESTYLE01, because application will be invalidated
                    isUpdate = true;
                  }

                  //update insured personalInfo
                  for (let j = 0; j < insured.length; j ++) {
                    if (insured[j].personalInfo.cid == profile.cid) {
                      insured[j].personalInfo = profile;

                      //no need to repopulate client profile isSmoker to insurability question LIFESTYLE01, because application will be invalidated
                      isUpdate = true;
                    }
                  }

                  if (isUpdate) {
                    upPromise.push(
                      new Promise((upResolve) => {
                        appDao.upsertApplication(app._id, app, (upResult) => {
                          logger.log('INFO: updateApplicationClientProfile - update Application', app._id, 'for', cid);
                          upResolve(upResult);
                        });
                      })
                    );
                  }
                }
              }

              logger.log('INFO: updateApplicationClientProfile - before update Applications', cid, '[num=', upPromise.length, ']');
              Promise.all(upPromise).then((result) => {
                logger.log('INFO: updateApplicationClientProfile - end [RETURN=1]', cid);
                resolve();
              }).catch((error) => {
                logger.error('ERROR: updateApplicationClientProfile [RETURN=-1]', cid, error);
              })
            }).catch((error) => {
              logger.error('ERROR: updateApplicationClientProfile - _getApplicationsByBundle', cid, error);
            })
          } else {
            logger.error('ERROR: updateApplicationClientProfile - end [RETURN=-2]', cid, _.get(bResult, 'error'));
            resolve();
          }
        });
      } else {
        resolve();
      }
    })
  })
};

module.exports.updateApplicationClientProfile = _updateApplicationClientProfile;

module.exports.getFormValuesByCid = function(cid) {
  logger.log('INFO: getFormValuesByCid starts');
  return new Promise((resolve)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      if (bundle && bundle.formValues) {
        logger.log('INFO: getFormValuesByCid -- succeeds');
        resolve(bundle.formValues);
      } else {
        logger.log('INFO: getFormValuesByCid -- cannot find formValues');
        resolve();
      }
    }).catch((error)=>{
      logger.error('ERROR: getFormValuesByCid', error);
      resolve();
    });
  });
};
// FUNCTION ENDS: getFormValuesByCid


var _getApplicationExisitingPolicies = function(cid, fe, bundle) {
  logger.log('INFO: getApplicationExisitingPolicies - start');

  return new Promise((resolve, reject)=>{
    let nPromises = [];
    let nCids = [];

    for (let clientId in bundle.formValues) {
      nCids.push(clientId);
      nPromises.push(new Promise((nResolve, nReject) => {
        nDao.getExistingPoliciesFromFe(cid, clientId, fe).then((ePolicies) => {
          nResolve(ePolicies);
        }).catch((error) => {
          logger.error('ERROR: getApplicationExisitingPolicies - getExistingPoliciesFromFe', error);
          nReject(error);
        })
      }))
    }

    Promise.all(nPromises).then((policies) => {
      let result = {};
      for (let i = 0; i < policies.length; i ++) {
        let ePolicies = policies[i];
        result[nCids[i]] = Object.assign(ePolicies, {
          havExtPlans: (ePolicies.existLife > 0 || ePolicies.existTpd > 0 || ePolicies.existCi > 0) ? "Y" : "N",
          havAccPlans: ePolicies.existPaAdb > 0 ? "Y" : "N"
        })
      }
      logger.log('INFO: getApplicationExisitingPolicies - end');
      resolve(result);
    })
  }).catch((error) => {
    logger.error('ERROR: getApplicationExisitingPolicies', error);
  })
};


//cid = proposer id
module.exports.resetApplicationResidency = function(cid, fe){
  logger.log('INFO: resetApplicationResidency - start', cid);

  return new Promise((resolve, reject)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{

      let nPromises = [];
      let nCids = [];

      if (bundle.formValues) {
        for (let clientId in bundle.formValues) {
          let formValues = bundle.formValues[clientId];

          // reset bundle residency
          if (formValues.residency) {
            formValues.residency = {};
          }

          // reset bundle foreigner
          if (formValues.foreigner) {
            formValues.foreigner = {};
          }
        }

        _getApplicationExisitingPolicies(cid, fe, bundle).then((newExPolicies) => {
          for (let clientId in bundle.formValues) {
            let formValues = bundle.formValues[clientId];
            formValues.policies = _.assign(formValues.policies, {
              existLife: newExPolicies[clientId].existLife,
              existTpd: newExPolicies[clientId].existTpd,
              existCi: newExPolicies[clientId].existCi,
              existPaAdb: newExPolicies[clientId].existPaAdb,
              existTotalPrem: newExPolicies[clientId].existTotalPrem,
              havExtPlans: newExPolicies[clientId].havExtPlans,
              havAccPlans: newExPolicies[clientId].havAccPlans,
              existLifeAXA: (_.get(formValues, 'policies.existLife',false) && formValues.policies.existLife !== newExPolicies[clientId].existLife) ? 0 : _.get(formValues, 'policies.existLifeAXA',0) ,
              existTpdAXA: (_.get(formValues, 'policies.existTpd',false) && formValues.policies.existTpd !== newExPolicies[clientId].existTpd) ? 0 : _.get(formValues, 'policies.existTpdAXA',0),
              existCiAXA: (_.get(formValues, 'policies.existCi',false) && formValues.policies.existCi !== newExPolicies[clientId].existCi) ? 0 : _.get(formValues, 'policies.existCiAXA',0),
              existPaAdbAXA: (_.get(formValues, 'policies.existPaAdb',false) && formValues.policies.existPaAdb !== newExPolicies[clientId].existPaAdb) ? 0 : _.get(formValues, 'policies.existPaAdbAXA',0),
              existTotalPremAXA: (_.get(formValues, 'policies.existTotalPrem',false) && formValues.policies.existTotalPrem !== newExPolicies[clientId].existTotalPrem) ? 0 : _.get(formValues, 'policies.existTotalPremAXA',0),

              existLifeOther: (_.get(formValues, 'policies.existLife',false) && formValues.policies.existLife !== newExPolicies[clientId].existLife) ? 0 : _.get(formValues, 'policies.existLifeOther',0),
              existTpdOther: (_.get(formValues, 'policies.existTpd',false) && formValues.policies.existTpd !== newExPolicies[clientId].existTpd) ? 0 : _.get(formValues, 'policies.existTpdOther',0),
              existCiOther: (_.get(formValues, 'policies.existCi',false) && formValues.policies.existCi !== newExPolicies[clientId].existCi) ? 0 : _.get(formValues, 'policies.existCiOther',0),
              existPaAdbOther: (_.get(formValues, 'policies.existPaAdb',false) && formValues.policies.existPaAdb !== newExPolicies[clientId].existPaAdb) ? 0 : _.get(formValues, 'policies.existPaAdbOther',0),
              existTotalPremOther: (_.get(formValues, 'policies.existTotalPrem',false) && formValues.policies.existTotalPrem !== newExPolicies[clientId].existTotalPrem) ? 0 : _.get(formValues, 'policies.existTotalPremOther',0)
            },
            (_.get(formValues, 'policies.havPndinApp') ? {
              havPndinApp: '',
              pendingLife: 0,
              pendingCi: 0,
              pendingTpd: 0,
              pendingPaAdb: 0,
              pendingTotalPrem: 0,
            } : {}),
            (_.get(formValues, 'policies.ROP_01') ? {
              ROP01_DATA: [],
              ROP_DECLARATION_01: '',
              ROP_DECLARATION_02: '',
            } : {})
          );
          }

          logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:resetApplicationResidency]`);
          _c.updateBundle(bundle, (bResult) => {
            if (bResult && !bResult.error) {
              _getApplicationsByBundle(bundle).then((apps) => {
                let upPromise = [];
                for (let i = 0; i < apps.length; i ++) {
                  let app = apps[i];
                  if (app && !app.error) {
                    let { proposer, insured } = app.applicationForm.values;

                    // reset proposer residency
                    if (proposer.residency) {
                      proposer.residency = {};
                    }

                    // reset proposer foreigner
                    if (proposer.foreigner) {
                      proposer.foreigner = {};
                    }

                    // reset proposer policies
                    if (_.get(app, 'quotation.quotType') === 'SHIELD') {
                      proposer.policies = _.assign({}, {ROP_01: _.get(proposer.policies, 'ROP_01', '')});
                    } else {
                      proposer.policies = _.assign(proposer.policies, {
                        existLife: newExPolicies[cid].existLife,
                        existTpd: newExPolicies[cid].existTpd,
                        existCi: newExPolicies[cid].existCi,
                        existPaAdb: newExPolicies[cid].existPaAdb,
                        existTotalPrem: newExPolicies[cid].existTotalPrem,
                        havExtPlans: newExPolicies[cid].havExtPlans,
                        havAccPlans: newExPolicies[cid].havAccPlans,

                        existLifeAXA: (_.get(proposer, 'policies.existLife',false) && proposer.policies.existLife !== newExPolicies[cid].existLife) ? 0 : _.get(proposer, 'policies.existLifeAXA',0) ,
                        existTpdAXA: (_.get(proposer, 'policies.existTpd',false) && proposer.policies.existTpd !== newExPolicies[cid].existTpd) ? 0 : _.get(proposer, 'policies.existTpdAXA',0),
                        existCiAXA: (_.get(proposer, 'policies.existCi',false) && proposer.policies.existCi !== newExPolicies[cid].existCi) ? 0 : _.get(proposer, 'policies.existCiAXA',0),
                        existPaAdbAXA: (_.get(proposer, 'policies.existPaAdb',false) && proposer.policies.existPaAdb !== newExPolicies[cid].existPaAdb) ? 0 : _.get(proposer, 'policies.existPaAdbAXA',0),
                        existTotalPremAXA: (_.get(proposer, 'policies.existTotalPrem',false) && proposer.policies.existTotalPrem !== newExPolicies[cid].existTotalPrem) ? 0 : _.get(proposer, 'policies.existTotalPremAXA',0),

                        existLifeOther: (_.get(proposer, 'policies.existLife',false) && proposer.policies.existLife !== newExPolicies[cid].existLife) ? 0 : _.get(proposer, 'policies.existLifeOther',0),
                        existTpdOther: (_.get(proposer, 'policies.existTpd',false) && proposer.policies.existTpd !== newExPolicies[cid].existTpd) ? 0 : _.get(proposer, 'policies.existTpdOther',0),
                        existCiOther: (_.get(proposer, 'policies.existCi',false) && proposer.policies.existCi !== newExPolicies[cid].existCi) ? 0 : _.get(proposer, 'policies.existCiOther',0),
                        existPaAdbOther: (_.get(proposer, 'policies.existPaAdb',false) && proposer.policies.existPaAdb !== newExPolicies[cid].existPaAdb) ? 0 : _.get(proposer, 'policies.existPaAdbOther',0),
                        existTotalPremOther: (_.get(proposer, 'policies.existTotalPrem',false) && proposer.policies.existTotalPrem !== newExPolicies[cid].existTotalPrem) ? 0 : _.get(proposer, 'policies.existTotalPremOther',0)

                      }, (_.get(proposer, "policies.havPndinApp")? {
                        havPndinApp: '',
                        pendingLife: 0,
                        pendingCi: 0,
                        pendingTpd: 0,
                        pendingPaAdb: 0,
                        pendingTotalPrem: 0,
                      }: {}));
                    }

                    for (let j = 0; j < insured.length; j ++) {
                      let la = insured[j];

                      // reset life assured residency
                      if (la.residency) {
                        la.residency = {};
                      }

                      // reset life assured foreigner
                      if (la.foreigner) {
                        la.foreigner = {};
                      }

                      //reset life assured policies
                      if (_.get(app, 'quotation.quotType') === 'SHIELD') {
                        la.policies = _.assign({}, {ROP_01: _.get(la.policies, 'ROP_01', '')});
                      } else {
                        la.policies = _.assign(la.policies, {
                          existLife: newExPolicies[la.personalInfo.cid].existLife,
                          existTpd: newExPolicies[la.personalInfo.cid].existTpd,
                          existCi: newExPolicies[la.personalInfo.cid].existCi,
                          existPaAdb: newExPolicies[la.personalInfo.cid].existPaAdb,
                          existTotalPrem: newExPolicies[la.personalInfo.cid].existTotalPrem,
                          havExtPlans: newExPolicies[la.personalInfo.cid].havExtPlans,
                          havAccPlans: newExPolicies[la.personalInfo.cid].havAccPlans,

                          existLifeAXA: (_.get(la, 'policies.existLife',false) && la.policies.existLife !== newExPolicies[la.personalInfo.cid].existLife) ? 0 : _.get(la, 'policies.existLifeAXA',0),
                          existTpdAXA: (_.get(la, 'policies.existTpd',false) && la.policies.existTpd !== newExPolicies[la.personalInfo.cid].existTpd) ? 0 : _.get(la, 'policies.existTpdAXA',0),
                          existCiAXA: (_.get(la, 'policies.existCi',false) && la.policies.existCi !== newExPolicies[la.personalInfo.cid].existCi) ? 0 : _.get(la, 'policies.existCiAXA',0),
                          existPaAdbAXA: (_.get(la, 'policies.existPaAdb',false) && la.policies.existPaAdb !== newExPolicies[la.personalInfo.cid].existPaAdb) ? 0 : _.get(la, 'policies.existPaAdbAXA',0),
                          existTotalPremAXA: (_.get(la, 'policies.existTotalPrem',false) && la.policies.existTotalPrem !== newExPolicies[la.personalInfo.cid].existTotalPrem) ? 0 : _.get(la, 'policies.existTotalPremAXA',0),

                          existLifeOther: (_.get(la, 'policies.existLife',false) && la.policies.existLife !== newExPolicies[la.personalInfo.cid].existLife) ? 0 : _.get(la, 'policies.existLifeOther',0),
                          existTpdOther: (_.get(la, 'policies.existTpd',false) && la.policies.existTpd !== newExPolicies[la.personalInfo.cid].existTpd) ? 0 : _.get(la, 'policies.existTpdOther',0),
                          existCiOther: (_.get(la, 'policies.existCi',false) && la.policies.existCi !== newExPolicies[la.personalInfo.cid].existCi) ? 0 : _.get(la, 'policies.existCiOther',0),
                          existPaAdbOther: (_.get(la, 'policies.existPaAdb',false) && la.policies.existPaAdb !== newExPolicies[la.personalInfo.cid].existPaAdb) ? 0 : _.get(la, 'policies.existPaAdbOther',0) ,
                          existTotalPremOther: (_.get(la, 'policies.existTotalPrem',false) && la.policies.existTotalPrem !== newExPolicies[la.personalInfo.cid].existTotalPrem) ? 0 : _.get(la, 'policies.existTotalPremOther',0)

                        }, (_.get(la, "policies.havPndinApp")? {
                          havPndinApp: "",
                          pendingLife: 0,
                          pendingCi: 0,
                          pendingTpd: 0,
                          pendingPaAdb: 0,
                          pendingTotalPrem: 0,
                        }: {}));
                      }
                    }

                    upPromise.push(
                      new Promise((upResolve) => {
                        appDao.upsertApplication(app._id, app, (upResult) => {
                          logger.log('INFO: resetApplicationResidency - update Application', app._id, 'for', cid);
                          upResolve(upResult);
                        })
                      })
                    )
                  }
                }

                logger.log('INFO: resetApplicationResidency - before update Application', cid, '[num=', upPromise.length, ']');
                Promise.all(upPromise).then((result) => {
                  logger.log('INFO: resetApplicationResidency - end [RETURN=1]', cid);
                  resolve();
                }).catch((error) => {
                  logger.error('ERROR: resetApplicationResidency - end [RETURN=-1]', cid, error);
                });
              }).catch((error) => {
                logger.error('ERROR: resetApplicationResidency - _getApplicationsByBundle', cid, error);
              });
            } else {
              logger.error('ERROR: resetApplicationResidency - end [RETURN=-2]', cid, _.get(bResult, 'error'));
              resolve();
            }
          });
        }).catch((error) => {
          logger.error('ERROR: resetApplicationResidency - getApplicationExisitingPolicies', cid, error);
        })
      } else {
        logger.log('INFO: resetApplicationResidency - end [RETURN=2]', cid);
        resolve();
      }
    });
  });
};

var _preformRollbackApplication = function (cid) {
  logger.log('INFO: _preformRollbackApplication');
  return new Promise((resolve, reject)=>{
    _c.getCurrentBundle(cid).then((bundle)=> {
      //rollback signed Pdf
      let status = bundle.status;
      if (status === _c.BUNDLE_STATUS.START_GEN_PDF || status === _c.BUNDLE_STATUS.SIGN_FNA){
        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; rollback:true; fn:_preformRollbackApplication]`);
        _rollbackApplication2Step1(cid).then(resolve);
      } else {
        resolve();
      }
    }).catch((error) => {
      logger.log('ERROR: preformRollbackApplication', error);
    });
  });
};

var _updateApplicationReplacementOfPolicies = function(cid, newData) {
  logger.log('INFO: updateApplicationReplacementOfPolicies - start', cid);
  return new Promise((resolve, reject)=>{
    _preformRollbackApplication(cid).then(() => {
      _c.getCurrentBundle(cid).then((bundle)=> {
        //reset bundle formValue for prepareReplacementOfPolicies
        if (bundle.formValues) {
          for (let i = 0; i < newData.length; i ++) {
            let nData = newData[i];
            if (nData.bundle) {
              for (let clientId in nData.bundle) {
                logger.log('INFO: updateApplicationReplacementOfPolicies - set bundle formValues', clientId);

                if (nData.bundle[clientId]){
                  let currentBundle = nData.bundle[clientId];
                  let replaceTotalArr = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
                  let isAllZeroAXA = true;
                  let isAllZeroOther = true;
                  for (let i = 0; i < 5; i++) {
                    let keyTotal = replaceTotalArr[i];
                    let keyAXA = replaceTotalArr[i] + 'AXA';
                    let keyOther = replaceTotalArr[i] + 'Other';
                    if (Number(currentBundle[keyAXA]) + Number(currentBundle[keyOther]) !== Number(currentBundle[keyTotal]) ){
                      currentBundle[keyAXA] = 0;
                      currentBundle[keyOther] = 0;
                    }
                    isAllZeroAXA = isAllZeroAXA && Number(currentBundle[keyAXA]) === 0;
                    isAllZeroOther = isAllZeroOther && Number(currentBundle[keyOther]) === 0;
                  }
                  if (isAllZeroAXA) {
                    currentBundle['replacePolTypeAXA'] = '-';
                  }
                  if (isAllZeroOther) {
                    currentBundle['replacePolTypeOther'] = '-';
                  }
                }
                if (bundle.formValues[clientId]) {
                  bundle.formValues[clientId].policies = nData.bundle[clientId];
                } else {
                  bundle.formValues[clientId] = {
                    policies: nData.bundle[clientId]
                  };
                }
              }
            }
          }
        }

        logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:updateApplicationReplacementOfPolicies]`);
        _c.updateBundle(bundle, (bResult) => {
          if (bResult && !bResult.error) {
            _getApplicationsByBundle(bundle).then((apps) => {
              let promises = [];
              for (let i = 0; i < apps.length; i ++) {
                let app = apps[i];
                if (app && !app.error) {
                  let nData = newData.find(function(dataset) {
                    return dataset.application && dataset.application._id == app._id;
                  });

                  if (nData) {
                    let { proposer, insured } = nData.application.applicationForm.values;
                    if (proposer.policies) {
                      let policies = proposer.policies;
                      let replaceTotalArr = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
                      let isAllZeroAXA = true;
                      let isAllZeroOther = true;
                      for (let i = 0; i < 5; i++) {
                        let keyTotal = replaceTotalArr[i];
                        let keyAXA = replaceTotalArr[i] + 'AXA';
                        let keyOther = replaceTotalArr[i] + 'Other';
                        if (Number(policies[keyAXA]) + Number(policies[keyOther]) !== Number(policies[keyTotal]) ){
                          policies[keyAXA] = 0;
                          policies[keyOther] = 0;
                        }
                        isAllZeroAXA = isAllZeroAXA && Number(policies[keyAXA]) === 0;
                        isAllZeroOther = isAllZeroOther && Number(policies[keyOther]) === 0;
                      }
                      if (isAllZeroAXA) {
                        policies['replacePolTypeAXA'] = '-';
                      }
                      if (isAllZeroOther) {
                        policies['replacePolTypeOther'] = '-';
                      }
                    }
                    for (let j = 0; j < insured.length; j ++) {
                      let la = insured[j];
                      if (la.policies) {
                        let policies = la.policies;
                        let replaceTotalArr = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
                        let isAllZeroAXA = true;
                        let isAllZeroOther = true;
                        for (let i = 0; i < 5; i++) {
                          let keyTotal = replaceTotalArr[i];
                          let keyAXA = replaceTotalArr[i] + 'AXA';
                          let keyOther = replaceTotalArr[i] + 'Other';
                          if (Number(policies[keyAXA]) + Number(policies[keyOther]) !== Number(policies[keyTotal]) ){
                            policies[keyAXA] = 0;
                            policies[keyOther] = 0;
                          }
                          isAllZeroAXA = isAllZeroAXA && Number(policies[keyAXA]) === 0;
                          isAllZeroOther = isAllZeroOther && Number(policies[keyOther]) === 0;
                        }
                        if (isAllZeroAXA) {
                          policies['replacePolTypeAXA'] = '-';
                        }
                        if (isAllZeroOther) {
                          policies['replacePolTypeOther'] = '-';
                        }
                      }
                    }
                    app.applicationForm = nData.application.applicationForm;

                    //reset residency menu completeness
                    let {completedMenus, checkedMenu} = app.applicationForm.values;
                    if (completedMenus && completedMenus.length > 0) {
                      let completedIndex = completedMenus.indexOf('menu_residency');
                      if (completedIndex > -1) {
                        completedMenus.splice(completedIndex, 1);
                      }
                    }
                    if (checkedMenu && checkedMenu.length > 0) {
                      let checkedIndex = checkedMenu.indexOf('menu_residency');
                      if (checkedIndex > -1) {
                        checkedMenu.splice(checkedIndex, 1);
                      }
                    }
                    // if (checkedMenu) {
                    //   let removeIndex = [];
                    //   for (let j = 0; j < checkedMenu.length; j ++) {
                    //     if (checkedMenu[j] === 'menu_residency') {
                    //       removeIndex.push(j);
                    //     }
                    //   }
                    //   for (let j = removeIndex.length - 1; j >= 0; j--) {
                    //     checkedMenu.splice(removeIndex[j], 1);
                    //   }
                    // }

                    promises.push(
                      new Promise((aResolve) => {
                        appDao.upsertApplication(app._id, app, (upApp) => {
                          logger.log('INFO: updateApplicationReplacementOfPolicies - update Application', app._id, 'for', cid);
                          aResolve(upApp);
                        });
                      })
                    );
                  }
                }
              }

              logger.log('INFO: updateApplicationReplacementOfPolicies - before update applications', cid, '[num=', promises.length, ']');
              Promise.all(promises).then((result) => {
                logger.log('INFO: updateApplicationReplacementOfPolicies - end [RETURN=1]', cid);
                resolve();
              }).catch((error) => {
                logger.error('ERROR: updateApplicationReplacementOfPolicies - end [RETURN=-1]', cid, error);
              });
            }).catch((error) => {
              logger.error('ERROR: updateApplicationReplacementOfPolicies - _getApplicationsByBundle', cid, error);
            });
          } else {
            logger.error('ERROR: updateApplicationReplacementOfPolicies - end [RETURN=-2]', cid, _.get(bResult, 'error'));
            resolve();
          }
        });
      }).catch((error) => {
        logger.error('ERROR: updateApplicationReplacementOfPolicies - getCurrentBundle', cid, error);
      });
    }).catch((error) => {
      logger.error('ERROR: updateApplicationReplacementOfPolicies - preformRollbackApplication', cid, error);
    });
  });
};
module.exports.updateApplicationReplacementOfPolicies = _updateApplicationReplacementOfPolicies;

var _updateApplicationAppStatus2Delete = function(appId) {
  logger.log('INFO: updateApplicationAppStatus2Delete', appId);
  return new Promise((resolve, reject) => {
    appDao.getApplication(appId, function(app) {
      app.appStatus = 'DELETE';

      appDao.upsertApplication(app._id, app, function(result) {
        if (result && !result.error) {
          resolve();
        } else {
          reject();
        }
      });
    });
  });
};

let updateQuotCheckedList = function(bundle, quotCheckedList) {
  logger.log('INFO: bundle -- updateQuotCheckedList');
  return new Promise((resolve)=>{
    if (!_.get(bundle, 'isFnaReportSigned')) {
      _.set(bundle, 'clientChoice.quotCheckedList', quotCheckedList);
    }
    logger.log(`INFO: BundleUpdate - [cid:${_.get(bundle, 'pCid')}; bundleId:${_.get(bundle, 'id')}; fn:updateQuotCheckedList]`);
    _c.updateBundle(bundle, resolve);
  });
};
module.exports.updateQuotCheckedList = updateQuotCheckedList;

const _updateApplicationFnaAnsToApplications = function(application) {
  const fnaQuestionFields = ['ROADSHOW01', 'ROADSHOW02', 'ROADSHOW03', 'ROADSHOW04', 'TRUSTED_IND01', 'TRUSTED_IND02', 'TRUSTED_IND04'];
  let cid = _.get(application, 'pCid');
  let appDeclaration = _.get(application, 'applicationForm.values.proposer.declaration', {});

  logger.log('INFO: _updateApplicationFnaAnsToApplications - start', cid);

  return _c.getCurrentBundle(cid).then((bundle) => {
    let bunDeclaration = _.get(bundle, `formValues.${cid}.declaration`, {});

    _.forEach(fnaQuestionFields, (fieldName) => {
      bunDeclaration[fieldName] = appDeclaration[fieldName];
    });

    return new Promise((bResolve, bReject) => {
      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:_updateApplicationFnaAnsToApplications]`);
      _c.updateBundle(bundle, (bResult) => {
        if (bResult && !bResult.error) {
          bundle._rev = bResult.rev;
          bResolve(bundle);
        } else {
          bReject(new Error('Fail to update bundle'));
        }
      });
    }).then((newBundle) => {
      logger.log('INFO: _updateApplicationFnaAnsToApplications - get applying applications', cid);
      return _getApplicationsByBundle(newBundle).then((apps) => {
        let filterApps = _.filter(apps, (app) => {return application._id !== app._id;});

        let upAppsPromise = [];

        _.forEach(filterApps, (app) => {
          let originalApp = _.cloneDeep(app);
          _.forEach(fnaQuestionFields, (fieldName) => {
            let declaration = _.get(app, 'applicationForm.values.proposer.declaration');
            if (_.has(appDeclaration, `${fieldName}`)) {
              _.set(app, `applicationForm.values.proposer.declaration.${fieldName}`, appDeclaration[fieldName]);
            } else if (_.has(declaration, `${fieldName}`)) {
              delete declaration[fieldName];
            }
          });

          if (!_.isEqual(_.get(originalApp, 'applicationForm.values.proposer.declaration'), _.get(app, 'applicationForm.values.proposer.declaration'))) {
            logger.log(`INFO: _updateApplicationFnaAnsToApplications - diff found between ${application._id} and ${app._id} for`, cid);
            if (app.appStep <= 1) {
              app.appStep = 0;
            }
            upAppsPromise.push(new Promise((aResolve, aReject) => {
              appDao.upsertApplication(app._id, app, (upApp) => {
                if (upApp && !upApp.error) {
                  aResolve(true);
                } else {
                  aReject(new Error('Fail to update application ' + app._id));
                }
              });
            }));
          }
        });

        return Promise.all(upAppsPromise).then((results) => {
          logger.log('INFO: _updateApplicationFnaAnsToApplications - end', cid);
          return results;
        });
      });
    });
  });
};
module.exports.updateApplicationFnaAnsToApplications = _updateApplicationFnaAnsToApplications;

var getApplicationStatus = function(app) {
  let pCid = _.get(app, 'pCid');
  let bundleId = _.get(app, 'bundleId');
  logger.log('INFO: getApplicationStatus', app.id, pCid, bundleId);
  return new Promise((resolve, reject) => {
    if (pCid && bundleId) {
      _c.getBundle(pCid, bundleId, function(bundle) {
        let foundApp = _.find(_.get(bundle, 'applications', []), (item) => {
          return item.applicationDocId === app.id;
        });
        logger.log('INFO: getApplicationStatus success');
        resolve(foundApp.appStatus);
      });
    } else {
      logger.error('ERROR: getApplicationStatus fails');
      reject();
    }
  });
};
module.exports.getApplicationStatus = getApplicationStatus;
