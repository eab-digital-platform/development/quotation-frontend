const _c = require("./common");
const _a = require("./applications");
const _n = require("./needs");
const _ = require("lodash");

const dao = require('../../cbDaoFactory').create();
const cDao = require('../client');
const nDao = require('../needs');
const appDao = require('../application');

const logger = global.logger || console;

const CLIENT_TYPE = {
  MYSELF: "M",
  SPOUSE: "S",
  DEPENDANTS: "D",
  PROPOSER: "P",
  INSURANCE: "I"
}

var recursive = function(template, values = {}, changedValues = {}, status, relationship, hasFNA, isMyselfNSpouse, invalidResult={}){
  let {id: tid} = template;
  let cValue = tid? _.get(changedValues, tid) : changedValues;
  let oValue = tid? _.get(values, tid) : values;
  let invalidate = _.get(template, "invalidate");
  let items = _.get(template, "items");
  if(invalidate && cValue != oValue){
    let {fna, quotation, application} = invalidate;
    if(tid === 'gender' && !relationship){
      invalidResult.removeApplicant = true;
    }
    if(hasFNA && status > _c.BUNDLE_STATUS.START_FNA && fna && ((!relationship && fna.indexOf(CLIENT_TYPE.MYSELF)>-1) ||
        (relationship === "SPO" && isMyselfNSpouse && fna.indexOf(CLIENT_TYPE.SPOUSE)>-1) ||
        (relationship && fna.indexOf(CLIENT_TYPE.DEPENDANTS)>-1))){
      if(tid === "gender"){
        invalidResult.removeDependant = true;
      }
      let spouse = _.find(changedValues.dependants, d=>d.relationship === 'SPO');
      if(tid === 'marital' && oValue === 'M' && oValue != cValue && spouse) {
        invalidResult.removeSpouseRelation = spouse.cid;
        spouse.relationship = null;
      }

      invalidResult.fna = true;
    }
    if(status > _c.BUNDLE_STATUS.HAVE_FNA && quotation && ((!relationship && quotation.indexOf(CLIENT_TYPE.PROPOSER)>-1) ||
        (relationship && quotation.indexOf(CLIENT_TYPE.INSURANCE) > -1))){
      invalidResult.quotation = true;
    }
    if(status >= _c.BUNDLE_STATUS.HAS_PRE_EAPP && application && ((!relationship && application.indexOf(CLIENT_TYPE.PROPOSER)>-1) ||
        (relationship && application.indexOf(CLIENT_TYPE.INSURANCE) > -1))){
      if(tid !== 'prStatus' || cValue){
        invalidResult.application = true;
      }
    }
    return invalidResult;
  }
  if(items){
    for(let i = items.length-1; i>=0; i--){
      let errObj = recursive(items[i], oValue, cValue, status, relationship, hasFNA, isMyselfNSpouse,  invalidResult);
    }
  }
  return invalidResult;
}


var _invalidFamilyMember = function(cid, agent, fid, invalidResult = {}){
  return new Promise((resolve)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      cDao.getProfile(cid, true).then((profile)=>{
        let dependants = _.map(profile.dependants, d=>d.cid);
        if (cid === fid || _.indexOf(dependants, fid) === -1) {
          dependants = _.concat(dependants, fid);
        }
        //if family member type is 'SPO',invalidate those related plans
        let isAllowInvalidate = false;
        _.forEach(profile.dependants,(value,index)=>{
           if (value.relationship && value.relationship === 'SPO' && value.cid === fid && invalidResult.removeApplicant && invalidResult.removeDependant && ( typeof invalidResult.code === 'undefined' ? true : false)){
             isAllowInvalidate = true;
           }

        });
        if (isAllowInvalidate){
          _.forEach(bundle.applications,(application)=>{
            if (application && application.quotationDocId && application.appStatus !== 'INVALIDATED'){
              dao.getDoc(application.quotationDocId, (quotation)=>{
                //remove dependents in applications
                let performInvalidate = false;
                performInvalidate = quotation && quotation.iCid && quotation.iCid === fid;
                if (performInvalidate){
                  logger.log(`Start invalid fna: cid - ${cid}, remove application - ${application.quotationDocId}`);
                  _a.onInvalidateApplicationById(cid, application.quotationDocId);
                }
              });
             }
          });
        }
        //notice that promises are async, so updateBundle in promises may occur error
        //if family member is a dependant of insurance, invalidate all related plans
        let {features = ""} = agent;
        let isFNA = features.indexOf("FNA") > -1 ? true: false;
        let resp = {};
        if (!isFNA || dependants.indexOf(fid) > -1) {
          let promises = [];
          _.forEach(_.keys(invalidResult), key => {
            //prevent they key is note cid, i.e. code
            let tid = _.get(invalidResult, `${key}.cid`);
            if (tid) {
              promises.push(_invalidClient(tid, fid, agent, invalidResult[key].errObj));
            }
          });

          Promise.all(promises).then(args => {
            resp.bundle = _.get(_.find(args, arg => arg.cid === fid), 'bundle');
            resp.invalid = {
              initApplications: _.get(_.find(args, arg => arg.cid === cid), 'invalid.initApplications')
            };
            resolve(resp);
          });
        }
        else {
          resolve();
        }
      }).catch((error)=>{
        logger.error("Error in _invalidFamilyMember->getProfile: ", error);
      });
    }).catch((error)=>{
      logger.error("Error in _invalidFamilyMember->getCurrentBundle: ", error);
    });
  });
}
module.exports.invalidFamilyMember = _invalidFamilyMember;

const hasInvalidApplication = function(pCid, cid, applications) {
  return new Promise(resolve => {
    applications = _.filter(applications, application => application.appStatus !== 'INVALIDATED');
    let promises = [];
    _.forEach(applications, application => {
      promises.push(new Promise(resolve2 => {
        dao.getDoc(application.quotationDocId, (quotation)=>{
          let cids = [];
          if (quotation.quotType === 'SHIELD') {
            cids = _.keys(quotation.insureds);
            cids.push(quotation.pCid);
          } else {
            cids = [quotation.pCid, quotation.iCid];
          }

          resolve2({
            quotationDocId: application.quotationDocId,
            applicationDocId: application.applicationDocId,
            appStatus: application.appStatus,
            isFullySigned: application.isFullySigned,
            cids
          });

        });
      }));
    });
    Promise.all(promises).then((apps) => {
      // invalidate bundle if:
      // 1. there is applying or submitted case
      // 2. pCid === iCid or there is any impact application
      let hasInvalidBundle = _.find(apps, app => 
        app.isFullySigned &&
        ["APPLYING", "SUBMITTED"].indexOf(app.appStatus) > -1 &&
        (pCid === cid || app.cids.indexOf(cid) > -1)
      );
      let hasInvalidApp = _.find(apps, app => ["APPLYING"].indexOf(app.appStatus) > -1 && app.cids.indexOf(cid) > -1);
      let hasInvalidBi = _.find(apps, app => app.cids.indexOf(cid) > -1);
      let hasProgressBi = _.find(apps, app => app.appStatus !== 'SUBMITTED');
      resolve({hasInvalidBi, hasInvalidApp, hasInvalidBundle, hasProgressBi});
    });
  })
}

var _invalidClient = function(cid, fid, agent, errObj={}){
  return new Promise((resolve, reject)=>{
    let resp = {cid};
    let {agentCode: aid, features= ""} = agent;
    let hasFNA = features.indexOf("FNA") > -1? true: false;
    let isChange = errObj.fna || errObj.quotation || errObj.application? true: false;
    let fnaParam = { fid, removeDependant: errObj.removeDependant, removeApplicant: errObj.removeApplicant };
    _c.getCurrentBundle(cid).then((bundle)=>{
      let checkCreateNewBundle = function() {
        return new Promise(checkResolve => {
          if (bundle.status >= _c.BUNDLE_STATUS.FULL_SIGN && hasFNA && isChange) {
            if (errObj.fna) {
              checkResolve(true);
            } else {
              hasInvalidApplication(cid, fid, bundle.applications)
              .then((invalidResult = {})=>{
                checkResolve(invalidResult.hasInvalidBundle);
              })
              .catch(error => {
                logger.log("Error in _invalidClient->hasInvalidApplication: ", error);
              });
            }
          }
          else {
            checkResolve(false);
          }
        });
      }

      let createNewBundle = function(isCreateNewBundle) {
           return new Promise(newBundleResolve => {
             if (isCreateNewBundle) {
                _c.createNewBundle(cid, agent).then((newBundle)=>{
                  cDao.getProfile(cid, true).then((profile)=>{
                    //update profile bundle
                    profile.bundle = newBundle;

                    dao.updDoc(cid, profile, ()=>{
                      if (errObj.fna) {
                        _n.invalidFna(cid, fnaParam);
                      }

                      resp = _.assign(resp, {bundle: cid === fid ? newBundle : null, invalid: {initApplications: true}});

                      newBundleResolve(true);
                    });
                  }).catch((error)=>{
                    logger.error("Error in _invalidClient->getProfile: ", error);
                  });
                }).catch((error)=>{
                  logger.error("Error in _invalidClient->createNewBundle: ", error);
                });
             } else {
               newBundleResolve(false);
             }
           })
      }

      let invalidateClient = function (haveCreateNewBundle) {
        return new Promise(invalidClientResolve=> {
          let invalidResp = {};
          if (haveCreateNewBundle || !isChange) {
            invalidClientResolve();
          } else {
            let isInitClientChoice = false;
            let rollbackStatus = bundle.status;

            let _removeSpouseRelationship = function(){
              return new Promise(removeSpouseRelationResolve=>{
                let sid = errObj.removeSpouseRelation;
                if (!sid) {
                  removeSpouseRelationResolve();
                } else {
                  cDao.getProfile(sid, true).then(sProfile => {
                    let myself = _.find(sProfile.dependants, d=>d.relationship === 'SPO');
                    myself.relationship = null;
                    dao.updDoc(sid, sProfile, removeSpouseRelationResolve);
                  })
                }
              })
            }

            let _invalidFna = function(){
              return new Promise(fnaResolve=>{
                if(errObj.fna && hasFNA){
                  logger.log(`Start invalid fna: cid - ${cid}, remove dependant - ${errObj.removeDependant}, remove applicant - ${errObj.removeApplicant}`);
                  invalidResp.fna = true;
                  rollbackStatus = rollbackStatus < _c.BUNDLE_STATUS.SIGN_FNA ? rollbackStatus : _c.BUNDLE_STATUS.SIGN_FNA;
                  _n.invalidFna(cid, fnaParam).then(fnaResolve);
                }
                else {
                  fnaResolve();
                }
              })
            }

            let _invalidQuotation = function(){
              return new Promise(quotationReoslve=>{
                if(errObj.quotation){
                  if(fid !== cid){
                    logger.log(`INFO: invalidate application by family member: cid - ${cid}, fid - ${fid}`);
                    _a.invalidateApplicationByFamilyClientId(cid, fid).then((hasInvalidateBi)=>{
                      if (hasInvalidateBi) {
                        invalidResp.initApplications = true;
                      }

                      //if there is any BI invalidated, client choice should be init
                      isInitClientChoice = hasInvalidateBi;
                      quotationReoslve();
                    }).catch((error)=>{
                      logger.error("Error in _invalidClient->invalidateApplicationByFamilyClientId: ", error);
                    });
                  }
                  else {
                    logger.log(`INFO: invalidate application by client: cid - ${cid}`);
                    invalidResp.initApplications = true;

                    // since all application would be invalid, client choice should always init in this condition
                    isInitClientChoice = true;

                    _a.invalidateApplicationByClientId(cid)
                      .then(quotationReoslve)
                      .catch((error)=>{
                        logger.error("Error in _invalidClient->invalidateApplicationByClientId: ", error);
                      });
                  }
                }
                else {
                  quotationReoslve();
                }
              });
            }

            let _initClientChoice = function() {
              return new Promise(clientChoiceResolve => {
                if (isInitClientChoice) {
                  invalidResp.initApplications = true;
                  rollbackStatus = rollbackStatus < _c.BUNDLE_STATUS.HAS_PRE_EAPP ? rollbackStatus : _c.BUNDLE_STATUS.HAS_PRE_EAPP;

                  _a.initClientChoice(cid).then(clientChoiceResolve).catch((error)=>{
                    logger.error("Error in _invalidClient->initClientChoice: ", error);
                  });
                } else {
                  clientChoiceResolve();
                }
              });
            }

            let _invalidApplication = function() {
              return new Promise(applicationResolve => {

                if (!errObj.quotation && errObj.application) {
                  _a.rollbackApplication2Proposal(cid, cid === fid ? null : fid, hasFNA)
                    .then((result = {})=>{
                      // if no application, rollback to has pre eapp status.
                      let hasApplications = _.find(result.applications, application=>application.appStatus === 'APPLYING' || !application.applicationDocId);

                      // prefer rollback status is greater than has_pre_eapp if hasApplications = true
                      rollbackStatus = rollbackStatus < _c.BUNDLE_STATUS.HAS_PRE_EAPP || hasApplications ? rollbackStatus : _c.BUNDLE_STATUS.HAS_PRE_EAPP;

                      //get invalidated application
                      if (_.size(result.invalidated)) {
                        invalidResp.initApplications = true;
                      }

                      applicationResolve();
                    })
                    .catch((error)=>{
                      logger.error("Error in _invalidClient->invalidateApplicationByClientId: ", error);
                    });
                } else {
                  applicationResolve();
                }
              })
            }

          let _rollbackApplication2Step1 = function() {
            return new Promise(rollbackResolve => {
              if (errObj.fna && hasFNA && !errObj.quotation && !errObj.application && (bundle.status < _c.BUNDLE_STATUS.FULL_SIGN)

              || fid && !errObj.fna && errObj.quotation  && (bundle.status < _c.BUNDLE_STATUS.FULL_SIGN)

                ){
                  rollbackStatus = rollbackStatus < _c.BUNDLE_STATUS.START_APPLY ? rollbackStatus : _c.BUNDLE_STATUS.START_APPLY;
                  _a.rollbackApplication2Step1(cid).then(rollbackResolve).catch((error) => {
                    logger.error("Error in _rollbackApplication2Step1->rollbackApplication2Step1: ", error);
                  });
              }
              else {
                rollbackResolve();
              }
            });
          }

          _removeSpouseRelationship()
            .then(_invalidFna)
            .then(_invalidQuotation)
            .then(_initClientChoice)
            .then(_invalidApplication)
            .then(_rollbackApplication2Step1)
            .then(()=>{
              _c.rollbackStatus(cid, rollbackStatus)
                .then(()=>{
                  resp = _.assign(resp, {invalid: invalidResp});
                  invalidClientResolve();
                })
                .catch(error => {
                    logger.error("Error in _invalidClient->rollbackStatus", error);
                });
            })
            .catch(error => {
              logger.error("Error in _invalidClient", error);
            });
          }
        })
      }

      checkCreateNewBundle()
        .then(createNewBundle)
        .then(invalidateClient)
        .then(()=>{
          resolve(resp);
        })
        .catch(error => {
          logger.error("Error in _invalidClient: ", error);
        })
    }).catch((error)=>{
      logger.error("Error in _invalidClient->getCurrentBundle: ", error);
    });
  });
}
module.exports.invalidClient = _invalidClient;

var getWarnCode = function(errObj, bundle, hasFNA, cid, fid){
  return new Promise(resolve => {
    //new profile if no cid
    if (!fid) {
      resolve(_c.CHECK_CODE.VALID);
    } else {
      let {applications = [], status} = bundle;

      //get all valid quotation data
      hasInvalidApplication(cid, fid, applications).then((invalidResult = {})=>{
        let returnCode = _c.CHECK_CODE.VALID;
        let hasFNAError = hasFNA && errObj.fna;
        if ((hasFNAError || errObj.quotation || errObj.application) && status >= _c.BUNDLE_STATUS.FULL_SIGN){
          if (status > _c.BUNDLE_STATUS.FULL_SIGN && ((errObj.quotation && invalidResult.hasInvalidBi) || (errObj.application && invalidResult.hasInvalidApp) )) {
                returnCode =  _c.CHECK_CODE.INVALID_BUNDLE;
          } else if (status === _c.BUNDLE_STATUS.FULL_SIGN && invalidResult.hasInvalidBundle && (invalidResult.hasInvalidBi || invalidResult.hasInvalidApp)) {
            returnCode = _c.CHECK_CODE.INVALID_SIGN_CASE;
          }
        } else if (errObj.quotation && status >= _c.BUNDLE_STATUS.HAVE_BI && invalidResult.hasInvalidBi){
          returnCode = _c.CHECK_CODE.INVALID_QU;
        } else if ((hasFNAError) && status === _c.BUNDLE_STATUS.SIGN_FNA){
          returnCode = _c.CHECK_CODE.INVALID_FNA_REPORT;
        } else if (errObj.application && status >= _c.BUNDLE_STATUS.HAS_PRE_EAPP && invalidResult.hasInvalidApp){
          returnCode = status === _c.BUNDLE_STATUS.SIGN_FNA ? _c.CHECK_CODE.INVALID_FNA_REPORT : _c.CHECK_CODE.INVALID_APP;
        }
        resolve(returnCode);
      }).catch(error => {
        logger.log("Error in _invalidClient->hasInvalidApplication: ", error);
      });
    }
  });
}

const validClient = function(template, cid, hasFNA, profile, tid) {
  return new Promise((resolve, reject) => {
    _c.getCurrentBundle(tid).then((bundle)=>{
      if (bundle) {
        if (bundle.status < _c.BUNDLE_STATUS.HAVE_FNA){
          resolve({cid: tid, code: _c.CHECK_CODE.VALID});
        }
        else{
          dao.getDoc(profile.cid, (oProfile)=>{
            nDao.getItem(tid, nDao.ITEM_ID.PDA).then(pda => {
              let isMyselfNSpouse = _.get(pda, 'applicant') === 'joint';
              let relationship = _.get(_.find(profile.dependants, d => d.cid === tid), "relationship");
              let result = recursive(template, oProfile, profile, bundle.status, relationship, hasFNA, isMyselfNSpouse);
              let handleRelationship = new Promise(resolve2 => {
                if (profile.relationship && cid != profile.cid && tid === cid) {
                  cDao.getProfile(cid, true).then((cProfile)=>{
                    let dependant = _.find(cProfile.dependants, d=>d.cid === profile.cid);
                    if(dependant && dependant.relationship != profile.relationship){
                      result.fna = true;
                      result.quotation = true;
                      if(dependant.relationship === 'SPO'){
                        result.removeApplicant = true;
                      }
                    }
                    resolve2();
                  }).catch((error)=>{
                    logger.error("Error in _onSaveClient->getCurrentBundle: ", error);
                  });
                } else {
                  resolve2();
                }
              });

            handleRelationship.then(() => {
                getWarnCode(result, bundle, hasFNA, cid, profile.cid)
                  .then(warnCode => {
                    resolve({
                      errObj: result,
                      code: warnCode,
                      hasInvalidateField: result.fna || result.quotation || result.application,
                      cid: tid
                      //code: warnCode  === _c.CHECK_CODE.INVALID_FNA_REPORT? _c.CHECK_CODE.INVALID_FNA_REPORT: _c.CHECK_CODE.VALID
                    });
                  })
                  .catch(error => logger.error("Error in _onSaveClient->getWarnCode: ", error));
              })
              .catch(error => logger.error("Error in onSaveClient->handleRelationship: ", error));
          })
        });
        }
      } else {
        logger.error("Error in _onSaveClient->getCurrentBundle, cannot get bundle");
        resolve();
      }
    }).catch((error)=>{
      logger.error("Error in _onSaveClient->getProfile: ", error);
    });
  });
}

var _onSaveClient = function(cid, hasFNA, profile){
  return new Promise((resolve) => {
    if (!profile.cid){
      resolve({code: _c.CHECK_CODE.VALID});
    }
    else {
      cDao.getProfileLayout({}, (template)=>{
        let promises = [];
        _.forEach(_.concat(profile.cid, _.map(profile.dependants, 'cid')), tid => {
          promises.push(validClient(template, cid, hasFNA, profile, tid));
        });

        Promise.all(promises).then(args => {
          let result = _.keyBy(args, 'cid');
          // get current client warning message first
          result.code = _.get(result, `${cid}.code`, _c.CHECK_CODE.VALID);
          if (result.code === _c.CHECK_CODE.VALID) {
            result.code = _.find(args, arg=>arg.code !== _c.CHECK_CODE.VALID) ? _c.CHECK_CODE.INVALID_DEPENDANT_BUNDLE : _c.CHECK_CODE.VALID;
          }
          resolve(result);
        });

      })
    }
  });
}
module.exports.onSaveClient = _onSaveClient;

var _onSaveTrustedIndividual = function(cid, tiInfo, nPda){
  return new Promise((resolve)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      if(bundle.status === _c.BUNDLE_STATUS.START_FNA){
        resolve({code: _c.CHECK_CODE.VALID});
      }
      else {
        cDao.getProfile(cid, false).then((profile)=>{
          nDao.getItem(cid, nDao.ITEM_ID.PDA).then((oPda)=>{
            let cnt = 0, hasChange = false, pda = nPda || oPda;
            let {age=0, language="", education=""} = profile;
            if(age >= 62){ cnt++; }
            if(language.indexOf("en") == -1){ cnt++; }
            if(education == "below") { cnt++ }
            if(cnt >=2 && pda.trustedIndividual === "Y"){
              for(var i in tiInfo){
                if(tiInfo[i] != _.get(profile, `trustedIndividuals[${i}]`)){
                  hasChange = true;
                }
              }
            }
            if(hasChange){
              resolve({code: 104});
            }
            else {
              resolve({code: _c.CHECK_CODE.VALID});
            }
          }).catch((error)=>{
            logger.error("Error in _onSaveTrustedIndividual->nDao.getItem: ", error);
          });
        }).catch((error)=>{
          logger.error("Error in _onSaveTrustedIndividual->getProfile: ", error);
        });
      }
    }).catch((error)=>{
      logger.error("Error in _onSaveTrustedIndividual->getCurrentBundle: ", error);
    });
  })
}

module.exports.onSaveTrustedIndividual = _onSaveTrustedIndividual

var _updateApplicationTrustedIndividual = function(cid, hasTi, tiInfo){
  logger.log('INFO: updateApplicationTrustedIndividual - start', cid, '[hasTi=', hasTi, ';TiDataIsEmpty=', _.isEmpty(tiInfo), ']');

  return new Promise((resolve, reject)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      if (bundle.formValues[cid]) {
        let { declaration, extra } = bundle.formValues[cid];
        if (declaration) {
          if (declaration.TRUSTED_IND01) {
            declaration.TRUSTED_IND01 = '';
          }
          if (declaration.TRUSTED_IND02) {
            declaration.TRUSTED_IND02 = '';
          }
          if (declaration.TRUSTED_IND04) {
            declaration.TRUSTED_IND04 = '';
          }
          if (hasTi) {
            declaration.trustedIndividuals = tiInfo;
          } else if (declaration.trustedIndividuals) {
            delete declaration.trustedIndividuals;
          }
        }
        if (extra && extra.hasTrustedIndividual) {
          extra.hasTrustedIndividual = hasTi ? 'Y' : 'N';
        }
      }

      logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:updateApplicationTrustedIndividual]`);
      _c.updateBundle(bundle, (bResult) => {
        if (bResult && !bResult.error) {
          _a.getApplicationsByBundle(bundle).then((apps) => {
            let upPromise = [];
            for (let i = 0; i < apps.length; i ++) {
              let app = apps[i];
              if (app && !app.error) {
                let { declaration, extra } = app.applicationForm.values.proposer;
                let isUpdate = false;

                if (declaration) {
                  if (declaration.TRUSTED_IND01) {
                    declaration.TRUSTED_IND01 = '';
                    if (!isUpdate) { isUpdate = true; }
                  }
                  if (declaration.TRUSTED_IND02) {
                    declaration.TRUSTED_IND02 = '';
                    if (!isUpdate) { isUpdate = true; }
                  }
                  if (declaration.TRUSTED_IND04) {
                    declaration.TRUSTED_IND04 = '';
                    if (!isUpdate) { isUpdate = true; }
                  }
                  if (hasTi) {
                    declaration.trustedIndividuals = tiInfo;
                    if (!isUpdate) { isUpdate = true; }
                  } else if (declaration.trustedIndividuals) {
                    delete declaration.trustedIndividuals;
                    if (!isUpdate) { isUpdate = true; }
                  }
                }

                if (extra && extra.hasTrustedIndividual) {
                  extra.hasTrustedIndividual = hasTi ? 'Y' : 'N';
                  if (!isUpdate) { isUpdate = true; }
                }

                if (isUpdate) {
                  upPromise.push(
                    new Promise((upResolve) => {
                      appDao.upsertApplication(app._id, app, (upResult) => {
                        logger.log('INFO: updateApplicationTrustedIndividual - update Application', app._id, 'for', cid);
                        upResolve(upResult);
                      });
                    })
                  );
                }
              }
            }

            logger.log('INFO: updateApplicationTrustedIndividual - before update Applications', cid, '[num=', upPromise.length, ']');
            Promise.all(upPromise).then((result) => {
              logger.log('INFO: updateApplicationTrustedIndividual - end [RETURN=1]', cid);
              resolve();
            }).catch((error) => {
              logger.error('ERROR: updateApplicationTrustedIndividual [RETURN=-3]', cid, error);
            })
          }).catch((error) => {
            logger.error('ERROR: updateApplicationTrustedIndividual [RETURN=-2]', cid, error);
          })
        } else {
          logger.error('ERROR: updateApplicationTrustedIndividual - end [RETURN=-1]', cid);
          resolve();
        }
      });
    })
  });
}
module.exports.updateApplicationTrustedIndividual = _updateApplicationTrustedIndividual;

var _invalidTrustedIndividual = function(cid){
  let promises = [
    _n.invalidFna(cid)
  ];
  promises.push(new Promise((resolve2)=>{
    //for eApp
    resolve2();
  }))
  return Promise.all(promises);
}

module.exports.invalidTrustedIndividual = _invalidTrustedIndividual;
