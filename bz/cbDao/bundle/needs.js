const _c = require("./common");
const _a = require("./applications");
const _cl = require("./client");
const _q = require('./quotation');
const _ = require("lodash");

const dao = require('../../cbDaoFactory').create();
const cDao = require('../client');
const nDao = require('../needs');
const clientChoiceHandler = require('../../handler/ClientChoiceHandler');

const logger = global.logger || console;


var _onSaveNeedItem = function(cid, data, itemId, extra = {}){
  return new Promise((resolve)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      let status = bundle.status;
      //no fna, pass
      if(status <= _c.BUNDLE_STATUS.HAVE_FNA){
        resolve({code: _c.CHECK_CODE.VALID});
      }
      //if no fully-signed case, pass, otherwise, prompt warning message
      else if(status >= _c.BUNDLE_STATUS.FULL_SIGN){
        let code = _.find(
          bundle.applications,
          app=>{
            return app.isFullySigned && app.appStatus === 'APPLYING'? true: false
          }
        )? _c.CHECK_CODE.INVALID_BUNDLE: _c.CHECK_CODE.VALID;
        resolve({code})
      }
      //exist fna report, prompt warning
      else if(status === _c.BUNDLE_STATUS.SIGN_FNA){
        resolve({code: _c.CHECK_CODE.INVALID_FNA_REPORT});
      }
      else if(status > _c.BUNDLE_STATUS.HAVE_FNA && status < _c.BUNDLE_STATUS.SIGN_FNA){
        if(itemId == nDao.ITEM_ID.PDA){
          _onSavePDA(cid, data, status, extra.tiInfo, resolve);
        }
        else if(itemId == nDao.ITEM_ID.FE){
          _onSaveFE(cid, data, resolve);
        }
        else if(itemId == nDao.ITEM_ID.NA){
          _onSaveFNA(cid, data, resolve);
        }
      }
      else {
        resolve({code: 107})
      }
    }).catch((error)=>{
      logger.error("Error in _onSaveNeedItem->getCurrentBundle: ", error);
    });
  })
}
module.exports.onSaveNeedItem = _onSaveNeedItem;


var _invalidNeedItem = function(cid, agent, data, itemId, errObj={}, params={}){
  return new Promise((resolve)=>{
    _c.getCurrentBundle(cid).then((bundle)=>{
      let {agentCode: aid} = agent;
      let status = bundle.status;
      if(status >= _c.BUNDLE_STATUS.FULL_SIGN){
        _c.createNewBundle(cid, agent).then(()=>{
          _c.getCurrentBundle(cid).then((newBundle)=>{
            resolve({[itemId]: _.get(newBundle, `fna.${itemId}`)});
          }).catch((error)=>{
            logger.error("Error in _invalidNeedItem->getCurrentBundle[2]: ", error);
          });
        }).catch((error)=>{
          logger.error("Error in _invalidNeedItem->createNewBundle: ", error);
        });
      }
      else if(status > _c.BUNDLE_STATUS.HAVE_FNA && status <= _c.BUNDLE_STATUS.SIGN_FNA){
        let rollbackApplication = () => {
          return new Promise(rollbackResolve => {
            if (status === _c.BUNDLE_STATUS.START_GEN_PDF || status === _c.BUNDLE_STATUS.SIGN_FNA) {
              _a.rollbackApplication2Step1(cid).then(()=>{
                _c.getCurrentBundle(cid).then((newBundle)=>{
                  bundle = newBundle;
                  rollbackResolve();
                });
              });
            } else {
             rollbackResolve();
            }
          });
        }
        rollbackApplication().then(()=>{
          if(itemId === nDao.ITEM_ID.NA){
            //check prodType
            nDao.getItem(cid, nDao.ITEM_ID.NA).then((oFna)=>{
              nDao.getItem(cid, nDao.ITEM_ID.PDA).then((oPda)=>{
                let oProdType = _.get(oFna, 'productType.lastProdType');
                let oAspect = _.split(_.get(oFna, 'aspects'), ",");
                let aspect = _.split(_.get(data, 'aspects'), ",");
                let raIds = ["riskPotentialReturn", "avgAGReturn", "smDroped", "alofLosses", "expInvTime", "invPref", "selfSelectedriskLevel"];
                //return true if aspect removed.
                let filterAspect = _.filter(oAspect, _a=>_.lastIndexOf(aspect, _a)===-1);
                //if aspect removed, remove all quotation
                if(filterAspect.length){
                  logger.log("INFO: Remove all BI", cid);
                  _a.invalidateApplicationByClientId(cid).then(resolve).catch((error)=>{
                    logger.error("Error in _invalidNeedItem->invalidateApplicationByClientId: ", error);
                  });
                }else{
                  let isLastStep = _.get(data, "lastStepIndex") >= 4;
                  //if change RA, invalidate all ILP BIs
                  let prodTypes = _.split(_.get(data, "productType.prodType"), ",");
                  let oProdTypes = _.split(oProdType, ",");
                  let hasILP = prodTypes.indexOf("investLinkedPlans") > -1;
                  let oOCkaPass = _.get(oFna, "ckaSection.owner.passCka");
                  let oCkaPass = _.get(data, "ckaSection.owner.passCka");
                  let hasJoint = _.get(oPda, "applicant") === "joint";
                  let rLv = _.get(data, "raSection.owner.selfSelectedriskLevel");
                  let oRLv = _.get(oFna, "raSection.owner.selfSelectedriskLevel");
                  let aLv = _.get(data, "raSection.owner.assessedRL");
                  let oALv = _.get(oFna, "raSection.owner.assessedRL");
                  let ckaPass = oCkaPass === "Y";
                  let ckaPassChange = oOCkaPass === "Y" && rLv && oOCkaPass !== oCkaPass;
                  let ckaFailChange = rLv && oOCkaPass === "N" && oOCkaPass !== oCkaPass;


                  //invalid if BI if risk accessment fulfill the condition
                  let _invalidByRa = function(){
                    return new Promise(resolve2=>{
                      if(hasILP && (ckaPassChange || ckaFailChange || rLv !== oRLv || ((!rLv || !ckaPass) && aLv !== oALv))){
                        _a.onInvalidateApplicationByProductLine(cid, "IL").then(resolve2).catch((error)=>{
                          logger.error("Error in _invalidNeedItem->onInvalidateApplicationByCovCode: ", error);
                        });;
                      }
                      else {
                        resolve2();
                      }
                    });
                  }

                  //invalid if BI if product type fulfill the condition
                  let _invalidByProdType = function(){
                    return new Promise(resolve2=>{
                      if(isLastStep){
                        _q.invalidateQuotationsByNA(cid, data).then(resolve2).catch((error)=>{
                          logger.error("Error in _invalidNeedItem->invalidateQuotationsByNA: ", error);
                        });;
                      }
                      else {
                        resolve2();
                      }
                    });
                  }

                  _invalidByRa()
                    .then(_invalidByProdType)
                    .then(resolve)
                    .catch((error)=>{
                      logger.error("Error in _invalidNeedItem: ", error);
                    });
                };
              });
            }).catch((error)=>{
              logger.error("Error in _invalidNeedItem->nDao.getItem[NA]: ", error);
            });
          } else if(itemId === nDao.ITEM_ID.FE){
            //invalidate budget
            nDao.getItem(cid, nDao.ITEM_ID.FE).then((oFe)=>{
              let owner = _.get(data, "owner") || {}, oOwner = _.get(oFe, "owner") || {};
              let {aRegPremBudget, singPrem, cpfOaBudget, cpfSaBudget, cpfMsBudget, srsBudget} = owner;
              let {aRegPremBudget: oARegPremBudget, singPrem: oSingPrem, cpfOaBudget: oCpfOaBudget,
                cpfSaBudget: oCpfSaBudget, cpfMsBudget: oCpfMsBudget, srsBudget: oSrsBudget} = oOwner;

              if (_.get(data, 'revisit')) {
                resolve();
              } else {
                _a.resetApplicationResidency(cid, data).then(()=>{
                  _q.invalidateQuotationsByFE(cid, data).then(() => {
                    if(aRegPremBudget != oARegPremBudget || singPrem != oSingPrem || cpfOaBudget != oCpfOaBudget ||
                      cpfSaBudget != oCpfSaBudget || cpfMsBudget != oCpfMsBudget || srsBudget != oSrsBudget){
                        clientChoiceHandler.resetClientChoiceBudget(cid).then(resolve);
                    }
                    else {
                      resolve();
                    }
                  }).catch((error)=>{
                    logger.error("Error in _invalidNeedItem->invalidateQuotationsByFE: ", error);
                  });
                }).catch((error)=>{
                  logger.error("Error in _invalidNeedItem->resetApplicationResidency: ", error);
                });
              }
            }).catch((error)=>{
              logger.error("Error in _invalidNeedItem->nDao.getItem[FE]: ", error);
            });
          } else if(itemId === nDao.ITEM_ID.PDA){
            //find relationship
            let promises = [], dependants = _.split(data.dependants, ",");
            cDao.getProfile(cid, true).then((profile)=>{
              if(status >= _c.BUNDLE_STATUS.HAS_PRE_EAPP && errObj.initTi){
                promises.push(_c.rollbackStatus(cid, _c.BUNDLE_STATUS.HAS_PRE_EAPP));
              }
              //add spouse ID to relationship if applicant = joint
              if(data.applicant === "joint"){
                let spouseId = _.get(_.find(profile.dependants, (d)=>{return d.relationship === "SPO"}), "cid");
                if(spouseId){dependants.push(spouseId);}
              }
              _.forEach(bundle.applications, (application)=>{
                promises.push(new Promise((resolve2)=>{
                  dao.getDoc(application.quotationDocId, (quotation)=>{
                    //remove dependents in applicants
                    let performInvalidate = false;
                    if (_.get(quotation, 'quotType') === 'SHIELD') {
                      let laIsNotDependants = false;
                      _.forEach(quotation.insureds, (la, iCid) => {
                        if (dependants.indexOf(iCid) === -1) {
                          laIsNotDependants = true;
                          return false;
                        }
                      });

                      performInvalidate = _.indexOf(_.keys(quotation.insureds), cid) < 0 && laIsNotDependants;
                    } else {
                      performInvalidate = quotation.iCid && quotation.iCid !== cid && dependants.indexOf(quotation.iCid) === -1;
                    }

                    if (performInvalidate){
                      _a.invalidateApplication(bundle, application.quotationDocId);
                    }
                    resolve2();
                  })
                }))
              })

              Promise.all(promises).then((args)=>{
                logger.log(`INFO: BundleUpdate - [cid:${cid}; bundleId:${_.get(bundle, 'id')}; fn:_invalidNeedItem]`);
                _c.updateBundle(bundle, resolve);
              }).catch((error)=>{
                logger.error("Error in _invalidNeedItem->Promise.all: ", error);
              });
            })
          }
          else {
            resolve();
          }
        });
      }
      else {
        resolve();
      }
    }).catch((error)=>{
      logger.error("Error in _invalidNeedItem->getCurrentBundle: ", error);
    });
  })
}
module.exports.invalidNeedItem = _invalidNeedItem;

var _onSavePDA = function(cid, pda, status, tiInfo={}, resolve){
  nDao.getItem(cid, nDao.ITEM_ID.PDA).then((oPda)=>{
    var exec = function(){
      if(oPda.dependants != pda.dependants || (pda.applicant == 'single' &&　pda.applicant != oPda.applicant)){
        //resolve({code: 107})
        resolve({code: _c.CHECK_CODE.VALID});
      }
      else {
        resolve({code: _c.CHECK_CODE.VALID});
      }
    }
    if(status >= _c.BUNDLE_STATUS.HAS_PRE_EAPP){
      _cl.onSaveTrustedIndividual(cid, tiInfo, pda).then((resp)=>{
        if(resp.code != _c.CHECK_CODE.VALID){
          resolve({
            //code: 107,
            code: _c.CHECK_CODE.VALID,
            errObj: {initTi: true}
          });
        }
        else {
          exec();
        }
      }).catch((error)=>{
        logger.error("Error in _onSavePDA->onSaveTrustedIndividual: ", error);
      });
    }
    else {
      exec();
    }
  }).catch((error)=>{
      logger.error("Error in _onSavePDA->nDao.getItem[PDA]: ", error);
  });
}
module.exports.onSavePDA = _onSavePDA;

var _onSaveFE = function(cid, fe, resolve){
  nDao.getItem(cid, nDao.ITEM_ID.FE).then((oFe)=>{
    let feValid = true;
    _.forEach(['singPrem', 'aRegPremBudget', 'cpfSaBudget', 'cpfOaBudget', 'srsBudget'], (itemId)=>{
      if(_.get(fe.owner, itemId) != _.get(oFe.owner, itemId)){
        feValid = false;
      }
    })
    if(!feValid){
      //resolve({code: 107})
      resolve({code: _c.CHECK_CODE.VALID});
    }
    else {
      resolve({code: _c.CHECK_CODE.VALID});
    }
  }).catch((error)=>{
      logger.error("Error in initClientChoice->nDao.getItem: ", error);
  });
}
module.exports.onSaveFE = _onSaveFE;

var _onSaveFNA = function(cid, fna, resolve){
  nDao.getItem(cid, nDao.ITEM_ID.NA).then((oFna)=>{
    let oProdType = _.get(oFna, 'productType.lastProdType');
    let oAspect = _.split(_.get(oFna, 'aspects'), ",");
    let aspect = _.split(_.get(fna, 'aspects'), ",");
    //return true if aspect removed.
    let filterAspect = _.filter(oAspect, _a=>_.lastIndexOf(aspect, _a)===-1);
    if(filterAspect.length || (fna.lastStepIndex === 4 && _.get(fna, 'productType.prodType') != oProdType)){
      //resolve({code: 107})
      resolve({code: _c.CHECK_CODE.VALID});
    }
    else {
      resolve({code: _c.CHECK_CODE.VALID});
    }
  }).catch((error)=>{
      logger.error("Error in _onSaveFNA->nDao.getItem[PDA]: ", error);
  });
}
module.exports.onSaveFNA = _onSaveFNA;

var _invalidFna = function(cid, fnaParam={}){
  var promises = [];
  promises.push(nDao.invalidSection(cid, nDao.ITEM_ID.PDA, fnaParam));
  promises.push(nDao.invalidSection(cid, nDao.ITEM_ID.FE, fnaParam));
  promises.push(nDao.invalidSection(cid, nDao.ITEM_ID.NA, fnaParam));
  return Promise.all(promises);
}

module.exports.invalidFna = _invalidFna;
