module.exports = {
    applicationsByDate: function (doc, meta) {
        if (doc.type === 'application') {
            var baseProductCode = doc.quotation && doc.quotation.baseProductCode;
            var createDate = doc.quotation && doc.quotation.createDate;
            if (createDate) {
                createDate = Date.parse(createDate);
            }
            emit(['01', baseProductCode, createDate], doc);
        }
    },
    applictionsBySignedDate: function (doc, meta) {
        if (doc.type === 'application') {
            var baseProductCode = doc.quotation && doc.quotation.baseProductCode;
            var bisignedDate = doc.biSignedDate;
            var applicationSignedDate = doc.applicationSignedDate;
            if (bisignedDate) {
                bisignedDate = Date.parse(bisignedDate);
                emit(['01', bisignedDate, baseProductCode], doc);
            }
            if (applicationSignedDate) {
                applicationSignedDate = Date.parse(applicationSignedDate);
                emit(['01', applicationSignedDate, baseProductCode], doc);
            }
        }
    }
};
