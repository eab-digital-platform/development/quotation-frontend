module.exports = {
    validBundleInClient: function (doc) {
        if (doc && doc.type === 'cust' && doc.bundle) {
            var validBundleId = '';
            for (var i in doc.bundle) {
                var bundle = doc.bundle[i];
                if (bundle.isValid) {
                    validBundleId = bundle.id;
                }
            }

            var emitObject = {
              id: doc.cid,
              validBundleId: validBundleId
            };
            // eslint-disable-next-line no-undef
            emit(['01', doc.agentId], emitObject);
          }
    },

    quotationsByBundleId: function(doc) {
        if (doc && doc.type === 'quotation') {
            var emitObj = {
                id: doc.id,
                bundleId: doc.bundleId,
                baseProductCode: doc.baseProductCode,
                clientId: doc.pCid
            };
            // eslint-disable-next-line no-undef
            emit(['01', doc.bundleId], emitObj);
        }
    },

    quotationsByBaseProductCode: function(doc) {
        if (doc && doc.type === 'quotation') {
            var emitObj = {
                bundleId: doc.bundleId,
                clientId: doc.pCid
            };
            // eslint-disable-next-line no-undef
            emit(['01', doc.baseProductCode], emitObj);
        }
    },

    quotationsByMutipleBaseProductCode: function(doc) {
        if (doc && doc.type === 'quotation') {
            var emitObj = {
                bundleId: doc.bundleId,
                clientId: doc.pCid
            };
            if (doc.baseProductCode === 'ASIM' || doc.baseProductCode === 'NPE') {
              // eslint-disable-next-line no-undef
              emit(['01', doc.baseProductCode], emitObj);
            }
        }
    },

    quotationsByNHAFFund: function(doc) {
        if (doc && doc.type === 'quotation') {
            var quotationCids = [];
            if (doc.pCid) {
                quotationCids.push(doc.pCid);
            }

            if (doc.iCid && doc.iCid !== doc.pCid) {
                quotationCids.push(doc.iCid);
            }
            var emitObj = {
                cids: quotationCids
            };
            var needEmit = false;
            if (doc.fund && doc.fund.funds && doc.fund.funds.length > 0) {
                for (var i = 0; i < doc.fund.funds.length; i++) {
                    var fundObj = doc.fund.funds[i];
                    if (fundObj && fundObj.fundCode === 'NHAF') {
                        needEmit = true;
                        break;
                    }
                  }
            }
            if (needEmit) {
              // eslint-disable-next-line no-undef
              emit(['01', doc.baseProductCode], emitObj);
            }
        }
    },

    release19InflightHandling: function(doc) {
        if (doc && doc.type === 'quotation') {
            var emitObj = {
                bundleId: doc.bundleId,
                clientId: doc.pCid
            };
            var needEmit = false;
            if (doc.fund && doc.fund.funds && doc.fund.funds.length > 0) {
                for (var i = 0; i < doc.fund.funds.length; i++) {
                    var fundObj = doc.fund.funds[i];
                    if (fundObj && (fundObj.fundCode === 'TGBO' || fundObj.fundCode === 'TGTR')) {
                        needEmit = true;
                        break;
                    }
                }
            }

            if (doc.plans) {
                for (var j = 0; j < doc.plans.length; j++) {
                    var planObj = doc.plans[j];
                    if (planObj && planObj.campaignIds && planObj.campaignIds.indexOf('campaign_TERM18_1') > -1) {
                        needEmit = true;
                    }
                }
            }

            if (needEmit) {
              // eslint-disable-next-line no-undef
                emit(['01'], emitObj);
            }
        }
    },

    qwer: function (doc) {
        if (doc && doc.type === 'quotation') {
          var emitObj = {
            bundleId: doc.bundleId,
            clientId: doc.pCid
          };
          if (doc.baseProductCode === 'RHP') {
            // eslint-disable-next-line no-undef
            emit(['01'], emitObj);
          }
        }
      },

    release20InflightHandling: function(doc) {
        if (doc && doc.type === 'quotation') {
            var emitObj = {
              bundleId: doc.bundleId,
              clientId: doc.pCid
            };
            if (doc.baseProductCode === 'RHP') {
              // eslint-disable-next-line no-undef
              emit(['01'], emitObj);
            }
        }
    },

    release21InflightHandling: function(doc) {
      if (doc && doc.type === 'quotation') {
        var emitObj = {
          bundleId: doc.bundleId,
          clientId: doc.pCid
        };
        var needEmit = false;
        if (doc.fund && doc.fund.funds && doc.fund.funds.length > 0) {
          for (var i = 0; i < doc.fund.funds.length; i++) {
            var fundObj = doc.fund.funds[i];
            if (fundObj && (fundObj.fundCode === 'HGTE' || fundObj.fundCode === 'HGPE' || fundObj.fundCode === 'HAPP')) {
              needEmit = true;
              break;
            }
          }
        }

        if (doc.baseProductCode === 'ESC') {
          needEmit = true;
        }

        if (needEmit) {
          // eslint-disable-next-line no-undef
          emit(['01'], emitObj);
        }
      }
    },

    release21InflightHandlingGetAll: function(doc) {
      if (doc && doc.type === 'quotation') {
        var emitObj = {
          bundleId: doc.bundleId,
          clientId: doc.pCid
        };
        // eslint-disable-next-line no-undef
        emit(['01'], emitObj);
      }
    },

    release22InflightHandling: function(doc) {
      if (doc && doc.type === 'quotation') {
        var emitObj = {
          bundleId: doc.bundleId,
          clientId: doc.pCid
        };
        var needEmit = false;
        if ((doc.baseProductCode === 'PUL' || doc.baseProductCode === 'AWTR' || doc.baseProductCode === 'AWICA') && doc.fund && doc.fund.funds && doc.fund.funds.length > 0) {
          for (var i = 0; i < doc.fund.funds.length; i++) {
            var fundObj = doc.fund.funds[i];
            if (fundObj && (fundObj.fundCode === 'LSEA' || fundObj.fundCode === 'GSIB' || fundObj.fundCode === 'FUSD' || fundObj.fundCode === 'FDSB' )) {
              needEmit = true;
              break;
            }
          }
        }
        if (needEmit) {
          // eslint-disable-next-line no-undef
          emit(['01'], emitObj);
        }
      }
    },

    release23CR247InflightHandling: function(doc) {
      if (doc && doc.type === 'quotation') {
        var emitObj = {
            bundleId: doc.bundleId,
            clientId: doc.pCid
        };
        if (doc.baseProductCode === 'ASIM') {
          // eslint-disable-next-line no-undef
          emit(['01'], emitObj);
        }
      }
    },

    release23InflightHandling: function(doc) {
      if (doc && doc.type === 'quotation') {
        var emitObj = {
          bundleId: doc.bundleId,
          clientId: doc.pCid
        };
        if (doc.baseProductCode === 'FPX' || doc.baseProductCode === 'FSX') {
          // eslint-disable-next-line no-undef
          emit(['01'], emitObj);
        }
      }
    },

    release24RH2InflightHandling: function(doc) {
      if (doc && doc.type === 'quotation') {
        var emitObj = {
          bundleId: doc.bundleId,
          clientId: doc.pCid
        };
        if (doc.baseProductCode === 'RHP') {
          // eslint-disable-next-line no-undef
          emit(['01'], emitObj);
        }
      }
    },

    FXAndRHPInflightHandling: function(doc) {
      if (doc && doc.type === 'quotation') {
        var emitObj = {
          bundleId: doc.bundleId,
          clientId: doc.pCid
        };
        if (doc.baseProductCode === 'FPX' || doc.baseProductCode === 'FSX' || doc.baseProductCode === 'RHP') {
          // eslint-disable-next-line no-undef
          emit(['01'], emitObj);
        }
      }
    },

    RHP_QQ_20191010: function(doc) {
      if (doc && doc.type === 'quotation') {
        var longUTCreateDateTime = Date.parse(doc.createDate);
        var emitObj = {
          bundleId: doc.bundleId,
          clientId: doc.pCid,
          quotationId: doc.id,
          agentCode: doc.agentCode,
          agentName: doc.agent.name,
          dealerGroup: doc.dealerGroup,
          createDate: doc.createDate,
          riskCommenDate: doc.riskCommenDate,
          quickQuote: doc.quickQuote,
          iCid: doc.iCid,
          iFullName: doc.iFullName,
          iDob: doc.iDob,
          iGender: doc.iGender,
          pCid: doc.pCid,
          pFullName: doc.pFullName,
          pDob: doc.pDob,
          pGender: doc.pGender,
          sameAs: doc.sameAs,
          paymentMode: doc.paymentMode,
          plans: doc.plans,
          policyOptionsDesc: doc.policyOptionsDesc,
          sumInsured: doc.sumInsured,
          premium: doc.premium,
          annualPremium: doc.annualPremium
        };
        if (doc.baseProductCode === 'RHP' && doc.quickQuote === true) {
          // eslint-disable-next-line no-undef
          emit(['01', 'CreateTime', longUTCreateDateTime], emitObj);
        }
      }
    },

    release24CR265InflightHandling: function(doc) {
      if (doc && doc.type === 'quotation') {
        var emitObj = {
          bundleId: doc.bundleId,
          clientId: doc.pCid
        };
        var needEmit = false;
        if (doc.fund && doc.fund.funds && doc.fund.funds.length > 0) {
          for (var i = 0; i < doc.fund.funds.length; i++) {
            var fundObj = doc.fund.funds[i];
            if (fundObj && fundObj.fundCode === 'SAPP') {
              needEmit = true;
              break;
            }
          }
        }
        if (needEmit) {
          // eslint-disable-next-line no-undef
          emit(['01'], emitObj);
        }
      }
    }
};
