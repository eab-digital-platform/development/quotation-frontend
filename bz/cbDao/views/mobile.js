module.exports = {
    mobileInvalidatedPaidToRLS: function (doc, meta) {
        if (doc.type === 'application' && doc.isInvalidatedPaidToRLS === false){
          var lastUpdateDate = new Date(doc.biSignedDate);
          // eslint-disable-next-line no-undef
          emit(['01', doc.id], {
            appId: doc.id || doc._id,
            polNo: doc.policyNumber || '',
            bundleId: doc.bundleId,
            signDate: lastUpdateDate.getTime(),
            payMethod: doc.payment && doc.payment.trxMethod,
            payStatus: doc.payment && doc.payment.trxStatus,
            isFullySigned: doc.isFullySigned,
            isInitialPaymentCompleted: doc.isInitialPaymentCompleted,
            docType: doc.type,
            isShield: doc.quotation && doc.quotation.quotType === 'SHIELD',
            parentId: doc.parentId
          });
        }
      },
};
