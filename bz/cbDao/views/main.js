module.exports = {
    // this is temp view for get agent who has authorised 'M8'/'M8A'/'IFAST'
  getUTAgents: function (doc, meta) {
    if (doc && doc.type === 'agent') {
      if (
        doc.authorised.indexOf('M8') > - 1 ||
        doc.authorised.indexOf('M8A') > -1 ||
        doc.authorised.indexOf('IFAST') > -1
       ) {
        if (doc.rawData) {
          // eslint-disable-next-line no-undef
          emit(['01', 'userId', doc.rawData.userId], null);
        }
        // eslint-disable-next-line no-undef
        emit(['01', 'agentCode', doc.agentCode], null);
      }
    }
  },

  getSignedFnaReport: function (doc, meta) {
    if (doc.type == 'bundle' && doc.fnaSignDate) {
      var signDate = new Date(doc.fnaSignDate);
      signDate = signDate.split('/');
      signDate = new Date(signDate[1] + '/' + signDate[0] + '/' + signDate[2]);
      // eslint-disable-next-line no-undef
      emit(
        [
          '01',
          doc.agentCode,
          signDate.getFullYear(),
          signDate.getMonth() + 1,
          signDate.getDate()
        ], null);
    }
  },

  contacts: function (doc, meta) {
    if (doc.type == 'cust') {
      var applicationCount = doc.applicationCount || 0;
      var emitObject = {
        'id': doc.cid,
        'idCardNo': doc.idCardNo,
        'idDocType': doc.idDocType,
        'idDocTypeOther': doc.idDocTypeOther,
        'firstName': doc.firstName,
        'lastName': doc.lastName,
        'fullName': doc.fullName,
        'nameOrder': doc.nameOrder,
        'mobileNo': doc.mobileNo,
        'email': doc.email,
        'photo': doc.photo,
        'applicationCount': applicationCount
      };
      // eslint-disable-next-line no-undef
      emit(['01', doc.agentId], emitObject);
    }
  },

  products: function (doc, meta) {
    if (doc.type == 'product') {
      var effDate = doc.effDate || 100000;
      var expDate = doc.expDate || 9999999900000;
      var prodFeature = doc.prodFeature || '';
      var keyRisk = doc.keyRisk || '';
      var insuredAgeDesc = doc.insuredAgeDesc || '';
      var payModeDesc = doc.payModeDesc  || '';
      var polTermDesc = doc.polTermDesc  || '';
      var premTermDesc = doc.premTermDesc  || '';
      var illustrationInd = doc.illustrationInd  || '';
      var scrOrderSeq = doc.scrOrderSeq  || 0;
      var tnc = doc.tnc  || '';
      // eslint-disable-next-line no-undef
      emit(['01', doc.planInd, doc.covCode], {
          compCode: doc.compCode,
          covCode: doc.covCode,
          covName: doc.covName,
          version: doc.version,
          planCode: doc.planCode,
          productLine: doc.productLine,
          productCategory: doc.productCategory,
          smokeInd: doc.smokeInd,
          genderInd: doc.genderInd,
          ctyGroup: doc.ctyGroup,
          entryAge: doc.entryAge,
          currencies: doc.currencies,
          quotForm: doc.quotForm || '',
          effDate: effDate,
          expDate: expDate,
          prodFeature: prodFeature,
          keyRisk: keyRisk,
          insuredAgeDesc: insuredAgeDesc,
          payModeDesc: payModeDesc,
          polTermDesc: polTermDesc,
          premTermDesc: premTermDesc,
          illustrationInd: illustrationInd,
          scrOrderSeq: scrOrderSeq,
          tnc: tnc
      });
    }
  },

  quickQuotes: function (doc) {
    if (doc && doc.type === 'quotation' && doc.quickQuote) {
      var iFullName = doc.iFullName;
      if (doc.quotType === 'SHIELD' && doc.insureds) {
        var iNames = [];
        for (var i in doc.insureds) {
          var insured = doc.insureds[i];
          iNames.push(insured.iFullName);
        }
        iFullName = iNames.join(', ');
      }
      // eslint-disable-next-line no-undef
      emit(['01', doc.pCid], {
        id: doc.id,
        type: doc.type,
        covName: doc.baseProductName || (doc.plans && doc.plans[0] && doc.plans[0].covName),
        pCid: doc.pCid,
        pFullName: doc.pFullName,
        iFullName: iFullName,
        lastUpdateDate: doc.lastUpdateDate,
        createDate: doc.createDate
      });
    }
  },

  bundleApplications: function (doc) {
    if (doc && doc.type === 'bundle' && doc.applications) {
      var bundle = doc;
      for (var i = 0; i < doc.applications.length; i++) {
        var app = doc.applications[i];
        if (app.applicationDocId) {
          // eslint-disable-next-line no-undef
          emit(['01', 'application', app.appStatus, app.applicationDocId], {
            bundleId: bundle.id,
            bundleStatus: bundle.status,
            pCid: bundle.pCid,
            bundleIsValid: bundle.isValid,
            quotationDocId: app.quotationDocId,
            applicationDocId: app.applicationDocId,
            appStatus: app.appStatus
          });
        } else {
          // eslint-disable-next-line no-undef
          emit(['01', 'quotation', null, app.quotationDocId], {
            bundleId: bundle.id,
            bundleStatus: bundle.status,
            pCid: bundle.pCid,
            bundleIsValid: bundle.isValid,
            quotationDocId: app.quotationDocId,
            applicationDocId: null,
            appStatus: null
          });
        }
      }
    }
  },

  summaryQuots: function (doc, meta) {
    if (doc && doc.type === 'quotation' && !doc.quickQuote) {
      var iCids;
      var totPremium;
      if (doc.quotType && doc.quotType === 'SHIELD') {
        totPremium = doc.totPremium;
      } else {
        totPremium = doc.premium;
      }

      if (doc.insureds) {
        iCids = Object.keys(doc.insureds);
      } else {
        iCids = [];
      }
      // eslint-disable-next-line no-undef
      emit(['01', doc.pCid, doc.id], {
        id: doc.id,
        type: doc.type,
        isValid: doc.isValid,
        bundleId: doc.bundleId,
        baseProductCode: doc.baseProductCode,
        baseProductName: doc.baseProductName,
        iName: doc.iFullName,
        pName: doc.pFullName,
        ccy: doc.ccy,
        totCpfPortion: doc.totCpfPortion,
        totCashPortion: doc.totCashPortion,
        totMedisave: doc.totMedisave,
        totPremium: totPremium,
        paymentMode: doc.paymentMode || '',
        lastUpdateDate: doc.lastUpdateDate,
        statusChangeDate: doc.proposedDate || doc.lastUpdateDate,
        productLine: doc.productLine,
        plans: doc.plans,
        iCid: doc.iCid,
        pCid: doc.pCid,
        fnaAns: doc.fnaAns || '',
        clientChoice: doc.clientChoice,
        policyOptions: doc.policyOptions,
        policyOptionsDesc: doc.policyOptionsDesc,
        quotType: doc.quotType || '',
        insureds: doc.insureds || {},
        iCids: iCids
      });
    }
  },

  summaryApps: function (doc, meta) {
    if (doc.type === 'application' || doc.type === 'masterApplication') {
      var quot = doc.quotation;
      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;
      // eslint-disable-next-line no-undef
      emit(['01', doc.pCid, doc.id], {
        id: doc.id,
        policyNumber:doc.policyNumber,
        type: doc.type,
        isValid: doc.isValid,
        bundleId: doc.bundleId,
        quotationDocId: doc.quotationDocId,
        baseProductCode: quot.baseProductCode,
        baseProductName: quot.baseProductName,
        iName: quot.iFullName,
        pName: quot.pFullName,
        ccy: quot.ccy,
        totPremium: quot.premium,
        paymentMode: quot.paymentMode,
        productLine: quot.productLine,
        quotType: quot.quotType || '',
        lastUpdateDate: lastUpdateDate,
        plans: quot.plans,
        productQuotForm: quot.productQuotForm,
        isSubQuotation: !!quot.isSubQuotation,
        isInitialPaymentCompleted: doc.isInitialPaymentCompleted,
        isAgentReportSigned: doc.isAgentReportSigned,
        iCid: doc.iCid,
        pCid: doc.pCid,
        isStartSignature: doc.isStartSignature,
        isFullySigned: doc.isFullySigned,
        quotation: doc.quotation,
        isMandDocsAllUploaded: doc.isMandDocsAllUploaded || false,
        iCids: doc.iCids || [],
        iCidMapping: doc.iCidMapping || {}
      });
    }
  },

  singpostApps: function (doc, meta) {
    if (doc.type === 'application' && doc.quotation && doc.applicationForm) {
      var quot = doc.quotation;
      var appForm = doc.applicationForm;
      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;

      if (quot.agent && appForm.values){

          var values = appForm.values;
          var agent = quot.agent;

          if (values.proposer){
            var proposer = values.proposer;

            var planDetails = values.planDetails ? values.planDetails:null;

            if (agent.dealerGroup === 'SINGPOST' && proposer.personalInfo){

              var personalInfo = proposer.personalInfo;

                if (personalInfo.branchInfo){

                var branchInfo = personalInfo.branchInfo;
                // eslint-disable-next-line no-undef
                emit(['01', doc.id], {
                  id: doc.id,
                  policyNumber: doc.policyNumber,
                  pId: personalInfo.idCardNo,
                  pName: personalInfo.fullName,
                  aName: agent.name,
                  aCode: agent.agentCode,
                  bankRefId : branchInfo.bankRefId,
                  lastUpdateDate: lastUpdateDate,
                  quotation: quot,
                  planDetails: planDetails,
                  productCode: quot.baseProductCode
                });
              }
          }
      }
    }
    }
  },

  submissionRptApps: function (doc, meta) {
    if (doc.type === 'application' && doc.quotation && doc.applicationForm) {

      var quot = doc.quotation;
      var appForm = doc.applicationForm;
      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;

      if (quot.agent && appForm.values) {

        var values = appForm.values;
        var agent = quot.agent;

        if (values.proposer) {
          var proposer = values.proposer;

          var planDetails = values.planDetails ? values.planDetails : null;
          var personalInfo = proposer.personalInfo;

          if (agent.dealerGroup === 'SINGPOST' && personalInfo) {
            if (personalInfo.branchInfo) {
              var branchInfo = personalInfo.branchInfo;
              // eslint-disable-next-line no-undef
              emit(['01', 'SINGPOST', doc.id], {
                id: doc.id,
                policyNumber: doc.policyNumber,
                pId: personalInfo.idCardNo,
                pName: personalInfo.fullName,
                aName: agent.name,
                aCode: agent.agentCode,
                bankRefId: branchInfo.bankRefId,
                lastUpdateDate: lastUpdateDate,
                quotation: quot,
                planDetails: planDetails,
                productCode: quot.baseProductCode
              });
            }
          }

          if (agent.dealerGroup === 'DIRECT' && personalInfo) {
            // eslint-disable-next-line no-undef
            emit(['01', 'DIRECT', doc.id], {
              id: doc.id,
              policyNumber: doc.policyNumber,
              pId: personalInfo.idCardNo,
              pName: personalInfo.fullName,
              aName: agent.name,
              aCode: agent.agentCode,
              lastUpdateDate: lastUpdateDate,
              quotation: quot,
              planDetails: planDetails,
              productCode: quot.baseProductCode
            });
          }
        }
      }
    }
  },

  submissionRptPendingDetails: function (doc, meta) {
    if (doc && doc.type === 'approval') {

      var longUTCLastEditDate = Date.parse(doc.lastEditedDate);
      var filteredApprovalStatus = ['SUBMITTED', 'PDoc', 'PDis'].indexOf(doc.approvalStatus) >= 0;

      var emitObj = {
        compCode: doc.compCode,
        displayCaseNo:  doc.policyId,
        caseNo: doc.policyId,
        product: doc.productName,
        dealerGroup: doc.dealerGroup,
        agentId: doc.agentId,
        agentName: doc.agentName,
        managerName: doc.managerName,
        managerId: doc.managerId,
        directorId: doc.directorId,
        directorName: doc.directorName,
        approveManagerId: doc.approveRejectManagerId,
        approveManagerName: doc.approveRejectManagerName,
        approveRejectManagerId: doc.approveRejectManagerId,
        approveRejectManagerName: doc.approveRejectManagerName,
        submittedDate: doc.submittedDate,
        approvalStatus: doc.approvalStatus,
        onHoldReason: doc.onHoldReason,
        approvalCaseId: doc.approvalCaseId,
        applicationId: doc.applicationId,
        quotationId:  doc.quotationId,
        customerId: doc.customerId,
        customerName: doc.customerName,
        lastEditedBy: doc.lastEditedBy,
        lastEditedDate: doc.lastEditedDate,
        approveRejectDate: doc.approveRejectDate,
        caseLockedManagerCodebyStatus: doc.caseLockedManagerCodebyStatus,
        customerICNo: doc.customerICNo,
        agentProfileId: doc.agentProfileId,
        expiredDate: doc.expiredDate,
        masterApprovalId: doc.masterApprovalId || '',
        isShield: doc.isShield,
        proposalNumber: doc.proposalNumber || doc.policyId
      };
      if (doc.dealerGroup === 'SINGPOST' && filteredApprovalStatus) {
        // eslint-disable-next-line no-undef
        emit(['01', 'SINGPOST', longUTCLastEditDate], emitObj);
      }
      if (doc.dealerGroup === 'DIRECT' && filteredApprovalStatus) {
        // eslint-disable-next-line no-undef
        emit(['01', 'DIRECT', longUTCLastEditDate], emitObj);
      }
    }
  },


  pdfTemplates: function (doc, meta) {
    if (doc && doc.type === 'pdfTemplate') {
      // eslint-disable-next-line no-undef
      emit(['01', 'QUOT', doc.pdfCode], {
        id: doc.id,
        compCode: doc.compCode,
        pdfCode: doc.pdfCode,
        effDate: doc.effDate || 100000,
        expDate: doc.expDate || 9999999900000,
        version: doc.version
      });
    }
  },

  funds: function (doc) {
    if (doc && doc.type === 'fund') {
      // eslint-disable-next-line no-undef
      emit(['01', doc.fundCode], {
        compCode: doc.compCode,
        fundCode: doc.fundCode,
        fundName: doc.fundName,
        ccy: doc.ccy,
        isMixedAsset: doc.isMixedAsset,
        assetClass: doc.assetClass,
        riskRating: doc.riskRating,
        paymentMethod: doc.paymentMethod,
        version: doc.version
      });
    }
  },

  downloadMaterial: function(doc){
    if (doc && doc.type === 'material'){
      // eslint-disable-next-line no-undef
      emit(['01',doc.id],{
        section  : doc.section,
        sectionId: doc.sectionId,
        name     : doc.name,
        id       : doc.id,
        effDate  : doc.effDate,
        expDate  : doc.expDate
      });
    }
  },

  approvalCases: function (doc) {
    if (doc && doc.type === 'approval') {
      var dtSupervisorApproveRejectDate = null;
      if (doc.supervisorApproveRejectDate){
          dtSupervisorApproveRejectDate = new Date(doc.supervisorApproveRejectDate).getTime();
      }
      var dtApproveRejectDate = null;
      if (doc.approveRejectDate ){
        dtApproveRejectDate = new Date(doc.approveRejectDate).getTime();
      }
      var jointFieldWorkCase = null;
      var purposeOfJointFieldWork = null;
      var dateOfCall = null;
      var personContacted = null;
      var mobileNo = null;
      var mobileCountryCode = null;
      var approveComment = null;

      if (doc.accept){
        var accept = doc.accept;
        jointFieldWorkCase = accept.jointFieldWorkCase;
        purposeOfJointFieldWork = accept.jointFieldWorkCBGroup;
        dateOfCall = accept.callDate;
        personContacted = accept.contactPerson;
        mobileNo = accept.mobileNo;
        mobileCountryCode = accept.mobileCountryCode;
        approveComment = accept.approveComment;
      }
      var emitObj = {
          compCode: doc.compCode,
          displayCaseNo:  doc.policyId,
          caseNo: doc.policyId,
          agentId: doc.agentId,
          agentName: doc.agentName,
          managerName: doc.managerName,
          managerId: doc.managerId,
          directorId: doc.directorId,
          directorName: doc.directorName,
          approveManagerId: doc.approveRejectManagerId,
          approveManagerName: doc.approveRejectManagerName,
          submittedDate: doc.submittedDate,
          approvalStatus: doc.approvalStatus,
          approvalCaseId: doc.approvalCaseId,
          applicationId: doc.applicationId,
          lastEditedBy: doc.lastEditedBy,
          supervisorApproveRejectDate : dtSupervisorApproveRejectDate,
          approveRejectDate: dtApproveRejectDate,
          approveRejectManagerId: doc.approveRejectManagerId,
          approveRejectManagerName : doc.approveRejectManagerName,
          faFirmName : doc.faFirmName,
          jointFieldWorkCase : jointFieldWorkCase,
          purposeOfJointFieldWork : purposeOfJointFieldWork,
          dateOfCall : dateOfCall,
          personContacted : personContacted,
          mobileNo : mobileNo,
          mobileCountryCode : mobileCountryCode,
          approveComment : approveComment
        };
      // eslint-disable-next-line no-undef
      emit(['01', doc.approvalCaseId], emitObj);
    }
  },

  approvalDetails: function (doc) {
    if (doc && doc.type === 'approval') {
      var emitObj = {
          compCode: doc.compCode,
          displayCaseNo:  doc.policyId,
          caseNo: doc.policyId,
          product: doc.productName,
          agentId: doc.agentId,
          agentName: doc.agentName,
          managerName: doc.managerName,
          managerId: doc.managerId,
          directorId: doc.directorId,
          directorName: doc.directorName,
          approveManagerId: doc.approveRejectManagerId,
          approveManagerName: doc.approveRejectManagerName,
          approveRejectManagerId: doc.approveRejectManagerId,
          approveRejectManagerName: doc.approveRejectManagerName,
          submittedDate: doc.submittedDate,
          approvalStatus: doc.approvalStatus,
          onHoldReason: doc.onHoldReason,
          approvalCaseId: doc.approvalCaseId,
          applicationId: doc.applicationId,
          quotationId:  doc.quotationId,
          customerId: doc.customerId,
          customerName: doc.customerName,
          lastEditedBy: doc.lastEditedBy,
          lastEditedDate: doc.lastEditedDate,
          approveRejectDate: doc.approveRejectDate,
          caseLockedManagerCodebyStatus: doc.caseLockedManagerCodebyStatus,
          customerICNo: doc.customerICNo,
          agentProfileId: doc.agentProfileId,
          expiredDate: doc.expiredDate,
          masterApprovalId: doc.masterApprovalId || '',
          isShield: doc.isShield,
          proposalNumber: doc.proposalNumber || doc.policyId
      };
      // eslint-disable-next-line no-undef
      emit(['01', doc.approvalStatus, doc.approvalCaseId], emitObj);
      // eslint-disable-next-line no-undef
      emit(['01', doc.agentId], emitObj);
    }
  },

  masterApprovalDetails: function (doc) {
    if (doc && doc.type === 'masterApproval') {
      var multiplePolicyNumber;
      if (doc.subApprovalList && doc.subApprovalList.length > 0) {
        for (var i = 0; i < doc.subApprovalList.length; i++) {
            if (i === 0) {
              multiplePolicyNumber = doc.subApprovalList[i];
            } else {
              multiplePolicyNumber += ', ' + doc.subApprovalList[i];
            }
        }
      }

      var emitObj = {
          compCode: doc.compCode,
          displayCaseNo:  multiplePolicyNumber || doc.policyId,
          caseNo: doc.approvalCaseId,
          product: doc.productName,
          agentId: doc.agentId,
          agentName: doc.agentName,
          managerName: doc.managerName,
          managerId: doc.managerId,
          directorId: doc.directorId,
          directorName: doc.directorName,
          approveManagerId: doc.approveRejectManagerId,
          approveManagerName: doc.approveRejectManagerName,
          approveRejectManagerId: doc.approveRejectManagerId,
          approveRejectManagerName: doc.approveRejectManagerName,
          submittedDate: doc.submittedDate,
          approvalStatus: doc.approvalStatus,
          onHoldReason: doc.onHoldReason,
          approvalCaseId: doc.approvalCaseId,
          applicationId: doc.applicationId,
          quotationId:  doc.quotationId,
          customerId: doc.customerId,
          customerName: doc.customerName,
          lastEditedBy: doc.lastEditedBy,
          lastEditedDate: doc.lastEditedDate,
          approveRejectDate: doc.approveRejectDate,
          caseLockedManagerCodebyStatus: doc.caseLockedManagerCodebyStatus,
          customerICNo: doc.customerICNo,
          agentProfileId: doc.agentProfileId,
          expiredDate: doc.expiredDate,
          subApprovalList: doc.subApprovalList || [],
          isShield: doc.isShield,
          proposalNumber: doc.proposalNumber || doc.policyId
        };
        // eslint-disable-next-line no-undef
      emit(['01', doc.approvalStatus, doc.approvalCaseId], emitObj);
      // eslint-disable-next-line no-undef
      emit(['01', doc.agentId], emitObj);
    }
  },

  agents: function (doc) {
    if (doc && doc.type === 'agent' || doc.TYPE === 'agent') {
      var faadminCode;
      if (doc.rawData && doc.rawData.faAdvisorRole !== undefined) {
        faadminCode = doc.rawData.upline2Code;
      }
      var emitObj = {
          email: doc.email,
          tel: doc.tel,
          agentCode: doc.agentCode,
          agentName: doc.name,
          mobile: doc.mobile,
          manager: doc.manager,
          managerCode: doc.managerCode,
          compCode: doc.compCode,
          company: doc.company,
          faAdminCode: faadminCode,
          profileId: doc.profileId,
          channel: doc.channel
        };
      // eslint-disable-next-line no-undef
      emit(['01', doc.agentCode], emitObj);
    }
  },

  agentWithDescendingOrder: function (doc) {
    if (doc && doc.type === 'agent' || doc.TYPE === 'agent') {
      var faadminCode;
      if (doc.rawData && doc.rawData.faAdvisorRole !== undefined) {
        faadminCode = doc.rawData.upline2Code;
      }
      var lastChangedTime = doc.lstChgDate;
      if (!lastChangedTime && doc.lastUpdateDate) {
        lastChangedTime = new Date(doc.lastUpdateDate).getTime();
      } else if (!lastChangedTime) {
        lastChangedTime = 0;
      }
      var emitObj = {
          email: doc.email,
          tel: doc.tel,
          agentCode: doc.agentCode,
          agentName: doc.name,
          mobile: doc.mobile,
          manager: doc.manager,
          managerCode: doc.managerCode,
          compCode: doc.compCode,
          company: doc.company,
          faAdminCode: faadminCode,
          profileId: doc.profileId
        };
      // eslint-disable-next-line no-undef
      emit(['01', 'timeFirst', lastChangedTime, doc.agentCode], emitObj);
      // eslint-disable-next-line no-undef
      emit(['01', 'agentCodeFirst', doc.agentCode, lastChangedTime], emitObj);
    }
  },

  agentDetails: function (doc) {
    if (doc && doc.type === 'agent') {
      var emitObj = Object.assign({}, doc);
      if (doc.rawData) {
        // eslint-disable-next-line no-undef
        emit(['01', 'userId', doc.rawData.agentCode], emitObj);
      }
      // eslint-disable-next-line no-undef
      emit(['01', 'agentCode', doc.agentCode], emitObj);

      if ((doc.channel === 'BROKER' || doc.channel === 'SYNERGY') && doc.rawData && doc.rawData.upline2Code) {
        // eslint-disable-next-line no-undef
        emit(['01', 'fafirmCode', doc.rawData.upline2Code], emitObj);
      }

      if (doc.rawData && doc.rawData.proxy1UserId) {
        // eslint-disable-next-line no-undef
        emit(['01', 'proxy', doc.rawData.proxy1UserId], {agentCode: doc.agentCode});
      }

      if (doc.rawData && doc.rawData.proxy2UserId) {
        // eslint-disable-next-line no-undef
        emit(['01', 'proxy', doc.rawData.proxy2UserId], {agentCode: doc.agentCode});
      }
    }
  },

  submission: function (doc) {
    if (doc && (doc.type === 'approval' || doc.type === 'masterApproval')) {
      const escapeStatusForExpiryView = ['E','A','R'];
      const pendingForFAFirmStatus = ['PFAFA', 'PDocFAF', 'PDisFAF'];
      var emitObj = {
        approvalCaseId: doc.approvalCaseId,
        status: doc.approvalStatus,
        applicationId: doc.applicationId,
        policyId: doc.policyId,
        agentId: doc.agentId,
        directorId: doc.directorId,
        managerId: doc.managerId,
        lastEditedDate: doc.lastEditedDate,
        submittedDate: doc.submittedDate,
        quotationId: doc.quotationId,
        approveRejectDate: doc.approveRejectDate,
        submisssionFlag : doc.submisssionFlag || false,
        isFACase: doc.isFACase || false,
        caseLockedManagerCodebyStatus: doc.caseLockedManagerCodebyStatus || '',
        isShield: doc.isShield || false,
        subApprovalList: doc.subApprovalList || [],
        approvalStatus: doc.approvalStatus
      };

      var longUTCSubmitDate = Date.parse(doc.submittedDate);
      var longUTCLastEditDate = Date.parse(doc.lastEditedDate);
      var longUTCApproveRejectDate = Date.parse(doc.approveRejectDate);
      var longUTCSupervisorApproveRejectDate = Date.parse(doc.supervisorApproveRejectDate);

      // Need child cases to send RLS and WFI, Need Parent Caeses to send notification
      //Prepare a list to set cases expired
      if (escapeStatusForExpiryView.indexOf(doc.approvalStatus) === -1) {
        // eslint-disable-next-line no-undef
        emit(['01', 'expired', longUTCSubmitDate, doc.approvalStatus], emitObj);
      }

      // RLS/WFI View, only need child json
      //Prepare a list to submit doc to WFI & EIP
      if (doc.type === 'approval') {
        // eslint-disable-next-line no-undef
        emit(['01', 'submitdoc', doc.approvalStatus,  doc.submisssionFlag || false], emitObj);
      }

      // Notifications View, only need master json
      if (doc.type === 'masterApproval' || !doc.isShield) {
        //Prepare a list to send notification of pending approval case
        // eslint-disable-next-line no-undef
        emit(['01', 'pendingapproval', longUTCSubmitDate, doc.managerId, doc.approvalStatus], emitObj);

        //Prepare a list to send notification of No action case
        // eslint-disable-next-line no-undef
        emit(['01', 'lastedit', longUTCLastEditDate, longUTCSubmitDate, doc.approvalStatus], emitObj);

        //Prepare a list for Case Approval Reminder (FA Admin)
        if (pendingForFAFirmStatus.indexOf(doc.approvalStatus) > -1) {
          // eslint-disable-next-line no-undef
          emit(['01', 'pendingForFAFirm', longUTCSupervisorApproveRejectDate, doc.agentId, doc.approvalStatus], emitObj);
        }

        //Prepare a list for expiry notification
        if (doc.approvalStatus === 'E' && doc.expiredDate) {
          var longUTCExpiredDate = Date.parse(doc.expiredDate);
          // eslint-disable-next-line no-undef
          emit(['01', 'expiredNotification', longUTCExpiredDate, doc.approvalStatus], emitObj);
        }

        //Prepare a list for secondary Notification
        // eslint-disable-next-line no-undef
        emit(['01', 'secondaryProxyNotification', longUTCSubmitDate, doc.approvalStatus], emitObj);
      }
    }
  },

  healthDeclarationNotification: function (doc, meta) {
    var productTohaveHDNotification = false;
    if (doc && doc.quotation && doc.quotation.quotType === 'SHIELD') {
      productTohaveHDNotification = true;
    }
    if (doc.type === 'masterApplication' && doc.applicationSignedDate && productTohaveHDNotification) {
      var lastUpdateDate = new Date(doc.applicationSignedDate);
      // eslint-disable-next-line no-undef
      emit(['01', lastUpdateDate.getTime()], {
        appId: doc.id || doc._id,
        bundleId: doc.bundleId,
        quotId: doc.quotationDocId,
        docType: doc.type
      });
    }
  },

  signatureExpire: function (doc, meta) {
    if ((doc.type === 'application' && !doc.isSubmittedStatus && !doc.isInvalidated && doc.biSignedDate)
        || (doc.type === 'masterApplication' && !doc.isSubmittedStatus && !doc.isInvalidated && doc.biSignedDate)) {
      var lastUpdateDate = new Date(doc.biSignedDate);
      // eslint-disable-next-line no-undef
      emit(['01', lastUpdateDate.getTime()], {
        appId: doc.id || doc._id,
        polNo: doc.policyNumber || '',
        bundleId: doc.bundleId,
        signDate: lastUpdateDate.getTime(),
        payMethod: doc.payment && doc.payment.trxMethod,
        payStatus: doc.payment && doc.payment.trxStatus,
        isFullySigned: doc.isFullySigned,
        isInitialPaymentCompleted: doc.isInitialPaymentCompleted || doc.isInitialPaymentComeleted,
        docType: doc.type
      });
    }
  },

  approvalDateCases: function (doc) {
    if (doc && doc.type === 'approval') {
      if (doc.approveRejectDate) {
        var approveDate = new Date(doc.approveRejectDate);
        var emitObj = {
            compCode: doc.compCode,
            caseNo: doc.policyId,
            product: doc.productName,
            agentId: doc.agentId,
            agentName: doc.agentName,
            managerName: doc.managerName,
            managerId: doc.managerId,
            directorId: doc.directorId,
            directorName: doc.directorName,
            approveManagerId: doc.approveRejectManagerId,
            approveManagerName: doc.approveRejectManagerName,
            submittedDate: doc.submittedDate,
            approvalStatus: doc.approvalStatus,
            onHoldReason: doc.onHoldReason,
            approvalCaseId: doc.approvalCaseId,
            applicationId: doc.applicationId,
            quotationId:  doc.quotationId,
            customerId: doc.customerId,
            customerName: doc.customerName,
            approveRejectDate: doc.approveRejectDate,
            lastEditedBy: doc.lastEditedBy
          };
        // eslint-disable-next-line no-undef
        emit(['01', approveDate.getTime()], emitObj);
      }

    }
  },

  cpfApps: function (doc, meta) {
    if (doc.type === 'application') {
      var quot = doc.quotation;
      var payment = doc.payment;
      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;

      if (payment  && quot && quot.agent ){
        var trxTime = payment.trxTime || payment.trxStartTime;
        var covName = null;
        if (quot.plans){
          var plans = quot.plans;
          if (plans && plans.length > 0 &&  plans[0].covName){
            covName = plans[0].covName;
          }
        }
        // eslint-disable-next-line no-undef
        emit(['01',  payment.initPayMethod], {
        id: doc.id,
        policyNumber:doc.policyNumber,
        type: doc.type,
        covName: covName,
        payment: doc.payment,
        lastUpdateDate: lastUpdateDate
      });
      }
    }
  },

  appPayment: function (doc, meta) {
    if (doc.type === 'application') {
      var quot = doc.quotation;
      var payment = doc.payment;
      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;

      if (payment  && quot && quot.agent ){
        var trxTime = payment.trxTime || payment.trxStartTime;
        // eslint-disable-next-line no-undef
        emit(['01', payment.initPayMethod, trxTime], {
          id: doc.id,
          policyNumber:doc.policyNumber,
          type: doc.type,
          trxTime: trxTime,
          ccy:payment.policyCcy,
          initTotalPrem:payment.initTotalPrem,
          trxAmount:payment.trxAmount,
          paymentMethod:payment.initPayMethod,
          trxNo:payment.trxNo,
          proposerName: quot.pFullName,
          agentName:quot.agent.name,
          agentCompany:quot.agent.company,
          trxStatus:payment.trxStatus,
          quotation: doc.quotation,
          payment: doc.payment,
          applicationSubmittedDate: doc.applicationSubmittedDate,
          lastUpdateDate: lastUpdateDate
        });
      }
    }
  },

  onlinePayment: function (doc, meta) {
    if (doc.type === 'application') {
      var quot = doc.quotation;
      var payment = doc.payment;
      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;

      if (payment  && quot && quot.agent ){
        var trxTime = payment.trxTime || payment.trxStartTime;
        var isShield = quot.quotType === 'SHIELD';
        if (isShield){
          var agent = doc.agent;
          var ccy = '';
          var applicationForm = doc.applicationForm;
          if (
            applicationForm.values &&
            applicationForm.values.planDetails){
            var planDetails = applicationForm.values.planDetails;
            ccy = planDetails.ccy;
              }

          var initPayMethod = null;
          if (payment){
            initPayMethod = payment.initPayMethod;
            if (payment.cashPortion  === 0){
              initPayMethod = '-';
            }
          }
          // eslint-disable-next-line no-undef
          emit(['01', initPayMethod, trxTime], {
            id: doc.id,
            parentId: doc.parentId,
            policyNumber:doc.policyNumber,
            type: doc.type,
            trxTime: trxTime,
            ccy:ccy,
            initTotalPrem:payment.cashPortion,
            trxAmount:payment.totCashPortion,
            paymentMethod:payment.initPayMethod,
            trxNo:payment.trxNo || '',
            proposerName: quot.pFullName,
            agentName:agent.name,
            agentCompany:agent.company,
            trxStatus:payment.trxStatus || '',
            trxEnquiryStatus:payment.trxEnquiryStatus,
            applicationSubmittedDate: doc.applicationSubmittedDate,
            lastUpdateDate: lastUpdateDate
          });
        } else {
          // eslint-disable-next-line no-undef
          emit(['01', payment.initPayMethod, trxTime], {
            id: doc.id,
            policyNumber:doc.policyNumber,
            type: doc.type,
            trxTime: trxTime,
            ccy:payment.policyCcy,
            initTotalPrem:payment.initTotalPrem,
            trxAmount:payment.initTotalPrem,
            paymentMethod:payment.initPayMethod,
            trxNo:payment.trxNo,
            proposerName: quot.pFullName,
            agentName:quot.agent.name,
            agentCompany:quot.agent.company,
            trxStatus:payment.trxStatus,
            trxEnquiryStatus:payment.trxEnquiryStatus,
            applicationSubmittedDate: doc.applicationSubmittedDate,
            lastUpdateDate: lastUpdateDate
          });
        }

      }
    }
  },

  bundleApp: function (doc) {
    if (doc && doc.type === 'bundle' && doc.applications) {
      var bundle = doc;
      for (var i = 0; i < doc.applications.length; i++) {
        var app = doc.applications[i];
        var docType = app.applicationDocId && app.applicationDocId.indexOf('SA') < 0 ? 'application' : 'masterApplication';
        // eslint-disable-next-line no-undef
        emit(['01', 'application',  app.applicationDocId], {
          bundleId: bundle.id,
          applicationDocId: app.applicationDocId,
          appStatus: app.appStatus,
          isValid: doc.isValid
        });

      }
    }
  },

  approvalApp: function (doc) {
    if (doc && doc.type === 'approval') {
      var emitObj = {
          compCode: doc.compCode,
          caseNo: doc.policyId,
          agentId: doc.agentId,
          agentName: doc.agentName,
          managerName: doc.managerName,
          managerId: doc.managerId,
          directorId: doc.directorId,
          directorName: doc.directorName,
          approveManagerId: doc.approveRejectManagerId,
          approveManagerName: doc.approveRejectManagerName,
          submittedDate: doc.submittedDate,
          approvalStatus: doc.approvalStatus,
          approvalCaseId: doc.approvalCaseId,
          applicationId: doc.applicationId,
          lastEditedBy: doc.lastEditedBy
        };
      // eslint-disable-next-line no-undef
      emit(['01', doc.approvalCaseId], emitObj);
    }
  },

  appWithSubmitDate: function (doc, meta) {
    if (doc.type === 'application') {
      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;
      if (doc.applicationSubmittedDate && doc.policyNumber){
        var dtAppSubDate = new Date(doc.applicationSubmittedDate);

        var quotation = doc.quotation;
        var payment = doc.payment;
        var applicationForm = doc.applicationForm;
        var agentName = null;
        var channel = null;
        var agentCode = null;
        var plans = null;
        var premium = null;
        var ccy = null;
        var sameAs = null;
        var paymentMode = null;
        var pTrustedIndividuals = null;
        var pFullName = null;
        var pLastName = null;
        var pFirstName = null;
        var pOthName = null;
        var pHanyuPinyinName = null;
        var iFullName = null;
        var iLastName = null;
        var iFirstName = null;
        var iOthName = null;
        var iHanyuPinyinName = null;
        var iUndischargedBankrupt = null;
        var pUndischargedBankrupt = null;
        var payorSurname = null;
        var payorGivenName = null;
        var payorOtherName = null;
        var payorPinYinName = null;
        var initPayMethod = null;
        var trxStatus = null;
        var trxNo = null;
        var isBackDate = null;
        if (quotation) {
          premium = quotation.premium;
          ccy = quotation.ccy;
          sameAs = quotation.sameAs;
          paymentMode = quotation.paymentMode;

          if (quotation.plans){
            var planList = quotation.plans;
            plans = new Array(planList.length);
            for ( var i = 0; i < planList.length; i++){
              var plan = planList[i];
              plans[i] = (
                {
                  covName : plan.covName,
                  sumInsured : plan.sumInsured,
                }
              );
            }
          }

          if (quotation.agent){
            agentName = quotation.agent.name;
            agentCode = quotation.agent.agentCode;
            channel = quotation.agent.dealerGroup;
          }
        }

        if (applicationForm && applicationForm.values){
          var values = applicationForm.values;
          if (values.proposer){
            var proposer = values.proposer;
            if (proposer.personalInfo){
              var personalInfo = proposer.personalInfo;
              pTrustedIndividuals = personalInfo.trustedIndividuals;
              pFullName = personalInfo.fullName;
              pLastName = personalInfo.lastName;
              pFirstName = personalInfo.firstName;
              pOthName = personalInfo.othName;
              pHanyuPinyinName = personalInfo.hanyuPinyinName;
            }
            if (proposer.declaration){
              var declaration = proposer.declaration;
              pUndischargedBankrupt = declaration.BANKRUPTCY01;
              payorSurname = declaration.FUND_SRC03;
              payorGivenName = declaration.FUND_SRC04;
              payorOtherName = declaration.FUND_SRC05;
              payorPinYinName = declaration.FUND_SRC06;
            }
          }
          if (values.insured && values.insured[0]){
            var insured = values.insured[0];
            if (insured.personalInfo){
              var personalInfo = insured.personalInfo;
              iFullName = personalInfo.fullName;
              iLastName = personalInfo.lastName;
              iFirstName = personalInfo.firstName;
              iOthName = personalInfo.othName;
              iHanyuPinyinName = personalInfo.hanyuPinyinName;
            }
            if (insured.declaration){
              iUndischargedBankrupt = insured.declaration.BANKRUPTCY01;
            }
          }
          if (values.planDetails){
            var planDetails = values.planDetails;
            isBackDate = planDetails.isBackDate;
          }
          if (quotation && quotation.quotType === 'SHIELD'){
            if (values.planDetails){
              var planDetails = values.planDetails;
              ccy = planDetails.ccy;
              premium = 0;
              if (planDetails.planList){
                var planList = planDetails.planList;
                plans = new Array(planList.length);
                for ( var i = 0; i < planList.length; i++){
                  var plan = planList[i];
                  plans[i] = (
                    {
                      covName : plan.covName,
                      sumInsured : plan.sumInsured,
                    }
                  );

                  premium = premium + plan.premium;
                  if (i === 0) {
                    paymentMode = plan.payFreq;
                  }
                }
              }
            }
          }
      }
      if (payment){
        initPayMethod = payment.initPayMethod;
        if (quotation && quotation.quotType === 'SHIELD'){
          if (payment.cashPortion  === 0){
            initPayMethod = '-';
          }
        }
        trxStatus = payment.trxStatus;
        trxNo = payment.trxNo;
      }
      // eslint-disable-next-line no-undef
        emit(['01', dtAppSubDate.getTime()], {
          id: doc.id,
          parentId: doc.parentId,
          appStatus: doc.appStatus,
          policyNumber:doc.policyNumber,
          type: doc.type,
          isCrossAge: doc.isCrossAge,
          isBackDate: isBackDate,
          applicationSubmittedDate: doc.applicationSubmittedDate,
          lastUpdateDate: lastUpdateDate,
          agentCode:agentCode,
          agentName:agentName,
          channel:channel,
          premium:premium,
          ccy:ccy,
          sameAs:sameAs,
          paymentMode:paymentMode,
          pTrustedIndividuals:pTrustedIndividuals,
          pFullName:pFullName,
          pLastName:pLastName,
          pFirstName:pFirstName,
          pOthName:pOthName,
          pHanyuPinyinName:pHanyuPinyinName,
          iFullName:iFullName,
          iLastName:iLastName,
          iFirstName:iFirstName,
          iOthName:iOthName,
          iHanyuPinyinName:iHanyuPinyinName,
          iUndischargedBankrupt:iUndischargedBankrupt,
          pUndischargedBankrupt:pUndischargedBankrupt,
          payorSurname:payorSurname,
          payorGivenName:payorGivenName,
          payorOtherName:payorOtherName,
          payorPinYinName:payorPinYinName,
          initPayMethod:initPayMethod,
          trxStatus:trxStatus,
          trxNo:trxNo,
          plans:plans
        });
      }
    }
  },

  appWithoutSubmitDate: function (doc, meta) {
    if (doc.type === 'application') {
      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;
      if (!doc.applicationSubmittedDate && doc.policyNumber) {
          var quotation = doc.quotation;
          var payment = doc.payment;
          var applicationForm = doc.applicationForm;
          var agentName = null;
          var channel = null;
          var agentCode = null;
          var plans = null;
          var premium = null;
          var ccy = null;
          var sameAs = null;
          var paymentMode = null;
          var pTrustedIndividuals = null;
          var pFullName = null;
          var pLastName = null;
          var pFirstName = null;
          var pOthName = null;
          var pHanyuPinyinName = null;
          var iFullName = null;
          var iLastName = null;
          var iFirstName = null;
          var iOthName = null;
          var iHanyuPinyinName = null;
          var iUndischargedBankrupt = null;
          var pUndischargedBankrupt = null;
          var payorSurname = null;
          var payorGivenName = null;
          var payorOtherName = null;
          var payorPinYinName = null;
          var initPayMethod = null;
          var trxStatus = null;
          var trxNo = null;
          var isBackDate = null;
          if (quotation) {
            premium = quotation.premium;
            ccy = quotation.ccy;
            sameAs = quotation.sameAs;
            paymentMode = quotation.paymentMode;

            if (quotation.plans){
              var planList = quotation.plans;
              plans = new Array(planList.length);
              for ( var i = 0; i < planList.length; i++){
                var plan = planList[i];
                plans[i] = (
                  {
                    covName : plan.covName,
                    sumInsured : plan.sumInsured,
                  }
                );
              }
            }

            if (quotation.agent){
              agentName = quotation.agent.name;
              agentCode = quotation.agent.agentCode;
              channel = quotation.agent.dealerGroup;
            }
          }

          if (applicationForm && applicationForm.values){
            var values = applicationForm.values;
            if (values.proposer){
              var proposer = values.proposer;
              if (proposer.personalInfo){
                var personalInfo = proposer.personalInfo;
                pTrustedIndividuals = personalInfo.trustedIndividuals;
                pFullName = personalInfo.fullName;
                pLastName = personalInfo.lastName;
                pFirstName = personalInfo.firstName;
                pOthName = personalInfo.othName;
                pHanyuPinyinName = personalInfo.hanyuPinyinName;
              }
              if (proposer.declaration){
                var declaration = proposer.declaration;
                pUndischargedBankrupt = declaration.BANKRUPTCY01;
                payorSurname = declaration.FUND_SRC03;
                payorGivenName = declaration.FUND_SRC04;
                payorOtherName = declaration.FUND_SRC05;
                payorPinYinName = declaration.FUND_SRC06;
              }
            }
            if (values.insured && values.insured[0]){
              var insured = values.insured[0];
              if (insured.personalInfo){
                var personalInfo = insured.personalInfo;
                iFullName = personalInfo.fullName;
                iLastName = personalInfo.lastName;
                iFirstName = personalInfo.firstName;
                iOthName = personalInfo.othName;
                iHanyuPinyinName = personalInfo.hanyuPinyinName;
              }
              if (insured.declaration){
                iUndischargedBankrupt = insured.declaration.BANKRUPTCY01;
              }
            }

            if (values.planDetails){
              var planDetails = values.planDetails;
              isBackDate = planDetails.isBackDate;
            }

            if (quotation && quotation.quotType === 'SHIELD'){
              if (values.planDetails){
                var planDetails = values.planDetails;
                ccy = planDetails.ccy;
                premium = 0;
                if (planDetails.planList){
                  var planList = planDetails.planList;
                  plans = new Array(planList.length);
                  for ( var i = 0; i < planList.length; i++){
                    var plan = planList[i];
                    plans[i] = (
                      {
                        covName : plan.covName,
                        sumInsured : plan.sumInsured,
                      }
                    );

                    premium = premium + plan.premium;
                    if (i === 0) {
                      paymentMode = plan.payFreq;
                    }
                  }
                }
              }
            }
        }
        if (payment){
          initPayMethod = payment.initPayMethod;
          if (quotation && quotation.quotType === 'SHIELD'){
            if (payment.cashPortion  === 0){
              initPayMethod = '-';
            }
          }
          trxStatus = payment.trxStatus;
          trxNo = payment.trxNo;
        }
        // eslint-disable-next-line no-undef
        emit(['01'], {
          id: doc.id,
          appStatus: doc.appStatus,
          parentId: doc.parentId,
          policyNumber:doc.policyNumber,
          type: doc.type,
          isCrossAge: doc.isCrossAge,
          isBackDate: isBackDate,
          applicationSubmittedDate: doc.applicationSubmittedDate,
          lastUpdateDate: lastUpdateDate,
          agentCode:agentCode,
          agentName:agentName,
          channel:channel,
          premium:premium,
          ccy:ccy,
          sameAs:sameAs,
          paymentMode:paymentMode,
          pTrustedIndividuals:pTrustedIndividuals,
          pFullName:pFullName,
          pLastName:pLastName,
          pFirstName:pFirstName,
          pOthName:pOthName,
          pHanyuPinyinName:pHanyuPinyinName,
          iFullName:iFullName,
          iLastName:iLastName,
          iFirstName:iFirstName,
          iOthName:iOthName,
          iHanyuPinyinName:iHanyuPinyinName,
          iUndischargedBankrupt:iUndischargedBankrupt,
          pUndischargedBankrupt:pUndischargedBankrupt,
          payorSurname:payorSurname,
          payorGivenName:payorGivenName,
          payorOtherName:payorOtherName,
          payorPinYinName:payorPinYinName,
          initPayMethod:initPayMethod,
          trxStatus:trxStatus,
          trxNo:trxNo,
          plans:plans
        });
      }
    }
  },

  allChannelApprovalCases: function (doc) {
    if (doc && doc.type === 'approval') {
      var dtSupervisorApproveRejectDate = null;
      if (doc.supervisorApproveRejectDate){
          dtSupervisorApproveRejectDate = new Date(doc.supervisorApproveRejectDate).getTime();
      }
      var dtApproveRejectDate = null;
      if (doc.approveRejectDate ){
        dtApproveRejectDate = new Date(doc.approveRejectDate).getTime();
      }
      var jointFieldWorkCase = null;
      var purposeOfJointFieldWork = null;
      var dateOfCall = null;
      var personContacted = null;
      var mobileNo = null;
      var mobileCountryCode = null;
      var approveComment = null;

      if (doc.accept){
        var accept = doc.accept;
        jointFieldWorkCase = accept.jointFieldWorkCase;
        purposeOfJointFieldWork = accept.jointFieldWorkCBGroup;
        dateOfCall = accept.callDate;
        personContacted = accept.contactPerson;
        mobileNo = accept.mobileNo;
        mobileCountryCode = accept.mobileCountryCode;
        approveComment = accept.approveComment;
      }
      var emitObj = {
          compCode: doc.compCode,
          caseNo: doc.policyId,
          agentId: doc.agentId,
          agentName: doc.agentName,
          managerName: doc.managerName,
          managerId: doc.managerId,
          directorId: doc.directorId,
          directorName: doc.directorName,
          approveManagerId: doc.approveRejectManagerId,
          approveManagerName: doc.approveRejectManagerName,
          submittedDate: doc.submittedDate,
          approvalStatus: doc.approvalStatus,
          approvalCaseId: doc.approvalCaseId,
          applicationId: doc.applicationId,
          lastEditedBy: doc.lastEditedBy,
          supervisorApproveRejectDate : dtSupervisorApproveRejectDate,
          approveRejectDate: dtApproveRejectDate,
          approveRejectManagerId: doc.approveRejectManagerId,
          approveRejectManagerName : doc.approveRejectManagerName,
          faFirmName : doc.faFirmName,
          jointFieldWorkCase : jointFieldWorkCase,
          purposeOfJointFieldWork : purposeOfJointFieldWork,
          dateOfCall : dateOfCall,
          personContacted : personContacted,
          mobileNo : mobileNo,
          mobileCountryCode : mobileCountryCode,
          approveComment : approveComment
        };
      // eslint-disable-next-line no-undef
      emit(['01', doc.approvalCaseId], emitObj);
    }
  },

  incorrectPremiumTypeApps: function (doc, meta) {
    var isIncorrect = false;
    var quot = {};

    if (doc.type === 'quotation') {
      quot = doc;
    } else if (doc.type === 'application') {
      quot = doc.quotation;
    }

    if (quot && quot.plans) {
      var plans = quot.plans;

      for (var i in plans) {
        var plan = plans[i];
        if (plan.premium && typeof plan.premium !== 'number') {
          isIncorrect = true;
          break;
        } else if (plan.halfYearPrem && typeof plan.halfYearPrem !== 'number') {
          isIncorrect = true;
          break;
        } else if (plan.quarterPrem && typeof plan.quarterPrem !== 'number') {
          isIncorrect = true;
          break;
        } else if (plan.monthPrem && typeof plan.monthPrem !== 'number') {
          isIncorrect = true;
          break;
        }
      }

      if (isIncorrect) {
        // eslint-disable-next-line no-undef
        emit(['01', doc.pCid, doc.id], {
          id: doc.id,
          policyNumber:doc.policyNumber,
          type: doc.type,
          isValid: doc.isValid,
          quotationDocId: doc.quotationDocId,
          baseProductCode: quot.baseProductCode,
          baseProductName: quot.baseProductName,
          iName: quot.iFullName,
          pName: quot.pFullName,
          ccy: quot.ccy,
          totPremium: quot.premium,
          paymentMode: quot.paymentMode,
          plans: quot.plans,
          productQuotForm: quot.productQuotForm,
          isSubQuotation: !!quot.isSubQuotation,
          isInitialPaymentCompleted: doc.isInitialPaymentCompleted,
          isAgentReportSigned: doc.isAgentReportSigned,
          iCid: doc.iCid,
          pCid: doc.pCid,
          isStartSignature: doc.isStartSignature,
          isFullySigned: doc.isFullySigned,
          quotation: doc.quotation,
          isMandDocsAllUploaded: doc.isMandDocsAllUploaded || false,
          iCids: doc.iCids || [],
          iCidMapping: doc.iCidMapping || {}
        });
      }

    }
  },


  validbundleApplicationsByAgent: function (doc) {
    if (doc && doc.type === 'bundle' && doc.applications && doc.isValid) {
      var bundle = doc;
      for (var i = 0; i < doc.applications.length; i++) {
        var app = doc.applications[i];
        if (app.applicationDocId && app.appStatus === 'APPLYING') {
          // eslint-disable-next-line no-undef
          emit(['01', doc.agentId], {
            bundleId: bundle.id,
            pCid: bundle.pCid,
            quotationDocId: app.quotationDocId,
            applicationDocId: app.applicationDocId
          });
        } else if (app.applicationDocId === undefined && app.appStatus === undefined){
          // eslint-disable-next-line no-undef
          emit(['01', doc.agentId], {
            bundleId: bundle.id,
            pCid: bundle.pCid,
            quotationDocId: app.quotationDocId,
            applicationDocId: null
          });
        }
      }
    }
  },

  inProgressQuotFunds: function (doc) {
    if (doc && doc.type === 'bundle') {
      if (doc.applications && doc.isValid) {
        var bundle = doc;
        for (var i = 0; i < doc.applications.length; i++) {
          var app = doc.applications[i];
          if (app.quotationDocId) {
            if (app.applicationDocId && app.appStatus === 'APPLYING') {
              // eslint-disable-next-line no-undef
              emit(['01', app.quotationDocId, null], {
                bundleId: bundle.id,
                quotationDocId: app.quotationDocId,
                applicationDocId: app.applicationDocId
              });
            } else if (app.applicationDocId === undefined && app.appStatus === undefined) {
              // eslint-disable-next-line no-undef
              emit(['01', app.quotationDocId, null], {
                bundleId: bundle.id,
                quotationDocId: app.quotationDocId,
              });
            }
          }
        }
      }
    } else if (doc && doc.type === 'quotation') {
      var funds = doc.fund && doc.fund.funds;
      if (funds) {
        for (var i = 0; i < funds.length; i++) {
          var fund = funds[i] || {};
          // eslint-disable-next-line no-undef
          emit(['01', doc.id, fund.fundCode], {
            quotationId: doc.id,
            fundCode: fund.fundCode,
            fundName: fund.fundName
          });
        }
      }
    }
  },

  applicationsByAgent: function (doc) {
    if (doc && (doc.type === 'application' || doc.type === 'masterApplication')) {
      var agentId = doc.quotation.agent.agentCode;
      var productName;
      var isShield = doc.quotation && doc.quotation.quotType === 'SHIELD';
      if (doc && doc.quotation && doc.quotation.plans && doc.quotation.plans[0] && doc.quotation.plans[0].covName && !isShield){
        productName = doc.quotation.plans[0].covName;
      } else if (doc.quotation && doc.quotation.baseProductName && isShield){
        doc.quotation.baseProductName.en =  `${doc.quotation.baseProductName.en} Plan`;
        productName = doc.quotation.baseProductName;
      }

      var customerICNo;
      if (doc && doc.applicationForm && doc.applicationForm.values && doc.applicationForm.values.proposer && doc.applicationForm.values.proposer.personalInfo) {
        customerICNo = doc.applicationForm.values.proposer.personalInfo.idCardNo;
      }
      // eslint-disable-next-line no-undef
      emit(['01', agentId], {
        caseNo: doc.policyNumber || doc.id,
        displayCaseNo: doc.policyNumber || doc.id,
        product: productName,
        productName: productName,
        agentId: agentId,
        agentName: doc.quotation.agent.name,
        submittedDate: doc.applicationStartedDate,
        proposerName: doc && doc.quotation && doc.quotation.pFullName,
        customerICNo: customerICNo,
        applicationId: doc.id,
        policyId: doc.policyNumber || doc.id,
        lifeAssuredName: doc && doc.quotation && doc.quotation.iFullName,
        isShield: isShield
      });
    }
  },

  quotationByAgent: function (doc) {
    if (doc && doc.type === 'quotation') {
      var agentId = doc.agent.agentCode;
      var productName;
      var isShield = doc.quotType === 'SHIELD';
      if (doc && doc.plans && doc.plans[0] && doc.plans[0].covName && !isShield){
        productName = doc.plans[0].covName;
      } else if (doc && doc.baseProductName && isShield){
        doc.baseProductName.en =  `${doc.baseProductName.en} Plan`;
        productName = doc.baseProductName;
      }
      // eslint-disable-next-line no-undef
      emit(['01', agentId], {
        caseNo: doc.id,
        displayCaseNo: doc.id,
        product: productName,
        agentId: agentId,
        agentName: doc.agent.name,
        submittedDate: doc.lastUpdateDate,
        proposerName: doc.pFullName
      });
    }
  },

  appByPolNum: function (doc) {
    if (doc.type === 'application') {
      if (doc.policyNumber) {
        // eslint-disable-next-line no-undef
        emit(['01', doc.policyNumber], doc);
      }
    }
  },

  managerDownline: function (doc) {
    if (doc && doc.type === 'agent') {
      var emitObj = {
        agentCode: doc.agentCode
      };
      // eslint-disable-next-line no-undef
      emit(['01', doc.managerCode], emitObj);
    }
  },

  directorDownline: function (doc) {
    if (doc && doc.type === 'agent') {
      var directorCode = doc.rawData && doc.rawData.upline2Code;
      if (directorCode) {
        var emitObj = {
          agentCode: doc.agentCode
        };
        // eslint-disable-next-line no-undef
        emit(['01', directorCode], emitObj);
      }
    }
  },

  validBundleById: function (doc) {
    if (doc && doc.type === 'bundle' && doc.isValid) {
      // eslint-disable-next-line no-undef
        emit(['01', doc.id], {
          id:doc.id,
          fna: doc.fna
        });
    }
  },

  naById: function (doc) {
    if (doc && doc.type === 'na') {
      // eslint-disable-next-line no-undef
      emit(['01', doc.id], {
        id:doc.id,
        productType: doc.productType,
        ckaSection: doc.ckaSection
      });
    }
  },

  quotationByBaseProductCode: function(doc) {
    if (doc && doc.type === 'quotation') {
      // eslint-disable-next-line no-undef
      emit(['01', doc.baseProductCode], doc);
    }
  },

  quotationCampaign: function(doc) {
    if (doc && doc.type === 'campaign') {
      var longUTCCampaignEndTime = Date.parse(doc.endTime);
      // eslint-disable-next-line no-undef
      emit(['01', 'endTime', longUTCCampaignEndTime], doc);
      // eslint-disable-next-line no-undef
      emit(['01', 'campaignId', doc.campaignId], doc);
    }
  },

  systemNotification: function(doc) {
    if (doc && doc.type === 'campaign') {
      var longUTCMessageEndTime = Date.parse(doc.endTime);
      var emitObj = {
        startTime: doc.startTime,
        endTime: doc.endTime,
        messageId: doc.messageId,
        messagePriorityInAlertBox: doc.messagePriorityInAlertBox,
        messageGroup: doc.messageGroup,
        groupPriority: doc.groupPriority,
        messageMapping: doc.messageMapping,
        message: doc.message,
        groupTitle: doc.groupTitle,
        messageDealerGroups: doc.messageDealerGroups
      };
      // eslint-disable-next-line no-undef
      emit(['01', 'endTime', longUTCMessageEndTime], emitObj);
      // eslint-disable-next-line no-undef
      emit(['01', 'messageId', doc.messageId], emitObj);
    }
  }
};
