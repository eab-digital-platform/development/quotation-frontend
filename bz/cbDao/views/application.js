module.exports = {
    applicationByQuotationId: function(doc) {
        if (doc && doc.type === 'application') {
            var emitObj = {
                id: doc.id,
                bundleId: doc.bundleId,
                quotationId: doc.quotationDocId,
                policyNumber: doc.policyNumber
            };
            // eslint-disable-next-line no-undef
            emit(['01', doc.quotationDocId], emitObj);
        }
    },
    // For SPE scan on iOS, we create this "getAttachments" view for filtering the documents which has supporting document attachment(s)
    getAttachments: function (doc, meta) {
        if (doc && doc.type === 'application' && doc.supportDocuments) {
            var attachmentIds = [];
            // Insured || policyForm || proposer
            for (var docTypeKey in doc.supportDocuments.values) {
                var docType = doc.supportDocuments.values[docTypeKey];
                for (var docObjKey in docType) {
                    var docObj = docType[docObjKey];
                    for (var docKey in docObj) {
                        var docArray = docObj[docKey];
                        // OptionalDocs
                        if (!Array.isArray(docArray)) {
                            // console.log(docArray);
                            for (var inner in docArray) {
                                // console.log(Array.isArray(docArray[inner]));
                                // ? Assumed docArray[inner] isArray,
                                // ? MedicalRequestForm_XXXX = Array
                                // ? SupplementaryProposalForm_XXXX = Array
                                // ? inner = the Key inside otherDocs
                                // if (inner.indexOf('_')) {
                                    if (Array.isArray(docArray[inner])) {
                                    var otherDocValue = (docArray[inner]);
                                    // console.log(otherDocValue)
                                    // console.log(Object.keys(otherDocValue));
                                    for (var otherDocValueKey in otherDocValue) {
                                        // console.log(Object.keys(otherDocValue));
                                        var insideOtherDoc = otherDocValue[otherDocValueKey];
                                        if (insideOtherDoc.uploadDate) {
                                            attachmentIds.push({
                                                id: insideOtherDoc.id,
                                                contentType: insideOtherDoc.fileType,
                                                isMandatory: 'otherDoc',
                                                sectionId: docObjKey,
                                                tabId: docTypeKey,
                                                subSectionValue: inner
                                            });
                                        }
                                    }
                                }
                            }
                            continue;
                        }
                        // MandDocs
                        for (var child in docArray) {
                            var targetDoc = docArray[child];
                            if (doc.hasOwnProperty('offlineCreateDate')) {
                                if (targetDoc && (targetDoc.uploadDate || targetDoc.id.indexOf('suppDocs') > -1)) {
                                    if (!doc.hasOwnProperty('scanResult') || (doc.hasOwnProperty('scanResult') && !doc.scanResult.hasOwnProperty(targetDoc.id))) {
                                        // if (doc.hasOwnProperty("scanResult") && !doc.scanResult.hasOwnProperty(targetDoc.id)) {
                                        attachmentIds.push({
                                            id: targetDoc.id,
                                            contentType: targetDoc.fileType,
                                            isMandatory: 'mandDocs',
                                            sectionId: docObjKey,
                                            tabId: docTypeKey,
                                            subSectionValue: docKey
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (attachmentIds.length > 0) {
                // eslint-disable-next-line no-undef
                emit(['01', 'agentCode', doc.agentCode], {
                    // docId:  doc.applicationForm.values.proposer.personalInfo._id,
                    attachment: attachmentIds
                  });
            }
        }
   }
};
