module.exports = {
    documentByLstChgDate: function(doc, meta) {
        // For Partick Chan extract data by lstChgDate field (CR157)
        // In UAT env, there are some data we don't need in this view
        // Just like appid-xxx-seq.json, clientid-xxx-seq.json, U_xxx_DELETEDID.json. U_XXX_RLSSTATUS.json
        // These types of data are not exist on our SIT3 env
        // They are from AXA env, so we need also exclude them
        if (
            doc.lstChgDate && doc.type !== 'DATA_SYNC_TRX_LOG'
            && meta.id.indexOf('appid-') === -1
            && meta.id.indexOf('clientid-') === -1
            && meta.id.indexOf('_DELETEDID') === -1
            && meta.id.indexOf('_RLSSTATUS') === -1) {
                // eslint-disable-next-line no-undef
                emit(['01',
                new Date(doc.lstChgDate).getFullYear()
                + '-'
                + (new Date(doc.lstChgDate).getMonth() + 1)
                + '-'
                + (new Date(doc.lstChgDate).getDate())], doc);
        }
    },
    documentsWithoutLstChgDate: function(doc, meta) {
        // Extract history data which the lstChgDate field is not existed (CR157)
        // In UAT env, there are some data we don't need in this view
        // Just like appid-xxx-seq.json, clientid-xxx-seq.json, U_xxx_DELETEDID.json. U_XXX_RLSSTATUS.json
        // These types of data are not exist on our SIT3 env
        // They are from AXA env, so we need also exclude them
        if (
            !doc.lstChgDate && doc._id
            && meta.id.indexOf('appid-') === -1
            && meta.id.indexOf('clientid-') === -1
            && meta.id.indexOf('_DELETEDID') === -1
            && meta.id.indexOf('_RLSSTATUS') === -1) {
            // eslint-disable-next-line no-undef
            emit(['01', doc._id],doc._id);
        }
    }
};
