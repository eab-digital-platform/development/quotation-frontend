module.exports = {
  allChannelAppCases: function (doc, meta) {
    if (doc.type === 'application') {
      var quotation = doc.quotation;
      var payment = doc.payment;
      var applicationForm = doc.applicationForm;

      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;
      var applicationStartedDate = doc.applicationStartedDate;
      if (applicationStartedDate) {
        var dtApplicationStartedDate = new Date(applicationStartedDate).getTime();
        var dtApplicationSubmittedDate = null;
        if (doc.applicationSubmittedDate && new Date(doc.applicationSubmittedDate)) {
          dtApplicationSubmittedDate = new Date(doc.applicationSubmittedDate).getTime();
        }

        var proposerIc = null,
          proposerName = null,
          proposerMobileNo = null,
          proposerMobileCountryCode = null,
          proposerEmailAddress = null,
          dob = null,
          educationLevel = null,
          language = null,
          languageOther = null,
          cid = null,
          pDocType = null,
          pDocTypeOther = null,
          insDocType = null,
          insDocTypeOther = null;
        var lifeAssuredIc = null,
          lifeAssuredName = null;
        var agentName = null,
          channel = null,
          premiumFrequency = null,
          riskProfile = null,
          rop = null,
          currency = null,
          agentCode = null,
          rspAmount = null,
          rspPayFreq = null,
          productLine = null;
        var plans = null;
        var paymentMethod = null;
        var dateOfRoadshow = null,
          venue = null,
          trustedIndividualName = null,
          trustedIndividualMobileNo = null,
          trustedIndividualMobileCountryCode = null;

        var quotation = doc.quotation;
        var payment = doc.payment;
        var applicationForm = doc.applicationForm;

        var pAge = null;
        if (quotation) {
          premiumFrequency = quotation.paymentMode;
          currency = quotation.ccy;
          productLine = quotation.productLine;

          if (quotation.agent) {
            agentName = quotation.agent.name;
            agentCode = quotation.agent.agentCode;
            channel = quotation.agent.dealerGroup;
          }
          if (quotation.extraFlags && quotation.extraFlags.fna) {
            riskProfile = quotation.extraFlags.fna.riskProfile;
          }
          if (quotation.clientChoice && quotation.clientChoice.recommendation && quotation.clientChoice.recommendation.rop) {
            rop = quotation.clientChoice.recommendation.rop.choiceQ1;
          }
          if (quotation.policyOptionsDesc) {
            rspAmount = quotation.policyOptionsDesc.rspAmount;
            rspPayFreq = quotation.policyOptionsDesc.rspPayFreq;
          }

        }

        if (applicationForm && applicationForm.values) {
          var values = applicationForm.values;
          if (values.proposer) {
            var proposer = values.proposer;
            if (proposer.personalInfo) {
              var personalInfo = proposer.personalInfo;
              pDocType = personalInfo.idDocType;
              pDocTypeOther = personalInfo.idDocTypeOther;
              proposerIc = personalInfo.idCardNo;
              proposerName = personalInfo.fullName;
              proposerMobileCountryCode = personalInfo.mobileCountryCode;
              proposerMobileNo = personalInfo.mobileNo;
              proposerEmailAddress = personalInfo.email;
              dob = personalInfo.dob;
              pAge = personalInfo.pAge;
              educationLevel = personalInfo.education;
              language = personalInfo.language;
              languageOther = personalInfo.languageOther;
              cid = personalInfo.cid;
              if (personalInfo.bundle) {
                var bundle = personalInfo.bundle;
                var fnId = null;
                for (var i = 0; i < bundle.length; i++) {
                  var item = bundle[i];
                  if (item.isValid) {
                    fnId = item.id;
                  }
                }
              }
            }
            if (proposer.declaration) {
              var declaration = proposer.declaration;
              dateOfRoadshow = declaration.ROADSHOW02;
              venue = declaration.ROADSHOW03;
              if (declaration.trustedIndividuals) {
                trustedIndividualName = declaration.trustedIndividuals.fullName;
                trustedIndividualMobileNo = declaration.trustedIndividuals.mobileNo;
                trustedIndividualMobileCountryCode = declaration.trustedIndividuals.mobileCountryCode;
              }
            }
          }
          if (values.insured && values.insured[0]) {
            var insured = values.insured[0];
            if (insured.personalInfo) {
              var personalInfo = insured.personalInfo;
              lifeAssuredIc = personalInfo.idCardNo;
              lifeAssuredName = personalInfo.fullName;
              insDocType = personalInfo.idDocType;
              insDocTypeOther = personalInfo.idDocTypeOther;
            }
          }

          if (values.planDetails) {
            var planDetails = values.planDetails;
            if (!currency) {
              currency = planDetails.ccy;
            }
            if (planDetails.planList) {
              var planList = planDetails.planList;
              plans = new Array(planList.length);
              for (var i = 0; i < planList.length; i++) {
                var plan = planList[i];
                plans[i] = ({
                  covName: plan.covName,
                  sumInsured: plan.sumInsured,
                  premium: plan.premium
                });
              }
            }
          }
        }
        if (payment) {
          paymentMethod = payment.initPayMethod;
        }
        var emitObj = {
          id: doc.id,
          parentId: doc.parentId,
          policyNumber:doc.policyNumber,
          type: doc.type,
          applicationSubmittedDate: dtApplicationSubmittedDate,
          applicationStartedDate:dtApplicationStartedDate,
          lastUpdateDate: lastUpdateDate,
          premiumFrequency: premiumFrequency,
          agentName: agentName,
          currency: currency,
          channel: channel,
          riskProfile: riskProfile,
          rop: rop,
          pDocType : pDocType,
          pDocTypeOther : pDocTypeOther,
          proposerIc : proposerIc,
          proposerName : proposerName,
          proposerMobileCountryCode : proposerMobileCountryCode,
          proposerMobileNo : proposerMobileNo,
          proposerEmailAddress : proposerEmailAddress,
          dob : dob,
          educationLevel : educationLevel,
          language : language,
          pAge:pAge,
          languageOther:languageOther,
          dateOfRoadshow : dateOfRoadshow,
          venue : venue,
          trustedIndividualName : trustedIndividualName,
          trustedIndividualMobileNo : trustedIndividualMobileNo,
          trustedIndividualMobileCountryCode:trustedIndividualMobileCountryCode,
          insDocType : insDocType,
          insDocTypeOther : insDocTypeOther,
          lifeAssuredIc : lifeAssuredIc,
          lifeAssuredName : lifeAssuredName,
          plans : plans,
          paymentMethod : paymentMethod,
          cid: cid,
          fnId : fnId,
          agentCode : agentCode,
          productLine:productLine,
          rspAmount:rspAmount,
          rspPayFreq:rspPayFreq
        };

        // eslint-disable-next-line no-undef
        emit(['01', 'applicationId', doc.id], emitObj);
        // eslint-disable-next-line no-undef
        emit(['01', 'appStartDate', dtApplicationStartedDate], emitObj);

        if (dtApplicationSubmittedDate !== null) {
          // eslint-disable-next-line no-undef
          emit(['01', 'appSubmissionDate', dtApplicationSubmittedDate], emitObj);
        }


      }
    }
  },

  allChannelPolicyCases: function (doc, meta) {
    if (doc && doc.type === 'approval' && doc.approveRejectDate && doc.approveRejectDate !== '') {
      var approveRejectDate = new Date(doc.approveRejectDate).getTime();
      // eslint-disable-next-line no-undef
      emit(['01', 'approveRejectDate', approveRejectDate], {
        applicationId: doc.applicationId
      });
    }
  },

  allChannelAppCasesByProduct: function(doc, meta) {
    if (doc.type === 'application') {
      var quotation = doc.quotation;
      var payment = doc.payment;
      var applicationForm = doc.applicationForm;

      var lastUpdateDate = doc.applicationSubmittedDate || doc.applicationSignedDate || doc.applicationStartedDate || doc.lastUpdateDate;
      var applicationStartedDate = doc.applicationStartedDate;
      if (applicationStartedDate) {
        var dtApplicationStartedDate = new Date(applicationStartedDate).getTime();
        var dtApplicationSubmittedDate = null;
        if (doc.applicationSubmittedDate && new Date(doc.applicationSubmittedDate)) {
          dtApplicationSubmittedDate = new Date(doc.applicationSubmittedDate).getTime();
        }

        var proposerIc = null,
          proposerName = null,
          proposerMobileNo = null,
          proposerMobileCountryCode = null,
          proposerEmailAddress = null,
          dob = null,
          educationLevel = null,
          language = null,
          languageOther = null,
          cid = null,
          pDocType = null,
          pDocTypeOther = null,
          insDocType = null,
          insDocTypeOther = null;
        var lifeAssuredIc = null,
          lifeAssuredName = null;
        var agentName = null,
          channel = null,
          premiumFrequency = null,
          riskProfile = null,
          rop = null,
          currency = null,
          agentCode = null,
          rspAmount = null,
          rspPayFreq = null,
          productLine = null;
        var plans = null;
        var paymentMethod = null;
        var dateOfRoadshow = null,
          venue = null,
          trustedIndividualName = null,
          trustedIndividualMobileNo = null,
          trustedIndividualMobileCountryCode = null;

        var quotation = doc.quotation;
        var payment = doc.payment;
        var applicationForm = doc.applicationForm;

        var pAge = null;
        if (quotation) {
          premiumFrequency = quotation.paymentMode;
          currency = quotation.ccy;
          productLine = quotation.productLine;

          if (quotation.agent) {
            agentName = quotation.agent.name;
            agentCode = quotation.agent.agentCode;
            channel = quotation.agent.dealerGroup;
          }
          if (quotation.extraFlags && quotation.extraFlags.fna) {
            riskProfile = quotation.extraFlags.fna.riskProfile;
          }
          if (quotation.clientChoice && quotation.clientChoice.recommendation && quotation.clientChoice.recommendation.rop) {
            rop = quotation.clientChoice.recommendation.rop.choiceQ1;
          }
          if (quotation.policyOptionsDesc) {
            rspAmount = quotation.policyOptionsDesc.rspAmount;
            rspPayFreq = quotation.policyOptionsDesc.rspPayFreq;
          }

        }

        if (applicationForm && applicationForm.values) {
          var values = applicationForm.values;
          if (values.proposer) {
            var proposer = values.proposer;
            if (proposer.personalInfo) {
              var personalInfo = proposer.personalInfo;
              pDocType = personalInfo.idDocType;
              pDocTypeOther = personalInfo.idDocTypeOther;
              proposerIc = personalInfo.idCardNo;
              proposerName = personalInfo.fullName;
              proposerMobileCountryCode = personalInfo.mobileCountryCode;
              proposerMobileNo = personalInfo.mobileNo;
              proposerEmailAddress = personalInfo.email;
              dob = personalInfo.dob;
              pAge = personalInfo.pAge;
              educationLevel = personalInfo.education;
              language = personalInfo.language;
              languageOther = personalInfo.languageOther;
              cid = personalInfo.cid;
              if (personalInfo.bundle) {
                var bundle = personalInfo.bundle;
                var fnId = null;
                for (var i = 0; i < bundle.length; i++) {
                  var item = bundle[i];
                  if (item.isValid) {
                    fnId = item.id;
                  }
                }
              }
            }
            if (proposer.declaration) {
              var declaration = proposer.declaration;
              dateOfRoadshow = declaration.ROADSHOW02;
              venue = declaration.ROADSHOW03;
              if (declaration.trustedIndividuals) {
                trustedIndividualName = declaration.trustedIndividuals.fullName;
                trustedIndividualMobileNo = declaration.trustedIndividuals.mobileNo;
                trustedIndividualMobileCountryCode = declaration.trustedIndividuals.mobileCountryCode;
              }
            }
          }
          if (values.insured && values.insured[0]) {
            var insured = values.insured[0];
            if (insured.personalInfo) {
              var personalInfo = insured.personalInfo;
              lifeAssuredIc = personalInfo.idCardNo;
              lifeAssuredName = personalInfo.fullName;
              insDocType = personalInfo.idDocType;
              insDocTypeOther = personalInfo.idDocTypeOther;
            }
          }

          if (values.planDetails) {
            var planDetails = values.planDetails;
            if (!currency) {
              currency = planDetails.ccy;
            }
            if (planDetails.planList) {
              var planList = planDetails.planList;
              plans = new Array(planList.length);
              for (var i = 0; i < planList.length; i++) {
                var plan = planList[i];
                plans[i] = ({
                  covName: plan.covName,
                  sumInsured: plan.sumInsured,
                  premium: plan.premium
                });
              }
            }
          }
        }
        if (payment) {
          paymentMethod = payment.initPayMethod;
        }
        var emitObj = {
          id: doc.id,
          parentId: doc.parentId,
          policyNumber:doc.policyNumber,
          type: doc.type,
          applicationSubmittedDate: dtApplicationSubmittedDate,
          applicationStartedDate:dtApplicationStartedDate,
          lastUpdateDate: lastUpdateDate,
          premiumFrequency: premiumFrequency,
          agentName: agentName,
          currency: currency,
          channel: channel,
          riskProfile: riskProfile,
          rop: rop,
          pDocType : pDocType,
          pDocTypeOther : pDocTypeOther,
          proposerIc : proposerIc,
          proposerName : proposerName,
          proposerMobileCountryCode : proposerMobileCountryCode,
          proposerMobileNo : proposerMobileNo,
          proposerEmailAddress : proposerEmailAddress,
          dob : dob,
          educationLevel : educationLevel,
          language : language,
          pAge:pAge,
          languageOther:languageOther,
          dateOfRoadshow : dateOfRoadshow,
          venue : venue,
          trustedIndividualName : trustedIndividualName,
          trustedIndividualMobileNo : trustedIndividualMobileNo,
          trustedIndividualMobileCountryCode:trustedIndividualMobileCountryCode,
          insDocType : insDocType,
          insDocTypeOther : insDocTypeOther,
          lifeAssuredIc : lifeAssuredIc,
          lifeAssuredName : lifeAssuredName,
          plans : plans,
          paymentMethod : paymentMethod,
          cid: cid,
          fnId : fnId,
          agentCode : agentCode,
          productLine:productLine,
          rspAmount:rspAmount,
          rspPayFreq:rspPayFreq
        };
        for (var i = 0; i < emitObj.plans.length; i++) {
          if (emitObj.plans[i] && emitObj.plans[i].covName && emitObj.plans[i].covName.en === 'AXA Wealth Invest (CPF)') {
            // eslint-disable-next-line no-undef
            emit(['01', doc.id], emitObj);
          }
        }
      }
    }
  },

  agentsDetail: function (doc, meta) {
    if (doc && doc.type === 'agent') {
      var agentCode = doc.agentCode;
      var agentName = doc.name;
      var managerCode = doc.managerCode;
      var channel = doc.channel;
      var upline1Code = doc.rawData && doc.rawData.upline1Code;
      var upline2Code = doc.rawData && doc.rawData.upline2Code;
      var profileId = doc.profileId;
      var emitAgentObj = {
        agentCode,
        agentName,
        managerCode,
        channel,
        upline1Code,
        upline2Code,
        profileId
      };
      // eslint-disable-next-line no-undef
      emit(['01', 'agents'], emitAgentObj);
    }
  },

  webvsiosReport: function (doc, meta) {
    if (doc && doc.type === 'bundle') {
      var createDate = doc.createTime;
      var createTime = new Date(createDate).getTime();
      var lstChgDate = doc.lstChgDate;
      var dealerGroup = doc.dealerGroup;
      var bundleAgentCode = doc.agentCode;
      var bundleClientId = doc.pCid;
      var bundleApplications = doc.applications;
      var bundleId = doc.id;
      var emitObj = {
        createDate,
        lstChgDate,
        dealerGroup,
        agentCode: bundleAgentCode,
        clientId: bundleClientId,
        bundleApplications,
        id: bundleId
      };
      // eslint-disable-next-line no-undef
      emit(['01', 'bundle', createTime], emitObj);
    }

    if (doc && doc.type === 'quotation') {
      var quotCreateDate = doc.lastUpdateDate;
      var quotCreateTime = new Date(quotCreateDate).getTime();
      var quickQuote = doc.quickQuote;
      var baseProductName = doc.baseProductName && doc.baseProductName.en;
      var quotClientId = doc.pCid;
      var quotProposerName = doc.pFullName;
      var quotLAName = doc.iFullName;
      var quotId = doc.id;
      var emitQuotObj = {
        quotCreateDate,
        baseProductName,
        clientId: quotClientId,
        quotationDocId: quotId,
        id: quotId,
        quotProposerName,
        quotLAName
      };
      if (quickQuote) {
        // eslint-disable-next-line no-undef
        emit(['01', 'quickQuote', quotCreateTime, quotClientId], emitQuotObj);
      } else {
        // eslint-disable-next-line no-undef
        emit(['01', 'quotation', quotCreateTime], emitQuotObj);
      }
    }

    if (doc && (doc.type === 'application' || doc.type === 'masterApplication')) {
      var applicationCreateDate = doc.applicationStartedDate;
      var applicationCreateTime = new Date(applicationCreateDate).getTime();
      var applicationSubmittedDate = doc.applicationSubmittedDate;
      var inApplyingQuotCreateDate = doc.quotation.createDate;
      var inApplyingQuotbaseProductName = doc.quotation.baseProductName && doc.quotation.baseProductName.en;
      var policyNumber = doc.policyNumber;
      var applicationId = doc.id;
      var quotProposerName = doc.quotation.pFullName;
      var quotLAName = doc.quotation.iFullName;
      var iCidMapping = doc.iCidMapping;

      var emitApplicationObj = {
        applicationCreateDate,
        applicationSubmittedDate,
        quotCreateDate: inApplyingQuotCreateDate,
        baseProductName: inApplyingQuotbaseProductName,
        policyNumber,
        quotationDocId: doc.quotation.id,
        applicationDocId: applicationId,
        id: applicationId,
        quotProposerName,
        quotLAName,
        iCidMapping: iCidMapping
      };
      // eslint-disable-next-line no-undef
      emit(['01', 'application', applicationCreateTime], emitApplicationObj);
    }

    if (doc && doc.type === 'agent') {
      var agentCode = doc.agentCode;
      var agentName = doc.name;
      var managerCode = doc.managerCode;
      var channel = doc.channel;
      var upline2Code = doc.rawData && doc.rawData.upline2Code;
      var emitAgentObj = {
        agentCode,
        agentName,
        managerCode,
        channel,
        upline2Code
      };
      // eslint-disable-next-line no-undef
      emit(['01', 'agents'], emitAgentObj);
    }

    if (doc && doc.type === 'cust') {
      for (var i = 0; i < doc.bundle.length; i++) {
        var initialBundle = doc.bundle[0];
        var bundle = doc.bundle[i];
        var custBundleId = bundle.id;
        var fullName = doc.fullName;
        var emitCustBundleObj = {
          initialBundleId: initialBundle.id,
          mappedBundleId: custBundleId,
          fullName
        };
        // eslint-disable-next-line no-undef
        emit(['01', 'bundleInCustomer',  custBundleId], emitCustBundleObj);
      }
    }
  }
};
