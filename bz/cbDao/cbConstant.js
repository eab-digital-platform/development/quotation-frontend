const dao = require('../cbDaoFactory').create();
var logger = global.logger || console;
var _pick = require('lodash/fp/pick');

module.exports.getsysParameter = function(callback) {
  dao.getDocFromCacheFirst('sysParameter', function(result) {
    callback(_pick(['EAPPROVAL_EXPIRY_DATE_FR_SUBMISSION'], result));
  });
};

