const fs = require('fs');
const _ = require('lodash');
const request = require('request-promise');
var { callApi } = require('./utils/RemoteUtils.js');
var logger = global.logger || console;

const SGTIMEZONE = 8;
const monthOfDate = [
  'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
];

let toNumber = function(vl){
  if (!vl) {
    return 0;
  } else {
    if (vl === '') {
      return 0;
    } else if (Number(vl)) {
      return Number(vl);
    } else {
      return 0;
    }
  }
};
module.exports.toNumber = toNumber;

var base64ToFile = function(data, file, cb) {
  //logger.log("DEBUG: base64ToFile", data);
  fs.writeFile(file, data, {encoding: 'base64'}, function(err) {
    if (err) {
      return logger.log(err);
    }
    cb();
	});
};
module.exports.base64ToFile = base64ToFile;

var fileToBase64 = function(file, cb) {
  fs.readFile(file, function read(err, data) {
    if (err) {
      throw err;
    }
    cb(Buffer(data).toString('base64'));
	});
};
module.exports.fileToBase64 = fileToBase64;

module.exports.protectBase64Pdf = function (pdf, password, callback) {
  const result = global.JavaRunner.setPdfPassword(pdf, password);
  callback && callback(result);
  return result;
};

module.exports.addWordsPdfs = function (pdf, agentName, proposerName, x1, y1, x2, y2) { 
  return global.JavaRunner.addWordsPdfs(pdf, agentName, proposerName, x1, y1, x2, y2);
};


module.exports.convertHtml2Pdf = function(html, pdfOptions, callback){
//  callback(global.JavaRunner.convertHtml2Pdf(html, pdfOptions));
  var reqJson = {
    html: html
  }
  var path = '/pdf/html2pdf';
  var startTime = new Date();
  var dataProcess = function(parsedBody) {
    var elapsedTimeMs = new Date() - startTime;
    logger.log('callApi ' + path + ' >> API_ID: ' + parsedBody.id + ', Elapsed ' + elapsedTimeMs + 'ms');
    for (var key in parsedBody) {
      if (key !== 'id') {
        callback(parsedBody[key]);
      }
    }
  };
  callApi(path, reqJson, dataProcess);
};

module.exports.mergePdfs = function (pdfs, callback) {
  if (!pdfs || pdfs.length <= 1) {
    callback(pdfs && pdfs[0]);
  }
  let base64Pdfs = _.filter(pdfs, pdf => pdf);
  callback(global.JavaRunner.mergePdfs(base64Pdfs));
};

module.exports.addPdfTitle = (pdf, title) => {
  return global.JavaRunner.addPdfTitle(pdf, title);
};

module.exports.convertBase64Imgs2Pdf = (base64Imgs) => {
  return global.JavaRunner.convertBase64Imgs2Pdf(base64Imgs);
};

module.exports.genName = function(lang, fName, lName){
	switch (lang){
		case 'en':
			return  fName + ' ' + lName;
		case 'hant':
			return 	lName + ' ' + fName;
		default:
			return 	lName + ' ' + fName;
	}
};

module.exports.getNameKey = function(lang, covNames){
	var res = 'en';
	for (let key in covNames) {
    if (key.toLowerCase() === lang.toLowerCase()){
      res = key;
    }
	}
	return res;
};

module.exports.getSysDate = function(){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  if (mm < 10) {
    mm = '0' + mm;
  }
  var yyyy = today.getFullYear();
  var sysDate = dd + '/' + mm + '/' + yyyy;

  return sysDate;
};

let getSGTimeZone = function() {
  return SGTIMEZONE + (new Date()).getTimezoneOffset() / 60;
};

module.exports.getLocalTime = function(){
  return new Date((new Date()).getTime() + getSGTimeZone() * 3600 * 1000);
};

module.exports.getFormattedDate = function(timestamp, dateFormat) {
  var d = new Date(new Date(timestamp).getTime() + getSGTimeZone() * 3600 * 1000);
  var dd = d.getDate();
  var mm = d.getMonth() + 1; //January is 0!
  var MMMM = monthOfDate[d.getMonth()];
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }
  var yyyy = d.getFullYear();

  if (dateFormat) {
    var formats = dateFormat.split('/');
    var dateStr = '';
    for (var i = 0; i < formats.length; i ++) {
      if (formats[i] === 'dd') {
        dateStr += dd;
      } else if (formats[i] === 'mm') {
        dateStr += mm;
      } else if (formats[i] === 'yyyy') {
        dateStr += yyyy;
      } else if (formats[i] === 'MMMM') {
        dateStr += MMMM;
      }

      if (i < formats.length - 1) {
        dateStr += '/';
      }
    }

    if (dateStr.length > 0) {
      return dateStr;
    }
  }

  return dd + '/' + mm + '/' + yyyy;
};

module.exports.getFormattedDateTime = function(timestamp) {
  var d = new Date(new Date(timestamp).getTime() + getSGTimeZone() * 3600 * 1000);
  var dd = d.getDate();
  var MM = d.getMonth() + 1; //January is 0!
  var hh = d.getHours();
  var mm = d.getMinutes();
  var ss = d.getSeconds();

  if (dd < 10) {
    dd = '0' + dd;
  }
  if (MM < 10) {
    MM = '0' + MM;
  }
  var yyyy = d.getFullYear();
  if (hh < 10) {
    hh = '0' + hh;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }
  if (ss < 10) {
    ss = '0' + ss;
  }

  return dd + '/' + MM + '/' + yyyy + ' ' + hh + ':' + mm + ':' + ss;
};

module.exports.base64MimeType = function(encodedString) {
  var result = null;

  if (typeof encodedString !== 'string') {
    return result;
  }

  var mime = encodedString.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

  if (mime && mime.length) {
    result = mime[1];
  }

  return result;
};

var callRestAPI = function(method, uri, body, callback) {
  var options = {
    method: method,
    uri: uri,
    body: body,
    json: true
  };

  var startTime = new Date();
  request(options)
    .then(function (parsedBody){
      var elapsedTimeMs = new Date() - startTime;
      logger.log("callRestAPI: Return_ID: " + parsedBody.id + ": Elapsed " + elapsedTimeMs + "ms");
      // find the only result which is not ID
      for (var key in parsedBody) {
        if (key != 'id') {
          callback(parsedBody[key]);
          break;
        }
      }
    })
    .catch(function (err) {
      logger.error("HTML to PDF Error: ", err);
    });
};

module.exports.callRestAPI = callRestAPI;
