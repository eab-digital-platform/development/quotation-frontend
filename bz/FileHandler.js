var fs = require('fs');
var encryptor = require('file-encryptor');

var key = "AAbb1122";
var options = { algorithm : 'aes256' };
var logger = global.logger || console;
var encryptFileSubName = "enc";
const auditFuncs = require('./AuditHandler');
var { callApiByGet } = require('./utils/RemoteUtils.js');
var { callApi } = require('./utils/RemoteUtils.js');

var cDao = require("./cbDao/file.js");
var {
  getFromRedis,
  setToRedis
} = require("./utils/CommonUtils");

const uuidv1 = require('uuid/v1');

module.exports.getAttachment = function(data, session, cb){
  cDao.getAttachment(data.docId, data.attId, function(attachment){
    cb(attachment)
  })
}

module.exports.getBinaryAttachment = function(data, session, cb){
  cDao.getBinaryAttachment(data.docId, data.attId, function(attachment){
    cb(attachment)
  })
}

module.exports.getAttachmentPdfByToken = function(data, session, cb){
  logger.log('INFO: getAttachmentPdfByToken - start', data.token);
  getFromRedis("attachmentToken", (tokens)=>{
    if (tokens) {
      var value = tokens[data.token];
      logger.debug('DEBUG: getAttachmentPdfByToken value:', value);
      if (value) {
        const expiryTime = value.expiryTime;
        const now = Date.now();
        if (now < expiryTime) {
          auditFuncs.accessLog(session, 'getAttachmentPdfByToken', value);
          cDao.getAttachmentByToken(value, cb);
        } else {
          logger.error('ERROR: getAttachmentPdfByToken - Invalid Token', data.token);

          cb({error: "invalid token"});
        }
        delete tokens[data.token];
        setToRedis("attachmentToken", tokens);
      } else {
        logger.error('ERROR: getAttachmentPdfByToken - No value in redis tokens', data.token);
        cb({error: "invalid token"});
      }
    } else {
      logger.error('ERROR: getAttachmentPdfByToken - No tokens from redis', data.token);
      cb({error: "invalid token"});
    }
  })
}

module.exports.getAttachmentContentType = function (data, session, cb) {
  cDao.getAttachmentContentType(data.docId, data.attId, cb);
};

module.exports.readFile = function(path, cb){
  logger.log("file handle : readFile");
  decryptFile(path, function(){
    try{
      var bitmap = fs.readFileSync(path);
      var pdf = Buffer(bitmap).toString('base64');
    }catch(err){
      var pdf = null;
      logger.log("readFileInBase64 err", err)
    }
    removeOriginalFile(path);
    cb(pdf);
  })

}


module.exports.saveFile = function(name, cb){
  logger.log("file handle : saveFile: ", name)
  var input = name;
  var output = name + encryptFileSubName;

  encryptor.encryptFile(input, output, key, options, function(err) {
    if (err)
        logger.log("err:", err);
    else{
      fs.unlinkSync(input);
    }
    if(cb)
      cb();

    logger.log("Successfully Encrypted!");
  });
}


var decryptFile = function(path, cb){
  logger.log("file handle : decryptFile")
  try{
    var output = path;
    var input = path + encryptFileSubName;
    encryptor.decryptFile(input, output, key, options, function(err) {
      if (err)
          throw err;
      logger.log("Successfully Decrypted!");
      cb();
    });
  }catch(errr){
    cb();
  }
}
module.exports.decryptFile = decryptFile;

var removeOriginalFile = function(path){
  logger.log("file handle : removeOriginalFile")
  try{
    fs.unlinkSync(path);
  }catch(err){
    logger.log("DEBUG: removeOriginalFile", err )
  }

}
module.exports.removeOriginalFile = removeOriginalFile;


var removeEncryptedFile = function(path){
  logger.log("file handle : removeEncryptedFile")
  path = path + encryptFileSubName;

  try{
    fs.unlinkSync(path);
  }catch(err){
    logger.log("DEBUG: removeEncryptedFile", err )
  }
}
module.exports.removeEncryptedFile = removeEncryptedFile;

var removeFiles = function(path){
  logger.log("file handle : remove")
  removeOriginalFile(path);
  removeEncryptedFile(path);
}
module.exports.removeFiles = removeFiles;

var downloadIPA = function (data,session,callback) {
  
  callApiByGet('/retrieve/url', (res) => {
      callback(res);
  });
}
module.exports.downloadIPA = downloadIPA;

var createToken = function (data,session,callback) {
  //createtoken
  const token = uuidv1();
  const timestamp = {
    currentTime : new Date()
  };

  global.redisClient.set(token, JSON.stringify(timestamp), 'EX', 900);
  var url = '/downloadIPA/' + token;
  callback({url:url});
}

module.exports.createToken = createToken;
