"use strict"

const _ = require('lodash');
const CryptoJS = require("crypto-js");
const favicon = require('serve-favicon');
if (global.NODE_ENV == 'production') {
  require('./favicon.ico');
}
const cbMgr = require('./cbDao/couchbaseMgr');

const authFuncs = require('./AuthHandler');
const agentFuncs = require('./AgentHandler');
const clientFuncs = require('./ClientHandler');
const downlaodMaterialFuncs = require('./DownloadMaterialHandler');
const prodFuncs = require('./handler/ProductHandler');
const quotFuncs = require('./handler/QuotationHandler');
const needsFuncs = require('./NeedsHandler');
const auditFuncs = require('./AuditHandler');
const fileFuncs = require('./FileHandler');
const applicationFuncs = require('./handler/ApplicationHandler');
const shieldApplicationFuncs = require('./handler/application/shield/index');
const clientChoiceFuncs = require('./handler/ClientChoiceHandler');
const emailFuncs = require('./EmailHandler');
const smsFuncs = require('./SMSHandler');
const approvalFuncs = require('./ApprovalHandler');
const approveNotifyFuncs = require('./ApprovalNotificationHandler');
const viewFuncs = require('./ViewHandler');
const commFuncs = require('../common/CommonUtils');
const sysNotifyFuncs = require('./handler/SystemNotificationHandler');

var {
  setToRedis,
  getFromRedis,
  escapeHtml
} = require('./utils/CommonUtils');

const { callApiByGet } = require('./utils/RemoteUtils');

const {execInitServerJobs} = require('./jobs/index');
var RemoteUtils = require('./utils/RemoteUtils');
var logger = global.logger || console;

require('../app/utils/DateFormater.js');
const rsa = require('node-rsa');

var handlers = {
  auth: authFuncs,
  agent: agentFuncs,
  client: clientFuncs,
  product: prodFuncs,
  quot: quotFuncs,
  needs: needsFuncs,
  application:applicationFuncs,
  shieldApplication:shieldApplicationFuncs,
  clientChoice: clientChoiceFuncs,
  approval: approvalFuncs,
  approvalNotification: approveNotifyFuncs,
  email: emailFuncs,
  sms: smsFuncs,
  file: fileFuncs,
  view: viewFuncs,
  systemNofication: sysNotifyFuncs,
  downloadMaterial: downlaodMaterialFuncs
};

module.exports.initServer = function(cb) {
  // initial couchbase lite and create views

  var JavaRunner = require('./JavaRunner');
  JavaRunner = JavaRunner.default || JavaRunner;
  if (global && !global.JavaRunner) {
    global.JavaRunner = new JavaRunner();
  }

  try {
    cbMgr.init(function(resp) {
      logger.log('Couchbase initial result:', resp.success);
      if (!resp || !resp.success) {
        cb(false);
      } else {
        execInitServerJobs().then(() => {
          cb(true);
        }).catch((err) => {
          logger.error('Failed to execute init server jobs\n', err);
          cb(false);
        });
      }
    });
  } catch(ex) {
    logger.error("Failure to access couchbase server. Server startup failure.", ex);
    cb(false);
  }
}

module.exports.exportViews = function(cb) {
  //return cbMgr.exportViewsJson()
  return cbMgr.generateAllViewJsons();
}

module.exports.callApiByGet = (url, callback) => {
  return callApiByGet(url, callback);
};

module.exports.escapeHtml = (html) => {
  return escapeHtml(html);
};

var allowExpireActions =['login', 'skipLogin', 'createClient', 'initial', 'changeLang'
  //, 'resetPassword', 'resetPasswordRequest', 'initReset'
];

// check referer
var isValidRequest = function(req, cb) {
  if (req.body.action == "login" || req.body.action == "initial") {
    req.session.userIp = req.connection.remoteAddress;
    cb(true);
    return true;
  } else {
    cb(true, null);
  }
  // } else if (req.session.agent && req.params && req.params.path && req.body && req.body.action) {
  //   // these 2 lines are for allowing concurrent login
  //   if (global.config.allowMultipleLogin) {
  //     cb(true);
  //     return true;
  //   }

  //   // check the user pool for duplicate user login
  //   getFromRedis("userPool", (data)=> {
  //     //logger.log('validate request: user pool:',req.session.loginToken, data);
  //     if (data) {
  //       let now = new Date().getTime();
  //       let validTime = now - global.config.defaultRedisValidTime;
  //       let agentId = req.session.agent.profileId;
  //       let valid = false;
  //       let error = "";
  //       let agentLoginSessionIndex = -1;
  //       if (data[agentId]) {
  //         _.each(data[agentId],(agentLoginSession,index)=>{
  //           if (req.session.loginToken === agentLoginSession.token ){
  //             if (agentLoginSession.time > validTime){
  //               valid = true;
  //             }
  //             agentLoginSessionIndex = index;
  //             return false;
  //           }
  //         });

  //         if (valid && agentLoginSessionIndex > -1) {
  //           data[agentId][agentLoginSessionIndex].time = now;
  //           setToRedis("userPool", data);
  //         }
  //         else if (agentLoginSessionIndex > -1 && data[agentId][agentLoginSessionIndex].time < validTime ) {
  //           _.remove(data[agentId], function (item) {
  //             return item.time < validTime;
  //           });
  //           setToRedis("userPool", data);
  //         }
  //         else {
  //           if (valid) {
  //             console.error("Token expired");
  //             error = "error.invaid_request"
  //           } else {
  //             console.error("Duplicate login");
  //             error = "error.duplicate_login"
  //           }
  //         }
  //       }
  //       cb(valid, error)
  //     } else {
  //       cb(false);
  //     }
  //   })
  // } else {
  //   cb(false, "error.invaid_request");
  // }
  // return false;
}

//var hiddenFields = ['iCid', 'pCid', 'quotationDocId', 'cnaModelDocId', 'clientId', 'profileId'];
var hiddenFields = [];
var removeAndMaskSensitiveField = function(data) {
  var tData = _.cloneDeep(data);

  var doRemove = function (obj) {
    if (obj && typeof obj == 'object') {
      if (obj instanceof Array) {
        for (var kk in obj) {
          doRemove(obj[kk]);
        }
      } else {
        for (var kk in obj) {
          if (kk.indexOf('_') === 0) {
            obj[kk] = undefined
          } else if (hiddenFields.indexOf(kk) >= 0) {
            obj[kk] = undefined;
          } else {
            doRemove(obj[kk]);
          }
        }
      }
    }
  }

  // var tData = {};
  for (var key in tData) {
    var sd = tData[key];
    if (sd && typeof sd == 'object') {
      // remove couchbase preserve fields
      doRemove(sd);
    }
  }
  return tData;
}

module.exports.initApprovalPage = function(req, resp) {
  logger.log('INFO: init Approval page:', req.cookies, req.sessionID);
  req.session.goToApproval = true;
  resp.redirect("/");
}

module.exports.initApp = function(req, resp) {
  logger.log('INFO: init App:', req.cookies, req.sessionID);
  var rsaKey = null;
  if (global.config.enableRSA) {
    rsaKey = new rsa({b: 1024});
    req.session.RSA = rsaKey.exportKey('private');
  }
  var lang = req.cookies.lang != 'undefined' ? req.cookies.lang : 'en';

  resp.cookie('lang', lang, {
    httpOnly: true,
    secure: true
  });

  var data = {
    success: true,
    lang: lang,
    sid: req.session.id,
    csrfToken: req.csrfToken && req.csrfToken(),
    saml: global.config.saml && global.config.saml.host,
    // samlSignOut: global.config.saml && global.config.saml.signOut,
    user: !!req.session.samlUser,
    version: global.config.version,
    build: global.config.build,
    yekasrs: rsaKey && rsaKey.exportKey('public')
  }

  // logger.log('INFO: Init clientside:', data);
  auditFuncs.accessLog(req.session, 'initApp');

  resp.write(JSON.stringify(data));
  resp.end();
}

module.exports.getFavicon = function() {
  return favicon(global.MODE == 'PRO'?'prod/favicon.ico':'bz/favicon.ico');
}

module.exports.logout = function(req, resp) {
  let easeTimeInitialized;
  if (req && req.url) {
    easeTimeInitialized = req.url.substr(1 + req.url.lastIndexOf('=') || 0, req.url.length);
  }
  authFuncs.logout({
    easeTimeInitialized
  }, req.session);
  try {
    if (_.isFunction(req.logout)) {
      req.logout();
    }
    if (_.isFunction(req.session.destroy)) {
      req.session.destroy(function(error){
        resp.redirect((global.config.saml && global.config.saml.signOut) || '/');
      });
    }
  } catch (e) {
    logger.error(`ERROR ::: Logout :::: ${e}`);
  }
}

module.exports.paymentResult = function(req, resp) {
  resp.writeHeader(200, {"Content-Type": "text/html"});
  resp.write(
    `<!DOCTYPE html>
    <html>
      <head>
        <script type="text/javascript">
          var ready = function() {
            window.close();
          }
        </script>
      </head>
      <body onload="ready()">
      </body>
    </html>`
  );
  resp.end();
};

module.exports.mobilePaymentResult = function(req, resp) {
  resp.writeHeader(200, {'Content-Type': 'text/html'});
  resp.write(
    `<!DOCTYPE html>
    <html>
      <head>
        <script type="text/javascript">
          var notifyWebView = function() {
            window.postMessage("closeWebView");
          }
        </script>
      </head>
      <body>
        <div style="text-align: center">Please click <a href="#close" onclick="return notifyWebView();">Close</a> to continue.</div>
      </body>
    </html>`
  );
  resp.end();
};

module.exports.getSignDocResult = function(req, resp) {
  logger.log("get SignDoc result", req.query);
  resp.writeHeader(200, {"Content-Type": "text/html"});
  resp.write('<html><head></head><body></body></html>');
  //resp.write('<html><head><script>document.domain="'+global.config.host.domain+'"</script></head><body></body></html>');
  resp.write('<html><head><script>document.domain="signdoc.eab.com"</script></head><body></body></html>');
  resp.end();
}

// module.exports.getAttachment = function(req, resp) {
//   logger.log("get attachment");
//   resp.write(JSON.string({success: true}));
//   resp.end();
// }

var sendResp = function(req, resp, respData) {
  resp.setHeader('content-type', 'text/plain');
  respData.csrfToken = req.csrfToken && req.csrfToken(); // create new csrf token
  // logger.log("respData.csrfToken", respData.csrfToken);

  var dataStr = "";
  respData = removeAndMaskSensitiveField(respData);

  if (req.session.skey) {
    let signature = commFuncs.getStrSignature(respData, req.session.skey);
    respData = {
      p0: signature,
      p1: respData
    }
  }

  // support RSA
  if (req.session && req.session.clientRSA) {
    const rsa = require('node-rsa');
    var crsa = new rsa()
    crsa.importKey(req.session.clientRSA);
    dataStr = crsa.encrypt(respData, 'base64');
  } else {
    logger.log("no session client rsa");
    dataStr = JSON.stringify(respData);
  }

  // , function(){
  resp.write(dataStr);
  resp.end();
  // });
};

module.exports.handleRequest = function(req, resp, next) {
  // for RSA support
  // if (req.session.RSA && typeof req.body.d0 == 'string') {
//     try {
//       const rsa = require('node-rsa');
//       var srsa = new rsa();
//       srsa.importKey(req.session.RSA, 'private');
// //      req.body = srsa.decrypt(req.body.d0, 'json');

//       var aeskey = srsa.decrypt(req.body.dk, 'utf8');
//       var dataLength = Math.round((req.body.d0.length / 1048576) * Math.pow(10, 4)) / Math.pow(10, 4);    // [Test] HTTP Body Size to MB
//       var startTime = new Date();                                                                         // [Test] Timer - Start
//       var decrypted = RemoteUtils.aesDecrypt(req.body.d0, aeskey);
//       var elapsedTimeMs = new Date() - startTime;                                                         // [Test] Timer - End
//       logger.log("DECRYPT: Size " + dataLength + "MB, Elapsed " + elapsedTimeMs + "ms");                  // [Test] Result
//       req.body = JSON.parse(decrypted);
//     } catch (e) {
//       // probably incorrect key, throw to logout
//       logger.error("ERROR: Missing encrypt key. Failure to handle request.");
//       Error(e);
//       req.body = null;
//     }
  // } else {
  //   logger.log('RSA?', req.session.RSA)
  // }

  // log the incoming request
  let bodyStr = JSON.stringify(req.body);
  if (bodyStr.length > 100) {
    bodyStr = bodyStr.substr(0, 100) + '...';
  } else {
    bodyStr = req.body;
  }
  logger.log('INFO: Handle request:', req.sessionID, req.path, bodyStr);

  isValidRequest(req, (valid, message)=>{
    if (req.body.action !== "login" && req.body.action !== "logout") {
      auditFuncs.accessLog(req.session, req.body.action, {
        valid,
        message
      });
    }

    if (valid) {
      var handler = handlers[req.params.path];
      if (handler) {
        var func = handler[req.body.action];
        if (func && typeof func == 'function') {
          //check if session expired
          var ix = allowExpireActions.indexOf(req.body.action);

          req.body.host = req.get('host');
          func(req.body, req.session, function(data) {
            // destroy session when logout
            if (data) {

              logger.log('INFO: Action complete:', req.sessionID, req.path, req.body.action, 'success:', data.success);

              // return the session id for socket connection if login success
              if (data.loginSuccess == 'Y') {
                //regenerate a new session
                var oldSessionData = req.session;
                req.session.regenerate(function (err) {
                  // var sid = req.session.id;
                  data.sid = req.session.id;

                  for (let k in oldSessionData) {
                    var value = oldSessionData[k];
                    if (typeof(value) != 'function') {
                      req.session[k] = value;
                    }
                  }
                  sendResp(req, resp, data);
                });

              } else {
                if (req.body.action == 'changeLang') {
                  resp.cookie('lang', req.body.lang, {
                    httpOnly: true,
                    secure: true // true for https
                  });
                }
                req.session.touch();
                sendResp(req, resp, data);
              }
            } else {
              logger.log('WARNING: not data return:', req.params.path, req.body.action)
              resp.end();
            }
          });
        }
      }
    } else {
      logger.log("WARNING: Invalid action, going to Logout: ", _.get(req, "params.path"), _.get(req,"body.action"));
      req.session.regenerate(function (err) {
        sendResp(req, resp, {
          logout: true,
          error: message || ""
        })
      });
    }
  })
};

module.exports.handleSocketMsg = function(path, data, sess, ws) {
  logger.log('call socket:', path, 'data:',!!data, 'session:', !!sess);
  var handler = handlers[path];
  if (handler) {
    var func = handler[data.action];
    if (typeof func == 'function') {
      logger.log('call function :', data.action);
      func(data, sess, function(resp) {
        logger.log('handleSocket return :', !!resp);
      // ws.send(JSON.stringify(resp), {maskted: true, isObj: true})
        // ws.send(resp, {maskted: true, isObj: true})
        ws.emit('message', resp)
      });
    } else {
      logger.log('call function  fail:');
      ws.emit('message', {
        error
      })
    }
  }
};

module.exports.getAttachment = function(req, resp, next){
  var docId = req.params.docId;
  var attId = req.params.attId;
  if(typeof fileFuncs.getAttachment == "function"){
    fileFuncs.getAttachment(req.params, req.session, function(data){
      resp.setHeader('content-type', 'text/json');
      resp.write(JSON.stringify(data));
      // logger.log('next response:', resp);
      resp.end();
    })
    return;
  }
  next();
};

const getAttachmentByToken = (req, resp, next, filename) => {
  if (typeof fileFuncs.getAttachmentPdfByToken === 'function'){
    let allData = [];
    let allDataLength = 0;
    fileFuncs.getAttachmentPdfByToken(req.params, req.session, function(res){
      if (res && res.data && !res.error) {
        allData.push(res.data);
        allDataLength += res.data.length;
      } else if (res && res.error) {
        logger.error('fileFuncs.getAttachmentPdfByToken data:', res.error);
        auditFuncs.accessLog(req.session, 'getAttachmentPdfByToken', res);

        resp.removeHeader('content-disposition');
        resp.setHeader('content-type', 'text/html');
        resp.sendStatus(403);
      } else {
        resp.setHeader('content-type', res.contentType);
        if (_.includes(res.contentType, 'image')) {
          resp.setHeader('content-length', allDataLength);
        }

        _.forEach(allData, (data) => {
          resp.write(data);
        });
        resp.end();
      }
    });
    return;
  }
  next();
};
module.exports.getAttachmentByToken = getAttachmentByToken;

module.exports.getAttachmentPdfByToken = function(req, resp, next){
  if (typeof fileFuncs.getAttachmentPdfByToken === 'function') {
    resp.setHeader('content-disposition', 'attachment; filename="attach.pdf"');
    getAttachmentByToken(req, resp, next, 'attach.pdf');
  }
};

// module.exports.getAttachmentData = function (req, resp, next) {
//   if (typeof fileFuncs.getAttachmentContentType === 'function') {
//     fileFuncs.getAttachmentContentType(req.params, req.session, function (contentType) {
//       resp.setHeader('content-type', contentType);
//       fileFuncs.getAttachmentPdf(req.params, req.session, function (data) {
//         if (data) {
//           resp.write(data);
//         } else {
//           resp.end();
//         }
//       });
//     });
//   } else {
//     next();
//   }
// };

module.exports.getBinaryAttachment = function (req, resp, next) {
  let params = null;
  let filename = null;
  // for RSA support
  try {
    if (req.session.RSA && req.session.agent && req.session.loginToken && typeof req.params.token == 'string') {
      // let iterations = 1000, keySize = 64, ivSize = 32;
      // let salt = CryptoJS.enc.Utf8.parse(global.config.build);
      // let output = CryptoJS.PBKDF2(global.config.version, salt, {
      //     keySize: (keySize+ivSize)/32,
      //     iterations: iterations
      // });
      // output.clamp();
      // let key = CryptoJS.lib.WordArray.create(output.words.slice(0, keySize/32));
      // let iv = CryptoJS.lib.WordArray.create(output.words.slice(keySize/32));

      var hashStr = CryptoJS.SHA256(global.config.version + global.config.build);
      hashStr = hashStr.toString(CryptoJS.enc.Hex);
      var key = hashStr.substr(5, 32);
      var iv = hashStr.substr(41, 16);

      // let temp = CryptoJS.AES.decrypt(req.params.token, key, {iv: iv});
      // temp = temp.toString(CryptoJS.enc.Utf8);

      // As per AXA IT request, making encrypted value static for disable no-cache (refer to /app/utils/ServerUtils.js)
      var crypto = require('crypto');
      var decipher = crypto.createDecipher('aes256', hashStr);
      let temp = decipher.update(req.params.token, 'hex', 'utf8');
      temp += decipher.final('utf8');
      logger.debug('CB-REQ: file = ' + temp);

      let paramArr = temp.split(',');
      if (paramArr.length >= 2) {
        params = {
          docId: paramArr[0],
          attId: paramArr[1]
        }
      }
      if (paramArr.length >= 3) {
        filename = paramArr[2];
      }
    } else {
      // params = req.param
      logger.log('ERROR: Invalid para getting binary attachment:', req.params, !!req.session.RSA, req.session);
    }
  } catch(e) {
    logger.log('ERROR: Invalid getting binary attachment', req.params.token, e);
  }

  if (params && typeof fileFuncs.getAttachmentContentType === 'function') {
    fileFuncs.getAttachmentContentType(params, req.session, function (contentType) {
      resp.setHeader('content-type', contentType);
      if (filename) {
        resp.setHeader('content-disposition', 'inline; filename="' + filename + '"');
      }
      fileFuncs.getBinaryAttachment(params, req.session, function (data) {
        if (data && !data.error) {
          resp.write(data);
        } else {
          resp.end();
        }
      });
    });
  } else {
    next();
  }
};

module.exports.setAttachmentSigned = function(req, resp, next) {
  logger.log('INFO: Receive POST request from SignDoc:', req.body.docid);
  // logger.log('req.body=', Object.keys(req.body));

  var handler = handlers["application"];
  if (handler) {
    var func = handler["getSignedPdfFromSignDocPost"];
    if (func && typeof func == 'function') {
      func(req.body.docid, req.body.docfile, req.body.tiffcopy, function(data) {
        logger.log('INFO: Response to SignDoc : success:', data.success, data.id);
        if (data.success) {
          resp.status(200);
          resp.send('OK');
        } else {
          resp.status(403);
          resp.send('Fail to handle PDF', data);
        }
      })
    } else {
      next();
    }
  } else {
    next();
  }
}

module.exports.downloadIPA = function (req, resp, next) {
  fileFuncs.downloadIPA(data => {
    resp.redirect('itms-services://?action=download-manifest&url=' + global.config.aws_url);
    resp.write(data);
    resp.end();
  });
};
