var _ = require('lodash');

var clientDao = require('../cbDao/client');
var prodDao = require('../cbDao/product');
var needDao = require('../cbDao/needs');
var needsHandler = require('../NeedsHandler');
var QuotDriver = require('../Quote/QuotDriver');
const {validateClient, validateFNAInfo, checkAllowQuotProduct, getFNAInfo} = require('./quote/common');

var logger = global.logger;

const getProfile = (profileId) => {
  if (!profileId) {
    return Promise.resolve(null);
  } else {
    return clientDao.getProfile(profileId, true);
  }
};

const filterProductsByEligibility = (agent, prods) => {
  if (agent && agent.eligProducts) {
    if (agent.eligProducts.includes('*')){
      return prods;
    } else {
      return _.filter(prods, prod => agent.eligProducts.indexOf(prod.covCode) > -1);
    }
  } else {
    return prods;
  }
};

module.exports.queryProducts = function(data, session, callback) {
  let {proposerCid, insuredCid, quickQuote, params} = data;
  var compCode = session.agent.compCode;

  var pCid = proposerCid || insuredCid;
  var requireFNA = session.agent.channel.type === 'AGENCY' && !quickQuote;

  var promises = [];
  promises.push(getProfile(proposerCid));
  promises.push(getProfile(insuredCid));
  promises.push(prodDao.getProductSuitability());
  if (requireFNA) {
    promises.push(getFNAInfo(pCid));
  }

  Promise.all(promises).then((args) => {
    let [proposer, insured, suitability, fnaInfo] = args;
    proposer = proposer || insured;

    // validate inputs including client profiles, FNA completeness etc.
    var inputError = validateInputForProducts(proposer, insured, fnaInfo, requireFNA);
    if (inputError) {
      callback({
        success: true,
        prodCategories: [],
        error: inputError
      });
      return;
    }

    logger.log('Products :: queryProducts :: start querying for basic plan by client');
    return prodDao.queryBasicPlansByClient(compCode, insured, proposer, params).then((prods) => {
      return filterProductsByEligibility(session.agent, prods);
    }).then((prods) => {
      logger.log('Products :: queryProducts :: ' + prods.length + ' prods queried');
      let result = new QuotDriver().getProducts(suitability, prods, session.agent, [proposer, insured], requireFNA ? fnaInfo : null, { optionsMap: global.optionsMap, quickQuote: quickQuote });
      let error = null;
      let prodCategories = [];
      if (!result || _.isEmpty(result.productList)) {
        error = 'noData';
      } else {
        prodCategories = _getProdByProductLine(result.productList);
        if (_.isEmpty(prodCategories)) {
          error = 'noData';
        }

        // CR71 - no products show for proposer if there is no need selected for myself
        if (requireFNA && (!proposerCid || proposerCid === insuredCid)) {
          const { fna } = fnaInfo;
          let aspects = _.split(_.get(fna, 'aspects'), ',');
          if (!_.find(aspects, aspect=>_.get(fna, `${aspect}.owner.isActive`))) {
            error = 'noData';
          }
        }
      }
      callback({
        success: true,
        prodCategories: prodCategories,
        error: error
      });
    });
  }).catch((err) => {
    logger.error('Products :: queryProducts :: Failed to query products\n', err);
    callback({ success: false });
  });
};

var validateInputForProducts = function (proposer, insured, fnaInfo, requireFNA) {
  if (proposer !== insured) {
    var dependant = _.find(proposer.dependants, (d) => d.cid === insured.cid);
    if (!validateClient(insured) || !dependant || !dependant.relationship) {
      return 'missingInsuredInfo';
    }
  }
  if (!validateClient(proposer)) {
    return 'missingProposerInfo';
  }
  if (requireFNA && !validateFNAInfo(fnaInfo)) {
    return 'incompleteFNA';
  }
};

var _getProdByProductLine = function (products) {
  const productLines = global.optionsMap.productLine.options;
  var pcMap = {};
  _.each(products, (prod) => {
    var pl = prod.productLine;
    var productLine = productLines[pl];
    var category;
    if (productLine) {
      category = pcMap[pl];
      if (!category) {
        category = pcMap[pl] = {
          title: productLine.title,
          seq: productLine.sequence,
          products: []
        };
      }
    } else {
      category = pcMap.others;
      if (!category) {
        category = pcMap.others = {
          title: 'Other Products',
          seq: 999,
          products: []
        };
      }
    }
    category.products.push(prod);
  });
  return _.sortBy(pcMap, 'seq');
};

module.exports.getBrochure = function(data, session, callback) {
  prodDao.getAttachmentWithLang(data.productId, 'brochures', data.lang, function(data){
    logger.log('Products :: getBrochure :: getBrochure after get:', !!data || data.length);
    callback({
      data: data.data
    });
  });
};

module.exports.getDependantList = function (data, session, callback) {
  var cid = data.cid;
  var requireFNA = session.agent.channel.type === 'AGENCY' && !data.quickQuote;
  getProfile(cid).then((profile) => {
    if (requireFNA) {
      let promises = [];
      promises.push(needDao.getItem(cid, needDao.ITEM_ID.PDA));
      promises.push(needDao.getItem(cid, needDao.ITEM_ID.NA));
      return Promise.all(promises).then((result) => {
        let pda = result[0];
        let fna = result[1];
        if (pda) {
          let dependants = [];
          if (pda.applicant === 'joint') {
            let spouse = _.find(profile.dependants, d => d.relationship === 'SPO');
            if (spouse) {
              dependants.push(spouse.cid);
            }
          }
          dependants = dependants.concat(pda.dependants && pda.dependants.length && pda.dependants.split(','));

          let needsMap = {};
          _.each(dependants.concat(cid), (cid) => {
            needsMap[cid] = needsHandler.getSelectedNeeds(cid, profile, pda, fna)
          });
          let validDependents = _.filter(_.map(needsMap, (needs, cid) => needs && needs.length > 0 ? cid : null), id => id !== cid && id);
          return validDependents;
        } else {
          return null;
        }
      });
    } else {
      return _.map(profile && profile.dependants, (dependant) => dependant.cid);
    }
  }).then((dCids) => {
    return Promise.all(_.map(dCids, (dCid) => getProfile(dCid)));
  }).then((dProfiles) => {
    callback({
      success: true,
      dependants: _.map(dProfiles, (dProfile) => {
        return {
          cid: dProfile.cid,
          fullName: dProfile.fullName
        };
      })
    });
  }).catch((err) => {
    logger.error('Products :: getDependantList :: failed to get dependant list\n', err);
    callback({ success: false });
  });
};

module.exports.validateProductForQuot = function (data, session, callback) {
  let {productId, insuredCid, proposerCid} = data;
  var promises = [];
  promises.push(getProfile(proposerCid));
  promises.push(getProfile(insuredCid || proposerCid));

  var requireFNA = session.agent.channel.type === 'AGENCY';
  if (requireFNA) {
    promises.push(getFNAInfo(proposerCid));
  }

  Promise.all(promises).then(([proposer, insured, fnaInfo]) => {
    if (!validateClient(proposer) || !validateClient(insured) || (requireFNA && !validateFNAInfo(fnaInfo))) {
      logger.log('Products :: validateProductForQuot :: mandatory fields are not completed');
      callback({ success: true, viewable: false });
    } else {
      return checkAllowQuotProduct(session.agent, productId, proposer, insured, fnaInfo).then((allowQuot) => {
        callback({
          success: true,
          viewable: allowQuot
        });
      });
    }
  }).catch((error) => {
    logger.error('Products :: validateProductForQuot :: failed to validate product\n', error);
    callback({ success: false });
  });
};
