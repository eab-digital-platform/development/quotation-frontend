var _ = require('lodash');

var notifyDao = require('../cbDao/systemNotification');
var async = require('async');

var logger = global.logger;

const getValidSystemNotification = function(compCode, dealerGroup, cb) {
  const currentDateTime = Date.parse(new Date().toUTCString());
  const expiryDate = Date.parse(new Date('9999-12-31').toISOString());
  async.waterfall([
      (callback) => {
        let promises = [];
        promises.push(notifyDao.getValidEndNotification(compCode, currentDateTime, expiryDate));

        Promise.all(promises).then(data => {
          let validMessages = [];
          let message;
          let messageId;
          let messageDealerGroups;
          _.each(data, (campaignView, key) => {
            _.each(campaignView && campaignView.rows, rowObj => {
              message = _.get(rowObj, 'value');
              messageId = _.get(rowObj, 'value.messageId');
              messageDealerGroups = _.get(rowObj, 'value.messageDealerGroups');
              if (message && message.startTime && currentDateTime > Date.parse(message.startTime) && messageId
                  && (!messageDealerGroups || (messageDealerGroups && _.indexOf(messageDealerGroups, dealerGroup)) > -1)) {
                validMessages.push(message);
              }
            });
          });
          callback(null, validMessages);
        });
      }
  ], (err, validMessage) => {
      if (err) {
          logger.error('ERROR in getvalidMessage: ', err);
          cb([]);
      } else {
          cb(validMessage);
      }
  });
};

module.exports.getValidSystemNotification = getValidSystemNotification;
