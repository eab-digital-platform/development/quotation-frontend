const summary = require('./summary');
const app = require('./application');
const sign = require('./signature');
const pay = require('./payment');
const submit = require('./submission');
const supp = require('./supportDocument');
const clientChoice = require('./clientChoice');

module.exports.applyAppForm = summary.applyAppForm;
module.exports.continueApplication = summary.continueApplication;

module.exports.saveAppForm = app.saveAppForm;
module.exports.invalidateApplication = app.invalidateApplication;
module.exports.setSignExpiryShown = app.setSignExpiryShown;
module.exports.goNextStep = app.goNextStep;

module.exports.getSignatureUpdatedUrl = sign.getSignatureUpdatedUrl;

module.exports.checkPaymentStatus = pay.checkPaymentStatus;
module.exports.savePaymentMethods = pay.savePaymentMethods;
module.exports.confirmAppPaid = pay.confirmAppPaid;

module.exports.saveSubmissionValues = submit.saveSubmissionValues;
module.exports.submission = submit.submission;

module.exports.showSupportDocments = supp.showSupportDocments;
module.exports.closeSupportDocments = supp.closeSupportDocments;

module.exports.setMenuItemTemplate_shield = clientChoice.setMenuItemTemplate_shield;
module.exports.setMenuItemValues_shield = clientChoice.setMenuItemValues_shield;
module.exports.getClientChoiceDefaultString_shield = clientChoice.getClientChoiceDefaultString_shield;
