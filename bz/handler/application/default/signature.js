const _ = require('lodash');
const moment = require('moment');
const logger = global.logger || console;

const tokenUtils = require('../../../utils/TokenUtils');
const commonApp = require('../common');
const dao = require('../../../cbDaoFactory').create();
const fDao = require('../../../cbDao/file');
const cDao = require('../../../cbDao/client');
const bDao = require('../../../cbDao/bundle');
const quotDao = require('../../../cbDao/quotation');
const appDao = require('../../../cbDao/application');
const _application = require('./application');

const coverPdfName = 'cover';

const FNA_SIGNDOC_SIZE = {
  AGENT : {
    // WIDTH: 0.139,
    // HEIGHT: 0.035
    WIDTH: 0.110,
    HEIGHT: 0.025
  },
  CLIENT : {
    // WIDTH: 0.139,
    // HEIGHT: 0.0300
    WIDTH: 0.103,
    HEIGHT: 0.026
  }
};

const PROPOSAL_SIGNDOC_SIZE = {
  // WIDTH: 0.139,
  // HEIGHT: 0.035
  WIDTH: 0.108,
  HEIGHT: 0.035
};

//=ApplicationHandler.getSignatureStatusFromCb
var _getSignatureInitUrl = function(data, session, callback) {
  const docId = data.docId;
  // const attUrl = global.config.host.path + '/getAttachmentPdf';
  const attUrl = global.config.signdoc.getPdf;
  const isFaChannel = session.agent.channel.type === 'FA';
  var result = {
    success: false,
    application: {
      showCoverTab: false,
      isFaChannel: isFaChannel,
      tabCount: 0,
      agentSignFields: [],
      clientSignFields: [],
      textFields: [],
      attachments: [],
      isMandDocsAllUploaded: false,
      appStep: 0
    },
    signDoc: {
      postUrl: global.config.signdoc.postUrl,
      resultUrl: global.config.signdoc.resultUrl,
      dmsId: global.config.signdoc.dmsid
    },
    crossAge: {}
  };
  var signFieldDefault = {
    searchTag: '',
    width: 0.127,
    height: 0.05
  };
  var textFieldDefault = {
    searchTag: '',
    size: 12,
    width: 0.127,
    height: 0.0055
  };
  var now = Date.now();

  //prepare fnaReport result
  //no pdfStr for fnaReport as SignDoc will be loaded
  var prepareFnaReport = function (app, cb) {
    logger.log('INFO: getSignatureStatusFromCb - prepareFnaReport', docId);
    bDao.getCurrentBundle(app.pCid).then((bundle)=>{
      let today = new Date();
      let signDate = `${today.getDate()}/${today.getMonth()}/${today.getFullYear()}`;
      if (bundle && !bundle.error) {
        result.application.tabCount += 1;
        result.application.agentSignFields.push([
          Object.assign({}, signFieldDefault, {
              name:app.quotation.agent.name,
              searchTag:'FNAREPORT_AGENT_SIGNATURE',
              width: FNA_SIGNDOC_SIZE.AGENT.WIDTH,
              height: FNA_SIGNDOC_SIZE.AGENT.HEIGHT
            })
        ]);
        result.application.textFields.push(
          [
            Object.assign({}, textFieldDefault, {
              searchTag: 'FNAREPORT_AGENT_NAME',
              name: 'agent'
            }),
            Object.assign({}, textFieldDefault, {
              searchTag: 'FNAREPORT_MYSELF_SIGN_DATE',
              name: signDate
            }),
            Object.assign({}, textFieldDefault, {
              searchTag: 'FNAREPORT_SPOUSE_SIGN_DATE',
              name: signDate
            }),
            Object.assign({}, textFieldDefault, {
              searchTag: 'FNAREPORT_AGENT_SIGN_DATE',
              name: signDate
            })
          ]
        );
        let clientSignFields = [
          Object.assign({}, signFieldDefault, {
            name:app.applicationForm.values.proposer.personalInfo.fullName,
            searchTag:'FNAREPORT_CLIENT_SIGNATURE',
            width: FNA_SIGNDOC_SIZE.CLIENT.WIDTH,
            height: FNA_SIGNDOC_SIZE.CLIENT.HEIGHT
          })
        ];
        let tokens = [];
        tokens.push(tokenUtils.createPdfToken(bundle._id, commonApp.PDF_NAME.FNA, now, session.loginToken));

        tokenUtils.setPdfTokensToRedis(tokens, () => {
          result.application.attachments.push({
            docid: app._id + '_' + commonApp.PDF_NAME.FNA,
            pdfStr: '',
            attUrl: attUrl + '/' + tokens[0].token,
            isSigned: bundle.isFnaReportSigned,
            auth: commonApp.genSignDocAuth(app._id + '_' + commonApp.PDF_NAME.FNA, now),
            docts: now
          });

          //Check if Spouse exists
          cDao.getClientById(app.pCid, function(pClient) {
            if (pClient && !pClient.error) {
              let dependant = pClient.dependants.find(function(d) {
                return d.relationship === 'SPO';
              });
              // add SPOUSE name if dependant has relationship SPO
              if (dependant) {
                cDao.getClientById(dependant.cid, function(spouse) {
                  if (spouse && !spouse.error) {
                    clientSignFields.push(Object.assign({}, signFieldDefault, {
                      name:spouse.fullName,
                      searchTag:'FNAREPORT_SPOUSE_SIGNATURE',
                      width: FNA_SIGNDOC_SIZE.CLIENT.WIDTH,
                      height: FNA_SIGNDOC_SIZE.CLIENT.HEIGHT
                    }));
                    result.application.clientSignFields.push(clientSignFields);
                    cb({success:true, isFnaReportSigned: bundle.isFnaReportSigned});
                  } else {
                    logger.error('ERROR getSignatureStatusFromCb - prepareFnaReport get spouse ', _.get(spouse, 'error'));
                    cb({success:false, result:dependant.cid});
                  }
                });
              } else {
                result.application.clientSignFields.push(clientSignFields);
                cb({success:true, isFnaReportSigned: bundle.isFnaReportSigned});
              }
            } else {
              logger.error('ERROR getSignatureStatusFromCb - prepareFnaReport get client ', _.get(pClient, 'error'));
              cb({success:false, result:app.pCid});
            }
          });
        });
      } else {
        logger.error('ERROR getSignatureStatusFromCb - prepareFnaReport get bundle ', _.get(bundle, 'error'));
        cb({success:false, result:app.pCid});
      }
    });
  };

  var prepareProposal = function(app, isFnaReportSigned, cb) {
    logger.log('INFO: getSignatureStatusFromCb - prepareProposal', docId);
    fDao.getAttachment(app.quotationDocId, commonApp.PDF_NAME.BI, function(attProposal){
      logger.log('INFO: getSignatureStatusFromCb - prepareProposal getAttachment ' + app.quotationDocId + '/' + commonApp.PDF_NAME.BI + ' success:', attProposal.success);

      if (attProposal.success) {
        //prepare proposal result
        result.application.tabCount += 1;
        result.application.agentSignFields.push(
          [
            Object.assign({}, signFieldDefault, {
              name:app.quotation.agent.name,
              searchTag:'ADVISOR_SIGNATURE',
              width: PROPOSAL_SIGNDOC_SIZE.WIDTH,
              height: PROPOSAL_SIGNDOC_SIZE.HEIGHT
            })
          ]
        );
        result.application.clientSignFields.push(
          [
            Object.assign({}, signFieldDefault, {
              name:app.applicationForm.values.proposer.personalInfo.fullName,
              searchTag:'PROPOSER_SIGNATURE',
              width: PROPOSAL_SIGNDOC_SIZE.WIDTH,
              height: PROPOSAL_SIGNDOC_SIZE.HEIGHT
            })
          ]
        );

        let isGenToken = app.isProposalSigned || isFnaReportSigned || isFaChannel;

        let tokens = [];
        if (isGenToken) {
          tokens.push(tokenUtils.createPdfToken(app.quotationDocId, commonApp.PDF_NAME.BI, now, session.loginToken));
        }

        tokenUtils.setPdfTokensToRedis(tokens, () => {
          result.application.attachments.push({
            docid: docId + '_' + commonApp.PDF_NAME.BI,
            pdfStr: (!app.isProposalSigned ? attProposal.data : ''),
            attUrl: (isGenToken ? attUrl + '/' + tokens[0].token : ''),
            isSigned: app.isProposalSigned,
            auth: commonApp.genSignDocAuth(docId + '_' + commonApp.PDF_NAME.BI, now),
            docts: now
          });
          cb(true);
        });
      } else {
        logger.error('ERROR getSignatureStatusFromCb - prepareProposal getAttachment ' + app.quotationDocId + '/' + commonApp.PDF_NAME.BI, _.get(attProposal, 'error'));
        cb(false);
      }
    });
  };

  var prepareAppForm = function(app, cb) {
    logger.log('INFO: getSignatureStatusFromCb - prepareAppForm', docId);
    // let isPhSameAsLa = app.applicationForm.values.proposer.extra.isPhSameAsLa === 'Y';
    let hasLifeAssured = app.applicationForm.values.insured.length > 0 && commonApp.checkLaIsAdult(app);
    let hasTrustedIndividual = app.applicationForm.values.proposer.extra.hasTrustedIndividual === 'Y';
    let signatureWidth = signFieldDefault.width;
    // const signatureHeight = 0.039;
    const signatureHeight = 0.030;
    //no signature for LA age < 18

    //Signature box width
    let numOfSignature = 2; //agent & proposor
    if (hasTrustedIndividual) {
      numOfSignature += 1;
    }
    if (hasLifeAssured) {
      numOfSignature += 1;
    }
    switch (numOfSignature) {
      case 4:
        signatureWidth = 0.062;
        break;
      case 3:
        // signatureWidth = 0.118;
        signatureWidth = 0.092;
        break;
      default:
        // signatureWidth = 0.188;
        signatureWidth = 0.147;
    }

    fDao.getAttachment(docId, commonApp.PDF_NAME.eAPP, function(attAppForm){
      logger.log('INFO: getSignatureStatusFromCb - prepareAppForm getAttachment ' + docId + '/' + commonApp.PDF_NAME.eAPP + ' success:', attAppForm.success);

      if (attAppForm.success) {
        //prepare result
        result.application.tabCount += 1;
        result.application.agentSignFields.push(
          [
            Object.assign({}, signFieldDefault, {
              name:app.quotation.agent.name,
              searchTag:'ADVISOR_SIGNATURE',
              height: signatureHeight,
              width: signatureWidth
            })
          ]
        );
        let clientSignFields = [
          Object.assign({}, signFieldDefault, {
            name: app.applicationForm.values.proposer.personalInfo.fullName,
            searchTag:'PROP_SIGNATURE',
            height: signatureHeight,
            width: signatureWidth
          })
        ];

        if (hasTrustedIndividual) {
          clientSignFields.push(Object.assign({}, signFieldDefault, {
            name: _.get(app, 'applicationForm.values.proposer.declaration.trustedIndividuals.fullName'),
            searchTag:'TI_SIGNATURE',
            height: signatureHeight,
            width: signatureWidth
          }));
        }

        if (hasLifeAssured) {
          clientSignFields.push(Object.assign({}, signFieldDefault, {
            name: app.applicationForm.values.insured[0].personalInfo.fullName,
            searchTag:'LA_SIGNATURE',
            height: signatureHeight,
            width: signatureWidth
          }));
        }
        result.application.clientSignFields.push(clientSignFields);

        let isGenToken = app.isApplicationSigned || app.isProposalSigned;
        let tokens = [];
        if (isGenToken) {
          tokens.push(tokenUtils.createPdfToken(docId, commonApp.PDF_NAME.eAPP, now, session.loginToken));
        }

        tokenUtils.setPdfTokensToRedis(tokens, () => {
          result.application.attachments.push({
            docid: docId + '_' + commonApp.PDF_NAME.eAPP,
            pdfStr:(!app.isApplicationSigned ? attAppForm.data : ''),
            attUrl: (isGenToken ? attUrl + '/' + tokens[0].token : ''),
            isSigned: app.isApplicationSigned,
            auth: commonApp.genSignDocAuth(docId + '_' + commonApp.PDF_NAME.eAPP, now),
            docts: now
          });

          cb(true);
        });
      } else {
        logger.error('ERROR getSignatureStatusFromCb - prepareAppForm getAttachment ' + docId + '/' + commonApp.PDF_NAME.eAPP, _.get(attAppForm, 'error'));
        cb(false);
      }
    });
  };

  //TODO: Cover Page
  var prepareCoverPage = function(app, cb) {
    // logger.log('INFO: getSignatureStatusFromCb - prepareCoverPage');
    // fileDao.getAttachment(app.quotationDocId, coverPdfName, function (attCover) {

    //   if (attCover.success) {
    //     //prepare cover result
    //     result.application.tabCount += 1;

    //     let tokens = [];
    //     tokens.push(createPdfToken(docId, coverPdfName, now, session.loginToken));

    //     setPdfTokensToRedis(tokens, () => {
    //       result.application.attachments.push({
    //         docid: docId + '_' + coverPdfName,
    //         pdfStr:(false? attCover.data: ''),
    //         attUrl: attUrl + '/' + tokens[0].token,
    //         isSigned: false,
    //         auth: genSignDocAuth(docId + '_' + coverPdfName, now),
    //         docts: now
    //       });
    //       callback(true);
    //     })
    //   } else {
    //     logger.error('ERROR: getSignatureStatusFromCb - prepareCoverPage (error in att cover)');
    //     callback(false);
    //   }
    // })
  };

  logger.log('INFO: getSignatureStatusFromCb - start', docId);

  appDao.getApplication(docId, function(app) {
    if (app._id) {
      _application.checkCrossAge(app, (crossAgeCb) => {
        result.crossAge = crossAgeCb;
        //TODO:  Update showCoverTab and isCoverSigned
        result.application.showCoverTab = app.hasOwnProperty('supplementaryForm');
        // result.application.agentSignFields = app.hasOwnProperty('signFields')? app.signFields.agent: [];
        // result.application.clientSignFields = app.hasOwnProperty('signFields')? app.signFields.client: [];
        result.application.appStep = app.appStep;
        result.application.isMandDocsAllUploaded = app.isSubmittedStatus || app.isMandDocsAllUploaded;

        if (isFaChannel){
          prepareProposal(app, false, function(resP) {
            if (resP) {
              prepareAppForm(app, function(resA) {
                if (resA) {
                  if (!app.hasOwnProperty('supplementaryForm')) {
                    result.success = true;

                    logger.log('INFO: getSignatureStatusFromCb - end [RETURN=1]', docId);
                    callback(result);
                  } else {
                    prepareCoverPage(app, function() {
                      logger.log('INFO: getSignatureStatusFromCb - end [RETURN=2]', docId);
                      callback(result);
                    });
                  }
                } else {
                  logger.error('ERROR: getSignatureStatusFromCb [RETURN=-3]', docId);
                  callback({success:false});
                }
              });
            } else {
              logger.error('ERROR: getSignatureStatusFromCb [RETURN=-2]', docId);
              callback({success:false});
            }
          });
        } else {
          prepareFnaReport(app, function(resF) {
            if (resF.success) {
              prepareProposal(app, resF.isFnaReportSigned, function(resP) {
                if (resP) {
                  prepareAppForm(app, function(resA) {
                    if (resA) {
                      if (!app.hasOwnProperty('supplementaryForm')) {
                        result.success = true;

                        logger.log('INFO: getSignatureStatusFromCb - end [RETURN=1]', docId);
                        callback(result);
                      } else {
                        prepareCoverPage(app, function() {
                          logger.log('INFO: getSignatureStatusFromCb - end [RETURN=2]', docId);
                          callback(result);
                        });
                      }
                    } else {
                      logger.error('ERROR: getSignatureStatusFromCb [RETURN=-6]', docId);
                      callback({success:false});
                    }
                  });
                } else {
                  logger.error('ERROR: getSignatureStatusFromCb [RETURN=-5]', docId);
                  callback({success:false});
                }
              });
            } else {
              logger.error('ERROR: getSignatureStatusFromCb [RETURN=-4]', docId);
              callback({success:false});
            }
          });
        }
      });
    } else {
      logger.error('ERROR: getSignatureStatusFromCb [RETURN=-1]');
      callback({success:false});
    }
  });
};


//=ApplicationHandler.getUpdatedAttachmentUrl
const _getSignatureUpdatedUrl = function(data, session, callback) {
  const signDocId = data.signDocId;
  const ids = commonApp.getIdFromSignDocId(signDocId);
  const docId = ids.docId;
  const attId = ids.attId;
  const tabIdx = data.tabIdx;
  // const attUrl = global.config.host.path + '/getAttachmentPdf';
  const attUrl = global.config.signdoc.getPdf;
  const now = Date.now();
  // var newPdfToken = '';
  var result = {
    success: false,
    newAttUrls:[],
    profile: {},
    appFormTemplate: {},
    application: {}
  };

  logger.log('INFO: getUpdatedAttachmentUrl - start', docId);

  var getFrozenTemplate = function(app, cb) {
    logger.log('INFO: getUpdatedAttachmentUrl - getFrozenTemplate', docId);
    _application.getAppFormTemplate(app.quotation, function(template){
      commonApp.getOptionsList(app, template, function(){
        if (app.isStartSignature){
          commonApp.frozenTemplate(template);
        }
        cb(template);
      });
    });
  };

  appDao.getApplication(docId, function(app) {
    if (app._id && !app.error) {

      cDao.setIsProfileCanDel(app.pCid, function(res) {
        if (res.profile && !res.profile.error) {
          result.profile = res.profile;
        }

        if (attId === commonApp.PDF_NAME.FNA) {
          getFrozenTemplate(app, function(template) {
            //generate signed fnaReport and next signing doc url - proposal

            bDao.getCurrentBundle(app.pCid).then((bundle)=>{
              let bundleId = _.get(bundle, 'id');
              if (!bundleId){
                result.success = false;
                result.appFormTemplate = template;
                logger.error('ERROR: getUpdatedAttachmentUrl - end [RETURN=-2]', docId);
                callback(result);
              }
              else {
                dao.getDoc(app.pCid, function(profile){
                  bundle.profile = profile;
                  let today = new Date();
                  bundle.fnaSignDate = `${today.getDate()}/${today.getMonth() + 1}/${today.getFullYear()}`;
                  dao.updDoc(bundleId, bundle, function(bResult){
                    let tokens = [];
                    tokens.push(tokenUtils.createPdfToken(bundleId, attId, now, session.loginToken));
                    tokens.push(tokenUtils.createPdfToken(app.quotationDocId, commonApp.PDF_NAME.BI, now, session.loginToken));

                    tokenUtils.setPdfTokensToRedis(tokens, () => {
                      result.newAttUrls.push({
                        index: tabIdx,
                        attUrl: attUrl + '/' + tokens[0].token
                      });
                      result.newAttUrls.push({
                        index: tabIdx + 1,
                        attUrl: attUrl + '/' + tokens[1].token
                      });
                      result.success = true;
                      result.appFormTemplate = template;
                      result.application = app;
                      logger.log('INFO: getUpdatedAttachmentUrl - end [RETURN=1]', docId);
                      callback(result);
                    });
                  });
                });
              }
            });
          });
        } else if (attId === commonApp.PDF_NAME.BI) {
          getFrozenTemplate(app, function(template) {
            //generate signed proposalPdfName and next signing doc url - app form
            let tokens = [];
            tokens.push(tokenUtils.createPdfToken(app.quotationDocId, attId, now, session.loginToken));
            tokens.push(tokenUtils.createPdfToken(app._id, commonApp.PDF_NAME.eAPP, now, session.loginToken));

            tokenUtils.setPdfTokensToRedis(tokens, () => {
              result.newAttUrls.push({
                index: tabIdx,
                attUrl: attUrl + '/' + tokens[0].token
              });
              result.newAttUrls.push({
                index: tabIdx + 1,
                attUrl: attUrl + '/' + tokens[1].token
              });
              result.success = true;
              result.appFormTemplate = template;
              result.application = app;
              logger.log('INFO: getUpdatedAttachmentUrl - end [RETURN=2]', docId);
              callback(result);
            });
          });
        } else if (attId === commonApp.PDF_NAME.eAPP && !app.hasOwnProperty('supplementaryForm')) {
          let tokens = [];
          tokens.push(tokenUtils.createPdfToken(app._id, attId, now, session.loginToken));

          tokenUtils.setPdfTokensToRedis(tokens, () => {
            result.newAttUrls.push({
              index: tabIdx,
              attUrl: attUrl + '/' + tokens[0].token
            });
            result.success = true;
            result.application = app;
            logger.log('INFO: getUpdatedAttachmentUrl - end [RETURN=3]', docId);
            callback(result);
          });
        }
        // else {
        //   //TODO: cover page
        //   result.newAttUrls.push({
        //     index: tabIdx + 1,
        //     attUrl: attUrl + '/' + createPdfToken(app._id, coverPdfName, now)
        //   })
        //   result.success = true;
        //   logger.log('INFO: getUpdatedAttachmentUrl - end [RETURN=4]', docId);
        //   cb(result);
        // }
      });
    } else {
      logger.error('ERROR: getUpdatedAttachmentUrl - end [RETURN=-1]', _.get(app, 'error'));
      result.success = false;
      callback(result);
    }
  });
};

const _saveSignedPdfFromSignDoc = function(appId, attId, pdfData, callback) {
  var updateApplicationSigned = function(upApp, cb) {
    var now = new Date();

    if (attId === commonApp.PDF_NAME.FNA) {
      upApp.isStartSignature = true;
    } else if (attId === commonApp.PDF_NAME.BI) {
      // in FA channel, there is no FNA, that's why proposal also need to set isStartSignature
      upApp.isStartSignature = true;
      upApp.isProposalSigned = true;
      upApp.biSignedDate = now.toISOString();
    } else if (attId === commonApp.PDF_NAME.eAPP) {
      upApp.isFullySigned = true;
      upApp.isApplicationSigned = true;
    } else if (attId === coverPdfName) {
      upApp.isCoverSigned = true;
    }
    upApp.applicationSignedDate = now.toISOString();

    //Update Application
    appDao.upsertApplication(upApp._id, upApp, function(result) {
      if (!result || result.error) {
        logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-10]', appId);
      } else {
        logger.log('INFO: getSignedPdfFromSignDocPost - end [RETURN=1]', appId);
      }
      cb({success:result.ok, result:result});
    });
  };

  //1. Get application for update signed status
  appDao.getApplication(appId, function(app){
    if (app._id) {
      if (attId === commonApp.PDF_NAME.FNA) {
        //handle fnaReport
        bDao.getCurrentBundle(app.pCid).then((bundle)=>{
          if (bundle) {
                dao.uploadAttachmentByBase64(bundle._id, attId, bundle._rev, pdfData, 'application/pdf', function(res) {
                  if (res && !res.error) {
                    // bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.SIGN_FNA, (newBundle)=>{
                    logger.log(`FNA Signature Checking Bundle ID :: ${_.get(bundle, 'id')}, -- application/signature _saveSignedPdfFromSignDoc :: FNA Status :: ${_.get(bundle, 'status')}`);
                    logger.log(`FNA Signature Checking Bundle ID :: ${_.get(bundle, 'id')}, -- application/signature _saveSignedPdfFromSignDoc :: FNA Signed :: ${_.get(bundle, 'isFnaReportSigned')}`);
                    logger.log(`FNA Signature Checking Bundle ID :: ${_.get(bundle, 'id')}, -- application/signature _saveSignedPdfFromSignDoc :: Applications Stored ::`, _.get(bundle, 'applications'));
                    bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.SIGN_FNA).then((newBundle)=>{
                      newBundle.isFnaReportSigned = true;
                      logger.log(`INFO: BundleUpdate - [cid:${app.pCid}; bundleId:${_.get(bundle, 'id')}; fn:_saveSignedPdfFromSignDoc]`);
                      bDao.updateBundle(newBundle, function(upRes) {
                        if (upRes && !upRes.error) {
                          updateApplicationSigned(app, callback);
                          logger.log('11111upRes:' + JSON.stringify(upRes));
                          logger.log('upRes.rev:' + upRes.rev);
                          logger.log('upRes._rev:' + upRes._rev);
                          // Because AXA SG reports that FNA signature sometimes missing after agent go to eApp stage and do other things
                          // In order to make it stable, we make a backup for fna report so that we can use it when signature miss
                          dao.uploadAttachmentByBase64(bundle._id, attId + '_backup', upRes.rev, pdfData, 'application/pdf', function(res) {
                            logger.log('Upload backup FNA report successfully');
                          });
                        } else {
                          logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-4]', appId);
                          callback({success:false, upRes, newBundle});
                        }
                      });
                    }).catch((error)=>{
                      logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-5]', appId, error);
                      callback({success:false, bundle});
                    });
                  } else {
                    logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-3]', appId);
                    callback({success:false, bundle});
                  }
                });

          } else {
            logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-2]', appId);
            callback({success:false, id:app.pCid, bundle});
          }
        });
      } else if (attId === commonApp.PDF_NAME.BI) {
        //handle proposal
        quotDao.getQuotation(app.quotationDocId, function(quot) {
          quotDao.uploadSignedProposal(app.quotationDocId, pdfData).then((res) => {
            if (res && !res.error) {
              return bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.SIGN_FNA);
            } else {
              throw new Error('Fail to update Signed proposal ' + _.get(res, 'error'));
            }
          }).then((newBundle)=>{
            updateApplicationSigned(app, callback);
          }).catch((err) => {
            logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-5]', err);
            callback({success:false, id:app.quotationDocId, quot:quot});
          });
        });
      } else {
        //handle appForm
        dao.uploadAttachmentByBase64(app._id, attId, app._rev, pdfData, 'application/pdf', function(res) {
          if (res && !res.error) {
            // bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.FULL_SIGN, (newBundle)=>{
            bDao.updateStatus(app.pCid, bDao.BUNDLE_STATUS.FULL_SIGN).then((newBundle)=>{
              for (let i = 0; i < newBundle.applications.length; i ++) {
                let appItem = newBundle.applications[i];
                if (appItem.applicationDocId === app._id) {
                  appItem.isFullySigned = true;
                  break;
                }
              }
              logger.log(`INFO: BundleUpdate - [cid:${app.pCid}; bundleId:${_.get(newBundle, 'id')}; fn:_saveSignedPdfFromSignDoc]`);
              bDao.updateBundle(newBundle, function(upRes) {
                if (upRes && !upRes.error) {
                  updateApplicationSigned(app, callback);
                } else {
                  logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-8]', _.get(upRes, 'error'));
                  callback({success:false, upRes});
                }
              });
            }).catch((error)=>{
              logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-9]', error);
              callback({success:false, app:app});
            });
          } else {
            logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-7]', _.get(res, 'error'));
            callback({success:false, app:app});
          }
        });
      }
    } else {
      logger.error('ERROR: getSignedPdfFromSignDocPost - end [RETURN=-1]', appId);
      callback({success:false, id:appId, app:app});
    }
  });
};

module.exports.saveSignedPdfFromSignDoc = _saveSignedPdfFromSignDoc;

module.exports.getSignatureInitUrl = _getSignatureInitUrl;
module.exports.getSignatureUpdatedUrl = _getSignatureUpdatedUrl;

