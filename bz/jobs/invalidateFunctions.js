const aDao = require('../cbDao/agent');
const dao = require('../cbDaoFactory').create();
const _ = require('lodash');
const logger = global.logger || console;
const {
  callApiByGet
} = require('../utils/RemoteUtils');
const moment = require('moment');

module.exports.getAllCustomer = function () {
  return new Promise(resolve => {
    dao.getViewRange('main', 'contacts', '', '', null, (pList = {}) => {
      resolve(_.compact(_.map(pList.rows, 'id')));
    });
  });
};


module.exports.getAllCustomerWithFundNHAF = function () {
  return new Promise(resolve => {
    dao.getViewRangeAfterIndex('invalidateViews', 'quotationsByNHAFFund', '', '', null, (pList = {}) => {
      let result = [];
      _.each(pList && pList.rows, row => {
        if (row && row.value && row.value.cids) {
          result = _.union(result, row.value.cids);
        }
      });
      resolve(result);
    });
  });
};

/**
 * @param {Array} baseProductCodeArray Array that Contans the base product code of plans going to invalidate
 */
module.exports.getAllCustomerWithMutipleBaseProductCode = function (compCode, baseProdcutCodeArray) {
  let keys = _.map(baseProdcutCodeArray, baseProductCode => `["${compCode}","${baseProductCode}"]`);
  return dao.getViewByKeysAfterIndex('invalidateViews', 'quotationsByBaseProductCode', keys, null).then((pList) => {
    let result = [];
    _.each(pList && pList.rows, row => {
      if (row && row.value && row.value.clientId && row.value.bundleId) {
        result.push(row.value);
      }
    });
    return result;
  }).catch((error)=>{
    logger.error('Error in getAgentsByAgentCode->getViewByKeys: ', error);
  });
};

/**
 * @param {Array} baseProductCodeArray Array that Contans the base product code of plans going to invalidate
 */
module.exports.getAllCustomerByRelease = function (compCodes) {
  let keys = _.map(compCodes, compCode => `["${compCode}"]`);
  return dao.getViewByKeysAfterIndex('invalidateViews', 'release24CR265InflightHandling', keys, null).then((pList) => {
    let result = [];
    _.each(pList && pList.rows, row => {
      if (row && row.value && row.value.clientId && row.value.bundleId) {
        result.push(row.value);
      }
    });
    return result;
  }).catch((error)=>{
    logger.error('Error in getAgentsByAgentCode->getViewByKeys: ', error);
  });
};


module.exports.getAllValidBundleId = function () {
  return new Promise(resolve => {
    dao.getViewRange('invalidateViews', 'validBundleInClient', '', '', null, (bundles = {}) => {
      resolve(_.compact(_.map(bundles.rows, row => row && row.value && row.value.validBundleId)));
    });
  });
};


module.exports.getImpactedClientIds = function (bundleIds, handleCovCode, compCode = '01') {
  let promises = [];
  _.each(handleCovCode, (isHandle, covCode) => {
    if (isHandle) {
      promises.push(
        new Promise(resolve => {
          let keys = _.map([covCode], code => '["' + compCode + '","' + code + '"]');
          dao.getViewByKeys('invalidateViews', 'quotationsByBaseProductCode', keys, null).then((result) => {
            let clientIds = [];
            if (result) {
              _.each(result.rows, (row) => {
                let quotation = row.value || {};
                if (quotation.clientId && quotation.bundleId && bundleIds.indexOf(quotation.bundleId) > -1) {
                  clientIds = _.union(clientIds, [quotation.clientId]);
                }
              });
            }
            resolve(clientIds);
          });
        })
      );
    }
  });

  return Promise.all(promises).then(clientIdsArray => {
    let results = [];
    _.forEach(clientIdsArray, clientIds => {
      results = _.union(results, clientIds);
    });
    return results;
  });
};

module.exports.getAllAgents = function (impactAgents = []) {
  return new Promise(resolve => {
    dao.getViewRange('main', 'agents', '', '', null, (pList = {}) => {
      resolve(_.map(pList.rows, data => {
        return {
          id: _.get(data, 'value.profileId'),
          agentCode: _.get(data, 'value.agentCode'),
          channel: _.get(data, 'value.channel')
        };
      }));
    });
  });
};

module.exports.getAllAgentRelatedDocumentIds = function (compCode, agentCodes) {
  return new Promise(resolve => {
    let keys = _.map([agentCodes], agentCode => '["' + compCode + '","' + agentCode + '"]');
    dao.getViewByKeys('dataSync', 'agentDocuments', keys, null).then((pList = {}) => {
      resolve(_.map(pList.rows, data => {
        return data && data.id;
      }));
    });
  });
};

module.exports.getFilterAgent = function (impactAgents = []) {
  return new Promise(resolve => {
    dao.getViewRange('main', 'agents', '', '', null, (pList = {}) => {
      const result = _.filter(pList.rows, row => impactAgents.indexOf(row.key[1]) > -1);
      resolve(_.map(result, 'value.profileId'));
    });
  });
};

module.exports.insertNoticeToAgent = function (agentId, messageIds, message, messageGroup, groupPriority, isMutilpleMessages) {
  return new Promise(resolve => {
    let agentDocId = aDao.getAgentSuppDocId(agentId) || '';
    dao.getDoc(agentDocId, (agentDoc = {}) => {
      resolve({
        agentDocId,
        agentDoc
      });
    });
  }).then(data => {
    if (data && data.agentDoc && !(data.agentDoc.error === 'not_found' ? true : false)) {
      let agentDoc = data.agentDoc;
      let agentDocId = data.agentDocId;
      if (!agentDoc.notifications) {
        agentDoc.notifications = [];
      }


      agentDoc.notifications = _.filter(agentDoc.notifications, function (notice) {
        if (!notice) {
          // remove Invalid object (Undefined, Null) in notifications array
          return false;
        } else if (notice.id === messageIds && !notice.read && !isMutilpleMessages) {
          // remove the notice which has the same key and not read before
          return false;
        } else if (_.isArray(messageIds) && messageIds.indexOf(notice.id) > -1 && !notice.read && isMutilpleMessages) {
          // remove the notice which has the same key and not read before
          return false;
        } else {
          return true;
        }
      });

      if (!isMutilpleMessages){
        agentDoc.notifications.push({
          id: messageIds,
          message,
          messageGroup,
          groupPriority,
          read: false
        });
      } else {
        messageIds.forEach((messageId,i) => {
          agentDoc.notifications.push({
            id: messageId,
            message: message[i],
            messageGroup: messageGroup[i],
            groupPriority: groupPriority[i],
            read: false
          });
        });

      }
      return new Promise(resolve => {
        dao.updDoc(agentDocId, agentDoc, resolve);
      });
    }
  }).catch(error => {
    logger.error(`Jobs :: InflightInvalidateJob :: ERROR caught at insertNoticeToAgent ${error}`);
    throw Error(`Fail to insert notification to agent ${agentId}`);
  });
};

module.exports.checkIsJobExecutable = function (runTime, sysParam) {
  return new Promise((resolve, reject) => {
    let fromTime = _.get(sysParam, 'fromTime');
    let toTime = _.get(sysParam, 'toTime');
    if (fromTime && toTime) {
      let runDateTime = moment(runTime);
      // The range of fromTime and toTime needs to include the scheduleCron Time
      let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
      let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
      //call API to check if this node server can perform Invalidation or not
      if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
        callApiByGet('/fairBiJobRunner', (result) => {
          resolve(_.get(result, 'success'));
        });
      } else {
        resolve(false);
      }
    } else {
      reject(new Error(`Fail to get fromTime="${fromTime}" toTime="${toTime}" in ${sysParam}`));
    }
  });
};

module.exports.addTrxLogs = function (runTime, trxLogs, errLogs, jobDetailsDocKey) {
  let nowString = moment().toDate().toISOString();
  let runTimeLong = runTime.getTime();
  let runTimeString = runTime.toISOString();
  return new Promise((resolve) => {
    dao.getDoc(jobDetailsDocKey, (doc) => {
      if (doc && !doc.error) {
        let trxData = _.get(doc, `log.${runTimeLong}`);
        if (trxData) {
          let currData = _.cloneDeep(trxData);
          trxData.transaction = _.has(currData, 'transaction') ? _.concat([], currData.transaction, trxLogs) : trxLogs;
          if (!_.isEmpty(errLogs)) {
            trxData.error = _.has(currData, 'error') ? _.concat([], currData.error, errLogs) : errLogs;
          }
        } else {
          trxData = Object.assign({}, {
              transaction: trxLogs
            },
            !_.isEmpty(errLogs) ? {
              error: errLogs
            } : {}
          );
        }
        trxData.completeTime = nowString;
        trxData.runTime = runTimeString;

        doc.log[runTimeLong] = trxData;

        logger.log(`Jobs :: DataPatchJob_DataSync :: update doc ${jobDetailsDocKey} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocKey, doc, (result) => {
          doc._rev = result.rev;
          resolve(doc);
        });
      } else if (doc && doc.error === 'not_found') {
        let newDoc = {
          log: {}
        };

        newDoc.log[runTimeLong] = Object.assign({}, {
            runTime: runTimeString,
            completeTime: nowString,
            transaction: trxLogs
          },
          !_.isEmpty(errLogs) ? {
            error: errLogs
          } : {}
        );

        logger.log(`Jobs :: DataPatchJob_DataSync :: new doc ${jobDetailsDocKey} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocKey, newDoc, (result) => {
          newDoc._rev = result.rev;
          resolve(newDoc);
        });
      } else {
        logger.log(`Jobs :: DataPatchJob_DataSync :: cannot find job details - runTime=${runTimeString} completedTime=${nowString}`);
        resolve(false);
      }
    });
  });
};

/**
 * 
 * Check Which Rules are satisfied by using docId
 */
module.exports.getRuleIdsByMappingDocIdnResult = function(docId, result) {
  let satisfiedResult = [];
  let resultOfDocIds = _.get(result, docId, {});
  _.each(resultOfDocIds, (ruleResult, index, ruleResults) => {
    if (ruleResult) {
      satisfiedResult.push(index);
    }
  });
  return satisfiedResult;
};
