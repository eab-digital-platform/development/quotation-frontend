const _ = require('lodash');
const logger = global.logger || console;
const moment = require('moment');

const invalidateShieldRiderRule = (invalidateParams) => {
  let {baseProductCode,quotation, ruleParams} = invalidateParams;
  let {
    covCodes
  } = ruleParams;
  if (baseProductCode !== 'ASIM' || !quotation || !covCodes) {
    return false;
  }
  let isShieldWithRiders = false;
  let {
    insureds
  } = quotation;
  if (_.isPlainObject(insureds)) {
    _.forEach(insureds, function (quot, cids) {
      if (quot) {
        let {
          plans
        } = quot;
        _.forEach(plans, plan => {
          let {
            covCode
          } = plan;
          if (covCodes.indexOf(covCode) > -1) {
            isShieldWithRiders = true;
            return false;
          }
        });
        if (isShieldWithRiders) {
          return false;
        }
      }
    });
  }
  return isShieldWithRiders;
};

const invalidateProductsByCampaignRule = (invalidateParams) => {
  let {quotation, ruleParams} = invalidateParams;
  let {
    campaignIds
  } = ruleParams;
  let planList = _.get(quotation,'plans');
  let hasCampaignPlan = _.filter(planList, plan => {
    let fulfilledCampaign = false;
    _.each(plan.campaignIds, singplePlanCampaignIds => {
      if (campaignIds.indexOf(singplePlanCampaignIds) > -1) {
        fulfilledCampaign = true;
      }
    });
    return fulfilledCampaign;
  });
  if (hasCampaignPlan.length > 0) {
    return true;
  } else {
    return false;
  }
};

const invalidateProductsByFundRule = (invalidateParams) => {
  let {quotation, ruleParams} = invalidateParams;
  let {
    fundCodes
  } = ruleParams;
  let fundList = _.get(quotation,'fund.funds');
  if (_.filter(fundList, fund => { return fundCodes.indexOf(fund.fundCode) > -1; }).length > 0) {
    return true;
  }
  return false;
};


const invalidateProductsRule = (invalidateParams) => {
  let {baseProductCode, ruleParams} = invalidateParams;
  let {
    productCodes
  } = ruleParams;
  if (productCodes.indexOf(baseProductCode) > -1) {
    return true;
  }
  return false;
};

const invalidateByCreateDateRule = (invalidateParams) => {
  let {createDate, ruleParams} = invalidateParams;
  if (moment(createDate).isValid()) {
    let {toDate} = ruleParams;
    let diff = moment(createDate).diff(toDate);
    //createDate before toDate
    if (diff < 0) {
      return true;
    }
  }
  return false;
};

const invalidateProductsByPaymentTypeRule = (invalidateParams) => {
  let {baseProductCode, quotation, ruleParams} = invalidateParams;
  let {
    productCodes,
    dealerGroups,
    paymentModes
  } = ruleParams;
  let invalidate = false;
  if (productCodes.indexOf(baseProductCode) > -1) {
    _.each(dealerGroups, dealerGroup => {
      if (dealerGroup === quotation.dealerGroup) {
        _.each(paymentModes, paymentMode => {
          if (paymentMode === quotation.paymentMode) {
            invalidate = true;
          }
        });
      }
    });
  }
  return invalidate;
};

const allRulesRunner = {
  invalidateShieldRiderRule: invalidateShieldRiderRule,
  invalidateProductsRule: invalidateProductsRule,
  invalidateByCreateDateRule: invalidateByCreateDateRule,
  invalidateProductsByFundRule: invalidateProductsByFundRule,
  invalidateProductsByCampaignRule: invalidateProductsByCampaignRule,
  invalidateProductsByPaymentTypeRule: invalidateProductsByPaymentTypeRule
};

module.exports.mainRulesHandler = (inflightInputData, jobParams) => {
  let {bundleApplications, docs} = inflightInputData;
  let {ruleIds, rulesParamsMapping} = jobParams;
  let allRulesResults = {};
  let docIds2RulesResult = {};
  let rules2docIdsResult = {};
  if (bundleApplications && _.isArray(bundleApplications)){
    _.forEach(bundleApplications, app => {
      logger.log(app);
      let isApplication = _.has(app, 'applicationDocId');
      let docId = isApplication ? _.get(app, 'applicationDocId', '') : _.get(app, 'quotationDocId', '');
      let isFullySignedCase = app.isFullySigned;
      if (docId){
        let data = _.find(docs, result => {return result.docId === docId && result.isApplication === isApplication;});
        let passRuleResults = {};

        if (data) {
          let doc = data.doc;
          let baseProductCode = isApplication ? _.get(doc, 'quotation.baseProductCode', '') : _.get(doc, 'baseProductCode', '');
          let quotation = isApplication ? _.get(doc, 'quotation', null) : doc;
          let {createDate} = quotation;

          _.forEach(ruleIds, ruleId => {
            if (ruleId) {
              let ruleParams = rulesParamsMapping[ruleId];
              let {functId, partiallySignedOnly} = ruleParams;
              let isRulePass = false;

              //check wheteher pass in each rule
              // isRulePasss --> fulfilled the rule and need to follow up
              let invalidateFunct = allRulesRunner[functId];
              let invalidateParams = {baseProductCode,quotation, createDate, ruleParams};
              
              // partiallySignedOnly from parameter JSON
              // No need to validate when the rule only involve the partially signed and this case is fully signed
              if (isFullySignedCase && partiallySignedOnly) {
                isRulePass = false;
              } else {
                isRulePass = invalidateFunct(invalidateParams);
              }

              passRuleResults[ruleId] = isRulePass;

              if (!_.hasIn(rules2docIdsResult,ruleId)){
                rules2docIdsResult[ruleId] = [];
              }
              if (isRulePass){
                rules2docIdsResult[ruleId].push(docId);
              }
            }
          });
        }
        docIds2RulesResult[docId] = passRuleResults;
      }
    });
    allRulesResults.rules2docIdsResult = rules2docIdsResult;
    allRulesResults.docIds2RulesResult = docIds2RulesResult;
  }

  return allRulesResults;
};
