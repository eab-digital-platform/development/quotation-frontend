var async = require('async');
const _ = require('lodash');
const schedule = require('node-schedule');
const moment = require('moment');
const logger = global.logger || console;

const {callApiByGet} = require('../utils/RemoteUtils');
const {getAllCustomer, getAllAgents, insertNoticeToAgent} = require('./invalidateFunctions');
const dao = require('../cbDaoFactory').create();
const bundleDao = require('../cbDao/bundle/index');
const agentDao = require('../cbDao/agent');
const prodDao = require('../cbDao/product');

const sysParameter = 'sysParameter';
const rules = require('./inflightRules/rules.json');

// Couchbase File Name
const currentDate  = new Date();
const jobDetailsDocKey = `InflightInvalidateJob_${currentDate.getUTCFullYear()}_${currentDate.getUTCMonth() + 1}_${currentDate.getDate()}`;

// Information of Invalidated Alert Message when agent Login 
const invalidateAgentMsgId = 'IndInflightInvalidateJob2018SEP';
const invalidateAgentMsg =  'INSPIRE Duo has been closed for New Business due to regulatory changes.<br/><br/>INSPIRE Duo CPF-OA/SA cases which are either pending for submission/not approved by Supervisor are no longer valid for submission.<br/><br/>For INSPIRE Duo Cash/SRS cases, you can still proceed to submit the cases which have been fully signed.<br/>';
const messageGroup = 'invalidateMsg';
const messageGroupPriority = 2;

const addTrxLogs = function(runTime, trxLogs, errLogs) {
  let nowString = moment().toDate().toISOString();
  let runTimeLong = runTime.getTime();
  let runTimeString = runTime.toISOString();
  return new Promise((resolve) => {
    dao.getDoc(jobDetailsDocKey, (doc) => {
      if (doc && !doc.error) {
        let trxData = _.get(doc, `log.${runTimeLong}`);
        if (trxData) {
          let currData = _.cloneDeep(trxData);
          trxData.transaction = _.has(currData, 'transaction') ? _.concat([], currData.transaction, trxLogs) : trxLogs;
          if (!_.isEmpty(errLogs)) {
            trxData.error = _.has(currData, 'error') ? _.concat([], currData.error, errLogs) : errLogs;
          }
        } else {
          trxData = Object.assign(
            {},
            {transaction: trxLogs},
            !_.isEmpty(errLogs) ? {error: errLogs} : {}
          );
        }
        trxData.completeTime = nowString;
        trxData.runTime = runTimeString;

        doc.log[runTimeLong] = trxData;

        logger.log(`Jobs :: InflightInvalidateJob :: update doc ${jobDetailsDocKey} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocKey, doc, (result) => {
          doc._rev = result.rev;
          resolve(doc);
        });
      } else if (doc && doc.error === 'not_found') {
        let newDoc = {
          log: {}
        };

        newDoc.log[runTimeLong] = Object.assign(
          {},
          {
            runTime: runTimeString,
            completeTime: nowString,
            transaction: trxLogs
          },
          !_.isEmpty(errLogs) ? { error: errLogs } : {}
        );

        logger.log(`Jobs :: InflightInvalidateJob :: new doc ${jobDetailsDocKey} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocKey, newDoc, (result) => {
          newDoc._rev = result.rev;
          resolve(newDoc);
        });
      } else {
        logger.log(`Jobs :: InflightInvalidateJob :: cannot find job details - runTime=${runTimeString} completedTime=${nowString}`);
        resolve(false);
      }
    });
  });
};

const checkIsJobExecutable = function(runTime, sysParam) {
  return new Promise((resolve, reject) => {
    let fromTime = _.get(sysParam, 'fromTime');
    let toTime = _.get(sysParam, 'toTime');
    if (fromTime && toTime) {
      let runDateTime = moment(runTime);
      // The range of fromTime and toTime needs to include the scheduleCron Time
      let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
      let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
      //call API to check if this node server can perform Invalidation or not
      if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
        callApiByGet('/fairBiJobRunner', (result) => {
          resolve(_.get(result, 'success'));
        });
      } else {
        resolve(false);
      }
    } else {
      reject(new Error(`Fail to get fromTime="${fromTime}" toTime="${toTime}" in ${sysParameter}`));
    }
  });
};

const prepareProductList = function(sysParam) {
  let promises = [];
  _.forEach(_.get(sysParam, 'handleCovCode', {}), (isHandle, covCode) => {
    if (isHandle && _.get(rules, `${covCode}.invalidateAllVersion`)) {
      promises.push(
        Promise.resolve({
          covCode,
          productVersion: 99999999999999999999
        })
      );
    } else if (isHandle) {
      promises.push(prodDao.getPlanByCovCode('01', covCode, 'B').then(doc => {
        if (doc && !doc.error) {
          return doc;
        } else {
          throw new Error(`Fail to get product for ${covCode}`);
        }
      }));
    }
  });

  return Promise.all(promises).then(products => {
    let productsVersion = {};
    _.forEach(products, product => {
      productsVersion[product.covCode] = _.get(product, 'productVersion', '1.0');
    });
    return productsVersion;
  });
};

const prepareChannels = function() {
  return new Promise((resolve, reject) => {
    agentDao.getChannels(channels => {
      if (channels) {
        resolve(channels);
      } else {
        reject(new Error('Fail to get channels'));
      }
    });
  });
};

const recursiveInvalidateCase = function (cid, invalidateList, result = [], index = 0) {
  if (index !== invalidateList.length) {
    let docId = invalidateList[index];
    return bundleDao.onInvalidateApplicationById(cid, docId).then(iResult => {
      if (iResult && !iResult.error) {
        result[index] = docId;
        return recursiveInvalidateCase(cid, invalidateList, result, index + 1);
      } else {
        throw Error(`Fail to invalidate case ${docId}`);
      }
    });
  } else {
    return result;
  }
};

const invalidateCustomer = function(runTime, cid, sysParam, channels, productsVersion) {
  return bundleDao.getCurrentBundle(cid).then(bundle => {
    if (!bundle) {
      throw new Error('Bundle is null');
    }

    const agent = {
      agentCode: bundle.agentCode,
      channel: {
        code: bundle.dealerGroup
      },
      compCode: bundle.compCode
    };

    const isFaChannel = _.get(channels, `channels.${bundle.dealerGroup}.type`) === 'FA';

    let promises = [];
              
    //get data for processing case
    _.forEach(bundle.applications, app => {
      let isApplication = _.has(app, 'applicationDocId');
      let docId = '';
      if (isApplication && (_.get(app, 'appStatus') === 'APPLYING' || _.get(app, 'appStatus') === 'SUBMITTED')) {
        docId = _.get(app, 'applicationDocId', '');
      }
      if (!isApplication) {
        docId = _.get(app, 'quotationDocId', '');
      }

      if (docId.length > 0) {
        promises.push(new Promise((resolve, reject) => {
          dao.getDoc(docId, (doc) => {
            if (doc && !doc.error) {
              resolve({docId, isApplication, doc});
            } else {
              reject(new Error(`Fail to get doc ${docId}`));
            }
          });
        }));
      }
    });

    return Promise.all(promises).then(results => {
      //check processing case

      // below are for only fully signed case, allow to proceed --hidden not for this case
      let noOffullySignedCases = _.size(
        _.filter(bundle.applications, app => {
          return app.isFullySigned;
        })
      );

      
      // let nonFullySignedCases = _.filter(bundle.applications, app => {
      //   return !app.isFullySigned && (!_.has(app, 'applicationDocId') && !_.has(app, 'appStatus') || _.get(app, 'appStatus') === 'APPLYING');
      // });

      // let checkCases = [];
      // if (fullySignedCases.length > 0 && nonFullySignedCases.length > 0) {
      //   checkCases = _.concat([], fullySignedCases, nonFullySignedCases);
      // } else if (nonFullySignedCases.length > 0) {
      //   //no fully signed case
      //   checkCases = nonFullySignedCases;
      // }
      let checkCases = bundle.applications || [];
      let handleCases = {
        isCreateNewBundle: false,
        invalidatedIdList: [],
        policyLists: []
      };
    if (checkCases.length > 0) {
        let invalidatedSigned = false;
        _.forEach(checkCases, app => {
          let isApplication = _.has(app, 'applicationDocId');
          let docId = isApplication ? _.get(app, 'applicationDocId', '') : _.get(app, 'quotationDocId', '');
          //let isFullySigned = _.get(app, 'isFullySigned', false);
          let data = _.find(results, result => {return result.docId === docId && result.isApplication === isApplication;});
          if (data) {
            let baseProductCode = isApplication ? _.get(data.doc, 'quotation.baseProductCode', '') : _.get(data.doc, 'baseProductCode', '');
            let isHandleCovCode = _.get(sysParam, `handleCovCode.${baseProductCode}`);
            let paymentMethod = isApplication ? _.get(data.doc,'quotation.policyOptions.paymentMethod') : _.get(data.doc, 'policyOptions.paymentMethod');
            let policyNumber  = isApplication ? _.get(data.doc, 'policyNumber', '') : null;
            let isFullySigned = _.get(data.doc, 'isFullySigned', false);
                if(baseProductCode === 'IND'){
                      let caseProductVersion = isApplication ? _.get(data.doc, 'quotation.productVersion', '1.0') : _.get(data.doc, 'productVersion', '1.0');
                      let currentProductVersion = _.get(productsVersion, baseProductCode);
                      let majorCaseProductVersion = Number.parseInt(caseProductVersion || 1, 10);
                      let majorCurrentProductVersion = Number.parseInt(currentProductVersion || 2, 10);
                      
                      if (isHandleCovCode && majorCaseProductVersion < majorCurrentProductVersion) {// && majorCaseProductVersion < majorCurrentProductVersion
                        if (paymentMethod === 'cpfisoa' || paymentMethod === 'cpfissa'){
                           if (_.get(app, 'appStatus') === 'SUBMITTED'){
                              handleCases.policyLists.push(policyNumber);  // expired
                          }
                          else {
                               handleCases.invalidatedIdList.push(docId);  // invalidated
                                if(isFullySigned){
                                  invalidatedSigned = true;
                                }
                          }
                        }
                        else {  //payment method SRS, cash
                          if (_.get(app, 'appStatus') === 'SUBMITTED'){
                            return;  //proceed
                          }
                          else {
                            if (isFullySigned){
                              return;  //proceed
                            } else {
                              handleCases.invalidatedIdList.push(docId);  //invalidated
                            }
                          }
                        }
                      }  
                       //if (bundle.applications.length > 0 && handleCases.invalidatedIdList.indexOf(docId) >= 0 && noOffullySignedCases && !isFaChannel) {
                       //  handleCases.isCreateNewBundle = true;
                       //}
            }
          } else {
            logger.log(`INFO: InflightInvalidateJob - no doc is found for ${docId}`);
          }
        }); 
        if(handleCases.invalidatedIdList.length > 0){
          _.forEach(checkCases, app => {
            let isApplication = _.has(app, 'applicationDocId');
            let docId = isApplication ? _.get(app, 'applicationDocId', '') : _.get(app, 'quotationDocId', '');
            let data = _.find(results, result => {return result.docId === docId && result.isApplication === isApplication;});
            if (data) {
              let isFullySigned = _.get(data.doc, 'isFullySigned', false);
              if(isFullySigned || (invalidatedSigned && !isFullySigned)){
                if (_.get(app, 'appStatus') !== 'SUBMITTED'){
                  handleCases.invalidatedIdList.push(docId);
                }
              }else{
                return;
              }
            }
          });
        }
      }
      return handleCases;
    }).then(handleCases => {
      // Expired The poclies
      let { policyLists } = handleCases;
      let policyPromises = [];
      _.each(policyLists, docId =>{
        policyPromises.push(new Promise((resolve, reject) => {
              dao.getDoc(docId, function(foundCase){
                  resolve(foundCase);
              });
          }).then(appCase => {
            if (appCase && appCase.submittedDate && ['R','A','E'].indexOf(appCase.approvalStatus) === -1) {
              appCase.approvalStatus = 'E';
              appCase.expiredDate = new Date().toISOString();
              return new Promise(resolve => {
                  dao.updDoc(docId, appCase, function(result) {
                      if (result && !result.error) {
                          resolve(docId);
                      } else {
                          resolve(false);
                      }
                  });
              });
            } else {
              return Promise.resolve();
            }
          }
        ));
      });
      return Promise.all(policyPromises).then(() =>{
        return handleCases;
      });
    }).then(handleCases => {
      if (!handleCases) {
        return null;
      }

      let { isCreateNewBundle, invalidatedIdList } = handleCases;

      if (isCreateNewBundle) {
        return bundleDao.createNewBundle(cid, agent, false).then(bundles => {
          return addTrxLogs(runTime, [{
            cid,
            bundleId: _.get(bundle, 'id'),
            agentCode: bundle.agentCode,
            action: 'createNewBundle',
            newBundleId: _.get(_.find(bundles, b => {return b.isValid; }), 'id'),
            triggerByDocIds: invalidatedIdList
          }]);
        }).then(lResult => {
          return agent.agentCode;
        });
      } else if (invalidatedIdList.length > 0) {
        return recursiveInvalidateCase(cid, invalidatedIdList).then(docIds => {
          return addTrxLogs(runTime, [{
            cid,
            bundleId: _.get(bundle, 'id'),
            agentCode: bundle.agentCode,
            action: 'invalidateApplicationById',
            triggerByDocIds: docIds
          }]);
        }).then(lResult => {
          if (!isFaChannel) {
            return bundleDao.rollbackApplication2Step1(cid);
          } else {
            return;
          }
        }).then(() => {
          return agent.agentCode;
        });
      }
      return null;
    }).then(agentCode => {
      if (agentCode) {
        return agentCode;
      } else {
        return addTrxLogs(runTime, [{
          cid,
          bundleId: _.get(bundle, 'id'),
          agentCode: _.get(bundle, 'agentCode'),
          action: 'checked'
        }]).then(lResult => {
          return null;
        });
      }
    });
  }).catch(error => {
    logger.error(`Jobs :: InflightInvalidateJob :: ERROR caught at ${moment().toDate()} - `, cid, error);

    let errors = [];
    let errorStr = error instanceof Error ? error.toString() : _.toString(error);
    let errorLog = _.isString(errorStr) ? errorStr : 'Unknown error type: ' + typeof errorStr;
    errors.push('[' + cid + '] - ' + errorLog + ' - at ' + moment().toISOString());
    return addTrxLogs(runTime, [{
      cid,
      action: 'checked',
      error: errorLog
    }], errors).then(result => {
      return null;
    });
  });
};

const recursiveInvalidateCustomer = function (runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes = [], index = 0) {
    if (index !== cids.length) {
    logger.log('Jobs :: InflightInvalidateJob :: EXECUTE Inflight Case Handling Process - START ' + (index + 1) + '/' + cids.length + ' (' + cids[index] + ')');
    return invalidateCustomer(runTime, cids[index], sysParam, channels, productsVersion).then(agentCode => {
      let willHandleNotification = handledAgentCodes.indexOf(agentCode) < 0 && agentCode;
      logger.log('Jobs :: InflightInvalidateJob :: EXECUTE Inflight Case Handling Process - END ' + (index + 1) + '/' + cids.length + ' (' + cids[index] + ')' + (willHandleNotification ? ' handleNotificationAgentCode=' + agentCode : ''));

      if (willHandleNotification) {
        handledAgentCodes.push(agentCode);
        return _.find(agents, agent => { return agent.agentCode === agentCode; });
      } else {
        return;
      }
    }).then(agent => {
      if (agent) {
        return insertNoticeToAgent(agent.id, invalidateAgentMsgId, invalidateAgentMsg, messageGroup, messageGroupPriority).then((iResult) => {
          if (iResult && !iResult.error) {
            return;
          } else {
            throw Error(`Fail to insert notification to agent ${agent.agentCode}`);
          }
        });
      } else {
        return;
      }
    }).then(() => {
      const timeout = _.get(sysParam, 'perCaseTimeout', 0);
      if (timeout) {
        async.waterfall([
          (callback) => {
            logger.log('Jobs :: InflightInvalidateJob :: Set Timeout ');
            setTimeout(function() {
              logger.log('Jobs :: InflightInvalidateJob :: Successfully callback in time out');
              callback(null, {success: true});
            }, timeout);
          }
        ], (err, result) => {
            if (err) {
              logger.error('Jobs :: InflightInvalidateJob :: Error in searchApprovalCaseByAppId: ', err);
              throw Error(`Jobs :: InflightInvalidateJob :: Fail to setTimeout ${cids[index]}`);
            } else {
              return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
            }
        });
      } else {
        return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
      }
    }).catch(error => {
      logger.error('Jobs :: InflightInvalidateJob :: Error in recursiveInvalidateCustomer: ', error);
      return recursiveInvalidateCustomer(runTime, sysParam, channels, productsVersion, cids, agents, handledAgentCodes, index + 1);
    });
  } else {
    logger.log('Jobs :: InflightInvalidateJob :: COMPLETED Inflight Case Handling');
    return;
  }
};

const executeJob = function (runTime, sysParam) {
  logger.log(`Jobs :: InflightInvalidateJob :: JOB STARTED at ${runTime}`);
  prepareChannels().then(channels => {
    return {channels};
  }).then(param => {
    return prepareProductList(sysParam).then(productsVersion => {
      return _.set(param, 'productsVersion', productsVersion);
    });
  }).then(param => {
    if (sysParam && sysParam.doDataPatch) {
      return new Promise((resolve, reject) => {
        dao.getDoc(sysParam.dataPatchCidsList, (doc) => {
          if (doc && !doc.error) {
            logger.log(`Jobs :: InflightInvalidateJob :: getAllCustomer by JSON ${doc.cids}`);
            param = _.set(param, 'cids', doc.cids);
            resolve(param);
          } else {
            reject(new Error(`Fail to get doc ${sysParam.dataPatchCidsList}`));
          }
        });
      });
    } else {
      logger.log('Jobs :: InflightInvalidateJob :: getAllCustomer by Views');
    return getAllCustomer().then(cids => {
      return _.set(param, 'cids', cids);
    });
    }
  }).then(param => {
    return getAllAgents().then(agents => {
      return _.set(param, 'agents', agents);
    });
  }).then(param => {
    return recursiveInvalidateCustomer(runTime, sysParam, param.channels, param.productsVersion, param.cids, param.agents);
  }).then(() => {
    logger.log(`Jobs :: InflightInvalidateJob :: JOB ENDED at ${moment().toDate()}`);
  }).catch(error => {
    logger.error(`Jobs :: InflightInvalidateJob :: ERROR caught at FINAL ${moment().toDate()}`, error);

    let errors = [];
    let errorStr = error instanceof Error ? error.toString() : _.toString(error);
    let errorLog = _.isString(errorStr) ? errorStr : 'Unknown error type: ' + typeof errorStr;
    errors.push(errorLog);
    addTrxLogs(runTime, [], errors);
  });
};

module.exports.execute = () => {
  return new Promise((resolve, reject) => {
    dao.getDoc(sysParameter, (param) => {
      if (param && !param.error) {
        let fairBISysParam = _.get(param, 'inflightCaseHandling');
        let scheduleCron = _.get(fairBISysParam, 'scheduleCron');
        let fromTime = _.get(fairBISysParam, 'fromTime');
        let toTime = _.get(fairBISysParam, 'toTime');

        if (fairBISysParam && scheduleCron) {
          schedule.scheduleJob(scheduleCron, (runTime) => {
            checkIsJobExecutable(runTime, fairBISysParam).then((run) => {
              let runDateTime = moment(runTime);
              let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
              let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
              if (run) {
                executeJob(runTime, fairBISysParam);
              } else if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
                logger.log(`Jobs :: InflightInvalidateJob :: JOB SKIPPED at ${runTime}`);
              }
            });
          });
          resolve();
        } else {
          reject(new Error(`Fail to get scheduleCron="${scheduleCron}" in ${sysParameter}`));
        }
      } else {
        reject(new Error('Fail to get doc', sysParameter));
      }
    });
  });
};
