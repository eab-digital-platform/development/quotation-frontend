const _ = require('lodash');
const schedule = require('node-schedule');
const moment = require('moment');
const logger = global.logger || console;

const dao = require('../cbDaoFactory').create();

const extractMissingAttachmentJson = 'extractMissingAttachmentJson';
const { checkIsJobExecutable } = require('./invalidateFunctions');

let canRun = true;
let startDate  = moment().toDate();
let endDate = moment().toDate();
let missingAttachmentData = {};
const failIds = [];
const checkMissAttachment = (docId) => new Promise((resolve) => {
 const checkDocId = `${docId}?attachments=true&revs=false&show_exp=false`;
    dao.getFromSGValidationAttachment('GET',checkDocId, (result) => {
        resolve(result);
});
});

const  executeJobDtl = (docs) =>{
  var p = Promise.resolve();
  docs.forEach(doc=>
    p = p.then(() => checkMissAttachment(doc.id).then((result)=>{
       logger.log('Jobs checkMissAttachment result = ',result);
        if (result){
          logger.log('Jobs checkMissAttachment MISS = FALSE');
        } else {
          logger.log('Jobs checkMissAttachment MISS = TRUE');
          failIds.push(doc.id);
         }
    })
  )
);
 return p;
};
const executeJob = function (runTime, sysParam) {
  logger.log( 'Jobs :: executeJob dList start' );
  canRun = false;
  missingAttachmentData = {};
  startDate  = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
  logger.log(`Jobs extractMissAttachment JOB STARTED at ${runTime}`);
  const documentName = _.get(sysParam, 'documentName');
  // const documentType = _.get(sysParam, 'documentType');
  missingAttachmentData.id = documentName;
  missingAttachmentData.startDate = startDate;
  logger.log('getViewRange start');
  dao.getViewRange(
    'dataSync',
    'getCPAllAttachmentDocument',
    '["01","allAtt"]',
    '["01","allAtt"]',
    null,
    function(dList){
      // const  testList = {
      //   'rows':[
      //     // {'id':'102-1732738'},
      //     // {'id':'102-1750573'},
      //     // {'id':'102-1752587'},
      //     // {'id':'102-1847528'},
      //     // {'id':'102-1852106'},
      //     // {'id':'102-1939341'},
      //     {'id':'102-2028706'},
      //     {'id':'102-2061889'},
      //     {'id':'102-2065658'},
      //     {'id':'102-2067977'},
      //     {'id':'102-2106890'},
      //     {'id':'102-2146771'},
      //     {'id':'102-2188484'},
      //     {'id':'102-2217564'},
      //     {'id':'102-2217580'},
      //     {'id':'102-2296279'}
      //   ]};
      if (dList && dList.rows){
        logger.log(`Jobs :: extractMissAttachment dList count ${dList.rows.length}`);
        if (dList.rows.length > 0) {
          executeJobDtl(dList.rows).then(()=>{
            endDate = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
            missingAttachmentData.endDate = endDate;
            missingAttachmentData.failIds = failIds;
            missingAttachmentData.status = 'Success';
            missingAttachmentData.message = `Jobs extractMissAttachment JOB ENDED at ${endDate}, total document scanned :${dList.rows.length}, miss count: ${failIds.length}`;
            logger.log(`Jobs extractMissAttachment missAttDocIds=${failIds.length} `);
            logger.log(`Jobs extractMissAttachment JOB ENDED at ${endDate}`);
            updateResult();
          });
        } else {
          endDate = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
          missingAttachmentData.endDate = endDate;
          missingAttachmentData.status = 'Fail';
          logger.log(`Jobs :: extractMissAttachment rows =0 JOB ENDED at ${endDate}`);
          missingAttachmentData.message = `Jobs extractMissAttachment rows =0 JOB ENDED at ${endDate}`;
          updateResult();
        }
      } else {
        endDate = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
        missingAttachmentData.endDate = endDate;
        missingAttachmentData.status = 'Success';
        logger.log(`Jobs extractMissAttachment not found document JOB ENDED at ${endDate}`);
        missingAttachmentData.message = `Jobs extractMissAttachment not found include attachment document JOB ENDED at ${endDate}`;
        updateResult();
      }
    }
  );
};

const updateResult = () =>{
  dao.getDoc(missingAttachmentData.id,(doc)=>{
    if (doc){
      missingAttachmentData._rev = doc._rev;
      dao.updDoc(missingAttachmentData.id, missingAttachmentData, updResult => {
        logger.log(`Jobs extractMissAttachment ${missingAttachmentData.id} ${updResult.ok}`);
        // canRun = true;
    });
    } else {
      dao.updDoc(missingAttachmentData.id, missingAttachmentData, updResult => {
        logger.log(`Jobs extractMissAttachment ${missingAttachmentData.id} ${updResult.ok}`);
        // canRun = true;
    });
    }
  });
};
module.exports.execute = () => {
  return new Promise((resolve, reject) => {
    dao.getDoc(_.trim(extractMissingAttachmentJson), (param) => {
    logger.log(param);
      if (param && !param.error) {
        let scheduleCron = _.get(param, 'scheduleCron');
        let fromTime = _.get(param, 'fromTime');
        let toTime = _.get(param, 'toTime');
        const allowRun = _.get(param, 'allowRun');
        logger.log(`Jobs extractMissAttachment allowRun = ${allowRun}`);
        if (scheduleCron && allowRun) {
          logger.log('Jobs extractMissAttachment start canRun=' ,canRun);
          schedule.scheduleJob(scheduleCron, (runTime) => {
            if (canRun){
              canRun = false;
              checkIsJobExecutable(runTime, param).then((run) => {
                logger.log('Jobs extractMissAttachment checkIsJobExecutable ');
                let runDateTime = moment(runTime);
                let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
                let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
                if (run) {
                  executeJob(runTime, param);
                } else if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
                  canRun = true;
                  logger.log(`Jobs extractMissAttachment JOB SKIPPED at ${runTime}`);
                }
              });
            }
          });
          resolve();
        } else {
          reject(new Error(`Jobs extractMissAttachment Fail to get scheduleCron="${scheduleCron}" in ${extractMissingAttachmentJson}`));
        }
      } else {
        reject(new Error('Jobs extractMissAttachment Fail to get doc ', extractMissingAttachmentJson));
      }
    });
  });
};
