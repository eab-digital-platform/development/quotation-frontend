package com.eab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.util.Enumeration;

import com.eab.database.CBLite;


public class Keystore {
	private final static boolean DEBUG = false;
	private final static String KEYSTORE_TYPE = "JCEKS";
	private static String MODE = "3DES";		// AES or 3DES
	
	/**
	 * Create empty Keystore file.
	 * @param filename - file name with full path
	 * @param storepass - KeyStore Password
	 * @return status
	 */
	public static boolean Create(String filename, String storepass) {
		boolean status = false;
		String alias = "dummy";
		String passphrase = null;
		Key key = null;
		byte[] encryptedPassphrase = null;
		FileOutputStream fos = null;
		KeyStore keyStore = null;
		
		try {
			if (DEBUG) System.out.println("[create] filename=" + filename);
			if (DEBUG) System.out.println("[create] storepass=" + storepass);
			
			// Create Keystore
			keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
			
			// Initialize a blank keystore
			keyStore.load(null, storepass.toCharArray());
			
			// Write Keystore file with Storepass
			fos = new FileOutputStream(filename);
			keyStore.store(fos, storepass.toCharArray());
			status = true;
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			key = null;
			encryptedPassphrase = null;
			keyStore = null;
			if (fos != null) try {fos.close();} catch(Exception e) {if (DEBUG) e.printStackTrace();}
			fos = null;
		}
		
		return status;
	}
	
	/**
	 * Load KeyStore from File.
	 * @param filename - file name with full path
	 * @param storepass - KeyStore Password
	 * @return KeyStore
	 */
	public static KeyStore Load(String filename, String storepass) {
		KeyStore keystore = null;
		InputStream fis = null;
		
		try {
			keystore = KeyStore.getInstance(KEYSTORE_TYPE);
			fis = new FileInputStream(filename);
			keystore.load(fis, storepass.toCharArray());
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			try {if (fis != null) fis.close();} catch(Exception e) {}
			fis = null;
		}
		
		return keystore;
	}
	
	/**
	 * Save KeyStore into File (overwrite if exist).
	 * @param filename - file name with full path
	 * @param storepass - KeyStore Password
	 * @param keystore - KeyStore
	 * @return status
	 */
	public static boolean Save(String filename, String storepass, KeyStore keystore) {
		boolean status = false;
		FileOutputStream fos = null;
		File keystoreFile = null;
		
		try {
			keystoreFile = new File(filename);
			fos = new FileOutputStream(keystoreFile);
			keystore.store(fos, storepass.toCharArray());
			status = true;
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			try {if (fos != null) fos.close();} catch(Exception e) {}
			fos = null;
		}
		
		return status;
	}
	
	/**
	 * Check existing Key Alias in KeyStore.
	 * @param keystore - KeyStore
	 * @param alias - Key Alias
	 * @return status
	 */
	public static boolean CheckAlias(KeyStore keystore, String alias) {
		boolean status = false;
		Enumeration<String> aliases = null;
		
		try {
			aliases = keystore.aliases();
			while (aliases.hasMoreElements()) {
				if (alias.equals( aliases.nextElement() )) {
					status = true;
					break;
				}
			}
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return status;
	}
	
	/**
	 * Delete Key Alias in KeyStore
	 * @param keystore - KeyStore
	 * @param alias - Key Alias
	 * @return status
	 */
	public static boolean DeleteAlias(KeyStore keystore, String alias) {
		boolean status = false;
		
		try {
			keystore.deleteEntry(alias);
			status = true;
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return status;
	}
	
	/**
	 * Import Key Alias into KeyStore
	 * @param filename - KeyStore file name with full path
	 * @param storepass - KeyStore Password
	 * @param alias - Name of Key Alias
	 * @param passphrase - Pass Phrase
	 * @param keypass - Key Alias Password
	 * @return status
	 */
	public static boolean ImportPassphrase(String filename, String storepass, String alias, String passphrase, String keypass) {
		boolean status = false;
		KeyStore keystore = null;
		char[] password = null;
		
		try {
			if (DEBUG) System.out.println("[importPassphrase] filename=" + filename);
			if (DEBUG) System.out.println("[importPassphrase] storepass=" + storepass);
			if (DEBUG) System.out.println("[importPassphrase] alias=" + alias);
			if (DEBUG) System.out.println("[importPassphrase] passphrase=" + passphrase);
			if (DEBUG) System.out.println("[importPassphrase] keypass=" + keypass);
			
			if (keypass != null && keypass.length() > 0)
				password = keypass.toCharArray();
			
			keystore = Load(filename, storepass);
			if (DEBUG) System.out.println("[importPassphrase] keystore is " + (keystore==null?"is null":"is not null") + ".");
			
			if (keystore != null) {
				// Find duplicate Alias inside KeyStore
				if (CheckAlias(keystore, alias)) {
					DeleteAlias(keystore, alias);
					if (DEBUG) System.out.println("[importPassphrase] Deleted existing alias " + alias + "."); 
				}
				
				Key importKey = MODE.equals("3DES") ? TripleDES.ImportKey(passphrase) : AES.ImportKey(passphrase);
				if(importKey == null)
					return false;
				else
					keystore.setKeyEntry(alias, importKey, password, null);
				// Add PassPhrase into KeyStore
//				
//					
//				if (MODE.equals("3DES"))
//					
//					keystore.setKeyEntry(alias, TripleDES.ImportKey(passphrase), password, null);
//				else
//					keystore.setKeyEntry(alias, AES.ImportKey(passphrase), password, null);
				
				// Save updated KeyStore into file
				status = Save(filename, storepass, keystore);
				if (DEBUG) System.out.println("[importPassphrase] saveKeyStore status is " + status + ".");
			}
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			keystore = null;
			password = null;
		}
		
		return status;
	}
	
	/*
	 *only get first 11 char from the encrypted string 
	 */		
	public static boolean ImportOffLineKey(String filename, String storepass, String alias, String passphrase, String keypass) {
		String keyString = passphrase.substring(0, 11);
		boolean status = false;
		KeyStore keystore = null;
		char[] password = null;
		
		try {
			if (keypass != null && keypass.length() > 0)
				password = keypass.toCharArray();
			
			keystore = Load(filename, storepass);
			
			if (keystore != null) {
				// Find duplicate Alias inside KeyStore
				if (CheckAlias(keystore, alias)) {
					DeleteAlias(keystore, alias);
				}
				
				Key importKey = CustomTripleDES.ImportKey(keyString);
				if(importKey == null)
					return false;
				else
					keystore.setKeyEntry(alias, importKey, password, null);// Add PassPhrase into KeyStore
				
				// Save updated KeyStore into file
				status = Save(filename, storepass, keystore);
				if (DEBUG) System.out.println("[importPassphrase] saveKeyStore status is " + status + ".");
			}
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			keystore = null;
			password = null;
		}
		
		return status;
	}
	
	public static String ExportPasspharase(String filename, String storepass, String alias, String keypass) {
		String output = null;
		KeyStore keystore = null;
		char[] password = null;
		Key key = null;
		
		try {
			if (keypass != null && keypass.length() > 0)
				password = keypass.toCharArray();
			
			keystore = Load(filename, storepass);
			key = keystore.getKey(alias, password);
			if (MODE.equals("3DES"))
				output = TripleDES.ExportKey(key);
			else
				output = AES.ExportKey(key);
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			keystore = null;
			password = null;
		}
		
		return output;
	}
	
	public static boolean connectCBL(String dbname, String filename, String storepass, String alias, String keypass){
		String key = ExportPasspharase(filename, storepass, alias, keypass);
		if(key != null){
			boolean connect = CBLite.Connect(dbname, key);
			return connect;
		}else{
			return false;
		}
	}
	
	public static String connectCBL(String filename, String storepass, String alias, String keypass){
		String key = ExportPasspharase(filename, storepass, alias, keypass);
		return key;
	}
	
	
	public static boolean ChangeStorePass(String filename, String storepass, String newpass) {
		boolean status = false;
		
		KeyStore keystore = Load(filename, storepass);
		
		if (keystore != null) {
			status = Save(filename, newpass, keystore);
		}
		
		return status;
	}
}
