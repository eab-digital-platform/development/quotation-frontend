package com.eab;

import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.SecureRandom;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
	private final static boolean DEBUG = false;
	private final static String CRYPTOKEY_ALGORITHM = "AES";
	private final static String CRYPTOKEY_TRANSFORMATION = "AES/ECB/PKCS5Padding";
	private final static int CRYPTOKEY_LENGTH = 256;		// 128 bit is minimum.  192 and 256 bits may not be available
    
    public static Key GenKey() {
    	KeyGenerator keyGenerator = null;
		Key key = null;
		
		try {
			keyGenerator = KeyGenerator.getInstance(CRYPTOKEY_ALGORITHM);
			keyGenerator.init(CRYPTOKEY_LENGTH, new SecureRandom());
			key = keyGenerator.generateKey();
			if (DEBUG) System.out.println("Key(Hex)=" + Util.ByteToHex(key.getEncoded()));
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		} finally {
			keyGenerator = null;
		}
		
		return key;
    }
    
    public static Key ImportKey(String text) {
		Key key = null;
		
		try {
			if (text != null && text.getBytes("UTF8").length == (CRYPTOKEY_LENGTH / 8)) {
				key = new SecretKeySpec(text.getBytes("UTF8"), CRYPTOKEY_ALGORITHM);
			}
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return key;
    }
    
	public static String ExportKey(Key key) {
		try {
			if (key != null)
				return new String(key.getEncoded(), "UTF8");
		} catch(Exception e) {
			if (DEBUG) e.printStackTrace();
		}
		
		return null;
	}
	
//	public static ArrayList<byte[]> Encrypt(Key key, String passphrase) {
//		ArrayList<byte[]> output = null;
//		Cipher cipher = null;
//		byte[] cipherText = null;
//		byte[] iv = null;
//		AlgorithmParameters params = null;
//		
//		try {
//			// Create a cipher using that key to initialize it
//		    cipher = Cipher.getInstance(CRYPTOKEY_TRANSFORMATION);
//		    cipher.init(Cipher.ENCRYPT_MODE, key);
//		    
//		    // Generate initialization vector (IV)
//		    params = cipher.getParameters();
//		    iv = params.getParameterSpec(IvParameterSpec.class).getIV();
//		    if (DEBUG) System.out.println("Initialization Vector: " + Util.ByteToHex(iv));
//		    
//		    // Perform the actual encryption
//		    cipherText = cipher.doFinal(passphrase.getBytes("UTF8"));
//		    if (DEBUG) System.out.println("Encrypted Key: " + Util.ByteToHex(cipherText));
//		    
//		    output = new ArrayList<byte[]>();
//		    output.add(cipherText);
//		    output.add(iv);
//		} catch(Exception e) {
//			if (DEBUG) e.printStackTrace();
//		}
//		
//		return output;
//	}
	
//	public static String Decrypt(Key key, byte[] cipherText, byte[] iv) {
//		String output = null;
//		Cipher cipher = null;
//		byte[] result = null;
//		
//		try {
//			cipher = Cipher.getInstance(CRYPTOKEY_TRANSFORMATION);
//			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
//			
//			result = cipher.doFinal(cipherText);
//			
//			if (result != null)
//				output = new String(result, "UTF8");
//		} catch(Exception e) {
//			if (DEBUG) e.printStackTrace();
//		}
//		
//		return output;
//	}
}
