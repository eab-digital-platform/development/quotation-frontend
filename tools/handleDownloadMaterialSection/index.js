const fs = require('fs');
const path = require('path');
const process = require('process');
const moveFrom = path.join(__dirname, '/originalFiles');
const moveTo = path.join(__dirname, '../../CB_resources/files/material');
const materialsText = path.join(__dirname, '../../CB_resources/materials.txt');
const _ = require('lodash');

const writeFile = (file, pathToSave) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(pathToSave, JSON.stringify(file, null, 4), 'utf8', (saveErr) => {
            if (saveErr) {
                reject(`save error ${saveErr}`);
            } else {
                resolve();
            }
        });
    }).catch(err => {
        console.log(err);
    });
};

/**
 * node index.js Funds
 */
const main = () => {
    const paras = process.argv;

    let materialMapping = {};
    var lineReader = require('readline').createInterface({
        input: require('fs').createReadStream(materialsText)
    });
    lineReader.on('line', function (line) {
        if (line.split('\t').length === 4) {
            materialMapping[line.split('\t')[3]] = line.split('\t')[1];
        }
    }).on('close', () => {
        // Loop through all the files in the temp directory
        fs.readdir(`${moveFrom}/${paras[2]}`, function (err, files) {
            if (err) {
                console.error('Could not list the directory.', err);
                process.exit(1);
            }
            files.forEach(function (file, index) {
                let fromPath = path.join(moveFrom, `${paras[2]}/${file}`);
                fs.stat(fromPath, async function (error, stat) {
                    if (stat.isFile && path.extname(fromPath) === '.pdf' || path.extname(fromPath) === '.PDF') {
                        let pdfFileName = file.split('_')[0] + '.pdf';
                        let toPath = path.join(moveTo, pdfFileName);
                        if (materialMapping[pdfFileName]) {
                            let mappedJsonPath = path.join(moveTo, materialMapping[pdfFileName] + '.json');
                            let mappedJson = require(mappedJsonPath);
                            mappedJson.releaseDate = new Date().toISOString();
                            await writeFile(mappedJson, mappedJsonPath);
                            await fs.rename(fromPath, toPath, () =>{});
                        } else {
                            console.log(`No JSON Found, need to create one ${pdfFileName}`);
                        }
                    }
                });
            });
        });
    });
};

main();
