const _ = require('lodash');
const csv = require('csvtojson');
const fs = require('fs');
const moment = require('moment');


const main = function () {
    const quotationList = require('./files/shieldQuotation_prod');
    const policyList = require('./files/shieldPolicy_prod');
    const csvFilePath = './files/shieldCases_prod.csv';

    let pVer2QuotationList = [];
    let pVer1QuotationList = [];
    // Prepare the impacted quoation List;
    _.each(quotationList && quotationList.rows, row => {
        let impactedCase = false;
        _.each(_.get(row, 'value.insureds'), (clientObj = []) => {
            _.each(clientObj.plans, plan => {
                if (plan.planCode === 'ASPA' || plan.planCode === 'ASPB') {
                    impactedCase = true;
                }
            });
        });
        if (_.get(row, 'value.productVersion') === '2.0' && impactedCase) {
            pVer2QuotationList.push(_.get(row, 'id'));
        } else if (impactedCase) {
            pVer1QuotationList.push(_.get(row, 'id'));
        }
    });

    // Export the quotation list as txt file
    let quotListVerion2 = fs.createWriteStream('version2QuotationList.txt');
    let quotListVerion1 = fs.createWriteStream('version1QuotationList.txt');

    let exceptionPolicyList = fs.createWriteStream('exception_Policy_list.csv');
    let impactedList = fs.createWriteStream('impactedPolicyMapping.csv');
    _.each(pVer2QuotationList, id => {
        quotListVerion2.write(`${id} \n`);
    });

    _.each(pVer1QuotationList, id => {
        quotListVerion1.write(`${id} \n`);
    });

    convertCsvtoJson(csvFilePath).then(jsonArray =>{
        let policyStatusMapping = {};
        _.each(jsonArray, oracleRecord => {
            let policyNumber = _.get(oracleRecord, 'pol_num');
            let oracleCreateTime = _.get(oracleRecord, 'create_dt');
            
            let createDate = moment(oracleCreateTime).toDate() || new Date();
            
            let tranformedResult = {
                eappNo: _.get(oracleRecord, 'eapp_no'),
                policyNumber: _.get(oracleRecord, 'pol_num'),
                policyStatus: _.get(oracleRecord, 'pol_status'),
                createDate,
                createDateinLongType: createDate.getTime(),
                createDateInISOString: createDate.toISOString()
            };
            
            if (policyNumber && policyStatusMapping[policyNumber]) {
                policyStatusMapping[policyNumber].push(tranformedResult);
            } else if (policyNumber) {
                policyStatusMapping[policyNumber] = [];
                policyStatusMapping[policyNumber].push(tranformedResult);
            }

            if (policyNumber) {
                policyStatusMapping[policyNumber] = policyStatusMapping[policyNumber].sort(function(a, b) {
                    return a.createDateinLongType - b.createDateinLongType;
                });
            }
        });
        impactedList.write('Policy_Number,Existing_Policy_Status,Original_Policy_Status,RLS_Submitted_Date_With_Time,RLS_Submitted_Date,Product_Version\n');
        let resultJSON = {impactedCases: []};
        _.each(policyList && policyList.rows, row => {
            let policyStatus = '';
            let policyExistingStatus = _.get(row, 'value.approvalStatus');
            let policyQuotationId = _.get(row, 'value.quotationDocId');
            let masterApprovalId = _.get(row, 'id');
            let masterApprovalIdAdded = false;
            _.each(_.get(row, 'value.policiesMapping'), policyObj => {
                let policyNumber = _.get(policyObj, 'policyNumber');
                let originalPolicyStatus = _.get(policyStatusMapping[policyNumber], '[0].policyStatus');
                let originalPolicyRLSSubmittedDate = _.get(policyStatusMapping[policyNumber], '[0].createDate');
                let rlsSubmittedDateISOString = _.get(policyStatusMapping[policyNumber], '[0].createDateInISOString');
                
                originalPolicyStatus = converOracleStatus(originalPolicyStatus);
                if (policyStatus !== '' && policyStatus !== originalPolicyStatus && (pVer2QuotationList.indexOf(policyQuotationId) > -1 || pVer1QuotationList.indexOf(policyQuotationId) > -1)) {
                    exceptionPolicyList.write(`${policyNumber} \n`);
                } else if ((originalPolicyStatus !== policyExistingStatus || originalPolicyStatus === 'E') && policyExistingStatus === 'E' && (pVer2QuotationList.indexOf(policyQuotationId) > -1 || pVer1QuotationList.indexOf(policyQuotationId) > -1)) {
                    policyStatus = originalPolicyStatus;
                    impactedList.write(`${policyNumber},${convertStatus(policyExistingStatus)},${convertStatus(originalPolicyStatus)},${originalPolicyRLSSubmittedDate},${originalPolicyRLSSubmittedDate.getFullYear()}-${originalPolicyRLSSubmittedDate.getMonth() + 1}-${originalPolicyRLSSubmittedDate.getDate()},${pVer2QuotationList.indexOf(policyQuotationId) > -1 ? '2.0' : '1.0'}\n`);
                    if (!masterApprovalIdAdded) {
                        resultJSON.impactedCases.push({
                            docId: masterApprovalId,
                            originalPolicyStatus: originalPolicyStatus,
                            rlsSubmittedDate: rlsSubmittedDateISOString,
                            specialHandle: pVer2QuotationList.indexOf(policyQuotationId) > -1 ? true : false
                        });
                    }

                    resultJSON.impactedCases.push({
                        docId: policyNumber,
                        originalPolicyStatus: originalPolicyStatus,
                        rlsSubmittedDate: rlsSubmittedDateISOString,
                        specialHandle: pVer2QuotationList.indexOf(policyQuotationId) > -1 ? true : false
                    });

                    masterApprovalIdAdded = true;
                }
            });
        });

        const strignifiedResult = JSON.stringify(resultJSON);
        fs.writeFile('datapatch.json', strignifiedResult, 'utf8', () => {
            console.log('Completed Write JSON');
            const datapatch = require('./datapatch');
            console.log(datapatch.impactedCases.length);
        });

    });
};

const convertCsvtoJson = function(filePath) {
    return new Promise(resolve => {
        csv().fromFile(filePath).then(jsonArray => {
            resolve(jsonArray);
        })
    }).catch(error => {
        console.error(`Conver JSON Error :: ${error}`);
    });
};

const converOracleStatus = function(status) {
    switch (status) {
        case 'Approved': 
            return 'A';
        case 'Reject':
            return 'R';
        case 'Expired':
            return 'E'
        case 'Invalidated Paid ':
            return 'Invalidated Paid';
        case 'Invalidated':
            return 'Invalidated';
        case 'API_RESUBMISSION':
            return 'API_RESUBMISSION';
        default:
            return '';
    }
};

const convertStatus = function(status) {
    switch (status) {
        case 'A': 
            return 'Approved';
        case 'R':
            return 'Reject';
        case 'E':
            return 'Expired'
        case 'Invalidated Paid ':
            return 'Invalidated Paid';
        case 'Invalidated':
            return 'Invalidated';
        case 'API_RESUBMISSION':
            return 'API_RESUBMISSION';
        default:
            return '';
    }
};

main();
