var fs = require('fs');
const Schema = require('./Config/Schema.json');

const INPUTFOLDERPATH = './Input';
const OUTPUTFOLDERPATH = './Output';
const SCHEMADEFAULTCOUNT = 120;

const main = function () {
  const paras = process.argv;
  if (paras) {
    for (var i = 1; i < paras.length; i++) {
      if (paras[i] === 'basicPlan') {
        runForOne(paras[++i].toUpperCase());
      } else if (paras[i] === 'all') {
        runForAll();
      }
    }
  }
  console.log(
    'Usage: node ExtractProductRates.js [OPTION] basicPlan COV_CODE\n' +
    '  or:  node ExtractProductRates.js [OPTION] all'
  );
};


const runForAll = function() {
  if (!fs.existsSync(INPUTFOLDERPATH)) {
    console.log("no dir ",INPUTFOLDERPATH);
    return;
  }

  var files=fs.readdirSync(INPUTFOLDERPATH);
  for(var i=0; i<files.length; i++){
    var fileName = files[i];
    if (fileName.indexOf('.json') > -1 || fileName.indexOf('.JSON') > -1) {
      console.log('-- found: ',fileName);
      const path = INPUTFOLDERPATH + '/' + fileName;
      runFile(path, fileName);
    }
  }
};

const runForOne = function(productCode) {
  var files=fs.readdirSync(INPUTFOLDERPATH);
  var result;
  var foundObjIndex = -1;

  for(var i=0; i<files.length; i++){
    var fileName = files[i];
    if (fileName.indexOf('.json') > -1 || fileName.indexOf('.JSON') > -1) {
      const path = INPUTFOLDERPATH + '/' + fileName;
      var inputObj = JSON.parse(fs.readFileSync(path));
      
      if (inputObj.covCode === productCode) {
        if (foundObjIndex > -1) {
          console.log('There are two conflicted files referring to same product file ' + fileName + ' and ' + files[foundObjIndex]);
          return;
        } else {
          foundObjIndex = i;
          result = path;
        }
      }
    }
  }

  if (foundObjIndex > -1) {
    console.log('File ' + files[foundObjIndex] + ' is found');
    runFile(result, files[foundObjIndex]);
  } else {
    console.log('Cannot find product Json for ' + productCode);
  }
};


const runFile = function(path, fileName) {
  if (!fs.existsSync(path)) {
    console.log("no dir ",path);
    return;
  }

  var inputObj = JSON.parse(fs.readFileSync(path));

  if (checkProductJsonStructure(fileName, inputObj)) {
    var productName = inputObj.covCode;
    var ratesTables = inputObj.rates;
    for (var tableName in ratesTables) {
      generateCSV(productName, tableName, ratesTables[tableName]);
    }
  }
};


const checkProductJsonStructure = function(fileName, inputObj) {
  if (inputObj.rates && inputObj.covCode) {
    return true;
  } else {
    console.log('File ' + fileName + '\'s data format is not correct. please check README file.');
    return false;
  }
};


const generateCSV = function(productName, tableName, rates) {
  var importFile = null;
  var fileName = OUTPUTFOLDERPATH + '/' + productName + '_' + tableName + '.csv';
  var csvString = '';

  importFile = fs.createWriteStream(fileName);

  console.log('CSV ' + fileName + ' generated');

  // csvString += writeSchema(productName, tableName);

  if (isSingleLevel(rates)) {
    csvString += writeTable(rates);
  } else {
    for (var key in rates) {
      var singleRate = rates[key];
      if (key === 'categories') {
        // Special handling for categories case, categories has one more level, which actually is 2 levels
        for (var nextLevelKey in singleRate) {
          var nextLevelRate = singleRate[nextLevelKey];
          csvString += writeRow(nextLevelKey);
          csvString += writeTable(nextLevelRate);
        }
      } else {
        csvString += writeRow(key);
        csvString += writeTable(singleRate); 
      }
    }
  }

  importFile.write(csvString);
};


const writeSchema = function (productName, tableName) {
  var csvString = '';
  var schemaList = [];

  if (Schema[productName] && Schema[productName][tableName] && getDataType(Schema[productName][tableName]) === 'array') {
    schemaList = Schema[productName][tableName]
  } else {
    console.log('Schema Data formate is not correct');
    for (var i=1; i<=SCHEMADEFAULTCOUNT; i++) {
      schemaList.push(i);
    }
  }

  for (var index in schemaList) {
    csvString += schemaList[index] + ',';
  }
  csvString = csvString.slice(0, -1);
  csvString += '\n';
  return csvString;
};


const isSingleLevel = function (rates) {
  for (var key in rates) {
    return getDataType(rates[key]) !== 'object';
    break;
  }
};


const writeRow = function(row) {
  if (getDataType(row) === 'array') {
    return row.join(',') + '\n';
  } else {
    return row + '\n';
  }
};


const writeTable = function(rates) {
  var csvString = '';

  for (var key in rates) {
    var row = rates[key];
    csvString += key + ',';
    csvString += writeRow(row);
  }

  csvString += '\n\n';

  return csvString;
};


const getDataType = function(data) {
  if (data instanceof Array) {
    return 'array';
  } else if (typeof data === 'object') {
    return 'object';
  } else {
    return 'others';
  }
};


main();