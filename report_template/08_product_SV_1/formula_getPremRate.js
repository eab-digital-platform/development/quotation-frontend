function(quotation, planInfo, planDetail, age) {
  var rateKey = 'SVNR_';
  if (planInfo.premTerm.endsWith('_YR')) {
    rateKey += Number.parseInt(planInfo.premTerm) + 'P_';
  } else {
    rateKey += 'RP_';
  }
  rateKey += quotation.iGender;
  rateKey += quotation.iSmoke === 'Y' ? 'S' : 'NS';
  var term = planInfo.premTerm.endsWith('_YR');
  var rates = planDetail.rates.premRate[rateKey];
  var premRate = math.bignumber(rates[Number.parseInt(planInfo.policyTerm)][age] || 0);
  return math.number(premRate);
}