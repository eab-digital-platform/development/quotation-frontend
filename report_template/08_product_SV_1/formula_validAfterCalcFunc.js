function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  if (planInfo.premium) {
    var mapping = planDetail.planCodeMapping;
    for (var i = 0; i < mapping.planCode.length; i++) {
      if (planInfo.premTerm === mapping.premTerm[i]) {
        planInfo.planCode = mapping.planCode[i];
        break;
      }
    }
    planInfo.policyTermYr = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    if (planInfo.premTerm === 'SP') {
      planInfo.premTermYr = 1;
    } else if (planInfo.premTerm.endsWith('_YR')) {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
  } else {
    planInfo.planCode = null;
  }
}