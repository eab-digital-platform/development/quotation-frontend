function(quotation, planInfo, planDetails, extraPara) { /* CRITICARE COVER FAIR PREPAREREPORTDATA */
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  var numberTrunc = function(value, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return Number.parseInt(value);
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return Number.parseFloat(value);
    }
  };
  var paddingZeroRight = function(value) {
    value = value.toString();
    if (value.indexOf('.') > -1) {
      var decimal = value.split('.');
      if (decimal[1].length === 1) {
        decimal[1] = decimal[1] + '0';
        return decimal[0] + '.' + decimal[1];
      } else {
        return value;
      }
    } else {
      return value + '.00';
    }
  };
  let company = extraPara.company;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = company;
  var retVal = {};
  if (company) {
    var retVal = {
      'compName': compName,
      'compRegNo': compRegNo,
      'compAddr': compAddr,
      'compAddr2': compAddr2,
      'compTel': compTel,
      'compFax': compFax,
      'compWeb': compWeb,
    };
  }
  retVal.proposerGenderTitle = (quotation.pGender) == 'M' ? 'Male' : 'Female';
  retVal.proposerSmokerTitle = (quotation.pSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.insurerGenderTitle = (quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.insurerSmokerTitle = (quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  var ccy = quotation.ccy;
  var iAge = quotation.iAge;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'A') {
    paymentModeTitle = 'Annual';
  } else if (quotation.paymentMode == 'S') {
    paymentModeTitle = 'Semi-Annual';
  } else if (quotation.paymentMode == 'Q') {
    paymentModeTitle = 'Quarterly';
  } else if (quotation.paymentMode == 'M') {
    paymentModeTitle = 'Monthly';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  var plans = quotation.plans;
  retVal.totalAPrem = quotation.totYearPrem;
  retVal.totalSPrem = quotation.totHalfyearPrem;
  retVal.totalQPrem = quotation.totQuarterPrem;
  retVal.totalMPrem = quotation.totMonthPrem;
  var planCodes = "";
  var plans = quotation.plans;
  for (var i = 0; i < plans.length; i++) {
    planCodes = planCodes + plans[i].planCode + ", ";
  }
  var planFields = [];
  var totalDistributionCostArr = [];
  var totalPremiumPaidArr = [];
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    var policyTerm = plan.policyTerm;
    var covCode = plan.covCode;
    var sumAssured = getCurrency(plan.sumInsured, ' ', 0);
    var planDetail = planDetails[covCode];
    var policyTermList = planDetail.inputConfig.policyTermList;
    var planInd = planDetail.planInd;
    var planName = !plan.covName ? "" : (typeof plan.covName == 'string' ? plan.covName : (plan.covName.en || plan.covName[Object.keys(plan.covName)[0]]));
    var polTermTitle = -1;
    for (var j = 0; j < policyTermList.length; j++) {
      var polTermOpt = policyTermList[j];
      if (polTermOpt.value == policyTerm) polTermTitle = polTermOpt.title;
    }
    if (polTermTitle != -1) {
      var polTerm = {
        covCode: covCode,
        planInd: planInd,
        policyTerm: polTermTitle,
        permTerm: polTermTitle,
        sumAssured: sumAssured,
        planName: planName
      };
      planFields.push(polTerm);
    }
  }
  retVal.planFields = planFields;
  retVal.planCodes = planCodes.substring(0, planCodes.length - 2);
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  retVal.iDob = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.proposerDob = new Date(quotation.pDob).format(extraPara.dateFormat);
  retVal.insurerDob = new Date(quotation.iDob).format(extraPara.dateFormat);
  for (var i = 0; i < extraPara.illustrations[quotation.plans[0].covCode].length; i++) {
    var age = iAge + i + 1;
    if (age <= 65) {
      totalDistributionCostArr.push(extraPara.illustrations[quotation.plans[0].covCode][i].tdc);
      totalPremiumPaidArr.push(extraPara.illustrations[quotation.plans[0].covCode][i].totYearPrem);
    }
  }
  var actualPolTerm;
  var policyTerm = quotation.plans[0].policyTerm;
  if (policyTerm && policyTerm.indexOf('_YR') > -1) {
    actualPolTerm = parseInt(policyTerm);
  } else if (policyTerm && policyTerm.indexOf('_TA') > -1) {
    actualPolTerm = 65 - quotation.iAge;
  }
  retVal.basicPlanName = quotation.plans[0].covName.en;
  retVal.basicPlanPremTermDesc = quotation.plans[0].premTermDesc || 'N/A';
  retVal.basicPlanPolicyTermDesc = quotation.plans[0].polTermDesc || 'N/A';
  retVal.basicPlanCurrency = polCcy; /* retVal.HighIRR = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE.HighIRR; retVal.LowIRR = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE.LowIRR; retVal.Year1_IR = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['Year1_IR']; retVal.Year2_IR = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['Year2_IR']; retVal.Year3_IR = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['Year3_IR']; retVal.IR_Year1 = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['IR_Year1']; retVal.IR_Year2 = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['IR_Year2']; retVal.IR_Year3 = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['IR_Year3']; retVal.IRaveragedLast3Years = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['IR_Averaged over the last 3 years']; retVal.IRaveragedLast5Years = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['IR_Averaged over the last 5 years']; retVal.IRaveragedLast10Years = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['IR_Averaged over the last 10 years']; retVal.Year1_TER = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['Year1_TER']; retVal.Year2_TER = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['Year2_TER']; retVal.Year3_TER = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['Year3_TER']; retVal.TER_Year1 = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['TER_Year1']; retVal.TER_Year2 = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['TER_Year2']; retVal.TER_Year3 = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['TER_Year3']; retVal.TERAveragedLast3Years = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['TER_Averaged over the last 3 years']; retVal.TERAveragedLast5Years = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['TER_Averaged over the last 5 years']; retVal.TERAveragedLast10Years = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE['TER_Averaged over the last 10 years']; /** retVal.YTM_H = numberTrunc(math.multiply(math.bignumber(extraPara.illustrations[quotation.plans[0].covCode][0].reductYield[4.75]), 100), 2); retVal.YTM_L = numberTrunc(math.multiply(math.bignumber(extraPara.illustrations[quotation.plans[0].covCode][0].reductYield[3.25]), 100), 2); */ /* retVal.YTM_H = extraPara.illustrations.secondTdc.REDUC_YIELD_H; retVal.YTM_L = extraPara.illustrations.secondTdc.REDUC_YIELD_L; */
  retVal.Basic_Total_Distribution = numberTrunc(totalDistributionCostArr[actualPolTerm - 1], 0);
  retVal.totalBasicPremium = numberTrunc(totalPremiumPaidArr[actualPolTerm - 1], 0);
  retVal.totalBasicTDC_Premium = paddingZeroRight(math.number(numberTrunc(math.multiply(math.divide(math.bignumber(retVal.Basic_Total_Distribution), math.bignumber(retVal.totalBasicPremium)), 100), 2)));
  retVal.Basic_Total_Distribution = getCurrency(retVal.Basic_Total_Distribution, ' ', 0);
  return retVal;
}