function(quotation, planInfo, planDetails, extraPara) {
  /*AWT_MAINBI_PDF*/
  var illustrations = extraPara.illustrations[planInfo.covCode];
  var deathBenefit = [];
  var deathBenefitLast = [];
  var surrenderValue = [];
  var surrenderValueLast = [];
  var deductions = [];
  var deductionsLast = [];
  var totalDistributionCost = [];
  var totalDistributionCostLast = [];
  var afterWithdrawal = [];
  var dbsvDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    guaranteed: '',
    nonGuaranteedLow: '',
    totalLow: '',
    nonGuaranteedHigh: '',
    totalHigh: ''
  };
  var deductionsDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    valueOfPremiumPaidToDateLow: '',
    effectOfDeductionToDateLow: '',
    totalSurrenderValueLow: '',
    valueOfPremiumPaidToDateHigh: '',
    effectOfDeductionToDateHigh: '',
    totalSurrenderValueHigh: ''
  };
  var totalDistributionCostDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    totalDistributionCostToDate: ''
  };
  var planDetail = planDetails[planInfo.covCode];
  var aror = {
    low: planDetail.rates.aror.aror1 * 100,
    high: planDetail.rates.aror.aror2 * 100,
    lowStr: getCurrency(planDetail.rates.aror.aror1 * 100, '', 2),
    highStr: getCurrency(planDetail.rates.aror.aror2 * 100, '', 2)
  };
  var reduceYieldYear = math.max(20, 65 - quotation.iAge);
  var rYieldValue = 0;
  var rYieldReducedReturnRate = 0;
  var incRowCnt = 0;
  if (illustrations instanceof Array) {
    var hasWithdrawal = quotation.policyOptions.wdSelect;
    var wdStartIndex = hasWithdrawal ? illustrations.findIndex(function(illustration) {
      return illustration.age === quotation.policyOptions.wdFromAge;
    }) + 1 : illustrations.length;
    illustrations.forEach(function(illustration, i) {
      var policyYear = illustration.policyYear;
      var age = illustration.age;
      var policyYearAge = illustration.policyYear + ' / ' + illustration.age;
      var iTotalPremiumPaidToDate = illustration.totalPremiumPaidToDate;
      var iGuaranteedDeathBenefit = illustration.guaranteedDeathBenefit;
      var iNonGuaranteedDeathBenefit = illustration.nonGuaranteedDeathBenefit;
      var iTotalDeathBenefit = illustration.totalDeathBenefit;
      var iSurrenderValue = illustration.surrenderValue;
      var iTotalAccountValue = illustration.totalAccountValue;
      var iAccumulatedPremPaid = illustration.accumulatedPremPaid;
      var iEffectOfDeduction = illustration.effectOfDeduction;
      var iAccumulatedTotalDistCost = illustration.accumulatedTotalDistCost;
      var iAnnualWithdrawalAmount = illustration.annualWithdrawalAmount;
      var iNonGuaranteedAccValueAfterWD = illustration.nonGuaranteedAccValueAfterWD;
      var iTotalDeathBenefitAfterWD = illustration.totalDeathBenefitAfterWD;
      var iRYieldTotalEffectDeduction = illustration.rYieldTotalEffectDeduction;
      var iRYieldReducedReturnRate = illustration.rYieldReducedReturnRate;
      incRowCnt++;
      var incRowCntLineBreak = false;
      if (incRowCnt !== 0 && incRowCnt % 5 === 0 && incRowCnt !== illustrations.length) {
        incRowCntLineBreak = true;
      }
      var deathBenefitRow = Object.assign({}, dbsvDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(iTotalPremiumPaidToDate, '', 0),
        guaranteed: getCurrency(iGuaranteedDeathBenefit, '', 0),
        nonGuaranteedLow: getCurrency(iNonGuaranteedDeathBenefit[aror.low], '', 0),
        totalLow: getCurrency(iTotalDeathBenefit[aror.low], '', 0),
        nonGuaranteedHigh: getCurrency(iNonGuaranteedDeathBenefit[aror.high], '', 0),
        totalHigh: getCurrency(iTotalDeathBenefit[aror.high], '', 0)
      });
      var surrenderValueRow = Object.assign({}, dbsvDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(iTotalPremiumPaidToDate, '', 0),
        guaranteed: getCurrency(0, '', 0),
        nonGuaranteedLow: getCurrency(iSurrenderValue[aror.low], '', 0),
        totalLow: getCurrency(iSurrenderValue[aror.low], '', 0),
        nonGuaranteedHigh: getCurrency(iSurrenderValue[aror.high], '', 0),
        totalHigh: getCurrency(iSurrenderValue[aror.high], '', 0),
        nonGuaranteedLowAccVal: getCurrency(iTotalAccountValue[aror.low], '', 0),
        nonGuaranteedHighAccVal: getCurrency(iTotalAccountValue[aror.high], '', 0)
      });
      var deductionsRow = Object.assign({}, deductionsDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(iTotalPremiumPaidToDate, '', 0),
        valueOfPremiumPaidToDateLow: getCurrency(iAccumulatedPremPaid[aror.low], '', 0),
        effectOfDeductionToDateLow: getCurrency(iEffectOfDeduction[aror.low], '', 0),
        totalSurrenderValueLow: getCurrency(iSurrenderValue[aror.low], '', 0),
        valueOfPremiumPaidToDateHigh: getCurrency(iAccumulatedPremPaid[aror.high], '', 0),
        effectOfDeductionToDateHigh: getCurrency(iEffectOfDeduction[aror.high], '', 0),
        totalSurrenderValueHigh: getCurrency(iSurrenderValue[aror.high], '', 0)
      });
      var totalDistributionCostRow = Object.assign({}, totalDistributionCostDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(iTotalPremiumPaidToDate, '', 0),
        totalDistributionCostToDate: getCurrency(iAccumulatedTotalDistCost[aror.high], '', 0)
      });
      if (i < 20 || i < 40 && (i + 1) % 5 === 0 || i === illustrations.length - 1) {
        deathBenefit.push(Object.assign({}, deathBenefitRow, {
          policyYearAge: policyYearAge
        }));
        surrenderValue.push(Object.assign({}, surrenderValueRow, {
          policyYearAge: policyYearAge
        }));
        deductions.push(Object.assign({}, deductionsRow, {
          policyYearAge: policyYearAge
        }));
        totalDistributionCost.push(Object.assign({}, totalDistributionCostRow, {
          policyYearAge: policyYearAge
        }));
      }
      if (incRowCntLineBreak && policyYear <= 20) {
        deathBenefit.push({
          policyYearAge: '',
          totalPremiumPaidToDate: '',
          guaranteed: '',
          nonGuaranteedLow: '',
          totalLow: '',
          nonGuaranteedHigh: '',
          totalHigh: ''
        });
        surrenderValue.push({
          policyYearAge: '',
          totalPremiumPaidToDate: '',
          guaranteed: '',
          nonGuaranteedLow: '',
          totalLow: '',
          nonGuaranteedHigh: '',
          totalHigh: '',
          nonGuaranteedLowAccVal: '',
          nonGuaranteedHighAccVal: ''
        });
      }
      var first20RecordEndIndex = wdStartIndex + 20;
      if (i >= wdStartIndex && (i < first20RecordEndIndex || i > first20RecordEndIndex && i < first20RecordEndIndex + 20 && (i - wdStartIndex + 1) % 5 === 0 || i === illustrations.length - 1)) {
        afterWithdrawal.push({
          policyYear: policyYear,
          age: age,
          annualWithdrawalAmountLow: getCurrency(iAnnualWithdrawalAmount[aror.low], '', 0),
          nonGuaranteedAccValueAfterWDLow: getCurrency(iNonGuaranteedAccValueAfterWD[aror.low], '', 0),
          totalDeathBenefitAfterWDLow: getCurrency(iTotalDeathBenefitAfterWD[aror.low], '', 0),
          annualWithdrawalAmountHigh: getCurrency(iAnnualWithdrawalAmount[aror.high], '', 0),
          nonGuaranteedAccValueAfterWDHigh: getCurrency(iNonGuaranteedAccValueAfterWD[aror.high], '', 0),
          totalDeathBenefitAfterWDHigh: getCurrency(iTotalDeathBenefitAfterWD[aror.high], '', 0)
        });
      }
      if (policyYear === reduceYieldYear) {
        rYieldValue = getCurrency(iRYieldTotalEffectDeduction[aror.high], '', 0);
      }
      if (i === illustrations.length - 1) {
        rYieldReducedReturnRate = getCurrency(iRYieldReducedReturnRate, '', 2);
      }
      incRowCntLineBreak = false;
    });
  }
  var finalDeduction = deductionsLast.length > 0 ? deductions.concat([deductionsDefault]).concat(deductionsLast) : deductions;
  var basicPlan = quotation.plans[0];
  var hasSpTopUp = quotation.policyOptions.topUpSelect;
  var hasRspTopUp = quotation.policyOptions.rspSelect;
  var hasWithdrawal = quotation.policyOptions.wdSelect;
  var company = extraPara.company;
  var minInvPeriod = quotation.policyOptions.mip;
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  return {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: basicPlan.planCode,
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      proposer: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke)
      },
      insured: {
        name: quotation.iFullName,
        gender: getGenderDesc(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmokingDesc(quotation.iSmoke)
      },
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      plans: quotation.plans.map(function(plan, minInvPeriod) {
        return {
          name: plan.covName.en,
          polTermDesc: plan.polTermDesc,
          premTermDesc: plan.premTermDesc,
          mip: minInvPeriod
        };
      }),
      funds: quotation.fund.funds.map(function(fund) {
        var fRsp = (quotation.policyOptions.rspAmount || 0) * fund.alloc / 100;
        return {
          name: fund.fundName.en,
          code: fund.fundCode,
          alloc: fund.alloc + '%',
          topUpAlloc: hasSpTopUp ? fund.topUpAlloc + '%' : '-',
          premium: getCurrency(basicPlan.premium * fund.alloc / 100, '', 2),
          annualPremium: getCurrency(basicPlan.yearPrem * fund.alloc / 100, '', 2),
          rsp: getCurrency(fRsp, '', 2),
          annualRsp: getCurrency(fRsp * 12, '', 2),
          topUp: getCurrency((quotation.policyOptions.topUpAmt || 0) * fund.topUpAlloc / 100, '', 2)
        };
      }),
      withdrawal: {
        value: hasWithdrawal ? getCurrency(quotation.policyOptions.wdAmount, '', 2) : "N/A",
        from: hasWithdrawal ? quotation.policyOptions.wdFromAge : "N/A",
        to: hasWithdrawal ? quotation.policyOptions.wdToAge : "N/A"
      }
    },
    basicPlan: {
      name: basicPlan.covName.en,
      sumInsured: getCurrency(basicPlan.sumInsured, '', 2),
      premium: getCurrency(basicPlan.premium, '', 2),
      aPremium: getCurrency(basicPlan.yearPrem, '', 2),
      sPremium: getCurrency(basicPlan.halfYearPrem, '', 2),
      qPremium: getCurrency(basicPlan.quarterPrem, '', 2),
      mPremium: getCurrency(basicPlan.monthPrem, '', 2),
      spTopUp: getCurrency(hasSpTopUp ? quotation.policyOptions.topUpAmt : 0, '', 2),
      rspTopUp: getCurrency(hasRspTopUp ? quotation.policyOptions.rspAmount : 0, '', 2),
      annualRspTopUp: getCurrency(hasRspTopUp ? quotation.policyOptions.rspAmount * 12 : 0, '', 2),
      paymentModeDesc: quotation.paymentMode === 'A' ? 'Annual' : (quotation.paymentMode === 'S' ? 'Semi-Annual' : (quotation.paymentMode === 'Q' ? 'Quarterly' : 'Monthly')),
      ccy: quotation.ccy,
      ccySymbol: quotation.ccy === 'SGD' ? 'S$' : (quotation.ccy === 'USD' ? 'US$' : '$'),
      hasTopUp: hasSpTopUp ? 'Y' : 'N',
      hasRspTopUp: hasRspTopUp ? 'Y' : 'N',
      hasWithdrawal: hasWithdrawal ? 'Y' : 'N'
    },
    illustration: {
      deathBenefit: deathBenefit.concat([dbsvDefault]).concat(deathBenefitLast),
      surrenderValue: surrenderValue.concat([dbsvDefault]).concat(surrenderValueLast),
      deductions: finalDeduction,
      totalDistributionCost: totalDistributionCost.concat([totalDistributionCostDefault]).concat(totalDistributionCostLast),
      reductionInYield: {
        age: 65 - quotation.iAge >= 20 ? 'at age 65' : 'at age ' + (quotation.iAge + 20),
        value: rYieldValue,
        reducedReturnRate: rYieldReducedReturnRate
      },
      afterWithdrawal: afterWithdrawal
    },
    aror: aror
  };
}