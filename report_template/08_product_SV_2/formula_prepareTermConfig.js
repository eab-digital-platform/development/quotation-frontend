function(planDetail, quotation, planDetails) {
  let planInfo = quotation.plans && quotation.plans.find(p => p.covCode === planDetail.covCode);
  let polTermList = quotDriver.runFunc(planDetail.formulas.polTermsFunc, planDetail, quotation);
  planInfo.policyTerm = polTermList[0].value;
  planDetail.inputConfig.canEditPolicyTerm = false;
  planDetail.inputConfig.policyTermList = polTermList;
  let premTermList = quotDriver.runFunc(planDetail.formulas.premTermsFunc, planDetail, quotation);
  if (planInfo && !premTermList.find((opt) => opt.value === planInfo.premTerm)) {
    planInfo.premTerm = null;
  }
  planDetail.inputConfig.canEditPremTerm = true;
  planDetail.inputConfig.premTermList = premTermList;
}