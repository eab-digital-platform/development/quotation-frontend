function(quotation, planInfo, planDetails) {
  const CALC_TYPE_INPUT_PREMIUM = 1;
  const CALC_TYPE_INPUT_RETIRE_INC = 2;
  const MINIMUM_RETIREMENT_INCOME_SP = 20000;
  var truncate = function(value) {
    var truncated;
    truncated = Math.round(value * 10000) / 10000; /** force that 1451.9999999998 to be rounded first before truncate*/
    return Math.floor(truncated * 100) / 100;
  };
  if (quotation.baseProductCode === 'RHP') {
    const PLAN_TYPE_BASIC = 0;
    const PLAN_TYPE_RIDER_TPD = 1;
    const CALC_TYPE_INPUT_PREMIUM = 1;
    const CALC_TYPE_INPUT_RETIRE_INC = 2;
    const PAYOUT_TERM_LIFETIME = 99;
    if (quotation.plans[0].premium == undefined) {
      planInfo.premium = NaN; /** force UI to be blank*/
      return;
    } else {
      if (quotation.plans[0].premium == 0) {
        planInfo.premium = NaN; /** force UI to be blank*/
        return;
      }
    }
    if ((quotation.plans[0].premium === NaN) || (quotation.plans[0].retirementIncome === NaN)) {
      planInfo.premium = NaN; /** force UI to be blank*/
      return;
    }
    var premiumTerm = quotation.plans[0].premTerm;
    var retirementAge = quotation.policyOptions.retirementAge;
    var TPD_annualPremium = 0;
    var getPlanCode = function(planType) {
      var s = '';
      var payoutType = quotation.policyOptions.payoutType;
      var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
      switch (premiumTerm) {
        case 1:
          {
            s += '01';
            break;
          }
        case 5:
          {
            s += '05';
            break;
          }
        case 10:
          {
            s += '10';
            break;
          }
        case 15:
          {
            s += '15';
            break;
          }
        case 20:
          {
            s += '20';
            break;
          }
        case 25:
          {
            s += '25';
            break;
          }
        default:
          {
            s += '05';
            break;
          }
      }
      switch (planType) {
        case PLAN_TYPE_BASIC:
          {
            if (payoutType === 'Inflated') {
              s += 'SI';
            } else {
              s += 'SL';
            }
            break;
          }
        case PLAN_TYPE_RIDER_TPD:
          {
            if (payoutType === 'Inflated') {
              s += 'TI';
            } else {
              s += 'TL';
            }
            break;
          }
        default:
          {
            if (payoutType === 'Inflated') {
              s += 'SI';
            } else {
              s += 'SL';
            }
            break;
          }
      }
      s += retirementAge;
      switch (payoutTerm) {
        case 15:
          {
            s += 'A';
            break;
          }
        case 20:
          {
            s += 'B';
            break;
          }
        default:
          {
            s += 'C';
            break;
          }
      }
      return s;
    };

    function calculateRiderTPDMultiplier(curPlanInfo) {
      var sumAssured = quotation.sumInsured;
      var LEP2_TPD_rates = planDetails['TPD'].rates.LEP2_TPD_rates;
      var TPD_planCode = '';
      var TPD_rate = 0;
      var get_LEP2_TPD_rate = function(rlPlan, insuredAge) {
        var ref = rlPlan;
        ref += quotation.iGender;
        ref += quotation.iSmoke;
        var planRates = LEP2_TPD_rates[ref];
        if (planRates == null) {
          return 0;
        }
        return planRates[insuredAge];
      };
      TPD_planCode = getPlanCode(PLAN_TYPE_RIDER_TPD);
      TPD_rate = get_LEP2_TPD_rate(TPD_planCode, quotation.iAge);
      var temp; /* compute annualPremium*/
      temp = truncate(sumAssured * TPD_rate); /* TRUNCATE to 2 decimal places*/
      temp = temp / 1000;
      temp = Math.round(temp * 100) / 100; /* ROUND to 2 decimal places*/
      var premTermDesc = (quotation.paymentMode === 'L') ? 'Single Premium' : premiumTerm + ' Years'; /** Update planInfo*/
      curPlanInfo.planCode = TPD_planCode;
      curPlanInfo.sumAssured = quotation.sumInsured;
      curPlanInfo.premTerm = premiumTerm;
      curPlanInfo.premTermDesc = premTermDesc;
      curPlanInfo.yearPrem = temp;
      curPlanInfo.halfYearPrem = truncate(temp * 0.51);
      curPlanInfo.quarterPrem = truncate(temp * 0.26);
      curPlanInfo.monthPrem = truncate(temp * 0.0875);
      quotation.totYearPrem += curPlanInfo.yearPrem;
      quotation.totHalfyearPrem += curPlanInfo.halfYearPrem;
      quotation.totQuarterPrem += curPlanInfo.quarterPrem;
      quotation.totMonthPrem += curPlanInfo.monthPrem;
      switch (quotation.paymentMode) {
        case 'A':
          {
            curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
            break;
          }
        case 'S':
          {
            curPlanInfo.premium = curPlanInfo.halfYearPrem;quotation.premium += curPlanInfo.halfYearPrem;
            break;
          }
        case 'Q':
          {
            curPlanInfo.premium = curPlanInfo.quarterPrem;quotation.premium += curPlanInfo.quarterPrem;
            break;
          }
        case 'M':
          {
            curPlanInfo.premium = curPlanInfo.monthPrem;quotation.premium += curPlanInfo.monthPrem;
            break;
          }
        default:
          {
            curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
            break;
          }
      }
      if (premiumTerm == 1) {
        curPlanInfo.yearPrem = temp;
        curPlanInfo.singlePrem = temp;
        quotation.totSinglePrem += temp;
      }
      curPlanInfo.annualPremium = temp;
      TPD_annualPremium = temp; /** SP and CALC_TYPE_INPUT_RETIRE_INC*/
      if ((quotation.prevCalcType == CALC_TYPE_INPUT_RETIRE_INC) && (quotation.paymentMode === 'L') && (quotation.policyOptions.paymentMethod === 'cash')) {
        if (quotation.premium < MINIMUM_RETIREMENT_INCOME_SP) {
          quotDriver.context.addError({
            covCode: quotation.baseProductCode,
            msg: 'Please increase the Retirement Income to meet the minimum premium of S$20,000'
          });
        }
      }
    }

    function calculateSumAssuredNonTPD() {
      var Premium_Lookup = 0;
      var TPD_Lookup = TPD_annualPremium;
      if (quotation.prevCalcType == CALC_TYPE_INPUT_PREMIUM) {
        Premium_Lookup = quotation.annualPremiumBSA;
      } else {
        Premium_Lookup = quotation.annualPremium;
      }
      var sumAssured = Premium_Lookup + TPD_Lookup;
      sumAssured = Math.floor(sumAssured * 100) / 100; /* TRUNCATE to 2 decimal places*/
      return sumAssured;
    }
    calculateRiderTPDMultiplier(planInfo);
    planInfo.sumAssuredNonTPD = calculateSumAssuredNonTPD();
  }
}