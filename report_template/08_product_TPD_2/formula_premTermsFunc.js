function(planDetail, quotation) {
  var premTermsList = [];
  var paymentTerm = quotation.plans[0].premTerm;
  if (paymentTerm == undefined) {
    return premTermsList;
  } /** Check for change in payment mode*/
  if (quotation.prevPaymentMode) {
    if (quotation.prevPaymentMode !== quotation.paymentMode) {
      if (quotation.prevPaymentMode === 'L') {
        return premTermsList;
      } else {
        if (quotation.paymentMode === 'L') {
          return premTermsList;
        }
      }
    }
  }
  planDetail.premTerm = paymentTerm;
  if (quotation.paymentMode == 'L') {
    premTermsList.push({
      value: 1,
      title: 'Single Premium',
      default: true
    });
  } else {
    premTermsList.push({
      value: paymentTerm,
      title: paymentTerm + ' Years',
      default: true
    });
  }
  return premTermsList;
}