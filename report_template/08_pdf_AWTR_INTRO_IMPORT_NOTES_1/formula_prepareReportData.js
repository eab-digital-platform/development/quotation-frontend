function(quotation, planInfo, planDetails, extraPara) {
  var basicPlan = quotation.plans[0];
  var hasSpTopUp = quotation.policyOptions.topUpSelect;
  var hasRspTopUp = quotation.policyOptions.rspSelect;
  var hasWithdrawal = quotation.policyOptions.wdSelect;
  var company = extraPara.company;
  var illust = extraPara.illustrations[planInfo.covCode];
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getCurrencyDesc = function(value, decimals) {
    return quotation.ccy + ' ' + getCurrency(value, '', decimals);
  };
  return {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: basicPlan.planCode,
      releaseVersion: "1"
    },
    proposer: {
      name: quotation.pFullName,
      gender: getGenderDesc(quotation.pGender),
      dob: new Date(quotation.pDob).format(extraPara.dateFormat),
      age: quotation.pAge,
      smoking: getSmokingDesc(quotation.pSmoke)
    },
    insured: {
      name: quotation.iFullName,
      gender: getGenderDesc(quotation.iGender),
      dob: new Date(quotation.iDob).format(extraPara.dateFormat),
      age: quotation.iAge,
      smoking: getSmokingDesc(quotation.iSmoke)
    },
    cover: {
      sameAs: quotation.sameAs,
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      proposer: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke)
      },
      insured: {
        name: quotation.iFullName,
        gender: getGenderDesc(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmokingDesc(quotation.iSmoke)
      },
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      plans: quotation.plans.map(function(plan, minInvPeriod) {
        return {
          name: plan.covName.en,
          polTermDesc: plan.polTermDesc,
          premTermDesc: plan.premTermDesc,
          mip: minInvPeriod
        };
      }),
      funds: quotation.fund.funds.map(function(fund) {
        var fRsp = (quotation.policyOptions.rspAmount || 0) * fund.alloc / 100;
        return {
          name: fund.fundName.en,
          code: fund.fundCode,
          alloc: fund.alloc + '%',
          topUpAlloc: hasSpTopUp ? fund.topUpAlloc + '%' : '-',
          premium: getCurrency(basicPlan.premium * fund.alloc / 100, '', 2),
          annualPremium: getCurrency(basicPlan.yearPrem * fund.alloc / 100, '', 2),
          rsp: getCurrency(fRsp, '', 2),
          annualRsp: getCurrency(fRsp * 12, '', 2),
          topUp: getCurrency((quotation.policyOptions.topUpAmt || 0) * fund.topUpAlloc / 100, '', 2)
        };
      }),
      withdrawal: {
        value: hasWithdrawal ? getCurrency(quotation.policyOptions.wdAmount, '', 2) : "N/A",
        from: hasWithdrawal ? quotation.policyOptions.wdFromAge : "N/A",
        to: hasWithdrawal ? quotation.policyOptions.wdToAge : "N/A"
      }
    },
  };
}