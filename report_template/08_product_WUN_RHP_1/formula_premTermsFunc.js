function(planDetail, quotation) {
  var premTermsList = [];
  var paymentTerm = quotation.plans[0].premTerm;
  if (paymentTerm == undefined) {
    return premTermsList;
  }
  if (quotation.paymentMode == 'L') {
    premTermsList.push({
      value: 1,
      title: 'Single Premium',
      default: true
    });
  } else {
    premTermsList.push({
      value: paymentTerm,
      title: paymentTerm + ' Years',
      default: true
    });
  }
  return premTermsList;
}