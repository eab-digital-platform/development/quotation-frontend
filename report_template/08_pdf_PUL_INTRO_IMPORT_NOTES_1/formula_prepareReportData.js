function(quotation, planInfo, planDetails, extraPara) {
  var illustrations = extraPara.illustrations[planInfo.covCode];
  return {
    illustrationImpro: {
      isOldAge: (65 - quotation.iAge) >= 20 ? 0 : 1,
      totalDistribution: getCurrency(illustrations.totalDistribution, '$', 0),
      reductionYield: illustrations.reductionYield
    }
  };
}