function(planDetail, quotation) {
  var planInfo = quotation.plans.find(p => p.covCode === planDetail.covCode);
  if (!planInfo.policyTerm) {
    return [];
  }
  var premTermList = [];
  if (planInfo.policyTerm) {
    var polTerm = Number.parseInt(planInfo.policyTerm);
    premTermList.push({
      value: planInfo.policyTerm,
      title: planInfo.policyTerm.endsWith('_YR') ? polTerm + ' Years' : 'To Age ' + polTerm
    });
  }
  return premTermList;
}