function(quotation, planInfo, planDetail, age) {
  var rateKey = planInfo.planCode + quotation.pGender + (quotation.pSmoke === 'Y' ? 'S' : 'NS');
  return planDetail.rates.premRate[rateKey][quotation.pAge];
}