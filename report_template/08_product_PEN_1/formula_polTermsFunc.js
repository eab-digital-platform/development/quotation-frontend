function(planDetail, quotation) {
  var policyTermList = [];
  var baseProductCode = quotation.baseProductCode;
  if (quotation && quotation.plans && quotation.plans.length > 0 && quotation.policyOptions.planType === 'renew') {
    var longestPayYear = 0;
    for (var v in quotation.plans) {
      var premTerm = quotation.plans[v].premTerm;
      var covCode = quotation.plans[v].covCode;
      var policyTermValueAndType = (premTerm) ? premTerm.split('_') : [];
      if (policyTermValueAndType[0] && policyTermValueAndType[1] && policyTermValueAndType[1] === 'YR' && Number.parseInt(premTerm) > longestPayYear && covCode !== 'PEN') {
        longestPayYear = Number.parseInt(premTerm);
      } else if (policyTermValueAndType[0] && policyTermValueAndType[1] && policyTermValueAndType[1] === 'TA' && (Number.parseInt(premTerm) - quotation.iAge) > longestPayYear && covCode !== 'PEN') {
        longestPayYear = Number.parseInt(premTerm) - quotation.iAge;
      }
    }
    var values = [5, 10, 15, 20, 25, 30];
    if ((quotation.pAge + longestPayYear) >= 65 && longestPayYear !== 0) {
      policyTermList.push({
        value: 65 + '_TA',
        title: 'To Age 65',
        default: true
      });
    } else if (longestPayYear >= 30) {
      policyTermList.push({
        value: 30 + '_YR',
        title: '30 Years',
        default: true
      });
    } else if (longestPayYear !== 0) {
      for (var v in values) {
        var value = values[v];
        var testValue = value - longestPayYear;
        if (testValue < 5 && testValue >= 0) {
          policyTermList.push({
            value: value + '_YR',
            title: value + ' Years',
            default: true
          });
        }
      }
    }
  } else if (quotation && quotation.plans && quotation.plans.length > 0 && quotation.policyOptions.planType === 'toAge') {
    var lpVals = {};
    var payYear = null;
    _.each(quotation.plans, (plan) => {
      if (plan.covCode !== planDetail.covCode && plan.premTerm) {
        var premTerm = plan.premTerm;
        if (premTerm.indexOf('YR') > -1) {
          lpVals[premTerm] = Number.parseInt(premTerm);
          payYear = Math.max(payYear, Number.parseInt(premTerm));
        } else if (premTerm.indexOf('TA') > -1) {
          payYear = Math.max(payYear, Number.parseInt(premTerm) - quotation.iAge);
        }
      }
    });
    _.each(lpVals, (val) => {
      if (quotation.pAge + val < 65) {
        policyTermList.push({
          value: val + '_YR',
          title: val + ' Years'
        });
      }
    });
    _.each([50, 55, 60, 65], (val) => {
      if (quotation.pAge + payYear >= val && val - quotation.pAge >= 5) {
        policyTermList.push({
          value: val + '_TA',
          title: 'To Age ' + val
        });
      }
    });
  }
  return policyTermList.sort((a, b) => a.value > b.value);
}