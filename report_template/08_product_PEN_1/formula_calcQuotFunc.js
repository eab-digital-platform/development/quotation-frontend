function(quotation, planInfo, planDetails) {
  var baseProductCode = quotation.baseProductCode;
  if (baseProductCode === 'ESP') {
    if (quotation.plans[0] && quotation.plans[0].premTerm === 5) {
      planInfo.policyTerm = quotation.plans[0].premTerm + '_YR';
    } else if (quotation.plans[0] && quotation.plans[0].premTerm === 10) {
      if (quotation.pAge >= 18 && quotation.pAge <= 55) {
        planInfo.policyTerm = quotation.plans[0].premTerm + '_YR';
      } else if (quotation.pAge >= 56 && quotation.pAge <= 60) {
        planInfo.policyTerm = (65 - quotation.pAge) + '_TA';
      }
    }
    planInfo.premTerm = planInfo.policyTerm;
    if (quotation.plans[0].premium) {
      quotCalc.calcPlanPrem(quotation, planInfo, planDetails);
    }
  } else {
    var planDetail = planDetails[planInfo.covCode];
    var planInfo = _.find(quotation.plans, p => p.covCode === planDetail.covCode);
    if (quotation.policyOptions.planType === 'renew' && planDetail.inputConfig && planDetail.inputConfig.policyTermList && planDetail.inputConfig.policyTermList[0]) {
      planInfo.policyTerm = planDetail.inputConfig.policyTermList[0].value;
    }
    planInfo.premTerm = planInfo.policyTerm;
    if (quotation.plans[0].premium) {
      quotCalc.calcPlanPrem(quotation, planInfo, planDetails);
    }
  }
}