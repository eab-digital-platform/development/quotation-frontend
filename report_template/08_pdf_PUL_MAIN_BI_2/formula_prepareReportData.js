function(quotation, planInfo, planDetails, extraPara) { /* PUL prepareReportData mainFormula*/
  var round = function(value, position) {
    var num = Number(value);
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(num, scale)), scale);
  };
  var illustrations = extraPara.illustrations[planInfo.covCode].illustration;
  var deathBenefit = [];
  var deathBenefitLast = [];
  var surrenderValue = [];
  var surrenderValueLast = [];
  var deductions = [];
  var deductionsLast = [];
  var totalDistributionCost = [];
  var totalDistributionCostLast = [];
  var afterWithdrawal = [];
  var dbsvDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    guaranteed: '',
    nonGuaranteedLow: '',
    totalLow: '',
    nonGuaranteedHigh: '',
    totalHigh: ''
  };
  var deductionsDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    valueOfPremiumPaidToDateLow: '',
    effectOfDeductionToDateLow: '',
    totalSurrenderValueLow: '',
    valueOfPremiumPaidToDateHigh: '',
    effectOfDeductionToDateHigh: '',
    totalSurrenderValueHigh: ''
  };
  var totalDistributionCostDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    totalDistributionCostToDate: ''
  };
  var planDetail = planDetails[planInfo.covCode];
  var aror = {
    low: planDetail.rates.aror.aror1 * 100,
    high: planDetail.rates.aror.aror2 * 100
  };
  var reduceYieldYear = math.max(20, 65 - quotation.iAge);
  var rYieldValue = 0;
  var rYieldReducedReturnRate = 0;
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  } else if (ccy == 'GBP') {
    polCcy = 'British Pound';
    ccySyb = '£';
  }
  if (illustrations instanceof Array) {
    var hasWithdrawal = quotation.policyOptions.wdSelect;
    var wdStartIndex = hasWithdrawal ? illustrations.findIndex(function(illustration) {
      return illustration.age === quotation.policyOptions.wdFromAge;
    }) + 1 : illustrations.length;
    illustrations.forEach(function(illustration, i) {
      var policyYear = illustration.policyYear;
      var age = illustration.age;
      var policyYearAge = illustration.policyYear + ' / ' + illustration.age;
      var iTotalPremiumPaidToDate = illustration.totalPremiumPaidToDate;
      var iTotalDeathBenefit = illustration.totalDeathBenefit;
      var iSurrenderValue = illustration.nonGuaranteedSurValue;
      var iAccumulatedPremPaid = illustration.nonGuaranteedAccValue;
      var iValueOfPremiumPaidToDate = illustration.valueOfPremiums;
      var iEffectOfDeduction = illustration.effectOfDeductions;
      var iAccumulatedTotalDistCost = illustration.totalDistributionCost;
      var deathBenefitRow = Object.assign({}, dbsvDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(round(iTotalPremiumPaidToDate, 0), '', 0),
        valueOfNonGuaranteedAccValueLow: getCurrency(round(iAccumulatedPremPaid[0], 0), '', 0),
        valueOffNonGuaranteedAccValueHigh: getCurrency(round(iAccumulatedPremPaid[1], 0), '', 0),
        totalLow: getCurrency(round(iTotalDeathBenefit[0], 0), '', 0),
        totalHigh: getCurrency(round(iTotalDeathBenefit[1], 0), '', 0)
      });
      var surrenderValueRow = Object.assign({}, dbsvDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(round(iTotalPremiumPaidToDate, 0), '', 0),
        guaranteed: '-',
        nonGuaranteedLow: getCurrency(round(iSurrenderValue[0], 0), '', 0),
        totalLow: getCurrency(round(iSurrenderValue[0], 0), '', 0),
        nonGuaranteedHigh: getCurrency(round(iSurrenderValue[1], 0), '', 0),
        totalHigh: getCurrency(round(iSurrenderValue[1], 0), '', 0)
      });
      var deductionsRow = Object.assign({}, deductionsDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(round(iTotalPremiumPaidToDate, 0), '', 0),
        valueOfPremiumPaidToDateLow: getCurrency(round(iValueOfPremiumPaidToDate[0], 0), '', 0),
        effectOfDeductionToDateLow: getCurrency(round(iEffectOfDeduction[0], 0), '', 0),
        totalSurrenderValueLow: getCurrency(round(iSurrenderValue[0], 0), '', 0),
        valueOfPremiumPaidToDateHigh: getCurrency(round(iValueOfPremiumPaidToDate[1], 0), '', 0),
        effectOfDeductionToDateHigh: getCurrency(round(iEffectOfDeduction[1], 0), '', 0),
        totalSurrenderValueHigh: getCurrency(round(iSurrenderValue[1], 0), '', 0)
      });
      var totalDistributionCostRow = Object.assign({}, totalDistributionCostDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(round(iTotalPremiumPaidToDate, 0), '', 0),
        totalDistributionCostToDate: getCurrency(round(iAccumulatedTotalDistCost, 0), '', 0)
      });
      if (i < 20 || i < 40 && (i + 1) % 5 === 0 || i === illustrations.length - 1) {
        deathBenefit.push(Object.assign({}, deathBenefitRow, {
          policyYearAge: policyYearAge
        }));
        surrenderValue.push(Object.assign({}, surrenderValueRow, {
          policyYearAge: policyYearAge
        }));
        deductions.push(Object.assign({}, deductionsRow, {
          policyYearAge: policyYearAge
        }));
        totalDistributionCost.push(Object.assign({}, totalDistributionCostRow, {
          policyYearAge: policyYearAge
        }));
      }
      if ([55, 60, 65].indexOf(illustration.age) >= 0) {
        deathBenefitLast.push(Object.assign({}, deathBenefitRow, {
          policyYearAge: 'AGE ' + illustration.age
        }));
        surrenderValueLast.push(Object.assign({}, surrenderValueRow, {
          policyYearAge: 'AGE ' + illustration.age
        }));
        deductionsLast.push(Object.assign({}, deductionsRow, {
          policyYearAge: 'AGE ' + illustration.age
        }));
        totalDistributionCostLast.push(Object.assign({}, totalDistributionCostRow, {
          policyYearAge: 'AGE ' + illustration.age
        }));
      }
      var first20RecordEndIndex = wdStartIndex + 20;
      if (i >= wdStartIndex && (i < first20RecordEndIndex || i > first20RecordEndIndex && i < first20RecordEndIndex + 20 && (i - wdStartIndex + 1) % 5 === 0 || i === illustrations.length - 1)) {
        afterWithdrawal.push({
          policyYear: policyYear,
          age: age
        });
      }
      if (policyYear === reduceYieldYear) {
        rYieldValue = 0;
      }
      if (i === illustrations.length - 1) {
        rYieldReducedReturnRate = 0;
      }
    });
  }
  return {
    illustration: {
      deathBenefit: deathBenefit.concat([dbsvDefault]).concat(deathBenefitLast),
      surrenderValue: surrenderValue.concat([dbsvDefault]).concat(surrenderValueLast),
      deductions: deductions.concat([deductionsDefault]).concat(deductionsLast),
      totalDistributionCost: totalDistributionCost.concat([totalDistributionCostDefault]).concat(totalDistributionCostLast),
      reductionInYield: {
        age: 65 - quotation.iAge >= 20 ? 'at age 65' : 'over 20 years',
        value: rYieldValue,
        reducedReturnRate: rYieldReducedReturnRate
      },
      afterWithdrawal: afterWithdrawal
    },
    aror: aror,
    polCcy
  };
}