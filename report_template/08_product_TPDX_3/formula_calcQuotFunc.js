function(quotation, planInfo, planDetails) { /*LMP_TPDX_calQuotFunc*/
  if (quotation.plans[0].premTerm) {
    planInfo.premTerm = quotation.plans[0].premTerm;
    planInfo.premTermDesc = quotation.plans[0].premTermDesc;
  }
  var singaporeResideneMap = {
    "5": 1200000,
    "3.5": 1714000,
    "2.5": 2400000
  };
  var nonsingaporeResideneMap = {
    "5": 800000,
    "3.5": 1142000,
    "2.5": 1600000
  };
  var juveniles = {
    "5": 200000,
    "3.5": 285000,
    "2.5": 400000
  };
  var max_value = 0;
  if (quotation.iAge < 18) {
    max_value = juveniles[quotation.policyOptions.multiFactor];
  } else {
    if (quotation.iResidence == 'R2') {
      max_value = singaporeResideneMap[quotation.policyOptions.multiFactor];
    } else {
      max_value = nonsingaporeResideneMap[quotation.policyOptions.multiFactor];
    }
  }
  if (quotation.plans[0].sumInsured > max_value) {
    planInfo.sumInsured = max_value;
  } else {
    planInfo.sumInsured = quotation.plans[0].sumInsured;
  }
  if (quotation.policyOptions.multiFactor && planInfo.premTerm) { /* planCode needed for prem calculation */
    var planCode = 'TPDX';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    var mbCodeMap = {
      "5": "A",
      "3.5": "B",
      "2.5": "C"
    };
    var mbCodeMapOlder = {
      "2.50": "A",
      "2.75": "B",
      "1.25": "C"
    };
    var group = quotation.iAge < 71 ? mbCodeMap[quotation.policyOptions.multiFactor] : mbCodeMapOlder[quotation.policyOptions.multiFactor];
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode + group;
    } else {
      planCode = planCode + group + Number.parseInt(planInfo.premTerm);
    }
    planInfo.planCode = planCode;
    planInfo.packagedRider = true;
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails['TPDX'].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer > 25) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer >= 20 && policyYearRefer <= 24) {
            crate = commission_rate[rate][1];
          } else {
            crate = commission_rate[rate][2];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
      }
      var SA_bundleInfo = {
        "2.5": 40000,
        "3.5": 28571.43,
        "5": 20000
      };
      var SA_policy_lookup = SA_bundleInfo[quotation.policyOptions.multiFactor];
      var bundle_lsd = 0;
      if (SA_policy_lookup < 50000) {
        bundle_lsd = 0;
      } else if (SA_policy_lookup < 100000) {
        bundle_lsd = 1.00;
      } else if (SA_policy_lookup < 200000) {
        bundle_lsd = 1.50;
      } else {
        bundle_lsd = 1.80;
      }
      var premRate = runFunc(planDetails['TPDX'].formulas.getPremRate, quotation, planInfo, planDetails['TPDX'], quotation.iAge);
      var process_premRate = premRate + bundle_lsd;
      var annualPrem_bundle = math.multiply(math.bignumber(process_premRate), math.bignumber(SA_policy_lookup), math.bignumber(0.001));
      var modalFactor = 1;
      for (var p in planDetails['TPDX'].payModes) {
        if (planDetails['TPDX'].payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetails['TPDX'].payModes[p].factor;
        }
      }
      var modlePrem_bundle = math.round(math.multiply(annualPrem_bundle, math.bignumber(modalFactor)), 2);
      planInfo.annualPrem_bundle = math.number(annualPrem_bundle);
      planInfo.modlePrem_bundle = math.number(modlePrem_bundle);
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}