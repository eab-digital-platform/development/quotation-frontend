function(planDetail, quotation) {
  var policyTermList = [];
  var policyTermValue = 99 - quotation.iAge;
  policyTermList.push({
    value: "" + policyTermValue,
    title: policyTermValue + ' years',
    default: true
  });
  return policyTermList;
}