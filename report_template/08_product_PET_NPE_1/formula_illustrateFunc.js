function(quotation, planInfo, planDetails, extraPara) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var dealerGroup = quotation.agent.dealerGroup;
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  var getChannel = function() {
    var mappedChannel = dealerGroup;
    if (dealerGroup === 'SYNERGY') {
      mappedChannel = 'AGENCY';
    } else if (dealerGroup === 'FUSION') {
      mappedChannel = 'BROKER';
    }
    return mappedChannel;
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var convertBignumberToReadable = function(data) {
    for (var row in data) {
      for (var col in data[row]) {
        if (typeof(data[row][col]) !== 'string') {
          data[row][col] = math.number(data[row][col]);
        }
      }
    }
  };
  var channel = getChannel();
  var MAX_POLICY_YEAR = 100;
  var commissionTable = rates.commission[channel];
  var calcIllustration = function(illustration) {
    var sustainTestResult = true;
    var rawData = [];
    var defaultRawData = {
      policyYear: 0,
      accumPremium: math.bignumber(0),
      tdc: math.bignumber(0)
    };
    rawData.push(Object.assign({}, defaultRawData));
    for (var year = 1; year <= MAX_POLICY_YEAR; year++) {
      var currData = Object.assign({}, defaultRawData);
      var prevData = rawData[year - 1];
      currData.policyYear = year;
      var currPrem = math.bignumber(year <= planInfo.premTerm ? planInfo.yearPrem : 0);
      currData.accumPremium = math.add(currPrem, prevData.accumPremium);
      var commissionPolicyYrIndex = commissionTable.policyYear.indexOf(year);
      if (commissionPolicyYrIndex < 0) {
        commissionPolicyYrIndex = commissionTable.policyYear.length - 1;
      }
      var commissionPermTermIndex = 0;
      var commissionTableKeys = Object.keys(commissionTable);
      for (var i = 0; i < commissionTableKeys.length; i++) {
        var key = commissionTableKeys[i];
        if (key !== 'policyYear') {
          var commissionPremTerm = parseInt(key);
          if (planInfo.premTerm >= commissionPremTerm) {
            commissionPermTermIndex = i;
          }
        }
      }
      var commissionKey = commissionTableKeys[commissionPermTermIndex];
      var comm = math.bignumber(commissionTable[commissionKey][commissionPolicyYrIndex]);
      var currTdc = math.bignumber(year <= planInfo.premTerm ? math.multiply(comm, planInfo.yearPrem) : 0);
      currData.tdc = math.add(trunc(currTdc, 2), prevData.tdc);
      var illData = illustration[year - 1];
      illData.policyYear = currData.policyYear;
      illData.accumPremium = currData.accumPremium;
      illData.tdc = currData.tdc;
      rawData.push(currData);
    }
    convertBignumberToReadable(rawData);
    return sustainTestResult;
  };
  var illustration = [];
  var illustrationData = {
    policyYear: 0,
    accumPremium: 0,
    tdc: 0
  };
  for (var y = 0; y < MAX_POLICY_YEAR; y++) {
    illustration.push(Object.assign({}, JSON.parse(JSON.stringify(illustrationData))));
  }
  calcIllustration(illustration);
  return illustration;
}