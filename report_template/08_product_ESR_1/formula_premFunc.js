function(quotation, planInfo, planDetail) { /** Enhanced Benefit Rider Prem Func*/
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var numberTrunc = function(value, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return Number.parseInt(value);
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return Number.parseFloat(value);
    }
  };
  var premium = null;
  var sumInsured = quotation.plans[0].sumInsured || 0;
  if (planInfo.premTerm && sumInsured) {
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
    var lsd = math.bignumber(runFunc(planDetail.formulas.getLSD, planDetail, sumInsured, planInfo.premTerm));
    premRate = premRate;
    var prem = math.divide(numberTrunc(math.multiply(premRate, sumInsured), 2), 1000);
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
      }
    }
    prem = numberTrunc(math.multiply(prem, math.bignumber(modalFactor)), 2);
    premium = math.number(prem);
  }
  return premium;
}