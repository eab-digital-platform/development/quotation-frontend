function(quotation, planInfo, planDetail, age) {
  var roundUp = function(value, digit) {
    var scale = Math.pow(10, digit);
    if (digit === 0) {
      return Math.ceil(value);
    } else {
      return Math.ceil(number * scale) / scale;
    }
  };
  var round = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var rateKey;
  if (planInfo.policyTerm.indexOf('YR') > -1 || quotation.policyOptions.planType === 'renew') {
    rateKey = 'UNB';
  } else {
    rateKey = 'UNBN';
  }
  rateKey += '_' + quotation.iGender;
  rateKey += quotation.iSmoke === 'Y' ? 'S' : 'NS';
  var rates = planDetail.rates.premRate[rateKey];
  var maturityAge = (planInfo.policyTerm.indexOf('TA') > -1) ? Number.parseInt(planInfo.policyTerm) : quotation.iAge + Number.parseInt(planInfo.policyTerm);
  var Term_WU = maturityAge - quotation.iAge;
  var netAnnualPremium = 0;
  var verticalKey;
  var horizontalKey;
  if (planInfo.policyTerm.indexOf('YR') > -1 || quotation.policyOptions.planType === 'renew') {
    horizontalKey = age - 18;
    verticalKey = math.multiply(roundUp(math.divide(Term_WU, 5), 0), 5);
    netAnnualPremium = rates[verticalKey][horizontalKey];
  } else {
    horizontalKey = age - 18;
    verticalKey = maturityAge;
    netAnnualPremium = rates[verticalKey][horizontalKey];
  }
  var loadingKey = '1';
  var substdClsMapping = planDetail.rates.substdCls[quotation.iResidence];
  if (substdClsMapping) {
    loadingKey = substdClsMapping[quotation.iResidenceCity] || substdClsMapping.ALL || '1';
  }
  var loadingFactor = planDetail.rates.premRate.UNB_SUBSTANDARD_LOADING[loadingKey] || 1;
  return round(math.multiply(netAnnualPremium, loadingFactor), 2);
}