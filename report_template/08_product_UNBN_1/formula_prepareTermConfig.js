function(planDetail, quotation, planDetails) {
  var baseProductCode = quotation.baseProductCode;
  if (baseProductCode === 'ESP') {
    let basicDetail = planDetails[quotation.baseProductCode];
    let polTermList = [];
    for (var index = 5; index <= 10; index++) {
      polTermList.push({
        value: index + '_YR',
        title: index + ' Years',
        default: true
      });
      polTermList.push({
        value: index + '_TA',
        title: index + ' Years',
        default: true
      });
    }
    planDetail.inputConfig.canEditPolicyTerm = false;
    planDetail.inputConfig.policyTermList = polTermList;
    planDetail.inputConfig.canEditPremTerm = false;
    planDetail.inputConfig.premTermList = polTermList;
    if (basicDetail && basicDetail.policyTerm) {
      planDetail.policyTerm = basicDetail.policyTerm;
    } else {
      planDetail.policyTerm = null;
    }
    if (basicDetail && basicDetail.premTerm) {
      planDetail.premTerm = basicDetail.premTerm;
    } else {
      planDetail.premTerm = null;
    }
  } else {
    let polTermList = quotDriver.runFunc(planDetail.formulas.polTermsFunc, planDetail, quotation);
    if (polTermList && polTermList.length) {
      for (let p in polTermList) {
        if (polTermList[p].default) {
          planDetail.inputConfig.defaultPolicyTerm = polTermList[p].value;
          break;
        }
      }
    }
    planDetail.inputConfig.canEditPolicyTerm = quotation.policyOptions.planType === 'toAge';
    planDetail.inputConfig.policyTermList = polTermList;
    let premTermList = [].concat(polTermList);
    if (premTermList && premTermList.length) {
      for (let p in premTermList) {
        if (premTermList[p].default) {
          planDetail.inputConfig.premTerm = premTermList[p].value;
          break;
        }
      }
    }
    planDetail.inputConfig.canEditPremTerm = false;
    planDetail.inputConfig.premTermList = premTermList;
    let planInfo = _.find(quotation.plans, p => p.covCode === planDetail.covCode);
    if (polTermList.length === 1) {
      planInfo.policyTerm = polTermList[0].value;
    } else if (planInfo && !polTermList.find((opt) => opt.value === planInfo.policyTerm)) {
      planInfo.policyTerm = null;
    }
    if (premTermList.length === 1) {
      planInfo.premTerm = premTermList[0].value;
    } else if (planInfo && !premTermList.find((opt) => opt.value === planInfo.premTerm)) {
      planInfo.premTerm = null;
    }
  }
}