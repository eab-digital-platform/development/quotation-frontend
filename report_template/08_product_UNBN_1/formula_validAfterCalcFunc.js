function(quotation, planInfo, planDetail) { /*UNBN validAfterCalcFunc*/
  var round = function(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
  };
  var paddingZeroStr = function(number) {
    if (number < 10 && number >= 0) {
      return '0' + number;
    } else if (number < 0) {
      return '00';
    } else {
      return number;
    }
  };
  var defaultArr = ['ESP'];
  if (defaultArr.indexOf(quotation.baseProductCode) > -1 && quotation && quotation.plans && quotation.plans.length) {
    if (planInfo.policyTerm) {
      var planCode;
      if (planInfo.policyTerm.indexOf('TA') > -1) {
        planCode = 'UNB' + paddingZeroStr(Number.parseInt(planInfo.policyTerm) + quotation.iAge);
      } else {
        planCode = paddingZeroStr(Number.parseInt(planInfo.policyTerm)) + 'UNBN';
      }
      planInfo.planCode = planCode;
    }
    if (!quotation.plans[0].premium) {
      planInfo.premium = planInfo.yearPrem = planInfo.halfYearPrem = planInfo.quarterPrem = planInfo.monthPrem = quotation.plans[0].premium;
    }
    if (planInfo.premium) {
      planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm);
      if (planInfo.premTerm === 'SP') {
        planInfo.premTermYr = 1;
      } else {
        planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
      }
    }
  } else if (quotation && quotation.plans && quotation.plans.length && quotation.plans[0].policyTerm) {
    if (planInfo.policyTerm) {
      var planCode;
      if (quotation.plans[0].policyTerm.indexOf('TA') > -1) {
        var selectedPolicyTermVal = paddingZeroStr(Number.parseInt(planInfo.policyTerm));
        if (planInfo.policyTerm.indexOf('TA') > -1) {
          planCode = 'UNB' + selectedPolicyTermVal;
        } else {
          planCode = selectedPolicyTermVal + 'UNBN';
        }
      } else {
        planCode = paddingZeroStr(Number.parseInt(quotation.plans[0].policyTerm)) + 'UNB';
      }
      planInfo.planCode = planCode;
    }
    if (planInfo.premium) {
      planInfo.policyTermYr = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
      if (planInfo.premTerm === 'SP') {
        planInfo.premTermYr = 1;
      } else if (planInfo.premTerm.endsWith('_YR')) {
        planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
      } else {
        planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
    }
  }
}