function(quotation, planInfo, planDetails, extraPara) { /*suppFormula*/
  try {
    var trunc = function(value, position) {
      if (!position) position = 0;
      var scale = math.pow(10, position);
      return math.number(math.divide(math.floor(math.multiply(value, scale)), scale));
    };
    let {
      compName,
      compRegNo,
      compAddr,
      compAddr2,
      compTel,
      compFax,
      compWeb
    } = extraPara.company;
    var retVal = {
      'compName': compName,
      'compRegNo': compRegNo,
      'compAddr': compAddr,
      'compAddr2': compAddr2,
      'compTel': compTel,
      'compFax': compFax,
      'compWeb': compWeb
    };
    retVal.rate_low = "3.25";
    retVal.rate_high = "4.75";
    retVal.deduct_low = "15,468";
    retVal.deduct_high = "27,112";
    retVal.reductInYield_low = "2.54";
    retVal.reductInYield_high = "3.77";
    retVal.headerGender = (quotation.sameAs == "Y" ? quotation.pGender : quotation.iGender) == 'M' ? 'Male' : 'Female';
    retVal.headerSmoke = (quotation.sameAs == "Y" ? quotation.pSmoke : quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
    retVal.headerAge = quotation.sameAs == "Y" ? quotation.pAge : quotation.iAge;
    var ccy = quotation.ccy;
    var polCcy = '';
    var ccySyb = '';
    if (ccy == 'SGD') {
      polCcy = 'Singapore Dollars';
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      polCcy = 'US Dollars';
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      polCcy = 'Australian Dollars';
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      polCcy = 'Euro';
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      polCcy = 'British Pound';
      ccySyb = '£';
    }
    retVal.polCcy = polCcy;
    retVal.ccySyb = ccySyb;
    var basicPlan = quotation.plans[0];
    var additionalPlanTitle = ' ' + basicPlan.premTerm.toString() + ' PAY';
    retVal.basicPlanName = basicPlan.covName.en + additionalPlanTitle;
    retVal.basicSumAssured = getCurrency(basicPlan.sumInsured, ' ', 0);
    retVal.policyCurrency = polCcy;
    retVal.policyTerm = basicPlan.polTermDesc;
    retVal.premiumPaymentTerm = basicPlan.premTermDesc;
    var paymentModeTitle = '';
    if (quotation.paymentMode == 'A') {
      paymentModeTitle = 'Annual';
    } else if (quotation.paymentMode == 'S') {
      paymentModeTitle = 'Semi-Annual';
    } else if (quotation.paymentMode == 'Q') {
      paymentModeTitle = 'Quarterly';
    } else if (quotation.paymentMode == 'M') {
      paymentModeTitle = 'Monthly';
    }
    retVal.paymentFrequency = paymentModeTitle;
    retVal.guaranteedCashPayoutType = quotation.policyOptionsDesc.guaranteedCashPayoutType.en;
    var illustrations = extraPara.illustrations;
    retVal.basicAnnualPremium = getCurrency(illustrations.premiumTPDLookup, ' ', 2); /* retVal.basicAnnualPremium = Math.round(illustrations.premiumTPDLookup); */
    var illustrateData_DB = [];
    var illustrateData_SV_b4_mature = [];
    var illustrateData_SV_at_mature = [];
    var illustrateData_deduct = [];
    var illustrateData_totalDistribCost = [];
    var dataArr = [];
    dataArr = illustrations.suppBIDB ? illustrations.suppBIDB : [];
    if (dataArr instanceof Array) {
      var row = {};
      var polTerm = dataArr.length;
      dataArr.forEach(function(illustration, i) {
        var polYear = illustration.polYear;
        var age = illustration.age;
        var polYr_age = polYear + '/' + age;
        if (polYear <= 10 || polYear <= 40 && polYear % 5 === 0 || polYear === polTerm) {
          row = {
            polYr_age: polYr_age,
            totalPremPaid: getCurrency(trunc(Number(illustration.totPremiumPaid), 0), ' ', 0),
            GteedDB: getCurrency(trunc(Number(illustration.guaranteed), 0), ' ', 0),
            nonGteedDB_low: getCurrency(trunc(Number(illustration.nongteed325), 0), ' ', 0),
            totalDB_low: getCurrency(trunc(Number(illustration.toal325), 0), ' ', 0),
            nonGteedDB_high: getCurrency(trunc(Number(illustration.nongteed475), 0), ' ', 0),
            totalDB_high: getCurrency(trunc(Number(illustration.total475), 0), ' ', 0)
          };
          illustrateData_DB.push(row);
        }
      });
    }
    retVal.illustrateData_DB = illustrateData_DB;
    dataArr = illustrations.suppBISV ? illustrations.suppBISV : [];
    if (dataArr instanceof Array) {
      var row = {};
      var polTerm = dataArr.length;
      dataArr.forEach(function(illustration, i) {
        var polYear = illustration.polYear;
        var age = illustration.age;
        var polYr_age = polYear + '/' + age;
        if (polYear <= 10 || polYear <= 40 && polYear % 5 === 0 || polYear === polTerm) {
          row = {
            polYr_age: polYr_age,
            totalPremPaid: getCurrency(trunc(Number(illustration.totPremiumPaid), 0), ' ', 0),
            GteedSV: getCurrency(trunc(Number(illustration.guaranteed), 0), ' ', 0),
            nonGteedSV_low: getCurrency(trunc(Number(illustration.nongteed325), 0), ' ', 0),
            totalSV_low: getCurrency(trunc(Number(illustration.toal325), 0), ' ', 0),
            nonGteedSV_high: getCurrency(trunc(Number(illustration.nongteed475), 0), ' ', 0),
            totalSV_high: getCurrency(trunc(Number(illustration.total475), 0), ' ', 0)
          };
          if (polYear < polTerm) {
            illustrateData_SV_b4_mature.push(row);
          } else {
            illustrateData_SV_at_mature.push(row);
          }
        }
      });
    }
    retVal.illustrateData_SV_b4_mature = illustrateData_SV_b4_mature;
    retVal.illustrateData_SV_at_mature = illustrateData_SV_at_mature;
    dataArr = illustrations.riders ? illustrations.riders : [];
    if (dataArr instanceof Array) {
      var row = {};
      var polTerm = 0; /*change*/
      if (quotation.plans[2]) {
        polTerm = quotation.plans[2].premTermYr;
      } else {
        polTerm = dataArr.length;
      }
      if (polTerm > 0) {
        for (let i = 0; i < polTerm; i++) {
          var illustration = dataArr[i];
          var polYear = illustration.polYear;
          var age = illustration.age;
          var polYr_age = polYear + '/' + age;
          if (polYear <= 10 || polYear <= 40 && polYear % 5 === 0 || polYear === polTerm) {
            row = {
              polYr_age: polYr_age,
              totalPremPaid: getCurrency(trunc(Number(illustration.totalPremiumPaid), 0), ' ', 0),
              totalDistribCost: getCurrency(trunc(Number(illustration.tdc), 0), ' ', 0),
            };
            illustrateData_totalDistribCost.push(row);
          }
        }
      }
    }
    retVal.illustrateData_totalDistribCost = illustrateData_totalDistribCost;
    var optionalRiders = _.filter(quotation.plans, function(p) {
      return ['WPN', 'UNBN', 'WUN', 'PEN', 'PENCI', 'PUNB', 'PUN', 'PPU'].indexOf(p.covCode) >= 0;
    });
    if (optionalRiders.length === 0) {
      retVal.hidePagesIndexArray = [2];
    }
  } catch (err) {} finally {
    retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
    retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
    return retVal;
  }
}