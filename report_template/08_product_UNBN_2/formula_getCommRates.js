function(quotation, planInfo, planDetails) {
  var rateKey = 'RP_';
  var polYrs = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
  switch (quotation.dealerGroup) {
    case 'SYNERGY':
      rateKey += 'AGENCY';
      break;
    case 'FUSION':
      rateKey += 'BROKER';
      break;
    default:
      rateKey += quotation.dealerGroup;
  }
  var rateTable = planDetails[planInfo.covCode].rates.commRates[rateKey];
  var commRates = [];
  if (rateTable) {
    var maxTerm = 0;
    _.each(rateTable, (rates, key) => {
      var rateTerm = Number(key);
      if (polYrs >= rateTerm && rateTerm > maxTerm) {
        commRates = [].concat(rates);
        maxTerm = rateTerm;
      }
    });
  }
  if (commRates[commRates.length - 1]) {
    for (var i = commRates.length; i < 99; i++) {
      commRates.push(commRates[i - 1]);
    }
  }
  return commRates;
}