function(quotation, planDetail, planInfo) {
  var rateKey;
  switch (quotation.dealerGroup) {
    case 'SYNERGY':
      rateKey = 'AGENCY';
      break;
    case 'FUSION':
      rateKey = 'BROKER';
      break;
    default:
      rateKey = quotation.dealerGroup;
  }
  var rateTable = planInfo.rates.COMM_TTL[rateKey];
  var commRates = 0;
  if (rateTable) {
    var maxTerm = 0;
    var lookupYr = Number.parseInt(planDetail.policyTerm);
    _.each(rateTable, (rates, key) => {
      var rateTerm = Number(key);
      if (lookupYr >= rateTerm && rateTerm > maxTerm) {
        commRates = rates;
        maxTerm = rateTerm;
      }
    });
  }
  return commRates;
}