function(quotation, planInfo, planDetails) {
  if (planInfo.covCode == "PNP") {
    quotation.baseProductName.en = "MumCare";
  } else if (planInfo.covCode == "PNPP") {
    quotation.baseProductName.en = "MumCare Plus";
  } else if (planInfo.covCode == "PNP2") {
    quotation.baseProductName.en = "MumCare";
  } else if (planInfo.covCode == "PNPP2") {
    quotation.baseProductName.en = "MumCare Plus";
  }
}