function(quotation, planInfo, planDetail) {
  var rates = planDetail.rates;
  var countryCode = quotation.iResidence;
  var countryGroup = rates.countryGroup[countryCode][0];
  return (countryGroup === "Major City of China" || countryGroup === "Other City of China" || countryGroup === "Indonesia" || countryGroup === "Thailand");
}