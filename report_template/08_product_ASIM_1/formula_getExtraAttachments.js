function(agent, quotation, planDetails) {
  var asimAB, aspAB, asg, ash, asimStd, aspStd;
  for (var i in quotation.insureds) {
    var subQuot = quotation.insureds[i];
    for (var p in subQuot.plans) {
      var plan = subQuot.plans[p];
      switch (plan.covCode) {
        case 'ASIM':
          if (['A', 'B'].indexOf(plan.covClass) > -1) {
            asimAB = true;
          } else {
            asimStd = true;
          }
          break;
        case 'ASP':
          if (['A', 'B'].indexOf(plan.covClass) > -1) {
            aspAB = true;
          } else {
            aspStd = true;
          }
          break;
        case 'ASG':
          asg = true;
          break;
        case 'ASH':
          ash = true;
          break;
      }
    }
  }
  var files = [];
  if (asimAB) {
    files.push({
      covCode: 'ASIM',
      fileId: 'prod_summary_1'
    });
  }
  if (aspAB) {
    files.push({
      covCode: 'ASP',
      fileId: 'prod_summary_1'
    });
  }
  if (asg) {
    files.push({
      covCode: 'ASG',
      fileId: 'prod_summary_1'
    });
  }
  if (ash) {
    files.push({
      covCode: 'ASH',
      fileId: 'prod_summary_1'
    });
  }
  if (asimStd) {
    files.push({
      covCode: 'ASIM',
      fileId: 'prod_summary_2'
    });
  }
  if (aspStd) {
    files.push({
      covCode: 'ASP',
      fileId: 'prod_summary_2'
    });
  }
  var attachments = [{
    id: 'shield_product_summary',
    label: 'Product Summary',
    saveAttId: 'proposal',
    title: 'AXA Shield Product Summary ' + quotation.pFullName,
    fileName: 'AXA Shield Product Summary ' + quotation.pFullName + '.pdf',
    files: files
  }];
  if (agent.channel.type !== 'AGENCY' || quotation.quickQuote) {
    attachments[0].hidden = true;
    attachments.push({
      id: 'shield_product_summary_1',
      label: 'Product Summary',
      attId: 'standaloneProposal',
      saveAttId: 'standaloneProposal',
      allowSave: true,
      title: 'AXA Shield Product Summary ' + quotation.pFullName,
      fileName: 'AXA Shield Product Summary ' + quotation.pFullName + '.pdf',
      files: files
    });
  }
  return attachments;
}