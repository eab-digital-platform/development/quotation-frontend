function(quotation, planDetails, profiles) {
  const classNameMap = {};
  const bpDetail = planDetails[quotation.baseProductCode];
  for (var c in bpDetail.classList) {
    var classItem = bpDetail.classList[c];
    classNameMap[classItem.covClass] = classItem.className;
  }
  const currentDate = new Date();
  const classMap = {};
  _.each(profiles, (profile) => {
    var dob = new Date(profile.dob);
    var days = dateUtils.getAttainedAge(currentDate, dob).day;
    var age = dateUtils.getAgeNextBirthday(currentDate, dob);
    var availableClasses = [];
    if (days >= 15 && age <= 75) {
      availableClasses = ['A', 'B', 'C'];
    } else if (days >= 15 && age <= 99) {
      availableClasses = ['C'];
    }
    if (availableClasses.length) {
      var cid = profile.cid;
      classMap[cid] = [];
      for (var c in availableClasses) {
        var covClass = availableClasses[c];
        classMap[cid].push({
          covClass: covClass,
          className: classNameMap[covClass]
        });
      }
    }
  });
  return classMap;
}