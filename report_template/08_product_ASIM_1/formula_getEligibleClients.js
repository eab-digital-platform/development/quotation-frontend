function(quotation, planDetails, profiles, fnaInfo) {
  const currentDate = new Date();
  return _.filter(profiles, (profile) => {
    if (profile.nationality !== 'N1' && profile.prStatus !== 'Y') {
      return false;
    }
    if (profile.cid !== quotation.pCid) {
      const dependant = _.find(profiles[0].dependants, d => d.cid === profile.cid);
      if (!dependant || ['SPO', 'SON', 'DAU', 'MOT', 'FAT', 'GFA', 'GMO'].indexOf(dependant.relationship) === -1) {
        return false;
      }
    }
    var dob = new Date(profile.dob);
    var days = dateUtils.getAttainedAge(currentDate, dob).day;
    var age = dateUtils.getAgeNextBirthday(currentDate, dob);
    if (days < 15 || age > 99) {
      return false;
    }
    return true;
  });
}