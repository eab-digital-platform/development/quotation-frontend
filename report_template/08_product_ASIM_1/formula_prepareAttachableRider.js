function(planDetail, quotation, planDetails) {
  var covClass = quotation.plans[0].covClass;
  var allowedRiders = [];
  if (covClass && quotation.pAge <= 99) {
    switch (covClass) {
      case 'A':
      case 'B':
        allowedRiders = ['ASP', 'ASG', 'ASH'];
        break;
      case 'C':
        allowedRiders = ['ASP'];
        break;
    }
  }
  if (!planDetail.inputConfig.riderList) {
    planDetail.inputConfig.riderList = {};
  }
  planDetail.inputConfig.riderList = [];
  for (var r in planDetail.riderList) {
    var rider = planDetail.riderList[r];
    if (allowedRiders.indexOf(rider.covCode) > -1) {
      var riderPlanDetail = planDetails[rider.covCode];
      var covName = null;
      for (var c in riderPlanDetail.classList) {
        var classItem = riderPlanDetail.classList[c];
        if (classItem.covClass === covClass) {
          covName = classItem.className;
          break;
        }
      }
      planDetail.inputConfig.riderList.push({
        covCode: rider.covCode,
        covName: covName
      });
    }
  }
}