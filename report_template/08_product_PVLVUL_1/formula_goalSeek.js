function(oParams) { /**github.com/adam-hanna/goalSeek.js/tree/master**/
  var setObjVal = function(Obj, propStr, Value) {
    var Schema = Obj;
    var pList = propStr.split('.');
    var Len = pList.length;
    for (var i = 0; i < Len - 1; i++) {
      var Elem = pList[i];
      if (!Schema[Elem]) Schema[Elem] = {};
      Schema = Schema[Elem];
    }
    Schema[pList[Len - 1]] = Value;
  };
  var getObjVal = function(Obj, propStr) {
    var Parts = propStr.split(".");
    var Cur = Obj;
    for (var i = 0; i < Parts.length; i++) {
      Cur = Cur[Parts[i]];
    }
    return Cur;
  };
  var g, Y, Y1, OldTarget;
  oParams.Tol = (oParams.Tol || 0.001 * oParams.Goal || 0.1);
  oParams.maxIter = (oParams.maxIter || 1000);
  if (oParams.oFuncArgTarget.propStr) {
    if (!oParams.aFuncParams[oParams.oFuncArgTarget.Position][oParams.oFuncArgTarget.propStr]) {
      for (var i = 0; i < 100; i++) {
        var iGuess = Math.random();
        setObjVal(oParams.aFuncParams[oParams.oFuncArgTarget.Position], oParams.oFuncArgTarget.propStr, iGuess);
        if (oParams.Func.apply(oParams.This, oParams.aFuncParams)) {
          break;
        }
        if (i === 99) {
          return null;
        }
      }
    }
    for (var i = 0; i < oParams.maxIter; i++) {
      Y = oParams.Func.apply(oParams.This, oParams.aFuncParams) - oParams.Goal;
      if (Math.abs(Y) <= oParams.Tol) {
        return getObjVal(oParams.aFuncParams[oParams.oFuncArgTarget.Position], oParams.oFuncArgTarget.propStr);
      } else {
        OldTarget = getObjVal(oParams.aFuncParams[oParams.oFuncArgTarget.Position], oParams.oFuncArgTarget.propStr);
        setObjVal(oParams.aFuncParams[oParams.oFuncArgTarget.Position], oParams.oFuncArgTarget.propStr, OldTarget + Y);
        Y1 = oParams.Func.apply(oParams.This, oParams.aFuncParams) - oParams.Goal;
        g = (Y1 - Y) / Y;
        if (g === 0) {
          g = 0.0001;
        }
        setObjVal(oParams.aFuncParams[oParams.oFuncArgTarget.Position], oParams.oFuncArgTarget.propStr, OldTarget - Y / g);
      }
    }
    if (Math.abs(Y) > oParams.Tol) {
      return null;
    }
  } else {
    if (!oParams.aFuncParams[oParams.oFuncArgTarget.Position]) {
      for (var i = 0; i < 100; i++) {
        var iGuess = Math.random();
        oParams.aFuncParams[oParams.oFuncArgTarget.Position] = iGuess;
        if (oParams.Func.apply(oParams.This, oParams.aFuncParams)) {
          break;
        }
        if (i === 99) {
          return null;
        }
      }
    }
    for (var i = 0; i < oParams.maxIter; i++) {
      Y = oParams.Func.apply(oParams.This, oParams.aFuncParams) - oParams.Goal;
      if (Math.abs(Y) <= oParams.Tol) {
        return oParams.aFuncParams[oParams.oFuncArgTarget.Position];
      } else {
        OldTarget = oParams.aFuncParams[oParams.oFuncArgTarget.Position];
        oParams.aFuncParams[oParams.oFuncArgTarget.Position] = OldTarget + Y;
        Y1 = oParams.Func.apply(oParams.This, oParams.aFuncParams) - oParams.Goal;
        g = (Y1 - Y) / Y;
        if (g === 0) {
          g = 0.0001;
        }
        oParams.aFuncParams[oParams.oFuncArgTarget.Position] = OldTarget - Y / g;
      }
    }
    if (Math.abs(Y) > oParams.Tol) {
      return null;
    }
  }
}