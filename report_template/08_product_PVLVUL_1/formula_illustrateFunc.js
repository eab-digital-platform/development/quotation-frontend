function(quotation, planInfo, planDetails, extraPara) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var premium = planInfo.premium;
  var sumSa = planInfo.sumInsured;
  var dealerGroup = quotation.agent.dealerGroup;
  var ccy = quotation.ccy;
  var policytype = quotation.policyOptions.policytype;
  var wdOption = quotation.policyOptions.withdrawalOption;
  var riskClass = quotation.policyOptions.riskClassification;
  var riskClass2 = quotation.policyOptions.riskClassification2;
  var ef_isp_rate = Number(quotation.policyOptions.estFee) / 100;
  var la1_em_perc = Number(quotation.policyOptions.extraMortality);
  var la1_pml = Number(quotation.policyOptions.loading);
  var pml_period = Number(quotation.policyOptions.loadingPeriod);
  var la1_em_perc2 = Number(quotation.policyOptions.extraMortality2);
  var la1_pml2 = Number(quotation.policyOptions.loading2);
  var pml_period2 = Number(quotation.policyOptions.loadingPeriod2);
  var grossRate = Number(quotation.policyOptions.grossInvRate);
  var charge = Number(quotation.policyOptions.charge);
  var inWDRate = Number(quotation.policyOptions.invRateForWD);
  var startYr1 = Number(quotation.policyOptions.startYr1);
  var startYr2 = Number(quotation.policyOptions.startYr2);
  var startYr3 = Number(quotation.policyOptions.startYr3);
  var startYr4 = Number(quotation.policyOptions.startYr4);
  var startYr5 = Number(quotation.policyOptions.startYr5);
  var endYr1 = Number(quotation.policyOptions.endYr1);
  var endYr2 = Number(quotation.policyOptions.endYr2);
  var endYr3 = Number(quotation.policyOptions.endYr3);
  var endYr4 = Number(quotation.policyOptions.endYr4);
  var endYr5 = Number(quotation.policyOptions.endYr5);
  var wdAmt1 = Number(quotation.policyOptions.wdAmt1);
  var wdAmt2 = Number(quotation.policyOptions.wdAmt2);
  var wdAmt3 = Number(quotation.policyOptions.wdAmt3);
  var wdAmt4 = Number(quotation.policyOptions.wdAmt4);
  var wdAmt5 = Number(quotation.policyOptions.wdAmt5);
  var wdPer1 = Number(quotation.policyOptions.wdPer1);
  var wdPer2 = Number(quotation.policyOptions.wdPer2);
  var wdPer3 = Number(quotation.policyOptions.wdPer3);
  var wdPer4 = Number(quotation.policyOptions.wdPer4);
  var wdPer5 = Number(quotation.policyOptions.wdPer5);
  var aror = ['IRR_UI', 'IRR_Low', 'IRR_High', 'IRR_Zero', 'IRR_WD'];
  if (dealerGroup === 'AGENCY' || dealerGroup === 'SYNERGY') {
    dealerGroup = 'AGENCY';
  } else if (dealerGroup === 'DIRECT') {
    dealerGroup = 'DIRECT';
  } else {
    dealerGroup = 'IFA';
  }
  if (isNaN(startYr1)) {
    startYr1 = 0;
  }
  if (isNaN(startYr2)) {
    startYr2 = 0;
  }
  if (isNaN(startYr3)) {
    startYr3 = 0;
  }
  if (isNaN(startYr4)) {
    startYr4 = 0;
  }
  if (isNaN(startYr5)) {
    startYr5 = 0;
  }
  if (isNaN(endYr1)) {
    endYr1 = 0;
  }
  if (isNaN(endYr2)) {
    endYr2 = 0;
  }
  if (isNaN(endYr3)) {
    endYr3 = 0;
  }
  if (isNaN(endYr4)) {
    endYr4 = 0;
  }
  if (isNaN(endYr5)) {
    endYr5 = 0;
  }
  if (isNaN(wdAmt1)) {
    wdAmt1 = 0;
  }
  if (isNaN(wdAmt2)) {
    wdAmt2 = 0;
  }
  if (isNaN(wdAmt3)) {
    wdAmt3 = 0;
  }
  if (isNaN(wdAmt4)) {
    wdAmt4 = 0;
  }
  if (isNaN(wdAmt5)) {
    wdAmt5 = 0;
  }
  if (isNaN(wdPer1)) {
    wdPer1 = 0;
  }
  if (isNaN(wdPer2)) {
    wdPer2 = 0;
  }
  if (isNaN(wdPer3)) {
    wdPer3 = 0;
  }
  if (isNaN(wdPer4)) {
    wdPer4 = 0;
  }
  if (isNaN(wdPer5)) {
    wdPer5 = 0;
  }
  if (isNaN(la1_em_perc)) {
    la1_em_perc = 0;
  }
  if (isNaN(la1_pml)) {
    la1_pml = 0;
  }
  if (isNaN(pml_period)) {
    pml_period = 0;
  }
  if (isNaN(la1_em_perc2)) {
    la1_em_perc2 = 0;
  }
  if (isNaN(la1_pml2)) {
    la1_pml2 = 0;
  }
  if (isNaN(pml_period2)) {
    pml_period2 = 0;
  }
  if (isNaN(grossRate)) {
    grossRate = 0;
  }
  if (isNaN(charge)) {
    charge = 0;
  }
  if (isNaN(inWDRate)) {
    inWDRate = 0;
  }
  var round = function(value, position) {
    var num = Number(value);
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(num, scale)), scale);
  };
  var sumwithDraw = function(currentYear) {
    var sumwd = math.bignumber(0);
    if (wdOption === 'amount') {
      if (currentYear >= startYr1 && currentYear <= endYr1) {
        sumwd = math.add(sumwd, math.bignumber(wdAmt1));
      }
      if (currentYear >= startYr2 && currentYear <= endYr2) {
        sumwd = math.add(sumwd, math.bignumber(wdAmt2));
      }
      if (currentYear >= startYr3 && currentYear <= endYr3) {
        sumwd = math.add(sumwd, math.bignumber(wdAmt3));
      }
      if (currentYear >= startYr4 && currentYear <= endYr4) {
        sumwd = math.add(sumwd, math.bignumber(wdAmt4));
      }
      if (currentYear >= startYr5 && currentYear <= endYr5) {
        sumwd = math.add(sumwd, math.bignumber(wdAmt5));
      }
    } else if (wdOption === 'percentage') {
      if (currentYear >= startYr1 && currentYear <= endYr1) {
        sumwd = math.add(sumwd, math.bignumber(wdPer1 / 100));
      }
      if (currentYear >= startYr2 && currentYear <= endYr2) {
        sumwd = math.add(sumwd, math.bignumber(wdPer2 / 100));
      }
      if (currentYear >= startYr3 && currentYear <= endYr3) {
        sumwd = math.add(sumwd, math.bignumber(wdPer3 / 100));
      }
      if (currentYear >= startYr4 && currentYear <= endYr4) {
        sumwd = math.add(sumwd, math.bignumber(wdPer4 / 100));
      }
      if (currentYear >= startYr5 && currentYear <= endYr5) {
        sumwd = math.add(sumwd, math.bignumber(wdPer5 / 100));
      }
    }
    return sumwd;
  };
  var getsumCol11M = function(currMonth, rawData, colName) {
    var accColM = math.bignumber(0);
    for (var i = 1; i <= 11; i++) {
      var tempRaw = rawData[currMonth - i];
      var tempval = math.bignumber(0);
      if (colName === 'M') {
        tempval = tempRaw.WD_Amount;
      } else if (colName === 'J') {
        tempval = tempRaw.elm_Fee;
      } else if (colName === 'R') {
        tempval = tempRaw.policy_Fee;
      } else if (colName === 'S') {
        tempval = tempRaw.Ongoing_Fee;
      } else if (colName === 'T') {
        tempval = tempRaw.Cost_Insurance;
      }
      accColM = math.add(accColM, tempval);
    }
    return accColM;
  };
  var polcyYear = function(month) {
    return math.floor((month - 1) / 12) + 1;
  };
  var coiRate = function(rates, countryClass, age, riskClass, smoke, gender) {
    var colnum = 1;
    if (gender + smoke === 'MN') {
      colnum = 1;
    } else if (gender + smoke === 'MY') {
      colnum = 2;
    } else if (gender + smoke === 'FN') {
      colnum = 3;
    } else if (gender + smoke === 'FY') {
      colnum = 4;
    }
    if (riskClass === 'P') {
      colnum += 4;
    } else if (riskClass === 'S') {
      colnum += 8;
    } else if (riskClass === 'SS') {
      colnum += 12;
    }
    age = math.min(120, age);
    return rates.COI[countryClass]['' + age][colnum - 1];
  };
  var getPrevTpRate = function(month, rawData, type) {
    if (type === 'X') {
      return rawData[month - 12].TPX;
    } else if (type === 'Y') {
      return rawData[month - 12].TPY;
    } else if (type === 'XY') {
      return rawData[month - 12].TPXY;
    }
  };
  var calcIllustration = function(month, arorkey, illustration, minLaAge) {
    var sustainTestResult = true;
    var breakYearTemp = 'N.A.';
    var endowAgeTemp = 'N.A.';
    var day1SVTemp = math.bignumber(0);
    var pfd_rate = rates.fee['PolicyFeeDuration'];
    var fpisp_rate = rates.fee['PolicyFee'];
    var oafisp_rate = rates.fee['OAF_ISP'];
    var min_sa_rate = rates.minAndMax['minSA']; /** var ef_isp_rate = rates.fee['EF_ISP']; */
    var tdc_fac = rates.fee['TDCFac'];
    var wdfee_rate = rates.fee['WithdrawalFee']; /** var tdc_rate = rates.commission[dealerGroup]['ISP']; */
    var ef_isp_TDC = ef_isp_rate;
    if (quotation.agent.dealerGroup === 'DIRECT') {
      ef_isp_TDC = 0.06;
    }
    var in_IRR = 0;
    if (arorkey === 'IRR_UI') {
      in_IRR = (grossRate - charge) / 100;
    } else if (arorkey === 'IRR_WD') {
      in_IRR = inWDRate / 100;
    } else {
      in_IRR = rates.IRR[arorkey];
    }
    var iCountryClass = rates.countryClass[quotation.iResidence];
    var pCountryClass = null;
    if (policytype === 'joint') {
      pCountryClass = rates.countryClass[quotation.pResidence];
    }
    var currnav_rate = rates.exchange[ccy];
    var min_nav = math.max(math.multiply(math.bignumber(150000), math.bignumber(currnav_rate)), math.multiply(math.bignumber(0.15), math.bignumber(premium)));
    var withDraw_Fee = math.multiply(math.bignumber(wdfee_rate), math.bignumber(currnav_rate));
    var rawData = [];
    var defaultRawData = {
      plYear: 0,
      plMonth: 0,
      iAge: 0,
      pAge: 0,
      sumSa: 0,
      allocated_Premium_BOM: math.bignumber(0),
      Initial_Premium: math.bignumber(0),
      elm_Fee: math.bignumber(0),
      account_Value_EOM: math.bignumber(0),
      account_Value_BOM: math.bignumber(0),
      sum_Risk_BOM: math.bignumber(0),
      account_Value_WD: math.bignumber(0),
      policy_Fee: math.bignumber(0),
      Trailer_Fee: math.bignumber(0),
      Fund_Return: math.bignumber(0),
      Ongoing_Fee: math.bignumber(0),
      Cost_Insurance: math.bignumber(0),
      WD_Amount: math.bignumber(0),
      WD_Fee: math.bignumber(0),
      Surrender_Charge_EOM: math.bignumber(0),
      sumRisk_WD: math.bignumber(0),
      sum_assured_WD: math.bignumber(0),
      accClount_M: math.bignumber(0),
      death_Benefit_EOM: math.bignumber(0),
      surrender_Value_EOM: math.bignumber(0),
      TPX: math.bignumber(0),
      TPY: math.bignumber(0),
      TPXY: math.bignumber(0),
      QXYT_1: math.bignumber(0)
    };
    rawData.push(Object.assign({}, defaultRawData));
    for (var m = 1; m <= month; m++) {
      var prevMonth = rawData[m - 1];
      var currMonth = Object.assign({}, defaultRawData);
      currMonth.plYear = polcyYear(m);
      currMonth.plMonth = m;
      currMonth.iAge = quotation.iAge + currMonth.plYear - 1;
      currMonth.pAge = quotation.pAge + currMonth.plYear - 1;
      var coi_rate = 0;
      if (policytype === 'single') {
        coi_rate = coiRate(rates, iCountryClass, currMonth.iAge, riskClass, quotation.iSmoke, quotation.iGender);
        coi_rate = math.multiply(math.bignumber(coi_rate), math.add(math.bignumber(1), math.bignumber(la1_em_perc / 100)));
        if (currMonth.iAge < quotation.iAge + pml_period) {
          coi_rate = math.add(coi_rate, math.bignumber(la1_pml));
        }
        coi_rate = math.min(coi_rate, 1000);
      } else {
        var coi_rate_x = coiRate(rates, pCountryClass, currMonth.pAge, riskClass, quotation.pSmoke, quotation.pGender);
        coi_rate_x = math.multiply(math.bignumber(coi_rate_x), math.add(math.bignumber(1), math.bignumber(la1_em_perc / 100)));
        if (currMonth.pAge < quotation.pAge + pml_period) {
          coi_rate_x = math.add(coi_rate_x, math.bignumber(la1_pml));
        }
        coi_rate_x = math.min(coi_rate_x, 1000);
        var coi_rate_Y = coiRate(rates, iCountryClass, currMonth.iAge, riskClass2, quotation.iSmoke, quotation.iGender);
        coi_rate_Y = math.multiply(math.bignumber(coi_rate_Y), math.add(math.bignumber(1), math.bignumber(la1_em_perc2 / 100)));
        if (currMonth.iAge < quotation.iAge + pml_period2) {
          coi_rate_Y = math.add(coi_rate_Y, math.bignumber(la1_pml2));
        }
        coi_rate_Y = math.min(coi_rate_Y, 1000);
        var prevTpx = math.bignumber(1);
        var prevTpy = math.bignumber(1);
        var prevTpxy = math.bignumber(1);
        if (currMonth.plYear !== 1) {
          prevTpx = getPrevTpRate(m, rawData, 'X');
          prevTpy = getPrevTpRate(m, rawData, 'Y');
          prevTpxy = getPrevTpRate(m, rawData, 'XY');
        }
        currMonth.TPX = math.multiply(math.subtract(math.bignumber(1), math.divide(math.bignumber(coi_rate_x), math.bignumber(1000))), prevTpx);
        currMonth.TPY = math.multiply(math.subtract(math.bignumber(1), math.divide(math.bignumber(coi_rate_Y), math.bignumber(1000))), prevTpy);
        currMonth.TPXY = math.subtract(math.add(currMonth.TPX, currMonth.TPY), math.multiply(currMonth.TPX, currMonth.TPY)); /** currMonth.QXYT_1 =math.multiply(math.bignumber(1000), math.subtract(math.bignumber(1),math.divide(currMonth.TPXY,prevTpxy))); */
        var rateTPXY = math.bignumber(0);
        if (Number(prevTpxy) !== 0) {
          rateTPXY = math.divide(currMonth.TPXY, prevTpxy);
        }
        currMonth.QXYT_1 = math.multiply(math.bignumber(1000), math.subtract(math.bignumber(1), rateTPXY));
        coi_rate = math.max(currMonth.QXYT_1, math.bignumber(0.18));
      }
      if (m === 1) {
        currMonth.sumSa = sumSa;
        currMonth.Initial_Premium = math.bignumber(premium);
      } else {
        if (arorkey === 'IRR_WD') {
          currMonth.sumSa = prevMonth.sum_assured_WD;
        } else {
          currMonth.sumSa = sumSa;
        }
      }
      currMonth.elm_Fee = math.multiply(currMonth.Initial_Premium, math.bignumber(ef_isp_rate));
      currMonth.allocated_Premium_BOM = math.subtract(currMonth.Initial_Premium, currMonth.elm_Fee);
      currMonth.account_Value_BOM = math.add(currMonth.allocated_Premium_BOM, prevMonth.account_Value_EOM);
      currMonth.sum_Risk_BOM = math.max(0, math.subtract(math.bignumber(currMonth.sumSa), currMonth.account_Value_BOM));
      var sumWithDrawValue = sumwithDraw(currMonth.plYear);
      if (arorkey === 'IRR_WD') {
        var accVal_Multip_WDval = math.bignumber(0);
        if (math.mod((m - 1), 12) === 0) {
          if (wdOption === 'amount') {
            accVal_Multip_WDval = sumWithDrawValue;
          } else if (wdOption === 'percentage') {
            accVal_Multip_WDval = math.multiply(currMonth.account_Value_BOM, sumWithDrawValue);
          }
        }
        if (Number(sumWithDrawValue) !== 0) {
          if (Number(currMonth.account_Value_BOM) > 0) {
            if (math.mod((m - 1), 12) === 0) {
              if (Number(math.subtract(currMonth.account_Value_BOM, accVal_Multip_WDval)) < Number(min_nav) || Number(currMonth.account_Value_BOM) < Number(min_nav)) {
                currMonth.WD_Amount = currMonth.account_Value_BOM;
              } else {
                if (Number(accVal_Multip_WDval) > Number(math.multiply(math.bignumber(0.1), currMonth.account_Value_BOM)) && Number(math.subtract(prevMonth.sum_assured_WD, math.subtract(accVal_Multip_WDval, math.multiply(math.bignumber(0.1), currMonth.account_Value_BOM)))) < Number(math.multiply(math.bignumber(min_sa_rate), math.bignumber(premium)))) {
                  currMonth.WD_Amount = math.add(math.subtract(prevMonth.sum_assured_WD, math.multiply(math.bignumber(min_sa_rate), math.bignumber(premium))), math.multiply(math.bignumber(0.1), currMonth.account_Value_BOM));
                } else {
                  currMonth.WD_Amount = accVal_Multip_WDval;
                }
              }
            }
          }
        }
        if (Number(currMonth.WD_Amount) !== 0) {
          if (Number(math.subtract(currMonth.account_Value_BOM, accVal_Multip_WDval)) >= Number(min_nav)) {
            if (Number(currMonth.WD_Amount) > Number(math.multiply(math.bignumber(0.1), currMonth.account_Value_BOM))) {
              currMonth.WD_Fee = withDraw_Fee;
            }
          }
        }
      }
      if (arorkey === 'IRR_WD') {
        var avwdTemp = math.subtract(math.subtract(currMonth.account_Value_BOM, currMonth.WD_Amount), currMonth.WD_Fee);
        if (Number(avwdTemp) === Number(prevMonth.Surrender_Charge_EOM)) {
          currMonth.account_Value_WD = math.subtract(avwdTemp, prevMonth.Surrender_Charge_EOM);
        } else {
          currMonth.account_Value_WD = avwdTemp;
        }
      } else {
        currMonth.account_Value_WD = currMonth.account_Value_BOM;
      }
      if (currMonth.plYear <= pfd_rate) {
        if (math.mod((m - 1), 3) === 0) {
          currMonth.policy_Fee = math.divide(math.multiply(math.bignumber(premium), math.bignumber(fpisp_rate)), math.bignumber(4));
        }
      }
      if (math.mod((m - 1), 3) === 0) {
        currMonth.Ongoing_Fee = math.divide(math.multiply(math.bignumber(premium), math.bignumber(oafisp_rate)), math.bignumber(4));
      }
      if (Number(currMonth.account_Value_BOM) < 0) {
        currMonth.sum_assured_WD = math.bignumber(currMonth.sumSa);
      } else {
        if (Number(currMonth.WD_Amount) > Number(math.multiply(math.bignumber(0.1), currMonth.account_Value_BOM))) {
          currMonth.sum_assured_WD = math.subtract(math.bignumber(currMonth.sumSa), math.subtract(currMonth.WD_Amount, math.multiply(math.bignumber(0.1), currMonth.account_Value_BOM)));
        } else {
          currMonth.sum_assured_WD = math.bignumber(currMonth.sumSa);
        }
      }
      if (m <= 12) {
        currMonth.accClount_M = !math.isZero(currMonth.WD_Amount) ? math.add(prevMonth.accClount_M, currMonth.WD_Amount) : prevMonth.accClount_M;
      } else {
        currMonth.accClount_M = math.add(currMonth.WD_Amount, getsumCol11M(m, rawData, 'M'));
      }
      currMonth.sumRisk_WD = math.max(math.bignumber(0), math.subtract(math.subtract(currMonth.sum_assured_WD, currMonth.account_Value_WD), currMonth.accClount_M));
      if (math.mod((m - 1), 3) === 0) {
        currMonth.Cost_Insurance = math.multiply(math.divide(math.divide(math.bignumber(coi_rate), math.bignumber(1000)), math.bignumber(4)), currMonth.sumRisk_WD);
      }
      var fund_reta = math.subtract(math.pow(math.add(1, in_IRR), math.divide(1, 12)), 1); /** var rundnum = 0; if(fund_reta.toString().indexOf('.')>-1){ rundnum=fund_reta.toString().split(".")[1].length-1; } if(rundnum !== 0){ fund_reta = math.bignumber(round(fund_reta,rundnum)); } */
      fund_reta = math.bignumber(fund_reta);
      currMonth.Fund_Return = math.multiply(math.subtract(currMonth.account_Value_WD, math.add(math.add(math.add(currMonth.policy_Fee, currMonth.Ongoing_Fee), currMonth.Cost_Insurance), currMonth.Trailer_Fee)), fund_reta);
      currMonth.account_Value_EOM = math.add(math.subtract(currMonth.account_Value_WD, math.add(math.add(math.add(currMonth.policy_Fee, currMonth.Ongoing_Fee), currMonth.Cost_Insurance), currMonth.Trailer_Fee)), currMonth.Fund_Return);
      if (Number(currMonth.account_Value_EOM) >= 0) {
        currMonth.death_Benefit_EOM = math.max(math.subtract(currMonth.sum_assured_WD, currMonth.accClount_M), currMonth.account_Value_EOM);
      }
      currMonth.surrender_Value_EOM = math.subtract(currMonth.account_Value_EOM, currMonth.Surrender_Charge_EOM);
      if (m === 1) {
        day1SVTemp = math.subtract(currMonth.Initial_Premium, math.add(math.add(math.add(math.add(math.add(currMonth.elm_Fee, currMonth.policy_Fee), currMonth.Ongoing_Fee), currMonth.Cost_Insurance), currMonth.Trailer_Fee), currMonth.Surrender_Charge_EOM));
      }
      var illustrationIndex = currMonth.plYear - 1;
      if (m === currMonth.plYear * 12) {
        var iData = illustration[illustrationIndex];
        iData.policyYear = currMonth.plYear;
        iData.totalPremium = premium;
        iData.ICharge = round(coi_rate, 2);
        if (!iData.age) {
          iData.age = {};
        }
        if (policytype === 'joint') {
          var inumage = quotation.iAge + currMonth.plYear;
          var pnumage = quotation.pAge + currMonth.plYear;
          iData.age = pnumage + "/" + inumage;
        } else {
          iData.age = quotation.iAge + currMonth.plYear;
        }
        if (!iData.policy_Fund) {
          iData.policy_Fund = {};
        }
        var avEom = Number(currMonth.account_Value_EOM) <= 0 ? 0 : currMonth.account_Value_EOM;
        iData.policy_Fund[arorkey] = round(avEom, 0);
        if (avEom > sumSa) {
          if (endowAgeTemp === 'N.A.') {
            endowAgeTemp = currMonth.plYear + minLaAge;
          }
        }
        if (!iData.sumAssured) {
          iData.sumAssured = {};
        }
        currMonth.sumSa = Number(avEom) <= 0 ? 0 : currMonth.sumSa;
        iData.sumAssured[arorkey] = round(currMonth.sumSa, 0);
        if (!iData.death_Benefit) {
          iData.death_Benefit = {};
        }
        currMonth.death_Benefit_EOM = Number(avEom) <= 0 ? 0 : currMonth.death_Benefit_EOM;
        iData.death_Benefit[arorkey] = round(currMonth.death_Benefit_EOM, 0);
        if (!iData.surrender_Value) {
          iData.surrender_Value = {};
        }
        currMonth.surrender_Value_EOM = Number(currMonth.surrender_Value_EOM) <= 0 ? 0 : currMonth.surrender_Value_EOM;
        iData.surrender_Value[arorkey] = round(currMonth.surrender_Value_EOM, 0);
        if (Number(currMonth.surrender_Value_EOM) > premium) {
          if (breakYearTemp === 'N.A.') {
            breakYearTemp = currMonth.plYear;
          }
        }
        if (!iData.dw_amount) {
          iData.dw_amount = {};
        }
        iData.dw_amount[arorkey] = round(math.add(getsumCol11M(m, rawData, 'M'), currMonth.WD_Amount), 0);
        if (!iData.valueOf_Premium) {
          iData.valueOf_Premium = {};
        }
        var val_of_prem = math.multiply(math.bignumber(premium), math.pow(math.add(math.bignumber(1), math.bignumber(in_IRR)), currMonth.plYear));
        iData.valueOf_Premium[arorkey] = round(val_of_prem, 0);
        if (!iData.effect_Deduct) {
          iData.effect_Deduct = {};
        }
        iData.effect_Deduct[arorkey] = round(math.subtract(val_of_prem, currMonth.surrender_Value_EOM), 0);
        if (!iData.fees) {
          iData.fees = {};
        }
        var sumcolJ = math.add(getsumCol11M(m, rawData, 'J'), currMonth.elm_Fee);
        var sumcolR = math.add(getsumCol11M(m, rawData, 'R'), currMonth.policy_Fee);
        var sumcolS = math.add(getsumCol11M(m, rawData, 'S'), currMonth.Ongoing_Fee);
        var feesTemp = math.add(math.add(sumcolJ, sumcolR), sumcolS);
        feesTemp = Number(avEom) <= 0 ? 0 : feesTemp;
        iData.fees[arorkey] = round(feesTemp, 0);
        if (!iData.insurance_Charge) {
          iData.insurance_Charge = {};
        }
        var sumcolT = math.add(getsumCol11M(m, rawData, 'T'), currMonth.Cost_Insurance);
        sumcolT = Number(avEom) <= 0 ? 0 : sumcolT;
        iData.insurance_Charge[arorkey] = round(sumcolT, 0);
        if (currMonth.plYear === 1) {
          iData.basic_TDC = round(math.multiply(math.multiply(math.bignumber(premium), math.add(math.bignumber(1), math.bignumber(tdc_fac))), math.bignumber(ef_isp_TDC)), 0);
        }
      }
      if (m === ((currMonth.plYear - 1) * 12 + 1)) {
        var iData2 = illustration[illustrationIndex];
        if (!iData2.sumAssured_WD) {
          iData2.sumAssured_WD = {};
        }
        iData2.sumAssured_WD[arorkey] = round(currMonth.sum_assured_WD, 0);
        if (!iData2.dw_fee) {
          iData2.dw_fee = {};
        }
        iData2.dw_fee[arorkey] = round(currMonth.WD_Fee, 0);
      }
      rawData.push(currMonth);
    }
    if (!illustration[0].day1SV) {
      illustration[0].day1SV = {};
    }
    illustration[0].day1SV[arorkey] = round(day1SVTemp, 0);
    if (!illustration[0].endowAge) {
      illustration[0].endowAge = {};
    }
    illustration[0].endowAge[arorkey] = endowAgeTemp;
    if (!illustration[0].breakYear) {
      illustration[0].breakYear = {};
    }
    illustration[0].breakYear[arorkey] = breakYearTemp;
    return sustainTestResult;
  };
  var minLaAge = quotation.iAge;
  if (policytype === 'joint') {
    minLaAge = math.min(quotation.pAge, quotation.iAge);
  }
  var endYear = 120 - minLaAge;
  var month = endYear * 12;
  var illustration = [];
  var defaultIllustrationData = {
    policyYear: 0,
    age: null,
    totalPremium: 0,
    sumAssured: null,
    sumAssured_WD: null,
    policy_Fund: null,
    death_Benefit: null,
    surrender_Value: null,
    fees: null,
    insurance_Charge: null,
    basic_TDC: 0,
    ICharge: 0,
    day1SV: null,
    endowAge: null,
    breakYear: null,
    valueOf_Premium: null,
    effect_Deduct: null,
    dw_amount: null,
    dw_fee: null
  };
  for (var i = 0; i < endYear; i++) {
    illustration.push(Object.assign({}, defaultIllustrationData));
  }
  for (var arorkey in aror) {
    var sustainTestResult = calcIllustration(month, aror[arorkey], illustration, minLaAge);
  }
  return illustration;
}