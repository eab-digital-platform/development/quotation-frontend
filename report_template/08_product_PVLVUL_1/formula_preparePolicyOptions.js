function(planDetail, quotation, planDetails) {
  quotDriver.preparePolicyOptions(planDetail, quotation, planDetails);
  var pdPolicyOptions = planDetail.policyOptions;
  var adjustNum = planDetail.formulas.adjustNum;
  var iAge = 0;
  var i2Age = 0;
  var maxWithdrawalAge = 0;
  var getCurrency = function(value, sign, decimals) {
    if (!_.isNumber(value)) {
      return value;
    }
    if (!_.isNumber(decimals)) {
      decimals = 2;
    }
    var text = parseFloat(value).toFixed(decimals);
    var parts = text.split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    parts[0] = (sign && sign.trim().length > 0 ? sign : '') + parts[0];
    return parts.join('.');
  };
  if (quotation.policyOptions.policytype === 'single') {
    iAge = math.number(quotation.iAge);
    maxWithdrawalAge = 120 - iAge;
  } else {
    iAge = math.number(quotation.pAge);
    i2Age = math.number(quotation.iAge);
    maxWithdrawalAge = math.max(120 - iAge, 120 - i2Age);
  }
  var pos = _.cloneDeep(planDetail.policyOptions);
  for (var i = pos.length - 1; i >= 0; i--) {
    var po = pos[i];
    var poId = po.id;
    var pgId = po.groupID;
    if (quotation.policyOptions.policytype === 'single') {
      if (pgId == 'rc2') {
        pos.splice(i, 1);
      }
      if (poId == 'LA1' || poId == 'LA2') {
        pos.splice(i, 1);
      }
    }
    if (quotation.policyOptions.withdrawalOption !== 'amount' && quotation.policyOptions.withdrawalOption !== 'percentage') {
      if (pgId == 'rateAndNumOfWD') {
        pos.splice(i, 1);
      }
    }
    if (isNaN(quotation.policyOptions.numOfWD) || quotation.policyOptions.numOfWD === '' || quotation.policyOptions.numOfWD === 0 || quotation.policyOptions.withdrawalOption === 'none') {
      if (pgId == 'wd1' || pgId == 'wd2' || pgId == 'wd3' || pgId == 'wd4' || pgId == 'wd5') {
        pos.splice(i, 1);
      }
    }
    if (quotation.policyOptions.numOfWD == '1' || quotation.policyOptions.numOfWD == 1) {
      if (pgId == 'wd2' || pgId == 'wd3' || pgId == 'wd4' || pgId == 'wd5') {
        pos.splice(i, 1);
      }
    }
    if (quotation.policyOptions.numOfWD == '2' || quotation.policyOptions.numOfWD == 2) {
      if (pgId == 'wd3' || pgId == 'wd4' || pgId == 'wd5') {
        pos.splice(i, 1);
      }
    }
    if (quotation.policyOptions.numOfWD == '3' || quotation.policyOptions.numOfWD == 3) {
      if (pgId == 'wd4' || pgId == 'wd5') {
        pos.splice(i, 1);
      }
    }
    if (quotation.policyOptions.numOfWD == '4' || quotation.policyOptions.numOfWD == 4) {
      if (pgId == 'wd5') {
        pos.splice(i, 1);
      }
    }
    if (quotation.policyOptions.withdrawalOption === 'amount') {
      if (poId === 'wdPer1' || poId === 'wdPer2' || poId === 'wdPer3' || poId === 'wdPer4' || poId === 'wdPer5') {
        pos.splice(i, 1);
      }
    }
    if (quotation.policyOptions.withdrawalOption === 'percentage') {
      if (poId === 'wdAmt1' || poId === 'wdAmt2' || poId === 'wdAmt3' || poId === 'wdAmt4' || poId === 'wdAmt5') {
        pos.splice(i, 1);
      }
    }
  }
  planDetail.inputConfig.policyOptions = pos;
  planDetail.inputConfig.policyOptions.forEach(function(pPolicyOption, poIndex) {
    if (pPolicyOption.id === 'policytype') {
      if (quotation.sameAs === 'Y') {
        pPolicyOption.disable = 'Y';
      }
    }
    if (quotation.policyOptions.policytype === 'single') {
      if (pPolicyOption.id === 'riskClassification') {
        if (quotation.iSmoke !== 'N') {
          for (var i = 0; i < pPolicyOption.options.length; i++) {
            if (eval(pPolicyOption.options[i]).value === 'SP') {
              pPolicyOption.options.splice(i, 1);
            }
          }
        }
        if (quotation.policyOptions.extraMortality > 0) {
          pPolicyOption.options = [{
            title: {
              en: "Substandard"
            },
            value: "SS"
          }];
        }
      }
    } else {
      if (pPolicyOption.id === 'riskClassification') {
        if (quotation.pSmoke !== 'N') {
          for (var i = 0; i < pPolicyOption.options.length; i++) {
            if (eval(pPolicyOption.options[i]).value === 'SP') {
              pPolicyOption.options.splice(i, 1);
            }
          }
        }
        if (quotation.policyOptions.extraMortality > 0) {
          pPolicyOption.options = [{
            title: {
              en: "Substandard"
            },
            value: "SS"
          }];
        }
      }
      if (pPolicyOption.id === 'riskClassification2') {
        if (quotation.iSmoke !== 'N') {
          for (var i = 0; i < pPolicyOption.options.length; i++) {
            if (eval(pPolicyOption.options[i]).value === 'SP') {
              pPolicyOption.options.splice(i, 1);
            }
          }
        }
        if (quotation.policyOptions.extraMortality2 > 0) {
          pPolicyOption.options = [{
            title: {
              en: "Substandard"
            },
            value: "SS"
          }];
        }
      }
    }
    if (quotation.agent.dealerGroup === 'DIRECT') {
      if (pPolicyOption.id === 'estFee') {
        pPolicyOption.options = [{
          title: {
            en: "13%"
          },
          value: "13"
        }];
        pPolicyOption.disable = 'Y';
      }
    }
    if (pPolicyOption.id === 'extraMortality' || pPolicyOption.id === 'loading' || pPolicyOption.id === 'loadingPeriod') {
      if (quotation.policyOptions.riskClassification !== 'SS') {
        if (pPolicyOption.id === 'extraMortality') { /** pPolicyOption.options = []; */
          quotation.policyOptions.extraMortality = '0';
        }
        if (pPolicyOption.id === 'loading') {
          quotation.policyOptions.loading = 0;
        }
        if (pPolicyOption.id === 'loadingPeriod') {
          quotation.policyOptions.loadingPeriod = 0;
        }
        pPolicyOption.disable = 'Y';
        pPolicyOption.hintMsg = "";
      } else {
        pPolicyOption.disable = 'N';
        if (quotation.policyOptions.loading === 0 || quotation.policyOptions.loading === '0' || isNaN(quotation.policyOptions.loading)) {
          quotation.policyOptions.loading = null;
        }
        if (quotation.policyOptions.loadingPeriod === 0 || quotation.policyOptions.loadingPeriod === '0' || isNaN(quotation.policyOptions.loadingPeriod)) {
          quotation.policyOptions.loadingPeriod = undefined;
        }
        if (pPolicyOption.id === 'extraMortality') {
          if (isNaN(quotation.policyOptions.loading) || quotation.policyOptions.loading === 0 || quotation.policyOptions.loading === '0') {
            pPolicyOption.mandatory = 'Y';
          } else {
            pPolicyOption.mandatory = 'N';
          } /** pPolicyOption.hintMsg = "If substandard risk classification is selected, either Extra Mortality or the Per Mille Loading inputs has to be completed. "; */
        }
        if (pPolicyOption.id === 'loading') {
          if (isNaN(quotation.policyOptions.extraMortality) || quotation.policyOptions.extraMortality === 0 || quotation.policyOptions.extraMortality === '0') {
            pPolicyOption.mandatory = 'Y';
          } else {
            pPolicyOption.mandatory = 'N';
          } /** pPolicyOption.hintMsg = "If substandard risk classification is selected, either Extra Mortality or the Per Mille Loading inputs has to be completed. "; */
        }
        if (pPolicyOption.id === 'loadingPeriod') {
          if (quotation.policyOptions.loading !== null && quotation.policyOptions.loading !== '' && quotation.policyOptions.loading !== undefined) {
            pPolicyOption.mandatory = 'Y';
            pPolicyOption.disable = 'N';
          } else {
            pPolicyOption.mandatory = 'N';
            pPolicyOption.disable = 'Y';
          }
          pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '.';
          quotation.policyOptions.loadingPeriod = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.loadingPeriod, pPolicyOption.min, pPolicyOption.max, 1);
        }
      }
    }
    if (pPolicyOption.id === 'extraMortality2' || pPolicyOption.id === 'loading2' || pPolicyOption.id === 'loadingPeriod2') {
      if (quotation.policyOptions.riskClassification2 !== 'SS') {
        if (pPolicyOption.id === 'extraMortality2') { /** pPolicyOption.options = [];*/
          quotation.policyOptions.extraMortality2 = '0';
        }
        if (pPolicyOption.id === 'loading2') {
          quotation.policyOptions.loading2 = 0;
        }
        if (pPolicyOption.id === 'loadingPeriod2') {
          quotation.policyOptions.loadingPeriod2 = 0;
        }
        pPolicyOption.disable = 'Y';
        pPolicyOption.hintMsg = "";
      } else {
        pPolicyOption.disable = 'N';
        if (quotation.policyOptions.loading2 === 0 || quotation.policyOptions.loading2 === '0' || isNaN(quotation.policyOptions.loading2)) {
          quotation.policyOptions.loading2 = null;
        }
        if (quotation.policyOptions.loadingPeriod2 === 0 || quotation.policyOptions.loadingPeriod2 === '0' || isNaN(quotation.policyOptions.loadingPeriod2)) {
          quotation.policyOptions.loadingPeriod2 = undefined;
        }
        if (pPolicyOption.id === 'extraMortality2') {
          if (isNaN(quotation.policyOptions.loading2) || quotation.policyOptions.loading2 === 0 || quotation.policyOptions.loading2 === '0') {
            pPolicyOption.mandatory = 'Y';
          } else {
            pPolicyOption.mandatory = 'N';
          } /** pPolicyOption.hintMsg = "If substandard risk classification is selected, either Extra Mortality or the Per Mille Loading inputs has to be completed. "; */
        }
        if (pPolicyOption.id === 'loading2') {
          if (isNaN(quotation.policyOptions.extraMortality2) || quotation.policyOptions.extraMortality2 === 0 || quotation.policyOptions.extraMortality2 === '0') {
            pPolicyOption.mandatory = 'Y';
          } else {
            pPolicyOption.mandatory = 'N';
          } /** pPolicyOption.hintMsg = "If substandard risk classification is selected, either Extra Mortality or the Per Mille Loading inputs has to be completed. "; */
        }
        if (pPolicyOption.id === 'loadingPeriod2') {
          if (quotation.policyOptions.loading2 !== null && quotation.policyOptions.loading2 !== '' && quotation.policyOptions.loading2 !== undefined) {
            pPolicyOption.mandatory = 'Y';
            pPolicyOption.disable = 'N';
          } else {
            pPolicyOption.mandatory = 'N';
            pPolicyOption.disable = 'Y';
          }
          pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '.';
          quotation.policyOptions.loadingPeriod2 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.loadingPeriod2, pPolicyOption.min, pPolicyOption.max, 1);
        }
      }
    }
    if (pPolicyOption.id === 'grossInvRate') {
      pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '%.';
      let groosvalue = quotation.policyOptions.grossInvRate;
      if (groosvalue > pPolicyOption.max) {
        groosvalue = pPolicyOption.max;
      }
      quotation.policyOptions.grossInvRate = pPolicyOption.disable === 'Y' ? null : groosvalue;
    }
    if (pPolicyOption.id === 'charge') {
      pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '%.';
      let charvalue = quotation.policyOptions.charge;
      if (charvalue > pPolicyOption.max) {
        charvalue = pPolicyOption.max;
      }
      quotation.policyOptions.charge = pPolicyOption.disable === 'Y' ? null : charvalue;
    }
    if (pPolicyOption.id === 'estFee') {
      var alertMsg = "Please note that the commission will be paid in accordance with establishment fee selected.";
      pPolicyOption.alertOnValue = {
        5: alertMsg,
        6: alertMsg,
        7: alertMsg,
        8: alertMsg,
        9: alertMsg,
        10: alertMsg,
        11: alertMsg,
        12: alertMsg,
        13: alertMsg
      };
    }
    if (pPolicyOption.id === 'invRateForWD') {
      pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '%.';
      let invWDvalue = quotation.policyOptions.invRateForWD;
      if (invWDvalue > pPolicyOption.max) {
        invWDvalue = pPolicyOption.max;
      }
      quotation.policyOptions.invRateForWD = pPolicyOption.disable === 'Y' ? null : invWDvalue;
    } /** if (pPolicyOption.id === 'loadingPeriod') { pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '.'; quotation.policyOptions.loadingPeriod = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.loadingPeriod, pPolicyOption.min, pPolicyOption.max, 1); } */
    if (pPolicyOption.id === 'invRateForWD' || pPolicyOption.id === 'numOfWD') {
      if (quotation.policyOptions.withdrawalOption !== 'amount' && quotation.policyOptions.withdrawalOption !== 'percentage') {
        quotation.policyOptions.invRateForWD = '';
        quotation.policyOptions.numOfWD = '';
        pPolicyOption.disable = 'Y';
      } else {
        pPolicyOption.disable = 'N';
        if (pPolicyOption.id === 'numOfWD') {
          pPolicyOption.mandatory = 'Y';
        }
      }
    }
    if (pPolicyOption.id === 'wdAmt1' || pPolicyOption.id === 'wdAmt2' || pPolicyOption.id === 'wdAmt3' || pPolicyOption.id === 'wdAmt4' || pPolicyOption.id === 'wdAmt5') {
      pPolicyOption.mandatory = 'Y';
      var symbol = '';
      if (quotation.ccy === 'USD') {
        pPolicyOption.min = 20000;
        symbol = 'US$';
      } else if (quotation.ccy === 'SGD') {
        pPolicyOption.min = 28000;
        symbol = 'S$';
      } else if (quotation.ccy === 'CHF') {
        pPolicyOption.min = 20000;
        symbol = 'CHF';
      } else if (quotation.ccy === 'EUR') {
        pPolicyOption.min = 18000;
        symbol = '€';
      } else if (quotation.ccy === 'GBP') {
        pPolicyOption.min = 16000;
        symbol = '£';
      } else if (quotation.ccy === 'JPY') {
        pPolicyOption.min = 2200000;
        symbol = '¥';
      } else if (quotation.ccy === 'AUD') {
        pPolicyOption.min = 26000;
        symbol = 'A$';
      }
      pPolicyOption.hintMsg = 'Minimum withdrawal is ' + getCurrency(pPolicyOption.min, symbol, 0) + '.';
      if (pPolicyOption.id === 'wdAmt1') {
        quotation.policyOptions.wdAmt1 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.wdAmt1, pPolicyOption.min, null, pPolicyOption.factor);
      } else if (pPolicyOption.id === 'wdAmt2') {
        quotation.policyOptions.wdAmt2 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.wdAmt2, pPolicyOption.min, null, pPolicyOption.factor);
      } else if (pPolicyOption.id === 'wdAmt3') {
        quotation.policyOptions.wdAmt3 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.wdAmt3, pPolicyOption.min, null, pPolicyOption.factor);
      } else if (pPolicyOption.id === 'wdAmt4') {
        quotation.policyOptions.wdAmt4 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.wdAmt4, pPolicyOption.min, null, pPolicyOption.factor);
      } else if (pPolicyOption.id === 'wdAmt5') {
        quotation.policyOptions.wdAmt5 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.wdAmt5, pPolicyOption.min, null, pPolicyOption.factor);
      }
    }
    if (pPolicyOption.id === 'wdPer1') {
      pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '.';
      let dwp1 = quotation.policyOptions.wdPer1;
      if (dwp1 > pPolicyOption.max) {
        dwp1 = pPolicyOption.max;
      }
      quotation.policyOptions.wdPer1 = pPolicyOption.disable === 'Y' ? null : dwp1;
    } else if (pPolicyOption.id === 'wdPer2') {
      pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '.';
      let dwp2 = quotation.policyOptions.wdPer2;
      if (dwp2 > pPolicyOption.max) {
        dwp2 = pPolicyOption.max;
      }
      quotation.policyOptions.wdPer2 = pPolicyOption.disable === 'Y' ? null : dwp2;
    } else if (pPolicyOption.id === 'wdPer3') {
      pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '.';
      let dwp3 = quotation.policyOptions.wdPer3;
      if (dwp3 > pPolicyOption.max) {
        dwp3 = pPolicyOption.max;
      }
      quotation.policyOptions.wdPer3 = pPolicyOption.disable === 'Y' ? null : dwp3;
    } else if (pPolicyOption.id === 'wdPer4') {
      pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '.';
      let dwp4 = quotation.policyOptions.wdPer4;
      if (dwp4 > pPolicyOption.max) {
        dwp4 = pPolicyOption.max;
      }
      quotation.policyOptions.wdPer4 = pPolicyOption.disable === 'Y' ? null : dwp4;
    } else if (pPolicyOption.id === 'wdPer5') {
      pPolicyOption.hintMsg = 'Maximum is ' + pPolicyOption.max + '.';
      let dwp5 = quotation.policyOptions.wdPer5;
      if (dwp5 > pPolicyOption.max) {
        dwp5 = pPolicyOption.max;
      }
      quotation.policyOptions.wdPer5 = pPolicyOption.disable === 'Y' ? null : dwp5;
    }
    if (pPolicyOption.id === 'startYr1') {
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'End Year must be equal to or greater than start Year. Maximum input year is ' + pPolicyOption.max + '.'; */
      quotation.policyOptions.startYr1 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.startYr1, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
    }
    if (pPolicyOption.id === 'startYr2') {
      pPolicyOption.min = quotation.policyOptions.endYr1 + 1;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'Start Year/End Year cannot overlap with other withdrawal periods.'; */
      if (pPolicyOption.min > pPolicyOption.max) {
        quotation.policyOptions.startYr2 = '';
      } else {
        quotation.policyOptions.startYr2 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.startYr2, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
      }
    }
    if (pPolicyOption.id === 'startYr3') {
      pPolicyOption.min = quotation.policyOptions.endYr2 + 1;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'Start Year/End Year cannot overlap with other withdrawal periods.'; */
      if (pPolicyOption.min > pPolicyOption.max) {
        quotation.policyOptions.startYr3 = '';
      } else {
        quotation.policyOptions.startYr3 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.startYr3, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
      }
    }
    if (pPolicyOption.id === 'startYr4') {
      pPolicyOption.min = quotation.policyOptions.endYr3 + 1;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'Start Year/End Year cannot overlap with other withdrawal periods.'; */
      if (pPolicyOption.min > pPolicyOption.max) {
        quotation.policyOptions.startYr4 = '';
      } else {
        quotation.policyOptions.startYr4 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.startYr4, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
      }
    }
    if (pPolicyOption.id === 'startYr5') {
      pPolicyOption.min = quotation.policyOptions.endYr4 + 1;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'Start Year/End Year cannot overlap with other withdrawal periods.'; */
      if (pPolicyOption.min > pPolicyOption.max) {
        quotation.policyOptions.startYr5 = '';
      } else {
        quotation.policyOptions.startYr5 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.startYr5, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
      }
    }
    if (pPolicyOption.id === 'endYr1') {
      pPolicyOption.min = quotation.policyOptions.startYr1;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'End Year must be equal to or greater than start Year. Maximum input year is ' + pPolicyOption.max + '.'; */
      quotation.policyOptions.endYr1 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.endYr1, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
    }
    if (pPolicyOption.id === 'endYr2') {
      pPolicyOption.min = quotation.policyOptions.startYr2;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'End Year must be equal to or greater than start Year. Maximum input year is ' + pPolicyOption.max + '.'; */
      quotation.policyOptions.endYr2 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.endYr2, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
    }
    if (pPolicyOption.id === 'endYr3') {
      pPolicyOption.min = quotation.policyOptions.startYr3;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'End Year must be equal to or greater than start Year. Maximum input year is ' + pPolicyOption.max + '.'; */
      quotation.policyOptions.endYr3 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.endYr3, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
    }
    if (pPolicyOption.id === 'endYr4') {
      pPolicyOption.min = quotation.policyOptions.startYr4;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'End Year must be equal to or greater than start Year. Maximum input year is ' + pPolicyOption.max + '.'; */
      quotation.policyOptions.endYr4 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.endYr4, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
    }
    if (pPolicyOption.id === 'endYr5') {
      pPolicyOption.min = quotation.policyOptions.startYr5;
      pPolicyOption.max = maxWithdrawalAge; /** pPolicyOption.hintMsg = 'End Year must be equal to or greater than start Year. Maximum input year is ' + pPolicyOption.max + '.'; */
      quotation.policyOptions.endYr5 = pPolicyOption.disable === 'Y' ? null : quotDriver.runFunc(adjustNum, quotation.policyOptions.endYr5, pPolicyOption.min, pPolicyOption.max, pPolicyOption.factor);
    } /** if (pPolicyOption.id === 'startYr5' || pPolicyOption.id === 'endYr5' || pPolicyOption.id === 'wdAmt5' || pPolicyOption.id === 'wdPer5') { } */
  });
  return planDetail;
}