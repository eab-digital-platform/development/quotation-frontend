function(quotation, planInfo, planDetail) {
  function trunc(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.bignumber(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  }
  var isp = planInfo.premium ? math.bignumber(planInfo.premium) : 0;
  var rsp = quotation.policyOptions.rspAmount ? math.bignumber(quotation.policyOptions.rspAmount) : 0;
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    if (plan.covCode == planInfo.covCode) {
      plan.premRate = math.number(1.01);
      break;
    }
  }
  var sumAssured = math.multiply(math.bignumber(1.01), math.add(isp, rsp));
  return Math.round(math.number(sumAssured));
}