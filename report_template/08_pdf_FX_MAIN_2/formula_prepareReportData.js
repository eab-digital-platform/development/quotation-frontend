function(quotation, planInfo, planDetails, extraPara) {
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var roundneo = function(value, position) {
    var output = value;
    if (output) {
      var valueStr = value.toString();
      var valueSplit = valueStr.split('.');
      if (valueSplit && valueSplit.length == 2) {
        if (valueSplit[0] && valueSplit[1] && valueSplit[1].length > position) {
          output = math.bignumber(valueSplit[0] + '.' + valueSplit[1].substr(0, position));
          var roundUpVal = math.bignumber(0);
          var roundCheck = Number(valueSplit[1].substr(position, 1));
          if (roundCheck >= 5) {
            roundUpVal = '1';
            while (roundUpVal.length < position) {
              roundUpVal = '0' + roundUpVal;
            }
            roundUpVal = math.bignumber('0.' + roundUpVal);
            output = math.add(output, roundUpVal);
          }
        }
      }
    }
    return math.number(output);
  };
  var roundN = function(num, n) {
    return parseFloat(Math.round(num * Math.pow(10, n)) / Math.pow(10, n)).toFixed(n);
  };
  var illustrations = extraPara.illustrations;
  var basicPlan = quotation.plans[0];
  var basicIllustrations = illustrations[basicPlan.covCode];
  var db = [],
    _db = [];
  var sv = [],
    _sv = [];
  var dd = [],
    _dd = [];
  var dc = [],
    _dc = [];
  var polTerm = basicPlan.policyTerm ? basicPlan.policyTerm : 15;
  var projections = planDetails[planInfo.covCode].projection;
  var low = 999;
  var high = -1;
  for (var i = 0; i < 2; i++) {
    var id = projections[i].title.en;
    low = Math.min(Number(id), low);
    high = Math.max(Number(id), high);
  }
  var lastRow = (99 - quotation.iAge);
  for (var i = 0; i < lastRow; i++) {
    var polYr = i + 1;
    var row = {};
    var basicIllustration = basicIllustrations[i];
    var polYr_age = polYr + '/' + basicIllustration.age;
    var totalPremPaid = getCurrency(round(Number(basicIllustration.totalPremPaid), 0), ' ', 0);
    var dbrow = {
      polYr_age: polYr_age,
      totalPremPaid: totalPremPaid,
      GteedDB: getCurrency(round(Number(basicIllustration.GteedDB), 0), ' ', 0),
      nonGteedDB_low: getCurrency(round(Number(basicIllustration.nonGteedDB[low]), 0), ' ', 0),
      totalDB_low: getCurrency(round(Number(basicIllustration.totalDB[low]), 0), ' ', 0),
      nonGteedDB_high: getCurrency(round(Number(basicIllustration.nonGteedDB[high]), 0), ' ', 0),
      totalDB_high: getCurrency(round(Number(basicIllustration.totalDB[high]), 0), ' ', 0),
    };
    var ddrow = {
      polYr_age: polYr_age,
      totalPremPaid: totalPremPaid,
      effectOfDeduct_high: getCurrency(round(Number(basicIllustration.effectOfDeduct[high]), 0), ' ', 0),
      VOP_high: getCurrency(round(Number(basicIllustration.VOP[high]), 0), ' ', 0),
      totalSV_high: getCurrency(round(Number(basicIllustration.totalSV[high]), 0), ' ', 0),
      effectOfDeduct_low: getCurrency(round(Number(basicIllustration.effectOfDeduct[low]), 0), ' ', 0),
      VOP_low: getCurrency(round(Number(basicIllustration.VOP[low]), 0), ' ', 0),
      totalSV_low: getCurrency(round(Number(basicIllustration.totalSV[low]), 0), ' ', 0)
    };
    var dcrow = {
      polYr_age: polYr_age,
      totalPremPaid: totalPremPaid,
      totalDistribCost: getCurrency(round(Number(basicIllustration.totalDistribCost), 0), ' ', 0)
    };
    var svrow = {
      polYr_age: polYr_age,
      totalPremPaid: totalPremPaid,
      GteedSV: getCurrency(round(Number(basicIllustration.GteedSV), 0), ' ', 0),
      nonGteedSV_low: getCurrency(round(Number(basicIllustration.nonGteedSV[low]), 0), ' ', 0),
      totalSV_low: getCurrency(round(Number(basicIllustration.totalSV[low]), 0), ' ', 0),
      nonGteedSV_high: getCurrency(round(Number(basicIllustration.nonGteedSV[high]), 0), ' ', 0),
      totalSV_high: getCurrency(round(Number(basicIllustration.totalSV[high]), 0), ' ', 0),
    };
    if (polYr <= 20 || (polYr > 20 && polYr <= 40 && polYr % 5 === 0) || polYr == lastRow) {
      db.push(dbrow);
      dd.push(ddrow);
      dc.push(dcrow);
      sv.push(svrow);
    }
    var age = basicIllustration.age;
    if (age == 55 || age == 60 || age == 65) {
      _db.push(Object.assign({}, dbrow, {
        polYr_age: 'AGE ' + basicIllustration.age
      }));
      _dd.push(Object.assign({}, ddrow, {
        polYr_age: 'AGE ' + basicIllustration.age
      }));
      _dc.push(Object.assign({}, dcrow, {
        polYr_age: 'AGE ' + basicIllustration.age
      }));
      _sv.push(Object.assign({}, svrow, {
        polYr_age: 'AGE ' + basicIllustration.age
      }));
    }
  }
  db.push({});
  sv.push({});
  dd.push({});
  dc.push({});
  for (var i in _db) {
    db.push(_db[i]);
    sv.push(_sv[i]);
    dd.push(_dd[i]);
    dc.push(_dc[i]);
  }
  var reductYield = basicIllustrations[0].reductYield;
  if (reductYield) {
    reductYield = math.multiply(math.bignumber(reductYield), math.bignumber(100));
    reductYield = round(Number(reductYield), 2);
    reductYield = roundN(reductYield, 2);
  }
  var yieldValue = basicIllustrations[0].yieldValue;
  if (yieldValue) {
    yieldValue = getCurrency(round(Number(yieldValue), 0), ' ', 0);
  }
  var map = {
    FPX: {
      iLife: 1,
      iTPD: 1,
      pLife: 1,
      pTPD: 1,
    },
    FSX: {
      iLife: 1,
      iTPD: 1,
      pLife: 1,
      pTPD: 1,
    },
    CARB: {
      iCi: 1
    },
    LARB: {
      iCi: 1
    },
    LVT: {
      iLife: 1,
      iTPD: 1,
      iCi: 1
    },
    LTT: {
      iLife: 1,
      iTPD: 1
    },
    EPB: {
      iTPD: 1
    },
    ADBR: {},
    WPSR: {
      iCi: 1
    },
    WPTN: {
      pLife: 1,
      pTPD: 1,
    },
    WPPT: {
      pLife: 1,
      pTPD: 1,
      pCi: 1
    }
  };
  var options = quotation.policyOptions;
  var proposer = {
      basic: [],
      rider: []
    },
    insured = {
      basic: [],
      rider: []
    };
  var polTerms = {};
  for (var i in quotation.plans) {
    var plan = quotation.plans[i];
    var premTerm = plan.premTerm;
    if (premTerm && typeof premTerm == 'string') {
      polTerms[plan.covCode] = math.number(premTerm.substring(3, premTerm.length));
      if (premTerm.substring(0, 3) == "age") polTerms[plan.covCode] = plan.premTermDesc;
    }
  }
  var getRow = function(covCode, type, position) {
    if (options[type] && map[covCode] && map[covCode][type]) {
      var isNA = ((["iTPD", "pTPD"].indexOf(type) == -1 && math.number(options[type])) === 0 || (["iTPD", "pTPD"].indexOf(type) > -1 && math.number(options[type]) == 1)) || (quotation.baseProductCode == covCode && type[0] == 'p');
      return {
        name: position ? '' : planDetails[covCode].covName.en,
        type: ["pLife", "iLife"].indexOf(type) > -1 ? "Life" : ["iTPD", "pTPD"].indexOf(type) > -1 ? "TPD" : "CI",
        value: isNA ? "NA" : options[type] + (["iTPD", "pTPD"].indexOf(type) > -1 ? "" : ".00"),
        term: isNA ? "NA" : polTerms[covCode]
      };
    }
  };
  var pIds = ["pLife", "pTPD", "pCi"];
  var iIds = ["iLife", "iTPD", "iCi"];
  var hasPLoading = options.pLoading;
  var hasILoading = options.iLoading;
  for (var i in quotation.plans) {
    var plan = quotation.plans[i];
    var pRowCnt = 0,
      iRowCnt = 0;
    var emptyRow = {
      name: ' ',
      type: ' ',
      value: ' ',
      term: ' '
    };
    if (hasPLoading) {
      for (var j in pIds) {
        var row = getRow(plan.covCode, pIds[j], pRowCnt);
        if (row) {
          pRowCnt++;
          if (plan.covCode == quotation.baseProductCode) {
            proposer.basic.push(row);
          } else {
            proposer.rider.push(row);
          }
        }
      }
    }
    if (!hasPLoading || !pRowCnt) {
      if (plan.covCode == quotation.baseProductCode) {
        proposer.basic.push(emptyRow);
      } else {
        proposer.rider.push(emptyRow);
      }
    }
    if (hasILoading) {
      for (var j in iIds) {
        var row = getRow(plan.covCode, iIds[j], iRowCnt, i === 0);
        if (row) {
          iRowCnt++;
          if (plan.covCode == quotation.baseProductCode) {
            insured.basic.push(row);
          } else {
            insured.rider.push(row);
          }
        }
      }
    }
    if (!hasILoading || !iRowCnt) {
      if (plan.covCode == quotation.baseProductCode) {
        insured.basic.push(emptyRow);
      } else {
        insured.rider.push(emptyRow);
      }
    }
  }
  return {
    illustrateData_DB: db,
    illustrateData_SV: sv,
    illustrateData_deduct: dd,
    illustrateData_totalDistribCost: dc,
    reductYield: reductYield,
    yieldAtAge: (basicIllustrations[0].yieldAtAge),
    yieldAtRate: roundN(basicIllustrations[0].yieldAtRate * 100, 2),
    yieldValue: '$' + yieldValue,
    rate_high: roundN(basicIllustrations[0].projRate[high] * 100, 1),
    rate_low: roundN(basicIllustrations[0].projRate[low] * 100, 1),
    sumLoad: {
      hasLoading: hasPLoading || hasILoading ? 'Y' : 'N',
      proposer: proposer,
      insured: insured
    }
  };
}