function(quotation, planInfo, planDetails) {
  var basicCarePlan;
  for (var p in quotation.plans) {
    if (quotation.plans[p].covCode === 'ASP') {
      basicCarePlan = quotation.plans[p];
      break;
    }
  }
  planInfo.paymentMethod = basicCarePlan.paymentMethod;
  planInfo.payFreq = basicCarePlan.payFreq;
  var covClass = quotation.plans[0].covClass;
  var gender = quotation.iGender;
  var age = quotation.iAge;
  if (covClass) {
    var planDetail = planDetails[planInfo.covCode];
    var modalFactor = math.bignumber(1);
    var covMonth = 12;
    var payModes = planDetail.payModes;
    for (var i in payModes) {
      if (payModes[i].mode === planInfo.payFreq) {
        modalFactor = math.bignumber(payModes[i].factor);
        covMonth = payModes[i].covMonth;
      }
    }
    var rate = math.bignumber(planDetail.rates.premRate[covClass][gender][age]);
    var regularPrem = math.divide(math.floor(math.multiply(math.multiply(rate, modalFactor), 100)), 100);
    var prem = regularPrem;
    if (planDetails[quotation.baseProductCode].gstInd === 'Y') {
      var gstRate = math.bignumber(planDetails[quotation.baseProductCode].gstRate);
      var gstAmt = math.round(math.multiply(regularPrem, gstRate), 2);
      prem = math.round(math.add(prem, gstAmt), 1);
      if (planInfo.payFreq === 'M') {
        planInfo.tax = {
          monthTax: math.number(gstAmt),
          yearTax: math.number(math.divide(math.floor(math.multiply(gstAmt, 12 / covMonth, 100)), 100))
        };
      } else if (planInfo.payFreq === 'A') {
        planInfo.tax = {
          yearTax: math.number(gstAmt)
        };
      }
    }
    planInfo.premium = math.number(prem);
    planInfo.yearPrem = math.number(math.divide(math.floor(math.multiply(prem, 12 / covMonth, 100)), 100));
    planInfo.covClass = covClass;
  }
}