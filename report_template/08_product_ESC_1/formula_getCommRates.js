function(quotation, planInfo, planDetail) { /*esc_getCommRates*/
  var rateKey = 'ALL';
  var rateTable = planDetail.rates.commRate[rateKey];
  var policyTerm = Number.parseInt(planInfo.policyTerm);
  var benefitPeriod = (policyTerm > 25) ? policyTerm - quotation.iAge : policyTerm;
  var lookupYr = (benefitPeriod > 25) ? 25 : benefitPeriod;
  var commRates;
  if (rateTable) {
    _.each(rateTable, (rates, key) => {
      if (lookupYr == key) {
        commRates = rates;
      }
    });
  }
  return commRates || [];
}