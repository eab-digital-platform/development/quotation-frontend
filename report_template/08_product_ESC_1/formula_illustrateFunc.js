function(quotation, planInfo, planDetails, extraPara) { /*esc_illustrateFunc*/
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  var planDetail = planDetails[planInfo.covCode];
  var sumInsured = math.bignumber(planInfo.sumInsured);
  var yearPrem = math.bignumber(planInfo.yearPrem);
  var commRates = runFunc(planDetail.formulas.getCommRates, quotation, planInfo, planDetail);
  var policyTerm = Number.parseInt(planInfo.policyTerm);
  var benefitPeriod = (policyTerm > 25) ? policyTerm - quotation.iAge : policyTerm;
  var illYrs = benefitPeriod;
  var totYearPrems = [0];
  var totComms = [0];
  for (var i = 1; i <= illYrs; i++) {
    let totYearPrem = trunc(math.multiply(math.bignumber(i), yearPrem), 0);
    totYearPrems.push(totYearPrem);
    let tdcYr = (i > 7) ? 7 : i;
    let commRate = commRates[tdcYr - 1] || 0;
    let comm = math.multiply(math.bignumber(commRate), yearPrem);
    let totComm = math.add(comm, totComms[i - 1]);
    totComms.push(trunc(totComm, 2));
  }
  var illustration = [];
  for (var i = 1; i <= illYrs; i++) {
    illustration.push({
      totYearPrem: math.number(totYearPrems[i]),
      tdc: math.number(trunc(totComms[i], 0)),
      maxGuaranteedCIB: trunc(planInfo.sumInsured, 0)
    });
  }
  return illustration;
}