function(quotation, planInfo, planDetail, age) { /*esc_getPremRate*/
  var rateKey = quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS');
  var policyTerm = Number.parseInt(planInfo.policyTerm);
  var benefitPeriod = (policyTerm > 25) ? policyTerm - age : policyTerm;
  var returnRate = planDetail.rates.premRate[rateKey][age][benefitPeriod - 6];
  return returnRate;
}