function(planDetail, quotation) {
  return [{
    value: '15_YR',
    title: '15 Years'
  }, {
    value: '20_YR',
    title: '20 Years'
  }, {
    value: '25_YR',
    title: '25 Years'
  }, {
    value: '60_TA',
    title: 'To Age 60'
  }, {
    value: '65_TA',
    title: 'To Age 65'
  }, {
    value: '75_TA',
    title: 'To Age 75'
  }];
}