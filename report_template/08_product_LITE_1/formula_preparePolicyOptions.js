function(planDetail, quotation, planDetails) {
  quotDriver.preparePolicyOptions(planDetail, quotation, planDetails);
  quotation.policyOptions.occupationClass = quotation.iOccupationClass;
  var pdPolicyOptions = planDetail.policyOptions;
  planDetail.inputConfig.policyOptions.forEach(function(pPolicyOption, poIndex) {
    if (pPolicyOption.id === 'multiFactor') {
      if (quotation.iAge > 60) {
        var options = pPolicyOption.options;
        options.splice(0, 6);
        pPolicyOption.options = options;
        pPolicyOption.disable = 'Y';
        quotation.policyOptions.multiFactor = "1";
      }
    }
  });
}