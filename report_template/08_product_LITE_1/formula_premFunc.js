function(quotation, planInfo, planDetail) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var getDiscountRate = function() {
    if (planDetail.campaign === undefined) {
      return 0;
    }
    var campaign = planDetail.campaign[0];
    if (!campaign) {
      return 0;
    }
    return campaign.discount;
  };
  var countryCode = quotation.iResidence;
  var isCountryMatch = (countryCode === "R51" || countryCode === "R52" || countryCode === "R53" || countryCode === "R54");
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  var discount = getDiscountRate();
  if (quotation.paymentMode == 'A') {
    if (planInfo.premTerm && sumInsured) {
      if (isCountryMatch) {
        var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
        var premBeforeLsd = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
        var lsd = -1 * runFunc(planDetail.formulas.getLsd, planDetail, sumInsured, quotation.iAge);
        var premAfterLsd = math.add(premBeforeLsd, roundDown(math.multiply(sumInsured, 0.001, lsd), 2));
        var residenceLoading = math.bignumber(runFunc(planDetail.formulas.getResidenceLoading, quotation, planInfo, planDetail));
        premAfterLsd = math.add(premAfterLsd, roundDown(math.multiply(residenceLoading, sumInsured, 0.001), 2));
        premium = premAfterLsd;
      } else {
        var lsd = -1 * runFunc(planDetail.formulas.getLsd, planDetail, sumInsured, quotation.iAge);
        var premRate = roundDown(math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge)), 2);
        var premAfterLsd = roundDown(math.multiply(math.add(lsd, premRate), sumInsured, 0.001), 2);
        premium = premAfterLsd;
      }
      if (typeof(discount) === 'number') {
        if (discount > 0) {
          premium = roundDown(math.multiply(premium, 1 - discount), 2);
        }
      } /** Add TPD Annual Premium to Basic Plan*/ /*let cQuot = Object.assign({}, quotation); let annPrem = 0; if (global && global.quotCache && global.quotCache.planDetails && global.quotCache.planDetails['LITE_TPDD']) { var tpddPlanDetail = global.quotCache.planDetails['LITE_TPDD']; annPrem = runFunc(tpddPlanDetail.formulas.premFunc, cQuot, planInfo, tpddPlanDetail); } premium = math.add(premium, annPrem);*/ /** Add TPD Annual Premium to Basic Plan*/
      return math.number(premium);
    }
  } else { /** This assumes that the planInfo.yearPrem is calculated first.*/
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
        break;
      }
    }
    var temp = math.multiply(math.bignumber(planInfo.yearPrem), modalFactor);
    temp = roundDown(temp, 2);
    return math.number(temp);
  }
}