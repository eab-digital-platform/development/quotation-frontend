function(planDetail, quotation, planDetails) {
  quotDriver.prepareAttachableRider(planDetail, quotation, planDetails);
  var riderList = []; /*planDetail.inputConfig.riderList;*/
  var showMul = false;
  if (quotation.policyOptions.multiFactor) {
    if (quotation.policyOptions.multiFactor != "1") {
      showMul = true;
    }
  }
  var chinaResid = quotation.iResidence === "R52";
  var showLITE_MBECI = false;
  var showLITE_MBCI = false;
  for (var i = 0; i < quotation.plans.length; i++) {
    if (quotation.plans[i].covCode === 'LITE_ECIB') {
      showLITE_MBECI = true;
    }
    if (quotation.plans[i].covCode === 'LITE_CIB') {
      showLITE_MBCI = true;
    }
  }
  if (riderList) { /*if (!riderList[0]) {*/
    riderList.push({
      autoAttach: 'Y',
      compulsory: 'Y',
      covCode: "LITE_TPDD",
      saRate: "",
      saRule: "0",
      hiddenRider: 'Y',
      hiddenInSelectRiderDialog: 'Y'
    });
    riderList.push({
      autoAttach: 'Y',
      compulsory: showMul ? 'Y' : 'N',
      covCode: "LITE_TPD",
      saRate: "",
      saRule: "0",
      hiddenRider: 'Y',
      hiddenInSelectRiderDialog: 'Y'
    });
    if (showMul) {
      riderList.push({
        autoAttach: 'Y',
        compulsory: 'Y',
        covCode: "LITE_MBX",
        saRate: "",
        saRule: "0"
      });
    }
    riderList.push({
      autoAttach: 'N',
      compulsory: 'N',
      covCode: "LITE_CIB",
      saRate: "",
      saRule: "0"
    });
    if (showLITE_MBCI && showMul) {
      riderList.push({
        autoAttach: 'Y',
        compulsory: 'Y',
        covCode: "LITE_MBCI",
        saRate: "",
        saRule: "0",
        hiddenInSelectRiderDialog: 'Y'
      });
    } else if (showMul) {
      riderList.push({
        autoAttach: 'N',
        compulsory: 'N',
        covCode: "LITE_MBCI",
        saRate: "",
        saRule: "0",
        hiddenInSelectRiderDialog: 'Y'
      });
    }
    if (!chinaResid) {
      riderList.push({
        autoAttach: 'Y',
        compulsory: 'N',
        covCode: "LITE_ECIB",
        saRate: "",
        saRule: "0"
      });
      if (showLITE_MBECI && showMul) {
        riderList.push({
          autoAttach: 'Y',
          compulsory: 'Y',
          covCode: "LITE_MBECI",
          saRate: "",
          saRule: "0",
          hiddenInSelectRiderDialog: 'Y'
        });
      } else if (showMul) {
        riderList.push({
          autoAttach: showMul ? 'Y' : 'N',
          compulsory: 'N',
          covCode: "LITE_MBECI",
          saRate: "",
          saRule: "0",
          hiddenInSelectRiderDialog: 'Y'
        });
      }
    }
    riderList.push({
      autoAttach: 'Y',
      compulsory: 'N',
      covCode: "LITE_ADB",
      saRate: "",
      saRule: "0"
    });
    riderList.push({
      autoAttach: 'N',
      compulsory: 'N',
      covCode: "LITE_DSB",
      saRate: "",
      saRule: "0"
    });
    riderList.push({
      autoAttach: 'N',
      compulsory: 'N',
      covCode: "LITE_CRBP",
      saRate: "",
      saRule: "0"
    });
    riderList.push({
      autoAttach: 'N',
      compulsory: 'N',
      covCode: "LITE_SPPE",
      saRate: "",
      saRule: "0"
    });
    riderList.push({
      autoAttach: 'Y',
      compulsory: 'N',
      covCode: "LITE_SPPEP",
      saRate: "",
      saRule: "0"
    });
    riderList.push({
      autoAttach: 'N',
      compulsory: 'N',
      covCode: "LITE_CIPE",
      saRate: "",
      saRule: "0"
    }); /*}*/
    var firstParty = quotation.sameAs === 'Y';
    var proposerTooOld = quotation.pAge > 60;
    var sum = 0;
    for (var i = 0; i < quotation.plans.length; i++) {
      if (quotation.plans[i].covCode === 'LITE_ECIB' || quotation.plans[i].covCode === 'LITE_CIB') {
        if (quotation.plans[i].sumInsured) {
          sum += quotation.plans[i].sumInsured;
        }
      }
    }
    var appear = true;
    if (quotation.plans[0].sumInsured) {
      if (sum < quotation.plans[0].sumInsured && quotation.plans[0].sumInsured !== 0) {
        appear = true;
      } else {
        appear = false;
      }
    }
    riderList = riderList.filter(function(rider) {
      switch (rider.covCode) {
        case 'LITE_MBX':
          return quotation.policyOptions.multiFactor != "1";
        case 'LITE_DSB':
          return quotation.plans[0].sumInsured >= 50000;
        case 'LITE_MBECI':
          return quotation.policyOptions.multiFactor != "1";
        case 'LITE_MBCI':
          return quotation.policyOptions.multiFactor != "1";
        case 'LITE_SPPE':
          return !firstParty && !proposerTooOld;
        case 'LITE_SPPEP':
          return !firstParty && !proposerTooOld;
        case 'LITE_CIPE':
          return firstParty && appear;
        default:
          return true;
      }
    });
  }
  planDetail.inputConfig.riderList = riderList;
}