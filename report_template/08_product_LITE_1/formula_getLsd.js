function(planDetail, sumInsured, age) {
  var lsdMap = [{
    band: 25000,
    lsd: 0
  }, {
    band: 50000,
    lsd: 0.9
  }, {
    band: 100000,
    lsd: 1.3
  }, {
    band: 200000,
    lsd: 1.5
  }];
  var mapping = _.findLast(lsdMap, m => sumInsured >= m.band);
  if (mapping) {
    return mapping.lsd;
  }
  return 0;
}