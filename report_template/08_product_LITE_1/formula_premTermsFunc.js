function(planDetail, quotation) {
  var premTermsList = [];
  _.each([10, 15, 20, 25, 30], (val) => {
    var age = quotation.iAge + val;
    if (quotation.policyOptions.multiFactor && quotation.policyOptions.multiFactor > 1) {
      if (age <= 70) {
        premTermsList.push({
          value: val + '_YR',
          title: val + ' Years'
        });
      }
    } else {
      if (age <= 80) {
        premTermsList.push({
          value: val + '_YR',
          title: val + ' Years'
        });
      }
    }
  });
  return premTermsList;
}