function(planDetail, quotation) {
  var policyTermList = [];
  var hasTa60 = false;
  _.each([10, 15, 20, 25], (val) => {
    if (quotation.iAge + val <= 60) {
      policyTermList.push({
        value: '' + val,
        title: val + ' years'
      });
    }
  });
  if (quotation.iAge <= 55) {
    policyTermList.push({
      value: '60',
      title: 'To Age 60'
    });
  }
  return policyTermList;
}