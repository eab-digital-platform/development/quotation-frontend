function(values) {
  var irrResult = function(values, dates, rate) {
    var r = math.add(math.bignumber(rate), 1);
    var result = math.bignumber(values[0]);
    for (var i = 1; i < values.length; i++) {
      result = math.add(result, math.divide(math.bignumber(values[i]), math.pow(r, math.divide(math.subtract(dates[i], dates[0]), 365))));
    }
    return math.bignumber(result);
  };
  var irrResultDeriv = function(values, dates, rate) {
    var r = math.add(math.bignumber(rate), 1);
    var result = math.bignumber(0);
    for (var i = 1; i < values.length; i++) {
      var frac = math.bignumber(math.divide(math.subtract(dates[i], dates[0]), 365));
      result = math.subtract(result, math.divide(math.bignumber(math.multiply(frac, math.bignumber(values[i]))), math.bignumber(math.pow(r, math.add(frac, 1)))));
    }
    return math.bignumber(result);
  };
  var dates = [];
  var positive = false;
  var negative = false;
  for (var i = 0; i < values.length; i++) {
    if (i === 0) {
      dates[i] = math.bignumber(0);
    } else {
      dates[i] = math.add(math.bignumber(dates[i - 1]), 365);
    }
    if (values[i] > 0) positive = true;
    if (values[i] < 0) negative = true;
  }
  if (!positive || !negative) return null;
  var irr = null;
  var guess = math.bignumber(-1);
  var maxGuess = math.bignumber(1);
  var trialInterval = math.bignumber(0.1);
  do {
    guess = math.add(guess, trialInterval);
    var resultRate = math.bignumber(guess);
    var epsMax = 1e-10;
    var iterMax = 50;
    var newRate, epsRate, resultValue, dValue;
    var iteration = 0;
    var contLoop = true;
    do {
      resultValue = irrResult(values, dates, resultRate);
      dValue = irrResultDeriv(values, dates, resultRate);
      newRate = math.subtract(resultRate, math.divide(resultValue, dValue));
      epsRate = math.bignumber(Math.abs(math.subtract(newRate, resultRate)));
      resultRate = newRate;
      contLoop = (epsRate > epsMax) && (Math.abs(resultValue) > epsMax);
    } while (contLoop && (++iteration < iterMax));
    if (!contLoop) {
      irr = resultRate;
    }
  } while (guess <= maxGuess && irr === null);
  return math.number(irr);
}