function(quotation, planInfo, planDetail, age) {
  var roundUp = function(value, digit) {
    var scale = Math.pow(10, digit);
    if (digit === 0) {
      return Math.ceil(value);
    } else {
      return Math.ceil(number * scale) / scale;
    }
  };
  var round = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var rateKey;
  if (planInfo.policyTerm.indexOf('YR') > -1) {
    rateKey = 'PPU';
  } else {
    rateKey = 'PPUN';
  }
  var maturityAge = quotation.pAge + Number.parseInt(planInfo.policyTerm);
  rateKey += '_' + quotation.pGender;
  rateKey += quotation.pSmoke === 'Y' ? 'S' : 'NS';
  var rates = planDetail.rates.premRate[rateKey];
  var netAnnualPremium = 0;
  var verticalKey;
  var horizontalKey;
  horizontalKey = age - 16;
  verticalKey = Number.parseInt(planInfo.policyTerm);
  if (planInfo.policyTerm.indexOf('YR') > -1) {
    netAnnualPremium = rates[verticalKey][horizontalKey];
  } else {
    netAnnualPremium = rates[maturityAge][horizontalKey];
  }
  return netAnnualPremium;
}