function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  if (planInfo.premium) {
    var mapping = planDetail.planCodeMapping;
    for (var i = 0; i < mapping.premPay.length; i++) {
      if (planInfo.premTerm == mapping.premPay[i]) {
        planInfo.planCode = planDetail.planCodeMapping[quotation.agent.dealerGroup][i];
        break;
      }
    }
    planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm);
    planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
  } else {
    planInfo.planCode = null;
  }
}