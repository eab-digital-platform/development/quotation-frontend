function(planDetail, quotation) {
  var planInfo = quotation.plans.find(p => p.covCode === planDetail.covCode);
  if (!planInfo.policyTerm) {
    return [];
  }
  var premTermList = [];
  var polTerm = Number.parseInt(planInfo.policyTerm);
  if (quotation.policyOptions.planType === 'renew') {
    var bpPremTerm = quotation.plans[0].premTerm;
    if (bpPremTerm === 'SP') {
      premTermList.push({
        value: 'SP',
        title: 'Single Premium'
      });
    } else {
      premTermList.push({
        value: planInfo.policyTerm,
        title: planInfo.policyTerm.indexOf('_TA') > -1 ? ('To Age ' + polTerm) : (polTerm + ' Years')
      });
    }
  } else if (quotation.policyOptions.planType === 'toAge') {
    var values = [15, 20];
    for (var v in values) {
      var value = values[v];
      if (Number(polTerm) - quotation.iAge - value >= 5) {
        premTermList.push({
          value: value + '_YR',
          title: value + ' Years'
        });
      }
    }
    premTermList.push({
      value: polTerm + '_TA',
      title: 'To Age ' + polTerm
    });
  }
  return premTermList;
}