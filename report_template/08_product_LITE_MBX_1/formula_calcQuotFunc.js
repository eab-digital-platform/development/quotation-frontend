function(quotation, planInfo, planDetails) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    var temp = math.round(math.multiply(value, 10000));
    temp = math.divide(temp, 10000);
    return math.divide(math.floor(math.multiply(temp, scale)), scale);
  };
  if (quotation.plans[0].premTerm) {
    planInfo.premTerm = quotation.plans[0].premTerm;
    planInfo.premTermDesc = quotation.plans[0].premTermDesc;
  }
  if (quotation.policyOptions.multiFactor && planInfo.premTerm) { /* planCode needed for prem calculation */
    var planCode = 'MBD';
    var planCode2 = 'LITE_MBX';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode;
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planCode = planCode + Number.parseInt(planInfo.premTerm);
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    var MB_codeList = {
      "2": "A",
      "3": "B",
      "4": "C",
      "5": "D",
      "6": "E",
      "7": "F"
    };
    var MB_code = MB_codeList[quotation.policyOptions.multiFactor];
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    planInfo.planCode = planCode + MB_code;
    planInfo.packagedRider = true;
    var bpInfo = quotation.plans[0];
    var amount = bpInfo.sumInsured > 0 ? bpInfo.sumInsured : 0;
    var multiFactor = quotation.policyOptions.multiFactor;
    if (multiFactor) {
      planInfo.sumInsured = bpInfo.sumInsured * multiFactor;
    }
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var modalFactor = 1;
        let cQuot = Object.assign({}, quotation);
        let cPlanInfo = Object.assign({}, planInfo);
        cQuot.paymentMode = 'A';
        var countryCode = quotation.iResidence;
        var isCountryMatch = (countryCode === "R51" || countryCode === "R52" || countryCode === "R53" || countryCode === "R54");
        let annPrem = 0;
        if (global && global.quotCache && global.quotCache.planDetails && global.quotCache.planDetails['LITE_TPDD']) {
          var tpddPlanDetail = global.quotCache.planDetails['LITE_TPD'];
          annPrem = runFunc(tpddPlanDetail.formulas.premFunc, cQuot, planInfo, tpddPlanDetail);
        }
        for (var p in planDetails['LITE'].payModes) {
          if (planDetails['LITE'].payModes[p].mode === quotation.paymentMode) {
            modalFactor = planDetails['LITE'].payModes[p].factor;
            var updated_premium = roundDown(math.multiply(math.bignumber(planInfo.yearPrem), modalFactor), 2);
            var N_annPrem = roundDown(math.multiply(math.bignumber(annPrem), modalFactor), 2);
            planInfo.premium = math.number(math.add(updated_premium, N_annPrem));
          }
          var NmodalFactor = planDetails['LITE'].payModes[p].factor;
          var Nupdated_premium = roundDown(math.multiply(math.bignumber(planInfo.yearPrem), NmodalFactor), 2);
          var NannPrem = roundDown(math.multiply(math.bignumber(annPrem), NmodalFactor), 2);
          if (planDetails['LITE'].payModes[p].mode === "S") {
            planInfo.alonetotHalfyearPrem = math.number(Nupdated_premium);
            planInfo.totHalfyearPrem = math.number(math.add(Nupdated_premium, NannPrem));
            planInfo.halfYearPrem = planInfo.totHalfyearPrem;
          }
          if (planDetails['LITE'].payModes[p].mode === "Q") {
            planInfo.alonetotQuarterPrem = math.number(Nupdated_premium);
            planInfo.totQuarterPrem = math.number(math.add(Nupdated_premium, NannPrem));
            planInfo.quarterPrem = planInfo.totQuarterPrem;
          }
          if (planDetails['LITE'].payModes[p].mode === "M") {
            planInfo.alonetotMonthPrem = math.number(Nupdated_premium);
            planInfo.totMonthPrem = math.number(math.add(Nupdated_premium, NannPrem));
            planInfo.monthPrem = planInfo.totMonthPrem;
          }
        }
        planInfo.alonetotYearPrem = planInfo.yearPrem;
        planInfo.totYearPrem = math.number(math.add(math.bignumber(planInfo.yearPrem), annPrem));
        planInfo.yearPrem = planInfo.totYearPrem;
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails[planCode2].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer < 15) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer < 20) {
            crate = commission_rate[rate][1];
          } else if (policyYearRefer < 25) {
            crate = commission_rate[rate][2];
          } else {
            crate = commission_rate[rate][3];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
        var SA_bundleInfo = {
          "2": 50000,
          "3": 33333.34,
          "4": 25000,
          "5": 20000,
          "6": 16666.67,
          "7": 14285.72
        };
        var SA_policy_lookup = 100000;
        var bundle_lsd = 0;
        var premRate = math.bignumber(runFunc(planDetails['LITE_MBX'].formulas.getPremRate, quotation, planInfo, planDetails['LITE_MBX'], quotation.iAge));
        var process_premRate = math.add(premRate, bundle_lsd);
        var annualPrem_bundle = roundDown(math.multiply(0, process_premRate, SA_policy_lookup, 0.001), 2);
        var modalFactor = 1;
        for (var p in planDetails['LITE_MBX'].payModes) {
          if (planDetails['LITE_MBX'].payModes[p].mode === quotation.paymentMode) {
            modalFactor = planDetails['LITE_MBX'].payModes[p].factor;
            break;
          }
        }
        var modlePrem_bundle = roundDown(math.multiply(annualPrem_bundle, modalFactor), 2);
        planInfo.annualPrem_bundle = math.number(annualPrem_bundle);
        planInfo.modlePrem_bundle = math.number(modlePrem_bundle);
        planInfo.bundleSA = SA_policy_lookup;
      } else {}
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}