function(quotation, planInfo, planDetail) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var getDiscountRate = function() {
    if (planDetail.campaign === undefined) {
      return 0;
    }
    var campaign = planDetail.campaign[0];
    if (!campaign) {
      return 0;
    }
    return campaign.discount;
  };
  var countryCode = quotation.iResidence;
  var isCountryMatch = (countryCode === "R51" || countryCode === "R52" || countryCode === "R53" || countryCode === "R54");
  var premium = null;
  var sumInsured = math.bignumber(planInfo.sumInsured); /*sumInsured = math.multiply(sumInsured, Number(quotation.policyOptions.multiFactor));*/
  var discount = getDiscountRate();
  if (quotation.paymentMode == 'A') {
    if (planInfo.premTerm && sumInsured) {
      if (isCountryMatch) {
        var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
        var premBeforeLsd = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
        var lsd = -1 * runFunc(planDetail.formulas.getLsd, planDetail, sumInsured, quotation.iAge);
        var premAfterLsd = math.add(premBeforeLsd, roundDown(math.multiply(sumInsured, 0.001, lsd), 2));
        var residenceLoading = math.bignumber(runFunc(planDetail.formulas.getResidenceLoading, quotation, planInfo, planDetail));
        premAfterLsd = math.add(premAfterLsd, roundDown(math.multiply(residenceLoading, sumInsured, 0.001), 2));
        premium = premAfterLsd;
      } else {
        var lsd = -1 * runFunc(planDetail.formulas.getLsd, planDetail, sumInsured, quotation.iAge);
        var premRate = roundDown(math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge)), 2);
        var premAfterLsd = roundDown(math.multiply(math.add(lsd, premRate), sumInsured, 0.001), 2);
        premium = premAfterLsd;
      } /**for(var i = 0; i < quotation.plans.length; i++) { if (quotation.plans[i].covCode === "LITE_TPD" && quotation.plans[i].premium) { premium = math.add(quotation.plans[i].yearPrem, premium); break; } }*/ /** Add TPD Annual Premium to Basic Plan*/ /* let cQuot = Object.assign({}, quotation); let cPlanInfo = Object.assign({}, planInfo); let annPrem = 0; if (global && global.quotCache && global.quotCache.planDetails && global.quotCache.planDetails['LITE_TPD']) { var planCode = 'MBT'; var planCode2 = 'LITE_TPD'; if (cPlanInfo.premTerm.indexOf('YR') > -1) { planCode = Number.parseInt(cPlanInfo.premTerm) + planCode; } else { planCode = planCode + Number.parseInt(cPlanInfo.premTerm); } var MB_codeList = { "2": "A", "3": "B", "4": "C", "5": "D", "6": "E", "7": "F" }; var MB_code = MB_codeList[quotation.policyOptions.multiFactor]; cPlanInfo.planCode = planCode + MB_code; var tpdPlanDetail = global.quotCache.planDetails['LITE_TPD']; annPrem = runFunc(tpdPlanDetail.formulas.premFunc, cQuot, cPlanInfo, tpdPlanDetail); } premium = math.add(premium || 0, annPrem || 0);*/ /** Add TPD Annual Premium to Basic Plan*/
      if (typeof(discount) === 'number') {
        if (discount > 0) {
          premium = roundDown(math.multiply(premium, 1 - discount), 2);
        }
      }
      return math.number(premium);
    }
  } else { /** This assumes that the planInfo.yearPrem is calculated first.*/
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
        break;
      }
    }
    var temp = math.multiply(math.bignumber(planInfo.yearPrem), modalFactor);
    temp = roundDown(temp, 2);
    return math.number(temp);
  }
  return null;
}