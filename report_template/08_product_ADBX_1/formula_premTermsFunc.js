function(planDetail, quotation) {
  var premTermsList = [];
  var hasTa65 = false;
  _.each([10, 15, 20, 25], (val) => {
    if (quotation.iAge + val <= 80) {
      premTermsList.push({
        value: val + '_YR',
        title: val + ' Years'
      });
      if (quotation.iAge + val === 65) {
        hasTa65 = true;
      }
    }
  });
  if (quotation.iAge < 50 && !hasTa65) {
    premTermsList.push({
      value: '65_TA',
      title: 'To Age 65'
    });
  }
  return premTermsList;
}