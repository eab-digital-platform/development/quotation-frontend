function(quotation, planInfo, planDetails) {
  const PAYOUT_TERM_LIFETIME = 99;
  var premiumTerm = quotation.plans[0].premTerm;
  var sumAssuredNonTPD = quotation.plans[1].sumAssuredNonTPD;

  function calculateRiderPremiumWaiverUN(curPlanInfo) {
    var testAge = 50 - premiumTerm;
    var policyTerm = 0;
    var polTermDesc = '';
    if (quotation.iAge <= testAge) {
      policyTerm = premiumTerm;
      polTermDesc = policyTerm + ' Years';
    } else if ((premiumTerm < 25) && (quotation.iAge <= 45) && (quotation.iAge > testAge)) {
      policyTerm = 50;
      polTermDesc = 'To Age 50';
    } else if ((premiumTerm == 25) && (quotation.iAge > 25) && (quotation.iAge <= 40)) {
      policyTerm = 50;
      polTermDesc = 'To Age 50';
    }
    var premTerm = 0;
    if (policyTerm == 50) {
      premTerm = 50 - quotation.iAge;
    } else {
      premTerm = policyTerm;
    } /** Basic plan has premTerm but no RI or premium inputs yet*/
    if ((quotation.plans[0].premTerm) && (quotation.plans[0].premium === undefined)) {
      curPlanInfo.policyTerm = policyTerm;
      curPlanInfo.polTermDesc = polTermDesc;
      curPlanInfo.premTerm = premTerm;
      curPlanInfo.premTermDesc = premTerm + ' Years';
      curPlanInfo.premium = NaN; /** force UI to be blank*/
      return;
    } /** Basic plan has no premterm yet*/
    if ((quotation.plans[0].premTerm === undefined) || (quotation.plans[0].premium === undefined) || (quotation.plans[0].premium === NaN)) {
      if (curPlanInfo.premium) {
        curPlanInfo.premium = NaN; /** force UI to be blank*/
      }
      return;
    }
    var LEP2_UN_rates = planDetails['UNBN_RHP'].rates.LEP2_UN_rates;
    var ref = '';
    var planCode = '';
    if (quotation.iAge <= testAge) {
      if (premiumTerm == 5) {
        ref += '05';
      } else {
        ref += premiumTerm;
      }
      ref += 'UNBN';
      planCode = ref.slice(0);
      ref += quotation.iGender;
      ref += quotation.iSmoke;
    } else {
      if (policyTerm == 50) {
        ref += 'UNB50';
        planCode = ref.slice(0);
        ref += quotation.iGender;
        ref += quotation.iSmoke;
      }
    }
    var get_LEP2_UN_rate = function(ref, insuredAge) {
      var planRates = LEP2_UN_rates[ref];
      if (planRates == null) {
        return 0;
      }
      return planRates[insuredAge];
    };
    var unRate = get_LEP2_UN_rate(ref, quotation.iAge);
    var temp = Math.floor(unRate * sumAssuredNonTPD * 100) / 100; /* TRUNCATE to 2 decimal places*/
    temp = temp / 100;
    temp = Math.round(temp * 100) / 100; /* ROUND to 2 decimal places*/
    var annualPremium = temp; /** Update planInfo*/
    curPlanInfo.planCode = planCode;
    curPlanInfo.sumAssured = sumAssuredNonTPD;
    curPlanInfo.policyTerm = policyTerm;
    curPlanInfo.polTermDesc = polTermDesc;
    curPlanInfo.premTerm = premTerm;
    curPlanInfo.premTermDesc = premTerm + ' Years';
    curPlanInfo.yearPrem = annualPremium;
    curPlanInfo.halfYearPrem = Math.floor(annualPremium * 0.51 * 100) / 100;
    curPlanInfo.quarterPrem = Math.floor(annualPremium * 0.26 * 100) / 100;
    curPlanInfo.monthPrem = Math.floor(annualPremium * 0.0875 * 100) / 100;
    quotation.totYearPrem += curPlanInfo.yearPrem;
    quotation.totHalfyearPrem += curPlanInfo.halfYearPrem;
    quotation.totQuarterPrem += curPlanInfo.quarterPrem;
    quotation.totMonthPrem += curPlanInfo.monthPrem;
    switch (quotation.paymentMode) {
      case 'A':
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
      case 'S':
        {
          curPlanInfo.premium = curPlanInfo.halfYearPrem;quotation.premium += curPlanInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          curPlanInfo.premium = curPlanInfo.quarterPrem;quotation.premium += curPlanInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          curPlanInfo.premium = curPlanInfo.monthPrem;quotation.premium += curPlanInfo.monthPrem;
          break;
        }
      default:
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
    }
  }
  calculateRiderPremiumWaiverUN(planInfo);
}