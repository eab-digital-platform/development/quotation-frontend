function(quotation, planInfo, planDetail) {
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.bignumber(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  var premium = math.bignumber(runFunc(planDetail.formulas.getPremium, quotation, planInfo, planDetail));
  var payModes = planDetail.payModes;
  for (var i = 0; i < payModes.length; i++) {
    let {
      mode,
      factor,
      operator
    } = payModes[i];
    factor = math.bignumber(factor);
    if (quotation.paymentMode == mode) {
      if (operator == 'M') {
        premium = math.multiply(premium, factor);
      } else if (operator == 'D') {
        premium = math.divide(premium, factor);
      }
    }
  }
  return math.number(trunc(premium, 2));
}