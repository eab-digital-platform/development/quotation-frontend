function(planDetail, quotation) {
  var policyTermsList = [];
  var policyTerm = parseInt(quotation.plans[0].policyTerm);
  var maxpolTerm = math.min((65 - quotation.pAge), (policyTerm - 3));
  _.forEach([10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], (policyTerm) => {
    if (policyTerm < maxpolTerm) {
      policyTermsList.push({
        value: policyTerm,
        title: policyTerm + ' Years'
      });
    }
  });
  return policyTermsList;
}