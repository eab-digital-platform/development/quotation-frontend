function(quotation, planInfo, planDetail) {
  var ocls = runFunc(planDetail.formulas.getOccupationClass, quotation, planDetail);
  var rate = parseFloat(planDetail.rates.premRate.rates[ocls]);
  var scale = parseFloat(planDetail.saScale);
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    if (plan.covCode == planInfo.covCode) {
      plan.premRate = rate;
      break;
    }
  }
  var prem = math.divide(math.multiply(math.bignumber(planInfo.sumInsured), rate), scale);
  var payModes = planDetail.payModes;
  for (var i = 0; i < payModes.length; i++) {
    let {
      mode,
      factor,
      operator
    } = payModes[i];
    if (quotation.paymentMode == mode) {
      if (operator == 'M') {
        prem = math.multiply(prem, factor);
      } else if (operator == 'D') {
        prem = math.divide(prem, factor);
      }
      break;
    }
  }
  return math.number(prem);
}