function(quotation, planInfo, planDetails, extraPara) {
  var basicPlan = quotation.plans[0];
  var company = extraPara.company;
  var illust = extraPara.illustrations[planInfo.covCode];
  var retireSol = illust[0].retirementSolution;
  var accumulated = retireSol.accumulated;
  var paidOut = retireSol.paidOut;
  var YTM_H = getCurrency(paidOut.totalRateOfReturn * 100, '', 2);
  var YTM_L = getCurrency(paidOut.totalRateOfReturnLow * 100, '', 2);
  var fairValues = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE;
  var annualPremium = quotation.plans[0].yearPrem;
  var sumAssured = getCurrency(quotation.sumInsured, '', 0);
  var annualIncome = getCurrency(paidOut.incomPayout, '', 2);
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var gurAnnualRetirementIncome = paidOut.incomPayout;
  var payoutTermDesc = '';
  var paymentTermDesc = '';
  var paymentModeDesc = '';
  var premiumType = '';
  var aPremiumTotalAck = 0;
  var sPremiumTotalAck = 0;
  var qPremiumTotalAck = 0;
  var mPremiumTotalAck = 0;
  var hasOptRiders = 0;
  var hasPetRider = 0;
  var typePremium = 0;
  var deposit = 'No';
  var checkeddeposit = 'N';
  if (quotation.policyOptions.depositOption !== undefined && quotation.policyOptions.depositOption) {
    checkeddeposit = 'Y';
    deposit = 'Yes;Re-deposit for ' + quotation.policyOptions.depositInput + ' year(s)';
  }
  var positPerid = quotation.policyOptions.depositInput;
  var positPeridYear = positPerid + ' Years';
  var tempaccterm = quotation.policyOptions.accumulationPeriod;
  var arryaccterm = tempaccterm.split(' ');
  var tempaccPeriod = parseInt(arryaccterm[0]);
  var accPeriod = tempaccPeriod + ' Years';
  var payType = quotation.policyOptions.paymentMethod;
  if (payType === 'srs') {
    payType = 'SRS';
  } else {
    payType = 'Cash';
  }
  var temp = parseInt(quotation.policyOptions.payoutTerm);
  var premTrem = parseInt(quotation.premTerm);
  if (temp === 120) {
    payoutTermDesc = 120 - quotation.iAge - premTrem - tempaccPeriod;
    payoutTermDesc = payoutTermDesc + ' Years';
  } else {
    payoutTermDesc = temp + ' Years';
  } /** if (quotation.policyOptions.payoutType === 'Level'){ payoutTypeDesc = 'Level'; } else{ payoutTypeDesc = 'Inflated at 3.50% p.a.'; } */
  if (quotation.premTerm == 1) {
    paymentTermDesc = 'Single Premium';
    paymentModeDesc = 'Single Premium';
    premiumType = 'Single';
    typePremium = getCurrency(planInfo.premium, '', 2);
  } else {
    typePremium = getCurrency(annualPremium, '', 2);
    paymentTermDesc = quotation.premTerm + ' Years';
    premiumType = 'Annual';
    switch (quotation.paymentMode) {
      case 'A':
        {
          paymentModeDesc = 'Annual';
          break;
        }
      case 'S':
        {
          paymentModeDesc = 'Semi-Annual';
          break;
        }
      case 'Q':
        {
          paymentModeDesc = 'Quarterly';
          break;
        }
      case 'M':
        {
          paymentModeDesc = 'Monthly';
          break;
        }
      default:
        {
          break;
        }
    }
  }
  if (quotation.plans.length >= 2) {
    hasOptRiders = 1;
    if (quotation.plans[1].covCode === 'PET_NPE') {
      hasPetRider = 1;
    }
  }
  var plans = [];
  var plansAck = []; /** Will not include optional riders*/
  var index;
  for (index = 0; index < quotation.plans.length; index++) {
    var curPlan = quotation.plans[index];
    if (index === 0) { /** This is always the basic plan*/
      aPremiumTotalAck += curPlan.yearPrem;
      sPremiumTotalAck += curPlan.halfYearPrem;
      qPremiumTotalAck += curPlan.quarterPrem;
      mPremiumTotalAck += curPlan.monthPrem;
      plans.push({
        name: curPlan.covName.en,
        polTermDesc: curPlan.polTermDesc,
        premTermDesc: curPlan.premTermDesc,
        retireIncome: getCurrency(gurAnnualRetirementIncome, '', 2),
        aPremium: getCurrency(curPlan.yearPrem, '', 2),
        sPremium: getCurrency(curPlan.halfYearPrem, '', 2),
        qPremium: getCurrency(curPlan.quarterPrem, '', 2),
        mPremium: getCurrency(curPlan.monthPrem, '', 2),
        singlePremium: getCurrency(curPlan.singlePrem, '', 2)
      });
      plansAck.push({
        name: curPlan.covName.en,
        polTermDesc: curPlan.polTermDesc,
        premTermDesc: curPlan.premTermDesc,
        retireIncome: getCurrency(gurAnnualRetirementIncome, '', 2),
        aPremium: getCurrency(curPlan.yearPrem, '', 2),
        sPremium: getCurrency(curPlan.halfYearPrem, '', 2),
        qPremium: getCurrency(curPlan.quarterPrem, '', 2),
        mPremium: getCurrency(curPlan.monthPrem, '', 2),
        singlePremium: getCurrency(curPlan.singlePrem, '', 2)
      });
    } else {
      plans.push({
        name: curPlan.covName.en,
        polTermDesc: curPlan.polTermDesc,
        premTermDesc: curPlan.premTermDesc,
        aPremium: getCurrency(curPlan.yearPrem, '', 2),
        sPremium: getCurrency(curPlan.halfYearPrem, '', 2),
        qPremium: getCurrency(curPlan.quarterPrem, '', 2),
        mPremium: getCurrency(curPlan.monthPrem, '', 2),
        singlePremium: getCurrency(curPlan.singlePrem, '', 2)
      });
    }
  }
  var singlePremTotal = 0;
  if (quotation.premTerm == 1) {
    singlePremTotal = quotation.totSinglePrem;
  }
  var planCodes = basicPlan.planCode; /** if (hasOptRiders > 0){ planCodes += ', ' + quotation.plans[1].planCode; }*/
  if (quotation.plans.length >= 2) {
    var ridepremTrem = parseInt(quotation.plans[1].premTerm);
    var ridepolicyTerm = parseInt(quotation.plans[1].policyTerm);
    var ridePlanCode = '';
    if (quotation.plans[1].covCode === 'PET_NPE') {
      if (ridepremTrem < 10) {
        ridePlanCode = '0' + ridepremTrem + 'WOP';
      } else {
        ridePlanCode = ridepremTrem + 'WOP';
      }
    } else if (quotation.plans[1].covCode === 'UNBN_NPE') {
      if (ridepolicyTerm === 50) {
        ridePlanCode = 'UNB50';
      } else if (ridepremTrem < 10) {
        ridePlanCode = '0' + ridepremTrem + 'UNBN';
      } else {
        ridePlanCode = ridepremTrem + 'UNBN';
      }
    } else if (quotation.plans[1].covCode === 'WUN_NPE') {
      if (ridepremTrem < 10) {
        ridePlanCode = '0' + ridepremTrem + 'WUN';
      } else {
        ridePlanCode = ridepremTrem + 'WUN';
      }
    }
    planCodes += ' , ' + ridePlanCode;
  }
  planCodes += ' ';
  var reportData = {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: planCodes,
      sumAssured: quotation.sumInsured,
      releaseVersion: "1",
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
    },
    cover: {
      sameAs: quotation.sameAs,
      proposer: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke)
      },
      insured: {
        name: quotation.iFullName,
        gender: getGenderDesc(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmokingDesc(quotation.iSmoke),
        payType: payType
      },
      retireSol: {
        poPremiums: getCurrency(paidOut.totalPremiumsPaid, '', 2),
        poGRetireInc: getCurrency(paidOut.totalGuranteedRetireInc, '', 0),
        poNGRetireInc: getCurrency(paidOut.nonGuranteedRetireInc, '', 0),
        poPayout: getCurrency(paidOut.totalProjectedPayout, '', 0),
        poPayoutRatio: Math.round(paidOut.totalProjectedPayoutRatio * 100),
        poGR: getCurrency(Math.round(paidOut.guranteedRateOfReturn * 100 * 100) / 100, '', 2),
        poROR: getCurrency(Math.round(paidOut.totalRateOfReturn * 100 * 100) / 100, '', 2),
        acPremiums: getCurrency(accumulated.totalPremiumsPaid, '', 2),
        acGRetireInc: getCurrency(accumulated.totalGuranteedRetireInc, '', 0),
        acNGRetireInc: getCurrency(accumulated.nonGuranteedRetireInc, '', 0),
        acPayout: getCurrency(accumulated.totalProjectedPayout, '', 0),
        acPayoutRatio: Math.round(accumulated.totalProjectedPayoutRatio * 100),
        acROR: getCurrency(Math.round(accumulated.totalRateOfReturn * 100 * 100) / 100, '', 2)
      },
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      premTerm: quotation.premTerm,
      plans: plans,
      plansAck: plansAck
    },
    common: {
      checkeddeposit: checkeddeposit,
      deposit: deposit,
      Redeposit_Period: positPerid,
      accPeriod: accPeriod,
      premiumType: premiumType,
      sumAssured: sumAssured,
      annualIncome: annualIncome,
      typePremium: typePremium,
      retirementInc: getCurrency(gurAnnualRetirementIncome, '', 2),
      payoutTerm: payoutTermDesc,
      annualPremium: getCurrency(annualPremium, '', 2),
      paymentTerm: paymentTermDesc,
      paymentMode: paymentModeDesc,
      polCurrency: 'Singapore Dollars',
      compTel: company.compTel,
      ytm_h: YTM_H,
      ytm_l: YTM_L,
      highIRR: fairValues['HighIRR'][0],
      lowIRR: fairValues['LowIRR'][0],
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat)
    },
    basicPlan: {
      name: basicPlan.covName.en,
      sumInsured: getCurrency(basicPlan.sumInsured, '', 2),
      premium: getCurrency(basicPlan.premium, '', 2),
      aPremium: getCurrency(basicPlan.yearPrem, '', 2),
      sPremium: getCurrency(basicPlan.halfYearPrem, '', 2),
      qPremium: getCurrency(basicPlan.quarterPrem, '', 2),
      mPremium: getCurrency(basicPlan.monthPrem, '', 2),
      aPremiumTotal: getCurrency(quotation.totYearPrem, '', 2),
      sPremiumTotal: getCurrency(quotation.totHalfyearPrem, '', 2),
      qPremiumTotal: getCurrency(quotation.totQuarterPrem, '', 2),
      mPremiumTotal: getCurrency(quotation.totMonthPrem, '', 2),
      aPremiumTotalAck: getCurrency(aPremiumTotalAck, '', 2),
      sPremiumTotalAck: getCurrency(sPremiumTotalAck, '', 2),
      qPremiumTotalAck: getCurrency(qPremiumTotalAck, '', 2),
      mPremiumTotalAck: getCurrency(mPremiumTotalAck, '', 2),
      singlePremTotal: getCurrency(singlePremTotal, '', 2),
      hasOptRiders: hasOptRiders,
      hasPetRider: hasPetRider,
      paymentModeDesc: '',
      ccy: quotation.ccy,
      ccySymbol: 'SGD'
    }
  };
  return reportData;
}