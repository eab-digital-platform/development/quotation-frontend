function(quotation, planInfo, planDetails, extraPara) {
  console.log('function iLLustrateFunc');
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var planDetail = planDetails[planInfo.covCode];
  var illYrs = 99 - quotation.iAge;
  var sumInsured = math.bignumber(planInfo.sumInsured);
  var declaredRB325 = math.floor(math.multiply(sumInsured, 0.0045));
  var declaredRB475 = math.floor(math.multiply(sumInsured, 0.01));
  var compoundMult325 = math.bignumber(1.0045);
  var compoundMult475 = math.bignumber(1.01);
  var invRetMult325 = math.bignumber(1.0325);
  var invRetMult475 = math.bignumber(1.0475);
  var totalPremiumRider = math.bignumber(quotation.plans[0].yearPrem + quotation.plans[1].yearPrem + quotation.plans[2].yearPrem + quotation.plans[3].yearPrem);
  var totYearPrems = [0];
  var multiplyBenefits = [0];
  var accumRBs325 = [0];
  var accumRBs475 = [0];
  var nonGtdDBs325 = [0];
  var nonGtdDBs475 = [0];
  var totDBs325 = [0];
  var totDBs475 = [0];
  var gtdSVs = [0];
  var nonGtdSVs325 = [0];
  var nonGtdSVs475 = [0];
  var totSVs325 = [0];
  var totSVs475 = [0];
  var premVals325 = [0];
  var premVals475 = [0];
  var deductions325 = [0];
  var deductions475 = [0];
  var totComms = [0];
  var riderPremTots = [0];
  var ridercummComms = [0];
  var illustration = [];
  var lasti = 0;
  var annualPrem = 0;
  var yearPrem = 0;
  for (var j = 0; j < 4; j++) {
    var rider = quotation.plans[j];
    yearPrem = math.add(math.bignumber(rider.yearPrem), yearPrem);
  }
  annualPrem = yearPrem;
  for (var i = 1; i <= illYrs; i++) {
    var age = quotation.iAge + i; /* TODO: add premium of packaged riders */
    var ppt_yr = 0;
    if (planInfo.premTerm.indexOf('YR') == -1) {
      ppt_yr = 65 - quotation.iAge;
    } else {
      ppt_yr = Number.parseInt(planInfo.premTerm)
    }
    if (i > ppt_yr) {
      yearPrem = 0;
    }
    totYearPrems.push(math.add(totYearPrems[i - 1], yearPrem));
    var premVal325 = roundDown(math.multiply(math.add(yearPrem, premVals325[i - 1]), invRetMult325), 2);
    var premVal475 = roundDown(math.multiply(math.add(yearPrem, premVals475[i - 1]), invRetMult475), 2);
    premVals325.push(premVal325);
    premVals475.push(premVal475);
    var accumRB325 = math.floor(math.add(declaredRB325, roundDown(math.multiply(accumRBs325[i - 1], compoundMult325), 2)));
    accumRBs325.push(accumRB325);
    var accumRB475 = math.floor(math.add(declaredRB475, roundDown(math.multiply(accumRBs475[i - 1], compoundMult475), 2)));
    accumRBs475.push(accumRB475);
    var termBonus325 = math.bignumber(planDetail.rates.termBonus.categories['3.25'].DEATH[i - 1]);
    var nonGtdDB325 = roundDown(math.multiply(accumRB325, math.add(termBonus325, 1)), 2);
    nonGtdDBs325.push(nonGtdDB325);
    var termBonus475 = math.bignumber(planDetail.rates.termBonus.categories['4.75'].DEATH[i - 1]);
    var nonGtdDB475 = roundDown(math.multiply(accumRB475, math.add(termBonus475, 1)), 2);
    nonGtdDBs475.push(nonGtdDB475);
    var multiplyBenefit;
    var totDB325;
    var totDB475;
    if (age <= 70) {
      multiplyBenefit = math.bignumber(planInfo.multiplyBenefit);
      totDB325 = math.max(multiplyBenefit, math.add(sumInsured, nonGtdDB325));
      totDB475 = math.max(multiplyBenefit, math.add(sumInsured, nonGtdDB475));
    } else if (age <= 80) {
      multiplyBenefit = math.bignumber(planInfo.multiplyBenefitAfter70);
      totDB325 = math.max(multiplyBenefit, math.add(sumInsured, nonGtdDB325));
      totDB475 = math.max(multiplyBenefit, math.add(sumInsured, nonGtdDB475));
    } else {
      multiplyBenefit = math.bignumber(0);
      totDB325 = math.add(sumInsured, nonGtdDB325);
      totDB475 = math.add(sumInsured, nonGtdDB475);
    }
    multiplyBenefits.push(multiplyBenefit);
    totDBs325.push(totDB325);
    totDBs475.push(totDB475);
    var cvRateKey = 'LEPX';
    if (planInfo.premTerm.indexOf('YR') > -1) {
      cvRateKey = Number.parseInt(planInfo.premTerm) + cvRateKey;
    } else {
      cvRateKey = cvRateKey + Number.parseInt(planInfo.premTerm);
    }
    cvRateKey = cvRateKey + quotation.iGender;
    var gcv = math.bignumber(planDetail.rates.gcv[cvRateKey][quotation.iAge][i]);
    var gtdSV = roundDown(math.multiply(sumInsured, gcv, 0.001), 2);
    gtdSVs.push(gtdSV);
    var ngcv = math.bignumber(planDetail.rates.ngcv[cvRateKey][quotation.iAge][i]);
    var nonGtdSV325 = roundDown(math.multiply(roundDown(math.multiply(accumRB325, ngcv), 2), math.bignumber(math.add(planDetail.rates.termBonus.categories['3.25'].SURRENDER[i - 1], 1)), 0.001), 2);
    var nonGtdSV475 = roundDown(math.multiply(roundDown(math.multiply(accumRB475, ngcv), 2), math.bignumber(math.add(planDetail.rates.termBonus.categories['4.75'].SURRENDER[i - 1], 1)), 0.001), 2);
    nonGtdSVs325.push(nonGtdSV325);
    nonGtdSVs475.push(nonGtdSV475);
    var totSV325 = roundDown(math.add(gtdSV, nonGtdSV325), 2);
    var totSV475 = roundDown(math.add(gtdSV, nonGtdSV475), 2);
    totSVs325.push(totSV325);
    totSVs475.push(totSV475);
    var deduction325 = roundDown(math.subtract(premVal325, totSV325), 2);
    var deduction475 = roundDown(math.subtract(premVal475, totSV475), 2);
    deductions325.push(deduction325);
    deductions475.push(deduction475);
    var cum = 0;
    if (quotation.plans[0].cummComm && quotation.plans[0].cummComm[i]) {
      cum = math.bignumber(quotation.plans[0].cummComm[i]);
    }
    var totComm = roundDown(math.multiply(cum, yearPrem), 2);
    totComms.push(roundDown(math.add(totComm, totComms[i - 1]), 2));
    var riderPremTot = math.bignumber(0);
    var ridercummComm = math.bignumber(0);
    for (var j = 4; j < quotation.plans.length; j++) {
      var rider = quotation.plans[j];
      if (i <= Number.parseInt(rider.premTermYr)) {
        riderPremTot = roundDown(math.add(riderPremTot, math.bignumber(rider.yearPrem)), 2);
      }
      var cum = 0;
      if (rider.cummComm && rider.cummComm[i]) {
        cum = rider.cummComm[i];
      }
      ridercummComm = roundDown(math.add(ridercummComm, math.multiply(cum, math.bignumber(rider.yearPrem))), 2);
    }
    riderPremTots.push(math.add(riderPremTots[i - 1], riderPremTot));
    ridercummComms.push(math.add(ridercummComms[i - 1], ridercummComm));
  }
  for (var i = 1; i <= illYrs; i++) {
    illustration.push({
      annualPrem: math.number(roundDown(totYearPrems[i], 2)),
      totYearPrem: Math.floor(math.number(totYearPrems[i])),
      multiplyBenefit: Math.floor(math.number(multiplyBenefits[i])),
      gtdDB: planInfo.sumInsured,
      nonGtdDB: {
        3.25: Math.floor(math.number(nonGtdDBs325[i])),
        4.75: Math.floor(math.number(nonGtdDBs475[i]))
      },
      totDB: {
        3.25: Math.floor(math.number(totDBs325[i])),
        4.75: Math.floor(math.number(totDBs475[i]))
      },
      gtdSV: Math.floor(math.number(gtdSVs[i])),
      nonGtdSV: {
        3.25: Math.floor(math.number(nonGtdSVs325[i])),
        4.75: Math.floor(math.number(nonGtdSVs475[i]))
      },
      totSV: {
        3.25: Math.floor(math.number(totSVs325[i])),
        4.75: Math.floor(math.number(totSVs475[i]))
      },
      premVal: {
        3.25: Math.floor(math.number(premVals325[i])),
        4.75: Math.floor(math.number(premVals475[i]))
      },
      deduction: {
        3.25: Math.floor(math.number(deductions325[i])),
        4.75: Math.floor(math.number(deductions475[i]))
      },
      tdc: Math.floor(math.number(totComms[i])),
      riderPrem: Math.floor(math.number(riderPremTots[i])),
      riderComm: Math.floor(math.number(ridercummComms[i]))
    });
  }
  return illustration;
}