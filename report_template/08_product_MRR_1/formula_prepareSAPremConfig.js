function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  if (quotation.plans && quotation.plans[0].sumInsured && planDetail.inputConfig.benlim.max > (quotation.plans[0].sumInsured * 0.1)) {
    planDetail.inputConfig.benlim.max = math.multiply(quotation.plans[0].sumInsured, 0.1);
  }
}