function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  if (planDetail.gstInd == "Y") {
    planInfo.tax = {
      yearTax: math.number(math.round(math.multiply(math.bignumber(planInfo.yearPrem), planDetail.gstRate), 2)),
      halfYearTax: math.number(math.round(math.multiply(math.bignumber(planInfo.halfYearPrem), planDetail.gstRate), 2)),
      quarterTax: math.number(math.round(math.multiply(math.bignumber(planInfo.quarterPrem), planDetail.gstRate), 2)),
      monthTax: math.number(math.round(math.multiply(math.bignumber(planInfo.monthPrem), planDetail.gstRate), 2))
    };
  }
}