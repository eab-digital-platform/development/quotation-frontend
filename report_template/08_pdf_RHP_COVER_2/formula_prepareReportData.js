function(quotation, planInfo, planDetails, extraPara) {
  var basicPlan = quotation.plans[0];
  var company = extraPara.company;
  var illust = extraPara.illustrations[planInfo.covCode];
  var paidOut = illust[0].retirementSolution.paidOut;
  var YTM_H = getCurrency(paidOut.totalRateOfReturn * 100, '', 2);
  var YTM_L = getCurrency(paidOut.totalRateOfReturnLow * 100, '', 2);
  var basicTDC = getCurrency(paidOut.totalDistributionCost, '$', 0);
  var fairValues = planDetails[planInfo.covCode].rates.FAIR_BI_VARIABLE;
  var TDCPercent = paidOut.totalDistributionCost / paidOut.totalPremiumsPaid * 100;
  TDCPercent = Math.floor(TDCPercent * 100) / 100;
  var plans = [];
  var index;
  for (index = 0; index < quotation.plans.length; index++) {
    var curPlan = quotation.plans[index];
    plans.push({
      polTermDesc: curPlan.polTermDesc,
      premTermDesc: curPlan.premTermDesc,
    });
  }
  var reportData = {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      sumAssured: quotation.sumInsured,
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat)
    },
    cover: {
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      plans: plans
    },
    common: {
      polCurrency: 'Singapore Dollars',
      compTel: company.compTel,
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      ytm_h: YTM_H,
      ytm_l: YTM_L,
      basicTDC: basicTDC,
      TDCPercent: TDCPercent,
      highIRR: fairValues['HighIRR'][0],
      lowIRR: fairValues['LowIRR'][0],
      year1_ir: fairValues['Year1_IR'][0],
      year2_ir: fairValues['Year2_IR'][0],
      year3_ir: fairValues['Year3_IR'][0],
      ir_year1: fairValues['IR_Year1'][0],
      ir_year2: fairValues['IR_Year2'][0],
      ir_year3: fairValues['IR_Year3'][0],
      ir_ave3: fairValues['IR_Averaged over the last 3 years'][0],
      ir_ave5: fairValues['IR_Averaged over the last 5 years'][0],
      ir_ave10: fairValues['IR_Averaged over the last 10 years'][0],
      year1_ter: fairValues['Year1_TER'][0],
      year2_ter: fairValues['Year2_TER'][0],
      year3_ter: fairValues['Year3_TER'][0],
      ter_year1: fairValues['TER_Year1'][0],
      ter_year2: fairValues['TER_Year2'][0],
      ter_year3: fairValues['TER_Year3'][0],
      ter_ave3: fairValues['TER_Averaged over the last 3 years'][0],
      ter_ave5: fairValues['TER_Averaged over the last 5 years'][0],
      ter_ave10: fairValues['TER_Averaged over the last 10 years'][0]
    },
    basicPlan: {
      name: basicPlan.covName.en
    }
  };
  return reportData;
}