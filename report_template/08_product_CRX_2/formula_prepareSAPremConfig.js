function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var saMax = {
    SGD: 3000000,
    USD: 2097000,
    EUR: 1948000,
    GBP: 1442000,
    AUD: 2857000
  };
  var saMaxChild = {
    SGD: 500000,
    USD: 349000,
    EUR: 324000,
    GBP: 240000,
    AUD: 476000
  };
  var bpSa = quotation.plans[0].sumInsured;
  var saMax = (quotation.iAge >= 16 ? saMax : saMaxChild)[quotation.ccy];
  planDetail.inputConfig.benlim = {
    max: Math.min(bpSa || saMax, saMax),
    min: 20000
  };
}