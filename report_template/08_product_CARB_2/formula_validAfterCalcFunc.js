function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  var targetAge = (99 - quotation.iAge);
  planInfo.policyTerm = "yrs" + targetAge;
  planInfo.polTermDesc = targetAge + " Years";
  planInfo.premTerm = planInfo.policyTerm;
  planInfo.premTermDesc = targetAge + " Years";
  planInfo.policyTermYr = targetAge;
  planInfo.premTermYr = targetAge;
  planInfo.premium = 0;
  planInfo.planCode = 'CARB';
}