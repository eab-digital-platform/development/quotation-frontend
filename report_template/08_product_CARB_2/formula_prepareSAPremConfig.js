function(planDetail, quotation, planDetails) {
  planDetail.inputConfig.canViewSumAssured = true;
  planDetail.inputConfig.canViewPremium = false;
  for (var i in quotation.plans) {
    if (quotation.plans[i].covCode === planDetail.covCode) {
      quotation.plans[i].sumInsured = quotation.plans[0].sumInsured;
    }
  }
}