function(quotation, planInfo, planDetail, age) {
  var rateKey;
  if (quotation.policyOptions.planType === 'renew') {
    rateKey = 'FPR_' + (planInfo.premTerm === 'SP' ? 'SP' : 'RP');
  } else {
    rateKey = 'FPNR_' + (planInfo.premTerm.endsWith('_YR') ? Number.parseInt(planInfo.premTerm) + 'P' : 'RP');
  }
  rateKey += '_' + quotation.iGender;
  rateKey += quotation.iSmoke === 'Y' ? 'S' : 'NS';
  var rates = planDetail.rates.premRate[rateKey];
  return rates[Number.parseInt(planInfo.policyTerm)][age] || 0;
}