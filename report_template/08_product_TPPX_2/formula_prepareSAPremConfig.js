function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var premMin = {
    A: 200,
    S: 100,
    Q: 75,
    M: 30
  };
  if (quotation.paymentMode && quotation.paymentMode !== 'L') {
    planDetail.inputConfig.premlim = {
      min: premMin[quotation.paymentMode]
    };
  }
}