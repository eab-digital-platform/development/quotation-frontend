function(planDetail, quotation, planDetails) {
  var policyOptions = [];
  var spliceFunc = function(po, ids) {
    for (var h = 0; h < ids.length; h++) {
      var id = ids[h];
      for (var i in po.options) {
        var opt = po.options[i];
        if (opt.value == id) {
          po.options.splice(i, 1);
          break;
        }
      }
    }
  };
  for (var p in planDetail.policyOptions) {
    var po = JSON.parse(JSON.stringify(planDetail.policyOptions[p]));
    if (po.type == 'datepicker' || (po.type == 'text' && (po.subType == 'currency' || po.subType == 'number'))) {
      if (po.max || po.max === 0) {
        po.max = parseInt(po.max, 10);
      }
      if (po.min || po.min === 0) {
        po.min = parseInt(po.min, 10);
      }
    }
    if (po.type == 'picker' && po.options) {
      if (!quotation.policyOptions[po.id] && po.value) {
        for (var i in po.options) {
          var opt = po.options[i];
          if (opt.value == po.value) {
            quotation.policyOptions[po.id] = po.value;
            break;
          }
        }
      }
      if (!quotation.policyOptions[po.id] && po.options.length) {
        if (po.value == '999') {
          quotation.policyOptions[po.id] = po.options[po.options.length - 1].value;
        } else {
          quotation.policyOptions[po.id] = null;
        }
      }
    } else {
      if (!quotation.policyOptions[po.id] && (po.value || po.value === 0 || po.value === '')) {
        if (po.type == 'text' && (po.subType == 'currency' || po.subType == 'number')) {
          quotation.policyOptions[po.id] = parseFloat(po.value);
        } else {
          quotation.policyOptions[po.id] = po.value;
        }
      }
    }
    var options = po.options;
    var id = po.id;
    var dealerGroup = quotation.agent ? quotation.agent.dealerGroup : null;
    var paymentMethod = quotation.policyOptions.paymentMethod;
    var quotVal = quotation.policyOptions[id];
    if (id === 'paymentMethod') {
      if (quotation.sameAs === 'Y') {
        if (21 > quotation.iAge || quotation.iAge > 58) {
          var ids = ["cpfisoa", "cpfissa", "srs"];
          spliceFunc(po, ids);
        }
      } else {
        quotation.policyOptions[id] = 'cash';
        po.options = _.filter(po.options, opt => opt.value === 'cash');
      }
      var alertMsg = "The CPF prevailing interest rates are: CPF OA: 2.5% (for balances above the non-investible $20,000), CPF SA: 4% (for balances above the non-investible $40,000).\n \nIt is a requirement to complete the Self-Awareness Questionnaire (SAQ) before any investment could be done using the CPFIS. \nIf SAQ is not done, please login via CPF website with your SingPass to complete the Self-Awareness Questionnaire (SAQ) before proceeding.";
      po.alertOnValue = {
        cpfissa: alertMsg,
        cpfisoa: alertMsg
      };
      po.clearFunds = "Y";
    }
    if (id === 'singlePremSalesCharge') {
      if (dealerGroup === 'SINGPOST' || dealerGroup === 'DIRECT') {
        quotation.policyOptions[id] = '3';
        spliceFunc(po, ["0", "1", "2", "4", "5"]);
      }
    } else if (id === 'serviceFee') {
      if (dealerGroup === 'AGENCY' || dealerGroup === 'BROKER' || dealerGroup === 'FUSION' || dealerGroup === 'SYNERGY') {
        if (paymentMethod == 'cpfisoa' || paymentMethod == 'cpfissa') {
          if (Number(quotVal) > 0) quotation.policyOptions[id] = null;
          spliceFunc(po, ["0.25", "0.50", "0.75"]);
        }
      } else if (dealerGroup === 'SINGPOST' || dealerGroup === 'DIRECT') {
        quotation.policyOptions[id] = '0';
        spliceFunc(po, ["0.25", "0.50", "0.75"]);
      }
    } else if (id === 'rspSalesCharges') {
      quotation.policyOptions[id] = quotation.policyOptions.singlePremSalesCharge ? quotation.policyOptions.singlePremSalesCharge + '%' : '-';
    } else if (id === 'rspSelect') {
      if ((quotation.iAge <= 60 && paymentMethod === 'cash') || (quotation.iAge <= 58 && (paymentMethod === 'cpfisoa' || paymentMethod === 'cpfissa' || paymentMethod === 'srs'))) {
        po.disable = "N";
      }
    }
    if (id === 'rspAmount' || id === 'rspPayFreq') {
      var isCheck = quotation.policyOptions.rspSelect;
      po.disable = isCheck ? "N" : "Y";
      po.mandatory = isCheck ? "Y" : "N";
      if (!isCheck) quotation.policyOptions[id] = null;
    }
    if (id === 'rspAmount') {
      var min = 0;
      var rspPayFreq = quotation.policyOptions.rspPayFreq;
      if (rspPayFreq === 'annual') min = 1200;
      else if (rspPayFreq === 'semiAnnual') min = 600;
      else if (rspPayFreq === 'quarterly') min = 300;
      po.hintMsg = 'Minimum RSP Amount is $' + min;
      if (po.mandatory === "Y" && quotation.policyOptions[id] !== null && !isNaN(quotation.policyOptions[id])) {
        var rspAmt = math.number(quotation.policyOptions[id]);
        var factor = math.number(po.factor ? po.factor : 1);
        var errorMsg = null;
        if (rspAmt % factor !== 0) quotation.policyOptions[id] = math.number(math.multiply(math.floor(math.divide(math.bignumber(rspAmt), factor)), factor));
        if (rspAmt < min) quotation.policyOptions[id] = math.number(min);
      }
    }
    policyOptions.push(po);
  }
  planDetail.inputConfig.policyOptions = policyOptions;
  return planDetail;
}