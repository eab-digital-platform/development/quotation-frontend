function(planDetail, quotation, planDetails) {
  let planInfo = quotation.plans && quotation.plans.find(p => p.covCode === planDetail.covCode);
  let polTermList = quotDriver.runFunc(planDetail.formulas.polTermsFunc, planDetail, quotation);
  if (!planInfo.policyTerm && quotation.plans[0].policyTerm) {
    planInfo.policyTerm = quotation.plans[0].policyTerm;
  }
  if (polTermList.length === 1) {
    planInfo.policyTerm = polTermList[0].value;
  } else if (planInfo && !polTermList.find((opt) => opt.value === planInfo.policyTerm)) {
    planInfo.policyTerm = null;
  }
  planDetail.inputConfig.canEditPolicyTerm = !!quotation.plans[0].policyTerm;
  planDetail.inputConfig.policyTermList = polTermList;
  let premTermList = quotDriver.runFunc(planDetail.formulas.premTermsFunc, planDetail, quotation);
  if (premTermList.length === 1) {
    planInfo.premTerm = premTermList[0].value;
  } else if (planInfo && !premTermList.find((opt) => opt.value === planInfo.premTerm)) {
    planInfo.premTerm = null;
  }
  planDetail.inputConfig.canEditPremTerm = false;
  planDetail.inputConfig.premTermList = premTermList;
}