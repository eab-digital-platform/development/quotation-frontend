function(quotation, planInfo, planDetail, age) {
  var rateKey = 'GS' + Number.parseInt(planInfo.policyTerm) + 'S' + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS');
  return planDetail.rates.premRate[rateKey][age];
}