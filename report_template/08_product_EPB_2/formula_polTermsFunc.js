function(planDetail, quotation) {
  var iAge = quotation.iAge;
  return [{
    value: "yrs" + (55 - iAge),
    title: (55 - iAge) + " Years",
    default: true
  }];
}