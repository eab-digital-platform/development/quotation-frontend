function(ccy) {
  var sign = '$';
  if (ccy === 'SGD') {
    sign = 'S$';
  } else if (ccy === 'USD') {
    sign = 'US$';
  } else if (ccy === 'GBP') {
    sign = '£';
  } else if (ccy === 'EUR') {
    sign = '€';
  } else if (ccy === 'AUD') {
    sign = 'A$';
  } else if (ccy === 'JPY') {
    sign = '¥';
  } else if (ccy === 'CHF') {
    sign = 'CHF';
  }
  return sign;
}