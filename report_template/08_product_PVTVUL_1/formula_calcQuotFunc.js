function(quotation, planInfo, planDetails) {
  const CALC_TYPE_INPUT_PREMIUM = 1;
  const CALC_TYPE_INPUT_SA = 2;
  var planDetail = planDetails[planInfo.covCode];
  var grossRate = Number(quotation.policyOptions.grossInvRate);
  var charge = Number(quotation.policyOptions.charge);
  if (isNaN(grossRate)) {
    grossRate = 0;
  }
  if (isNaN(charge)) {
    charge = 0;
  }
  var irr = (grossRate - charge) / 100;
  var ccySign = runFunc(planDetail.formulas.getCurrencySign, quotation.ccy);
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };

  function calculateBasicPlan() {
    var checkIfSameInputs = function() {
      if (Number(quotation.prevPremium) !== Number(planInfo.premium)) {
        return false;
      }
      if (Number(quotation.prevSumInsured) !== Number(planInfo.sumInsured)) {
        return false;
      }
      return true;
    };
    var calculatePremiumFromSA = function(sa) {
      var calcAge100AV = function(quotation, planInfo, planDetail) {
        var age100AV = 0;
        age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
        return age100AV;
      };
      planInfo["premium"] = 1000000;
      var oParams = {
        Func: calcAge100AV,
        aFuncParams: [quotation, planInfo, planDetail],
        oFuncArgTarget: {
          Position: 1,
          propStr: "premium"
        },
        Goal: sa,
        Tol: 0.01,
        maxIter: 1000
      };
      var premium = runFunc(planDetail.formulas.goalSeek, oParams);
      premium = roundDown(premium, -2) + 100;
      return math.number(premium);
    };
    var calculateSAFromPremium = function(premium) {
      var sa = 0;
      var age100AV = 0;
      sa = math.multiply(premium, math.bignumber(1.5));
      sa = math.number(roundDown(sa, -3));
      planInfo.sumInsured = sa;
      age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
      age100AV = age100AV < 0 ? 0 : age100AV;
      if (premium > 1000000000) {
        while (age100AV > sa) {
          sa = sa + 10000000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
        while (age100AV < sa) {
          sa = sa - 5000000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
      }
      if (premium > 500000000) {
        while (age100AV > sa) {
          sa = sa + 2000000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
        while (age100AV < sa) {
          sa = sa - 1000000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
      }
      if (premium > 100000000) {
        while (age100AV > sa) {
          sa = sa + 500000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
        while (age100AV < sa) {
          sa = sa - 300000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
      }
      if (premium > 40000000 || (premium > 10000000 && (grossRate - charge) >= 4)) {
        while (age100AV > sa) {
          sa = sa + 100000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
        while (age100AV < sa) {
          sa = sa - 50000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
      }
      if (premium > 5000000) {
        while (age100AV > sa) {
          sa = sa + 5000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
        while (age100AV < sa) {
          sa = sa - 2000000;
          planInfo.sumInsured = sa;
          age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
          age100AV = age100AV < 0 ? 0 : age100AV;
        }
      }
      while (age100AV > sa) {
        sa = sa + 1000000;
        planInfo.sumInsured = sa;
        age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
        age100AV = age100AV < 0 ? 0 : age100AV;
      }
      while (age100AV < sa) {
        sa = sa - 100000;
        planInfo.sumInsured = sa;
        age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
        age100AV = age100AV < 0 ? 0 : age100AV;
      }
      while (age100AV > sa) {
        sa = sa + 10000;
        planInfo.sumInsured = sa;
        age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
        age100AV = age100AV < 0 ? 0 : age100AV;
      }
      while (age100AV < sa) {
        sa = sa - 1000;
        planInfo.sumInsured = sa;
        age100AV = runFunc(planDetail.formulas.getAge100AV, quotation, planInfo, planDetail, irr);
        age100AV = age100AV < 0 ? 0 : age100AV;
      }
      return math.number(sa);
    };
    var calcType = 0;
    var premium_1 = planInfo.premium;
    var premium_2 = quotation.prevPremium;
    var sa_1 = planInfo.sumInsured;
    var sa_2 = quotation.prevSumInsured;
    if ((premium_1 === undefined) && (sa_1 === undefined)) {
      return;
    }
    if (premium_1 === undefined) {
      calcType = CALC_TYPE_INPUT_SA;
    }
    if (sa_1 === undefined) {
      calcType = CALC_TYPE_INPUT_PREMIUM;
    }
    if (sa_1 === null && premium_1 === premium_2) {
      quotation.prevPremium = null;
      quotation.prevSumInsured = null;
      planInfo.premium = null;
      quotation.lessMinSA = 'N';
      return;
    }
    if (premium_1 === null && sa_1 === sa_2) {
      quotation.prevPremium = null;
      quotation.prevSumInsured = null;
      planInfo.sumInsured = null;
      quotation.lessMinSA = 'N';
      return;
    }
    if (sa_1 === 0 && (Number(premium_1) === 0)) {
      quotation.prevPremium = 0;
      quotation.prevSumInsured = 0;
      planInfo.premium = sa_1;
      quotation.lessMinSA = 'N';
      return;
    }
    if (premium_1 === 0 && (Number(sa_1) === 0)) {
      quotation.prevPremium = 0;
      quotation.prevSumInsured = 0;
      planInfo.sumInsured = premium_1;
      quotation.lessMinSA = 'N';
      return;
    }
    if ((Number(premium_1) === 0) && (Number(premium_2) !== 0)) {
      quotation.prevPremium = 0;
      quotation.prevSumInsured = 0;
      planInfo.sumInsured = premium_1;
      quotation.lessMinSA = 'N';
      return;
    }
    if ((Number(sa_1) === 0) && (Number(sa_2) !== 0)) {
      quotation.prevPremium = 0;
      quotation.prevSumInsured = 0;
      planInfo.premium = sa_1;
      quotation.lessMinSA = 'N';
      return;
    }
    if (premium_1 === undefined) {
      premium_1 = 0;
    }
    if (premium_2 === undefined) {
      premium_2 = 0;
    }
    if (sa_1 === undefined) {
      sa_1 = 0;
    }
    if (sa_2 === undefined) {
      sa_2 = 0;
    }
    if (checkIfSameInputs()) {
      calcType = quotation.prevCalcType;
    } else {
      if (calcType === 0) {
        if (quotation.prevCalcType === CALC_TYPE_INPUT_PREMIUM) {
          if (Number(premium_1) === Number(premium_2)) {
            if (Number(sa_1) !== Number(sa_2)) {
              calcType = CALC_TYPE_INPUT_SA;
            } else {
              calcType = CALC_TYPE_INPUT_PREMIUM;
            }
          } else {
            calcType = CALC_TYPE_INPUT_PREMIUM;
          }
        } else if (quotation.prevCalcType === CALC_TYPE_INPUT_SA) {
          if (Number(premium_1) === Number(premium_2)) {
            if (Number(sa_1) !== Number(sa_2)) {
              calcType = CALC_TYPE_INPUT_SA;
            } else {
              calcType = CALC_TYPE_INPUT_PREMIUM;
            }
          } else {
            calcType = CALC_TYPE_INPUT_PREMIUM;
          }
        }
      }
    }
    if (calcType == CALC_TYPE_INPUT_PREMIUM) {
      quotation.prevCalcType = CALC_TYPE_INPUT_PREMIUM;
      var inputPremium = planInfo.premium;
      var minSA = inputPremium * 1.5;
      var calc_SA = calculateSAFromPremium(inputPremium);
      quotation.prevPremium = inputPremium;
      quotation.prevSumInsured = calc_SA;
      planInfo.sumInsured = calc_SA;
      planInfo.singlePrem = inputPremium;
      planInfo.yearPrem = inputPremium;
      quotation.totSinglePrem = inputPremium;
      quotation.totYearPrem = inputPremium;
      quotation.annualPremium = inputPremium;
      quotation.sumInsured = calc_SA;
      quotation.premium = inputPremium;
      if (calc_SA < minSA) {
        quotation.lessMinSA = 'Y';
        quotDriver.context.addError({
          covCode: quotation.baseProductCode,
          msg: 'Minimum Sum Assured is ' + getCurrency(minSA, ccySign, 0) + '.'
        });
      } else {
        quotation.lessMinSA = 'N';
      }
    } else {
      quotation.prevCalcType = CALC_TYPE_INPUT_SA;
      var inputSA = planInfo.sumInsured;
      var calc_Premium = calculatePremiumFromSA(inputSA);
      var minSA = calc_Premium * 1.5;
      if (calc_Premium < quotation.minPrem) {
        minSA = quotation.minPrem * 1.5;
      }
      quotation.prevPremium = calc_Premium;
      quotation.prevSumInsured = inputSA;
      planInfo.premium = calc_Premium;
      planInfo.yearPrem = calc_Premium;
      planInfo.sumInsured = inputSA;
      planInfo.singlePrem = calc_Premium;
      quotation.totSinglePrem = calc_Premium;
      quotation.totYearPrem = calc_Premium;
      quotation.annualPremium = calc_Premium;
      quotation.sumInsured = inputSA;
      quotation.premium = calc_Premium;
      if (calc_Premium < quotation.minPrem) {
        quotation.lessMinSA = 'Y';
        quotDriver.context.addError({
          covCode: quotation.baseProductCode,
          msg: 'Minimum Premium is ' + getCurrency(quotation.minPrem, ccySign, 0) + '.'
        });
      } else if (inputSA < minSA) {
        quotation.lessMinSA = 'Y';
        quotDriver.context.addError({
          covCode: quotation.baseProductCode,
          msg: 'Minimum Sum Assured is ' + getCurrency(minSA, ccySign, 0) + '.'
        });
      } else {
        quotation.lessMinSA = 'N';
      }
    }
    planInfo.planCode = 'PVTVUL';
  }
  calculateBasicPlan();
  planInfo.noApplication = true;
  planInfo.noApplicationMsg = "Please print the generated PI and proceed with paper submission.";
  planInfo.hasTitle = "Warning";
}