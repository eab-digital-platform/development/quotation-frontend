function(quotation, planInfo, planDetail, in_IRR) {
  var rates = planDetail.rates;
  var premium = planInfo.premium;
  var sumSa = planInfo.sumInsured;
  var policytype = quotation.policyOptions.policytype;
  var riskClass = quotation.policyOptions.riskClassification;
  var riskClass2 = quotation.policyOptions.riskClassification2;
  var la1_em_perc = Number(quotation.policyOptions.extraMortality);
  var la1_pml = Number(quotation.policyOptions.loading);
  var pml_period = Number(quotation.policyOptions.loadingPeriod);
  var la1_em_perc2 = Number(quotation.policyOptions.extraMortality2);
  var la1_pml2 = Number(quotation.policyOptions.loading2);
  var pml_period2 = Number(quotation.policyOptions.loadingPeriod2);
  if (isNaN(la1_em_perc)) {
    la1_em_perc = 0;
  }
  if (isNaN(la1_pml)) {
    la1_pml = 0;
  }
  if (isNaN(pml_period)) {
    pml_period = 0;
  }
  if (isNaN(la1_em_perc2)) {
    la1_em_perc2 = 0;
  }
  if (isNaN(la1_pml2)) {
    la1_pml2 = 0;
  }
  if (isNaN(pml_period2)) {
    pml_period2 = 0;
  }
  var coiRate = function(rates, countryClass, age, riskClass, smoke, gender) {
    var colnum = 1;
    if (gender + smoke === 'MN') {
      colnum = 1;
    } else if (gender + smoke === 'MY') {
      colnum = 2;
    } else if (gender + smoke === 'FN') {
      colnum = 3;
    } else if (gender + smoke === 'FY') {
      colnum = 4;
    }
    if (riskClass === 'P') {
      colnum += 4;
    } else if (riskClass === 'S') {
      colnum += 8;
    } else if (riskClass === 'SS') {
      colnum += 12;
    }
    age = math.min(120, age);
    return rates.COI[countryClass]['' + age][colnum - 1];
  };
  var polcyYear = function(month) {
    return math.floor((month - 1) / 12) + 1;
  };
  var getPrevTpRate = function(month, rawData, type) {
    if (type === 'X') {
      return rawData[month - 12].TPX;
    } else if (type === 'Y') {
      return rawData[month - 12].TPY;
    } else if (type === 'XY') {
      return rawData[month - 12].TPXY;
    }
  };
  var calcIllustration = function(month, in_IRR) {
    var age100av = 0;
    var pfd_rate = rates.fee['PolicyFeeDuration'];
    var ef_isp_rate = rates.fee['EF_ISP'];
    var fpisp_rate = rates.fee['PolicyFee'];
    var oafisp_rate = rates.fee['OAF_ISP'];
    var iCountryClass = rates.countryClass[quotation.iResidence];
    var pCountryClass = null;
    if (policytype === 'joint') {
      pCountryClass = rates.countryClass[quotation.pResidence];
    }
    var rawData = [];
    var defaultRawData = {
      plYear: 0,
      plMonth: 0,
      iAge: 0,
      pAge: 0,
      allocated_Premium_BOM: math.bignumber(0),
      Initial_Premium: math.bignumber(0),
      elm_Fee: math.bignumber(0),
      account_Value_EOM: math.bignumber(0),
      account_Value_BOM: math.bignumber(0),
      sum_Risk_BOM: math.bignumber(0),
      account_Value_WD: math.bignumber(0),
      policy_Fee: math.bignumber(0),
      Trailer_Fee: math.bignumber(0),
      Fund_Return: math.bignumber(0),
      Ongoing_Fee: math.bignumber(0),
      Cost_Insurance: math.bignumber(0),
      TPX: math.bignumber(0),
      TPY: math.bignumber(0),
      TPXY: math.bignumber(0),
      QXYT_1: math.bignumber(0)
    };
    rawData.push(Object.assign({}, defaultRawData));
    for (var m = 1; m <= month; m++) {
      var prevMonth = rawData[m - 1];
      var currMonth = Object.assign({}, defaultRawData);
      currMonth.plYear = polcyYear(m);
      currMonth.plMonth = m;
      currMonth.iAge = quotation.iAge + currMonth.plYear - 1;
      currMonth.pAge = quotation.pAge + currMonth.plYear - 1;
      var coi_rate = 0;
      if (policytype === 'single') {
        coi_rate = coiRate(rates, iCountryClass, currMonth.iAge, riskClass, quotation.iSmoke, quotation.iGender);
        coi_rate = math.multiply(math.bignumber(coi_rate), math.add(math.bignumber(1), math.bignumber(la1_em_perc / 100)));
        if (currMonth.iAge < quotation.iAge + pml_period) {
          coi_rate = math.add(coi_rate, math.bignumber(la1_pml));
        }
        coi_rate = math.min(coi_rate, 1000);
      } else {
        var coi_rate_x = coiRate(rates, iCountryClass, currMonth.iAge, riskClass, quotation.iSmoke, quotation.iGender);
        coi_rate_x = math.multiply(math.bignumber(coi_rate_x), math.add(math.bignumber(1), math.bignumber(la1_em_perc / 100)));
        if (currMonth.iAge < quotation.iAge + pml_period) {
          coi_rate_x = math.add(coi_rate_x, math.bignumber(la1_pml));
        }
        coi_rate_x = math.min(coi_rate_x, 1000);
        var coi_rate_Y = coiRate(rates, pCountryClass, currMonth.pAge, riskClass2, quotation.pSmoke, quotation.pGender);
        coi_rate_Y = math.multiply(math.bignumber(coi_rate_Y), math.add(math.bignumber(1), math.bignumber(la1_em_perc2 / 100)));
        if (currMonth.pAge < quotation.pAge + pml_period2) {
          coi_rate_Y = math.add(coi_rate_Y, math.bignumber(la1_pml2));
        }
        coi_rate_Y = math.min(coi_rate_Y, 1000);
        var prevTpx = math.bignumber(1);
        var prevTpy = math.bignumber(1);
        var prevTpxy = math.bignumber(1);
        if (currMonth.plYear !== 1) {
          prevTpx = getPrevTpRate(m, rawData, 'X');
          prevTpy = getPrevTpRate(m, rawData, 'Y');
          prevTpxy = getPrevTpRate(m, rawData, 'XY');
        }
        currMonth.TPX = math.multiply(math.subtract(math.bignumber(1), math.divide(math.bignumber(coi_rate_x), math.bignumber(1000))), prevTpx);
        currMonth.TPY = math.multiply(math.subtract(math.bignumber(1), math.divide(math.bignumber(coi_rate_Y), math.bignumber(1000))), prevTpy);
        currMonth.TPXY = math.subtract(math.add(currMonth.TPX, currMonth.TPY), math.multiply(currMonth.TPX, currMonth.TPY));
        currMonth.QXYT_1 = math.multiply(math.bignumber(1000), math.subtract(math.bignumber(1), math.divide(currMonth.TPXY, prevTpxy)));
        coi_rate = math.max(currMonth.QXYT_1, math.bignumber(0.18));
      }
      if (m === 1) {
        currMonth.Initial_Premium = math.bignumber(premium);
      }
      currMonth.elm_Fee = math.multiply(currMonth.Initial_Premium, math.bignumber(ef_isp_rate));
      currMonth.allocated_Premium_BOM = math.subtract(currMonth.Initial_Premium, currMonth.elm_Fee);
      currMonth.account_Value_BOM = math.add(currMonth.allocated_Premium_BOM, prevMonth.account_Value_EOM);
      currMonth.sum_Risk_BOM = math.max(0, math.subtract(math.bignumber(sumSa), currMonth.account_Value_BOM));
      if (currMonth.plYear <= pfd_rate) {
        if (math.mod((m - 1), 3) === 0) {
          currMonth.policy_Fee = math.divide(math.multiply(math.bignumber(premium), math.bignumber(fpisp_rate)), math.bignumber(4));
        }
      }
      if (math.mod((m - 1), 3) === 0) {
        currMonth.Ongoing_Fee = math.divide(math.multiply(math.bignumber(premium), math.bignumber(oafisp_rate)), math.bignumber(4));
      }
      if (math.mod((m - 1), 3) === 0) {
        currMonth.Cost_Insurance = math.multiply(math.divide(math.divide(math.bignumber(coi_rate), math.bignumber(1000)), math.bignumber(4)), currMonth.sum_Risk_BOM);
      }
      currMonth.Fund_Return = math.multiply(math.subtract(currMonth.account_Value_BOM, math.add(math.add(math.add(currMonth.policy_Fee, currMonth.Ongoing_Fee), currMonth.Cost_Insurance), currMonth.Trailer_Fee)), math.subtract(math.pow(math.add(math.bignumber(1), math.bignumber(in_IRR)), math.divide(math.bignumber(1), math.bignumber(12))), math.bignumber(1)));
      currMonth.account_Value_EOM = math.add(math.subtract(currMonth.account_Value_BOM, math.add(math.add(math.add(currMonth.policy_Fee, currMonth.Ongoing_Fee), currMonth.Cost_Insurance), currMonth.Trailer_Fee)), currMonth.Fund_Return);
      if (m === month) {
        age100av = Number(currMonth.account_Value_EOM);
      }
      rawData.push(currMonth);
    }
    return age100av;
  };
  var age100 = 100 - quotation.iAge;
  if (policytype === 'joint') {
    var minLaAge = math.min(quotation.pAge, quotation.iAge);
    age100 = 100 - minLaAge;
  }
  var month = age100 * 12;
  var age100AccValue = calcIllustration(month, in_IRR);
  return age100AccValue;
}