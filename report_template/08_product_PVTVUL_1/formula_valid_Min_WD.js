function(quotation, planInfo, planDetail) {
  var rates = planDetail.rates;
  var premium = planInfo.premium;
  var sumSa = planInfo.sumInsured;
  var ccy = quotation.ccy;
  var policytype = quotation.policyOptions.policytype;
  var riskClass = quotation.policyOptions.riskClassification;
  var riskClass2 = quotation.policyOptions.riskClassification2;
  var la1_em_perc = Number(quotation.policyOptions.extraMortality);
  var la1_pml = Number(quotation.policyOptions.loading);
  var pml_period = Number(quotation.policyOptions.loadingPeriod);
  var la1_em_perc2 = Number(quotation.policyOptions.extraMortality2);
  var la1_pml2 = Number(quotation.policyOptions.loading2);
  var pml_period2 = Number(quotation.policyOptions.loadingPeriod2);
  var in_IRR = Number(quotation.policyOptions.invRateForWD);
  var startYr1 = Number(quotation.policyOptions.startYr1);
  var startYr2 = Number(quotation.policyOptions.startYr2);
  var startYr3 = Number(quotation.policyOptions.startYr3);
  var startYr4 = Number(quotation.policyOptions.startYr4);
  var startYr5 = Number(quotation.policyOptions.startYr5);
  var endYr1 = Number(quotation.policyOptions.endYr1);
  var endYr2 = Number(quotation.policyOptions.endYr2);
  var endYr3 = Number(quotation.policyOptions.endYr3);
  var endYr4 = Number(quotation.policyOptions.endYr4);
  var endYr5 = Number(quotation.policyOptions.endYr5);
  var wdPer1 = Number(quotation.policyOptions.wdPer1);
  var wdPer2 = Number(quotation.policyOptions.wdPer2);
  var wdPer3 = Number(quotation.policyOptions.wdPer3);
  var wdPer4 = Number(quotation.policyOptions.wdPer4);
  var wdPer5 = Number(quotation.policyOptions.wdPer5);
  var ccySign = runFunc(planDetail.formulas.getCurrencySign, quotation.ccy);
  var minWD = 0;
  if (ccy === 'USD') {
    minWD = 20000;
  } else if (ccy === 'SGD') {
    minWD = 28000;
  } else if (ccy === 'CHF') {
    minWD = 20000;
  } else if (ccy === 'EUR') {
    minWD = 18000;
  } else if (ccy === 'GBP') {
    minWD = 16000;
  } else if (ccy === 'JPY') {
    minWD = 2200000;
  } else if (ccy === 'AUD') {
    minWD = 26000;
  }
  if (isNaN(la1_em_perc)) {
    la1_em_perc = 0;
  }
  if (isNaN(la1_pml)) {
    la1_pml = 0;
  }
  if (isNaN(pml_period)) {
    pml_period = 0;
  }
  if (isNaN(la1_em_perc2)) {
    la1_em_perc2 = 0;
  }
  if (isNaN(la1_pml2)) {
    la1_pml2 = 0;
  }
  if (isNaN(pml_period2)) {
    pml_period2 = 0;
  }
  if (isNaN(in_IRR)) {
    in_IRR = 0;
  }
  if (isNaN(startYr1)) {
    startYr1 = 0;
  }
  if (isNaN(startYr2)) {
    startYr2 = 0;
  }
  if (isNaN(startYr3)) {
    startYr3 = 0;
  }
  if (isNaN(startYr4)) {
    startYr4 = 0;
  }
  if (isNaN(startYr5)) {
    startYr5 = 0;
  }
  if (isNaN(endYr1)) {
    endYr1 = 0;
  }
  if (isNaN(endYr2)) {
    endYr2 = 0;
  }
  if (isNaN(endYr3)) {
    endYr3 = 0;
  }
  if (isNaN(endYr4)) {
    endYr4 = 0;
  }
  if (isNaN(endYr5)) {
    endYr5 = 0;
  }
  if (isNaN(wdPer1)) {
    wdPer1 = 0;
  }
  if (isNaN(wdPer2)) {
    wdPer2 = 0;
  }
  if (isNaN(wdPer3)) {
    wdPer3 = 0;
  }
  if (isNaN(wdPer4)) {
    wdPer4 = 0;
  }
  if (isNaN(wdPer5)) {
    wdPer5 = 0;
  }
  var sumwithDraw = function(currentYear) {
    var sumwd = 0;
    if (currentYear >= startYr1 && currentYear <= endYr1) {
      sumwd += (wdPer1 / 100);
    }
    if (currentYear >= startYr2 && currentYear <= endYr2) {
      sumwd += (wdPer2 / 100);
    }
    if (currentYear >= startYr3 && currentYear <= endYr3) {
      sumwd += (wdPer3 / 100);
    }
    if (currentYear >= startYr4 && currentYear <= endYr4) {
      sumwd += (wdPer4 / 100);
    }
    if (currentYear >= startYr5 && currentYear <= endYr5) {
      sumwd += (wdPer5 / 100);
    }
    return sumwd;
  };
  var getsumCol11M = function(currMonth, rawData, colName) {
    var accColM = math.bignumber(0);
    for (var i = 1; i <= 11; i++) {
      var tempRaw = rawData[currMonth - i];
      var tempval = 0;
      if (colName === 'M') {
        tempval = tempRaw.WD_Amount;
      } else if (colName === 'J') {
        tempval = tempRaw.elm_Fee;
      } else if (colName === 'R') {
        tempval = tempRaw.policy_Fee;
      } else if (colName === 'S') {
        tempval = tempRaw.Ongoing_Fee;
      } else if (colName === 'T') {
        tempval = tempRaw.Cost_Insurance;
      }
      accColM += tempval;
    }
    return accColM;
  };
  var coiRate = function(rates, countryClass, age, riskClass, smoke, gender) {
    var colnum = 1;
    if (gender + smoke === 'MN') {
      colnum = 1;
    } else if (gender + smoke === 'MY') {
      colnum = 2;
    } else if (gender + smoke === 'FN') {
      colnum = 3;
    } else if (gender + smoke === 'FY') {
      colnum = 4;
    }
    if (riskClass === 'P') {
      colnum += 4;
    } else if (riskClass === 'S') {
      colnum += 8;
    } else if (riskClass === 'SS') {
      colnum += 12;
    }
    age = math.min(120, age);
    return rates.COI[countryClass]['' + age][colnum - 1];
  };
  var polcyYear = function(month) {
    return math.floor((month - 1) / 12) + 1;
  };
  var getPrevTpRate = function(month, rawData, type) {
    if (type === 'X') {
      return rawData[month - 12].TPX;
    } else if (type === 'Y') {
      return rawData[month - 12].TPY;
    } else if (type === 'XY') {
      return rawData[month - 12].TPXY;
    }
  };
  var roundup = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.ceil(math.multiply(value, scale)), scale);
  };
  var calcIllustration = function(month) {
    var showmessage = 'N';
    var pfd_rate = rates.fee['PolicyFeeDuration'];
    var ef_isp_rate = rates.fee['EF_ISP'];
    var surr_chrg_dur = rates.minAndMax['surChgDuration'];
    var fpisp_rate = rates.fee['PolicyFee'];
    var oafisp_rate = rates.fee['OAF_ISP'];
    var min_sa_rate = rates.minAndMax['minSA'];
    var wdfee_rate = rates.fee['WithdrawalFee'];
    var iCountryClass = rates.countryClass[quotation.iResidence];
    var pCountryClass = null;
    if (policytype === 'joint') {
      pCountryClass = rates.countryClass[quotation.pResidence];
    }
    var currnav_rate = rates.exchange[ccy];
    var min_nav = math.max((250000 * currnav_rate), (0.15 * premium));
    var withDraw_Fee = wdfee_rate * currnav_rate;
    var rawData = [];
    var defaultRawData = {
      plYear: 0,
      plMonth: 0,
      iAge: 0,
      pAge: 0,
      sumSa: 0,
      sum_assured_WD: 0,
      WD_Amount: 0,
      WD_Fee: 0,
      Surrender_Charge_EOM: 0,
      allocated_Premium_BOM: 0,
      Initial_Premium: 0,
      elm_Fee: 0,
      account_Value_EOM: 0,
      account_Value_BOM: 0,
      sum_Risk_BOM: 0,
      sumRisk_WD: 0,
      accClount_M: 0,
      account_Value_WD: 0,
      policy_Fee: 0,
      Trailer_Fee: 0,
      Fund_Return: 0,
      Ongoing_Fee: 0,
      Cost_Insurance: 0,
      TPX: 0,
      TPY: 0,
      TPXY: 0,
      QXYT_1: 0
    };
    rawData.push(Object.assign({}, defaultRawData));
    for (var m = 1; m <= month; m++) {
      var refmonth = 72;
      if (m < 12) {
        refmonth = 1;
      } else if (m < 24) {
        refmonth = 12;
      } else if (m < 36) {
        refmonth = 24;
      } else if (m < 48) {
        refmonth = 36;
      } else if (m < 60) {
        refmonth = 48;
      } else if (m < 72) {
        refmonth = 60;
      }
      var supp_chrg_rate = rates.surrenderCharge['' + refmonth];
      var prevMonth = rawData[m - 1];
      var currMonth = Object.assign({}, defaultRawData);
      currMonth.plYear = polcyYear(m);
      currMonth.plMonth = m;
      currMonth.iAge = quotation.iAge + currMonth.plYear - 1;
      currMonth.pAge = quotation.pAge + currMonth.plYear - 1;
      var coi_rate = 0;
      if (policytype === 'single') {
        coi_rate = coiRate(rates, iCountryClass, currMonth.iAge, riskClass, quotation.iSmoke, quotation.iGender);
        coi_rate = roundup(coi_rate * (1 + la1_em_perc / 100), 9);
        if (currMonth.iAge < quotation.iAge + pml_period) {
          coi_rate = roundup(coi_rate + la1_pml, 9);
        }
        coi_rate = math.min(coi_rate, 1000);
      } else {
        var coi_rate_x = coiRate(rates, pCountryClass, currMonth.pAge, riskClass, quotation.pSmoke, quotation.pGender);
        coi_rate_x = roundup(coi_rate_x * (1 + (la1_em_perc / 100)), 9);
        if (currMonth.pAge < quotation.pAge + pml_period) {
          coi_rate_x = roundup(coi_rate_x + la1_pml, 9);
        }
        coi_rate_x = math.min(coi_rate_x, 1000);
        var coi_rate_Y = coiRate(rates, iCountryClass, currMonth.iAge, riskClass2, quotation.iSmoke, quotation.iGender);
        coi_rate_Y = roundup(coi_rate_Y * (1 + (la1_em_perc2 / 100)), 9);
        if (currMonth.iAge < quotation.iAge + pml_period2) {
          coi_rate_Y = roundup(coi_rate_Y + la1_pml2, 9);
        }
        coi_rate_Y = math.min(coi_rate_Y, 1000);
        var prevTpx = 1;
        var prevTpy = 1;
        var prevTpxy = 1;
        if (currMonth.plYear !== 1) {
          prevTpx = getPrevTpRate(m, rawData, 'X');
          prevTpy = getPrevTpRate(m, rawData, 'Y');
          prevTpxy = getPrevTpRate(m, rawData, 'XY');
        }
        currMonth.TPX = roundup((1 - (coi_rate_x / 1000)) * prevTpx, 9);
        currMonth.TPY = roundup((1 - (coi_rate_Y / 1000)) * prevTpy, 9);
        currMonth.TPXY = roundup((currMonth.TPX + currMonth.TPY) - (currMonth.TPX * currMonth.TPY), 9);
        var rateTPXY = 0;
        if (Number(prevTpxy) !== 0) {
          rateTPXY = currMonth.TPXY / prevTpxy;
        }
        currMonth.QXYT_1 = roundup(1000 * (1 - rateTPXY), 9);
        coi_rate = math.max(currMonth.QXYT_1, 0.18);
      }
      if (m === 1) {
        currMonth.Initial_Premium = premium;
      }
      currMonth.elm_Fee = roundup(currMonth.Initial_Premium * ef_isp_rate, 9);
      currMonth.allocated_Premium_BOM = roundup(currMonth.Initial_Premium - currMonth.elm_Fee, 9);
      currMonth.account_Value_BOM = roundup(currMonth.allocated_Premium_BOM + prevMonth.account_Value_EOM, 9);
      currMonth.sum_Risk_BOM = roundup(math.max(0, (sumSa - currMonth.account_Value_BOM)), 9);
      if (m === 1) {
        currMonth.sumSa = sumSa;
      } else {
        currMonth.sumSa = prevMonth.sum_assured_WD;
      }
      var sumWithDrawValue = sumwithDraw(currMonth.plYear);
      var accVal_Multip_WDval = 0;
      if (math.mod((m - 1), 12) === 0) {
        accVal_Multip_WDval = currMonth.account_Value_BOM * sumWithDrawValue;
      }
      if (Number(sumWithDrawValue) !== 0) {
        if (Number(currMonth.account_Value_BOM) > 0) {
          if (math.mod((m - 1), 12) === 0) {
            if (Number(currMonth.account_Value_BOM - accVal_Multip_WDval) < Number(min_nav) || Number(currMonth.account_Value_BOM) < Number(min_nav)) {
              currMonth.WD_Amount = currMonth.account_Value_BOM;
            } else {
              if (Number(accVal_Multip_WDval) > Number(0.1 * currMonth.account_Value_BOM) && Number(prevMonth.sum_assured_WD - (accVal_Multip_WDval - (0.1 * currMonth.account_Value_BOM))) < Number(min_sa_rate * premium)) {
                currMonth.WD_Amount = (prevMonth.sum_assured_WD - (min_sa_rate * premium)) + (0.1 * currMonth.account_Value_BOM);
              } else {
                currMonth.WD_Amount = accVal_Multip_WDval;
              }
            }
          }
        }
      }
      if (Number(currMonth.WD_Amount) !== 0) {
        if (Number(currMonth.account_Value_BOM - accVal_Multip_WDval) >= Number(min_nav)) {
          if (Number(currMonth.WD_Amount) > Number(0.1 * currMonth.account_Value_BOM)) {
            currMonth.WD_Fee = withDraw_Fee;
          }
        }
      }
      if (m < surr_chrg_dur) {
        currMonth.Surrender_Charge_EOM = supp_chrg_rate * premium;
      }
      var avwdTemp = currMonth.account_Value_BOM - currMonth.WD_Amount - currMonth.WD_Fee;
      if (Number(avwdTemp) === Number(prevMonth.Surrender_Charge_EOM)) {
        currMonth.account_Value_WD = avwdTemp - prevMonth.Surrender_Charge_EOM;
      } else {
        currMonth.account_Value_WD = avwdTemp;
      }
      if (Number(currMonth.account_Value_BOM) < 0) {
        currMonth.sum_assured_WD = currMonth.sumSa;
      } else {
        if (Number(currMonth.WD_Amount) > Number(0.1 * currMonth.account_Value_BOM)) {
          currMonth.sum_assured_WD = currMonth.sumSa - (currMonth.WD_Amount - (0.1 * currMonth.account_Value_BOM));
        } else {
          currMonth.sum_assured_WD = currMonth.sumSa;
        }
      }
      if (currMonth.plYear <= pfd_rate) {
        if (math.mod((m - 1), 3) === 0) {
          currMonth.policy_Fee = roundup((premium * fpisp_rate) / 4, 9);
        }
      }
      if (math.mod((m - 1), 3) === 0) {
        currMonth.Ongoing_Fee = roundup((premium * oafisp_rate) / 4, 9);
      }
      if (m <= 12) {
        currMonth.accClount_M = !math.isZero(currMonth.WD_Amount) ? (prevMonth.accClount_M + currMonth.WD_Amount) : prevMonth.accClount_M;
      } else {
        currMonth.accClount_M = math.add(currMonth.WD_Amount, getsumCol11M(m, rawData, 'M'));
      }
      currMonth.sumRisk_WD = math.max(0, (currMonth.sum_assured_WD - currMonth.account_Value_WD - currMonth.accClount_M));
      if (math.mod((m - 1), 3) === 0) {
        currMonth.Cost_Insurance = roundup((coi_rate / 1000 / 4) * currMonth.sumRisk_WD, 9);
      }
      currMonth.Fund_Return = roundup((currMonth.account_Value_WD - (currMonth.policy_Fee + currMonth.Ongoing_Fee + currMonth.Cost_Insurance + currMonth.Trailer_Fee)) * (math.pow(1 + (in_IRR / 100), 1 / 12) - 1), 9);
      currMonth.account_Value_EOM = roundup(currMonth.account_Value_WD - (currMonth.policy_Fee + currMonth.Ongoing_Fee + currMonth.Cost_Insurance + currMonth.Trailer_Fee) + currMonth.Fund_Return, 9);
      if (m === currMonth.plYear * 12) {
        var dwAmount = Number(getsumCol11M(m, rawData, 'M') + currMonth.WD_Amount);
        if (dwAmount !== 0 && !isNaN(dwAmount) && dwAmount < minWD) {
          showmessage = 'Minimum withdrawal amount of ' + getCurrency(minWD, ccySign, 0) + ' has not been met.';
          return showmessage;
        }
      }
      rawData.push(currMonth);
    }
    return showmessage;
  };
  var minLaAge = quotation.iAge;
  if (policytype === 'joint') {
    minLaAge = math.min(quotation.pAge, quotation.iAge);
  }
  var endYear = 120 - minLaAge;
  var month = endYear * 12;
  var message = calcIllustration(month);
  return message;
}