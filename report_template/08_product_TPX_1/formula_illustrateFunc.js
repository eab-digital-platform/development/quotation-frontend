function(quotation, planInfo, planDetails, extraPara) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var planDetail = planDetails[planInfo.covCode];
  var planType = quotation.policyOptions.planType;
  var indexation = quotation.policyOptions.indexation === 'Y';
  var sumInsured = planInfo.sumInsured;
  var polYrs = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
  var premYrs;
  if (planInfo.premTerm === 'SP') {
    premYrs = 1;
  } else if (planInfo.premTerm.endsWith('_YR')) {
    premYrs = Number.parseInt(planInfo.premTerm);
  } else {
    premYrs = Number.parseInt(planInfo.premTerm) - quotation.iAge;
  }
  var illYrs;
  if (planType === 'renew') {
    var maxRenewalAge = Math.min(99 - polYrs, 75);
    illYrs = (Math.floor((maxRenewalAge - quotation.iAge) / polYrs) + 1) * polYrs;
  } else {
    illYrs = polYrs;
  }
  var emLoading = math.bignumber(0);
  var emLoadingRates = planDetail.rates.emLoading[quotation.iResidence];
  if (emLoadingRates) {
    emLoading = math.bignumber(emLoadingRates[quotation.iResidenceCity] || emLoadingRates.ALL || 0);
  }
  var yearPrems = [0];
  var guaranteedDBs = [0];
  var indexPrems = [];
  for (var i = 1; i <= illYrs; i++) {
    var age = quotation.iAge + i;
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, age - 1) || 0);
    var lsd = runFunc(planDetail.formulas.getLsd, planDetail, sumInsured, age - 1);
    var yearPrem;
    var guaranteedDB;
    var indexPrem = math.bignumber(0);
    if (i === 1) {
      yearPrem = math.bignumber(planInfo.yearPrem);
      guaranteedDB = math.bignumber(sumInsured);
    } else {
      yearPrem = yearPrems[i - 1];
      guaranteedDB = guaranteedDBs[i - 1];
      var prem = math.multiply(premRate, sumInsured, 0.001);
      prem = math.subtract(prem, roundDown(math.multiply(prem, lsd), 2));
      var loading = math.round(math.multiply(math.divide(roundDown(prem, 2), sumInsured), emLoading, 1000), 2);
      prem = math.add(prem, roundDown(math.multiply(loading, sumInsured, 0.001), 2));
      if (planType === 'renew') {
        if (i % polYrs === 1) {
          yearPrem = roundDown(prem, 2);
        } else if (i % polYrs > premYrs) {
          yearPrem = math.bignumber(0);
        }
      } else {
        if (indexation && i < polYrs + 1 && age <= 60 && polYrs > 5 && polYrs - i >= 4) {
          guaranteedDB = math.multiply(guaranteedDB, 1.05);
        } else if (indexation && i < polYrs + 1 && (age > 60 || polYrs - i < 4)) {
          guaranteedDB = math.max(guaranteedDB, sumInsured);
        }
        if (indexation) {
          indexPrem = math.multiply(math.add(premRate, loading), math.subtract(guaranteedDB, guaranteedDBs[i - 1]), 0.001);
        }
        if (i % polYrs <= premYrs) {
          if (indexation) {
            yearPrem = math.add(yearPrem, indexPrem);
          }
        } else {
          yearPrem = math.bignumber(0);
        }
      }
    }
    yearPrems.push(yearPrem);
    guaranteedDBs.push(guaranteedDB);
    indexPrems.push(indexPrem);
  }
  var commRates = runFunc(planDetail.formulas.getCommRates, quotation, planInfo, planDetails);
  var totComms = [0];
  var premIncSum = [0];
  for (var i = 1; i <= illYrs; i++) {
    var premInc = math.subtract(yearPrems[i], yearPrems[i - 1]);
    premIncSum.push(math.add(premInc, premIncSum[i - 1]));
    if (math.number(premInc)) {
      for (var j = 0; j < commRates.length; j++) {
        if (commRates[j]) {
          var comm = math.multiply(indexation ? premInc : premIncSum[i], commRates[j]);
          if (!totComms[i + j]) {
            totComms[i + j] = math.bignumber(0);
          }
          totComms[i + j] = math.add(totComms[i + j], comm);
        }
      }
    }
  }
  var totYearPrem = math.bignumber(0);
  var illustration = [];
  for (var i = 0; i < illYrs; i++) {
    totYearPrem = math.add(totYearPrem, yearPrems[i + 1] ? yearPrems[i + 1] : 0);
    illustration.push({
      yearPrem: yearPrems[i + 1] ? math.number(yearPrems[i + 1]) : 0,
      totComm: totComms[i + 1] ? math.number(totComms[i + 1]) : 0,
      guaranteedDB: guaranteedDBs[i + 1] ? math.number(guaranteedDBs[i + 1]) : 0,
      totYearPrem: math.number(totYearPrem),
      indexPrem: indexPrems[i] ? math.number(indexPrems[i]) : 0
    });
  }
  return illustration;
}