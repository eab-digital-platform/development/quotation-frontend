function(planDetail, quotation) {
  var polTerm = Number.parseInt(quotation.plans[0].policyTerm);
  if (!polTerm) {
    return [];
  }
  var premTermList = [];
  if (quotation.policyOptions.planType === 'renew') {
    if (quotation.paymentMode === 'L') {
      premTermList.push({
        value: 'SP',
        title: 'Single Premium'
      });
    } else {
      premTermList.push({
        value: polTerm + '_YR',
        title: polTerm + ' Years'
      });
    }
  } else if (quotation.policyOptions.planType === 'toAge') {
    var values = [15, 20];
    for (var v in values) {
      var value = values[v];
      if (Number.parseInt(polTerm) - quotation.iAge - value >= 5) {
        premTermList.push({
          value: value + '_YR',
          title: value + ' Years'
        });
      }
    }
    premTermList.push({
      value: polTerm + '_TA',
      title: 'To Age ' + polTerm
    });
  }
  return premTermList;
}