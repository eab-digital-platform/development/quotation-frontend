function(quotation, planInfo, planDetail) {
  var roundup = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.ceil(math.multiply(value, scale)), scale);
  };
  var modalFactor = -1;
  var payModes = planDetail.payModes;
  for (var i = 0; i < payModes.length; i++) {
    let {
      mode,
      factor,
      operator
    } = payModes[i];
    if (quotation.paymentMode == mode) {
      modalFactor = factor;
    }
  }
  var SizeDiscount_Table = planDetail.rates.SizeDiscount_Table;
  var LOW_SA = SizeDiscount_Table.LOW_SA;
  var LOADING = SizeDiscount_Table.LOADING;
  var rawPremRate = math.bignumber(planDetail.rates.premRate[quotation.iGender + (quotation.iSmoke == 'N' ? 'N' : 'S')][quotation.plans[0].premTerm + 'PT'][quotation.iAge]);
  var basicPlan = quotation.plans[0];
  var modalPrem = math.bignumber(basicPlan.premium);
  var maxSA = 0;
  for (var i = 0; i < LOW_SA.length; i++) {
    var lowSA = LOW_SA[i];
    var premRateWithLSD = math.bignumber(math.add(math.bignumber(LOADING[i]), rawPremRate));
    var calSA = math.bignumber(math.divide(math.multiply(math.divide(modalPrem, modalFactor), 1000), premRateWithLSD));
    var appSA = (calSA < lowSA) ? 0 : calSA;
    if (maxSA < appSA) maxSA = appSA;
  }
  maxSA = roundup(maxSA, -2);
  return math.number(maxSA);
}