function(planDetail, quotation) {
  var premTermList = null;
  var foundDefault = false;
  if (planDetail.premMaturities) {
    premTermList = [];
    for (var p in planDetail.premMaturities) {
      var mat = planDetail.premMaturities[p];
      if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
        var interval = mat.interval || 1;
        for (var m = mat.minTerm; m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || (m + quotation.iAge) <= mat.maxAge) && (!mat.ownerMaxAge || !quotation.pAge || (m + quotation.pAge) <= mat.ownerMaxAge); m += interval) {
          foundDefault |= (mat.default === m);
          premTermList.push({
            "value": m + "",
            "title": m + " Years",
            "default": mat.default === m
          });
        }
        if (mat.default === 0) {
          premTermList[0].default = true;
        } else if (mat.default === 999 || !foundDefault) {
          premTermList[premTermList.length - 1].default = true;
        }
      }
    }
  }
  return premTermList;
}