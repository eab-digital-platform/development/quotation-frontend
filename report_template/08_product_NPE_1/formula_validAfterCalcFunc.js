function(quotation, planInfo, planDetail) {
  quotValid.validateMandatoryFields(quotation, planInfo, planDetail);
  if (planInfo.calcBy) {
    var totalPrem = 0;
    for (var i = 0; i < quotation.plans.length; i++) {
      var plan = quotation.plans[i];
      if (plan.premium) {
        totalPrem = math.add(totalPrem, math.bignumber(plan.premium));
      }
    }
    totalPrem = math.number(totalPrem);
    if (planDetail.inputConfig && planDetail.inputConfig.premlim) {
      let min = planDetail.inputConfig.premlim.min;
      if (min && min > totalPrem) {
        var err = {
          covCode: planInfo.covCode,
          msgPara: [],
          code: ''
        };
        err.msgPara.push(getCurrency(min, '$', 2));
        err.code = 'js.err.invalid_premlim_min';
        err.msgPara.push(totalPrem);
        quotDriver.context.addError(err);
      }
    }
  }
  var planCodeMap = planDetail.planCodeMapping;
  if (planInfo.policyTerm) {
    var idx = planCodeMap && planCodeMap.policyTerm ? planCodeMap.policyTerm.indexOf(planInfo.policyTerm) : -1;
    if (idx >= 0) planInfo.planCode = planCodeMap.planCode[idx];
  }
}