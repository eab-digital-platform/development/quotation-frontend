function(quotation, planInfo, planDetails) {
  planInfo.paymentMethod = quotation.policyOptions.axaShield > quotation.policyOptions.awl ? 'CPF_CASH' : 'CPF';
  planInfo.payFreq = 'A';
  var covClass = planInfo.covClass;
  var gender = quotation.iGender;
  var age = quotation.iAge;
  if (covClass) {
    var planDetail = planDetails[planInfo.covCode];
    var modalFactor = math.bignumber(1);
    var payModes = planDetail.payModes;
    for (var i in payModes) {
      if (payModes[i].mode === planInfo.payFreq) {
        modalFactor = math.bignumber(payModes[i].factor);
        break;
      }
    }
    var rate = math.bignumber(planDetail.rates.premRate[covClass][gender][age]);
    var regularPrem = math.divide(math.floor(math.multiply(math.multiply(rate, modalFactor), 100)), 100);
    var prem = regularPrem;
    if (planDetail.gstInd === 'Y') {
      var gstRate = math.bignumber(planDetail.gstRate);
      var gstAmt = math.round(math.multiply(regularPrem, gstRate), 2);
      prem = math.round(math.add(prem, gstAmt), 1);
      planInfo.tax = {
        yearTax: math.number(gstAmt)
      };
    }
    planInfo.premium = math.number(prem);
  }
}