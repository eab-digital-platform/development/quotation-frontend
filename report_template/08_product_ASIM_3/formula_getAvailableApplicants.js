function(quotation, planDetails, profiles, fnaInfo) {
  const pda = fnaInfo && fnaInfo.pda;
  const fna = fnaInfo && fnaInfo.fna;
  if (!pda || !fna) {
    return profiles;
  }
  var getSelectedNeeds = function(tid, p, pda, fna) {
    var dt = '';
    var hasSpouse = _.get(pda, 'applicant') === 'joint' ? true : false;
    var sp = _.find(_.get(p, 'dependants'), d => d.relationship === 'SPO');
    var sid = _.get(sp, 'cid');
    if (tid === p.cid) {
      dt = 'owner';
    } else if (hasSpouse && sid === tid) {
      dt = 'spouse';
    } else if (_.find(_.get(pda, 'dependants') && _.get(pda, 'dependants').split(','), dCid => dCid === tid)) {
      dt = 'dependants';
    }
    if (!dt) {
      return;
    }
    var aspects = _.split(_.get(fna, 'aspects'), ',');
    var result = [];
    for (var i in aspects) {
      var a = aspects[i];
      var v = _.get(fna, `${a}.${dt}`);
      if (dt === 'dependants') {
        for (var _v in v) {
          if (v[_v] && v[_v].cid === tid) {
            v = v[_v];
            break;
          }
        }
      }
      if (!_.get(v, 'init') && _.get(v, 'isActive')) {
        result.push(a);
      }
    }
    return result;
  };
  const proposer = profiles[0];
  let dependants = [proposer.cid];
  if (pda.applicant === 'joint') {
    const spouse = _.find(proposer.dependants, d => d.relationship === 'SPO');
    if (spouse) {
      dependants.push(spouse.cid);
    }
  }
  dependants = dependants.concat(pda.dependants.length && pda.dependants.split(','));
  const needsMap = {};
  _.each(dependants, (cid) => {
    needsMap[cid] = getSelectedNeeds(cid, proposer, pda, fna);
  });
  return _.filter(profiles, p => needsMap[p.cid] && needsMap[p.cid].indexOf('hcProtection') > -1);
}