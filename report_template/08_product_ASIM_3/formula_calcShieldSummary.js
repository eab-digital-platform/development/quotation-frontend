function(quotation) {
  var totCashPortion = math.bignumber(0);
  var totMedisave = math.bignumber(0);
  var totPremium = math.bignumber(0);
  var totYearCashPortion = math.bignumber(0);
  var totYearPrem = math.bignumber(0);
  if (quotation.insureds) {
    for (var i in quotation.insureds) {
      var subQuot = quotation.insureds[i];
      if (subQuot.plans[0].premium) {
        totCashPortion = math.add(totCashPortion, subQuot.policyOptions.cashPortion, subQuot.totRiderPrem);
        totMedisave = math.add(totMedisave, subQuot.policyOptions.medisave);
        totPremium = math.add(totPremium, math.bignumber(subQuot.plans[0].premium), subQuot.totRiderPrem);
        totYearCashPortion = math.add(totYearCashPortion, subQuot.totYearCashPortion);
        totYearPrem = math.add(totYearPrem, math.bignumber(subQuot.plans[0].premium), subQuot.totRiderYearPrem);
      }
    }
  }
  quotation.totCashPortion = math.number(totCashPortion);
  quotation.totMedisave = math.number(totMedisave);
  quotation.totPremium = math.number(totPremium);
  quotation.totYearCashPortion = math.number(totYearCashPortion);
  quotation.totYearPrem = math.number(totYearPrem);
  quotation.premium = math.number(totPremium);
}