function(planDetail, quotation, planDetails) {
  quotDriver.prepareAttachableRider(planDetail, quotation, planDetails);
  var covClass = quotation.plans[0].covClass;
  if (covClass == '4') {
    for (var i in planDetail.inputConfig.riderList) {
      if (planDetail.inputConfig.riderList[i].covCode == 'WIR') {
        planDetail.inputConfig.riderList.splice(i, 1);
        break;
      }
    }
  }
}