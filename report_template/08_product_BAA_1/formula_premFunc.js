function(quotation, planInfo, planDetail) {
  var ocls = runFunc(planDetail.formulas.getOccupationClass, quotation, planDetail, null, null, arguments[4]);
  var prate = parseFloat(planDetail.rates.premRate[quotation.paymentMode][ocls]);
  var scale = parseFloat(planDetail.saScale);
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    if (plan.covCode == planInfo.covCode) {
      plan.premRate = math.number(prate);
      break;
    }
  }
  var prem = math.divide(math.multiply(math.bignumber(planInfo.sumInsured), prate), scale);
  return math.number(prem);
}