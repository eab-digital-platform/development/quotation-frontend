function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  let plans = quotation.plans;
  if (planDetail.gstInd == "Y") {
    for (let i in plans) {
      let plan = plans[i];
      if (plan.sumInsured) {
        plan.tax = {
          yearTax: math.number(math.round(math.multiply(math.bignumber(plan.yearPrem), planDetail.gstRate), 2)),
          halfYearTax: math.number(math.round(math.multiply(math.bignumber(plan.halfYearPrem), planDetail.gstRate), 2)),
          quarterTax: math.number(math.round(math.multiply(math.bignumber(plan.quarterPrem), planDetail.gstRate), 2)),
          monthTax: math.number(math.round(math.multiply(math.bignumber(plan.monthPrem), planDetail.gstRate), 2))
        };
        plan.premium = math.number(math.round(math.multiply(math.bignumber(plan.premium), 1 + planDetail.gstRate), 2));
      }
    }
    quotation.tax = {
      yearTax: math.number(math.round(math.multiply(math.bignumber(quotation.totYearPrem), planDetail.gstRate), 2)),
      halfYearTax: math.number(math.round(math.multiply(math.bignumber(quotation.totHalfyearPrem), planDetail.gstRate), 2)),
      quarterTax: math.number(math.round(math.multiply(math.bignumber(quotation.totQuarterPrem), planDetail.gstRate), 2)),
      monthTax: math.number(math.round(math.multiply(math.bignumber(quotation.totMonthPrem), planDetail.gstRate), 2))
    };
    quotation.totYearPrem = math.number(math.add(math.bignumber(quotation.totYearPrem), quotation.tax.yearTax));
    quotation.totHalfyearPrem = math.number(math.add(math.bignumber(quotation.totHalfyearPrem), quotation.tax.halfYearTax));
    quotation.totQuarterPrem = math.number(math.add(math.bignumber(quotation.totQuarterPrem), quotation.tax.quarterTax));
    quotation.totMonthPrem = math.number(math.add(math.bignumber(quotation.totMonthPrem), quotation.tax.monthTax));
    quotation.premium = math.number(math.round(math.multiply(math.bignumber(quotation.premium), 1 + planDetail.gstRate), 2));
  }
  if (planDetail.planCodeMapping) {
    let ocls = runFunc(planDetail.formulas.getOccupationClass, quotation, planDetail);
    for (let i in plans) {
      if (planDetail.planCodeMapping[plans[i].covCode]) {
        plans[i].planCode = planDetail.planCodeMapping[plans[i].covCode][ocls];
      }
    }
  }
  return quotation.plans[0];
}