function(agent, quotation, planDetails) {
  var files = []; /** if (quotation.policyOptions.payoutType === 'Level'){ files.push({ covCode: quotation.plans[0].covCode, fileId: 'prod_summary_2' }); }else{ files.push({ covCode: quotation.plans[0].covCode, fileId: 'prod_summary_1' }); } */
  files.push({
    covCode: quotation.plans[0].covCode,
    fileId: 'prod_summary_1'
  });
  var plan_length = quotation.plans.length;
  if (plan_length >= 2) {
    files.push({
      covCode: quotation.plans[1].covCode,
      fileId: 'prod_summary_1'
    });
  }
  var attachments = [{
    id: 'prodSummary',
    label: 'Product Summary',
    fileName: 'product_summary.pdf',
    files: files
  }];
  return attachments;
}