function(quotation, planInfo, planDetails, extraPara) {
  var planDetail = planDetails[planInfo.covCode]; /** var planInfoTPD = quotation.plans.find(function(p) { return p.covCode === 'TPD'; }); */
  var rates = planDetail.rates;
  var dealerGroup = quotation.agent.dealerGroup;
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm); /**---------------------*/
  var payMethod = quotation.policyOptions.paymentMethod;
  var tempaccterm = quotation.policyOptions.accumulationPeriod;
  var arryaccterm = tempaccterm.split(' ');
  var accPeriod = parseInt(arryaccterm[0]);
  var depositOption = quotation.policyOptions.depositOption;
  var depositFile = parseInt(quotation.policyOptions.depositInput);
  var premTerm = parseInt(planInfo.premTerm);
  var gtdPayoutFactor = 0;
  var sa = quotation.sumInsured;
  var yearPrem = planInfo.yearPrem;
  var policyTerm = parseInt(planInfo.policyTerm); /**--------------------*/
  if (payoutTerm === 120) {
    payoutTerm = 120 - quotation.iAge - premTerm - accPeriod;
  }
  const MAX_POLICY_YEAR = 105;
  const TOTAL_ACC_INCOME_RATE = 0.03;
  var getChannel = function() {
    var mappedChannel = dealerGroup;
    if (dealerGroup === 'SYNERGY') {
      mappedChannel = 'AGENCY';
    } else if (dealerGroup === 'FUSION') {
      mappedChannel = 'BROKER';
    }
    return mappedChannel;
  };
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var roundup = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.ceil(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var convertBignumberToReadable = function(data) { /* for (var row in data) { for (var col in data[row]) { if (typeof(data[row][col]) !== 'string') { data[row][col] = math.number(data[row][col]); } } } console.log('rawData', data); */ }; /** var prepareIRR = function(currData, irrDataGtdPaidOut, irrDataProjectedPaidOut, irrDataProjectedAccum, roll0) { if(roll0){ irrGtdRate_Return.push(math.multiply(math.bignumber(yearPrem), -1)); }else{ irrGtdRate_Return.push(currData.CFgtdRate_Return); } var negYearPrem = 1; **math.subtract(math.multiply(math.bignumber(yearPrem), -1), math.bignumber(planInfo.yearPrem)); if (currData.lAttainedAgeEOY >= retirementAge) { currData._irrGtdCashFlowPaidOut = currData.dGteedAnnuityIncome; currData._irrProjectedCashFlowPaidOut = math.add(currData.dGteedAnnuityIncome, currData.eNonGteedIncome); } else if (currData.aPolicyYear < planInfo.premTerm) { currData._irrGtdCashFlowPaidOut = negYearPrem; currData._irrProjectedCashFlowPaidOut = negYearPrem; } if (currData.aPolicyYear < planInfo.premTerm) { currData._irrGtdCashFlowAccum = negYearPrem; currData._irrProjectedCashFlowAccum = negYearPrem; } else if (currData.aPolicyYear === basicBT) { currData._irrGtdCashFlowAccum = trunc(currData._suppGteedSurrenderValue, 0); currData._irrProjectedCashFlowAccum = trunc(currData._suppTotalSurrenderValue, 0); } irrDataGtdPaidOut.push(currData._irrGtdCashFlowPaidOut); irrDataProjectedPaidOut.push(currData._irrProjectedCashFlowPaidOut); irrDataProjectedAccum.push(currData._irrProjectedCashFlowAccum); }; */
  var channel = getChannel(); /** var ageIndex = rates.LEP2_GRI_rates.rate.age.indexOf(quotation.iAge); var LEP2_GRI_rate = math.bignumber(ageIndex >= 0 ? rates.LEP2_GRI_rates[planInfo.planCode][ageIndex] : 0); var LEP2_GRI_rate = 8; var basicBT = retirementAge + (payoutTerm === 99 ? 99 - retirementAge : payoutTerm) - quotation.iAge; var cashAdvanceFactor = rates.cashAdvance.factor[payoutType]; var cashAdvanceIndexation = rates.cashAdvance.indexation[payoutType]; */
  var cashAdvanceFactor = 1;
  var cashAdvanceIndexation = 1;
  var commissionTable = rates.commission[channel];
  var GTD_FACTOR_RATE = rates.GTD_FACTOR_RATE;
  var SUR_RB_RATE = rates.SUR_RB_RATE;
  var TB_RATE = rates.TB_RATE;
  var GCV_RATE = rates.GCV_RATE;
  var cvLookupRef = planInfo.planCode + quotation.iGender + quotation.iAge;
  var rbsurrRef = planInfo.planCode + quotation.iAge + quotation.iGender;
  var tbRef = planInfo.planCode + quotation.iAge;
  var gtdRef = planInfo.planCode + quotation.iGender + quotation.iSmoke; /** get rate */
  var getRate = function(ref, index, Rates) {
    var tempRates = Rates[ref];
    if (tempRates === null) {
      return 0;
    }
    return tempRates[index];
  };
  var tempgtdPayoutFactor = getRate(gtdRef, quotation.iAge, GTD_FACTOR_RATE);
  var calcIllustration = function(arorkey, illustration) {
    var sustainTestResult = true;
    var aror = rates.aror[arorkey];
    var isHighRate = aror * 100 === 475;
    var rawData = [];
    var irrDataGtdPaidOut = [];
    var irrDataProjectedPaidOut = [];
    var irrDataProjectedAccum = [];
    var irrGtd = [];
    var irrGtd_Ngtd = [];
    var irrsuppGtd_Ngtd = [];
    var retireSolutionData = {
      totalPremiumsPaid: math.bignumber(0),
      totalGuranteedRetireInc: math.bignumber(0),
      totalReGtd: math.bignumber(0),
      nonGtdRedeposited: math.bignumber(0),
      redepositedGtd: math.bignumber(0),
      nonGuranteedRetireIncPaidOut: math.bignumber(0),
      nonGuranteedRetireIncAccum: math.bignumber(0)
    };
    var defaultRawData = {
      aPolicyYear: 0,
      gtdAnnual: math.bignumber(0),
      suppgtdAnnual: math.bignumber(0),
      cashOption: math.bignumber(0),
      column_I: math.bignumber(0),
      column_K: math.bignumber(0),
      column_O: math.bignumber(0),
      column_Q: math.bignumber(0),
      column_H: math.bignumber(0),
      column_P: math.bignumber(0),
      sumcolumn_P: math.bignumber(0),
      column_AQ: math.bignumber(0),
      column_AW: math.bignumber(0),
      column_AX: math.bignumber(0),
      column_AV: math.bignumber(0),
      db_nongtd: math.bignumber(0),
      column_AT: math.bignumber(0),
      column_BF: math.bignumber(0),
      column_AR: math.bignumber(0),
      column_AS: math.bignumber(0),
      column_AP: math.bignumber(0),
      sv_nongtd: math.bignumber(0),
      todSv: math.bignumber(0),
      suppaccGtd: math.bignumber(0),
      suppDbGtd: math.bignumber(0),
      bAnnuityIncomeFactor: math.bignumber(0),
      cIndexationFactor: math.bignumber(0),
      dGteedAnnuityIncome: math.bignumber(0),
      eNonGteedIncome: math.bignumber(0),
      fTotalAccIncome: math.bignumber(0),
      gPremiumsPaid: math.bignumber(0),
      hTotalPremiumsPaidToDate: math.bignumber(0),
      iVopPaidToDate: math.bignumber(0),
      jTotalDistributionCost: math.bignumber(0),
      lAttainedAgeEOY: quotation.iAge,
      mEndOfPolicyYearAge: "",
      nGteedDeathBenefit: math.bignumber(0),
      oTotalDeathBenefit: math.bignumber(0),
      pGteedSurrenderValue: math.bignumber(0),
      qTotalSurrenderValue: math.bignumber(0),
      rAccumCashAdv: math.bignumber(0),
      sEffectOfDeductions: math.bignumber(0),
      tNegPremiumPHYieldCalc: math.bignumber(0),
      uNegCFPHYieldCalc: math.bignumber(0),
      vInterestOnCashAdvOpt: math.bignumber(0),
      wH_SuppBIAccumOptAddGteed: math.bignumber(0),
      xHwL_PremiumPaidTPD: math.bignumber(0),
      yHxL_AccumNGRI: math.bignumber(0),
      _commission: 0,
      _accumGteedAnnuityIncome: math.bignumber(0),
      _accumNonGteedIncome: math.bignumber(0),
      _suppGteedDeathBenefit: math.bignumber(0),
      _suppGteedSurrenderValue: math.bignumber(0),
      _suppNonGteedInvestmentReturn: math.bignumber(0),
      _suppTotalSurrenderValue: math.bignumber(0),
      _irrGtdCashFlowPaidOut: math.bignumber(0),
      _irrProjectedCashFlowPaidOut: math.bignumber(0),
      _irrGtdCashFlowAccum: math.bignumber(0),
      _irrProjectedCashFlowAccum: math.bignumber(0)
    };
    rawData.push(Object.assign({}, defaultRawData)); /** this Rate need get value */
    var gtdPayoutFactor = 0; /*F*/
    var RB_Rate = 0; /*B*/
    var RB_IND_Rate = 0; /*C*/
    var tbRate = 0; /*E*/
    var gcv_rate = 0;
    var Su_RB_Rate = 0; /*D*/
    var ngtdrate = 0;
    var incomPayout = 0;
    var nGtdBonus = math.bignumber(0);
    var suppNgtd = math.bignumber(0);
    for (var year = 1; year <= MAX_POLICY_YEAR; year++) {
      var currData = Object.assign({}, defaultRawData);
      var prevData = rawData[year - 1];
      if (isHighRate) {
        RB_Rate = 10;
        ngtdrate = 0.03;
      } else {
        RB_Rate = 1.5;
        ngtdrate = 0.015;
      }
      RB_IND_Rate = RB_Rate / 1000;
      Su_RB_Rate = getRate(rbsurrRef, year - 1, SUR_RB_RATE);
      Su_RB_Rate = Su_RB_Rate / 100;
      tbRate = getRate(tbRef, year, TB_RATE);
      tbRate = tbRate / 100;
      if (year > premTerm + accPeriod) {
        gtdPayoutFactor = tempgtdPayoutFactor;
      } else {
        gtdPayoutFactor = 0;
      }
      gcv_rate = getRate(rbsurrRef, year, GCV_RATE) || 0;
      currData.aPolicyYear = year; /** if (year + quotation.iAge < retirementAge || year > (payoutTerm === 99 ? basicBT : basicBT - 1)) { currData.bAnnuityIncomeFactor = math.bignumber(0); } else { var aTemp = math.pow(math.add(math.bignumber(1), math.bignumber(cashAdvanceIndexation)), math.bignumber(year + quotation.iAge - retirementAge)); currData.bAnnuityIncomeFactor = math.multiply(aTemp, math.divide(LEP2_GRI_rate, math.bignumber(100))); } currData.cIndexationFactor = roundup(round(currData.bAnnuityIncomeFactor, 10), 4); currData.dGteedAnnuityIncome = math.multiply(currData.cIndexationFactor, quotation.sumInsured); currData._accumGteedAnnuityIncome = math.add(prevData._accumGteedAnnuityIncome, currData.dGteedAnnuityIncome); */ /** Prem Paid To Date */
      if (year <= premTerm) {
        currData.gPremiumsPaid = math.bignumber(yearPrem);
      } /** lzx */
      if (year <= policyTerm) {
        currData.hTotalPremiumsPaidToDate = !math.isZero(currData.gPremiumsPaid) ? math.add(prevData.hTotalPremiumsPaidToDate, currData.gPremiumsPaid) : prevData.hTotalPremiumsPaidToDate; /** column G */
        currData.gtdAnnual = math.multiply(math.bignumber(gtdPayoutFactor), math.bignumber(sa));
        currData._accumGteedAnnuityIncome = math.add(prevData._accumGteedAnnuityIncome, currData.gtdAnnual);
        incomPayout = math.max(currData.gtdAnnual, incomPayout); /** column J RB without Cash Out Option*/
        currData.cashOption = math.add(round(round(math.multiply(prevData.cashOption, (math.bignumber(1 + RB_IND_Rate))), 2), 0), round(math.divide(math.multiply(math.bignumber(RB_Rate), math.bignumber(sa)), math.bignumber(1000)), 0)); /**column K = AP*/
        currData.column_K = math.add(round(round(math.multiply(prevData.column_K, (math.bignumber(1 + RB_IND_Rate))), 2), 0), round(math.divide(math.multiply(math.bignumber(sa), math.bignumber(RB_Rate)), math.bignumber(1000)), 0)); /**column AQ=K*E */ /**currData.column_AQ=math.multiply(currData.column_K,math.bignumber(tbRate));*/
        currData.column_AQ = currData.column_K; /**column AR=AQ*E */
        currData.column_AR = math.multiply(currData.column_AQ, math.bignumber(tbRate)); /**column AI Non-guaranteed Bonus at 4.75% p.a.1 [C]*/
        if (year == policyTerm) { /**column AI */
          nGtdBonus = math.multiply(currData.cashOption, (math.bignumber(tbRate + 1))); /**column BB */
          suppNgtd = math.multiply(currData.column_K, (math.bignumber(tbRate + 1)));
        } /**column I */
        var pol_yea = (policyTerm - year + 1);
        if (depositFile == payoutTerm && year == policyTerm) { /**column H */
          if (year < (premTerm + accPeriod + depositFile + 1)) {
            currData.column_H = math.multiply(math.add(prevData.column_H, currData.gtdAnnual), math.bignumber(1 + ngtdrate));
          } else {
            currData.column_H = math.multiply(math.subtract(prevData.column_H, currData.column_I), math.bignumber(1 + ngtdrate));
          }
          currData.column_I = currData.column_H;
        } else if (year >= (premTerm + accPeriod + depositFile + 1)) { /**currData.column_I =math.divide(math.multiply(prevData.column_H,math.bignumber(1+ngtdrate)),math.bignumber(pol_yea));*/
          currData.column_I = math.divide(prevData.column_H, math.bignumber(pol_yea));
        } /**column H */
        if (year < (premTerm + accPeriod + depositFile + 1)) {
          currData.column_H = math.multiply(math.add(prevData.column_H, currData.gtdAnnual), math.bignumber(1 + ngtdrate));
        } else {
          currData.column_H = math.multiply(math.subtract(prevData.column_H, currData.column_I), math.bignumber(1 + ngtdrate));
        } /**column Q */
        if (depositFile === 0) {
          currData.column_Q = currData.gtdAnnual;
        } else {
          if (depositFile == payoutTerm) {
            currData.column_Q = currData.column_I;
          } else if (year >= (premTerm + accPeriod + depositFile + 1)) {
            currData.column_Q = math.add(currData.gtdAnnual, currData.column_I);
          }
        } /**column O */
        if (year == policyTerm && depositFile == payoutTerm) {
          currData.column_O = currData._accumGteedAnnuityIncome;
        } else {
          if (currData.column_Q != 0) {
            if (payoutTerm === depositFile) {
              currData.column_O = math.bignumber(0);
            } else {
              currData.column_O = math.add(currData.gtdAnnual, math.divide(math.multiply(depositFile, currData.gtdAnnual), math.subtract(payoutTerm, depositFile)));
            }
          }
        } /**sum column O */
        currData.suppaccGtd = math.add(prevData.suppaccGtd, currData.column_O); /**column p */
        if (currData.column_Q != 0) {
          currData.column_P = math.subtract(currData.column_Q, currData.column_O);
        }
        currData.sumcolumn_P = math.add(prevData.sumcolumn_P, currData.column_P); /**column AM */
        var cf = math.add(math.multiply(currData.gPremiumsPaid, -1), currData.gtdAnnual);
        irrGtd.push(cf); /**column AN*/
        irrGtd_Ngtd.push(cf); /**column BF*/
        if (depositFile === payoutTerm) {
          if (year - 1 <= policyTerm) {
            currData.column_BF = math.add(math.multiply(currData.gPremiumsPaid, -1), prevData.column_O);
          }
        } else {
          if (year <= policyTerm) {
            currData.column_BF = math.add(math.multiply(currData.gPremiumsPaid, -1), currData.column_O);
          }
        }
        irrsuppGtd_Ngtd.push(currData.column_BF); /**column AC */
        currData.pGteedSurrenderValue = trunc(math.divide(math.multiply(math.bignumber(gcv_rate), math.bignumber(sa)), 1000), 0); /**column AA*/
        currData.db_nongtd = trunc(math.add(currData.cashOption, math.multiply(currData.cashOption, math.bignumber(tbRate))), 2); /**column AF*/
        currData.sv_nongtd = trunc(math.add(math.multiply(currData.cashOption, math.bignumber(Su_RB_Rate)), round(math.multiply(math.multiply(currData.cashOption, math.bignumber(Su_RB_Rate)), math.bignumber(tbRate)), 2)), 2); /**column T*/
        currData.iVopPaidToDate = trunc(math.multiply(math.add(currData.gPremiumsPaid, prevData.iVopPaidToDate), math.add(math.divide(math.bignumber(aror), 100), 1)), 2); /**column AK*/
        currData.rAccumCashAdv = trunc(math.multiply(math.add(prevData.rAccumCashAdv, currData.gtdAnnual), math.add(ngtdrate, 1)), 2);
      } else if (year == policyTerm + 1) {
        irrGtd.push(math.bignumber(0));
        irrGtd_Ngtd.push(nGtdBonus); /**column BF*/
        if (depositFile === payoutTerm) {
          if (year - 1 <= policyTerm) {
            currData.column_BF = math.add(math.multiply(currData.gPremiumsPaid, -1), prevData.column_O);
          }
        } else {
          if (year <= policyTerm) {
            currData.column_BF = math.add(math.multiply(currData.gPremiumsPaid, -1), currData.column_O);
          }
        }
        irrsuppGtd_Ngtd.push(math.add(currData.column_BF, suppNgtd));
      } /** var LEP2_NGRI = rates.LEP2_NGRI[aror * 100]; var LEP2_NGRI ageIndex = LEP2_NGRI.age.indexOf(quotation.iAge); var LEP2_NGRI_rate = math.bignumber(ageIndex >= 0 ? LEP2_NGRI[planInfo.planCode][ageIndex] : 0); if (math.isZero(currData.dGteedAnnuityIncome) || currData.aPolicyYear > basicBT) { LEP2_NGRI_rate = math.bignumber(0); } var LEP2_NGRI_rate =2; currData.eNonGteedIncome = round(math.multiply(LEP2_NGRI_rate, currData.dGteedAnnuityIncome), 2); currData._accumNonGteedIncome = math.add(prevData._accumNonGteedIncome, currData.eNonGteedIncome); */ /** if (year <= planInfoTPD.premTerm) { currData.xHwL_PremiumPaidTPD = math.bignumber(planInfoTPD.yearPrem); } if (year <= basicBT) { currData.fTotalAccIncome = math.add(math.add(math.multiply(prevData.fTotalAccIncome, 1 + TOTAL_ACC_INCOME_RATE), currData.dGteedAnnuityIncome), currData.eNonGteedIncome); var currYearPremiumPaid = math.add(currData.gPremiumsPaid, currData.xHwL_PremiumPaidTPD); currData.hTotalPremiumsPaidToDate = !math.isZero(currYearPremiumPaid) ? math.add(prevData.hTotalPremiumsPaidToDate, currYearPremiumPaid) : prevData.hTotalPremiumsPaidToDate; currData.iVopPaidToDate = trunc(math.multiply(math.add(currYearPremiumPaid, prevData.iVopPaidToDate), math.add(math.divide(math.bignumber(aror), 100), 1)), 2); if( year===basicBT && payoutTerm===99) { currData.wH_SuppBIAccumOptAddGteed = currData._accumGteedAnnuityIncome; } else{ currData.wH_SuppBIAccumOptAddGteed = prevData._accumGteedAnnuityIncome; } } */
      var commissionColIndex = commissionTable.policyYear.indexOf(year);
      if (commissionColIndex < 0) {
        commissionColIndex = commissionTable.policyYear.length - 1;
      }
      var ppt = planInfo.premTerm;
      if (ppt === 30) {
        ppt = 25;
      }
      currData._commission = trunc(math.bignumber(commissionTable[ppt][commissionColIndex]), 4); /**column U Total Distribution Cost To-date*/
      currData.jTotalDistributionCost = math.add(math.multiply(currData.gPremiumsPaid, currData._commission), prevData.jTotalDistributionCost);
      currData.lAttainedAgeEOY = quotation.iAge + year;
      currData.mEndOfPolicyYearAge = currData.aPolicyYear + '/' + (currData.lAttainedAgeEOY); /* if (year <= basicBT) { var cvl = 'CVL' + (year < 10 ? '0' : '') + year; var LEP2_CV_rates = 'LEP2_CV_rates_' + quotation.iGender; var cvlIndex = rates[LEP2_CV_rates].Ref.indexOf(cvl); var LEP2_CV_rate = cvlIndex < 0 ? 0 : rates[LEP2_CV_rates][cvLookupRef][cvlIndex]; currData.pGteedSurrenderValue = math.divide(math.multiply(math.bignumber(LEP2_CV_rate), math.bignumber(quotation.sumInsured)), 1000); } */ /** DEATH BENEFIT */ /**column X*/
      currData.nGteedDeathBenefit = trunc(math.max(math.max(math.subtract(math.multiply(currData.hTotalPremiumsPaidToDate, 1.01), currData._accumGteedAnnuityIncome), currData.pGteedSurrenderValue), math.bignumber(0)), 0); /**column AO Supp DEATH BENEFIT Guaranteed*/ /** currData.suppDbGtd = trunc(math.max(math.max(math.subtract(math.multiply(currData.hTotalPremiumsPaidToDate, 1.01), prevData.suppaccGtd), currData.column_AT), math.bignumber(0)),0);*/
      if (depositFile == payoutTerm && year == (premTerm + accPeriod + payoutTerm)) {
        currData.suppDbGtd = currData.column_O;
      } else {
        if ((year + 1) >= (premTerm + accPeriod + 1) && (year + 1) <= (premTerm + accPeriod + depositFile)) {
          currData.suppDbGtd = currData._accumGteedAnnuityIncome;
        } else {
          if ((year + 1) > (premTerm + accPeriod + depositFile)) {
            currData.suppDbGtd = math.subtract(currData._accumGteedAnnuityIncome, currData.suppaccGtd);
          }
        }
      } /**column AW = K*D */
      currData.column_AW = round(math.multiply(currData.column_K, math.bignumber(Su_RB_Rate)), 2); /**column AX = AW*E */
      currData.column_AX = round(math.multiply(currData.column_AW, math.bignumber(tbRate)), 2); /**column AV = AC+AO/*/
      currData.column_AV = trunc(math.add(math.divide(math.multiply(math.bignumber(gcv_rate), math.bignumber(sa)), 1000), currData.suppDbGtd), 0);
      currData.column_AP = trunc(math.max(math.max(math.add(math.subtract(math.multiply(currData.hTotalPremiumsPaidToDate, 1.01), currData._accumGteedAnnuityIncome), currData.suppDbGtd), math.bignumber(0)), currData.column_AV), 0); /**column AS= if >= Prem_Term+Acc_Period+1 = H-AO */
      if (year >= (premTerm + accPeriod + 1)) {
        currData.column_AS = math.subtract(currData.column_H, currData.suppDbGtd);
      } /**column AT*/ /** if (isHighRate) { currData.column_AT = trunc(math.add(math.divide(math.multiply( math.bignumber(gcv_rate), math.bignumber(sa) ), 1000) , currData._accumGteedAnnuityIncome),0); }else{ currData.column_AT = trunc(math.divide(math.multiply( math.bignumber(gcv_rate), math.bignumber(sa) ), 1000),0); }*/
      currData.column_AT = trunc(math.add(math.add(currData.column_AQ, currData.column_AR), currData.column_AS), 2); /**column AB*/
      currData.oTotalDeathBenefit = math.add(trunc(currData.db_nongtd, 0), trunc(currData.nGteedDeathBenefit, 0)); /** SURRENDER VALUE */ /**column AG*/
      currData.qTotalSurrenderValue = math.add(currData.pGteedSurrenderValue, currData.sv_nongtd); /**column AL*/
      currData.sEffectOfDeductions = trunc(math.subtract(currData.iVopPaidToDate, math.add(currData.qTotalSurrenderValue, currData.rAccumCashAdv)), 2);
      currData.todSv = math.add(currData.qTotalSurrenderValue, currData.rAccumCashAdv); /** if (year <= planInfo.policyTerm) { currData.rAccumCashAdv = math.add(math.add(math.multiply(prevData.rAccumCashAdv, math.add(math.divide(math.bignumber(aror), 100), 1)), currData.dGteedAnnuityIncome), currData.eNonGteedIncome); } if (year <= basicBT) { currData.sEffectOfDeductions = math.subtract(currData.iVopPaidToDate, math.add(currData.qTotalSurrenderValue, currData.rAccumCashAdv)); currData.tNegPremiumPHYieldCalc = math.multiply(currData.gPremiumsPaid, -1); currData.vInterestOnCashAdvOpt = math.subtract(math.subtract(currData.fTotalAccIncome, currData._accumGteedAnnuityIncome), currData._accumNonGteedIncome); } currData.uNegCFPHYieldCalc = math.add(currData.tNegPremiumPHYieldCalc, prevData.dGteedAnnuityIncome); currData.yHxL_AccumNGRI = math.add(prevData.yHxL_AccumNGRI, currData.eNonGteedIncome); currData._suppNonGteedInvestmentReturn = math.add(currData.vInterestOnCashAdvOpt, currData.yHxL_AccumNGRI); currData._suppGteedDeathBenefit = math.add(currData.nGteedDeathBenefit, currData.wH_SuppBIAccumOptAddGteed); currData._suppGteedSurrenderValue = math.add(currData.pGteedSurrenderValue, currData.wH_SuppBIAccumOptAddGteed); currData._suppTotalSurrenderValue = math.add(currData._suppGteedSurrenderValue, currData._suppNonGteedInvestmentReturn); if (year === retirementAge - quotation.iAge) { var gteedAnnualRetirementIncome = illustration[0].gteedAnnualRetirementIncome; gteedAnnualRetirementIncome[planInfo.covCode] = math.number(currData.dGteedAnnuityIncome); gteedAnnualRetirementIncome[planInfoTPD.covCode] = math.number(math.multiply(currData.dGteedAnnuityIncome, 5)); for (var i = 0; i < quotation.plans.length; i ++) { var plan = quotation.plans[i]; plan.gteedAnnualRetireIncome = [planInfo.covCode].indexOf(plan.covCode) >= 0 ? gteedAnnualRetirementIncome[plan.covCode] : 0; } } */ /*IRR*/ /** prepareIRR(currData, irrDataGtdPaidOut, irrDataProjectedPaidOut, irrDataProjectedAccum);*/
      if (isHighRate) { /*RetirementSolution*/
        retireSolutionData.totalPremiumsPaid = math.max(retireSolutionData.totalPremiumsPaid, currData.hTotalPremiumsPaidToDate);
        retireSolutionData.totalGuranteedRetireInc = math.add(retireSolutionData.totalGuranteedRetireInc, currData.gtdAnnual);
        retireSolutionData.totalReGtd = math.add(retireSolutionData.totalReGtd, currData.column_O);
        retireSolutionData.redepositedGtd = math.add(retireSolutionData.redepositedGtd, currData.column_P); /** Non-guaranteed Bonus inclusive of interest on Re-deposited Guaranteed Annual Income */
        if (policyTerm === (premTerm + accPeriod + depositFile)) {
          if (year === policyTerm) {
            retireSolutionData.nonGtdRedeposited = math.add(retireSolutionData.nonGtdRedeposited, trunc(currData.column_P, 0));
          }
        } else {
          if (year >= (premTerm + accPeriod + depositFile + 1)) {
            retireSolutionData.nonGtdRedeposited = math.add(retireSolutionData.nonGtdRedeposited, trunc(currData.column_P, 0));
          }
        } /** retireSolutionData.nonGuranteedRetireIncPaidOut = math.add(retireSolutionData.nonGuranteedRetireIncPaidOut, currData.eNonGteedIncome); retireSolutionData.nonGuranteedRetireIncAccum = math.max(retireSolutionData.nonGuranteedRetireIncAccum, currData.fTotalAccIncome);*/
      }
      var illData = illustration[year - 1];
      illData.policyYear = currData.aPolicyYear;
      illData.age = currData.lAttainedAgeEOY;
      illData.endOfPolicyYearAge = currData.mEndOfPolicyYearAge; /** DEATH BENEFIT */
      illData.totalPremiumPaidToDate = math.number(trunc(currData.hTotalPremiumsPaidToDate, 0));
      illData.guaranteedDeathBenefit = math.number(trunc(currData.nGteedDeathBenefit, 0));
      illData.totalDeathBenefit[aror] = math.number(currData.oTotalDeathBenefit);
      illData.nonguaranteedDeathBenefit[aror] = math.number(trunc(currData.db_nongtd, 0)); /** SURRENDER VALUE */
      illData.guranteedAnnualRetireIncPayout = math.number(trunc(currData.gtdAnnual, 0));
      illData.guranteedSvBenefit = math.number(trunc(currData.pGteedSurrenderValue, 0));
      illData.nonguranteedSvBenefit[aror] = math.number(trunc(currData.sv_nongtd, 0));
      illData.totalSurrenderValue[aror] = math.number(math.add(trunc(currData.pGteedSurrenderValue, 0), trunc(currData.sv_nongtd, 0))); /**TABLE OF DEDUCTIONS*/
      illData.valueOfPremiums[aror] = math.number(trunc(currData.iVopPaidToDate, 0));
      illData.effectOfDeduction[aror] = math.number(trunc(currData.sEffectOfDeductions, 0)); /**illData.todSurrenderValue[aror]=math.number(trunc(math.subtract(currData.iVopPaidToDate,currData.sEffectOfDeductions), 0));*/
      illData.todSurrenderValue[aror] = math.number(trunc(math.add(currData.qTotalSurrenderValue, currData.rAccumCashAdv), 0));
      if (isHighRate) { /** TDC Total Distribution Cost*/
        illData.totalDistributionCost = math.number(trunc(currData.jTotalDistributionCost, 0)); /**SUPPLEMENTARY DB Guaranteed*/
        illData.suppGteedDeathBenefit = math.number(trunc(currData.column_AP, 0)); /**SUPPLEMENTARY SURRENDER Guaranteed*/
        if (year == policyTerm) { /** Maturity Value */
          illData.suppGteedSurrenderValue = 0;
        } else {
          illData.suppGteedSurrenderValue = math.number(trunc(currData.column_AV, 0));
        }
        illData.guranteedAnnualPayout = math.number(trunc(currData.column_O, 0));
      } /** ANNUAL BENEFIT PAYOUT SCHEDULE */
      illData.nonGuranteedAnnualPayout[aror] = math.number(trunc(currData.column_P, 0));
      illData.totalAnnualPayout[aror] = math.number(trunc(currData.column_Q, 0));
      var _suppNonGteedInvestmentReturnTrunc = trunc(currData._suppNonGteedInvestmentReturn, 0);
      var suppDBNonGtd = trunc(currData.column_AT, 0); /**SUPPLEMENTARY DB */
      illData.suppNonGteedDeathBenefit[aror] = math.number(suppDBNonGtd);
      illData.suppTotalDeathBenefit[aror] = math.number(trunc(math.add(currData.column_AP, currData.column_AT), 0)); /**SUPPLEMENTARY SURRENDER */
      if (year == policyTerm) { /** Maturity Value */
        illData.suppNonGteedSurrenderValue[aror] = math.number(trunc(suppNgtd, 0));
        illData.suppTotalSurrenderValue[aror] = math.number(trunc(suppNgtd, 0));
      } else {
        var suppSURRNonGtd = trunc(math.add(math.add(currData.column_AW, currData.column_AX), currData.column_AS), 0);
        illData.suppNonGteedSurrenderValue[aror] = math.number(suppSURRNonGtd);
        illData.suppTotalSurrenderValue[aror] = math.number(math.add(trunc(currData.column_AV, 0), suppSURRNonGtd));
      }
      rawData.push(currData);
    }
    var retireSolution = illustration[0].retirementSolution;
    var paidOut = retireSolution.paidOut;
    var accumulated = retireSolution.accumulated;
    if (isHighRate) {
      paidOut.totalPremiumsPaid = math.number(trunc(retireSolutionData.totalPremiumsPaid, 2));
      paidOut.totalGuranteedRetireInc = math.number(trunc(retireSolutionData.totalGuranteedRetireInc, 0));
      paidOut.nonGuranteedRetireInc = math.number(trunc(nGtdBonus, 0));
      paidOut.totalProjectedPayout = math.number(trunc(math.add(paidOut.totalGuranteedRetireInc, paidOut.nonGuranteedRetireInc), 0));
      var vPaidOutTotalProjectedPayoutRatio = math.divide(retireSolution.paidOut.totalProjectedPayout, retireSolution.paidOut.totalPremiumsPaid);
      paidOut.totalProjectedPayoutRatio = math.number(trunc(vPaidOutTotalProjectedPayoutRatio, 2));
      var vPaidOutGuranteedRateOfReturn = runExcelFunc('IRR', irrGtd, 0.0001);
      var vPaidOutTotalRateOfReturn = runExcelFunc('IRR', irrGtd_Ngtd, 0.0001);
      paidOut.guranteedRateOfReturn = math.number(trunc(vPaidOutGuranteedRateOfReturn, 4));
      paidOut.totalRateOfReturn = math.number(trunc(vPaidOutTotalRateOfReturn, 4));
      paidOut.totalDistributionCost = math.number(round(currData.jTotalDistributionCost, 2));
      paidOut.totalDTCnoTrun = math.number(currData.jTotalDistributionCost);
      paidOut.incomPayout = math.number(trunc(incomPayout, 2));
      accumulated.totalPremiumsPaid = retireSolution.paidOut.totalPremiumsPaid;
      accumulated.totalGuranteedRetireInc = math.number(trunc(round(retireSolutionData.totalReGtd, 11), 0));
      accumulated.nonGuranteedRetireInc = math.number(trunc(math.add(retireSolutionData.nonGtdRedeposited, suppNgtd), 0));
      accumulated.totalProjectedPayout = math.number(trunc(math.add(accumulated.totalGuranteedRetireInc, accumulated.nonGuranteedRetireInc), 0));
      var vAccumTotalProjectedPayoutRatio = math.divide(retireSolution.accumulated.totalProjectedPayout, retireSolution.accumulated.totalPremiumsPaid);
      accumulated.totalProjectedPayoutRatio = math.number(trunc(vAccumTotalProjectedPayoutRatio, 2));
      var vAccumTotalRateOfReturn = runExcelFunc('IRR', irrsuppGtd_Ngtd, 0.0001);
      accumulated.totalRateOfReturn = math.number(trunc(vAccumTotalRateOfReturn, 4));
    } else {
      var vPaidOutTotalRateOfReturnLow = runExcelFunc('IRR', irrGtd_Ngtd, 0.0001);
      paidOut.totalRateOfReturnLow = math.number(trunc(vPaidOutTotalRateOfReturnLow, 4));
    }
    convertBignumberToReadable(irrDataProjectedPaidOut);
    return sustainTestResult;
  };
  var illustration = [];
  var illustrationData = {
    policyYear: 0,
    age: 0,
    endOfPolicyYearAge: "",
    /** DEATH BENEFIT */ totalPremiumPaidToDate: 0,
    guaranteedDeathBenefit: 0,
    totalDeathBenefit: {},
    nonguaranteedDeathBenefit: {},
    /** SURRENDER VALUE */ guranteedSvBenefit: 0,
    totalSurrenderValue: {},
    guranteedAnnualRetireIncPayout: 0,
    nonguranteedSvBenefit: {},
    /**TABLE OF DEDUCTIONS*/ valueOfPremiums: {},
    effectOfDeduction: {},
    todSurrenderValue: {},
    totalDistributionCost: 0,
    guranteedAnnualPayout: 0,
    nonGuranteedAnnualPayout: {},
    totalAnnualPayout: {},
    suppGteedDeathBenefit: 0,
    suppNonGteedDeathBenefit: {},
    suppTotalDeathBenefit: {},
    suppGteedSurrenderValue: 0,
    suppNonGteedSurrenderValue: {},
    suppTotalSurrenderValue: {},
    suppRiderPremiumPaid: 0,
    suppTotalDistributionCost: 0
  };
  var retirementSolution = {
    paidOut: {
      totalPremiumsPaid: 0,
      totalGuranteedRetireInc: 0,
      nonGuranteedRetireInc: 0,
      totalProjectedPayout: 0,
      totalDistributionCost: 0,
      incomPayout: 0,
      totalDTCnoTrun: 0,
      totalProjectedPayoutRatio: 0,
      guranteedRateOfReturn: 0,
      totalRateOfReturn: 0
    },
    accumulated: {
      totalPremiumsPaid: 0,
      totalGuranteedRetireInc: 0,
      nonGuranteedRetireInc: 0,
      totalProjectedPayout: 0,
      totalProjectedPayoutRatio: 0,
      totalRateOfReturn: 0
    }
  };
  var gteedAnnualRetirementIncome = {};
  for (var y = 0; y < MAX_POLICY_YEAR; y++) {
    if (y === 0) {
      illustration.push(Object.assign({}, JSON.parse(JSON.stringify(illustrationData)), {
        retirementSolution
      }, {
        gteedAnnualRetirementIncome
      }));
    } else {
      illustration.push(Object.assign({}, JSON.parse(JSON.stringify(illustrationData))));
    }
  }
  var arorKeys = Object.keys(rates.aror);
  for (var i = arorKeys.length - 1; i >= 0; i--) {
    var sustainTestResult = calcIllustration(arorKeys[i], illustration);
  }
  return illustration;
}