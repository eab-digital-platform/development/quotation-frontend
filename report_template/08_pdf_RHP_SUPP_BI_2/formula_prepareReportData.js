function(quotation, planInfo, planDetails, extraPara) {
  var illust = extraPara.illustrations[planInfo.covCode];
  var deathBenefit = [];
  var surrenderValue = [];
  var distributionCost = [];
  var retirementAge = parseInt(quotation.policyOptions.retirementAge);
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  var policyTerm = planInfo.policyTerm;
  var payoutAgeMax = 0;
  if ((payoutTerm == 15) || (payoutTerm == 20)) {
    payoutAgeMax = retirementAge + payoutTerm;
  } else {
    payoutAgeMax = 99;
  }
  var sizePlans = quotation.plans.length;
  var bpPremTerm = quotation.plans[0].premTerm;
  var maxRiderPolTerm = 0;
  var numYearsRiderLeft = 0;
  if (sizePlans > 2) { /**Only one optional rider can be added at a time.*/
    var riderPlan = quotation.plans[2];
    if (riderPlan.covCode === 'PET_RHP') {
      maxRiderPolTerm = riderPlan.policyTerm;
    }
    if (riderPlan.covCode === 'UNBN_RHP') {
      maxRiderPolTerm = riderPlan.premTerm;
    }
    if (riderPlan.covCode === 'WUN_RHP') {
      maxRiderPolTerm = riderPlan.policyTerm;
    }
    numYearsRiderLeft = maxRiderPolTerm;
  }
  var i;
  var yearCount = 0;
  var yearCountRider = 0;
  for (i = 0; i < illust.length; i++) {
    var row = illust[i];
    var policyYear = row.policyYear;
    var age = row.age;
    var policyYearAge = policyYear + ' / ' + age;
    var premium = getCurrency(Math.round(row.totalPremiumPaidToDate), '', 0);
    var incRow = false;
    var incRowRider = false;
    if ((policyYear <= policyTerm) && (age <= payoutAgeMax)) {
      if (sizePlans > 2) { /** has optional riders*/
        if (policyYear <= maxRiderPolTerm) {
          if (i < 10) {
            incRowRider = true;
            numYearsRiderLeft--;
          } else {
            if (numYearsRiderLeft >= 5) {
              yearCountRider++;
              if (yearCountRider == 5) {
                yearCountRider = 0;
                numYearsRiderLeft -= 5;
                incRowRider = true;
              }
            } else {
              if (numYearsRiderLeft > 0) {
                numYearsRiderLeft--;
                if (numYearsRiderLeft == 0) {
                  incRowRider = true;
                }
              }
            }
          }
        }
      }
      if (age < retirementAge) {
        if (i < 10) {
          incRow = true;
        } else {
          yearCount++;
          if (yearCount == 5) {
            yearCount = 0;
            incRow = true;
          }
        }
      } else {
        incRow = true;
      }
    }
    if (incRow) {
      deathBenefit.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        guaranteed: getCurrency(Math.round(row.suppGteedDeathBenefit), '', 0),
        nonGuaranteedLow: getCurrency(Math.round(row.suppNonGteedDeathBenefit['3.25']), '', 0),
        totalLow: getCurrency(Math.round(row.suppTotalDeathBenefit['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(Math.round(row.suppNonGteedDeathBenefit['4.75']), '', 0),
        totalHigh: getCurrency(Math.round(row.suppTotalDeathBenefit['4.75']), '', 0),
      });
      surrenderValue.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: premium,
        retireIncPayout: getCurrency(Math.round(row.guranteedAnnualRetireIncPayout), '', 0),
        guaranteed: getCurrency(Math.round(row.suppGteedSurrenderValue), '', 0),
        nonGuaranteedLow: getCurrency(Math.round(row.suppNonGteedSurrenderValue['3.25']), '', 0),
        totalLow: getCurrency(Math.round(row.suppTotalSurrenderValue['3.25']), '', 0),
        nonGuaranteedHigh: getCurrency(Math.round(row.suppNonGteedSurrenderValue['4.75']), '', 0),
        totalHigh: getCurrency(Math.round(row.suppTotalSurrenderValue['4.75']), '', 0)
      });
    }
    if (incRowRider) {
      distributionCost.push({
        policyYearAge: policyYearAge,
        premiumsPaidToDate: getCurrency(Math.round(row.suppRiderPremiumPaid), '', 0),
        distributionCost: getCurrency(Math.round(row.suppTotalDistributionCost), '', 0)
      });
    }
  }
  var result = {
    illustrationSup: {
      deathBenefit: deathBenefit,
      surrenderValue: surrenderValue,
      distributionCost: distributionCost
    }
  };
  return result;
}