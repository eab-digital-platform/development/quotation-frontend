function(quotation, planDetail) {
  var oCls = quotation.plans[0].covClass;
  if (oCls) {
    oCls = parseInt(oCls, 10);
    if (oCls) return oCls - 1;
  }
  return 0;
}