function(planDetail, quotation, planDetails) {
  var bsa = math.number(quotation.plans[0].sumInsured || 0);
  var options = [],
    valOpts = [];
  for (var i = planDetail.classList.length - 1; i >= 0; i--) {
    var classOpt = planDetail.classList[i];
    var classOptVal = math.number(classOpt.covClass || 0) / 100;
    if (bsa * classOptVal >= 6000 && bsa * classOptVal <= 2000000) {
      valOpts.push(classOpt.covClass);
      options.push({
        value: classOpt.covClass,
        title: classOpt.className
      });
    }
  }
  for (var i in quotation.plans) {
    var plan = quotation.plans[i];
    if (plan.covCode == planDetail.covCode) {
      if (!options.length || (plan.covClass && valOpts.indexOf(plan.covClass) == -1)) {
        plan.covClass = null;
        plan.sumInsured = null;
      }
    }
  }
  planDetail.inputConfig.canEditClassType = true;
  planDetail.inputConfig.hasClass = true;
  planDetail.inputConfig.classTitle = planDetail.classTitle;
  planDetail.inputConfig.classTypeList = options;
}