function(quotation, planInfo, planDetail) {
  var trunc = function(value, position) {
    return runFunc(planDetail.formulas.trunc, value, position);
  };
  var sumInsured = trunc(math.bignumber(planInfo.sumInsured), 2);
  var iGender = quotation.iGender;
  var iSmoke = quotation.iSmoke;
  var iAge = quotation.iAge;
  var polTerm = quotation.plans[0].policyTerm;
  var Raw_PremiumRate = 0;
  if (polTerm) {
    Raw_PremiumRate = planDetail.rates.premRate[iGender + (iSmoke == 'N' ? 'N' : 'S')][polTerm + 'PT'][iAge];
  }
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    if (plan.covCode == planInfo.covCode) {
      plan.premRate = math.number(Raw_PremiumRate);
      break;
    }
  }
  Raw_PremiumRate = math.bignumber(Raw_PremiumRate);
  var r1_discount = math.bignumber(0);
  var annualPrem = math.number(trunc(math.multiply(math.multiply(Raw_PremiumRate, math.subtract(math.bignumber(1), r1_discount)), math.divide(sumInsured, math.bignumber(100))), 2));
  return math.number(annualPrem);
}