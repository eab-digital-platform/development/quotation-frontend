function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    return runFunc(planDetails[planInfo.covCode].formulas.trunc, value, position);
  };
  var annual_prem = runFunc(planDetails[planInfo.covCode].formulas.getPremium, quotation, planInfo, planDetails[planInfo.covCode]);
  annual_prem = math.bignumber(annual_prem);
  var oldAnnaulPrem = math.bignumber(0);
  var oldTDC = math.bignumber(0);
  var basicPolTerm = quotation.plans[0].policyTerm;
  var illustration = [];
  for (var j = 0; j < basicPolTerm; j++) {
    var row = {};
    var totalDistribCost = math.bignumber(0);
    var premTerm = quotation.plans[0].policyTerm;
    var isPolYrBiggerPremTerm = (j <= premTerm - 1) ? 1 : 0;
    isPolYrBiggerPremTerm = math.bignumber(isPolYrBiggerPremTerm);
    var annaulPrem = math.bignumber(math.add(math.multiply(annual_prem, isPolYrBiggerPremTerm), oldAnnaulPrem));
    var tdc = '';
    if (quotation.agent.dealerGroup == 'AGENCY' || quotation.agent.dealerGroup == 'SYNERGY') tdc = 'AGY_SYN';
    else if (quotation.agent.dealerGroup == 'BROKER' || quotation.agent.dealerGroup == 'FUSION') tdc = 'BKR_FUS';
    else if (quotation.agent.dealerGroup == 'SINGPOST') tdc = 'SINGPOST';
    else if (quotation.agent.dealerGroup == 'DIRECT') tdc = 'DIRECT';
    var rates = planDetails[planInfo.covCode].rates;
    var tbl_TDC = rates.TDC[tdc];
    var oldDiff = 999;
    var targetPolYr = -1;
    var polTerm = basicPolTerm;
    for (var key in tbl_TDC) {
      var diff = polTerm - key;
      if (diff >= 0 && diff < oldDiff) {
        targetPolYr = key;
        oldDiff = diff;
      }
    }
    var TDCRate = math.bignumber(tbl_TDC[targetPolYr][Math.min(5, j)]);
    var TDC_Factor = math.bignumber(1);
    totalDistribCost = math.bignumber(math.add((j > 5) ? 0 : math.multiply(trunc(math.multiply(math.multiply(TDCRate, annual_prem), TDC_Factor), 2), isPolYrBiggerPremTerm), oldTDC));
    oldAnnaulPrem = annaulPrem;
    oldTDC = totalDistribCost;
    row.annaulPrem = math.number(annaulPrem);
    row.totalDistribCost = math.number(totalDistribCost);
    illustration.push(row);
  }
  return illustration;
}