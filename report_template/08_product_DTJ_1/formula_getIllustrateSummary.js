function(quotation, planDetails, extraPara, illustrations) {
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var basicIllust = illustrations[quotation.baseProductCode];
  var maxAge = quotation.iAge + parseInt(quotation.plans[0].policyTerm);
  var maxIndex = basicIllust.findIndex(function(item) {
    return item.age === maxAge;
  });
  for (var i = basicIllust.length - 1; i > maxIndex; i--) {
    basicIllust.splice(i, 1);
  } /** for(var j=0;j<quotation.plans.length;j++){ if(quotation.plans[j].covCode !== 'DTJ'){ var riderIllust = illustrations[quotation.plans[j].covCode]; for (var i = 0; i < basicIllust.length; i ++) { var basicIllData = basicIllust[i]; var riderIllData = riderIllust[i]; basicIllData.suppRiderPremiumPaid = math.number(basicIllData.suppRiderPremiumPaid) + math.number(riderIllData.accumPremium); basicIllData.suppTotalDistributionCost = math.number(basicIllData.suppTotalDistributionCost) + math.number(riderIllData.tdc); } } } for (var k = 0; k < basicIllust.length; k ++) { var basicIllData = basicIllust[k]; basicIllData.suppRiderPremiumPaid = math.number(round(basicIllData.suppRiderPremiumPaid,0)); basicIllData.suppTotalDistributionCost = math.number(round(basicIllData.suppTotalDistributionCost,0)); } */
  return illustrations;
}