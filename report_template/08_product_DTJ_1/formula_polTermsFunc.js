function(planDetail, quotation) {
  var policyTermsList = []; /** var resetInputs = function(){ quotation.plans[0].premTerm = undefined; quotation.plans[0].sumAssred = undefined; quotation.prevPaymentMode = quotation.paymentMode; }; if (quotation.prevPaymentMode){ if (quotation.prevPaymentMode === 'L'){ if (quotation.paymentMode !== 'L'){ resetInputs(); } } else{ if (quotation.paymentMode === 'L'){ resetInputs(); } } } */
  var maxLage = quotation.iAge;
  if (maxLage < quotation.pAge) {
    maxLage = quotation.pAge;
  }
  if (quotation.paymentMode == 'L') {
    _.forEach([5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], (policyTerm) => {
      if (maxLage + policyTerm <= 70) {
        policyTermsList.push({
          value: policyTerm,
          title: policyTerm + ' Years'
        });
      }
    });
  } else {
    _.forEach([10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], (policyTerm) => {
      if (maxLage + policyTerm <= 70) {
        policyTermsList.push({
          value: policyTerm,
          title: policyTerm + ' Years'
        });
      }
    });
  }
  if (quotation.policyOptions.interRate === undefined) {
    planDetail.polTermInd = 'N';
    planDetail.saInputInd = 'N';
  } else {
    planDetail.polTermInd = 'Y';
    planDetail.saInputInd = 'Y';
  }
  return policyTermsList;
}