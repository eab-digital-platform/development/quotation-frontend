function(planDetail, quotation) {
  var policyTermList = null;
  var foundDefault = false;
  if (planDetail.polMaturities) {
    policyTermList = [];
    for (var p in planDetail.polMaturities) {
      var mat = planDetail.polMaturities[p];
      if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
        var interval = mat.interval || 1;
        for (var m = mat.minTerm; m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || (m + quotation.iAge) <= mat.maxAge) && (!mat.ownerMaxAge || !quotation.pAge || (m + quotation.pAge) <= mat.ownerMaxAge); m += interval) {
          foundDefault = foundDefault || (mat.default === m);
          policyTermList.push({
            "value": m + "",
            "title": m + (quotation.iAge ? "(@" + (quotation.iAge + m) + ")" : ""),
            "default": mat.default === m
          });
        }
        if (mat.default === 0) {
          policyTermList[0].default = true;
        } else if (mat.default === 999 || !foundDefault) {
          policyTermList[policyTermList.length - 1].default = true;
        }
      }
    }
  } else if (planDetail.wholeLifeInd && planDetail.wholeLifeInd == 'Y' && planDetail.wholeLifeAge) {
    quotation.plans[0].policyTerm = "999";
    return [{
      "value": "999",
      "title": "To Age " + planDetail.wholeLifeAge,
      "default": true
    }];
  }
  return policyTermList;
}