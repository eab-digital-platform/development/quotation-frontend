function(value, min, max, factor) {
  var numValue = math.number(value),
    minValue = math.number(min),
    maxValue = math.number(max);
  if (isNaN(numValue)) {
    return value;
  }
  if (min !== null && !isNaN(minValue) && numValue < minValue) {
    return minValue;
  }
  if (max !== null && !isNaN(maxValue) && numValue > maxValue) {
    return maxValue;
  }
  if (value % factor !== 0) {
    return math.number(math.multiply(math.floor(math.divide(math.bignumber(value), factor)), factor));
  }
  return value;
}