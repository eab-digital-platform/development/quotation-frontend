function(quotation, planInfo, planDetails) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var petRates = rates.premRate;
  var interRate = parseInt(quotation.policyOptions.interRate);
  var sumSa = quotation.plans[0].sumInsured;
  if (interRate < 10) {
    interRate = "0" + interRate;
  }
  var plancode = planInfo.policyTerm + "DLZ" + interRate;
  var ref = "LAR" + interRate + quotation.iGender + quotation.iSmoke + quotation.iAge;
  var getRate = function(ref, columnNum, Rates) {
    var planRates = Rates[ref];
    if (planRates === null) {
      return 0;
    }
    return planRates[columnNum];
  };
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  if (sumSa > 2000000) {
    quotDriver.context.addError({
      covCode: planInfo.covCode,
      msg: 'Living Accelerator for Decreasing Term is no longer available.'
    });
  }

  function calculateRiderPremium(curPlanInfo) {
    var annualPremium = 0;
    var premRate = getRate(ref, (planInfo.policyTerm - 10), petRates);
    var lsd = 0.15;
    if (sumSa < 200000) {
      lsd = -0.6;
    } else if (sumSa < 500000) {
      lsd = 0;
    }
    var np = math.subtract(math.bignumber(premRate), math.bignumber(lsd));
    annualPremium = math.divide(math.multiply(math.bignumber(np), sumSa), math.bignumber(1000));
    annualPremium = round(annualPremium, 2);
    curPlanInfo.yearPrem = math.number(annualPremium);
    curPlanInfo.halfYearPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.51)), 2)); /** Math.floor(annualPremium * 0.51 * 100)/100; */
    curPlanInfo.quarterPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.26)), 2)); /** Math.floor(annualPremium * 0.26 * 100)/100;*/
    curPlanInfo.monthPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.0875)), 2)); /** Math.floor(annualPremium * 0.0875 * 100)/100;*/
    quotation.totYearPrem += curPlanInfo.yearPrem;
    quotation.totHalfyearPrem += curPlanInfo.halfYearPrem;
    quotation.totQuarterPrem += curPlanInfo.quarterPrem;
    quotation.totMonthPrem += curPlanInfo.monthPrem;
    switch (quotation.paymentMode) {
      case 'A':
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
      case 'S':
        {
          curPlanInfo.premium = curPlanInfo.halfYearPrem;quotation.premium += curPlanInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          curPlanInfo.premium = curPlanInfo.quarterPrem;quotation.premium += curPlanInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          curPlanInfo.premium = curPlanInfo.monthPrem;quotation.premium += curPlanInfo.monthPrem;
          break;
        }
      default:
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
    }
    curPlanInfo.planCode = plancode;
  }
  calculateRiderPremium(planInfo);
}