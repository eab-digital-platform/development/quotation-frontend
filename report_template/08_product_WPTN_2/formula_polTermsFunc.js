function(planDetail, quotation) {
  var opts = [],
    polTerm = 5,
    pAge = quotation.pAge;
  while (polTerm <= 30 && pAge + polTerm <= 70) {
    opts.push({
      value: "yrs" + polTerm,
      title: polTerm + " Years"
    });
    polTerm++;
  }
  return opts;
}