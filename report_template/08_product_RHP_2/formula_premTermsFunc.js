function(planDetail, quotation) {
  var premTermsList = [];
  var resetInputs = function() {
    quotation.plans[0].premTerm = undefined; /** force UI to be blank*/
    quotation.plans[0].retirementIncome = undefined; /** force UI to be blank*/
    quotation.plans[0].premium = undefined; /** force UI to be blank*/
    quotation.prevPaymentMode = quotation.paymentMode;
  };
  if (quotation.prevPaymentMode) { /** Change from RP to SP or vice versa*/
    if (quotation.prevPaymentMode === 'L') {
      if (quotation.paymentMode !== 'L') {
        resetInputs();
      }
    } else {
      if (quotation.paymentMode === 'L') {
        resetInputs();
      }
    }
  }
  if (quotation.paymentMode == 'L') {
    if (quotation.plans[0].premTerm) {
      premTermsList.push({
        value: 1,
        title: 'Single Premium',
        default: true
      });
    } else {
      premTermsList.push({
        value: 1,
        title: 'Single Premium'
      });
    }
  } else {
    var retirementAge = quotation.policyOptions.retirementAge;
    _.forEach([5, 10, 15, 20, 25], (paymentTerm) => {
      if (retirementAge >= quotation.iAge + paymentTerm + 5) {
        premTermsList.push({
          value: paymentTerm,
          title: paymentTerm + ' Years'
        });
      }
    });
  }
  return premTermsList;
}