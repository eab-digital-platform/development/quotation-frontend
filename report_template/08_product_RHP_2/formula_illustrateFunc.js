function(quotation, planInfo, planDetails, extraPara) {
  var planDetail = planDetails[planInfo.covCode];
  var planInfoTPD = quotation.plans.find(function(p) {
    return p.covCode === 'TPD';
  });
  var rates = planDetail.rates;
  var dealerGroup = quotation.agent.dealerGroup;
  var payoutType = quotation.policyOptions.payoutType;
  var retirementAge = parseInt(quotation.policyOptions.retirementAge);
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  const MAX_POLICY_YEAR = 100;
  const TOTAL_ACC_INCOME_RATE = 0.03;
  var getChannel = function() {
    var mappedChannel = dealerGroup;
    if (dealerGroup === 'SYNERGY') {
      mappedChannel = 'AGENCY';
    } else if (dealerGroup === 'FUSION') {
      mappedChannel = 'BROKER';
    }
    return mappedChannel;
  };
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var roundup = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.ceil(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var convertBignumberToReadable = function(data) { /* for (var row in data) { for (var col in data[row]) { if (typeof(data[row][col]) !== 'string') { data[row][col] = math.number(data[row][col]); } } } console.log('rawData', data); */ };
  var prepareIRR = function(currData, irrDataGtdPaidOut, irrDataProjectedPaidOut, irrDataProjectedAccum) {
    var negYearPrem = math.subtract(math.multiply(math.bignumber(planInfo.yearPrem), -1), math.bignumber(planInfoTPD.yearPrem));
    if (currData.lAttainedAgeEOY >= retirementAge) {
      currData._irrGtdCashFlowPaidOut = currData.dGteedAnnuityIncome;
      currData._irrProjectedCashFlowPaidOut = math.add(currData.dGteedAnnuityIncome, currData.eNonGteedIncome);
    } else if (currData.aPolicyYear < planInfo.premTerm) {
      currData._irrGtdCashFlowPaidOut = negYearPrem;
      currData._irrProjectedCashFlowPaidOut = negYearPrem;
    }
    if (currData.aPolicyYear < planInfo.premTerm) {
      currData._irrGtdCashFlowAccum = negYearPrem;
      currData._irrProjectedCashFlowAccum = negYearPrem;
    } else if (currData.aPolicyYear === basicBT) {
      currData._irrGtdCashFlowAccum = trunc(currData._suppGteedSurrenderValue, 0);
      currData._irrProjectedCashFlowAccum = trunc(currData._suppTotalSurrenderValue, 0);
    }
    irrDataGtdPaidOut.push(currData._irrGtdCashFlowPaidOut);
    irrDataProjectedPaidOut.push(currData._irrProjectedCashFlowPaidOut);
    irrDataProjectedAccum.push(currData._irrProjectedCashFlowAccum);
  };
  var channel = getChannel();
  var ageIndex = rates.LEP2_GRI_rates.age.indexOf(quotation.iAge);
  var LEP2_GRI_rate = math.bignumber(ageIndex >= 0 ? rates.LEP2_GRI_rates[planInfo.planCode][ageIndex] : 0);
  var basicBT = retirementAge + (payoutTerm === 99 ? 99 - retirementAge : payoutTerm) - quotation.iAge;
  var cashAdvanceFactor = rates.cashAdvance.factor[payoutType];
  var cashAdvanceIndexation = rates.cashAdvance.indexation[payoutType];
  var commissionTable = rates.commission[channel];
  var cvLookupRef = planInfo.planCode + quotation.iGender + quotation.iAge;
  var calcIllustration = function(arorkey, illustration) {
    var sustainTestResult = true;
    var aror = rates.aror[arorkey];
    var isHighRate = aror * 100 === 475;
    var rawData = [];
    var irrDataGtdPaidOut = [];
    var irrDataProjectedPaidOut = [];
    var irrDataProjectedAccum = [];
    var retireSolutionData = {
      totalPremiumsPaid: math.bignumber(0),
      totalGuranteedRetireInc: math.bignumber(0),
      nonGuranteedRetireIncPaidOut: math.bignumber(0),
      nonGuranteedRetireIncAccum: math.bignumber(0)
    };
    var defaultRawData = {
      aPolicyYear: 0,
      bAnnuityIncomeFactor: math.bignumber(0),
      cIndexationFactor: math.bignumber(0),
      dGteedAnnuityIncome: math.bignumber(0),
      eNonGteedIncome: math.bignumber(0),
      fTotalAccIncome: math.bignumber(0),
      gPremiumsPaid: math.bignumber(0),
      hTotalPremiumsPaidToDate: math.bignumber(0),
      iVopPaidToDate: math.bignumber(0),
      jTotalDistributionCost: math.bignumber(0),
      lAttainedAgeEOY: quotation.iAge,
      mEndOfPolicyYearAge: "",
      nGteedDeathBenefit: math.bignumber(0),
      oTotalDeathBenefit: math.bignumber(0),
      pGteedSurrenderValue: math.bignumber(0),
      qTotalSurrenderValue: math.bignumber(0),
      rAccumCashAdv: math.bignumber(0),
      sEffectOfDeductions: math.bignumber(0),
      tNegPremiumPHYieldCalc: math.bignumber(0),
      uNegCFPHYieldCalc: math.bignumber(0),
      vInterestOnCashAdvOpt: math.bignumber(0),
      wH_SuppBIAccumOptAddGteed: math.bignumber(0),
      xHwL_PremiumPaidTPD: math.bignumber(0),
      yHxL_AccumNGRI: math.bignumber(0),
      _commission: 0,
      _accumGteedAnnuityIncome: math.bignumber(0),
      _accumNonGteedIncome: math.bignumber(0),
      _suppGteedDeathBenefit: math.bignumber(0),
      _suppGteedSurrenderValue: math.bignumber(0),
      _suppNonGteedInvestmentReturn: math.bignumber(0),
      _suppTotalSurrenderValue: math.bignumber(0),
      _irrGtdCashFlowPaidOut: math.bignumber(0),
      _irrProjectedCashFlowPaidOut: math.bignumber(0),
      _irrGtdCashFlowAccum: math.bignumber(0),
      _irrProjectedCashFlowAccum: math.bignumber(0)
    };
    rawData.push(Object.assign({}, defaultRawData, {
      benefitTerm: basicBT
    }));
    prepareIRR(rawData[0], irrDataGtdPaidOut, irrDataProjectedPaidOut, irrDataProjectedAccum);
    var totalDistributionCost;
    for (var year = 1; year <= MAX_POLICY_YEAR; year++) {
      var currData = Object.assign({}, defaultRawData);
      var prevData = rawData[year - 1];
      currData.aPolicyYear = year;
      if (year + quotation.iAge < retirementAge || year > (payoutTerm === 99 ? basicBT : basicBT - 1)) {
        currData.bAnnuityIncomeFactor = math.bignumber(0);
      } else {
        var aTemp = math.pow(math.add(math.bignumber(1), math.bignumber(cashAdvanceIndexation)), math.bignumber(year + quotation.iAge - retirementAge));
        currData.bAnnuityIncomeFactor = math.multiply(aTemp, math.divide(LEP2_GRI_rate, math.bignumber(100)));
      }
      currData.cIndexationFactor = roundup(round(currData.bAnnuityIncomeFactor, 10), 4);
      currData.dGteedAnnuityIncome = math.multiply(currData.cIndexationFactor, quotation.sumInsured);
      currData._accumGteedAnnuityIncome = math.add(prevData._accumGteedAnnuityIncome, currData.dGteedAnnuityIncome);
      var LEP2_NGRI = rates.LEP2_NGRI[aror * 100];
      ageIndex = LEP2_NGRI.age.indexOf(quotation.iAge);
      var LEP2_NGRI_rate = math.bignumber(ageIndex >= 0 ? LEP2_NGRI[planInfo.planCode][ageIndex] : 0);
      if (math.isZero(currData.dGteedAnnuityIncome) || currData.aPolicyYear > basicBT) {
        LEP2_NGRI_rate = math.bignumber(0);
      }
      currData.eNonGteedIncome = round(math.multiply(LEP2_NGRI_rate, currData.dGteedAnnuityIncome), 2);
      currData._accumNonGteedIncome = math.add(prevData._accumNonGteedIncome, currData.eNonGteedIncome);
      if (year <= planInfo.premTerm) {
        currData.gPremiumsPaid = math.bignumber(planInfo.yearPrem);
      }
      if (year <= planInfoTPD.premTerm) {
        currData.xHwL_PremiumPaidTPD = math.bignumber(planInfoTPD.yearPrem);
      }
      if (year <= basicBT) {
        currData.fTotalAccIncome = math.add(math.add(math.multiply(prevData.fTotalAccIncome, 1 + TOTAL_ACC_INCOME_RATE), currData.dGteedAnnuityIncome), currData.eNonGteedIncome);
        var currYearPremiumPaid = math.add(currData.gPremiumsPaid, currData.xHwL_PremiumPaidTPD);
        currData.hTotalPremiumsPaidToDate = !math.isZero(currYearPremiumPaid) ? math.add(prevData.hTotalPremiumsPaidToDate, currYearPremiumPaid) : prevData.hTotalPremiumsPaidToDate;
        currData.iVopPaidToDate = trunc(math.multiply(math.add(currYearPremiumPaid, prevData.iVopPaidToDate), math.add(math.divide(math.bignumber(aror), 100), 1)), 2);
      }
      var commissionColIndex = commissionTable.policyYear.indexOf(year);
      if (commissionColIndex < 0) {
        commissionColIndex = commissionTable.policyYear.length - 1;
      }
      currData._commission = trunc(math.bignumber(commissionTable[planInfo.premTerm][commissionColIndex]), 4);
      currData.jTotalDistributionCost = math.add(math.multiply(math.add(math.bignumber(planInfoTPD.yearPrem), math.bignumber(planInfo.yearPrem)), currData._commission), prevData.jTotalDistributionCost);
      currData.lAttainedAgeEOY = quotation.iAge + year;
      currData.mEndOfPolicyYearAge = currData.aPolicyYear + '/' + (currData.lAttainedAgeEOY);
      if (year <= basicBT) {
        var cvl = 'CVL' + (year < 10 ? '0' : '') + year;
        var LEP2_CV_rates = 'LEP2_CV_rates_' + quotation.iGender;
        var cvlIndex = rates[LEP2_CV_rates].Ref.indexOf(cvl);
        var LEP2_CV_rate = cvlIndex < 0 ? 0 : rates[LEP2_CV_rates][cvLookupRef][cvlIndex];
        currData.pGteedSurrenderValue = math.divide(math.multiply(math.bignumber(LEP2_CV_rate), math.bignumber(quotation.sumInsured)), 1000);
      }
      currData.nGteedDeathBenefit = math.max(math.max(math.subtract(math.multiply(currData.hTotalPremiumsPaidToDate, 1.01), prevData._accumGteedAnnuityIncome), currData.pGteedSurrenderValue), math.bignumber(0));
      currData.oTotalDeathBenefit = trunc(currData.nGteedDeathBenefit, 0);
      currData.qTotalSurrenderValue = trunc(currData.pGteedSurrenderValue, 2);
      if (year <= planInfo.policyTerm) {
        currData.rAccumCashAdv = math.add(math.add(math.multiply(prevData.rAccumCashAdv, math.add(math.divide(math.bignumber(aror), 100), 1)), currData.dGteedAnnuityIncome), currData.eNonGteedIncome);
      }
      if (year <= basicBT) {
        currData.sEffectOfDeductions = math.subtract(currData.iVopPaidToDate, math.add(currData.qTotalSurrenderValue, currData.rAccumCashAdv));
        currData.tNegPremiumPHYieldCalc = math.multiply(currData.gPremiumsPaid, -1);
        currData.vInterestOnCashAdvOpt = math.subtract(math.subtract(currData.fTotalAccIncome, currData._accumGteedAnnuityIncome), currData._accumNonGteedIncome);
        currData.wH_SuppBIAccumOptAddGteed = prevData._accumGteedAnnuityIncome;
      }
      currData.uNegCFPHYieldCalc = math.add(currData.tNegPremiumPHYieldCalc, prevData.dGteedAnnuityIncome);
      currData.yHxL_AccumNGRI = math.add(prevData.yHxL_AccumNGRI, currData.eNonGteedIncome);
      currData._suppNonGteedInvestmentReturn = math.add(currData.vInterestOnCashAdvOpt, currData.yHxL_AccumNGRI);
      currData._suppGteedDeathBenefit = math.add(currData.nGteedDeathBenefit, currData.wH_SuppBIAccumOptAddGteed);
      currData._suppGteedSurrenderValue = math.add(currData.pGteedSurrenderValue, currData.wH_SuppBIAccumOptAddGteed);
      currData._suppTotalSurrenderValue = math.add(currData._suppGteedSurrenderValue, currData._suppNonGteedInvestmentReturn);
      if (year === retirementAge - quotation.iAge) {
        var gteedAnnualRetirementIncome = illustration[0].gteedAnnualRetirementIncome;
        gteedAnnualRetirementIncome[planInfo.covCode] = math.number(currData.dGteedAnnuityIncome);
        gteedAnnualRetirementIncome[planInfoTPD.covCode] = math.number(math.multiply(currData.dGteedAnnuityIncome, 5));
        for (var i = 0; i < quotation.plans.length; i++) {
          var plan = quotation.plans[i];
          plan.gteedAnnualRetireIncome = [planInfo.covCode, planInfoTPD.covCode].indexOf(plan.covCode) >= 0 ? gteedAnnualRetirementIncome[plan.covCode] : 0;
        }
      } /*IRR*/
      prepareIRR(currData, irrDataGtdPaidOut, irrDataProjectedPaidOut, irrDataProjectedAccum);
      if (isHighRate) { /*RetirementSolution*/
        retireSolutionData.totalPremiumsPaid = math.max(retireSolutionData.totalPremiumsPaid, currData.hTotalPremiumsPaidToDate);
        retireSolutionData.totalGuranteedRetireInc = math.add(retireSolutionData.totalGuranteedRetireInc, currData.dGteedAnnuityIncome);
        retireSolutionData.nonGuranteedRetireIncPaidOut = math.add(retireSolutionData.nonGuranteedRetireIncPaidOut, currData.eNonGteedIncome);
        retireSolutionData.nonGuranteedRetireIncAccum = math.max(retireSolutionData.nonGuranteedRetireIncAccum, currData.fTotalAccIncome);
      }
      var illData = illustration[year - 1];
      illData.policyYear = currData.aPolicyYear;
      illData.age = currData.lAttainedAgeEOY;
      illData.endOfPolicyYearAge = currData.mEndOfPolicyYearAge;
      illData.totalPremiumPaidToDate = math.number(trunc(currData.hTotalPremiumsPaidToDate, 0));
      illData.guaranteedDeathBenefit = math.number(trunc(currData.nGteedDeathBenefit, 0));
      illData.totalDeathBenefit[aror] = math.number(trunc(currData.oTotalDeathBenefit, 0));
      illData.guranteedAnnualRetireIncPayout = math.number(round(round(currData.dGteedAnnuityIncome, 10), 0));
      illData.guranteedSvBenefit = math.number(trunc(currData.pGteedSurrenderValue, 0));
      illData.totalSurrenderValue[aror] = math.number(trunc(currData.qTotalSurrenderValue, 0));
      illData.valueOfPremiums[aror] = math.number(trunc(currData.iVopPaidToDate, 0));
      illData.effectOfDeduction[aror] = math.number(trunc(currData.sEffectOfDeductions, 0));
      illData.nonGuranteedAnnualRetireInc[aror] = math.number(round(round(currData.eNonGteedIncome, 10), 0));
      illData.totalAnnualRetireInc[aror] = math.number(trunc(math.add(illData.guranteedAnnualRetireIncPayout, illData.nonGuranteedAnnualRetireInc[aror]), 0));
      if (isHighRate) {
        illData.totalDistributionCost = math.number(trunc(currData.jTotalDistributionCost, 0));
        illData.suppGteedDeathBenefit = math.number(trunc(currData._suppGteedDeathBenefit, 0));
        illData.suppGteedSurrenderValue = math.number(trunc(currData._suppGteedSurrenderValue, 0));
        if (year == MAX_POLICY_YEAR) {
          totalDistributionCost = illData.totalDistributionCost;
        }
      }
      var _suppNonGteedInvestmentReturnTrunc = trunc(currData._suppNonGteedInvestmentReturn, 0);
      illData.suppNonGteedDeathBenefit[aror] = math.number(_suppNonGteedInvestmentReturnTrunc);
      illData.suppNonGteedSurrenderValue[aror] = math.number(_suppNonGteedInvestmentReturnTrunc);
      illData.suppTotalDeathBenefit[aror] = math.number(math.add(trunc(currData._suppGteedDeathBenefit, 0), _suppNonGteedInvestmentReturnTrunc));
      illData.suppTotalSurrenderValue[aror] = math.number(math.add(trunc(currData._suppGteedSurrenderValue, 0), _suppNonGteedInvestmentReturnTrunc));
      rawData.push(currData);
    }
    var retireSolution = illustration[0].retirementSolution;
    var paidOut = retireSolution.paidOut;
    var accumulated = retireSolution.accumulated;
    if (isHighRate) {
      paidOut.totalPremiumsPaid = math.number(round(retireSolutionData.totalPremiumsPaid, 2));
      paidOut.totalGuranteedRetireInc = math.number(round(retireSolutionData.totalGuranteedRetireInc, 2));
      paidOut.nonGuranteedRetireInc = math.number(round(retireSolutionData.nonGuranteedRetireIncPaidOut, 2));
      paidOut.totalProjectedPayout = math.number(round(math.add(retireSolutionData.totalGuranteedRetireInc, retireSolutionData.nonGuranteedRetireIncPaidOut), 2));
      var vPaidOutTotalProjectedPayoutRatio = math.divide(retireSolution.paidOut.totalProjectedPayout, retireSolution.paidOut.totalPremiumsPaid);
      var vPaidOutGuranteedRateOfReturn = runExcelFunc('IRR', irrDataGtdPaidOut, 0.0001);
      var vPaidOutTotalRateOfReturn = runExcelFunc('IRR', irrDataProjectedPaidOut, 0.0001);
      paidOut.totalProjectedPayoutRatio = math.number(trunc(vPaidOutTotalProjectedPayoutRatio, 2));
      paidOut.guranteedRateOfReturn = math.number(trunc(vPaidOutGuranteedRateOfReturn, 4));
      paidOut.totalRateOfReturn = math.number(trunc(vPaidOutTotalRateOfReturn, 4));
      paidOut.totalDistributionCost = math.number(round(totalDistributionCost, 2));
      accumulated.totalPremiumsPaid = retireSolution.paidOut.totalPremiumsPaid;
      accumulated.totalGuranteedRetireInc = retireSolution.paidOut.totalGuranteedRetireInc;
      accumulated.nonGuranteedRetireInc = math.number(round(math.subtract(retireSolutionData.nonGuranteedRetireIncAccum, retireSolution.paidOut.totalGuranteedRetireInc), 2));
      accumulated.totalProjectedPayout = math.number(round(math.add(retireSolution.accumulated.totalGuranteedRetireInc, retireSolution.accumulated.nonGuranteedRetireInc), 2));
      var vAccumTotalProjectedPayoutRatio = math.divide(retireSolution.accumulated.totalProjectedPayout, retireSolution.accumulated.totalPremiumsPaid);
      var vAccumTotalRateOfReturn = runExcelFunc('IRR', irrDataProjectedAccum, 0.0001);
      accumulated.totalProjectedPayoutRatio = math.number(trunc(vAccumTotalProjectedPayoutRatio, 2));
      accumulated.totalRateOfReturn = math.number(trunc(vAccumTotalRateOfReturn, 4));
    } else {
      var vPaidOutTotalRateOfReturnLow = runExcelFunc('IRR', irrDataProjectedPaidOut, 0.0001);
      paidOut.totalRateOfReturnLow = math.number(trunc(vPaidOutTotalRateOfReturnLow, 4));
    }
    convertBignumberToReadable(irrDataProjectedPaidOut);
    return sustainTestResult;
  };
  var illustration = [];
  var illustrationData = {
    policyYear: 0,
    age: 0,
    endOfPolicyYearAge: "",
    totalPremiumPaidToDate: 0,
    guaranteedDeathBenefit: 0,
    totalDeathBenefit: {},
    guranteedSvBenefit: 0,
    totalSurrenderValue: {},
    guranteedAnnualRetireIncPayout: 0,
    valueOfPremiums: {},
    effectOfDeduction: {},
    totalDistributionCost: 0,
    nonGuranteedAnnualRetireInc: {},
    totalAnnualRetireInc: {},
    suppGteedDeathBenefit: 0,
    suppNonGteedDeathBenefit: {},
    suppTotalDeathBenefit: {},
    suppGteedSurrenderValue: 0,
    suppNonGteedSurrenderValue: {},
    suppTotalSurrenderValue: {},
    suppRiderPremiumPaid: 0,
    suppTotalDistributionCost: 0
  };
  var retirementSolution = {
    paidOut: {
      totalPremiumsPaid: 0,
      totalGuranteedRetireInc: 0,
      nonGuranteedRetireInc: 0,
      totalProjectedPayout: 0,
      totalProjectedPayoutRatio: 0,
      guranteedRateOfReturn: 0,
      totalRateOfReturn: 0
    },
    accumulated: {
      totalPremiumsPaid: 0,
      totalGuranteedRetireInc: 0,
      nonGuranteedRetireInc: 0,
      totalProjectedPayout: 0,
      totalProjectedPayoutRatio: 0,
      totalRateOfReturn: 0
    }
  };
  var gteedAnnualRetirementIncome = {};
  for (var y = 0; y < MAX_POLICY_YEAR; y++) {
    if (y === 0) {
      illustration.push(Object.assign({}, JSON.parse(JSON.stringify(illustrationData)), {
        retirementSolution
      }, {
        gteedAnnualRetirementIncome
      }));
    } else {
      illustration.push(Object.assign({}, JSON.parse(JSON.stringify(illustrationData))));
    }
  }
  var arorKeys = Object.keys(rates.aror);
  for (var i = arorKeys.length - 1; i >= 0; i--) {
    var sustainTestResult = calcIllustration(arorKeys[i], illustration);
  }
  return illustration;
}