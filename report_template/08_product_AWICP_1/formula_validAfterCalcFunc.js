function(quotation, planInfo, planDetail) {
  var planCodeMap = planDetail.planCodeMapping;
  var paymentMethod = quotation.policyOptions.paymentMethod;
  var idx = (planInfo.premium < 10000) ? 0 : 1;
  if (planCodeMap && planCodeMap[paymentMethod] && planCodeMap[paymentMethod][idx]) {
    planInfo.planCode = planCodeMap[paymentMethod][idx];
  }
  if (planInfo.calcBy) {
    var premium = math.number(quotation.plans[0].premium);
    if (planDetail.inputConfig && planDetail.inputConfig.premlim) {
      let min = planDetail.inputConfig.premlim.min;
      if (min && min > premium) {
        var err = {
          covCode: planInfo.covCode,
          msgPara: [],
          code: ''
        };
        err.msgPara.push(getCurrency(min, '$', 2));
        err.code = 'js.err.invalid_premlim_min';
        err.msgPara.push(premium);
        quotDriver.context.addError(err);
      }
    }
  }
}