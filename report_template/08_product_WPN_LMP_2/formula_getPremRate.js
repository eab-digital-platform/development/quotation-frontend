function(quotation, planInfo, planDetail, age) {
  var rateKey = planInfo.planCodeREF + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS');
  return planDetail.rates.premRate[rateKey][quotation.iAge];
}