function(planDetail, quotation, planDetails) {
  for (var i in quotation.plans) {
    planDetail.inputConfig.canViewSumAssured = true;
    planDetail.inputConfig.canViewPremium = false;
    var plan = quotation.plans[i];
    if (planDetail.covCode == plan.covCode && plan.covClass) {
      plan.sumInsured = math.number(quotation.plans[0].sumInsured || 0) * math.number(plan.covClass) / 100;
      plan.sumInsured = math.multiply(math.ceil(math.divide(math.number(plan.sumInsured), 100)), 100);
    }
  }
}