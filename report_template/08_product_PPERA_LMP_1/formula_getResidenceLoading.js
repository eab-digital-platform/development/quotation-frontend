function(quotation, planInfo, planDetail) {
  var rates = planDetail.rates;
  var countryCode = quotation.pResidence;
  var countryGroup = rates.countryGroup[countryCode];
  var substandardClass = rates.substandardClass[countryGroup]['PE'];
  var rateKey = planInfo.planCode + quotation.pGender + (quotation.pSmoke === 'Y' ? 'S' : 'NS') + substandardClass;
  if (planDetail.rates.residentialLoading[rateKey] && planDetail.rates.residentialLoading[rateKey][quotation.pAge]) {
    return planDetail.rates.residentialLoading[rateKey][quotation.pAge];
  }
  return 0;
}