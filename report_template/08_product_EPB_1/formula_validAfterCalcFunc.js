function(quotation, planInfo, planDetail) {
  var benefit = quotation.policyOptions.basicBenefit;
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  planInfo.planCode = 'EPB' + (benefit == 'M' ? 'B' : benefit == 'C' ? 'A' : '');
  planInfo.premTerm = planInfo.policyTerm;
  planInfo.premTermDesc = planInfo.polTermDesc;
  if (planInfo.policyTerm) {
    var year = planInfo.policyTerm.substring(3, planInfo.policyTerm.length);
    planInfo.policyTermYr = math.number(year);
    planInfo.premTermYr = math.number(year);
  }
}