function(quotation, planInfo, planDetails) {
  var planDetail = planDetails[planInfo.covCode];
  var saInput = planDetail.inputConfig.saInput;
  var premInput = planDetail.inputConfig.premInput;
  var closestPrem = null;
  var closestSA = null;
  var bsa = quotation.plans[0].sumInsured || 0,
    minSa = 75000;
  closestSA = bsa * 0.1 < minSa ? bsa * 0.1 : minSa;
  planInfo.sumInsured = closestSA;
  planInfo.saViewInd = 'Y';
  quotation.sumInsured = math.number(math.add(math.number(quotation.sumInsured || 0), closestSA));
}