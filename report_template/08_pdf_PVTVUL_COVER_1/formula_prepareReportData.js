function(quotation, planInfo, planDetails, extraPara) { /*test vul cover*/
  var basicPlan = quotation.plans[0];
  var hasSpTopUp = quotation.policyOptions.topUpSelect;
  var hasRspTopUp = quotation.policyOptions.rspSelect;
  var hasWithdrawal = quotation.policyOptions.wdSelect;
  var company = extraPara.company;
  var policyOption = quotation.policyOptions;
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var getCurrencyDesc = function(value, decimals) {
    return quotation.ccy + ' ' + getCurrency(value, '', decimals);
  };
  var getCurrencyWdDesc = function(value, decimals) {
    return getCurrency(value, '', decimals) + '%';
  };
  var getCurrencyPerDesc = function(value, decimals) {
    return quotation.ccy + ' ' + getCurrency(value, '', decimals) + '%';
  };
  var policyCcyDisplay;
  if (quotation.ccy === 'SGD') {
    policyCcyDisplay = 'Singapore Dollars';
  } else if (quotation.ccy === 'USD') {
    policyCcyDisplay = 'US Dollars';
  }
  var residencyCountryOptions, residencyCityOptions, iRCountry, iRCity, pRCountry, pRCity, occOptions;
  residencyCountryOptions = extraPara.optionsMap.residency.options;
  residencyCityOptions = extraPara.optionsMap.city.options;
  occOptions = extraPara.optionsMap.occupation.options;
  for (var v in residencyCountryOptions) {
    if (residencyCountryOptions[v].value === quotation.pResidence) {
      pRCountry = residencyCountryOptions[v].title.en;
    }
    if (residencyCountryOptions[v].value === quotation.iResidence) {
      iRCountry = residencyCountryOptions[v].title.en;
    }
  }
  var reportData = [];
  reportData = {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      ccy: quotation.ccy,
      ccySymbol: quotation.ccy === 'SGD' ? 'S$' : (quotation.ccy === 'USD' ? 'US$' : '$'),
      polType: quotation.policyOptions.policytype,
      proposer: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke),
        country: quotation.pResidenceCountryName ? quotation.pResidenceCountryName : '',
        city: quotation.pResidenceCityName ? quotation.pResidenceCityName : ''
      },
      insured: {
        name: quotation.iFullName,
        gender: getGenderDesc(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmokingDesc(quotation.iSmoke),
        country: quotation.iResidenceCountryName ? quotation.iResidenceCountryName : '',
        city: quotation.iResidenceCityName ? quotation.iResidenceCityName : ''
      },
      policyOptions: {
        numOfWD: policyOption.numOfWD ? policyOption.numOfWD : null,
        riskClassification: policyOption.riskClassification ? policyOption.riskClassification : null,
        riskClassificationDesc: quotation.policyOptionsDesc.riskClassification.en ? quotation.policyOptionsDesc.riskClassification.en : null,
        extraMortality: policyOption.extraMortality ? policyOption.extraMortality : null,
        extraMortalityDesc: quotation.policyOptionsDesc.extraMortality ? quotation.policyOptionsDesc.extraMortality.en : null,
        loading: policyOption.loading ? policyOption.loading : null,
        loadingPeriod: quotation.policyOptionsDesc.loadingPeriod ? quotation.policyOptionsDesc.loadingPeriod : null,
        riskClassification2: policyOption.riskClassification2 !== undefined ? policyOption.riskClassification2 : null,
        riskClassificationDesc2: quotation.policyOptionsDesc.riskClassification2 !== undefined ? quotation.policyOptionsDesc.riskClassification2.en : null,
        extraMortality2: policyOption.extraMortality2 !== undefined ? policyOption.extraMortality2 : null,
        extraMortality2Desc: quotation.policyOptionsDesc.extraMortality2 !== undefined ? quotation.policyOptionsDesc.extraMortality2.en : null,
        loading2: policyOption.loading2 ? policyOption.loading2 : null,
        loadingPeriod2: quotation.policyOptionsDesc.loadingPeriod2 ? quotation.policyOptionsDesc.loadingPeriod2 : null,
        withdrawalOption: policyOption.withdrawalOption,
        withdrawalOptionDesc: quotation.policyOptionsDesc.withdrawalOption.en,
        invRateForWD: getCurrencyWdDesc(policyOption.invRateForWD, 2),
        startYr1: policyOption.startYr1,
        startYr2: policyOption.startYr2,
        startYr3: policyOption.startYr3,
        startYr4: policyOption.startYr4,
        startYr5: policyOption.startYr5,
        endYr1: policyOption.endYr1,
        endYr2: policyOption.endYr2,
        endYr3: policyOption.endYr3,
        endYr4: policyOption.endYr4,
        endYr5: policyOption.endYr5,
        wdAmt1: getCurrencyDesc(policyOption.wdAmt1, 0),
        wdAmt2: getCurrencyDesc(policyOption.wdAmt2, 0),
        wdAmt3: getCurrencyDesc(policyOption.wdAmt3, 0),
        wdAmt4: getCurrencyDesc(policyOption.wdAmt4, 0),
        wdAmt5: getCurrencyDesc(policyOption.wdAmt5, 0),
        wdPer1: getCurrencyWdDesc(policyOption.wdPer1, 2),
        wdPer2: getCurrencyWdDesc(policyOption.wdPer2, 2),
        wdPer3: getCurrencyWdDesc(policyOption.wdPer3, 2),
        wdPer4: getCurrencyWdDesc(policyOption.wdPer4, 2),
        wdPer5: getCurrencyWdDesc(policyOption.wdPer5, 2)
      },
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      plans: quotation.plans.map(function(plan) {
        return {
          name: plan.covName.en,
          polTermDesc: plan.polTermDesc,
          sumInsured: getCurrency(plan.sumInsured, '', 0),
          initPremium: getCurrency(plan.premium, '', 0)
        };
      })
    }
  };
  return reportData;
}