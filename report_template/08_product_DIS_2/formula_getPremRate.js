function(quotation, planInfo, planDetail, age) {
  var policyTerm = Number.parseInt(planInfo.policyTerm);
  if (planInfo.policyTerm.endsWith("_TA") && planInfo.premTerm === 'SP') {
    policyTerm = age === 60 ? 10 : 15;
  }
  var rateKey;
  if (planInfo.policyTerm.endsWith("_TA") && planInfo.premTerm !== 'SP') {
    rateKey = 'TPDNR_';
  } else {
    rateKey = 'TPDR_';
  }
  if (quotation.policyOptions.planType === 'renew') {
    rateKey += (planInfo.premTerm === 'SP' ? 'SP' : 'RP');
  } else {
    rateKey += (planInfo.premTerm.endsWith('_YR') ? Number.parseInt(planInfo.premTerm) + 'P' : 'RP');
  }
  rateKey += '_' + quotation.iGender;
  rateKey += quotation.iSmoke === 'Y' ? 'S' : 'NS';
  var rates = planDetail.rates.premRate[rateKey];
  var premRate = math.bignumber(rates[policyTerm][age] || 0);
  var substdClsMapping = planDetail.rates.substdCls[quotation.iResidence];
  if (substdClsMapping) {
    var substdCls = substdClsMapping[quotation.iResidenceCity] || substdClsMapping.ALL;
    if (substdCls) {
      var substdRates = planDetail.rates.residentialLoading[rateKey + '_' + substdCls];
      if (substdRates) {
        premRate = math.add(premRate, substdRates[policyTerm][age] || 0);
      }
    }
  }
  return premRate;
}