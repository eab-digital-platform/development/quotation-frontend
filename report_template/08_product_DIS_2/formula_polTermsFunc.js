function(planDetail, quotation) {
  var bpPolTerm = Number.parseInt(quotation.plans[0].policyTerm);
  var bpPremTerm = quotation.plans[0].premTerm;
  if (!bpPolTerm) {
    return [];
  }
  var policyTermList = [];
  if (quotation.policyOptions.planType === 'renew') {
    if (bpPolTerm + quotation.iAge >= 70 && bpPremTerm !== 'SP') {
      policyTermList.push({
        value: '70_TA',
        title: 'To Age 70'
      });
    } else if (bpPolTerm + quotation.iAge > 70 && bpPremTerm === 'SP') {
      policyTermList.push({
        value: '70_TA',
        title: 'To Age 70'
      });
    } else {
      policyTermList.push({
        value: bpPolTerm + '_YR',
        title: bpPolTerm + ' Years'
      });
    }
  } else if (quotation.policyOptions.planType === 'toAge') {
    if (bpPolTerm >= 70) {
      policyTermList.push({
        value: '70_TA',
        title: 'To Age 70'
      });
    } else {
      policyTermList.push({
        value: bpPolTerm + '_TA',
        title: 'To Age ' + bpPolTerm
      });
    }
  }
  return policyTermList;
}