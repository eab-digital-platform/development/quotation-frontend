function(value, position) {
  if (!value) return null;
  var sign = value < 0 ? -1 : 1;
  if (!position) position = 0;
  var scale = math.pow(10, position);
  return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
}