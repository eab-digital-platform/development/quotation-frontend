function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  var limit = runFunc(planDetail.formulas.getMinMaxSa, quotation, planDetail);
  var coi = quotation.policyOptions.insuranceCharge;
  planDetail.premInputInd = true;
  planDetail.inputConfig.canEditPremium = quotation.paymentMode && coi ? true : false;
  planDetail.saInputInd = true;
  planDetail.inputConfig.canViewSumAssured = true;
  planDetail.inputConfig.canEditSumAssured = quotation.paymentMode && coi ? true : false;
  if (!planDetail.inputConfig.canEditSumAssured) {
    quotation.plans[0].sumInsured = null;
  }
  planDetail.inputConfig.saInput = {
    factor: 100
  };
  planDetail.inputConfig.planInfoHintMsg = 'Sum Assured / Benefit # must be ' + getCurrency(limit.min, 'S$', 2) + ' - ' + getCurrency(limit.max, 'S$', 2) + '. Sum Assured / Benefit must be in multiples of $100';
  return planDetail;
}