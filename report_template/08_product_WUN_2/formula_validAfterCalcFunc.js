function(quotation, planInfo, planDetail) { /**WUN validAfterCalcFunc*/
  var paddingZeroStr = function(number) {
    if (number < 10 && number >= 0) {
      return '0' + number;
    } else if (number < 0) {
      return '00';
    } else {
      return number;
    }
  };
  var defaultArr = ['ESP'];
  if (defaultArr.indexOf(quotation.baseProductCode) > -1 && quotation && quotation.plans && quotation.plans.length && quotation.plans[0].policyTerm) {
    if (planInfo.policyTerm) {
      var planType = quotation.policyOptions.planType;
      if (planInfo.policyTerm.indexOf('TA') > -1) {
        planInfo.planCode = 'WU' + paddingZeroStr(Number.parseInt(planInfo.policyTerm) + quotation.iAge);
      } else {
        planInfo.planCode = paddingZeroStr(Number.parseInt(planInfo.policyTerm)) + 'WUN';
      }
    }
    if (!quotation.plans[0].premium) {
      planInfo.premium = planInfo.yearPrem = planInfo.halfYearPrem = planInfo.quarterPrem = planInfo.monthPrem = quotation.plans[0].premium;
    }
    if (planInfo.premium) {
      planInfo.policyTermYr = Number.parseInt(planInfo.policyTerm);
      if (planInfo.premTerm === 'SP') {
        planInfo.premTermYr = 1;
      } else {
        planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
      }
    }
  } else if (quotation && quotation.plans && quotation.plans.length && quotation.plans[0].policyTerm) {
    if (planInfo.policyTerm) {
      var planType = quotation.policyOptions.planType;
      var policyTermValue = paddingZeroStr(Number.parseInt(planInfo.policyTerm));
      if (quotation.policyOptions.planType === 'renew') {
        planInfo.planCode = policyTermValue + 'WU';
      } else if (planInfo.policyTerm.indexOf('TA') > -1) {
        planInfo.planCode = 'WU' + policyTermValue;
      } else {
        planInfo.planCode = policyTermValue + 'WUN';
      }
      if (planInfo.premium) {
        planInfo.policyTermYr = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
        if (planInfo.premTerm === 'SP') {
          planInfo.premTermYr = 1;
        } else if (planInfo.premTerm.endsWith('_YR')) {
          planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
        } else {
          planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
        }
      }
    }
  }
  var crxPlan = _.find(quotation.plans, p => p.covCode === 'CRX');
  if (crxPlan && crxPlan.sumInsured && crxPlan.sumInsured === quotation.plans[0].sumInsured) {
    quotDriver.context.addError({
      covCode: planInfo.covCode,
      msg: 'Premium Waiver (CIUN) is not allowed if Advance CI Payout rider Sum Assured is the same as your Basic Plan\'s Sum Assured.'
    });
  }
}