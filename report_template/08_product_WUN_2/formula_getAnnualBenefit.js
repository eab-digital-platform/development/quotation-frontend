function(quotation, planInfo, year, extraPara) {
  var indexation = quotation.policyOptions.indexation === 'Y';
  var annualBenefit = math.bignumber(0);
  _.each(extraPara.illustrations, (ills, covCode) => {
    var ill = ills[year - 1];
    if (ill) {
      if (covCode === quotation.baseProductCode) {
        annualBenefit = math.add(annualBenefit, math.bignumber(indexation ? ills[0].yearPrem : ill.yearPrem));
      } else if (covCode === 'SV') {
        annualBenefit = math.add(annualBenefit, math.bignumber(indexation ? ills[0].yearPrem : ill.yearPrem));
      } else if (covCode !== planInfo.covCode) {
        annualBenefit = math.add(annualBenefit, math.bignumber(ill.yearPrem));
      }
    }
  });
  return annualBenefit;
}