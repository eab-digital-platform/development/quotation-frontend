function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  var coi = quotation.policyOptions.insuranceCharge,
    iAge = quotation.iAge;
  var iminAge = planDetail.entryAge[0].iminAge,
    premium = math.number(quotation.plans[0].premium || 0),
    sa = 0,
    payMode = quotation.paymentMode,
    minSa = 0;
  var min = payMode == "M" ? 100 : payMode == "Q" ? 300 : payMode == "S" ? 600 : payMode == "A" ? 1200 : 0;
  var errorMsg = "";
  if (!quotation.plans[0].premium && quotation.plans[0].sumInsured) {
    quotDriver.context.addError({
      covCode: planInfo.covCode,
      code: 'Please enter premium.'
    });
  } else if (quotation.plans[0].sumInsured === 0 || quotation.plans[0].sumInsured) {
    var limit = runFunc(planDetail.formulas.getMinMaxSa, quotation, planDetail);
    minSa = limit.min;
    for (var i in quotation.plans) {
      var plan = quotation.plans[i];
      if (["EPB", "ADBR", "WPSR", "WPTN", "WPPT"].indexOf(plan.covCode) == -1) {
        sa += math.number(plan.sumInsured || 0);
      }
    }
    if (sa < minSa || (quotation.plans[0].premium && (!quotation.plans[0].sumInsured || quotation.plans[0].sumInsured === 0))) {
      errorMsg = 'Minimum Sum Assured / Benefit # is ' + getCurrency(minSa, '$', 2) + '. Sum Assured / Benefit must be in multiples of $100.';
    }
    if (errorMsg.length > 0) {
      quotDriver.context.addError({
        covCode: planInfo.covCode,
        code: errorMsg
      });
    }
  } else if (!quotation.plans[0].sumInsured) {
    quotDriver.context.addError({
      covCode: planInfo.covCode,
      code: ' '
    });
  }
  planInfo.policyTerm = "999";
  planInfo.polTermDesc = "Whole Life";
  planInfo.premTerm = "yrs" + (99 - iAge);
  planInfo.premTermDesc = (99 - iAge) + " Years";
  planInfo.premTermYr = 99 - iAge;
  planInfo.policyTermYr = 99 - iAge;
  var planCode = "FP" + quotation.policyOptions.basicBenefit + "T";
  if (coi && coi != "yrt") {
    planCode += coi.substring(coi.length - 2, coi.length);
  }
  planInfo.planCode = planCode;
}