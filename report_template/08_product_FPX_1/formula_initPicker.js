function(quotation, policy) {
  var options = policy.options,
    value = policy.value,
    id = policy.id;
  if (policy.type == 'picker' && options) {
    if (!quotation.policyOptions[id] && value) {
      for (var i in options) {
        var opt = options[i];
        if (opt.value == value) {
          return value;
        }
      }
    }
    if (!quotation.policyOptions[id] && options.length) {
      if (value == '999') {
        return policy.options[options.length - 1].value;
      } else {
        return null;
      }
    }
  }
  return quotation.policyOptions[id];
}