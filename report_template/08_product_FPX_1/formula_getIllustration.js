function(quotation, planInfo, planDetails, extraParam) {
  var trunc = function(value, position) {
    return runFunc(planDetails[planInfo.covCode].formulas.trunc, value, position);
  };
  var round = function(value, position) {
    return runFunc(planDetails[planInfo.covCode].formulas.round, value, position);
  };
  var coiOptionMapping = function(COI_Option) {
    switch (COI_Option) {
      case 0:
      case 1:
        return 125;
      case 2:
        return 50;
      case 3:
        return 55;
      case 4:
        return 60;
      case 5:
        return 62;
      case 6:
        return 65;
      case 7:
        return 99;
    }
  };
  var getLoading = function(flag, r1, f1, r2, f2, f3) {
    r1 = math.bignumber(r1);
    r2 = math.bignumber(r2);
    r2 = (flag == 'No') ? r2 : 0;
    var r3 = math.subtract(math.subtract(1, r1), r2);
    r3 = math.bignumber(r3);
    var loading = math.multiply(r1, f1);
    loading = math.add(loading, math.multiply(r2, f2));
    loading = math.add(loading, math.multiply(r3, f3));
    return round(loading, 2);
  };
  var rates = planDetails[quotation.baseProductCode]['rates'];
  var Pay_Mode = quotation.paymentMode;
  var Basic_SA = math.bignumber(quotation.plans[0].sumInsured ? quotation.plans[0].sumInsured : 0);
  var LA_ANB = quotation.iAge;
  var P_ANB = quotation.pAge;
  var polOpt = quotation.policyOptions;
  var eml_1 = math.divide(polOpt.iLife ? math.bignumber(polOpt.iLife) : 0, 100);
  var eml_2 = math.divide(polOpt.pLife ? math.bignumber(polOpt.pLife) : 0, 100);
  var emci_1 = math.divide(polOpt.iCi ? math.bignumber(polOpt.iCi) : 0, 100);
  var emci_2 = math.divide(polOpt.pCi ? math.bignumber(polOpt.pCi) : 0, 100);
  var tpd_1 = math.bignumber(polOpt.iTPD ? polOpt.iTPD : 0);
  var tpd_2 = math.bignumber(polOpt.pTPD ? polOpt.pTPD : 0);
  var Service_Fee = math.bignumber(polOpt.serviceFee ? polOpt.serviceFee : 0) / 100;
  var occupationClass = polOpt.occupationClass;
  var RSP_Prem = math.bignumber(polOpt.rspAmount ? polOpt.rspAmount : 0);
  var Start_Age = math.bignumber(polOpt.wdFromAge ? polOpt.wdFromAge : 0);
  var End_Age = math.bignumber(polOpt.wdToAge ? polOpt.wdToAge : 0);
  var Withdrawal_Frequency = polOpt.wdFreq;
  var Withdrawal = math.bignumber(polOpt.wdAmount ? polOpt.wdAmount : 0);
  var BA_Benefit = polOpt.basicBenefit;
  var Insurance_Charge = polOpt.insuranceCharge;
  var SP_Topup = math.bignumber(polOpt.topUpAmt ? polOpt.topUpAmt : 0);
  var change_SA = math.bignumber(polOpt.saAmt ? polOpt.saAmt : 0);
  var Change_SA_Year = math.bignumber(polOpt.saPolYr ? polOpt.saPolYr : 0);
  var Pay_Mode_Tbl = rates['Pay_Mode_Tbl'];
  var paymentMode = Pay_Mode_Tbl[Pay_Mode];
  var RP = math.bignumber(quotation.plans[0].premium ? quotation.plans[0].premium : 0);
  var annualised_basicprem = math.bignumber(math.multiply(paymentMode, RP));
  var EMYR_Basic = math.bignumber(100);
  var EMYR_WPP = math.bignumber(100);
  var EMYR_LAR_CAR = math.bignumber(100);
  var EMYR_LBR = math.bignumber(100);
  var EMYR_LTR = math.bignumber(100);
  var EMYR_ETPD = math.bignumber(100);
  var EMYR_WP = math.bignumber(100);
  var EMYR_WPS = math.bignumber(100);
  var MaxAge_LTR_LBR = math.bignumber(85);
  var MaxAge_ETPD = math.bignumber(55);
  var MaxAge_ADB = math.bignumber(65);
  var max_etpd_perc = math.bignumber(0.10);
  var max_etpd_payout = math.bignumber(75000);
  var ETPDRate = math.bignumber(0.460);
  var ADM = math.bignumber(5);
  var SaleChg = math.bignumber(0.05);
  var TPD_LA_FLAG = 'No';
  var TPD_A_FLAG = 'No';
  var rebate_value_low = math.bignumber(30000);
  var rebate_value_med = math.bignumber(100000);
  var rebate_value_high = math.bignumber(500000);
  var rebate_bonus_low = math.bignumber(0.001);
  var rebate_bonus_med = math.bignumber(0.002);
  var rebate_bonus_high = math.bignumber(0.003);
  var COI_Option;
  var tableAC = rates['coiOption'];
  if (BA_Benefit == 'C') {
    COI_Option = 0;
  } else {
    if ((BA_Benefit == 'M' && Insurance_Charge == 'yrt')) {
      COI_Option = 1;
    } else COI_Option = tableAC[Insurance_Charge];
  }
  var COI_EndYear = coiOptionMapping(COI_Option) - LA_ANB;
  var PIRR_Low = math.bignumber(extraParam.PIRR);
  var AMF = 0;
  var fundlist = quotation.fund.funds;
  for (var i = 0; i < fundlist.length; i++) {
    var fund = fundlist[i];
    var amfRate = rates['amfRate'][fund.fundCode];
    if (amfRate) {
      amfRate = math.bignumber(amfRate);
      AMF = math.add(AMF, math.multiply(amfRate, math.divide(math.bignumber(fund.alloc), 100)));
    }
  }
  var mort_lookup_col_LA = ((quotation.iGender == 'M') ? 'M' : 'F') + ((quotation.iSmoke == 'N') ? 'NS' : 'S');
  var mort_lookup_col_A = ((quotation.pGender == 'M') ? 'M' : 'F') + ((quotation.pSmoke == 'N') ? 'NS' : 'S');
  var mort_lookup = 'coi' + ((COI_Option < 2) ? '_yrt_basic' : '_level_age' + coiOptionMapping(COI_Option)) + ((TPD_LA_FLAG == 'No') ? '_tpd' : '_ntpd');
  var mort_lookup_table = rates['coiRate'][mort_lookup][mort_lookup_col_LA];
  var CAR_percent = 0;
  var LAR_Percent = 0;
  var LTR_SA = 0;
  var LBR_SA = 0;
  var WPS_term = 0;
  var ADB_SA = 0;
  var WP_n = 0;
  var WPP_n = 0;
  var WPS_n = 0;
  var CAR_Op = 'N';
  var LTR_Op = 'N';
  var LBR_Op = 'N';
  var ADB_Op = 'N';
  var ETPD_Op = 'N';
  var WP_Op = 'N';
  var WPP_Op = 'N';
  var WPS_Op = 'N';
  var LAR_Op = 'N';
  var plans = quotation.plans;
  for (var i = 0; i < plans.length; i++) {
    var plan = plans[i];
    var covClass = plan.covClass;
    covClass = (covClass) ? math.divide(covClass, 100) : 0;
    var covCode = plan.covCode;
    var policyTerm = plan.policyTerm;
    if (policyTerm && typeof policyTerm == 'string') policyTerm = plan.policyTerm.replace('yrs', '').replace('age', '');
    var sumInsured = plan.sumInsured;
    switch (covCode) {
      case 'CARB':
        CAR_Op = 'Y';
        CAR_percent = covClass ? covClass : 1;
        CAR_percent = math.bignumber(CAR_percent);
        break;
      case 'LARB':
        LAR_Op = 'Y';
        LAR_Percent = covClass;
        LAR_Percent = math.bignumber(LAR_Percent);
        break;
      case 'LTT':
        LTR_Op = 'Y';
        LTR_SA = math.bignumber(sumInsured);
        break;
      case 'LVT':
        LBR_Op = 'Y';
        LBR_SA = math.bignumber(sumInsured);
        break;
      case 'ADBR':
        ADB_Op = 'Y';
        ADB_SA = math.bignumber(sumInsured);
        break;
      case 'EPB':
        ETPD_Op = 'Y';
        break;
      case 'WPTN':
        WP_Op = 'Y';
        WP_n = policyTerm;
        break;
      case 'WPPT':
        WPP_Op = 'Y';
        WPP_n = policyTerm;
        break;
      case 'WPSR':
        WPS_Op = 'Y';
        WPS_term = policyTerm;
        WPS_n = plan.policyTerm.indexOf('yrs') > -1 ? WPS_term : WPS_term - LA_ANB;
        break;
    }
  }
  var tpd_1_remain = math.subtract(tpd_1, 1);
  var tpd_2_remain = math.subtract(tpd_2, 1);
  var loading_Basic;
  if (TPD_LA_FLAG == 'No') {
    loading_Basic = math.add(math.multiply(0.85, eml_1), math.multiply(0.15, tpd_1_remain));
  } else {
    loading_Basic = eml_1;
  }
  loading_Basic = round(loading_Basic, 2);
  var COI_StartYear = (COI_Option < 2) ? 1 : 3;
  var n = 125 - LA_ANB;
  var annualised_rsp = math.bignumber(trunc(math.multiply(RSP_Prem, 12), 2));
  var Allo_PremCharge_Tbl = rates['Allo_PremCharge_Tbl'];
  var dealerGroup = quotation.dealerGroup;
  var Channel = ((dealerGroup == 'SYNERGY' || dealerGroup == 'AGENCY') ? 'FP' : ((dealerGroup == 'FUSION' || dealerGroup == 'BROKER') ? 'IFA' : dealerGroup));
  var RSP_SP_COM = rates['rspSpCom'];
  var mod_prem = rates['mod_prem'];
  var lbr = rates['coiRate']['coi_yrt_lwp_tpd'];
  var lbr_no_tpd = rates['coiRate']['coi_yrt_lwp_ntpd'];
  var ltr = rates['coiRate']['coi_yrt_lw_tpd'];
  var ltr_no_tpd = rates['coiRate']['coi_yrt_lw_ntpd'];
  var WPS = rates['coiRate']['coi_yrt_wp_tpd'];
  var wps_no_tpd = rates['coiRate']['coi_yrt_wp_ntpd'];
  var LAR = rates['coiRate']['coi_yrt_la_tpd'];
  var LAR_NO_TPD = rates['coiRate']['coi_yrt_la_ntpd'];
  var Loading_WPP = getLoading(TPD_A_FLAG, 0.62, emci_2, 0.06, tpd_2_remain, eml_2);
  var Loading_LBR = getLoading(TPD_LA_FLAG, 0.62, emci_1, 0.06, tpd_1_remain, eml_1);
  var Loading_LTR = getLoading(TPD_LA_FLAG, 0, 0, 0.15, tpd_1_remain, eml_1);
  var Loading_WP = getLoading(TPD_A_FLAG, 0, 0, 0.15, tpd_2_remain, eml_2);
  var Loading_WPS = round(emci_1, 2);
  var Loading_ETPD = (ETPD_Op == 'N' ? 0 : round(tpd_1_remain, 2));
  var loading_LAR_CAR = ((LAR_Op == 'N' && CAR_Op == 'N') ? 0 : round(emci_1, 2));
  var AVs = [];
  var Ms = [];
  var AJs = [];
  var Ns = [];
  var BBs = [];
  var Js = [];
  var Ps = [];
  var Is = [];
  var Ds = [];
  var AYs = [];
  var oldAV, oldBB, oldC, oldAA, oldAB, oldAC, oldAD, oldAE, oldAF, oldAG_1, oldAG_2, oldAH, oldAI;
  var iDobArray = quotation.iDob.split('-');
  var brithYear = iDobArray[0];
  var birthMonth = iDobArray[1];
  var birthDay = iDobArray[2];
  var now = new Date();
  var curDay = now.getDate();
  var curMonth = now.getMonth() + 1;
  var curYear = now.getFullYear();
  var daysInYear = 365.25;
  var LA_Age = 0;
  LA_Age = math.multiply(math.subtract(curYear, brithYear), daysInYear);
  LA_Age = math.add(LA_Age, math.multiply(math.divide(math.subtract(curMonth, birthMonth), 12), 365.25));
  LA_Age = math.add(LA_Age, math.subtract(curDay, birthDay));
  LA_Age = math.divide(LA_Age, 365.25);
  var cvTable = null;
  var cvTblKey = ((quotation.iGender == 'M') ? 'm' : 'f') + ((quotation.iSmoke == 'N') ? 'n' : 'y');
  if (COI_Option > 1 && COI_Option < 7) {
    var cvTableName = 'coi' + coiOptionMapping(COI_Option) + '_cv' + '_' + cvTblKey + ((TPD_LA_FLAG == 'No') ? '_tpd' : '_ntpd');
    cvTable = rates['cvRate'][cvTableName];
  }
  var bd = 1;
  var items = [];
  var runMonthPeriod = math.multiply((125 - LA_Age), 12);
  for (var row = 0; row < n; row++) {
    var item = {};
    items.push(item);
    bd++;
  }
  var isChangeSA = extraParam.isChangeSA;
  var isWithdrawal = extraParam.isWithdrawal;
  var j = 0;
  for (var row = 0; row <= runMonthPeriod; row++) {
    var b = row;
    var a, c, d, e;
    if (row === 0) c = 0;
    else if (row == 1) c = 1;
    else c = (oldC == 12 ? 1 : oldC + 1);
    oldC = c;
    if (row === 0) {
      a = 0;
      d = LA_ANB;
      e = P_ANB;
    } else {
      a = math.floor(math.divide((b - 1), 12)) + 1;
      d = trunc(math.add(math.bignumber(LA_ANB + a - 1), math.bignumber(math.divide((c - 1), 12))), 0);
      e = trunc(math.add(math.bignumber(P_ANB + a - 1), math.bignumber(math.divide((c - 1), 12))), 0);
    }
    d = math.number(d);
    e = math.number(e);
    Ds.push(d);
    var aj = 0;
    var aa, ab, ac, ad, ae, af, ag, ah, ai;
    var bb = (a < n) ? Basic_SA : 0;
    if (isChangeSA) {
      bb = ((change_SA > 0 && a >= Change_SA_Year) ? change_SA : (a <= n ? Basic_SA : 0));
    }
    if (row > 0 && a !== '') {
      aa = 0;
      if (COI_Option === 0 && CAR_Op == 'Y') {
        var rate = LAR[mort_lookup_col_LA][d];
        aa = math.multiply(math.bignumber(CAR_percent), rate ? math.bignumber(rate) : 0);
        aa = math.bignumber(aa);
        aa = math.multiply(aa, math.add(1, (a <= EMYR_LAR_CAR ? loading_LAR_CAR : 0)));
        var tmp = math.subtract(math.bignumber(bb), math.bignumber(Math.max(oldAV, 0)));
        tmp = math.bignumber(tmp);
        tmp = math.divide(Math.max(tmp, 0), 1000);
        aa = math.multiply(aa, math.bignumber(tmp));
        aa = math.divide(aa, 12);
        aa = trunc(aa, 2);
      }
      ab = math.bignumber(0);
      if (COI_Option > 0 && !(COI_Option > 1 && a > COI_EndYear && (oldAB == 0 || oldAV == 0)) && (COI_Option > 0 && LAR_Op == 'Y')) {
        ab = LAR_Percent && LAR[mort_lookup_col_LA][d] ? math.multiply(LAR_Percent, LAR[mort_lookup_col_LA][d]) : 0;
        ab = math.bignumber(ab);
        ab = math.multiply(ab, math.add(1, (a <= EMYR_LAR_CAR ? loading_LAR_CAR : 0)));
        var tmpAb = math.bignumber(bb);
        if (COI_Option === 0) {
          tmpAb = math.subtract(tmpAb, Math.max(oldAV, 0));
          tmpAb = Math.max(tmpAb, 0);
        }
        tmpAb = math.divide(bb, 1000);
        ab = math.multiply(ab, tmpAb);
        ab = math.divide(ab, 12);
        ab = trunc(ab, 2);
      }
      oldAB = ab;
      ac = 0;
      if (a !== '' && LTR_Op == 'Y' && !(COI_Option > 1 && a > COI_EndYear && (oldAC == 0 || oldAV <= 0)) && (d < MaxAge_LTR_LBR)) {
        var tmp = (TPD_LA_FLAG == 'No') ? ltr[mort_lookup_col_LA][d] : ltr_no_tpd[mort_lookup_col_LA][d];
        tmp = math.bignumber(tmp);
        var ac_1 = math.divide(LTR_SA, 1000);
        ac_1 = math.bignumber(ac_1);
        ac = math.multiply(math.bignumber(tmp), math.bignumber(ac_1));
        ac = math.multiply(math.bignumber(ac), math.bignumber(math.add(1, (a <= EMYR_LTR ? Loading_LTR : 0))));
        ac = math.divide(ac, 12);
        if (isChangeSA) {
          ac = math.divide(math.multiply(ac, bb), Basic_SA);
        }
        ac = trunc(ac, 2);
      }
      oldAC = ac;
      ad = 0;
      if (a !== '' && LBR_Op == 'Y' && !((COI_Option > 1 && a > COI_EndYear && (oldAD == 0 || oldAV <= 0))) && (d < MaxAge_LTR_LBR)) {
        var tmp = 0;
        if (TPD_LA_FLAG == 'No') {
          tmp = lbr[mort_lookup_col_LA][d];
        } else {
          tmp = lbr_no_tpd[mort_lookup_col_LA][d];
        }
        if (!tmp) tmp = 0;
        tmp = math.bignumber(tmp);
        var ad_2 = math.add(1, (a <= EMYR_LBR ? Loading_LBR : 0));
        ad_2 = math.bignumber(ad_2);
        var ad_3 = math.divide(LBR_SA, 1000);
        if (isChangeSA) ad_3 = math.divide(math.divide(math.multiply(LBR_SA, bb), Basic_SA), 1000);
        ad_3 = math.bignumber(ad_3);
        ad = math.multiply(math.multiply(tmp, ad_3), ad_2);
        ad = math.divide(ad, 12);
        ad = trunc(ad, 2);
      }
      oldAD = ad;
      ae = 0;
      if (a !== '' && ADB_Op == 'Y' && !((COI_Option > 1 && a > COI_EndYear && (oldAE == 0 || oldAV <= 0))) && (d < MaxAge_ADB)) {
        ae = math.bignumber(math.bignumber(rates.adbRate.rate[occupationClass - 1]));
        ae = math.multiply(math.bignumber(ae), (ADB_SA));
        if (isChangeSA) {
          ae = math.multiply(ae, bb);
          ae = math.divide(ae, Basic_SA);
        }
        ae = math.divide(ae, 12);
        ae = trunc(math.divide(ae, 1000), 2);
      }
      oldAE = ae;
      af = 0;
      if (a !== '' && ETPD_Op == 'Y' && !((COI_Option > 1 && a > COI_EndYear && (oldAF == 0 || oldAV <= 0))) && d < MaxAge_ETPD) {
        var af = math.bignumber(65 - (d + 1));
        af = math.multiply(af, Math.min(math.multiply(max_etpd_perc, (isChangeSA ? bb : oldBB)), max_etpd_payout));
        af = math.multiply(af, ETPDRate);
        af = math.divide(af, 1000);
        af = math.multiply(af, math.add(1, (a <= EMYR_ETPD ? Loading_ETPD : 0)));
        af = math.divide(af, 12);
        af = trunc(af, 2);
      }
      oldAF = af;
      ag = 0;
      if (a !== '' && WP_Op == 'Y' && !((COI_Option > 1 && a > COI_EndYear && (oldAG_2 == 0 || oldAV <= 0))) && a <= WP_n) {
        var tmp = 0;
        if (TPD_A_FLAG == 'No') {
          tmp = ltr[mort_lookup_col_A][e];
        } else {
          tmp = ltr_no_tpd[mort_lookup_col_A][e];
        }
        var ag1 = math.bignumber(math.divide((WP_n - a + 1), 1000));
        var ag2 = math.bignumber(tmp);
        ag2 = math.multiply(math.bignumber(ag2), math.bignumber(annualised_basicprem));
        var ag3 = math.add(1, (a <= EMYR_WP ? Loading_WP : 0));
        ag3 = math.bignumber(ag3);
        ag = math.multiply(math.multiply(ag1, ag2), ag3);
        ag = math.divide(ag, 12);
        ag = trunc(ag, 2);
      }
      oldAG_2 = oldAG_1;
      oldAG_1 = ag;
      ah = 0;
      if (a !== '' && WPP_Op == 'Y' && !((COI_Option > 1 && a > COI_EndYear && (oldAH == 0 || oldAV <= 0))) && (a <= WPP_n)) {
        var tmp = 0;
        if (TPD_A_FLAG == 'No') {
          tmp = lbr[mort_lookup_col_A][e];
        } else {
          tmp = lbr_no_tpd[mort_lookup_col_A][e];
        }
        var ah1 = math.bignumber(tmp);
        ah1 = math.multiply(math.bignumber(ah1), math.bignumber(annualised_basicprem));
        ah = math.multiply(math.divide(math.multiply(ah1, (WPP_n - a + 1)), 1000), math.add(1, (a <= EMYR_WPP ? Loading_WPP : 0)));
        ah = math.divide(ah, 12);
        ah = trunc(ah, 2);
      }
      oldAH = ah;
      ai = 0;
      if (a !== '' && WPS_Op == 'Y' && !((COI_Option > 1 && a > COI_EndYear && (oldAI == 0 || oldAV <= 0))) && a <= WPS_n) {
        var tmp = WPS[mort_lookup_col_LA][d];
        ai = math.divide(math.multiply(math.multiply(math.bignumber(tmp), annualised_basicprem), (WPS_n - a + 1)), 1000);
        ai = math.multiply(ai, math.add(1, (a <= EMYR_WPS ? Loading_WPS : 0)));
        ai = math.divide(ai, math.bignumber(12));
        ai = trunc(ai, 2);
      }
      oldAI = ai;
    }
    BBs.push(bb);
    oldBB = bb;
    var l = 0;
    aj = 0;
    if (a !== '' && a !== 0) {
      var r = 0;
      var s = 0,
        t = 0,
        u = 0,
        v = 0,
        w = 0,
        x = 0,
        y = 0,
        z = 0;
      if (!(a < COI_StartYear || a > COI_EndYear)) {
        var multiplier = math.bignumber(math.add(1, (a <= EMYR_Basic ? loading_Basic : 0)));
        if (COI_Option == 1) {
          r = math.bignumber((mort_lookup_table[d] ? mort_lookup_table[d] : 0));
          r = math.multiply(r, multiplier);
          s = math.divide(bb, 1000);
          t = math.multiply(math.bignumber(r), math.bignumber(s));
          t = math.divide(t, 12);
          t = trunc(t, 2);
        }
        if (COI_Option === 0) {
          u = math.bignumber(mort_lookup_table[d] ? mort_lookup_table[d] : 0);
          u = math.multiply(u, multiplier);
          v = math.max(math.subtract(math.bignumber(bb), math.bignumber(Math.max(oldAV, 0))));
          v = math.divide(v, 1000);
          w = math.multiply(math.bignumber(u), math.bignumber(v));
          w = math.divide(w, 12);
          w = trunc(w, 2);
        }
        if (COI_Option > 1) {
          x = math.bignumber((mort_lookup_table[LA_ANB]));
          x = math.multiply(x, multiplier);
          y = math.divide(bb, 1000);
          z = math.multiply(math.bignumber(x), math.bignumber(y));
          z = math.divide(z, 12);
          z = trunc(z, 2);
        }
      }
      if (!(row == 0 || a === '')) {
        aj = Math.max(t, w, z);
        aj = math.add(aj, math.bignumber(aa));
        aj = math.add(aj, math.bignumber(ab));
        aj = math.add(aj, math.bignumber(ac));
        aj = math.add(aj, math.bignumber(ad));
        aj = math.add(aj, math.bignumber(ae));
        aj = math.add(aj, math.bignumber(af));
        aj = math.add(aj, math.bignumber(ag));
        aj = math.add(aj, math.bignumber(ah));
        aj = math.add(aj, math.bignumber(ai));
      }
    }
    AJs.push(aj);
    var f = 0;
    if (b !== '' && ((b + 11) % math.divide(12, Pay_Mode_Tbl[Pay_Mode]) === 0)) f = RP;
    var allocIdx = (a < Allo_PremCharge_Tbl.allocation.length ? a : Allo_PremCharge_Tbl.allocation.length) - 1;
    var allocIdxB = (b < Allo_PremCharge_Tbl.allocation.length ? b : Allo_PremCharge_Tbl.allocation.length) - 1;
    if (a > 0) l = (b === '') ? '' : math.multiply(f, Allo_PremCharge_Tbl.allocation[allocIdx]);
    var h = 0;
    var g = 0;
    if (b !== '') {
      h = (d <= 70) ? RSP_Prem : 0;
      g = (b == 1) ? SP_Topup : 0;
    }
    var m = row === 0 ? 0 : h;
    var _n = g;
    var o = b === '' ? '' : math.add(l, math.add(m, _n));
    Ms.push(m);
    Ns.push(_n);
    var al = 0;
    if (row === 0) {
      al = '';
    } else if (row == 1) {
      al = oldAV;
    } else {
      al = a === '' ? 0 : math.add(math.bignumber(oldAV), math.bignumber(o));
    }
    var am = al <= 0 ? 0 : math.subtract(math.bignumber(al), math.add(math.bignumber(aj), ADM));
    var an = Math.max(0, ((math.largerEq(am, rebate_value_low) && math.largerEq(am, rebate_value_med)) ? math.subtract(rebate_value_med, rebate_value_low) : math.subtract(am, rebate_value_low)));
    an = math.multiply(math.bignumber(an), rebate_bonus_low);
    an = math.divide(an, 12);
    var ao = Math.max(0, ((math.largerEq(am, rebate_value_med) && math.largerEq(am, rebate_value_high)) ? math.subtract(rebate_value_high, rebate_value_med) : math.subtract(am, rebate_value_med)));
    ao = math.multiply(math.bignumber(ao), rebate_bonus_med);
    ao = math.divide(ao, 12);
    var ap = math.largerEq(am, rebate_value_high) ? math.subtract(am, rebate_value_high) : 0;
    ap = math.multiply(math.bignumber(ap), rebate_bonus_high);
    ap = math.divide(ap, 12);
    ap = Math.max(0, ap);
    var aq = math.add(math.bignumber(an), math.add(math.bignumber(ao), math.bignumber(ap)));
    var ar = math.add(math.bignumber(am), math.bignumber(aq));
    var as = 0;
    if (row > 0) {
      as = mod_prem[Pay_Mode][c - 1];
      as = math.multiply(as, RP);
      as = math.multiply(as, Allo_PremCharge_Tbl.allocation[allocIdx]);
      as = math.multiply(as, SaleChg);
      if (b == 1) as = math.add(as, math.multiply(SP_Topup, SaleChg));
      if (d <= 70) as = math.add(as, math.multiply(math.multiply(mod_prem[polOpt.rspPayFreq][c - 1], RSP_Prem), SaleChg));
    }
    var at = math.multiply(math.subtract(math.bignumber(ar), math.bignumber(as)), math.subtract(1, math.divide(math.bignumber(Service_Fee), 12)));
    at = math.bignumber(at);
    var ay = Math.pow((math.add(1, PIRR_Low)), math.divide(1, 24));
    ay = math.multiply(math.bignumber(ay), math.bignumber(at));
    ay = math.multiply(ay, math.bignumber(Service_Fee));
    ay = math.divide(ay, 12);
    ay = ay < 0 ? 0 : ay;
    AYs.push(ay);
    var au = 0;
    if (row === 0) {
      au = SP_Topup;
      au = math.add(au, RSP_Prem);
      au = math.add(au, math.multiply(RP, Allo_PremCharge_Tbl.allocation[0]));
    } else {
      au = Math.pow((math.add(1, PIRR_Low)), (math.divide(1, 12)));
      au = math.multiply(math.bignumber(au), math.bignumber(at));
      var auTmp = Math.pow((1 - AMF), math.divide(1, 12));
      au = math.multiply(math.bignumber(au), math.bignumber(auTmp));
    }
    if (isWithdrawal) {
      var withdrawReducer = 0;
      if (b !== '' && (d >= Start_Age && d <= End_Age && (b % (math.divide(12, Pay_Mode_Tbl[Withdrawal_Frequency]))) === 0)) withdrawReducer = Withdrawal;
      au = au - withdrawReducer;
      Ps.push(withdrawReducer);
    }
    var av = (COI_Option > 1 && a > COI_EndYear && au < 0) || b >= 1177 ? 0 : au;
    AVs.push(a > 98 ? 0 : av);
    oldAV = av;
    var i = 0;
    if (b !== '' && (b + 11) % 12 === 0) {
      h = (d <= 70) ? RSP_Prem : 0;
      i = annualised_basicprem;
      if (b == 1) {
        i = math.add(i, math.add(annualised_rsp, SP_Topup));
      } else {
        i = math.add(i, ((h > 0) ? annualised_rsp : 0));
      }
      j = math.add(j, i);
    }
    Is.push(i);
    Js.push(j);
  }
  bd = 1;
  var zeroCnt = 0;
  for (var row = 0; row < n; row++) {
    var b = row;
    var a;
    if (row === 0) {
      a = 0;
    } else {
      a = math.floor(math.divide((b - 1), 12)) + 1;
    }
    var be = '' === bd ? '' : Ds[12 * bd];
    items[row].polYear = bd, items[row].age = (be + 1);
    var gdb = 0,
      ngdb = 0;
    if (bd < n) {
      var changeSaFactor = 1;
      if (isChangeSA) {
        changeSaFactor = math.divide(BBs[12 * (row + 1) - 11], Basic_SA);
      }
      gdb = a < n ? BBs[12 * (row + 1) - 11] : 0, be < MaxAge_LTR_LBR && ('Y' == LTR_Op && (gdb = math.add(gdb, math.multiply(LTR_SA, changeSaFactor))), 'Y' == LBR_Op && (gdb = math.add(gdb, math.multiply(LBR_SA, changeSaFactor))));
      var bj = '' === bd ? '' : Math.max(AVs[12 * bd], 0);
      ngdb = bj;
    }
    var tdb = 0 === COI_Option ? Math.max(gdb, ngdb) : math.add(math.bignumber(gdb), math.bignumber(ngdb)),
      gsv = 0;
    var bn = 0;
    if (!(0 === COI_Option || 1 == COI_Option || 7 == COI_Option) && cvTable[row + 1] && cvTable[row + 1][Math.floor(LA_Age)]) {
      bn = math.bignumber(math.divide(Basic_SA, 1e3));
      bn = math.multiply(bn, math.bignumber(cvTable[row + 1][Math.floor(LA_Age)]));
    }
    var ngsv = math.max(math.add(bn, math.bignumber(ngdb)), 0);
    var tsv = ngsv;
    items[row].tdb = math.number(tdb);
    if (isChangeSA || isWithdrawal) {
      if (tsv <= 0) items[row].tdb = 0;
    } else {
      if (ngdb <= 0) items[row].tdb = 0;
    }
    items[row].gdb = math.number(gdb), items[row].ngdb = math.number(ngdb), items[row].gsv = math.number(gsv), items[row].ngsv = math.number(ngsv), items[row].tsv = math.number(tsv), bd++;
  }
  bd = 1;
  for (var oldBH = 0, row = 0; row < n; row++) {
    var bp = 0;
    if ('' !== bd) {
      var bf = Is[12 * bd - 11];
      var bh = '' === bf || !bf ? '' : math.multiply(math.add(bf, oldBH), math.add(1, PIRR_Low));
      oldBH = bh, items[row].vop = math.number(bh);
      var bn = 0;
      if (!(0 === COI_Option || 1 == COI_Option || 7 == COI_Option) && cvTable[row + 1] && cvTable[row + 1][Math.floor(LA_Age)]) {
        bn = math.bignumber(math.divide(Basic_SA, 1e3));
        bn = math.multiply(bn, math.bignumber(cvTable[row + 1][Math.floor(LA_Age)]));
      }
      var bj_2 = Math.max(AVs[12 * bd], 0);
      var bo = math.add(math.bignumber(bj_2), math.bignumber(bn));
      bo = Math.max(bo, 0);
      bp = math.subtract(math.bignumber(bh), math.bignumber(bo));
      oldBH = bh, items[row].eod = math.number(bp);
    }
    bd++;
  }
  bd = 1;
  var oldBV = 0;
  var basicYr3CoiLv = 0;
  if (COI_Option > 1) {
    basicYr3CoiLv = math.divide(math.multiply(mort_lookup_table[math.floor(LA_Age)], Basic_SA), 1000);
    basicYr3CoiLv = math.bignumber(math.multiply(basicYr3CoiLv, math.add(1, math.bignumber(loading_Basic))));
  }
  var firstYearAJ = 0;
  if (extraParam.PIRR == '0.04' && !isChangeSA && !isWithdrawal) {
    for (var i = 1; i <= 12; i++) {
      firstYearAJ = math.add(firstYearAJ, AJs[i]);
    }
  }
  items.firstYearAJ = firstYearAJ;
  var COI_AP = math.divide(math.add(math.bignumber(extraParam.firstYearAJ), basicYr3CoiLv), annualised_basicprem);
  var key1 = (COI_Option <= 1) ? 'yrt' : 'level';
  var tmpCoiOpt = COI_Option <= 1 ? 0.08 : 0.32;
  var key2 = (COI_AP < tmpCoiOpt) ? 'Smaller' : 'Greater';
  var channelTable = rates['COMM'][Channel] && rates['COMM'][Channel][key1 + key2] ? rates['COMM'][Channel][key1 + key2] : rates['COMM'][Channel];
  for (var row = 0; row < n; row++) {
    var bv = 0;
    if (bd !== '') {
      var bs = 0;
      var bu = 0;
      for (var j = 0; j < 12; j++) {
        var idx = j + row * 12 + 1;
        if (idx < Ms.length) bs = math.add(bs, Ms[idx]);
        if (idx < Ns.length) bs = math.add(bs, Ns[idx]);
        if (idx < AYs.length) bu = math.add(bu, AYs[idx]);
      }
      var channelAmt = 0;
      if (channelTable) {
        var channelTblIdx = bd <= channelTable.length ? bd - 1 : channelTable.length - 1,
          channelAmt = channelTable[channelTblIdx];
      }
      var rspSpComTblIdx = bd <= RSP_SP_COM.Total.length ? bd - 1 : RSP_SP_COM.Total.length - 1,
        bt = math.add(math.bignumber(math.multiply(channelAmt, annualised_basicprem)), math.bignumber(math.multiply(RSP_SP_COM.Total[rspSpComTblIdx], bs)));
      bv = math.add(math.bignumber(oldBV), math.add(math.bignumber(bt), math.bignumber(bu))), oldBV = bv;
    }
    items[row].tdc = bv;
    bd++;
  }
  for (var row = 1; row <= n; row++) {
    var idx = 12 * (row - 1) + 1,
      itemIdx = row - 1;
    items[itemIdx].tpp = math.number(Js[idx]), isChangeSA && (items[itemIdx].sa = math.number(BBs[idx]));
    var amountWithdrawn = 0;
    if (isWithdrawal) {
      for (var b = 0; b < 12; b++) {
        var wdIdx = 12 * row + b + 1;
        amountWithdrawn = amountWithdrawn + Ps[wdIdx] ? Ps[wdIdx] : 0;
      }
      items[itemIdx].amountWithdrawn = amountWithdrawn;
    }
  }
  var yieldCal = [],
    yieldLoop = Math.max(20, 65 - LA_ANB);
  for (i = 0; i < yieldLoop; i++) yieldCal.push(math.multiply(-1, Is[12 * i + 1]));
  yieldCal.push(items[yieldLoop - 1].tsv), items.yieldCal = yieldCal, items.yieldValue = items[yieldLoop - 1].eod, items.yieldAtAge = ((65 - LA_ANB >= 20) ? 65 : (LA_ANB + 20)), items.yieldAtRate = PIRR_Low;
  items.COI_AP = COI_AP;
  return items;
}