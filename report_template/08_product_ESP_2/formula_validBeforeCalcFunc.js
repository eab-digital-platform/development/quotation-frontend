function(quotation, planInfo, planDetails) {
  if (planInfo.covCode === quotation.baseProductCode) {
    var sumInsured = quotation.plans[0].sumInsured;
    if (planInfo.calcBy === 'sumAssured') {
      quotation.plans[0].sumInsured = (sumInsured !== null) ? Math.ceil(sumInsured / 100) * 100 : 0;
    }
    var premium = quotation.plans[0].premium;
    if (planInfo.calcBy === 'premium') {
      if (premium !== null && premium !== undefined) {
        if (planDetails[planInfo.covCode].inputConfig && planDetails[planInfo.covCode].inputConfig.premlim) {
          let min = planDetails[planInfo.covCode].inputConfig.premlim.min;
          if (min && min > premium) {
            quotation.plans[0].premium = min;
          }
        }
      } else {
        quotation.plans[0].premium = 0;
        quotation.plans[0].sumInsured = 0;
      }
    } /**var basicPlanDetails = planDetails[quotation.baseProductCode]; var hasPolicyTerminList = false; if (basicPlanDetails && basicPlanDetails.inputConfig && basicPlanDetails.inputConfig.policyTermList && planInfo.policyTerm) { var policyTermList = basicPlanDetails.inputConfig.policyTermList; for (var i = 0; i < policyTermList.length; i++) { var policyTerm = policyTermList[i]; if (policyTerm && policyTerm.value === planInfo.policyTerm) { hasPolicyTerminList = true; } } if (!hasPolicyTerminList) { planInfo.policyTerm = null; planInfo.polTermDesc = null; planInfo.sumInsured = null; planInfo.premium = null; } }*/
  }
}