function(quotation, planInfo, planDetail, planDetails) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var numberTrunc = function(value, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return Number.parseInt(value);
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return Number.parseFloat(value);
    }
  };
  var roundUp = function(value, digit) {
    var scale = Math.pow(10, digit);
    if (digit === 0) {
      return Math.ceil(value);
    } else {
      return Math.ceil(math.multiply(value, scale)) / scale;
    }
  };
  var round = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  }; /** saFuncWithPlanDetails*/
  var premium = null;
  var sumInsured = null;
  var intermediateSA;
  var validation;
  var intermediatePrem;
  var packageRiderPremium;
  var rateKey;
  if (planInfo.premTerm && planInfo.premium && planInfo.policyTerm) {
    premium = planInfo.premium; /**var previousPremium = runFunc(planDetail.formulas.premFunc, quotation, planInfo, planDetail);*/
    if (quotation.extraFlags.previousPremium !== premium) {
      quotation.extraFlags.previousInputPremium = planInfo.premium;
      premium = math.bignumber(planInfo.premium);
    } else { /** premium = math.add(math.bignumber(planInfo.premium), math.bignumber(quotation.plans[1].premium || 0));*/
      premium = quotation.extraFlags.previousInputPremium;
    }
    var paymentFactor = math.bignumber(runFunc(planDetail.formulas.getPaymentModeFactor, planDetail, quotation.paymentMode));
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
    var packagedRiderPlanDetail = planDetails['ESR'];
    var riderPremRate = math.bignumber(runFunc(packagedRiderPlanDetail.formulas.getPremRate, quotation, planInfo, packagedRiderPlanDetail, quotation.iAge));
    var lsdSA = planDetail.rates.LSD.SA;
    for (var i in planDetail.rates.LSD) {
      var lsdPremTerm = Number(i);
      if (typeof planInfo.premTerm === 'number' && planInfo.premTerm == lsdPremTerm) {
        rateKey = lsdPremTerm;
      }
    }
    var firstLSDiscountPremRate = planDetail.rates.LSD[rateKey + ''][1];
    var secondDiscountPremRate = planDetail.rates.LSD[rateKey + ''][2];
    var firstDiscountPrem = math.multiply(math.multiply(math.add(math.subtract(premRate, firstLSDiscountPremRate), riderPremRate), math.divide(lsdSA[1], 1000)), paymentFactor);
    var secondDiscountPrem = math.multiply(math.multiply(math.add(math.subtract(premRate, secondDiscountPremRate), riderPremRate), math.divide(lsdSA[2], 1000)), paymentFactor);
    var lsdArray = [];
    lsdArray.push(firstDiscountPrem);
    lsdArray.push(secondDiscountPrem);
    var rateIndex = 0;
    for (var i in lsdArray) {
      if (math.number(premium) >= math.number(lsdArray[i])) {
        rateIndex = Number.parseInt(i) + 1;
      }
    }
    var intermediateLSD = planDetail.rates.LSD[rateKey][rateIndex];
    intermediateSA = roundUp(math.divide(math.multiply(math.divide(premium, paymentFactor), 1000), math.add(roundDown(math.subtract(premRate, intermediateLSD), 2), riderPremRate)), -2);
    var basicTemplsd = math.bignumber(runFunc(planDetail.formulas.getLSD, planDetail, intermediateSA, planInfo.premTerm));
    var intermediateBasicPrem = round(math.divide(numberTrunc(math.multiply(roundDown(math.subtract(premRate, basicTemplsd), 2), intermediateSA), 2), 1000), 2);
    var intermediateTPDPrem = numberTrunc(math.divide(math.multiply(riderPremRate, intermediateSA), 1000), 2);
    intermediatePrem = math.add(intermediateBasicPrem, intermediateTPDPrem);
    validation = !((math.number(intermediateSA) < math.number(lsdSA[1]) && math.number(intermediatePrem) >= math.number(firstDiscountPrem)) || (math.number(intermediateSA) < math.number(lsdSA[2]) && math.number(intermediatePrem) >= math.number(secondDiscountPrem)));
    var basicSA = (validation) ? intermediateSA : roundUp(intermediateSA, -3);
    var lsd = math.bignumber(runFunc(planDetail.formulas.getLSD, planDetail, basicSA, planInfo.premTerm));
    premRate = roundDown(math.subtract(premRate, lsd), 2);
    var prem = round(math.divide(numberTrunc(math.multiply(premRate, basicSA), 2), 1000), 2);
    premium = math.number(prem);
  } else {
    planInfo.sumInsured = null;
    if (quotation.plans && quotation.plans[0]) {
      quotation.plans[0].sumInsured = null;
      quotation.plans[0].premium = null;
    }
  }
  planInfo.premium = prem;
  return basicSA;
}