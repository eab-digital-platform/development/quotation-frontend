function(planDetail, quotation) {
  var premTermList = [];
  var values = [5, 10];
  for (var v in values) {
    var value = values[v];
    premTermList.push({
      value: value,
      title: value + ' Years'
    });
  }
  return premTermList;
}