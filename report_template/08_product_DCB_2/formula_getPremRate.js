function(quotation, planInfo, planDetail, age) {
  var rateKey = quotation.policyOptions.planType === 'renew' ? 'DCBR_' : 'DCBNR_';
  rateKey += quotation.iGender;
  rateKey += quotation.iSmoke === 'Y' ? 'S' : 'NS';
  var term;
  if (quotation.policyOptions.planType === 'renew') {
    term = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - age;
  } else {
    term = Number.parseInt(planInfo.policyTerm);
  }
  var rates = planDetail.rates.premRate[rateKey];
  var premRate = math.bignumber(rates[term][age] || 0);
  var substdClsMapping = planDetail.rates.substdCls[quotation.iResidence];
  if (substdClsMapping) {
    var substdCls = substdClsMapping[quotation.iResidenceCity] || substdClsMapping.ALL;
    if (substdCls) {
      var substdRates = planDetail.rates.residentialLoading[rateKey + '_' + substdCls];
      if (substdRates) {
        premRate = math.add(premRate, substdRates[Number.parseInt(planInfo.policyTerm)][age] || 0);
      }
    }
  }
  return math.number(premRate);
}