function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  var saMax = {
    SGD: 100000,
    USD: 69000,
    EUR: 64000,
    GBP: 48000,
    AUD: 95000
  };
  var bpSa = quotation.plans[0].sumInsured;
  var saMax = saMax[quotation.ccy];
  planDetail.inputConfig.benlim = {
    max: bpSa ? Math.min(bpSa / 10, saMax) : saMax,
    min: 5000
  };
}