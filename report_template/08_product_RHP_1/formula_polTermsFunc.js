function(planDetail, quotation) {
  const PAYOUT_TERM_LIFETIME = 99;
  var policyTermList = [];
  if (quotation.policyOptions.retirementAge === undefined) {
    return null;
  }
  if (quotation.policyOptions.payoutTerm === undefined) {
    return null;
  }
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var retirementAge = parseInt(quotation.policyOptions.retirementAge);
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  var benefitTerm = retirementAge;
  var policyTerm = 0;
  var polTermDesc = '';
  if (payoutTerm == PAYOUT_TERM_LIFETIME) {
    policyTerm = 99;
    polTermDesc = 'Until Age 99';
  } else {
    benefitTerm += payoutTerm;
    benefitTerm -= quotation.iAge;
    policyTerm = benefitTerm;
    polTermDesc = benefitTerm + ' Years';
  } /**Needed so that RHP policy term will appear in UI.*/
  quotation.plans[0].policyTerm = policyTerm; /**Needed so that TPD premium term will appear in UI.*/
  quotation.plans[1].premTerm = quotation.plans[0].premTerm;
  policyTermList.push({
    value: policyTerm,
    title: polTermDesc,
    default: true
  });
  return policyTermList;
}