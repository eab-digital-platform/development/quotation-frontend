function(quotation, planInfo, planDetails, extraPara) { /*BAND AID product summary prepareReportData*/
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb,
  } = extraPara.company;
  var retVal = {
    'compName': compName,
    'compRegNo': compRegNo,
    'compAddr': compAddr,
    'compAddr2': compAddr2,
    'compTel': compTel,
    'compFax': compFax,
    'compWeb': compWeb
  };
  var planFields = [];
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    var planName = plan.covName.en;
    var postfix = plan.saPostfix ? " (" + plan.saPostfix.en + ")" : "";
    var info = {
      planName: planName + postfix
    };
    planFields.push(info);
  }
  var plans = quotation.plans;
  retVal.planFields = planFields;
  return retVal;
}