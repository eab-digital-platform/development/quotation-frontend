function(quotation, planInfo, planDetails, extraPara) {
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var roundneo = function(value, position) {
    var output = value;
    if (output) {
      var valueStr = value.toString();
      var valueSplit = valueStr.split('.');
      if (valueSplit && valueSplit.length == 2) {
        if (valueSplit[0] && valueSplit[1] && valueSplit[1].length > position) {
          output = math.bignumber(valueSplit[0] + '.' + valueSplit[1].substr(0, position));
          var roundUpVal = math.bignumber(0);
          var roundCheck = Number(valueSplit[1].substr(position, 1));
          if (roundCheck >= 5) {
            roundUpVal = '1';
            while (roundUpVal.length < position) {
              roundUpVal = '0' + roundUpVal;
            }
            roundUpVal = math.bignumber('0.' + roundUpVal);
            output = math.add(output, roundUpVal);
          }
        }
      }
    }
    return math.number(output);
  };
  var illustrations = extraPara.illustrations;
  var basicPlan = quotation.plans[0];
  var basicIllustrations = illustrations[basicPlan.covCode];
  var wd = [],
    _wd = [];
  var sv = [],
    _sv = [];
  var sa = [],
    _sa = [];
  var polTerm = basicPlan.policyTerm ? basicPlan.policyTerm : 15;
  var projections = planDetails[planInfo.covCode].projection;
  var low = 999;
  var high = -1;
  for (var i = 0; i < 2; i++) {
    var id = projections[i].title.en;
    low = Math.min(Number(id), low);
    high = Math.max(Number(id), high);
  }
  var lastRow = 99 - quotation.iAge;
  var saLowZeroCnt = 0;
  var saHighZeroCnt = 0;
  var wdLowZeroCnt = 0;
  var wdHighZeroCnt = 0;
  var nonGteedSV_lowValue, totalDB_lowValue, nonGteedSV_highValue, totalDB_highValue;
  for (var i = 0; i < lastRow; i++) {
    var polYr = i + 1;
    var row = {};
    var basicIllustration = basicIllustrations[i];
    var age = basicIllustration.age;
    var polYr_age = polYr + '/' + age;
    var totalPremPaid = getCurrency(round(Number(basicIllustration.totalPremPaid), 0), ' ', 0);
    var wdStartYr = quotation.policyOptions.wdFromAge;
    if (age >= wdStartYr) {
      var wdrow = {
        polYr: polYr,
        age: age,
        amountWithdrawn: getCurrency(round(Number(basicIllustration.amountWithdrawn), 0), ' ', 0),
        nonGteedSV_low: getCurrency(round(wdLowZeroCnt > 0 ? 0 : Number(basicIllustration.nonGteedSV_withdrawal[low]), 0), ' ', 0),
        totalDB_low: getCurrency(round(Number(basicIllustration.totalDB_withdrawal[low]), 0), ' ', 0),
        nonGteedSV_high: getCurrency(round(wdHighZeroCnt > 0 ? 0 : Number(basicIllustration.nonGteedSV_withdrawal[high]), 0), ' ', 0),
        totalDB_high: getCurrency(round(Number(basicIllustration.totalDB_withdrawal[high]), 0), ' ', 0),
      };
      if (polYr != lastRow) {
        if (age <= (wdStartYr + 40)) {
          if ((age - wdStartYr) < 20 || (age - wdStartYr) % 5 == 4) {
            if (basicIllustration.nonGteedSV_withdrawal[low] <= 0) wdLowZeroCnt++;
            if (basicIllustration.nonGteedSV_withdrawal[high] <= 0) wdHighZeroCnt++;
          }
          if ((age - wdStartYr) < 20) {
            wd.push(wdrow);
          } else if ((age - wdStartYr) % 5 == 4) {
            wd.push(wdrow);
          }
        }
      } else {
        _wd.push(Object.assign({}, wdrow, {
          polYr_age: 'age ' + age
        }));
      }
    }
    nonGteedSV_lowValue = saLowZeroCnt > 0 ? 0 : round(Number(basicIllustration.nonGteedSV_changeSA[low]), 0);
    if (nonGteedSV_lowValue === 0) {
      totalDB_lowValue = 0;
    } else {
      totalDB_lowValue = round(Number(basicIllustration.totalDB_changeSA[low]), 0);
    }
    nonGteedSV_highValue = saHighZeroCnt > 0 ? 0 : round(Number(basicIllustration.nonGteedSV_changeSA[high]), 0);
    if (nonGteedSV_highValue === 0) {
      totalDB_highValue = 0;
    } else {
      totalDB_highValue = round(Number(basicIllustration.totalDB_changeSA[high]), 0);
    }
    var sarow = {
      polYr: polYr,
      sa: getCurrency(round(Number(basicIllustration.sa), 0), ' ', 0),
      nonGteedSV_low: getCurrency(nonGteedSV_lowValue, ' ', 0),
      totalDB_low: getCurrency(totalDB_lowValue, ' ', 0),
      nonGteedSV_high: getCurrency(nonGteedSV_highValue, ' ', 0),
      totalDB_high: getCurrency(totalDB_highValue, ' ', 0),
    };
    var svrow = {
      polYr: polYr,
      totalPremPaid: totalPremPaid,
      GteedSV: getCurrency(round(Number(basicIllustration.GteedSV), 0), ' ', 0),
      nonGteedSV_low: getCurrency(round(Number(basicIllustration.nonGteedDB[low]), 0), ' ', 0),
      totalSV_low: getCurrency(round(Number(basicIllustration.totalSV[low]), 0), ' ', 0),
      nonGteedSV_high: getCurrency(round(Number(basicIllustration.nonGteedDB[high]), 0), ' ', 0),
      totalSV_high: getCurrency(round(Number(basicIllustration.totalSV[high]), 0), ' ', 0),
    };
    if (polYr <= 20 || (polYr > 20 && polYr <= 40 && polYr % 5 === 0) || polYr == lastRow) {
      if (basicIllustration.nonGteedSV_changeSA[low] <= 0) saLowZeroCnt++;
      if (basicIllustration.nonGteedSV_changeSA[high] <= 0) saHighZeroCnt++;
      sa.push(sarow);
      sv.push(svrow);
    }
    if (age == 55 || age == 60 || age == 65) {
      _sa.push(Object.assign({}, sarow, {
        polYr: 'AGE ' + age
      }));
      _sv.push(Object.assign({}, svrow, {
        polYr: 'AGE ' + age
      }));
    }
  }
  wd.push({});
  sv.push({});
  sa.push({});
  for (var i in _sa) {
    sv.push(_sv[i]);
    sa.push(_sa[i]);
  }
  for (var i in _wd) {
    wd.push(_wd[i]);
  }
  return {
    illustrateData_WD: wd,
    illustrateData_COI: sv,
    illustrateData_SA: sa,
  };
}