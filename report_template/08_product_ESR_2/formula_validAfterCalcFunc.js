function(quotation, planInfo, planDetail) { /**ESR validAfterCalcFunc*/
  quotValid.validateMandatoryFields(quotation, planInfo, planDetail);
  if (planInfo.calcBy) {
    var totalPrem = 0;
    for (var i = 0; i < quotation.plans.length; i++) {
      var plan = quotation.plans[i];
      if (plan.premium) {
        totalPrem = math.add(totalPrem, math.bignumber(plan.premium));
      }
    }
    totalPrem = math.number(totalPrem);
    if (planDetail.inputConfig && planDetail.inputConfig.premlim) {
      let min = planDetail.inputConfig.premlim.min;
      if (min && min > totalPrem) {
        var err = {
          covCode: planInfo.covCode,
          msgPara: [],
          code: ''
        };
        err.msgPara.push(getCurrency(min, '$', 2));
        err.code = 'js.err.invalid_premlim_min';
        err.msgPara.push(totalPrem);
        quotDriver.context.addError(err);
      }
    }
  }
  var planCodeMap = quotDriver.runFunc(planDetail.formulas.getPlanCodeMapping, planInfo);
  if (planInfo.policyTerm) {
    planInfo.planCode = planCodeMap;
  }
  if (typeof quotation.plans[0].sumInsured != 'undefined') {
    planInfo.sumInsured = quotation.plans[0].sumInsured;
  }
  if (!planInfo.sumInsured) {
    planInfo.premium = planInfo.yearPrem = planInfo.halfYearPrem = planInfo.quarterPrem = planInfo.monthPrem = planInfo.sumInsured;
  }
}