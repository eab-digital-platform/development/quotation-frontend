function(quotation, planInfo, planDetails) {
  var planDetail = planDetails[planInfo.covCode];
  var basicPlanDetail = planDetails[quotation.plans[0].covCode];
  if (quotation.plans[0] && quotation.plans[0].premTerm) {
    planInfo.premTerm = quotation.plans[0].premTerm;
  }
  if (quotation.plans[0] && quotation.plans[0].policyTerm) {
    planInfo.policyTerm = quotation.plans[0].policyTerm;
  }
  if (quotation.plans[0].premium) {
    quotCalc.calcPlanPrem(quotation, planInfo, planDetails);
  }
}