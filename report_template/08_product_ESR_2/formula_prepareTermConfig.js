function(planDetail, quotation, planDetails) {
  let basicDetail = planDetails[quotation.baseProductCode];
  let polTermList = basicDetail.inputConfig.policyTermList;
  let premTermList = basicDetail.inputConfig.premTermList;
  planDetail.inputConfig.canEditPolicyTerm = false;
  planDetail.inputConfig.policyTermList = polTermList;
  planDetail.inputConfig.canEditPremTerm = false;
  planDetail.inputConfig.premTermList = premTermList;
  if (basicDetail && basicDetail.policyTerm) {
    planDetail.policyTerm = basicDetail.policyTerm;
  } else {
    planDetail.policyTerm = null;
  }
  if (basicDetail && basicDetail.premTerm) {
    planDetail.premTerm = basicDetail.premTerm;
  } else {
    planDetail.premTerm = null;
  }
}