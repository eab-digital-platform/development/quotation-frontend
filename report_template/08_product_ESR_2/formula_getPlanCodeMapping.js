function(planInfo) {
  if (planInfo.premTerm < 10) {
    return '0' + planInfo.premTerm + 'ESR' + planInfo.policyTerm;
  } else {
    return planInfo.premTerm + 'ESR' + planInfo.policyTerm;
  }
}