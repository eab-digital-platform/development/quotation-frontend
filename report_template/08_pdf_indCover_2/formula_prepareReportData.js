function(quotation, planInfo, planDetails, extraPara) {
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  let company = extraPara.company;
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb
  } = company;
  var retVal = {};
  if (company) {
    var retVal = {
      'compName': compName,
      'compRegNo': compRegNo,
      'compAddr': compAddr,
      'compAddr2': compAddr2,
      'compTel': compTel,
      'compFax': compFax,
      'compWeb': compWeb,
    };
  }
  retVal.proposerName = quotation.pFullName;
  retVal.proposerGenderTitle = (quotation.pGender) == 'M' ? 'Male' : 'Female';
  retVal.proposerSmokerTitle = (quotation.pSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  retVal.proposerAge = quotation.pAge;
  retVal.insurerGenderTitle = (quotation.iGender) == 'M' ? 'Male' : 'Female';
  retVal.insurerSmokerTitle = (quotation.iSmoke) == 'Y' ? 'Smoker' : 'Non-Smoker';
  var ccy = quotation.ccy;
  var polCcy = '';
  var ccySyb = '';
  if (ccy == 'SGD') {
    polCcy = 'Singapore Dollars';
    ccySyb = 'S$';
  } else if (ccy == 'USD') {
    polCcy = 'US Dollars';
    ccySyb = 'US$';
  } else if (ccy == 'ASD') {
    polCcy = 'Australian Dollars';
    ccySyb = 'A$';
  } else if (ccy == 'EUR') {
    polCcy = 'Euro';
    ccySyb = '€';
  }
  retVal.polCcy = polCcy;
  retVal.ccySyb = ccySyb;
  var planDetail = planDetails[quotation.baseProductCode];
  var paymentModeTitle = '';
  if (quotation.paymentMode == 'L') {
    paymentModeTitle = 'Single Premium (SP)';
  }
  retVal.paymentModeTitle = paymentModeTitle;
  var plans = quotation.plans;
  var polOpt = quotation.policyOptions;
  var paymentMethod = polOpt.paymentMethod;
  retVal.rspSalesCharges = polOpt.rspSalesCharges;
  retVal.funds = quotation.fund ? quotation.fund.funds : [];
  retVal.planCodes = quotation.plans[0] ? quotation.plans[0].planCode : '';
  var planFields = [];
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    var policyTerm = plan.policyTerm;
    var covCode = plan.covCode;
    var sumAssured = trunc(math.bignumber(quotation.sumInsured), 0);
    var policyTermList = planDetail.inputConfig.policyTermList;
    var planInd = planDetail.planInd;
    var planName = !plan.covName ? "" : (typeof plan.covName == 'string' ? plan.covName : (plan.covName.en || plan.covName[Object.keys(plan.covName)[0]]));
    var polTermTitle = -1;
    for (var j = 0; j < policyTermList.length; j++) {
      var polTermOpt = policyTermList[j];
      if (polTermOpt.value == policyTerm) polTermTitle = polTermOpt.title;
    }
    if (polTermTitle != -1) {
      var polTerm = {
        covCode: covCode,
        planInd: planInd,
        policyTerm: polTermTitle,
        permTerm: polTermTitle,
        sumAssured: getCurrency(quotation.sumInsured, '', 0),
        planName: planName,
        rsp: polOpt.rspAmount ? getCurrency(polOpt.rspAmount, '', 0) : '-',
        isp: planInfo.premium ? getCurrency(planInfo.premium, '', 0) : '-'
      };
      planFields.push(polTerm);
    }
  }
  for (var po in quotation.policyOptions) {
    var configPolOpts = planDetail.inputConfig.policyOptions;
    for (var j = 0; j < configPolOpts.length; j++) {
      var configPolOpt = configPolOpts[j];
      if (configPolOpt.id === po && configPolOpt.options) {
        var opts = configPolOpt.options;
        for (var k = 0; k < opts.length; k++) {
          var opt = opts[k];
          if (opt.value == quotation.policyOptions[po]) {
            retVal[po] = opt.title;
            break;
          }
        }
        break;
      }
    }
    if (!retVal[po]) retVal[po] = '-';
  }
  retVal.planFields = planFields;
  retVal.genDate = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.backDate = new Date(quotation.riskCommenDate).format(extraPara.dateFormat);
  retVal.iDob = new Date(extraPara.systemDate).format(extraPara.dateFormat);
  retVal.proposerDob = new Date(quotation.pDob).format(extraPara.dateFormat);
  retVal.insurerDob = new Date(quotation.iDob).format(extraPara.dateFormat);
  return retVal;
}