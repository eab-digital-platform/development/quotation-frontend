function(quotation, planInfo, planDetails) {
  var basicPlan = quotation.plans[0];
  var idx = -1;
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    if (plan.covCode == planInfo.covCode) {
      idx = i;
      break;
    }
  }
  if (idx > 0) {
    var selfPlan = quotation.plans[idx];
    if (Number(quotation.pAge) + Number(basicPlan.policyTerm) > 65) {
      planInfo.policyTerm = 'toAge65';
      selfPlan.policyTerm = 'toAge65';
    } else {
      planInfo.policyTerm = basicPlan.policyTerm;
      selfPlan.policyTerm = basicPlan.policyTerm;
    }
    planInfo.premTerm = planInfo.policyTerm;
    selfPlan.premTerm = selfPlan.policyTerm;
  }
}