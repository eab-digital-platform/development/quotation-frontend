function(planDetail, quotation, planDetails) {
  planDetail.inputConfig.canEditPolicyTerm = true;
  planDetail.inputConfig.policyTermList = quotDriver.runFunc(planDetail.formulas.polTermsFunc, planDetail, quotation);
  planDetail.inputConfig.canEditPremTerm = false;
  planDetail.inputConfig.premTermList = quotDriver.runFunc(planDetail.formulas.premTermsFunc, planDetail, quotation);
  const plan = _.find(quotation.plans, p => p.covCode === planDetail.covCode);
  if (plan) {
    plan.premTerm = plan.policyTerm;
  }
}