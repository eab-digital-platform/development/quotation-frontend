function(quotation, planInfo, planDetails) {
  var baseProductCode = quotation.baseProductCode;
  if (baseProductCode === 'ESP') {
    planInfo.policyTerm = quotation.plans[0].premTerm + '_YR';
    planInfo.premTerm = planInfo.policyTerm;
    if (quotation.plans[0].premium) {
      quotCalc.calcPlanPrem(quotation, planInfo, planDetails);
    }
  } else {
    var planDetail = planDetails[planInfo.covCode];
    var planInfo = _.find(quotation.plans, p => p.covCode === planDetail.covCode);
    if (quotation.policyOptions.planType === 'renew' && planDetail.inputConfig && planDetail.inputConfig.policyTermList && planDetail.inputConfig.policyTermList[0]) {
      planInfo.policyTerm = planDetail.inputConfig.policyTermList[0].value;
    }
    planInfo.premTerm = planInfo.policyTerm;
    if (quotation.plans[0].premium) {
      quotCalc.calcPlanPrem(quotation, planInfo, planDetails);
    }
  }
}