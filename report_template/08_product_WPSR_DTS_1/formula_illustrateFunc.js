function(quotation, planInfo, planDetails, extraPara) {
  var planDetail = planDetails[planInfo.covCode];
  var MAX_POLICY_YEAR = 100;
  var rates = planDetail.rates;
  var commissionTable = rates.comm;
  var commRate = 0;
  var policyTerm = planInfo.policyTerm;
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var convertBignumberToReadable = function(data) {
    for (var row in data) {
      for (var col in data[row]) {
        if (typeof(data[row][col]) !== 'string') {
          data[row][col] = math.number(data[row][col]);
        }
      }
    }
  };
  var calcIllustration = function(illustration) {
    var sustainTestResult = true;
    var rawData = [];
    var defaultRawData = {
      policyYear: 0,
      accumPremium: math.bignumber(0),
      tdc: math.bignumber(0)
    };
    rawData.push(Object.assign({}, defaultRawData));
    for (var year = 1; year <= MAX_POLICY_YEAR; year++) {
      var currData = Object.assign({}, defaultRawData);
      var prevData = rawData[year - 1];
      currData.policyYear = year;
      var currPrem = math.bignumber(year <= planInfo.premTerm ? planInfo.yearPrem : 0);
      currData.accumPremium = math.add(currPrem, prevData.accumPremium);
      var row = year;
      if (row > 7) {
        row = 7;
      }
      commRate = commissionTable[row][policyTerm - 10];
      var currTdc = math.multiply(math.bignumber(commRate), currPrem);
      currData.tdc = math.add(currTdc, prevData.tdc);
      var illData = illustration[year - 1];
      illData.policyYear = currData.policyYear;
      illData.accumPremium = currData.accumPremium;
      illData.tdc = currData.tdc;
      rawData.push(currData);
    }
    convertBignumberToReadable(rawData);
    return sustainTestResult;
  };
  var illustration = [];
  var illustrationData = {
    policyYear: 0,
    accumPremium: 0,
    tdc: 0
  };
  for (var y = 0; y < MAX_POLICY_YEAR; y++) {
    illustration.push(Object.assign({}, JSON.parse(JSON.stringify(illustrationData))));
  }
  calcIllustration(illustration);
  return illustration;
}