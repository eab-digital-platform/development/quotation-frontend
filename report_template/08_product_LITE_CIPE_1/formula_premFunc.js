function(quotation, planInfo, planDetail) { /*LITE_premFunc*/
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (planInfo.premTerm && sumInsured) {
    var residenceLoading = math.bignumber(runFunc(planDetail.formulas.getResidenceLoading, quotation, planInfo, planDetail));
    var rates = planDetail.rates;
    var countryCode = quotation.iResidence;
    var countryGroup = rates.countryGroup[countryCode];
    if (Array.isArray(countryGroup)) {
      countryGroup = countryGroup[0];
    }
    var substandardClass = rates.substandardClass[countryGroup]['CIPE'];
    if (Array.isArray(substandardClass)) {
      substandardClass = substandardClass[0];
    }
    var loadingFactor = math.bignumber(rates.WPRLoadingFactor[substandardClass]);
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
    var premRateMultiplyLoadingFactor = math.round(math.multiply(premRate, loadingFactor), 2);
    var premAfterLsd = roundDown(math.multiply(premRateMultiplyLoadingFactor, math.bignumber(sumInsured), math.bignumber(0.01)), 2);
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
        break;
      }
    }
    premium = trunc(math.multiply(premAfterLsd, modalFactor), 2);
    premium = math.number(premium);
  }
  return premium;
}