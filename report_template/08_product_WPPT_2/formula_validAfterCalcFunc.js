function(quotation, planInfo, planDetail) {
  if (planInfo.policyTerm) {
    var year = planInfo.policyTerm.substring(3, planInfo.policyTerm.length);
    if (year.length < 2) {
      year = '0' + year;
    }
    planInfo.planCode = year + "WPPT";
    planInfo.policyTermYr = math.number(year);
    planInfo.premTermYr = math.number(year);
  }
  planInfo.premTerm = planInfo.policyTerm;
  planInfo.premTermDesc = planInfo.polTermDesc;
}