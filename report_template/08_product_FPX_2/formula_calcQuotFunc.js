function(quotation, planInfo, planDetails) {
  var planDetail = planDetails[planInfo.covCode];
  var cPlanInfo = Object.assign({}, planInfo);
  var saInput = planDetail.inputConfig.saInput;
  var premInput = planDetail.inputConfig.premInput;
  var closestPrem = null;
  var closestSA = null;
  var trunc = function(value, position) {
    if (!value) return null;
    var sign = value < 0 ? -1 : 1;
    if (!position) position = 0;
    var scale = math.pow(10, position);
    return math.multiply(sign, Number(math.divide(math.floor(math.multiply(math.abs(value), scale)), scale)));
  };
  if (planInfo.sumInsured) {
    closestSA = math.number(planInfo.sumInsured || 0);
    closestSA = math.number(math.multiply(math.ceil(math.divide(math.number(closestSA), 100)), 100));
  }
  if (planInfo.premium) {
    closestPrem = math.number(planInfo.premium || 0);
    if (premInput && premInput.decimal) {
      var scale = math.pow(10, premInput.decimal);
      closestPrem = math.number(math.divide(math.floor(math.multiply(math.number(closestPrem), scale)), scale));
    }
    if (premInput && premInput.factor) {
      var factor = math.number(premInput.factor);
      closestPrem = math.number(math.multiply(math.ceil(math.divide(math.number(closestPrem), factor)), factor));
    }
  }
  planInfo.premium = closestPrem;
  planInfo.sumInsured = closestSA;
  var annPrem = quotCalc.getAnnPrem(quotation, planInfo, planDetail) || 0;
  var decimal = 0;
  var factor = 0;
  if (premInput && premInput.length > 0) {
    for (var i in premInput) {
      if (premInput[i].ccy === quotation.ccy) {
        decimal = premInput[i].decimal;
        factor = premInput[i].factor;
        break;
      }
    }
  }
  for (var i in planDetail.payModes) {
    var payment = planDetail.payModes[i];
    var prem = payment.operator == "D" ? math.divide(math.number(annPrem), payment.factor) : math.multiply(math.number(annPrem), payment.factor) || 0;
    if (decimal) {
      var scale = math.pow(10, decimal);
      prem = math.divide(math.floor(math.multiply(prem, scale)), scale);
    }
    prem = trunc(math.number(prem), 2);
    if (payment.mode == 'L' || payment.mode == 'A') {
      planInfo.yearPrem = prem;
      quotation.totYearPrem = math.number(math.add(math.number(quotation.totYearPrem || 0), prem));
    } else if (payment.mode == 'S') {
      planInfo.halfYearPrem = prem;
      quotation.totHalfyearPrem = math.number(math.add(math.number(quotation.totHalfyearPrem || 0), prem));
    } else if (payment.mode == 'Q') {
      planInfo.quarterPrem = prem;
      quotation.totQuarterPrem = math.number(math.add(math.number(quotation.totQuarterPrem || 0), prem));
    } else if (payment.mode == 'M') {
      planInfo.monthPrem = prem;
      quotation.totMonthPrem = math.number(math.add(math.number(quotation.totMonthPrem || 0), prem));
    }
  }
  quotation.premium = math.number(math.add(math.number(quotation.premium || 0), closestPrem));
  quotation.sumInsured = math.number(math.add(math.number(quotation.sumInsured || 0), closestSA));
}