function(quotation, planInfo, planDetails) {
  const CALC_TYPE_INPUT_PREMIUM = 1;
  const CALC_TYPE_INPUT_RETIRE_INC = 2;
  var premiumTerm = quotation.plans[0].premTerm;
  var TPD_annualPremium = quotation.plans[1].annualPremium;
  var sumAssuredNonTPD = quotation.plans[1].sumAssuredNonTPD;

  function calculateRiderPremiumEraserTotal(curPlanInfo) { /** Basic plan has premTerm but no RI or premium inputs yet*/
    if ((quotation.plans[0].premTerm) && (quotation.plans[0].premium === undefined)) {
      curPlanInfo.policyTerm = premiumTerm;
      curPlanInfo.polTermDesc = premiumTerm + ' Years';
      curPlanInfo.premTerm = premiumTerm;
      curPlanInfo.premTermDesc = premiumTerm + ' Years';
      curPlanInfo.premium = NaN; /** force UI to be blank*/
      return;
    } /** Basic plan has no premterm yet*/
    if ((quotation.plans[0].premTerm === undefined) || (quotation.plans[0].premium === undefined) || (quotation.plans[0].premium === NaN)) {
      if (curPlanInfo.premium) {
        curPlanInfo.premium = NaN; /** force UI to be blank*/
      }
      return;
    }
    var LEP2_WOP_rates = planDetails['PET_RHP'].rates.LEP2_WOP_rates;
    var ref = '';
    if (premiumTerm == 5) {
      ref += '05WOP';
    } else {
      ref = premiumTerm + 'WOP';
    }
    var planCode = ref.slice(0);
    ref += quotation.iGender;
    ref += quotation.iSmoke;
    var get_LEP2_WOP_rate = function(ref, insuredAge) {
      var planRates = LEP2_WOP_rates[ref];
      if (planRates == null) {
        return 0;
      }
      return planRates[insuredAge];
    };
    var wopRate = get_LEP2_WOP_rate(ref, quotation.iAge);
    var temp = Math.floor(wopRate * sumAssuredNonTPD * 100) / 100; /* TRUNCATE to 2 decimal places*/
    temp = temp / 100;
    temp = Math.round(temp * 100) / 100; /* ROUND to 2 decimal places*/
    var annualPremium = temp; /** Update planInfo*/
    curPlanInfo.planCode = planCode;
    curPlanInfo.sumAssured = sumAssuredNonTPD;
    curPlanInfo.policyTerm = premiumTerm;
    curPlanInfo.polTermDesc = premiumTerm + ' Years';
    curPlanInfo.premTerm = premiumTerm;
    curPlanInfo.premTermDesc = premiumTerm + ' Years';
    curPlanInfo.yearPrem = annualPremium;
    curPlanInfo.halfYearPrem = Math.floor(annualPremium * 0.51 * 100) / 100;
    curPlanInfo.quarterPrem = Math.floor(annualPremium * 0.26 * 100) / 100;
    curPlanInfo.monthPrem = Math.floor(annualPremium * 0.0875 * 100) / 100;
    quotation.totYearPrem += curPlanInfo.yearPrem;
    quotation.totHalfyearPrem += curPlanInfo.halfYearPrem;
    quotation.totQuarterPrem += curPlanInfo.quarterPrem;
    quotation.totMonthPrem += curPlanInfo.monthPrem;
    switch (quotation.paymentMode) {
      case 'A':
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
      case 'S':
        {
          curPlanInfo.premium = curPlanInfo.halfYearPrem;quotation.premium += curPlanInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          curPlanInfo.premium = curPlanInfo.quarterPrem;quotation.premium += curPlanInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          curPlanInfo.premium = curPlanInfo.monthPrem;quotation.premium += curPlanInfo.monthPrem;
          break;
        }
      default:
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
    }
  }
  calculateRiderPremiumEraserTotal(planInfo);
}