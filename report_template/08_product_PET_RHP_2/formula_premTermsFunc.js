function(planDetail, quotation) {
  var premTermsList = [];
  var premiumTerm = quotation.plans[0].premTerm;
  if (premiumTerm == undefined) {
    return premTermsList;
  }
  if (quotation.paymentMode == 'L') {
    premTermsList.push({
      value: 1,
      title: 'Single Premium',
      default: true
    });
  } else {
    premTermsList.push({
      value: premiumTerm,
      title: premiumTerm + ' Years',
      default: true
    });
  }
  return premTermsList;
}