function(planDetail, quotation) {
  var policyTermList = [];
  var hasTa65 = false;
  var hasTa75 = false;
  var hasTa99 = false;
  _.each([10, 15, 20, 25], (val) => {
    if (quotation.iAge + val <= 80) {
      policyTermList.push({
        value: '' + val,
        title: val + ' years'
      });
      if (quotation.iAge + val === 65) {
        hasTa65 = true;
      }
      if (quotation.iAge + val === 75) {
        hasTa75 = true;
      }
      if (quotation.iAge + val === 99) {
        hasTa99 = true;
      }
    }
  });
  if (quotation.iAge < 65) {
    policyTermList.push({
      value: '65',
      title: 'To Age 65'
    });
  }
  if (quotation.iAge < 75) {
    policyTermList.push({
      value: '75',
      title: 'To Age 75'
    });
  }
  if (quotation.iAge < 99) {
    policyTermList.push({
      value: '99',
      title: 'To Age 99'
    });
  }
  return policyTermList;
}