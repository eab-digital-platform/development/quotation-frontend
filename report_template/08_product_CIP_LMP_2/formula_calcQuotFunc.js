function(quotation, planInfo, planDetails) {
  if (planDetails['CIP_LMP'].inputConfig) {
    if (planDetails['CIP_LMP'].inputConfig.premTermList) {
      var array = planDetails['CIP_LMP'].inputConfig.premTermList;
      for (var i = 0; i < array.length; i++) {
        if (Number.parseInt(array[i].value) == Number.parseInt(planInfo.policyTerm)) {
          planInfo.premTerm = array[i].value;
          planInfo.premTermDesc = array[i].title;
        }
      }
    }
  }
  if (quotation.policyOptions.multiFactor && planInfo.premTerm) { /* planCode needed for prem calculation */
    var planCode = 'CRBP';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    var mbCodeMap = {
      "5": "A",
      "3.5": "B",
      "2.5": "C"
    };
    var mbCodeMapOlder = {
      "2.50": "A",
      "2.75": "B",
      "1.25": "C"
    };
    var group = quotation.iAge < 71 ? mbCodeMap[quotation.policyOptions.multiFactor] : mbCodeMapOlder[quotation.policyOptions.multiFactor];
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode;
    } else {
      planCode = planCode + Number.parseInt(planInfo.premTerm);
    }
    planInfo.planCode = planCode;
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails['CIP_LMP'].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer < 6) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer < 10) {
            crate = commission_rate[rate][1];
          } else if (policyYearRefer < 15) {
            crate = commission_rate[rate][2];
          } else if (policyYearRefer < 20) {
            crate = commission_rate[rate][3];
          } else if (policyYearRefer < 25) {
            crate = commission_rate[rate][4];
          } else {
            crate = commission_rate[rate][5];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
      }
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}