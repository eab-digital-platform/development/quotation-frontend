function(quotation, planInfo, planDetail, age) {
  var rates = planDetail.rates;
  var countryCode = quotation.iResidence;
  var countryGroup = rates.countryGroup[countryCode];
  if (Array.isArray(countryGroup)) {
    countryGroup = countryGroup[0];
  }
  var substandardClass = rates.substandardClass[countryGroup]['CRBP'];
  if (Array.isArray(substandardClass)) {
    substandardClass = substandardClass[0];
  }
  var rateKey = planInfo.planCode + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + substandardClass;
  console.log('Critical Illness Plus get residenceLoading rateKey = ' + rateKey);
  if (planDetail.rates.residentialLoading[rateKey] && planDetail.rates.residentialLoading[rateKey][quotation.iAge]) {
    return planDetail.rates.residentialLoading[rateKey][quotation.iAge];
  }
  return 0;
}