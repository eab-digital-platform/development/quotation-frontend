function(quotation, planInfo, planDetails, extraParam) {
  var trunc = function(value, position) {
    return runFunc(planDetails[planInfo.covCode].formulas.trunc, value, position);
  };
  var projRate = extraParam.tbl_Bonus.ProjectInvestReturn[0];
  var tbl_Bonus = extraParam.tbl_Bonus;
  var MatYrCB = extraParam.tbl_Bonus.CashBack[0];
  var netCashFlow = extraParam.netCashFlow;
  var Premium_Lookup = extraParam.Premium_Lookup;
  var baseSA = quotation.plans[0].sumInsured;
  var polTerm = planInfo.policyTerm,
    premTerm = polTerm;
  var totalPremPaid = math.bignumber(0),
    sumGteedAnnualCashback = math.bignumber(0),
    oldAA = math.bignumber(0),
    oldAccumCashAdv = math.bignumber(0),
    oldAccumCashback = math.bignumber(0),
    oldAccumAtCBinterest = math.bignumber(0),
    oldVOP = math.bignumber(0),
    oldTotalDistribCost = math.bignumber(0),
    oldGteedAnnualCashback = math.bignumber(0);
  var illustration = [];
  var projections = planDetails[planInfo.covCode].projections;
  var tdc = '';
  if (quotation.agent.dealerGroup == 'AGENCY' || quotation.agent.dealerGroup == 'SYNERGY') tdc = 'AGY_SYN';
  else if (quotation.agent.dealerGroup == 'BROKER' || quotation.agent.dealerGroup == 'FUSION') tdc = 'BKR_FUS';
  else if (quotation.agent.dealerGroup == 'SINGPOST') tdc = 'SINGPOST';
  else if (quotation.agent.dealerGroup == 'DIRECT') tdc = 'DIRECT';
  var rates = planDetails[planInfo.covCode].rates,
    tbl_GCV = rates.CASH_VALUE.GUARANTEED,
    tbl_NGCV = rates.CASH_VALUE.NON_GUARANTEED,
    tbl_CashAdv = rates.CASH_ADVANCE,
    tbl_TDC = rates.TDC[tdc];
  for (var i = 0; i < polTerm; i++) {
    var polYr = i + 1;
    var isIEqualBT = (i == polTerm - 1) ? 1 : 0;
    isIEqualBT = math.bignumber(isIEqualBT);
    var premiumsPaid = math.bignumber(Premium_Lookup);
    totalPremPaid = math.bignumber(math.add(totalPremPaid, premiumsPaid));
    var GteedDB = math.bignumber(math.multiply(totalPremPaid, (polYr <= 2 ? 1.01 : 1.05)));
    var rbCompound = math.bignumber(tbl_Bonus['CRB_' + polTerm][i]);
    var RB = math.bignumber(tbl_Bonus['RB_' + polTerm][i]);
    var db_nonGtdRB = math.bignumber(trunc(math.add(trunc(math.multiply(oldAA, math.add(1, rbCompound)), 2), trunc(math.multiply(RB, baseSA), 0)), 0));
    oldAA = db_nonGtdRB;
    var sv_nonGtdAccOnCB = math.bignumber(0);
    var NGCVrate = math.bignumber(tbl_NGCV['T' + polTerm + 'Y' + math.add(i, 1) + quotation.iGender][quotation.iAge]);
    var CashAdvRate = math.bignumber(i > tbl_CashAdv[polTerm + 'PT'].length - 1 ? 0 : math.bignumber(tbl_CashAdv[polTerm + 'PT'][i]));
    var GCVrate = math.bignumber(tbl_GCV['T' + polTerm + 'Y' + math.add(i, 1) + quotation.iGender][quotation.iAge]);
    var surrBonus = math.bignumber(tbl_Bonus['Surr TB_' + polTerm][i]);
    var maturityBonus = math.bignumber(math.multiply(math.bignumber(tbl_Bonus['Mat TB_' + polTerm][i]), isIEqualBT));
    var sv_nonGtdRB = math.bignumber(trunc(math.divide(math.multiply(NGCVrate, db_nonGtdRB), math.bignumber(100)), 2));
    var sv_nonGtdTB = math.bignumber(math.add(trunc(math.multiply(trunc(math.divide(math.multiply(NGCVrate, db_nonGtdRB), math.bignumber(100)), 2), surrBonus), 2), trunc(math.multiply(db_nonGtdRB, maturityBonus), 2)));
    var GteedSV = math.bignumber(trunc(math.divide(math.multiply(GCVrate, baseSA), math.bignumber(1000)), 0));
    var nonGteedSV = math.bignumber(trunc(math.add(math.add(sv_nonGtdRB, sv_nonGtdTB), sv_nonGtdAccOnCB), 2));
    var totalSV = math.bignumber(math.add(GteedSV, nonGteedSV));
    var totalDB = math.bignumber(Math.max(totalSV, GteedDB));
    var nonGteedDB = math.bignumber(math.subtract(totalDB, GteedDB));
    var VOP = math.bignumber(trunc(math.multiply(math.add(premiumsPaid, oldVOP), math.add(1, math.bignumber(projRate))), 2));
    oldVOP = VOP;
    var gtdCashback = CashAdvRate;
    var GteedAnnualCashback = math.bignumber(trunc(math.multiply(gtdCashback, baseSA)));
    sumGteedAnnualCashback = math.bignumber(math.add(sumGteedAnnualCashback, GteedAnnualCashback));
    var netcash = math.bignumber(math.subtract(oldGteedAnnualCashback, premiumsPaid));
    netCashFlow.push(netcash);
    oldGteedAnnualCashback = GteedAnnualCashback;
    var accumCashAdv = math.bignumber(math.add(trunc(math.multiply(oldAccumCashAdv, math.add(1, math.bignumber(projRate))), 2), GteedAnnualCashback));
    oldAccumCashAdv = accumCashAdv;
    var effectOfDeduct = math.bignumber(trunc(math.subtract(VOP, math.subtract(math.add(totalSV, accumCashAdv), GteedAnnualCashback)), 2));
    var nonGtdCashback = math.bignumber(0);
    var P = math.bignumber(trunc(math.multiply(nonGtdCashback, baseSA), 0));
    var accumCashback = math.bignumber(math.add(math.add(trunc(math.multiply(oldAccumCashback, math.add(1, MatYrCB)), 2), GteedAnnualCashback), P));
    oldAccumCashback = accumCashback;
    var accumAtCBinterest = math.bignumber(math.add(math.add(trunc(math.multiply(oldAccumAtCBinterest, math.add(1, MatYrCB)), 2), GteedAnnualCashback), P));
    oldAccumAtCBinterest = accumAtCBinterest;
    var interestOnCashAdvOpt = math.bignumber(math.add(trunc(math.subtract(accumCashback, sumGteedAnnualCashback), 2), isIEqualBT ? math.subtract(accumAtCBinterest, accumCashback) : 0));
    var totalSBsupp = math.bignumber(math.subtract(sumGteedAnnualCashback, GteedAnnualCashback));
    var tdc_factor = [];
    for (var j = 0; j < 6; j++) tdc_factor.push(tbl_TDC[polTerm + 'PT'][j]);
    var totalDistribCost = 0;
    if (i >= 6) {
      totalDistribCost = oldTotalDistribCost;
    } else {
      totalDistribCost = math.bignumber(math.add(trunc(math.multiply(math.bignumber(tdc_factor[i]), math.bignumber(Premium_Lookup)), 2), oldTotalDistribCost));
    }
    oldTotalDistribCost = totalDistribCost;
    var nonGteedDB_SUPP = math.bignumber(math.add(math.bignumber(nonGteedDB), math.bignumber(interestOnCashAdvOpt)));
    var nonGteedSV_SUPP = math.bignumber(math.add(math.bignumber(nonGteedSV), math.bignumber(interestOnCashAdvOpt)));
    var row = {
      'totalPremPaid': math.number(totalPremPaid),
      'GteedDB': math.number(GteedDB),
      'nonGteedDB': math.number(nonGteedDB),
      'totalDB': math.number(totalDB),
      'GteedSV': math.number(GteedSV),
      'nonGteedSV': math.number(nonGteedSV),
      'totalSV': math.number(totalSV),
      'VOP': math.number(VOP),
      'effectOfDeduct': math.number(effectOfDeduct),
      'totalSBsupp': math.number(totalSBsupp),
      'totalDistribCost': math.number(totalDistribCost),
      'GteedAnnualCashback': math.number(GteedAnnualCashback),
      'nonGteedDB_SUPP': math.number(nonGteedDB_SUPP),
      'nonGteedSV_SUPP': math.number(nonGteedSV_SUPP),
      'pol_yr': polYr,
      'age': (quotation.iAge + polYr)
    };
    illustration.push(row);
  }
  return illustration;
}