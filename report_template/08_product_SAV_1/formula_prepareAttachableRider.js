function(planDetail, quotation, planDetails) {
  var riderList = [];
  if (planDetail.riderList && planDetail.riderList.length) {
    var validRider = 0;
    for (var r in planDetail.riderList) {
      var rider = planDetail.riderList[r];
      if (rider.condition && rider.condition.length) {
        for (var c in rider.condition) {
          var cond = rider.condition[c];
          if ((!quotation.iResidence || cond.country == '*' || quotation.iResidence == cond.country) && (!quotation.dealerGroup || cond.dealerGroup == '*' || quotation.dealerGroup == cond.dealerGroup) && (!cond.endAge || quotation.pAge <= cond.endAge) && (!cond.staAge || quotation.pAge >= cond.staAge)) {
            var riderDetail = planDetails[rider.covCode];
            if (riderDetail) {
              validRider = {};
              for (var rd in rider) {
                if (rd != 'condition') {
                  validRider[rd] = rider[rd];
                }
              }
              for (var c in cond) {
                if (c != 'country' && c != 'dealerGroup' && c != 'staAge' && c != 'endAge' && c != 'amount') {
                  validRider[c] = cond[c];
                }
              }
              if (cond.amount) {
                for (var a in cond.amount) {
                  var amt = cond.amount[a];
                  if (quotation.ccy == amt.ccy) {
                    validRider = Object.assign(validRider, amt);
                    break;
                  }
                }
              }
              if (validRider) {
                if (quotation.sameAs == "Y" && rider.covCode == "PET") riderList.push(validRider);
                let extraFlags = quotation.extraFlags;
                if (extraFlags && extraFlags.nationalityResidence) {
                  let iRejected = extraFlags.nationalityResidence.iRejected;
                  let pRejected = extraFlags.nationalityResidence.pRejected;
                  if (quotation.sameAs == "N" && (rider.covCode == "PPERA" || rider.covCode == "PPEPS") && !(iRejected == 'N' && pRejected == 'Y')) riderList.push(validRider);
                }
              }
            }
          }
        }
      }
    }
    if (!planDetail.inputConfig) planDetail.inputConfig = {};
    planDetail.inputConfig.riderList = riderList;
  }
  return planDetail.inputConfig.riderList;
}