function(quotation, planInfo, planDetail) {
  for (var i in quotation.plans) {
    var plan = quotation.plans[i];
    if (plan.covCode == "LAB") {
      var sa = math.number(plan.sumInsured || 0);
      var bsa = math.number(quotation.plans[0].sumInsured || 0);
      if (sa == bsa) {
        quotDriver.context.addError({
          covCode: planDetail.covCode,
          code: "Waiver of Premium Special cannot be attached when Living Accelerator Rider Sum Assured is equal to Basic Sum Assured"
        });
      }
    }
  }
  if (planInfo.policyTerm) {
    var type = planInfo.policyTerm.substring(0, 3);
    var year = planInfo.policyTerm.substring(3, planInfo.policyTerm.length);
    if (type == "yrs") {
      planInfo.planCode = year + "WPSR";
    } else {
      planInfo.planCode = "WPSR" + year;
    }
  }
  planInfo.premTerm = planInfo.policyTerm;
  planInfo.premTermDesc = planInfo.polTermDesc;
  if (planInfo.policyTerm) {
    var type = planInfo.policyTerm.substring(0, 3);
    if (type === "yrs") {
      planInfo.policyTermYr = math.number(planInfo.policyTerm.substring(3, planInfo.policyTerm.length));
      planInfo.premTermYr = math.number(planInfo.policyTermYr);
    } else {
      planInfo.policyTermYr = math.number(planInfo.policyTerm.substring(3, planInfo.policyTerm.length)) - quotation.iAge;
      planInfo.premTermYr = math.number(planInfo.policyTermYr);
    }
  }
}