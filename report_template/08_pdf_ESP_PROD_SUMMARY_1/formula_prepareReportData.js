function(quotation, planInfo, planDetails, extraPara) {
  let {
    compName,
    compRegNo,
    compAddr,
    compAddr2,
    compTel,
    compFax,
    compWeb,
  } = extraPara.company;
  var retVal = {
    'compName': compName,
    'compRegNo': compRegNo,
    'compAddr': compAddr,
    'compAddr2': compAddr2,
    'compTel': compTel,
    'compFax': compFax,
    'compWeb': compWeb
  };
  var plans = quotation.plans;
  var basicPlan = plans[0];
  var additionalPlanTitle = ' ' + basicPlan.premTerm.toString() + ' PAY';
  var infos = [];
  plans.forEach(function(plan, i) {
    let info = {};
    var covCode = plan.covCode;
    if (covCode === 'ESP' || covCode === 'ESR') {
      info.newPlanName = plan.covName.en + additionalPlanTitle;
    } else {
      info.newPlanName = plan.covName.en;
    }
    infos.push(info);
  });
  retVal.newPlans = infos;
  return retVal;
}