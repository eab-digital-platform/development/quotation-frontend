function(planDetail, quotation) {
  var policyTermList = null;
  var foundDefault = false;
  if (planDetail.polMaturities) {
    policyTermList = [];
    for (var p in planDetail.polMaturities) {
      var mat = planDetail.polMaturities[p];
      if (!mat.country || mat.country == '*' || mat.country == quotation.residence && mat.minTerm && mat.maxTerm) {
        var interval = mat.interval || 1;
        for (var m = mat.minTerm; m <= mat.maxTerm && (!mat.maxAge || !quotation.iAge || (m + quotation.iAge) <= mat.maxAge) && (!mat.ownerMaxAge || !quotation.pAge || (m + quotation.pAge) <= mat.ownerMaxAge); m += interval) {
          foundDefault |= (mat.default === m);
          policyTermList.push({
            "value": m + "",
            "title": m + " Years",
            "default": mat.default === m
          });
        }
      }
    }
    policyTermList.push({
      "value": 'toAge65',
      "title": 'To Age 65',
      "default": false
    });
  } else if (planDetail.wholeLifeInd && planDetail.wholeLifeInd == 'Y' && planDetail.wholeLifeAge) {
    return [{
      "value": "999",
      "title": "term.swhole_life",
      "default": true
    }];
  }
  return policyTermList;
}