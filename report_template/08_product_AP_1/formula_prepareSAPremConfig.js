function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  var saMax = {
    SGD: 1000000,
    USD: 699000,
    EUR: 649000,
    GBP: 480000,
    AUD: 952000
  };
  var bpSa = quotation.plans[0].sumInsured;
  var saMax = saMax[quotation.ccy];
  planDetail.inputConfig.benlim = {
    max: bpSa ? Math.min(bpSa * 10, saMax) : saMax,
    min: 20000
  };
}