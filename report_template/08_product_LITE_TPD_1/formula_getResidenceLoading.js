function(quotation, planInfo, planDetail) {
  var planCode = "MBT";
  var premTermYr = "";
  if (planInfo.premTerm.indexOf('YR') > -1) {
    planCode = Number.parseInt(planInfo.premTerm) + planCode;
    premTermYr = Number.parseInt(planInfo.premTerm);
  } else {
    planCode = planCode + Number.parseInt(planInfo.premTerm);
    premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
  }
  var MB_codeList = {
    "2": "A",
    "3": "B",
    "4": "C",
    "5": "D",
    "6": "E",
    "7": "F"
  };
  var MB_code = MB_codeList[quotation.policyOptions.multiFactor];
  var rates = planDetail.rates;
  var countryCode = quotation.iResidence;
  var countryGroup = rates.countryGroup[countryCode];
  if (Array.isArray(countryGroup)) {
    countryGroup = countryGroup[0];
  }
  var substandardClass = rates.substandardClass[countryGroup]['MBT'];
  if (Array.isArray(substandardClass)) {
    substandardClass = substandardClass[0];
  }
  var rateKey = planCode + MB_code + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + substandardClass;
  if (planDetail.rates.residentialLoading[rateKey] && planDetail.rates.residentialLoading[rateKey][quotation.iAge]) {
    return planDetail.rates.residentialLoading[rateKey][quotation.iAge];
  }
  return 0;
}