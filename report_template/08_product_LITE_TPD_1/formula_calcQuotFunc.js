function(quotation, planInfo, planDetails) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    var temp = math.round(math.multiply(value, 10000));
    temp = math.divide(temp, 10000);
    return math.divide(math.floor(math.multiply(temp, scale)), scale);
  };
  if (quotation.plans[0].premTerm) {
    planInfo.premTerm = quotation.plans[0].premTerm;
    planInfo.premTermDesc = quotation.plans[0].premTermDesc;
  } /*var max_value = 0; if(quotation.iResidence == 'R2'){ max_value = 6000000; }else{ max_value = 4000000; } if(quotation.plans[0].sumInsured > max_value){ planInfo.sumInsured = max_value; }else{ planInfo.sumInsured = quotation.plans[0].multiplyBenefit; }*/
  planInfo.sumInsured = quotation.plans[0].multiplyBenefit;
  if (quotation.policyOptions.multiFactor && planInfo.premTerm && quotation.policyOptions.multiFactor != 1) { /* planCode needed for prem calculation */
    var planCode = 'MBT';
    var planCode2 = 'LITE_TPD';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode;
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planCode = planCode + Number.parseInt(planInfo.premTerm);
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    var MB_codeList = {
      "2": "A",
      "3": "B",
      "4": "C",
      "5": "D",
      "6": "E",
      "7": "F"
    };
    var MB_code = MB_codeList[quotation.policyOptions.multiFactor];
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    planInfo.planCode = planCode + MB_code;
    planInfo.packagedRider = true;
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails[planCode2].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer < 15) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer < 20) {
            crate = commission_rate[rate][1];
          } else if (policyYearRefer < 25) {
            crate = commission_rate[rate][2];
          } else {
            crate = commission_rate[rate][3];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
        var SA_bundleInfo = {
          "2": 50000,
          "3": 33333.34,
          "4": 25000,
          "5": 20000,
          "6": 16666.67,
          "7": 14285.72
        };
        var SA_policy_lookup = 100000;
        var bundle_lsd = 0;
        var premRate = math.bignumber(runFunc(planDetails['LITE_TPD'].formulas.getPremRate, quotation, planInfo, planDetails['LITE_TPD'], quotation.iAge));
        var process_premRate = math.add(premRate, bundle_lsd);
        var annualPrem_bundle = roundDown(math.multiply(0, process_premRate, SA_policy_lookup, 0.001), 2);
        var modalFactor = 1;
        for (var p in planDetails['LITE_TPD'].payModes) {
          if (planDetails['LITE_TPD'].payModes[p].mode === quotation.paymentMode) {
            modalFactor = planDetails['LITE_TPD'].payModes[p].factor;
            break;
          }
        }
        var modlePrem_bundle = roundDown(math.multiply(annualPrem_bundle, modalFactor), 2);
        planInfo.annualPrem_bundle = math.number(annualPrem_bundle);
        planInfo.modlePrem_bundle = math.number(modlePrem_bundle);
        planInfo.bundleSA = SA_policy_lookup;
      }
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}