function(quotation, planInfo, planDetail) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (planInfo.premTerm && sumInsured) {
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
    var premBeforeLsd = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
    var lsd = runFunc(planDetail.formulas.getLsd, planDetail, sumInsured);
    var premAfterLsd = math.subtract(premBeforeLsd, roundDown(math.multiply(premRate, sumInsured, 0.001, lsd), 2));
    var substdCls = planDetail.rates.substdCls[quotation.iResidence];
    var emLoading = substdCls === '1B' ? math.bignumber(0.5) : 0;
    var residenceLoading = math.round(math.multiply(math.divide(premAfterLsd, sumInsured), emLoading, 1000), 2);
    premAfterLsd = math.add(premAfterLsd, roundDown(math.multiply(residenceLoading, sumInsured, 0.001), 2));
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
      }
    }
    var prem = roundDown(math.multiply(premAfterLsd, modalFactor), 2);
    premium = math.number(prem);
  }
  return premium;
}