function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation, planDetails);
  planDetail.inputConfig.canEditSumAssured = true;
  planDetail.inputConfig.canViewSumAssured = true;
  planDetail.inputConfig.canViewPremium = false;
  let basicSaMax = _.get(quotation, 'plans[0].sumInsured', 0) * 10;
  planDetail.inputConfig.benlim = {
    min: 10000,
    max: basicSaMax > 10000 ? math.min(basicSaMax, 500000) : 500000
  };
  planDetail.inputConfig.saInput.factor = 100;
  return planDetail;
}