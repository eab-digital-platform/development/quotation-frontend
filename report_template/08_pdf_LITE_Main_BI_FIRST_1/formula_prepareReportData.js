function(quotation, planInfo, planDetails, extraPara) { /*lmp*/
  var round = function(value, position) {
    var num = Number(value);
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(num, scale)), scale);
  };
  var basicPlan = quotation.plans[0];
  var company = extraPara.company;
  var get_currency_symbol = () => {
    var ccy = quotation.ccy;
    var ccySyb = '';
    if (ccy == 'SGD') {
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      ccySyb = '£';
    }
    return ccySyb;
  };
  var get_policy_currency = () => {
    var ccy = quotation.ccy;
    var polCcy = '';
    var ccySyb = '';
    if (ccy == 'SGD') {
      polCcy = 'Singapore Dollars';
      ccySyb = 'S$';
    } else if (ccy == 'USD') {
      polCcy = 'US Dollars';
      ccySyb = 'US$';
    } else if (ccy == 'ASD') {
      polCcy = 'Australian Dollars';
      ccySyb = 'A$';
    } else if (ccy == 'EUR') {
      polCcy = 'Euro';
      ccySyb = '€';
    } else if (ccy == 'GBP') {
      polCcy = 'British Pound';
      ccySyb = '£';
    }
    return polCcy;
  };
  var is_plan_selected = (covCode) => {
    let plans = quotation.plans;
    for (let i in plans) {
      if (plans[i].covCode == covCode) {
        return true;
      }
    }
    return false;
  };
  var conditions_init = () => {
    var residencyCountryOptions, residencyCityOptions, iRCountry = "",
      iRCity = "",
      pRCountry = "",
      pRCity = "",
      iOtherCountry = "",
      pOtherCountry = "";
    let multiFactor = quotation.policyOptions.multiFactor;
    residencyCountryOptions = extraPara.optionsMap.residency.options;
    residencyCityOptions = extraPara.optionsMap.city.options;
    if (quotation.iResidence === 'R128') {
      iOtherCountry = 'Y';
    }
    if (quotation.pResidence === 'R128') {
      pOtherCountry = 'Y';
    }
    for (let v in residencyCityOptions) {
      if (residencyCityOptions[v].value === quotation.pResidenceCity) {
        pRCity = residencyCityOptions[v].title.en;
      }
      if (residencyCityOptions[v].value === quotation.iResidenceCity) {
        iRCity = residencyCityOptions[v].title.en;
      }
    }
    for (let v in residencyCountryOptions) {
      if (residencyCountryOptions[v].value === quotation.pResidence) {
        pRCountry = residencyCountryOptions[v].title.en;
      }
      if (residencyCountryOptions[v].value === quotation.iResidence) {
        iRCountry = residencyCountryOptions[v].title.en;
      }
    }
    var show_suppBiPage = 'Y';
    var isMultiplierBenefitSelected = 'N';
    if (multiFactor !== "1") {
      isMultiplierBenefitSelected = 'Y';
    }
    return {
      same_as: quotation.sameAs,
      iOtherCountry,
      pOtherCountry,
      pRCity,
      iRCity,
      pRCountry,
      iRCountry,
      show_suppBiPage,
      isMultiplierBenefitSelected
    };
  };
  var cover_init = () => {
    return {
      genDate: new Date().format(extraPara.dateFormat)
    };
  };
  var proposer_init = () => {
    return {
      assured_name: quotation.pFullName,
      assured_gender: quotation.pGender == "M" ? "Male" : "Female",
      assured_age: quotation.pAge,
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      comm_date: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      assured_dob: new Date(quotation.pDob).format(extraPara.dateFormat),
      assured_smoke: quotation.pSmoke == "N" ? "Non-Smoker" : "Smoker"
    };
  };
  var life_assured_init = () => {
    return {
      l_assured_name: quotation.iFullName,
      l_assured_gender: quotation.iGender == "M" ? "Male" : "Female",
      l_assured_age: quotation.iAge,
      l_comm_date: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      l_assured_dob: new Date(quotation.iDob).format(extraPara.dateFormat),
      l_assured_smoke: quotation.iSmoke == "N" ? "Non-Smoker" : "Smoker"
    };
  };
  var basic_plan_init = () => {
    let multiFactor = quotation.policyOptions.multiFactor;
    let multiFactorDesc = quotation.policyOptionsDesc.multiFactor.en;
    if (multiFactor === "1") {
      multiFactorDesc = quotation.policyOptionsDesc.multiFactor.en;
    }
    let ADB_ind = is_plan_selected("LITE_ADB") ? "Y" : "N";
    let showOccupationClass = ADB_ind === "Y" ? "Y" : "N";
    let occupationClass = showOccupationClass == "Y" ? quotation.policyOptionsDesc.occupationClass : "";
    let basicPlanName = quotation.plans[0].covName.en;
    return {
      paymentModeDesc: (quotation.paymentMode == "A" ? "Annual" : (quotation.paymentMode == "S" ? "Semi-Annual" : (quotation.paymentMode == "Q" ? "Quarterly" : "Monthly"))),
      policy_currency: get_policy_currency(),
      currency_symbol: get_currency_symbol(),
      multiFactorDesc,
      showOccupationClass,
      occupationClass,
      basicPlanName
    };
  };
  var getPremiumFromCovcode = (plans, covCode) => {
    var returnPremium = {};
    plans.map((plan) => {
      if (plan.covCode === covCode) {
        returnPremium.yearPrem = plan.yearPrem;
        returnPremium.halfYearPrem = plan.halfYearPrem;
        returnPremium.quarterPrem = plan.quarterPrem;
        returnPremium.monthPrem = plan.monthPrem;
      }
    });
    return returnPremium;
  };
  var your_plan_init = () => {
    let multiFactor = quotation.policyOptions.multiFactor;
    let multiFactorDesc = quotation.policyOptionsDesc.multiFactor.en;
    let seqsMapping = {
      "LITE": 1,
      "LITE_MBX": 2,
      "LITE_CIB": 3,
      "LITE_MBCI": 4,
      "LITE_ECIB": 5,
      "LITE_MBECI": 6,
      "LITE_ADB": 7,
      "LITE_DSB": 8,
      "LITE_CRBP": 9,
      "LITE_CIPE": 10,
      "LITE_SPPE": 11,
      "LITE_SPPEP": 12,
      "LITE_TPD": -1,
      "LITE_TPDD": -1
    };
    let yourPlans = quotation.plans.map((plan) => {
      let covCode = plan.covCode;
      let seq = seqsMapping[covCode] ? seqsMapping[covCode] : 99;
      if (seq > -1) {
        if (!plan.sumInsured) {
          plan.sumInsured = 0;
        }
        let sum_assured = "";
        let multiplyBenefit = "";
        let planName = plan.covName.en;
        let yearPrem = plan.yearPrem; /*if(covCode==="LITE"){ let returnPremium = getPremiumFromCovcode(quotation.extraPlans,"LITE_TPDD"); if(returnPremium.yearPrem){ yearPrem += returnPremium.yearPrem; } } if(covCode==="LITE_MBX"){ let returnPremium = getPremiumFromCovcode(quotation.extraPlans,"LITE_TPD"); if(returnPremium.yearPrem){ yearPrem += returnPremium.yearPrem; } } */
        let annualPremium = getCurrency(yearPrem, ' ', 2);
        let padding = "N";
        if (covCode === "LITE_MBX" || covCode === "LITE_MBCI" || covCode === "LITE_MBECI") {
          padding = "Y";
        }
        sum_assured = getCurrency(round(plan.sumInsured, 0), '', 0);
        if (covCode === "LITE_CIPE" || covCode === "LITE_SPPE" || covCode === "LITE_SPPEP") {
          sum_assured = "-";
        }
        return {
          covCode,
          name: planName,
          polTermDesc: plan.polTermDesc,
          premTermDesc: plan.premTermDesc,
          sum_assured: sum_assured,
          annualPremium,
          padding,
          seq
        };
      }
    }).filter((plan) => {
      return plan !== undefined;
    });
    yourPlans.sort(function(a, b) {
      return a.seq - b.seq
    });
    return {
      plans: yourPlans
    };
  };
  var summary_multiplier_benefit_init = () => {
    var outputPlans = [];
    quotation.plans.map((plan) => {
      if (!plan.sumInsured) {
        plan.sumInsured = 0;
      }
      let sumInsured = "";
      let multiplyBenefit = "";
      let covCode = plan.covCode;
      let planName = plan.covName.en;
      let title = "";
      if (covCode === "LITE") {
        title = "Death / Terminal Illness / Total and Permanent Disability";
      }
      if (covCode === "LITE_CIB" || covCode === "LITE_ECIB") {
        title = planName;
      }
      sumInsured = getCurrency(round(plan.sumInsured, 0), '', 0);
      multiplyBenefit = getCurrency(round(plan.multiplyBenefit, 0), '', 0);
      let resultPlan = {
        title,
        sumInsured,
        multiplyBenefit
      };
      if (covCode === "LITE" || covCode === "LITE_CIB" || covCode === "LITE_ECIB") {
        outputPlans.push(resultPlan);
      }
    });
    return {
      plans: outputPlans,
      multiFactor: quotation.policyOptionsDesc.multiFactor.en
    };
  };
  var your_premium_init = () => {
    var filterPlans = quotation.plans.filter(function(plan, index, array) {
      return plan.yearPrem != null && plan.halfYearPrem != null && plan.quarterPrem != null && plan.monthPrem != null;
    });
    return {
      plans: filterPlans.map((plan) => {
        let covCode = plan.covCode;
        let planName = plan.covName.en;
        return {
          covCode,
          name: planName,
          annual_pre: getCurrency(plan.yearPrem, ' ', 2),
          semi_annual_pre: getCurrency(plan.halfYearPrem, ' ', 2),
          quarterly_pre: getCurrency(plan.quarterPrem, ' ', 2),
          monthly_pre: getCurrency(plan.monthPrem, ' ', 2),
        };
      })
    };
  };
  var total_premium_init = () => {
    let totYearPrem = quotation.totYearPrem;
    let totHalfyearPrem = quotation.totHalfyearPrem;
    let totQuarterPrem = quotation.totQuarterPrem;
    let totMonthPrem = quotation.totMonthPrem; /*let returnPremiumTPD = getPremiumFromCovcode(quotation.extraPlans,"LITE_TPD"); if(returnPremiumTPD.yearPrem && returnPremiumTPD.halfYearPrem && returnPremiumTPD.quarterPrem && returnPremiumTPD.monthPrem){ totYearPrem += returnPremiumTPD.yearPrem; totHalfyearPrem += returnPremiumTPD.halfYearPrem; totQuarterPrem += returnPremiumTPD.quarterPrem; totMonthPrem += returnPremiumTPD.monthPrem; } */
    return {
      total_annual_pre: getCurrency(totYearPrem, ' ', 2),
      total_semi_annual_pre: getCurrency(totHalfyearPrem, ' ', 2),
      total_quarterly_pre: getCurrency(totQuarterPrem, ' ', 2),
      total_monthly_pre: getCurrency(totMonthPrem, ' ', 2)
    };
  };
  var footer_init = () => {
    let planCodes = "";
    let numPlans = quotation.plans.length;
    let multiFactor = quotation.policyOptions.multiFactor;
    quotation.plans.forEach((plan, i) => {
      let covCode = plan.covCode;
      if (covCode !== "LITE_TPDD") {
        if ((multiFactor !== "1" && covCode === "LITE_TPD") || covCode !== "LITE_TPD") {
          if (i != 0) {
            planCodes += ', ';
          }
          planCodes += plan.planCode;
        }
      }
      if (covCode === "LITE_MBX") {
        let planCode = plan.planCode;
        if (planCode.indexOf('MBD') > -1) {
          planCode = planCode.replace('MBD', 'MBT');
          if (i != 0) {
            planCodes += ', ';
          }
          planCodes += planCode;
        }
      }
    });
    return {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: planCodes
    };
  };
  var others_init = () => {
    var first_illustrations = extraPara.illustrations[planInfo.covCode][0];
    return {
      age65orLater: first_illustrations.fairAgeOld,
      lowIRR: 3.25,
      highIRR: 4.75,
      YTSLow: getCurrency(first_illustrations.fairYTSLow, ' ', 2),
      YTSHigh: getCurrency(first_illustrations.fairYTSHigh, ' ', 2)
    };
  };
  var illustration_init = () => {
    var iAge = quotation.iAge;
    var illustrations = extraPara.illustrations[planInfo.covCode];
    var deathBenefit = [];
    var surrenderValue = [];
    var maturityValue = [];
    var deduction = [];
    var totalDistributionCostMainBi = [];
    var deathBenefitLast = [];
    var surrenderValueLast = [];
    var deductionsLast = [];
    var totalDistributionCostLast = [];
    var totalDistributionCostSuppBi = [];
    var totalDistributionCostSuppBiLast = [];
    var deathBenefitRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      multiplierBenefit: "",
      guaranteedDeathBenefit: "",
      nonGuaranteedDeathBenefitLow: "",
      totalDeathBenefitLow: "",
      nonGuaranteedDeathBenefitHigh: "",
      totalDeathBenefitHigh: ""
    };
    var surrenderValueRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      guaranteed: "",
      nonGuaranteedLow: "",
      totalLow: "",
      nonGuaranteedHigh: "",
      totalHigh: ""
    };
    var deductionsRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      valueOfPremiumPaidToDateLow: "",
      effectOfDeductionToDateLow: "",
      totalSurrenderValueLow: "",
      valueOfPremiumPaidToDateHigh: "",
      effectOfDeductionToDateHigh: "",
      totalSurrenderValueHigh: ""
    };
    var totalDistributionCostRowDefault = {
      policyYearAge: "",
      totalPremiumPaidToDate: "",
      totalDistributionCostToDate: ""
    };
    var Annual_Premium = 0;
    var multiFactor = quotation.policyOptions.multiFactor;
    if (illustrations instanceof Array) {
      var row = {};
      var polTerm = illustrations.length;
      var sLow = '3.25';
      var sHigh = '4.75';
      var addEmptyRowEveryFiveRow = true;
      var countRow = 0;
      illustrations.forEach(function(illustration, i) {
        var policyYear = i + 1;
        var age = iAge + i + 1;
        var policyYearAge = policyYear + ' / ' + age;
        if (policyYear === 1) {
          Annual_Premium = getCurrency(illustration.annualPrem, '', 2);
        }
        var multiplierBenefit = (multiFactor === "1") ? "0" : getCurrency(round(illustration.multiplyBenefit, 0), '', 0);
        var deathBenefitRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.totYearPrem, 0), '', 0),
          multiplierBenefit: multiplierBenefit,
          guaranteedDeathBenefit: getCurrency(round(illustration.gtdDB, 0), '', 0),
          nonGuaranteedDeathBenefitLow: getCurrency(round(illustration.nonGtdDB[sLow], 0), '', 0),
          totalDeathBenefitLow: getCurrency(round(illustration.totDB[sLow], 0), '', 0),
          nonGuaranteedDeathBenefitHigh: getCurrency(round(illustration.nonGtdDB[sHigh], 0), '', 0),
          totalDeathBenefitHigh: getCurrency(round(illustration.totDB[sHigh], 0), '', 0)
        };
        var surrenderValueRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.totYearPrem, 0), '', 0),
          guaranteed: getCurrency(round(illustration.gtdSV, 0), '', 0),
          nonGuaranteedLow: getCurrency(round(illustration.nonGtdSV[sLow], 0), '', 0),
          totalLow: getCurrency(round(illustration.totSV[sLow], 0), '', 0),
          nonGuaranteedHigh: getCurrency(round(illustration.nonGtdSV[sHigh], 0), '', 0),
          totalHigh: getCurrency(round(illustration.totSV[sHigh], 0), '', 0)
        };
        var deductionsRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.totYearPrem, 0), '', 0),
          valueOfPremiumPaidToDateLow: getCurrency(round(illustration.premVal[sLow], 0), '', 0),
          effectOfDeductionToDateLow: getCurrency(round(illustration.deduction[sLow], 0), '', 0),
          totalSurrenderValueLow: getCurrency(round(illustration.totSV[sLow], 0), '', 0),
          valueOfPremiumPaidToDateHigh: getCurrency(round(illustration.premVal[sHigh], 0), '', 0),
          effectOfDeductionToDateHigh: getCurrency(round(illustration.deduction[sHigh], 0), '', 0),
          totalSurrenderValueHigh: getCurrency(round(illustration.totSV[sHigh], 0), '', 0)
        };
        var totalDistributionCostRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.totYearPrem, 0), '', 0),
          totalDistributionCostToDate: getCurrency(round(illustration.tdc, 0), '', 0)
        };
        var totalDistributionCostSuppBiRow = {
          policyYearAge: policyYearAge,
          totalPremiumPaidToDate: getCurrency(round(illustration.riderPrem, 0), '', 0),
          totalDistributionCostToDate: getCurrency(round(illustration.riderComm, 0), '', 0)
        };
        if (policyYear <= 10 || policyYear <= polTerm && policyYear % 5 === 0 || policyYear === polTerm) {
          deathBenefit.push(deathBenefitRow);
          surrenderValue.push(surrenderValueRow);
          if (policyYear === polTerm) {
            maturityValue.push(Object.assign({}, surrenderValueRow, {
              policyYearAge: 'Age ' + age
            }));
          }
          deduction.push(deductionsRow);
          totalDistributionCostMainBi.push(totalDistributionCostRow);
          totalDistributionCostSuppBi.push(totalDistributionCostSuppBiRow);
        }
        if ([55, 60, 65].indexOf(age) >= 0) {
          deathBenefitLast.push(Object.assign({}, deathBenefitRow, {
            policyYearAge: 'Age ' + age
          }));
          surrenderValueLast.push(Object.assign({}, surrenderValueRow, {
            policyYearAge: 'Age ' + age
          }));
          deductionsLast.push(Object.assign({}, deductionsRow, {
            policyYearAge: 'Age ' + age
          }));
          totalDistributionCostLast.push(Object.assign({}, totalDistributionCostRow, {
            policyYearAge: 'Age ' + age
          }));
          totalDistributionCostSuppBiLast.push(Object.assign({}, totalDistributionCostSuppBiRow, {
            policyYearAge: 'Age ' + age
          }));
        }
        if (policyYear <= 10) {
          countRow += 1;
        } else if (policyYear <= polTerm && policyYear % 5 === 0) {
          countRow += 1;
        }
        if (policyYear <= 10 || policyYear <= polTerm && policyYear % 5 === 0) {
          if (addEmptyRowEveryFiveRow && countRow % 5 === 0) {
            deathBenefit.push(deathBenefitRowDefault);
            surrenderValue.push(surrenderValueRowDefault);
            deduction.push(deductionsRowDefault);
            totalDistributionCostMainBi.push(totalDistributionCostRowDefault);
            totalDistributionCostSuppBi.push(totalDistributionCostRowDefault);
          }
        }
      });
    }
    return {
      deathBenefit: deathBenefit.concat(deathBenefitRowDefault).concat(deathBenefitLast),
      surrenderValue: surrenderValue.concat(surrenderValueRowDefault).concat(surrenderValueLast).concat(maturityValue),
      deduction: deduction.concat(deductionsRowDefault).concat(deductionsLast),
      totalDistributionCostMainBi: totalDistributionCostMainBi.concat(totalDistributionCostRowDefault).concat(totalDistributionCostLast),
      totalDistributionCostSuppBi: totalDistributionCostSuppBi.concat(totalDistributionCostRowDefault).concat(totalDistributionCostSuppBiLast),
      Annual_Premium
    };
  };
  return {
    proposer_details: proposer_init(),
    life_assured_details: life_assured_init(),
    basic_plan: basic_plan_init(),
    your_plan: your_plan_init(),
    summary_multiplier_benefit: summary_multiplier_benefit_init(),
    your_premimun: your_premium_init(),
    total_premimu: total_premium_init(),
    conditions: conditions_init(),
    cover: cover_init(),
    illustration: illustration_init(),
    others: others_init(),
    footer: footer_init()
  };
}