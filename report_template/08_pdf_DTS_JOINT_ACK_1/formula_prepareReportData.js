function(quotation, planInfo, planDetails, extraPara) {
  var retVal = {};
  var psPlanFields = [];
  for (var i = 0; i < quotation.plans.length; i++) {
    var plan = quotation.plans[i];
    var planName = plan.covName.en;
    var polTerm = {
      planName: planName,
    };
    psPlanFields.push(polTerm);
  }
  retVal.psPlanFields = psPlanFields;
  return retVal;
}