<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_declaration">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">DECLARATION</span>
    </p>
    <xsl:if test="/root/showQuestions/declaration/question = 'BANKRUPTCY01'">
      <table class="dataGroupLast">
        <tr>
          <xsl:call-template name="infoSectionHeaderLaTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="sectionHeader" select="'Bankruptcy'"/>
          </xsl:call-template>
        </tr>
        <tr>
          <xsl:call-template name="questionTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="question" select="'Are you an undischarged bankrupt?'"/>
            <xsl:with-param name="sectionGroupName" select="'declaration'"/>
            <xsl:with-param name="answerId" select="'BANKRUPTCY01'"/>
            <xsl:with-param name="isQuestionWithoutNumber" select="boolean(1)" />
          </xsl:call-template>
        </tr>
      </table>
    </xsl:if>
  </div>
  <div class="section">
    <xsl:if test="/root/showQuestions/declaration/question = 'PEP01'">
      <table class="dataGroupLastNoAutoColor">
        <tr>
          <xsl:call-template name="infoSectionHeaderLaTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="sectionHeader" select="'Politically Exposed Person (PEP)'"/>
          </xsl:call-template>
        </tr>
        <tr>
          <xsl:call-template name="questionTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="question" select="'
                Are you a current/former Political Exposed Person (PEP) or a family
                member (parent, step parent, child, step-child, adopted child,
                spouse, sibling, step-sibling and adopted sibling) or close
                associate (closely connected either socially or professionally)
                of a PEP?'"/>
            <xsl:with-param name="sectionGroupName" select="'declaration'"/>
            <xsl:with-param name="answerId" select="'PEP01'"/>
            <xsl:with-param name="isQuestionWithoutNumber" select="boolean(1)" />
          </xsl:call-template>
        </tr>
        <xsl:if test="/root/showQuestions/declaration/question = 'PEP01_DATA'">
          <tr>
            <td class="tdOneCol">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
              <table class="data">
                <xsl:choose>
                  <xsl:when test="$lifeAssuredIsProposer = 'true'">
                    <col width="90"/>
                    <col width="95"/>
                    <col width="90"/>
                    <col width="96"/>
                    <col width="162"/>
                    <col width="162"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <col width="64"/>
                    <col width="90"/>
                    <col width="95"/>
                    <col width="90"/>
                    <col width="96"/>
                    <col width="130"/>
                    <col width="130"/>
                  </xsl:otherwise>
                </xsl:choose>

                <tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">For</span>
                      </p>
                    </td>
                  </xsl:if>
                  <td class="tdAns">
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">Relationship with PEP</span>
                    </p>
                  </td>
                  <td class="tdAns">
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">Full Name of the PEP</span>
                    </p>
                  </td>
                  <td class="tdAns">
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">Position held by the PEP</span>
                    </p>
                  </td>
                  <td class="tdAns">
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">Country where he/she is the PEP</span>
                    </p>
                  </td>
                  <td class="tdAns">
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">Source of Funds</span>
                    </p>
                  </td>
                  <td class="tdAns">
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">Source of Wealth</span>
                    </p>
                  </td>
                </tr>
                <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                  <xsl:for-each select="/root/insured">
                    <xsl:for-each select="declaration/*[local-name() = 'PEP01_DATA']">
                      <tr class="odd">
                        <xsl:if test="position() = 1">
                          <td>
                            <xsl:attribute name="class">
                              <xsl:choose>
                                <xsl:when test="count(/root/proposer/declaration/*[local-name() = 'PEP01_DATA']) > 0">
                                  <xsl:value-of select="'tdFor'"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="'tdFor bottom'"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:attribute>
                            <xsl:attribute name="rowspan">
                              <xsl:value-of select="last()"/>
                            </xsl:attribute>
                            <p>
                              <span lang="EN-HK" class="person">Life Assured</span>
                            </p>
                          </td>
                        </xsl:if>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="PEP01a"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="PEP01b"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="PEP01c"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="PEP01d"/>
                            </span>
                          </p>
                        </td>
                        <td class="tdAns">
                          <xsl:if test="PEP01e1 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Salary/Commission
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01e2 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Gift/Inheritance
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01e3 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Proceeds from sale of assets
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01e4 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Business Income
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01e5 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Proceeds from maturity/surrender of a policy
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01e6 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/><xsl:value-of select='PEP01e7'/>
                              </span>
                            </p>
                          </xsl:if>
                        </td>
                        <td class="tdAns">
                          <xsl:if test="PEP01f1 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Salary/Commission
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01f2 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Gift/Inheritance
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01f3 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Investment Profits (shares, bonds, unit trust, property etc)
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01f4 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Business Income
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01f5 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Withdrawal of CPF money
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="PEP01f6 = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/><xsl:value-of select='PEP01f7'/>
                              </span>
                            </p>
                          </xsl:if>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </xsl:for-each>
                </xsl:if>
                <xsl:for-each select="/root/proposer/declaration/*[local-name() = 'PEP01_DATA']">
                  <tr class="even">
                    <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                      <td class="tdFor bottom">
                        <xsl:attribute name="rowspan">
                          <xsl:value-of select="last()"/>
                        </xsl:attribute>
                        <p class="tdAns">
                          <span lang="EN-HK" class="person">Proposer</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td class="tdAns">
                      <p class="tdAns">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="PEP01a"/>
                        </span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="tdAns">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="PEP01b"/>
                        </span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="tdAns">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="PEP01c"/>
                        </span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="tdAnsLeft">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="PEP01d"/>
                        </span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <xsl:if test="PEP01e1 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Salary/Commission
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01e2 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Gift/Inheritance
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01e3 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Proceeds from sale of assets
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01e4 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Business Income
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01e5 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Proceeds from maturity/surrender of a policy
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01e6 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/><xsl:value-of select='PEP01e7'/>
                          </span>
                        </p>
                      </xsl:if>
                    </td>
                    <td class="tdAns">
                      <xsl:if test="PEP01f1 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Salary/Commission
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01f2 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Gift/Inheritance
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01f3 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Investment Profits (shares, bonds, unit trust, property etc)
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01f4 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Business Income
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01f5 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Withdrawal of CPF money
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="PEP01f6 = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/><xsl:value-of select='PEP01f7'/>
                          </span>
                        </p>
                      </xsl:if>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>
              <p class="question">
                <span lang="EN-HK" class="question"><br/></span>
              </p>
            </td>
          </tr>
        </xsl:if>
        <tr>
          <td
            style="border: solid windowtext 1.0pt; border-top: none; border-bottom: none; padding: 0in 5.4pt 0in 5.4pt">
            <xsl:attribute name="colspan">
              <xsl:value-of select="$colspan"/>
            </xsl:attribute>
            <p class="statement padding"
              style="margin-top: 3.0pt; margin-right: 0in; margin-bottom: 3.0pt; margin-left: 0in; line-height: normal">
              <span lang="EN-HK" class="questionBold">
                PEP means an individual who is or has been entrusted with prominent
                public functions in Singapore, a foreign country or an
                international organisation, which includes the roles held by a
                head of state, a head of government, government ministers,
                senior civil or public servants, senior judicial or military
                officials, senior executives of state owned corporations,
                senior political party officials, members of the legislature
                and senior management of international organisations.</span>
            </p>
          </td>
        </tr>
      </table>
    </xsl:if>
  </div>
  <div class="section">
    <xsl:if test="/root/showQuestions/declaration/question[substring(., 1, 3) = 'CRS' or substring(., 1, 5) = 'FATCA']">
      <table class="dataGroupLastNoAutoColor">
        <tr>
          <xsl:call-template name="infoSectionHeaderPropTmpl">
            <xsl:with-param name="colspan" select="$colspan"/>
            <xsl:with-param name="sectionHeader" select="'Foreign Account Tax Compliance Act (FATCA) and Common Reporting Standard (CRS)'"/>
          </xsl:call-template>
        </tr>
        <xsl:if test="/root/showQuestions/declaration/question = 'FATCA01'">
          <tr>
            <td class="tdFirst padding">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan - 1"/>
              </xsl:attribute>
              <p class="question">
                <span lang="EN-HK" class="question">
                  1. Is the Proposer a tax resident of <u>Singapore</u>?
                </span>
              </p>
            </td>
            <td width="99" class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="/root/proposer/declaration/FATCA01"/>
                </span>
              </p>
            </td>
          </tr>
          <!-- <xsl:if test="/root/showQuestions/declaration/question = 'FATCA01b'">
            <tr>
              <td class="tdFirst padding">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan - 1"/>
              </xsl:attribute>
                <p class="question"  style="text-align: right;">
                  <span lang="EN-HK" class="question">
                    Please provide Taxpayer Identification Number TIN:
                  </span>
                </p>
              </td>
              <td width="99" class="tdLast padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="/root/proposer/declaration/FATCA01b"/>
                  </span>
                </p>
              </td>
            </tr>
          </xsl:if> -->
          <xsl:if test="/root/showQuestions/declaration/question = 'FATCA02'">
            <tr class="even">
              <td class="tdFirst padding">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan - 1"/>
                </xsl:attribute>
                <p class="question">
                  <span lang="EN-HK" class="question">
                    2. Is the Proposer a citizen or tax resident of <u>United States (US)</u>?
                  </span>
                </p>
              </td>
              <td width="99" class="tdLast padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="/root/proposer/declaration/FATCA02"/>
                  </span>
                </p>
              </td>
            </tr>
          </xsl:if>
          <!-- <xsl:if test="/root/showQuestions/declaration/question = 'FATCA03'">
            <tr class="even">
              <td class="tdFirst padding">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan - 1"/>
                </xsl:attribute>
                <p class="question"  style="text-align: right;">
                  <span lang="EN-HK" class="question">
                    Please provide Taxpayer Identification Number TIN:
                  </span>
                </p>
              </td>
              <td width="99" class="tdLast padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="/root/proposer/declaration/FATCA03"/>
                  </span>
                </p>
              </td>
            </tr>
          </xsl:if> -->
        </xsl:if>
        <xsl:if test="/root/showQuestions/declaration/question = 'CRS01'">
          <tr>
            <td class="tdFirst padding">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan - 1"/>
              </xsl:attribute>
              <p class="question">
                <span lang="EN-HK" class="question">
                  3. Other than Singapore and US, is the Proposer a tax resident of <u>other countries/jurisdictions</u>?
                </span>
              </p>
            </td>
            <td width="99" class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="/root/proposer/declaration/CRS01"/>
                </span>
              </p>
            </td>
          </tr>
          <xsl:if test="/root/showQuestions/declaration/question[. = 'FATCA01b' or . = 'FATCA03' or . = 'CRS01_DATA']">
            <tr>
              <td valign="top"
                style="border: solid windowtext 1.0pt; border-top: none; padding: 1.4pt 5.4pt 1.4pt 5.4pt">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <col width="144"/>
                  <col width="174"/>
                  <col width="223"/>
                  <col width="155"/>
                  <tr>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Country/Jurisdiction of Tax Residency</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Taxpayer Identification Number (TIN)</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Reason (if TIN is not available, please select appropriate Reason)</span>
                      </p>
                    </td>
                    <td class="tdAns">
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Please provide further details if Reason B is selected</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="/root/showQuestions/declaration/question = 'FATCA01b'">
                    <tr class="even">
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">Singapore</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <xsl:choose>
                            <xsl:when test="string-length(/root/proposer/declaration/FATCA01b) > 0">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="/root/proposer/declaration/FATCA01b"/>
                              </span>
                            </xsl:when>
                            <xsl:otherwise>
                              <span lang="EN-HK" class="answer">-</span>
                            </xsl:otherwise>
                          </xsl:choose>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="answer">-</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="answer">-</span>
                        </p>
                      </td>
                    </tr>
                  </xsl:if>
                  <xsl:if test="/root/showQuestions/declaration/question = 'FATCA03'">
                    <tr class="even">
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">United States (US)</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <xsl:choose>
                            <xsl:when test="string-length(/root/proposer/declaration/FATCA03) > 0">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="/root/proposer/declaration/FATCA03"/>
                              </span>
                            </xsl:when>
                            <xsl:otherwise>
                              <span lang="EN-HK" class="answer">-</span>
                            </xsl:otherwise>
                          </xsl:choose>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="answer">-</span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="answer">-</span>
                        </p>
                      </td>
                    </tr>
                  </xsl:if>
                  <xsl:for-each select="/root/proposer/declaration/*[local-name() = 'CRS01_DATA']">
                    <tr class="even">
                      <td class="tdAns">
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="CRS01a"/>
                          </span>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAns">
                          <xsl:choose>
                            <xsl:when test="string-length(CRS01b) > 0">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="CRS01b"/>
                              </span>
                            </xsl:when>
                            <xsl:otherwise>
                              <span lang="EN-HK" class="answer">-</span>
                            </xsl:otherwise>
                          </xsl:choose>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <xsl:choose>
                            <xsl:when test="string-length(CRS01c) > 0">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="CRS01c"/>
                              </span>
                            </xsl:when>
                            <xsl:otherwise>
                              <span lang="EN-HK" class="answer">-</span>
                            </xsl:otherwise>
                          </xsl:choose>
                        </p>
                      </td>
                      <td class="tdAns">
                        <p class="tdAnsLeft">
                          <xsl:choose>
                            <xsl:when test="string-length(CRS01d) > 0">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="CRS01d"/>
                              </span>
                            </xsl:when>
                            <xsl:otherwise>
                              <span lang="EN-HK" class="answer">-</span>
                            </xsl:otherwise>
                          </xsl:choose>
                        </p>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
          </xsl:if>
        </xsl:if>
      </table>
    </xsl:if>
  </div>
  <div class="section">
    <xsl:if test="/root/showQuestions/declaration/question[substring(., 1, 11) = 'TRUSTED_IND']">
      <table class="dataGroupLastNoAutoColor">
        <col width="38"/>
        <col width="680"/>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Trusted Individual'"/>
            <xsl:with-param name="colspan" select="2"/>
          </xsl:call-template>
        </tr>
        <tr>
          <td class="tdFirst padding">
            <p class="tdAns">
              <xsl:choose>
                <xsl:when test="/root/proposer/declaration/TRUSTED_IND04 = 'Yes'">
                  <xsl:copy-of select="$tick"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:copy-of select="$untick"/>
                </xsl:otherwise>
              </xsl:choose>
            </p>
          </td>
          <td class="tdLast padding">
            <p class="statement">
              <span lang="EN-HK" class="statement">
                I,
              </span>
              <span lang="EN-HK" class="statementBold">
                <xsl:value-of select="/root/proposer/declaration/trustedIndividuals/fullName"/>
              </span>
              <span lang="EN-HK" class="statement"> of NRIC/Passport: </span>
              <span lang="EN-HK" class="statementBold">
                <xsl:value-of select="/root/proposer/declaration/trustedIndividuals/idCardNo"/>
              </span>
              <span lang="EN-HK" class="statement"> acknowledge that I have fulfilled the definition of a Trusted Individual and I am a Trusted Individual to </span>
              <span lang="EN-HK" class="statementBold">
                <xsl:value-of select="/root/proposer/personalInfo/fullName"/>
              </span>
              <span lang="EN-HK" class="statement">.I confirm that the Financial Consultant has conversed in </span>
              <span lang="EN-HK" class="statementBold">
                <xsl:choose>
                  <xsl:when test="/root/proposer/declaration/TRUSTED_IND01 = 'Other'">
                    <xsl:value-of select="/root/proposer/declaration/TRUSTED_IND02"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/declaration/TRUSTED_IND01"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
              <span lang="EN-HK" class="statement"> to conduct the Financial Needs Analysis and explained the recommendations and application form(s). I confirm that I understand the explanations and recommendations made by the Financial Consultant and I have translated and explained them to </span>
              <span lang="EN-HK" class="statementBold">
                <xsl:value-of select="/root/proposer/personalInfo/fullName"/>
              </span>
              <span lang="EN-HK" class="statement">.</span>
            </p>
          </td>
        </tr>
      </table>
    </xsl:if>
  </div>
  <div class="section">
    <xsl:if test="/root/showQuestions/declaration/question[substring(., 1, 8) = 'FUND_SRC']">
      <xsl:variable name="person">
        <xsl:choose>
          <xsl:when test="$lifeAssuredIsProposer = 'true'">
            <xsl:value-of select="'proposer'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'insured'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <table class="dataGroupLastNoAutoColor">
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Source of Funds'"/>
            <xsl:with-param name="colspan" select="4"/>
          </xsl:call-template>
        </tr>

        <xsl:if test="/root/showQuestions/declaration/question='FUND_SRC01'">
          <tr class="odd">
            <td width="418" colspan="2" class="tdFirst padding">
              <p class="question">
                <span lang="EN-HK" class="question">
                  1. Source of Payment
                </span>
              </p>
            </td>
            <td width="418" colspan="2" class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="/root/proposer/declaration/FUND_SRC01"/>
                </span>
              </p>
            </td>
          </tr>
        </xsl:if>
        <xsl:if test="/root/showQuestions/declaration/question='FUND_SRC02'">
          <tr class="even">
            <td width="418" colspan="2" class="tdFirst padding">
              <p class="question">
                <span lang="EN-HK" class="question">
                  2. Payor's Details - Who is paying the insurance premium for this application?
                </span>
              </p>
            </td>
            <td width="418" colspan="2" class="tdLast padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="/root/proposer/declaration/FUND_SRC02"/>
                </span>
              </p>
            </td>
          </tr>
        </xsl:if>
        <xsl:if test="/root/proposer/declaration/FUND_SRC02='Other'">
          <tr class="odd">
            <td width="720" colspan="4" class="tdOneCol">
              <p class="question">
                <span lang="EN-HK" class="question"><br/></span>
              </p>
            </td>
          </tr>
          <tr class="odd">
            <td class="tdOneCol" colspan="4">
              <table class="dataPayor">
                <col width="190"/>
                <col width="163"/>
                <col width="190"/>
                <col width="163"/>
                <tr class="even">
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">Surname of Payor (as shown in ID)</span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:choose>
                          <xsl:when test="string-length(/root/proposer/declaration/FUND_SRC03) > 0">
                            <xsl:value-of select="/root/proposer/declaration/FUND_SRC03"/>
                          </xsl:when>
                          <xsl:otherwise>-</xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">Given Name of Payor (as shown in ID)</span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="/root/proposer/declaration/FUND_SRC04"/>
                      </span>
                    </p>
                  </td>
                </tr>
                <tr class="odd">
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">English or other Name of Payor</span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:choose>
                          <xsl:when test="string-length(/root/proposer/declaration/FUND_SRC05) > 0">
                            <xsl:value-of select="/root/proposer/declaration/FUND_SRC05"/>
                          </xsl:when>
                          <xsl:otherwise>-</xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">Han Yu Pin Yin Name of Payor</span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:choose>
                          <xsl:when test="string-length(/root/proposer/declaration/FUND_SRC06) > 0">
                            <xsl:value-of select="/root/proposer/declaration/FUND_SRC06"/>
                          </xsl:when>
                          <xsl:otherwise>-</xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>
                <tr class="even">
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">
                        Relationship of Payor to Proposer
                      </span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:choose>
                          <xsl:when test="/root/proposer/declaration/FUND_SRC09[. = 'Others' or . = 'OTHERS']">
                            <xsl:value-of select="/root/proposer/declaration/FUND_SRC10"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="/root/proposer/declaration/FUND_SRC09"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">
                        Payor's Occupation
                      </span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="/root/proposer/declaration/FUND_SRC20"/>
                      </span>
                    </p>
                  </td>
                </tr>
                <tr class="odd">
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">
                        Payor's ID No.
                      </span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="/root/proposer/declaration/FUND_SRC07"/>
                      </span>
                    </p>
                  </td>
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">
                        Payor's Annual Income (S$)
                      </span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:choose>
                          <xsl:when test="string-length(/root/proposer/declaration/FUND_SRC21) > 0">
                            <xsl:value-of select="/root/proposer/declaration/FUND_SRC21"/>
                          </xsl:when>
                          <xsl:otherwise>-</xsl:otherwise>
                        </xsl:choose>
                      </span>
                    </p>
                  </td>
                </tr>
                <tr class="even">
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">
                        Payor's Nationality or Country of Incorporation
                      </span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="/root/proposer/declaration/FUND_SRC08"/>
                      </span>
                    </p>
                  </td>
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">
                        Payor’s Contact No.
                      </span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p  class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="/root/proposer/declaration/FUND_SRC11"/> - <xsl:value-of select="/root/proposer/declaration/FUND_SRC12"/>
                      </span>
                    </p>
                  </td>
                </tr>
                <tr class="odd">
                  <td  class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">
                        Reason for payment by third party
                      </span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <p class="answer">
                      <span lang="EN-HK" class="answer">
                        <xsl:value-of select="/root/proposer/declaration/FUND_SRC22"/>
                      </span>
                    </p>
                  </td>
                  <td class="tdFirst padding">
                    <p class="title">
                      <span lang="EN-HK" class="questionItalic">
                        Payor's Residential/ Registered Address
                      </span>
                    </p>
                  </td>
                  <td class="tdLast padding">
                    <xsl:call-template name="personalInfoAddressTmpl">
                      <xsl:with-param name="addrBlock" select="/root/proposer/declaration/FUND_SRC15"/>
                      <xsl:with-param name="addrStreet" select="/root/proposer/declaration/FUND_SRC16"/>
                      <xsl:with-param name="addrUnitNum" select="/root/proposer/declaration/FUND_SRC17"/>
                      <xsl:with-param name="addrEstate" select="/root/proposer/declaration/FUND_SRC18"/>
                      <xsl:with-param name="addrCity" select="/root/proposer/declaration/FUND_SRC19"/>
                      <xsl:with-param name="addrCountry" select="/root/proposer/declaration/FUND_SRC13"/>
                      <xsl:with-param name="addrPostalCode" select="/root/proposer/declaration/FUND_SRC14"/>
                    </xsl:call-template>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr class="odd">
            <td width="720" colspan="4" class="tdOneCol">
              <p class="question">
                <span lang="EN-HK" class="question"><br/></span>
              </p>
            </td>
          </tr>
        </xsl:if>
        <xsl:if test="/root/showQuestions/declaration/question = 'FUND_SRC33'
          or /root/showQuestions/declaration/question = 'FUND_SRC34'
          or /root/showQuestions/declaration/question = 'FUND_SRC35'
          or /root/showQuestions/declaration/question = 'FUND_SRC36'
          or /root/showQuestions/declaration/question = 'FUND_SRC38'">
          <tr class="odd">
            <td width="528" colspan="3" class="tdFirst padding">
              <p class="question">
                <span lang="EN-HK" class="question">
                  3. Source of Wealth - Refers to the origin of your total assets.
                </span>
              </p>
            </td>
            <td width="192" class="tdLast padding">
              <xsl:if test="/root/proposer/declaration/FUND_SRC33 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/>Salary/Commissions/CPF
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="/root/proposer/declaration/FUND_SRC34 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/>Gift/Inheritance
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="/root/proposer/declaration/FUND_SRC35 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/>Profit from investments
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="/root/proposer/declaration/FUND_SRC36 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/>Business or trade income
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="/root/proposer/declaration/FUND_SRC38 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/><xsl:value-of select='/root/proposer/declaration/FUND_SRC39'/>
                  </span>
                </p>
              </xsl:if>
            </td>
          </tr>
        </xsl:if>
        <xsl:if test="/root/showQuestions/declaration/question = 'FUND_SRC41'
          or /root/showQuestions/declaration/question = 'FUND_SRC42'
          or /root/showQuestions/declaration/question = 'FUND_SRC43'
          or /root/showQuestions/declaration/question = 'FUND_SRC44'
          or /root/showQuestions/declaration/question = 'FUND_SRC30'">
          <tr class="even">
            <td width="528" colspan="3" class="tdFirst padding">
              <p class="question">
                <span lang="EN-HK" class="question">
                  4. Source of Funds - Refers to the origin of funds/assets used for this investment.
                </span>
              </p>
            </td>
            <td width="192" class="tdLast padding">
              <xsl:if test="/root/proposer/declaration/FUND_SRC41 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/>Local Funds from Self/CPF
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="/root/proposer/declaration/FUND_SRC42 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/>Local Funds from Spouse
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="/root/proposer/declaration/FUND_SRC43 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/>Foreign Funds from Self
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="/root/proposer/declaration/FUND_SRC44 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/>Foreign Funds from Spouse
                  </span>
                </p>
              </xsl:if>
              <xsl:if test="/root/proposer/declaration/FUND_SRC30 = 'Yes'">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:copy-of select="$tick"/><xsl:value-of select='/root/proposer/declaration/FUND_SRC31'/>
                  </span>
                </p>
              </xsl:if>
            </td>
          </tr>
        </xsl:if>
      </table>
    </xsl:if>
  </div>
  <div class="section">
    <xsl:if test="/root/showQuestions/declaration/question = 'EPOLICY01'">
      <table class="dataGroupLast">
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'e-Policy (By default your policy documents will be available in MyAXA only)'"/>
            <xsl:with-param name="colspan" select="1"/>
          </xsl:call-template>
        </tr>
        <tr>
          <td class="tdOneColNoPadding">
            <table>
              <tr>
                <td width="28" class="tdFirst padding noBorder">
                  <p class="tdAns">
                    <xsl:choose>
                      <xsl:when test="/root/proposer/declaration/EPOLICY01 = 'Yes'">
                        <xsl:copy-of select="$tick"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:copy-of select="$untick"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </p>
                </td>
                <td width="690" class="tdLast padding noBorder">
                  <p class="statement">
                    <span lang="EN-HK" class="statement">
                      I would also like to receive a copy of my policy documents by mail, aside from my e-policy in MyAXA
                    </span>
                  </p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </xsl:if>
  </div>
  <div class="section">
    <xsl:if test="/root/policyOptions/paymentMethod[. = 'CPFIS-SA' or . = 'cpfissa']">
      <table class="dataGroupLastNoAutoColor">
        <col width="30"/>
        <col width="688"/>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'CPFIS Special Account - Authorisation For Withdrawal'"/>
            <xsl:with-param name="colspan" select="2"/>
          </xsl:call-template>
        </tr>
        <tr>
          <td colspan="2" class="tdOneCol">
            <p class="statement">
              <span lang="EN-HK" class="statement"><br/></span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="statement">
                I hereby irrevocably authorise the Board to:
              </span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="statement"><br/></span>
            </p>
          </td>
        </tr>
        <tr>
          <td class="tdFirst padding noBorder">
            <p class="declarationPt">
              <span lang="EN-HK" class="statement">1.</span>
            </p>
          </td>
          <td class="tdLast noPadding noBorder">
            <p class="statement">
              <span lang="EN-HK" class="statement">Debit
              my CPF Special Account the sum of monies specified by AXA Insurance
              Pte Ltd or the amount determined by the Board for the purchase/placement
              of the above policy, including any related fees, expenses and charges
              under the CPF Investment Scheme - Special Account (CPFIS-SA)
              </span>
            </p>
          </td>
        </tr>
        <tr>
          <td class="tdFirst padding noBorder">
            <p class="declarationPt">
              <span lang="EN-HK" class="statement">2.</span>
            </p>
          </td>
          <td class="tdLast noPadding noBorder">
            <p class="statement">
              <span lang="EN-HK" class="statement">Credit
                my CPF Special Account with any income or any proceeds from the liquidation
                of the above policy under the CPFIS-SA that are received from AXA Insurance
                Pte Ltd.
              </span>
            </p>
          </td>
        </tr>
        <tr>
          <td class="tdFirst padding noBorder">
            <p class="declarationPt">
              <span lang="EN-HK" class="statement">3.</span>
            </p>
          </td>
          <td class="tdLast noPadding noBorder">
            <p class="statement">
              <span lang="EN-HK" class="statement">Disclose
                any particulars or information whatsoever relating to or in connection with
                my investment with AXA Insurance Pte Ltd, to facilitate any transactions that
                cannot be settled due to data discrepancies, insufficient funds or any other
                reasons that the Board deems fit.
              </span>
            </p>
          </td>
        </tr>

        <tr>
          <td colspan="2" class="tdOneCol">
            <p class="statement">
              <span lang="EN-HK" class="statement"><br/></span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="statement">I
                understand that the above transactions shall be made, subject to the provisions
                of the Central Provident Fund Act and the Central Provident Fund (Investment
                Schemes) Regulations as may be amended from time to time and also to all such
                terms and conditions as may be imposed by the Board from time to time.
              </span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="statement"><br/></span>
            </p>
          </td>
        </tr>

        <tr>
          <td colspan="2" class="tdOneCol">
            <p class="statement">
              <span lang="EN-HK" class="statement">I
                hereby agree to indemnify the Board and shall keep the Board indemnified against
                all actions, proceedings, liabilities, claims, damages, expenses or legal costs
                whatsoever arising out of or inconnection with the Board accepting and acting upon
                this authorisation.
              </span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="statement"><br/></span>
            </p>
          </td>
        </tr>

      </table>
    </xsl:if>
  </div>
  <div class="section">
    <xsl:call-template name="infoSectionHeaderNoneTmplDiv">
      <xsl:with-param name="sectionHeader" select="'Declaration and Authorisation'"/>
    </xsl:call-template>
    <div class="sectionContent">
      <div class="oneCol">
        <p class="statement">
          <span lang="EN-HK" class="statement"><br/></span>
        </p>
        <p class="statement">
          <span lang="EN-HK" class="statement">
            I or We declare that:
          </span>
        </p>
        <p class="statement">
          <span lang="EN-HK" class="statement"><br/></span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">1.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">To
            the best of my or our knowledge and belief the information
            given by me or us to AXA Insurance Pte Ltd or its Medical
            Examiner is true and complete and that no material facts such
            as facts likely to influence the assessment and acceptance of
            this proposal have been withheld.
          </span>
        </p>
      </div>
    </div>
  </div>

  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">2.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">I
            or We, the Life Assured, Adult Life Assured and Proposer,
            authorise any medical source, insurance office or organisation,
            to release to AXA Insurance Pte Ltd and AXA Insurance Pte Ltd
            to release to any medical source, insurance office or
            organisation, any relevant information concerning me or
            ourselves, at any time, irrespective of whether the proposal is
            accepted by AXA Insurance Pte Ltd. A photocopy of this
            authorisation shall be as valid as the original.
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">3a.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">I
            or We agree that payment of premium before acceptance of this
            proposal by AXA Insurance Pte Ltd does not commit AXA Insurance
            Pte Ltd to issue the policy I or We have applied for and the
            said policy shall not take effect unless and until this
            proposal has been fully accepted and the full initial premium
            has been paid during my life or our lives and good health.
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">3b.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">I
            or We agree to inform AXA Insurance Pte Ltd if there is any
            change in the state of health, occupation or activity of the
            Life Assured, Adult Life Assured and Proposer between the date
            of this proposal or medical examination and the issue of my or
            our policy. On receiving this information AXA Insurance Pte Ltd
            is entitled to accept or reject my or our proposal.
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">4.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">I or We declare that
            I or We have received a copy of “Your Guide to Health Insurance”
            and the Product Summary, the contents of which have been explained
            to and understood by me or us.
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">5.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">Should
            I or We decide not to take up the proposal under the standard
            terms offered by AXA Insurance Pte Ltd or if the proposal is
            officially accepted by AXA Insurance Pte Ltd and I or We decide
            to terminate the policy within 14 days from the date of receipt
            of the policy document, then the amount refundable to me or us
            shall be determined by AXA Insurance Pte Ltd after taking into
            account the premium(s) paid, less medical fees incurred in
            underwriting the policy. Should this be an investment-linked
            policy, an adjustment made to reflect the change in market
            value of the underlying assets and medical fees, shall be taken
            into account before the premium(s) is refunded. However, should
            AXA Insurance Pte Ltd decline the proposal, then I or We shall
            be entitled to a full refund of the premium(s) paid.
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">6.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">I am or We are aware that
            Accidental Death cover (subject to terms and conditions) will be
            granted to me or us upon receipt by AXA Insurance Pte Ltd of the
            first premium and the top-up amount (for investment-linked policies
            and where applicable) in respect of my proposal.
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">7a.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">The information I or We have
            provided is my or our personal data and, where it is not my or our personal data,
            that I or We have the consent of the owner of such personal data to
            provide such information.
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">7b.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">By providing this information,
            I or We understand and give my or our consent for AXA Insurance Pte Ltd
            and their respective representatives or agents to:
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">i.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">Collect,
            use, store, transfer and/or disclose the information, to or
            with all such persons (including any member of the AXA Group or
            any third party service provider, and whether within or outside
            of Singapore) for the purpose of enabling AXA Insurance Pte Ltd
            to provide me with services required of an insurance provider,
            including the evaluating, processing, administering and/or
            managing of my or our relationship and policy(ies) with AXA
            Insurance Pte Ltd, and for the purposes set out in AXA
            Insurance Pte Ltd’s Data Use Statement which can be found at
            http://www.axa.com.sg (&quot;Purposes&quot;).
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">ii.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">Collect,
            use, store, transfer and/or disclose personal data about me or
            us and those whose personal data I or We have provided from
            sources other than myself or us for the Purposes.
          </span>
        </p>
      </div>
    </div>
  </div>
  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">iii.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">Contact
            me or us to share marketing information about products and services
            offered by AXA Insurance Pte Ltd that may be of interest to me
            or us by post and e-mail and
          </span>
        </p>
      </div>
    </div>
  </div>

  <div class="sectionContent">
    <div class="pdaDeclaration">
      <div>
        <p>
          <xsl:choose>
            <xsl:when test="/root/proposer/declaration/PDPA01a = 'Yes'">
              <xsl:copy-of select="$tick"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="$untick"/>
            </xsl:otherwise>
          </xsl:choose>
          <span class="tdAns">
            By Telephone
          </span>
        </p>
      </div>
      <div>
        <p>
          <xsl:choose>
            <xsl:when test="/root/proposer/declaration/PDPA01b = 'Yes'">
              <xsl:copy-of select="$tick"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="$untick"/>
            </xsl:otherwise>
          </xsl:choose>
          <span class="tdAns">
            By Text Message
          </span>
        </p>
      </div>
    </div>
  </div>

  <div class="sectionContent">
    <div class="twoColDeclaration">
      <div class="first">
        <p>
          <span lang="EN-HK">7c.</span>
        </p>
      </div>
      <div class="last">
        <p>
          <span lang="EN-HK">I or We consent to receive customer service communication by e-mail and/or SMS instead of hard copies by post where available.
          </span>
        </p>
      </div>
    </div>
  </div>

  <div class="sectionContent">
    <div class="oneCol last">
      <p class="statement">
          <span lang="EN-HK" class="warningDeclaration"><br/></span>
      </p>
      <p class="statement">
        <span lang="EN-HK" class="warningDeclaration">If
          a material fact is not disclosed in this proposal, any policy
          issued may not be valid. If you are in doubt as to whether a
          fact is material, you are advised to disclose it. <u>This
          includes any information that you may have provided to the
          financial consultant but was not included in the proposal</u>.
          Please check to ensure that you are fully satisfied with the
          information declared in this proposal before signing.
        </span>
      </p>
      <p class="statement">
        <span lang="EN-HK" class="warningDeclaration"><br/></span>
      </p>
    </div>
  </div>
  <div class="section">
    <table class="dataGroupLastNoAutoColor">
      <tr>
        <xsl:call-template name="infoSectionHeaderLaTmpl">
          <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
          <xsl:with-param name="sectionHeader" select="'Financial Consultant’s Declaration'"/>
        </xsl:call-template>
      </tr>
      <tr>
        <xsl:call-template name="questionTmpl">
          <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
          <xsl:with-param name="question" select="'1. How long have you known your customer?'"/>
          <xsl:with-param name="sectionGroupName" select="'declaration'"/>
          <xsl:with-param name="answerId" select="'FCD_01'"/>
        </xsl:call-template>
      </tr>
      <tr>
        <xsl:call-template name="questionTmpl">
          <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
          <xsl:with-param name="question" select="'2. Are you related to your customer?'"/>
          <xsl:with-param name="sectionGroupName" select="'declaration'"/>
          <xsl:with-param name="answerId" select="'FCD_02'"/>
        </xsl:call-template>
      </tr>
      <xsl:if test="/root/showQuestions/declaration/question = 'FCD_03'">
        <tr>
          <xsl:call-template name="questionTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="question" select="'3. Relationship to customer'"/>
            <xsl:with-param name="sectionGroupName" select="'declaration'"/>
            <xsl:with-param name="answerId" select="'FCD_03'"/>
          </xsl:call-template>
        </tr>
      </xsl:if>
      <tr>
        <td class="tdOneColBottom">
          <xsl:attribute name="colspan">
            <xsl:value-of select="$colspan"/>
          </xsl:attribute>
        </td>
      </tr>
      <tr>
        <td class="tdOneCol" style = "padding-left:25px;">
          <xsl:attribute name="colspan">
            <xsl:value-of select="$colspan"/>
          </xsl:attribute>
          <p class="normal">
            <br/>
            <ul class="statement">
              <li>I, Financial Consultant, certify that the proof of Identity/(ies)
              uploaded and submitted is(are) true copy(ies) of the original(s).
              </li>
              <li class="red">I declare that all answers provided to me by the Proposer or Life
              Assured are declared in the proposal. I have not withheld any other
              information which may influence the acceptance of this proposal by the
              Company.
              </li>
              <li>I have not given any statement to the Proposer or Life Assured
              which is contrary to the provision given in the Company’s standard policy.
              </li>
              <li class="red">I confirm that the Proposer or Life Assured have fully understood
              all questions and answers in the Proposal form. I have checked and
              verified the personal particulars given against their original NRIC or
              Passport or Birth certificate and confirm them to be true and correct.
              </li>
              <li>I have personally seen the Proposer or Life Assured and have
              explained the terms of the insurance to him / her.
              </li>
            </ul>
          </p>
        </td>
      </tr>
    </table>
  </div>
  <p class="sectionGroup">
    <span class="sectionGroup"><br/></span>
  </p>
</xsl:template>

</xsl:stylesheet>
