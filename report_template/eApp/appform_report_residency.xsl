<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="residencyQuestionTmpl">
  <xsl:param name="laIsProp" />
  <xsl:param name="question" />
  <xsl:param name="answerPrefix" />
  <xsl:param name="answerId" />

  <xsl:variable name="laDataExist" select="count(/root/insured/residency/*[local-name()=$answerId]) > 0 and starts-with(/root/insured/residency/*[local-name()=$answerId], $answerPrefix)" />
  <xsl:variable name="propDataExist" select="count(/root/proposer/residency/*[local-name()=$answerId]) > 0 and starts-with(/root/proposer/residency/*[local-name()=$answerId], $answerPrefix)" />

  <xsl:if test="$laDataExist = 'true' or $propDataExist = 'true'">
    <td class="tdFirst padding">
      <p>
        <span lang="EN-HK" class="residStatementBold">
          <xsl:value-of select="$question"/>
        </span>
      </p>
    </td>
    <xsl:if test="not($laIsProp = 'true')">
      <xsl:for-each select="/root/insured">
        <td width="99" class="tdMid">
          <p class="answer center">
            <span lang="EN-HK" class="answer">
              <xsl:choose>
                <xsl:when test="starts-with(residency/*[local-name()=$answerId], $answerPrefix)">
                  <xsl:copy-of select="$tick"/>
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
      </xsl:for-each>
    </xsl:if>
    <td width="99" class="tdLast padding">
      <p class="answer center">
        <span lang="EN-HK" class="answer">
          <xsl:choose>
            <xsl:when test="starts-with(/root/proposer/residency/*[local-name()=$answerId], $answerPrefix)">
              <xsl:copy-of select="$tick"/>
            </xsl:when>
            <xsl:otherwise>-</xsl:otherwise>
          </xsl:choose>
        </span>
      </p>
    </td>
  </xsl:if>
</xsl:template>

<xsl:template name="policiesTableTmpl">
  <xsl:param name="isLa" />
  <xsl:param name="iCid" />
  <xsl:param name="laIsProp" />

  <xsl:variable name="showExist">
    <xsl:choose>
      <xsl:when test="$isLa">
        <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/havExtPlans = 'Yes'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="/root/proposer/policies/havExtPlans = 'Yes'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="showPending">
    <xsl:choose>
      <xsl:when test="$isLa">
        <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/havPndinApp = 'Yes'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="/root/proposer/policies/havPndinApp = 'Yes'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="showReplace">
    <xsl:choose>
      <xsl:when test="$isLa">
        <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/isProslReplace = 'Yes'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="/root/proposer/policies/isProslReplace = 'Yes'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:if test="$showExist = 'true' or $showPending = 'true' or $showReplace = 'true'">
    <table class="data">
      <col width="100"/>
      <col width="119"/>
      <col width="80"/>
      <col width="80"/>
      <col width="80"/>
      <col width="80"/>
      <col width="80"/>
      <col width="80"/>
      <tr>
        <td colspan="8" class="headerRow"
          style="border: solid windowtext 1.0pt; border-bottom: solid black 1.0pt;  padding: 0in 5.4pt 0in 5.4pt">
          <p class="header">
            <span class="sectionHeader">
              <xsl:choose>
                <xsl:when test="$isLa or $laIsProp = 'true'">Life Assured</xsl:when>
                <xsl:otherwise>Proposer</xsl:otherwise>
              </xsl:choose>
            </span>
          </p>
        </td>
      </tr>
      <tr>
        <td style="vertical-align:bottom;" class="tdAns noBorderBottom">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">Type of Insurance Application</span>
          </p>
        </td>
        <td style="vertical-align:bottom;" class="tdAns noBorderBottom">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">Company</span>
          </p>
        </td>
        <td style="vertical-align:bottom;" class="tdAns noBorderBottom">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">Type of Policy(ies)</span>
          </p>
        </td>
        <td style="vertical-align:middle;" class="tdAns" colspan="4">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">Total Sum Assured (S$)</span>
          </p>
        </td>
        <td style="vertical-align:middle;" class="tdAns noBorderBottom" rowspan="2">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">Total Annual Premium (S$)</span>
          </p>
        </td>
      </tr>
      <tr>
        <td class="tdAns noBorderTop">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP"></span>
          </p>
        </td>
        <td class="tdAns noBorderTop">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP"></span>
          </p>
        </td>
        <td class="tdAns noBorderTop">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP"></span>
          </p>
        </td>
        <td class="tdAns">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">Life</span>
          </p>
        </td>
        <td class="tdAns">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">TPD</span>
          </p>
        </td>
        <td class="tdAns">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">CI</span>
          </p>
        </td>
        <td class="tdAns">
          <p class="thAnsCenter">
            <span lang="EN-HK" class="thROP">PA/ ADB</span>
          </p>
        </td>
      </tr>

      <xsl:if test="$showExist = 'true'">
        <tr>
          <td style="vertical-align:middle;" rowspan="3" class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">1. Existing Policies</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">AXA</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">-</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existLifeAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existLifeAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existTpdAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existTpdAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existCiAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existCiAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existPaAdbAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existPaAdbAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existTotalPremAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existTotalPremAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <tr>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">Other Company(ies)</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">-</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existLifeOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existLifeOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existTpdOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existTpdOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existCiOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existCiOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existPaAdbOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existPaAdbOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existTotalPremOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existTotalPremOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <tr class="even">
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="tdAns">Total</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="tdAns"></span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existLife"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existLife"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existTpd"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existTpd"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existCi"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existCi"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existPaAdb"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existPaAdb"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/existTotalPrem"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/existTotalPrem"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
      </xsl:if>

      <xsl:if test="$showPending = 'true'">
        <tr>
          <td style="vertical-align:middle;" rowspan="3" class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">2. Pending Applications (excluding this application)</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">AXA</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">-</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingLifeAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingLifeAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingTpdAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingTpdAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingCiAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingCiAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingPaAdbAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingPaAdbAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingTotalPremAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingTotalPremAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <tr>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">Other Company(ies)</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">-</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingLifeOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingLifeOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingTpdOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingTpdOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingCiOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingCiOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingPaAdbOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingPaAdbOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingTotalPremOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingTotalPremOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <tr class="even">
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="tdAns">Total</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th"></span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingLife"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingLife"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingTpd"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingTpd"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingCi"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingCi"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingPaAdb"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingPaAdb"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/pendingTotalPrem"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/pendingTotalPrem"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>

      </xsl:if>

      <xsl:if test="$showReplace = 'true'">
        <tr>
          <td style="vertical-align:middle;" rowspan="3" class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">3. Policy to be Replaced</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">AXA</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replacePolTypeAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replacePolTypeAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceLifeAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceLifeAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceTpdAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceTpdAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceCiAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceCiAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replacePaAdbAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replacePaAdbAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceTotalPremAXA"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceTotalPremAXA"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <tr>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th">Other Company(ies)</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replacePolTypeOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replacePolTypeOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceLifeOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceLifeOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceTpdOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceTpdOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceCiOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceCiOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replacePaAdbOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replacePaAdbOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="thAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceTotalPremOther"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceTotalPremOther"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <tr class="even">
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="tdAns">Total</span>
            </p>
          </td>
          <td class="tdAns">
            <p class="thAnsCenter">
              <span lang="EN-HK" class="th"></span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceLife"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceLife"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceTpd"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceTpd"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceCi"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceCi"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replacePaAdb"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replacePaAdb"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td class="tdAns">
            <p class="tdAns">
              <span lang="EN-HK" class="tdAns">
                <xsl:choose>
                  <xsl:when test="$isLa">
                    <xsl:value-of select="/root/insured[personalInfo/cid = $iCid]/policies/replaceTotalPrem"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/root/proposer/policies/replaceTotalPrem"/>
                  </xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
      </xsl:if>
    </table>
    <br/>
  </xsl:if>
</xsl:template>

<xsl:template name="appform_pdf_residency">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">RESIDENCY &amp; INSURANCE INFO</span>
    </p>
    <xsl:if test="count(/root/showQuestions/residency/question) > 0">
      <table class="dataGroupLastNoAutoColor">
        <tr>
          <xsl:call-template name="infoSectionHeaderLaTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="sectionHeader" select="'Residency Questions'"/>
          </xsl:call-template>
        </tr>

        <xsl:if test="/root/showQuestions/residency/question = 'EAPP-P6-F1'">
          <tr>
            <td class="tdOneCol">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
              <p>
                <span lang="EN-HK" class="statementBold">Singapore Citizen</span>
              </p>
              <p>
                <span lang="EN-HK" class="question">I am currently:</span>
              </p>
            </td>
          </tr>
          <tr class="odd">
            <xsl:call-template name="residencyQuestionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'Residing in Singapore.'" />
              <xsl:with-param name="answerPrefix" select="'i)'" />
              <xsl:with-param name="answerId" select="'EAPP-P6-F1'" />
            </xsl:call-template>
          </tr>
          <tr class="odd">
            <xsl:call-template name="residencyQuestionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'Residing outside Singapore: I have resided outside Singapore continuously for less than 5 years preceding the date of proposal.'" />
              <xsl:with-param name="answerPrefix" select="'ii)'" />
              <xsl:with-param name="answerId" select="'EAPP-P6-F1'" />
            </xsl:call-template>
          </tr>
          <tr class="odd">
            <xsl:call-template name="residencyQuestionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'Residing outside Singapore: I have resided outside Singapore continuously for 5 years or more preceding the date of proposal.'" />
              <xsl:with-param name="answerPrefix" select="'iii)'" />
              <xsl:with-param name="answerId" select="'EAPP-P6-F1'" />
            </xsl:call-template>
          </tr>
          <tr class="odd">
            <td class="tdOneColBottom">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
            </td>
          </tr>
        </xsl:if>

        <xsl:if test="/root/showQuestions/residency/question = 'EAPP-P6-F2' or /root/showQuestions/residency/question = 'EAPP-P6-F3'">
          <tr>
            <td class="tdResid">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
              <p>
                <span lang="EN-HK" class="statementBold">
                  <xsl:value-of select="/root/proposer/residency/groupOnePass"/>
                </span>
              </p>
            </td>
          </tr>
          <xsl:if test="/root/showQuestions/residency/question = 'EAPP-P6-F2'">
            <tr class="odd">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'1.
                    Have you resided in Singapore for at least 183 days in the
                    last 12 months preceding the date of proposal?'"/>
                <xsl:with-param name="sectionGroupName" select="'residency'"/>
                <xsl:with-param name="answerId" select="'EAPP-P6-F2'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <xsl:if test="/root/showQuestions/residency/question = 'EAPP-P6-F3'">
            <tr class="even">
              <xsl:call-template name="questionTmpl">
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                <xsl:with-param name="question" select="'2.
                    Do you intend to stay in Singapore on a permanent basis?'"/>
                <xsl:with-param name="sectionGroupName" select="'residency'"/>
                <xsl:with-param name="answerId" select="'EAPP-P6-F3'"/>
              </xsl:call-template>
            </tr>
          </xsl:if>
          <tr class="odd">
            <td class="tdOneColBottom">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
            </td>
          </tr>
        </xsl:if>

        <xsl:if test="/root/showQuestions/residency/question = 'EAPP-P6-F4'">
          <tr>
            <td class="tdResid">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
              <xsl:variable name="passTwoTitle" select="' Dependent Pass / Student Pass / Long Term Visit Pass '"/>
              <xsl:variable name="passTwoTitleAfterFirstCheck">
                <xsl:choose>
                  <xsl:when test="count(/root/proposer/personalInfo/pass[. = 'DEPENDENT PASS']) = 0 and count(/root/insured[personalInfo/pass = 'DEPENDENT PASS']) = 0">
                    <xsl:value-of select="substring-after($passTwoTitle, 'Dependent Pass /')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$passTwoTitle"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>

              <xsl:variable name="passTwoTitleAfterSecondCheck">
                <xsl:choose>
                  <xsl:when test="count(/root/proposer/personalInfo/pass[. = 'STUDENT']) = 0 and count(/root/insured[personalInfo/pass = 'STUDENT']) = 0">
                    <xsl:value-of select="concat(substring-before($passTwoTitleAfterFirstCheck, 'Student Pass /'), substring-after($passTwoTitleAfterFirstCheck, 'Student Pass / '))"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$passTwoTitleAfterFirstCheck"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>

              <xsl:variable name="passTwoTitleAfterFinalCheck">
                <xsl:choose>
                  <xsl:when test="count(/root/proposer/personalInfo/pass[. = 'LONG TERM VISIT PASS']) = 0 and count(/root/insured[personalInfo/pass = 'LONG TERM VISIT PASS']) = 0">
                    <xsl:value-of select="substring-before($passTwoTitleAfterSecondCheck, '/ Long Term Visit Pass ')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$passTwoTitleAfterSecondCheck"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>

              <p>
                <span lang="EN-HK" class="statementBold">
                  <xsl:value-of select="$passTwoTitleAfterFinalCheck"/>
                </span>
              </p>
            </td>
          </tr>
          <tr>
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'Have
                  you resided in Singapore for at least 90 days in the last 12
                  months preceding the date of proposal?'"/>
              <xsl:with-param name="sectionGroupName" select="'residency'"/>
              <xsl:with-param name="answerId" select="'EAPP-P6-F4'"/>
            </xsl:call-template>
          </tr>
          <tr class="odd">
            <td class="tdOneColBottom">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
            </td>
          </tr>
        </xsl:if>

        <xsl:if test="/root/showQuestions/residency/question = 'EAPP-P6-F5'">
          <tr>
            <td class="tdResid">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>

              <xsl:variable name="passOtherTitle">
                <xsl:choose>
                  <xsl:when test="count(/root/proposer/personalInfo/pass[. = 'SOCIAL VISIT PASS']) > 0 or count(/root/insured[personalInfo/pass = 'SOCIAL VISIT PASS']) > 0">
                    <xsl:value-of select="'Social Visit Pass'"/>
                  </xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </xsl:variable>

              <xsl:variable name="passOtherTitleAfterPh">
                <xsl:choose>
                  <xsl:when test="count(/root/proposer/personalInfo/pass[. = 'OTHERS']) > 0">
                    <xsl:choose>
                      <xsl:when test="string-length($passOtherTitle) > 0">
                        <xsl:value-of select="concat(concat($passOtherTitle, ' / '), /root/originalData/proposer/personalInfo/passOther)"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="/root/originalData/proposer/personalInfo/passOther"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$passOtherTitle"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>

              <xsl:variable name="passOtherTitlePhLength">
                <xsl:value-of select="string-length($passOtherTitleAfterPh) - string-length($passOtherTitle)"/>
              </xsl:variable>

              <xsl:variable name="passOtherTitleAfterFinal">
                <xsl:choose>
                  <xsl:when test="count(/root/insured/personalInfo/pass[. = 'OTHERS']) > 0">
                    <xsl:for-each select="/root/originalData/insured">
                      <xsl:choose>
                        <xsl:when test="contains($passOtherTitleAfterPh, personalInfo/passOther) and string-length(personalInfo/passOther) = $passOtherTitlePhLength">
                          <xsl:value-of select="$passOtherTitleAfterPh"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:choose>
                            <xsl:when test="string-length($passOtherTitleAfterPh) > 0">
                              <xsl:value-of select="concat(concat($passOtherTitleAfterPh, ' / '), personalInfo/passOther)"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="personalInfo/passOther"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$passOtherTitleAfterPh"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>

              <p>
                <span lang="EN-HK" class="statementBold"><xsl:value-of select="$passOtherTitleAfterFinal"/></span>
              </p>
            </td>
          </tr>
          <tr>
            <td width="516" class="tdFirst padding">
              <p class="question">
                <span lang="EN-HK" class="question">I hold type of pass above</span>
              </p>
            </td>
            <xsl:if test="not($lifeAssuredIsProposer = 'true')">
              <xsl:for-each select="/root/insured">
                <td width="99" class="tdMid">
                  <p class="answer center">
                    <span lang="EN-HK" class="answer">
                      <xsl:choose>
                        <xsl:when test="residency/*[local-name()='EAPP-P6-F5']">
                          <xsl:copy-of select="$tick"/>
                        </xsl:when>
                        <xsl:otherwise>-</xsl:otherwise>
                      </xsl:choose>
                    </span>
                  </p>
                </td>
              </xsl:for-each>
            </xsl:if>
            <td width="99" class="tdLast padding">
              <p class="answer center">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/proposer/residency/*[local-name()='EAPP-P6-F5']">
                      <xsl:copy-of select="$tick"/>
                    </xsl:when>
                    <xsl:otherwise>-</xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
          </tr>
          <tr class="odd">
            <td class="tdOneColBottom">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
            </td>
          </tr>
        </xsl:if>
      </table>
    </xsl:if>
    <xsl:if test="count(/root/showQuestions/foreigner/question) > 0">
      <table class="dataGroupLastNoAutoColor">
        <tr>
          <xsl:call-template name="infoSectionHeaderLaTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="sectionHeader" select="'Foreigner Questions'"/>
          </xsl:call-template>
        </tr>
        <xsl:if test="/root/showQuestions/foreigner/question = 'faDate'">
          <tr class="odd">
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'1.
                  When did you first arrive in Singapore (excluding holidays of
                  less than 3 months)? (mm/yyyy)?'"/>
              <xsl:with-param name="sectionGroupName" select="'foreigner'"/>
              <xsl:with-param name="answerId" select="'faDate'"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
        <xsl:if test="/root/showQuestions/foreigner/question = 'familyResCountry' or /root/showQuestions/foreigner/question = 'familyResCity'">
          <tr class="even">
            <td class="tdQuestionOnly">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
              <p class="question">
                <span lang="EN-HK" class="question">
                  2. Where do your immediate family members currently reside?</span>
              </p>
            </td>
          </tr>
        </xsl:if>
        <xsl:if test="/root/showQuestions/foreigner/question = 'familyResCountry'">
          <tr class="odd">
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'Country:'"/>
              <xsl:with-param name="sectionGroupName" select="'foreigner'"/>
              <xsl:with-param name="answerId" select="'familyResCountry'"/>
              <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
        <xsl:if test="/root/showQuestions/foreigner/question = 'familyResCity'">
          <tr class="odd">
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'City:'"/>
              <xsl:with-param name="sectionGroupName" select="'foreigner'"/>
              <xsl:with-param name="answerId" select="'familyResCity'"/>
              <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
        <xsl:if test="/root/showQuestions/foreigner/question = 'reside5yrs'">
          <tr>
            <td class="tdQuestionOnly">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
              <p class="question">
                <span lang="EN-HK" class="question">
                  3. Please provide details of your residence over the last five years: </span>
              </p>
            </td>
          </tr>
          <tr>
            <td class="tdOneCol">
              <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
              <table class="data">
                <xsl:choose>
                  <xsl:when test="$lifeAssuredIsProposer = 'true'">
                    <col width="100"/>
                    <col width="100"/>
                    <col width="120"/>
                    <col width="132"/>
                    <col width="235"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <col width="72"/>
                    <col width="100"/>
                    <col width="100"/>
                    <col width="120"/>
                    <col width="132"/>
                    <col width="163"/>
                  </xsl:otherwise>
                </xsl:choose>
                <tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <td>
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">For</span>
                      </p>
                    </td>
                  </xsl:if>
                  <td>
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">From (mm/yyyy)</span>
                    </p>
                  </td>
                  <td>
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">To (mm/yyyy)</span>
                    </p>
                  </td>
                  <td>
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">Country of Residence</span>
                    </p>
                  </td>
                  <td>
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">City of Residence</span>
                    </p>
                  </td>
                  <td>
                    <p class="thAnsCenter">
                      <span lang="EN-HK" class="th">Reason</span>
                    </p>
                  </td>
                </tr>
                <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                  <xsl:for-each select="/root/insured">
                    <xsl:for-each select="foreigner/*[local-name() = 'reside5yrs']">
                      <tr class="odd">
                        <xsl:if test="position() = 1">
                          <td>
                            <xsl:attribute name="class">
                              <xsl:choose>
                                <xsl:when test="count(/root/proposer/foreigner/*[local-name() = 'reside5yrs']) > 0">
                                  <xsl:value-of select="'tdFor'"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="'tdFor bottom'"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:attribute>
                            <xsl:attribute name="rowspan">
                              <xsl:value-of select="last()"/>
                            </xsl:attribute>
                            <p>
                              <span lang="EN-HK" class="person">Life Assured</span>
                            </p>
                          </td>
                        </xsl:if>
                        <td>
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="res5Frm"/>
                            </span>
                          </p>
                        </td>
                        <td>
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="res5To"/>
                            </span>
                          </p>
                        </td>
                        <td>
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="res5Country"/>
                            </span>
                          </p>
                        </td>
                        <td>
                          <p class="tdAns">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:value-of select="res5City"/>
                            </span>
                          </p>
                        </td>
                        <td>
                          <xsl:if test="permanent = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Permanent Residence
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="work = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Business / Work
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="study = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Study
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="cob = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Country of birth
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="family = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Place where family members or relatives residing
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="immigrate = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Immigration
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="visit = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/>Visit family/relatives/friends
                              </span>
                            </p>
                          </xsl:if>
                          <xsl:if test="other = 'Y'">
                            <p class="tdAnsLeft">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:copy-of select="$tick"/><xsl:value-of select="resultOther"/>
                              </span>
                            </p>
                          </xsl:if>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </xsl:for-each>
                </xsl:if>
                <xsl:for-each select="/root/proposer/foreigner/*[local-name() = 'reside5yrs']">
                  <tr class="even">
                    <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                      <td class="tdFor bottom">
                        <xsl:attribute name="rowspan">
                          <xsl:value-of select="last()"/>
                        </xsl:attribute>
                        <p class="tdAns">
                          <span lang="EN-HK" class="person">Proposer</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td>
                      <p class="tdAns">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="res5Frm"/>
                        </span>
                      </p>
                    </td>
                    <td>
                      <p class="tdAns">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="res5To"/>
                        </span>
                      </p>
                    </td>
                    <td>
                      <p class="tdAns">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="res5Country"/>
                        </span>
                      </p>
                    </td>
                    <td>
                      <p class="tdAns">
                        <span lang="EN-HK" class="tdAns">
                          <xsl:value-of select="res5City"/>
                        </span>
                      </p>
                    </td>
                    <td>
                      <xsl:if test="permanent = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Permanent Residence
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="work = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Business / Work
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="study = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Study
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="cob = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Country of birth
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="family = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Place where family members or relatives residing
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="immigrate = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Immigration
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="visit = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/>Visit family/relatives/friends
                          </span>
                        </p>
                      </xsl:if>
                      <xsl:if test="other = 'Y'">
                        <p class="tdAnsLeft">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:copy-of select="$tick"/><xsl:value-of select="resultOther"/>
                          </span>
                        </p>
                      </xsl:if>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="3" class="tdOneCol">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
              <p class="question">
                <span lang="EN-HK" class="question"><br/></span>
              </p>
            </td>
          </tr>
        </xsl:if>
        <xsl:if test="/root/showQuestions/foreigner/question = 'hasTravelPlans'">
          <tr class="even">
            <xsl:call-template name="questionTmpl">
              <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              <xsl:with-param name="question" select="'
                  4. In the next 2 years, do you expect to go abroad (outside the
                  current country of residence) for any future residency or
                  travel plans?'"/>
              <xsl:with-param name="sectionGroupName" select="'foreigner'"/>
              <xsl:with-param name="answerId" select="'hasTravelPlans'"/>
            </xsl:call-template>
          </tr>
          <xsl:if test="/root/insured[count(foreigner/travelPlans) > 0] or count(/root/proposer/foreigner/travelPlans) > 0">
            <tr>
              <td class="tdOneCol">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$colspan"/>
                </xsl:attribute>
                <table class="data">
                  <xsl:choose>
                    <xsl:when test="$lifeAssuredIsProposer = 'true'">
                      <col width="130"/>
                      <col width="110"/>
                      <col width="110"/>
                      <col width="95"/>
                      <col width="235"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <col width="72"/>
                      <col width="130"/>
                      <col width="110"/>
                      <col width="110"/>
                      <col width="95"/>
                      <col width="163"/>
                    </xsl:otherwise>
                  </xsl:choose>
                  <tr>
                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                      <td>
                        <p class="thAnsCenter">
                          <span lang="EN-HK" class="th">For</span>
                        </p>
                      </td>
                    </xsl:if>
                    <td>
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Country</span>
                      </p>
                    </td>
                    <td>
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">City</span>
                      </p>
                    </td>
                    <td>
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Duration of each Stay (Days)</span>
                      </p>
                    </td>
                    <td>
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Frequency (per year)</span>
                      </p>
                    </td>
                    <td>
                      <p class="thAnsCenter">
                        <span lang="EN-HK" class="th">Reason</span>
                      </p>
                    </td>
                  </tr>
                  <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                    <xsl:for-each select="/root/insured">
                      <xsl:for-each select="foreigner/*[local-name()='travelPlans']">
                        <tr>
                          <xsl:if test="position() = 1">
                            <td>
                              <xsl:attribute name="class">
                                <xsl:choose>
                                  <xsl:when test="count(/root/proposer/foreigner/*[local-name() = 'travelPlans']) > 0">
                                    <xsl:value-of select="'tdFor'"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="'tdFor bottom'"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:attribute>
                              <xsl:attribute name="rowspan">
                                <xsl:value-of select="last()"/>
                              </xsl:attribute>
                              <p>
                                <span lang="EN-HK" class="person">Life Assured</span>
                              </p>
                            </td>
                          </xsl:if>
                          <td>
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="travlCountry"/>
                              </span>
                            </p>
                          </td>
                          <td>
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="travlCity"/>
                              </span>
                            </p>
                          </td>
                          <td>
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="travlCityDrn"/>
                              </span>
                            </p>
                          </td>
                          <td>
                            <p class="tdAns">
                              <span lang="EN-HK" class="tdAns">
                                <xsl:value-of select="travlCityFrq"/>
                              </span>
                            </p>
                          </td>
                          <td>
                            <xsl:if test="permanent = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Permanent Residence
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="work = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Business / Work
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="study = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Study
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="cob = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Country of birth
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="family = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Place where family members or relatives residing
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="immigrate = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Immigration
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="visit = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/>Visit family/relatives/friends
                                </span>
                              </p>
                            </xsl:if>
                            <xsl:if test="other = 'Y'">
                              <p class="tdAnsLeft">
                                <span lang="EN-HK" class="tdAns">
                                  <xsl:copy-of select="$tick"/><xsl:value-of select="resultOther"/>
                                </span>
                              </p>
                            </xsl:if>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:for-each>
                  </xsl:if>
                  <xsl:for-each select="/root/proposer/foreigner/*[local-name() = 'travelPlans']">
                    <tr class="even">
                      <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                        <td class="tdFor bottom">
                          <xsl:attribute name="rowspan">
                            <xsl:value-of select="last()"/>
                          </xsl:attribute>
                          <p class="tdAns">
                            <span lang="EN-HK" class="person">Proposer</span>
                          </p>
                        </td>
                      </xsl:if>
                      <td>
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="travlCountry"/>
                          </span>
                        </p>
                      </td>
                      <td>
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="travlCity"/>
                          </span>
                        </p>
                      </td>
                      <td>
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="travlCityDrn"/>
                          </span>
                        </p>
                      </td>
                      <td>
                        <p class="tdAns">
                          <span lang="EN-HK" class="tdAns">
                            <xsl:value-of select="travlCityFrq"/>
                          </span>
                        </p>
                      </td>
                      <td>
                        <xsl:if test="permanent = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Permanent Residence
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="work = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Business / Work
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="study = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Study
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="cob = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Country of birth
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="family = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Place where family members or relatives residing
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="immigrate = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Immigration
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="visit = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/>Visit family/relatives/friends
                            </span>
                          </p>
                        </xsl:if>
                        <xsl:if test="other = 'Y'">
                          <p class="tdAnsLeft">
                            <span lang="EN-HK" class="tdAns">
                              <xsl:copy-of select="$tick"/><xsl:value-of select="resultOther"/>
                            </span>
                          </p>
                        </xsl:if>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td valign="top" class="tdOneCol">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>
              <p class="question">
                <span class="question"><br/></span>
              </p>
            </td>
          </tr>
        </xsl:if>
      </table>
    </xsl:if>
  </div>
  <div class="section">
    <xsl:if test="/root/showQuestions/policies">
      <table class="dataGroupLastNoAutoColor">
        <tr>
          <xsl:call-template name="infoSectionHeaderLaTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="sectionHeader" select="'Details of Previous and Concurrent Insurance Applications'"/>
          </xsl:call-template>
        </tr>
        <tr class="odd">
          <xsl:call-template name="questionTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="question" select="'1.
              Do you have any existing life insurance policies or other
              investment products?'"/>
            <xsl:with-param name="sectionGroupName" select="'policies'"/>
            <xsl:with-param name="answerId" select="'havExtPlans'"/>
          </xsl:call-template>
        </tr>
        <tr class="even">
          <xsl:call-template name="questionTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="question" select="'2.
              Other than this current application, do you have any other pending or concurrent insurance applications with AXA and/or other companies?'"/>
            <xsl:with-param name="sectionGroupName" select="'policies'"/>
            <xsl:with-param name="answerId" select="'havPndinApp'"/>
          </xsl:call-template>
        </tr>
        <tr class="odd">
          <xsl:call-template name="questionTmpl">
            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
            <xsl:with-param name="question" select="'3.
                Is this proposal to replace or intended to replace (in part or full)
                any existing or recently terminated insurance policy, or other investment product
                from AXA Insurance Pte Ltd or other Financial Institution?'"/>
            <xsl:with-param name="sectionGroupName" select="'policies'"/>
            <xsl:with-param name="answerId" select="'isProslReplace'"/>
          </xsl:call-template>
        </tr>
        <tr>
          <td class="tdOneCol">
            <xsl:attribute name="colspan">
              <xsl:value-of select="$colspan"/>
            </xsl:attribute>

            <p class="statement">
              <span lang="EN-HK" class="warningPolicies"><br/></span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="warningPoliciesBold">WARNING NOTE:</span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="warningPolicies">It
                is usually disadvantageous to replace an existing life
                insurance policy or other investment product with a new one.
                Some of the factors to consider:</span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="warningPolicies">1.
                You may suffer a penalty for terminating the original policy or
                other investment product.</span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="warningPolicies">2.
                You may incur transaction costs without gaining any real
                benefit from replacing the policy or other investment product.</span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="warningPolicies">3.
                You may not be insurable on standard terms or may have to pay a
                higher premium in view of higher age or the financial benefits
                accumulated over the years may be lost.</span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="warningPolicies">In
                your own interest, we would advise that you consult your own
                financial consultant before making a final decision. Hear from
                both sides and make a careful comparison to ensure that you are
                making a decision that is in your best interest.</span>
            </p>
            <p class="statement">
              <span lang="EN-HK" class="warningPolicies"><br/></span>
            </p>
          </td>
        </tr>
        <xsl:if test="count(/root/showQuestions/policies/question) > 0">
          <tr>
            <td class="tdOneCol">
              <xsl:attribute name="colspan">
                <xsl:value-of select="$colspan"/>
              </xsl:attribute>


              <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                <xsl:for-each select="/root/insured">
                  <xsl:call-template name="policiesTableTmpl">
                    <xsl:with-param name="isLa" select="boolean(1)"/>
                    <xsl:with-param name="iCid" select="personalInfo/cid"/>
                  </xsl:call-template>
                </xsl:for-each>
              </xsl:if>
              <xsl:call-template name="policiesTableTmpl">
                <xsl:with-param name="isLa" select="boolean(0)"/>
                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
              </xsl:call-template>
            </td>
          </tr>
        </xsl:if>
      </table>
    </xsl:if>
    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
