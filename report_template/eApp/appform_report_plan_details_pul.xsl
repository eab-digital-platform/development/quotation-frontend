<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">



<xsl:template name="appform_pul_plan_details">
  <xsl:param  name="nodePlan"/>

  
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PLAN DETAILS</span>
    </p>

    <table class="dataGroupLast">
      <colgroup>
        <col style="width:28%"/>
        <col style="width:18%"/>
        <col style="width:22%"/>
        <col style="width:32%"/>
      </colgroup>

      <tr>
        <xsl:call-template  name="infoSectionHeaderNoneTmpl">
          <xsl:with-param   name="sectionHeader"  select="'Pulsar'"/>
          <xsl:with-param   name="colspan"        select="4"/>
        </xsl:call-template>
      </tr>
      
      <!-- Plan Name-->
      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Plan Name</span>
          </p>
        </td>

        <td>
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Premium Term</span>
          </p>
        </td>

        <td>
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Total Premium Amount</span>
          </p>
        </td>
      </tr>

      <!-- Pulsar-->
      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/planList/covName/en"/>
            </span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/planList/premTermDesc"/>
            </span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/planList/premium"/>
            </span>
          </p>
        </td>
      </tr>

      <!-- Other Plan Details-->
      <tr>
        <xsl:call-template  name="infoSectionHeaderNoneTmpl">
          <xsl:with-param   name="sectionHeader"  select="'Other Plan Details'"/>
          <xsl:with-param   name="colspan"        select="4"/>
        </xsl:call-template>
      </tr>

      <!-- Policy Currency-->
      <tr>
        <td style="padding-left: 7.2pt;">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Policy Currency</span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/ccy"/>
            </span>
          </p>
        </td>

        <td>
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Payment Mode</span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/paymentMode"/>
            </span>
          </p>
        </td>
      </tr>

      <!-- Death Benefit-->
      <tr class = "even">
        <td style="padding-left: 7.2pt;">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Death Benefit</span>
          </p>
        </td>

        <td>
          <p class="answer">
            <span lang="EN-HK" class="answer">
              <xsl:value-of select="$nodePlan/deathBenefit"/>
            </span>
          </p>
        </td>

        <xsl:choose>
          <xsl:when test="$nodePlan/deathBenefit= 'Enhanced'">
            <td>
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Insurance Charge</span>
              </p>
            </td>

            <td>
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="$nodePlan/insuranceCharge"/>
                </span>
              </p>
            </td>
          </xsl:when>

          <xsl:otherwise>
            <td></td>
            <td></td>
          </xsl:otherwise>
        </xsl:choose>
      </tr>

      <!-- Fund Details-->
      <tr>
        <xsl:call-template  name="infoSectionHeaderNoneTmpl">
          <xsl:with-param   name="sectionHeader"  select="'Fund Details'"/>
          <xsl:with-param   name="colspan"        select="4"/>
        </xsl:call-template>
      </tr>

      <!-- Fund Name-->
      <tr>
        <td colspan = "2" style="padding-left: 7.2pt;">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">Fund Name</span>
          </p>
        </td>

        <td colspan="2">
          <p class="title">
            <span lang="EN-HK" class="questionItalic">
              Fund Allocation (Regular Premium)
            </span>
          </p>
        </td>
      </tr>

      <xsl:for-each select="$nodePlan/fundList">
        <!-- Fund Row-->
        
        <xsl:choose>
          <xsl:when test="position() mod 2 = 0">
            
            <tr class="">
              <!-- doesnt work
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">
                  <xsl:value-of select="even"/>
                </xsl:attribute>
              </xsl:if>
              -->

              <td colspan = "2" style="padding-left: 7.2pt;">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./fundName/en"/>
                  </span>
                </p>
              </td>

              <td colspan="2">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./alloc"/>%
                  </span>
                </p>
              </td>
                
            </tr>
          </xsl:when>

          <xsl:otherwise>
            
            <tr class="even">
              <!-- doesnt work
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">
                  <xsl:value-of select="even"/>
                </xsl:attribute>
              </xsl:if>
              -->

              <td colspan = "2" style="padding-left: 7.2pt;">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./fundName/en"/>
                  </span>
                </p>
              </td>

              <td colspan="2">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./alloc"/>%
                  </span>
                </p>
              </td>
                
            </tr>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:for-each>
      


    </table>

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
   
  </div>

  

</xsl:template>

</xsl:stylesheet>
