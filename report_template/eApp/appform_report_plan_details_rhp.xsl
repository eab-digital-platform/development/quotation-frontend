<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_planDetails">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PLAN DETAILS</span>
    </p>

    <xsl:variable name="isRegular" select="/root/planDetails/paymentTerm = 'Regular'"/>
    <xsl:variable name="colspan">
      <xsl:choose>
        <xsl:when test="$isRegular = 'true'">
          <xsl:value-of select="5"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="4"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <table class="dataGroup">
      <col width="10%"/>
      <col width="40%"/>
      <xsl:choose>
        <xsl:when test="$isRegular = 'true'">
          <col width="22%"/>
          <col width="14%"/>
          <col width="14%"/>
        </xsl:when>
        <xsl:otherwise>
          <col width="25%"/>
          <col width="25%"/>
        </xsl:otherwise>
      </xsl:choose>
      <thead>
        <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
          <tr>
            <xsl:variable name="basicPlanName">
              <xsl:choose>
                <xsl:when test="/root/lang = 'en'">
                  <xsl:value-of select="./covName/en"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="./covName/zh-Hant"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="$basicPlanName"/>
              <xsl:with-param name="colspan" select="$colspan"/>
            </xsl:call-template>
          </tr>
        </xsl:for-each>
      </thead>
      <tbody>
        <tr class="dataGroup">
          <td class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic"><br/></span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Plan / Rider Name</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Guaranteed Annual Retirement Income/Benefit</span>
            </p>
          </td>
          <td >
            <xsl:attribute name="class">
              <xsl:choose>
                <xsl:when test="$isRegular = 'true'">
                  <xsl:value-of select="'tdMid'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="'tdLast padding'"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Policy Term</span>
            </p>
          </td>
          <xsl:if test="$isRegular = 'true'">
            <td class="tdLast padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Premium Term</span>
              </p>
            </td>
          </xsl:if>
        </tr>
        <tr class="dataGroup">
          <td class="tdFirst padding">
            <p class="title noPadding">
              <span lang="EN-HK" class="questionItalic">Basic Plan</span>
            </p>
          </td>
          <xsl:for-each select="/root/planDetails/displayPlanList">
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="./gteedAnnualRetireIncome = '0' or string-length(./gteedAnnualRetireIncome) = 0">-</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./gteedAnnualRetireIncome"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <xsl:attribute name="class">
                <xsl:choose>
                  <xsl:when test="$isRegular = 'true'">
                    <xsl:value-of select="'tdMid'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="'tdLast padding'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./polTermDesc"/>
                </span>
              </p>
            </td>
            <xsl:if test="$isRegular = 'true'">
              <td class="tdLast padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./premTermDesc"/>
                  </span>
                </p>
              </td>
            </xsl:if>
          </xsl:for-each>
        </tr>
        <xsl:for-each select="/root/planDetails/riderList">
          <tr class="dataGroup">
            <xsl:attribute name="class">
              <xsl:choose>
                <xsl:when test="position() mod 2 = 0">
                  <xsl:value-of select="'even'"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="'odd'"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <td class="tdFirst padding">
              <p class="title noPadding">
                <span lang="EN-HK" class="questionItalic">
                  <xsl:choose>
                    <xsl:when test="position() = 1">Rider</xsl:when>
                    <xsl:otherwise></xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="./gteedAnnualRetireIncome = '0' or string-length(./gteedAnnualRetireIncome) = 0">-</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./gteedAnnualRetireIncome"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td>
              <xsl:attribute name="class">
                <xsl:choose>
                  <xsl:when test="$isRegular = 'true'">
                    <xsl:value-of select="'tdMid'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="'tdLast padding'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./polTermDesc"/>
                </span>
              </p>
            </td>
            <xsl:if test="$isRegular = 'true'">
              <td class="tdLast padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./premTermDesc"/>
                  </span>
                </p>
              </td>
            </xsl:if>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>

    <!-- OTHER PLAN DETAILS START -->
    <table class="dataGroupLast">
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Other Plan Details'"/>
            <xsl:with-param name="colspan" select="8"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <col width="25%"/>
        <col width="25%"/>
        <col width="25%"/>
        <col width="25%"/>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Policy Currency</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/ccy"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Payment Term</span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/paymentTerm"/>
              </span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Payout Type</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/payoutType"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Backdating</span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/isBackDate"/>
              </span>
            </p>
          </td>

        </tr>
        <tr>
          <td colspan="2" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Payout Term</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/payoutTerm"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">
                <xsl:choose>
                  <xsl:when test="not(/root/planDetails/isBackDate = 'No')">Selected Commencement Date (dd/mm/yyyy)</xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:choose>
                  <xsl:when test="not(/root/planDetails/isBackDate = 'No')">
                    <xsl:value-of select="/root/planDetails/riskCommenDate"/>
                  </xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Retirement Age</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/retirementAge"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Total Premium Amount (including riders if any)</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/totYearPrem"/>
              </span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">
                <xsl:choose>
                  <xsl:when test="/root/originalData/planDetails/paymentMode ='L'">Payment Method</xsl:when>
                  <xsl:otherwise>Payment Mode</xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td colspan="6" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:choose>
                  <xsl:when test="/root/originalData/planDetails/paymentMode ='L'"><xsl:value-of select="/root/planDetails/paymentMethod"/></xsl:when>
                  <xsl:otherwise><xsl:value-of select="/root/planDetails/paymentMode"/></xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
      </tbody>
    </table>
    <!-- OTHER PLAN DETAILS END -->

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
