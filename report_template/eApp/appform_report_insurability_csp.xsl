<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_insurability">
    <xsl:if test="count(/root/showQuestions/insurability/question) > 0">
        <div class="section">
            <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 2) = 'HW']">
                <p class="sectionGroup">
                    <span class="sectionGroup">INSURABILITY INFORMATION</span>
                </p>
                <table class="dataGroupLastNoAutoColor">
                    <tr>
                        <xsl:call-template name="infoSectionHeaderLaTmpl">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="sectionHeader" select="'Height and Weight'"/>
                        </xsl:call-template>
                    </tr>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HW01'">
                        <tr class="odd">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'Height (m)'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HW01'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HW02'">
                        <tr class="even">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'Weight (kg)'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HW02'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HW03'">
                        <tr class="odd">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'Any weight change in the last 12 months? (kg)'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HW03'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HW03a'">
                        <tr class="even">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'Weight change'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HW03a'"/>
                                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HW03b'">
                        <tr class="odd">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'How many kg?'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HW03b'"/>
                                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HW03c'">
                        <tr class="even">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'Reason of weight change'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HW03c'"/>
                                <xsl:with-param name="repalceOtherAnswerId" select="'HW03c1'"/>
                                <xsl:with-param name="isSubQuestion" select="boolean(1)"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                </table>
            </xsl:if>
        </div>
        <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 3) = 'INS']">
            <div class="section">
                <xsl:if test="count(/root/showQuestions/insurability/question[substring(., 1, 2) = 'HW']) = 0">
                    <p class="sectionGroup">
                        <span class="sectionGroup">INSURABILITY INFORMATION</span>
                    </p>
                </xsl:if>
                <table class="dataGroup first">
                    <tr>
                        <xsl:call-template name="infoSectionHeaderLaTmpl">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="sectionHeader" select="'Insurance History'"/>
                        </xsl:call-template>
                    </tr>
                    <tr class="odd">
                        <xsl:call-template name="questionTmpl">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="question" select="'1.Have you ever made an application or application for reinstatement of a life, disability, accident, medical or critical illness insurance which has been accepted with an extra premium or on special terms, postponed, declined,withdrawn or is still being considered?'"/>
                            <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                            <xsl:with-param name="answerId" select="'INS01'"/>
                        </xsl:call-template>
                    </tr>
                    <xsl:if test="/root/showQuestions/insurability/question = 'INS01_DATA'">
                        <tr>
                            <td class="tdOneCol">
                                <xsl:attribute name="colspan">
                                    <xsl:value-of select="$colspan"/>
                                </xsl:attribute>
                                <table class="data">
                                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                                        <col width="72"/>
                                    </xsl:if>
                                    <col width="148"/>
                                    <col width="110"/>
                                    <col width="110"/>
                                    <col width="95"/>
                                    <col width="156"/>
                                    <tr>
                                        <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                                            <td class="tdAns">
                                                <p class="thAnsCenter">
                                                    <span lang="EN-HK" class="th">For</span>
                                                </p>
                                            </td>
                                        </xsl:if>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Name of Insurance Company</span>
                                            </p>
                                        </td>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Type of Coverage</span>
                                            </p>
                                        </td>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Date incurred (MM/YYYY)</span>
                                            </p>
                                        </td>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Condition of Special Terms</span>
                                            </p>
                                        </td>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Decision &amp; Detailed Reason(s) of special terms</span>
                                            </p>
                                        </td>
                                    </tr>
                                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                                        <xsl:for-each select="/root/insured">
                                            <xsl:for-each select="insurability/*[local-name() = 'INS01_DATA']">
                                                <tr class="odd">
                                                    <xsl:if test="position() = 1">
                                                        <td>
                                                            <xsl:attribute name="class">
                                                                <xsl:choose>
                                                                    <xsl:when test="count(/root/proposer/insurability/*[local-name() = 'INS01_DATA']) > 0">
                                                                        <xsl:value-of select="'tdFor'"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="'tdFor bottom'"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="rowspan">
                                                                <xsl:value-of select="last()"/>
                                                            </xsl:attribute>
                                                            <p>
                                                                <span lang="EN-HK" class="person">Life Assured</span>
                                                            </p>
                                                        </td>
                                                    </xsl:if>
                                                    <td class="tdAns">
                                                        <p class="tdAns">
                                                            <span lang="EN-HK" class="tdAns">
                                                                <xsl:value-of select="INS01a"/>
                                                            </span>
                                                        </p>
                                                    </td>
                                                    <td class="tdAns">
                                                        <xsl:if test="INS01b1 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Life
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                        <xsl:if test="INS01b2 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Total Permanent Disability
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                        <xsl:if test="INS01b3 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Critical Illness
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                        <xsl:if test="INS01b4 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Accident
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                        <xsl:if test="INS01b5 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                    </td>
                                                    <td class="tdAns">
                                                        <p class="tdAns">
                                                            <span lang="EN-HK" class="tdAns">
                                                                <xsl:value-of select="INS01c"/>
                                                            </span>
                                                        </p>
                                                    </td>
                                                    <td class="tdAns">
                                                        <p class="tdAnsLeft">
                                                            <span lang="EN-HK" class="tdAns">
                                                                <xsl:value-of select="INS01d"/>
                                                            </span>
                                                        </p>
                                                    </td>
                                                    <td class="tdAns">
                                                        <p class="tdAnsLeft">
                                                            <span lang="EN-HK" class="tdAns">
                                                                <xsl:value-of select="INS01e"/>
                                                            </span>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:for-each>
                                    </xsl:if>
                                    <xsl:for-each select="/root/proposer/insurability/*[local-name() = 'INS01_DATA']">
                                        <tr class="even">
                                            <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                                                <td class="tdFor bottom">
                                                    <xsl:attribute name="rowspan">
                                                        <xsl:value-of select="last()"/>
                                                    </xsl:attribute>
                                                    <p class="tdAns">
                                                        <span lang="EN-HK" class="person">Proposer</span>
                                                    </p>
                                                </td>
                                            </xsl:if>
                                            <td class="tdAns">
                                                <p class="tdAns">
                                                    <span lang="EN-HK" class="tdAns">
                                                        <xsl:value-of select="INS01a"/>
                                                    </span>
                                                </p>
                                            </td>
                                            <td class="tdAns">
                                                <xsl:if test="INS01b1 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Life
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                                <xsl:if test="INS01b2 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Total Permanent Disability
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                                <xsl:if test="INS01b3 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Critical Illness
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                                <xsl:if test="INS01b4 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Accident
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                                <xsl:if test="INS01b5 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                            </td>
                                            <td class="tdAns">
                                                <p class="tdAns">
                                                    <span lang="EN-HK" class="tdAns">
                                                        <xsl:value-of select="INS01c"/>
                                                    </span>
                                                </p>
                                            </td>
                                            <td class="tdAns">
                                                <p class="tdAnsLeft">
                                                    <span lang="EN-HK" class="tdAns">
                                                        <xsl:value-of select="INS01d"/>
                                                    </span>
                                                </p>
                                            </td>
                                            <td class="tdAns">
                                                <p class="tdAnsLeft">
                                                    <span lang="EN-HK" class="tdAns">
                                                        <xsl:value-of select="INS01e"/>
                                                    </span>
                                                </p>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                                <xsl:attribute name="colspan">
                                    <xsl:value-of select="$colspan"/>
                                </xsl:attribute>
                                <p class="tdAns">
                                    <span lang="EN-HK" class="tdAns">
                                        <br/>
                                    </span>
                                </p>
                            </td>
                        </tr>
                    </xsl:if>
                </table>
            </div>
            <div class="section">
                <table class="dataGroup last">
                    <tr class="even">
                        <xsl:call-template name="questionTmpl">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="question" select="'2.Are you presently receiving a disability benefit or incapable for work or have you ever made or intend to make any claim against any insurer for disability, accident, medical care,hospitalisation, critical illness and/or other benefits?'"/>
                            <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                            <xsl:with-param name="answerId" select="'INS02'"/>
                        </xsl:call-template>
                    </tr>
                    <xsl:if test="/root/showQuestions/insurability/question = 'INS02_DATA'">
                        <tr>
                            <td class="tdOneCol">
                                <xsl:attribute name="colspan">
                                    <xsl:value-of select="$colspan"/>
                                </xsl:attribute>
                                <table class="data">
                                    <xsl:choose>
                                        <xsl:when test="$lifeAssuredIsProposer = 'true'">
                                            <col width="140"/>
                                            <col width="118"/>
                                            <col width="80"/>
                                            <col width="112"/>
                                            <col width="240"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <col width="84"/>
                                            <col width="140"/>
                                            <col width="118"/>
                                            <col width="80"/>
                                            <col width="112"/>
                                            <col width="156"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <tr>
                                        <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                                            <td class="tdAns">
                                                <p class="thAnsCenter">
                                                    <span lang="EN-HK" class="th">For</span>
                                                </p>
                                            </td>
                                        </xsl:if>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Name of Insurance Company</span>
                                            </p>
                                        </td>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Type of the Claim</span>
                                            </p>
                                        </td>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Date incurred (MM/YYYY)</span>
                                            </p>
                                        </td>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Medical Condition</span>
                                            </p>
                                        </td>
                                        <td class="tdAns">
                                            <p class="thAnsCenter">
                                                <span lang="EN-HK" class="th">Claim Details</span>
                                            </p>
                                        </td>
                                    </tr>
                                    <xsl:if test="not($lifeAssuredIsProposer = 'true')">
                                        <xsl:for-each select="/root/insured">
                                            <xsl:for-each select="insurability/*[local-name() = 'INS02_DATA']">
                                                <tr class="odd">
                                                    <xsl:if test="position() = 1">
                                                        <td>
                                                            <xsl:attribute name="class">
                                                                <xsl:choose>
                                                                    <xsl:when test="count(/root/proposer/insurability/*[local-name() = 'INS02_DATA']) > 0">
                                                                        <xsl:value-of select="'tdFor'"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="'tdFor bottom'"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="rowspan">
                                                                <xsl:value-of select="last()"/>
                                                            </xsl:attribute>
                                                            <p>
                                                                <span lang="EN-HK" class="person">Life Assured</span>
                                                            </p>
                                                        </td>
                                                    </xsl:if>
                                                    <td class="tdAns">
                                                        <p class="tdAns">
                                                            <span lang="EN-HK" class="tdAns">
                                                                <xsl:value-of select="INS02a"/>
                                                            </span>
                                                        </p>
                                                    </td>
                                                    <td class="tdAns">
                                                        <xsl:if test="INS02b1 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Total Permanent Disability
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                        <xsl:if test="INS02b2 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Critical Illness
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                        <xsl:if test="INS02b3 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Accident
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                        <xsl:if test="INS02b4 = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                        <xsl:if test="other = 'Y'">
                                                            <p class="tdAnsLeft">
                                                                <span lang="EN-HK" class="tdAns">
                                                                    <xsl:copy-of select="$tick"/>
                                                                    <xsl:value-of select="resultOther"/>
                                                                </span>
                                                            </p>
                                                        </xsl:if>
                                                    </td>
                                                    <td class="tdAns" width="100">
                                                        <p class="tdAns">
                                                            <span lang="EN-HK" class="tdAns">
                                                                <xsl:value-of select="INS02c"/>
                                                            </span>
                                                        </p>
                                                    </td>
                                                    <td class="tdAns" width="100">
                                                        <p class="tdAnsLeft">
                                                            <span lang="EN-HK" class="tdAns">
                                                                <xsl:value-of select="INS02d"/>
                                                            </span>
                                                        </p>
                                                    </td>
                                                    <td class="tdAns" width="149">
                                                        <p class="tdAnsLeft">
                                                            <span lang="EN-HK" class="tdAns">
                                                                <xsl:value-of select="INS02e"/>
                                                            </span>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:for-each>
                                    </xsl:if>
                                    <xsl:for-each select="/root/proposer/insurability/*[local-name() = 'INS02_DATA']">
                                        <tr class="even">
                                            <xsl:if test="not($lifeAssuredIsProposer = 'true') and position() = 1">
                                                <td class="tdFor bottom">
                                                    <xsl:attribute name="rowspan">
                                                        <xsl:value-of select="last()"/>
                                                    </xsl:attribute>
                                                    <p class="tdAns">
                                                        <span lang="EN-HK" class="person">Proposer</span>
                                                    </p>
                                                </td>
                                            </xsl:if>
                                            <td class="tdAns">
                                                <p class="tdAns">
                                                    <span lang="EN-HK" class="tdAns">
                                                        <xsl:value-of select="INS02a"/>
                                                    </span>
                                                </p>
                                            </td>
                                            <td class="tdAns">
                                                <xsl:if test="INS02b1 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Total Permanent Disability
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                                <xsl:if test="INS02b2 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Critical Illness
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                                <xsl:if test="INS02b3 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Accident
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                                <xsl:if test="INS02b4 = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>Hospitalisation &amp; Surgical Benefit
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                                <xsl:if test="other = 'Y'">
                                                    <p class="tdAnsLeft">
                                                        <span lang="EN-HK" class="tdAns">
                                                            <xsl:copy-of select="$tick"/>
                                                            <xsl:value-of select="resultOther"/>
                                                        </span>
                                                    </p>
                                                </xsl:if>
                                            </td>
                                            <td class="tdAns">
                                                <p class="tdAns">
                                                    <span lang="EN-HK" class="tdAns">
                                                        <xsl:value-of select="INS02c"/>
                                                    </span>
                                                </p>
                                            </td>
                                            <td class="tdAns">
                                                <p class="tdAnsLeft">
                                                    <span lang="EN-HK" class="tdAns">
                                                        <xsl:value-of select="INS02d"/>
                                                    </span>
                                                </p>
                                            </td>
                                            <td class="tdAns">
                                                <p class="tdAnsLeft">
                                                    <span lang="EN-HK" class="tdAns">
                                                        <xsl:value-of select="INS02e"/>
                                                    </span>
                                                </p>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-top: none; border-bottom: none; border-left: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt">
                                <xsl:attribute name="colspan">
                                    <xsl:value-of select="$colspan"/>
                                </xsl:attribute>
                                <p class="tdAns">
                                    <span lang="EN-HK" class="tdAns">
                                        <br/>
                                    </span>
                                </p>
                            </td>
                        </tr>
                    </xsl:if>
                </table>
            </div>
        </xsl:if>
        <div class="section">
            <xsl:if test="/root/showQuestions/insurability/question[substring(., 1, 6) = 'HEALTH']">
                <table class="dataGroupLastNoAutoColor">
                    <tr>
                        <xsl:call-template name="infoSectionHeaderLaTmpl">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="sectionHeader" select="'Medical and Health Information'"/>
                        </xsl:call-template>
                    </tr>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH01_CSP'">
                        <tr class="odd">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'1. Have you been diagnosed with more than one cancer or had a recurrence of a cancer?'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HEALTH01_CSP'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH02_CSP'">
                        <tr class="even">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'2. Have you received any treatment related to cancer or carcinoma in situ (CIS) in the last 3 years? '"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HEALTH02_CSP'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH03_CSP'">
                        <tr class="odd">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'3. Please provide the types(s) of cancer or carcinoma in situ (CIS) you were diagnosed with in the past? (e.g. breast cancer, colon cancer, leukemia, etc) '"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HEALTH03_CSP'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH03a_CSP'">
                        <xsl:call-template name="questionTmplForTextbox">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="question" select="'Please provide type of cancer or carcinoma in situ.'"/>
                            <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                            <xsl:with-param name="answerId" select="'HEALTH03a_CSP'"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH04_CSP'">
                        <tr class="even">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'4. Please specify the stage of your Cancer at the time of diagnosis, if known. E.g. stage I, stage II, stage III or stage IV or any other. '"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HEALTH04_CSP'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH04a_CSP'">
                        <xsl:call-template name="questionTmplForTextbox">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="question" select="'Please provide details why stage of cancer is Not Known.'"/>
                            <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                            <xsl:with-param name="answerId" select="'HEALTH04a_CSP'"/>
                            <xsl:with-param name="bgColor" select="'even'"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH05_CSP'">
                        <tr class="odd">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'5. Have you fully undertaken and completed all treatments for cancer or carcinoma in situ (CIS) as suggested and supervised by your doctor(s)?'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HEALTH05_CSP'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH05a_CSP'">
                        <xsl:call-template name="questionTmplForTextbox">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="question" select="'Please provide details of your current treatment.'"/>
                            <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                            <xsl:with-param name="answerId" select="'HEALTH05a_CSP'"/>
                            <xsl:with-param name="bgColor" select="'odd'"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH06_CSP'">
                        <tr class="even">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'6. Have you been informed by your doctor that you are in complete remission with no evidence of cancer, carcinoma in situ (CIS), or signs and symptoms of cancer, with no requirement for any treatment other than regular cancer checkup?'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HEALTH06_CSP'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH06a_CSP'">
                        <xsl:call-template name="questionTmplForTextbox">
                            <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                            <xsl:with-param name="question" select="'Please provide details of your condition.'"/>
                            <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                            <xsl:with-param name="answerId" select="'HEALTH06a_CSP'"/>
                            <xsl:with-param name="bgColor" select="'even'"/>
                        </xsl:call-template>
                    </xsl:if>
                    <xsl:if test="/root/showQuestions/insurability/question = 'HEALTH07_CSP'">
                        <tr class="odd">
                            <xsl:call-template name="questionTmpl">
                                <xsl:with-param name="laIsProp" select="$lifeAssuredIsProposer"/>
                                <xsl:with-param name="question" select="'7. When did you last see your treating doctor for a full cancer check-up?'"/>
                                <xsl:with-param name="sectionGroupName" select="'insurability'"/>
                                <xsl:with-param name="answerId" select="'HEALTH07_CSP'"/>
                            </xsl:call-template>
                        </tr>
                    </xsl:if>
                </table>
            </xsl:if>
        </div>
        <p class="sectionGroup">
            <span class="sectionGroup">
                <br/>
            </span>
        </p>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>
