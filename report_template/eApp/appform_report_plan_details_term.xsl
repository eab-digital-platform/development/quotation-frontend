<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="appform_pdf_planDetails">
  <div class="section">
    <p class="sectionGroup">
      <span class="sectionGroup">PLAN DETAILS</span>
    </p>

    <table class="dataGroup">
      <col width="24.5%"/>
      <col width="17%"/>
      <col width="17%"/>
      <col width="17%"/>
      <col width="24.5%"/>
      <thead>
        <xsl:for-each select="/root/planDetails/planList[covCode = /root/baseProductCode]">
          <tr>
            <xsl:variable name="basicPlanName">
              <xsl:choose>
                <xsl:when test="/root/lang = 'en'">
                  <xsl:value-of select="./covName/en"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="./covName/zh-Hant"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:call-template name="infoSectionHeaderNoneTmpl">
              <xsl:with-param name="sectionHeader" select="$basicPlanName"/>
              <xsl:with-param name="colspan" select="5"/>
            </xsl:call-template>
          </tr>
        </xsl:for-each>
      </thead>

      <tbody>
        <tr class="dataGroup">
          <td class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Basic Plan</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Sum Assured/ Benefits</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Policy Term</span>
            </p>
          </td>
          <td class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Premium Term</span>
            </p>
          </td>
          <td class="tdLast padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Total Premium Amount (including riders, if any)</span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <xsl:for-each select="/root/planDetails/displayPlanList">
            <td class="tdFirst padding">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:choose>
                    <xsl:when test="/root/lang = 'en'">
                      <xsl:value-of select="./covName/en"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="./covName/zh-Hant"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./sumInsured"/>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./polTermDesc"/>
                </span>
              </p>
            </td>
            <td class="tdMid">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./premTermDesc"/>
                </span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="title">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="./totalPremium"/>
                </span>
              </p>
            </td>
          </xsl:for-each>
        </tr>
        <xsl:if test="/root/planDetails/hasRiders = 'Y'">
          <tr class="dataGroup">
            <td class="tdFirst padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Rider</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Sum Assured/ Benefits</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Policy Term</span>
              </p>
            </td>
            <td class="tdMid">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Premium Term</span>
              </p>
            </td>
            <td class="tdLast padding">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Premium</span>
              </p>
            </td>
          </tr>
          <xsl:for-each select="/root/planDetails/riderList">
            <tr class="dataGroup">
              <td class="tdFirst padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:choose>
                      <xsl:when test="/root/lang = 'en'">
                        <xsl:value-of select="./covName/en"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./covName/zh-Hant"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </span>
                </p>
              </td>
              <td class="tdMid">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:choose>
                      <xsl:when test="not(./sumInsured)">-</xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="./sumInsured"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </span>
                </p>
              </td>
              <td class="tdMid">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./polTermDesc"/>
                  </span>
                </p>
              </td>
              <td class="tdMid">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./premTermDesc"/>
                  </span>
                </p>
              </td>
              <td class="tdLast padding">
                <p class="answer">
                  <span lang="EN-HK" class="answer">
                    <xsl:value-of select="./premium"/>
                  </span>
                </p>
              </td>
            </tr>
          </xsl:for-each>
        </xsl:if>
      </tbody>
    </table>

    <table class="dataGroupLast">
      <thead>
        <tr>
          <xsl:call-template name="infoSectionHeaderNoneTmpl">
            <xsl:with-param name="sectionHeader" select="'Other Plan Details'"/>
            <xsl:with-param name="colspan" select="8"/>
          </xsl:call-template>
        </tr>
      </thead>
      <tbody>
        <col width="25%"/>
        <col width="25%"/>
        <col width="25%"/>
        <col width="25%"/>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Policy Currency</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/ccy"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Payment Mode</span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/paymentMode"/>
              </span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Plan Type</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/planType"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
                <span lang="EN-HK" class="questionItalic">Backdating</span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/isBackDate"/>
              </span>
            </p>
          </td>
        </tr>
        <tr class="dataGroup">
          <td colspan="2" class="tdFirst padding">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">Occupation Class</span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:value-of select="/root/planDetails/occupationClass"/>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdMid">
            <p class="title">
              <span lang="EN-HK" class="questionItalic">
                <xsl:choose>
                  <xsl:when test="not(/root/planDetails/isBackDate = 'No')">Selected Commencement Date (dd/mm/yyyy)</xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
          <td colspan="2" class="tdLast padding">
            <p class="answer">
              <span lang="EN-HK" class="answer">
                <xsl:choose>
                  <xsl:when test="not(/root/planDetails/isBackDate = 'No')">
                    <xsl:value-of select="/root/planDetails/riskCommenDate"/>
                  </xsl:when>
                  <xsl:otherwise></xsl:otherwise>
                </xsl:choose>
              </span>
            </p>
          </td>
        </tr>
        <xsl:if test="/root/originalData/planDetails/planType = 'toAge'">
          <tr class="dataGroup">
            <td colspan="2" class="tdFirst padding" style="border-bottom: solid windowtext 1.0pt;">
              <p class="title">
                <span lang="EN-HK" class="questionItalic">Indexation</span>
              </p>
            </td>
            <td colspan="6" class="tdLast padding" style="border-bottom: solid windowtext 1.0pt;">
              <p class="answer">
                <span lang="EN-HK" class="answer">
                  <xsl:value-of select="/root/planDetails/indexation"/>
                </span>
              </p>
            </td>
          </tr>
        </xsl:if>
      </tbody>
    </table>

    <p class="sectionGroup">
      <span class="sectionGroup"><br/></span>
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
