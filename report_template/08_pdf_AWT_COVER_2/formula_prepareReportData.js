function(quotation, planInfo, planDetails, extraPara) {
  var basicPlan = quotation.plans[0];
  var hasSpTopUp = quotation.policyOptions.topUpSelect;
  var hasRspTopUp = quotation.policyOptions.rspSelect;
  var hasWithdrawal = quotation.policyOptions.wdSelect;
  var company = extraPara.company;
  var getGenderDesc = function(value) {
    return value == "M" ? "Male" : "Female";
  };
  var getSmokingDesc = function(value) {
    return value == "N" ? "Non-Smoker" : "Smoker";
  };
  var policyCcyDisplay;
  if (quotation.ccy === 'SGD') {
    policyCcyDisplay = 'Singapore Dollars';
  } else if (quotation.ccy === 'USD') {
    policyCcyDisplay = 'US Dollars';
  }
  return {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: basicPlan.planCode,
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      proposer: {
        name: quotation.pFullName,
        gender: getGenderDesc(quotation.pGender),
        dob: new Date(quotation.pDob).format(extraPara.dateFormat),
        age: quotation.pAge,
        smoking: getSmokingDesc(quotation.pSmoke)
      },
      insured: {
        name: quotation.iFullName,
        gender: getGenderDesc(quotation.iGender),
        dob: new Date(quotation.iDob).format(extraPara.dateFormat),
        age: quotation.iAge,
        smoking: getSmokingDesc(quotation.iSmoke)
      },
      riskCommenDate: new Date(quotation.riskCommenDate).format(extraPara.dateFormat),
      genDate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      plans: quotation.plans.map(function(plan) {
        return {
          name: plan.covName.en,
          polTermDesc: plan.polTermDesc,
          premTermDesc: plan.premTermDesc,
        };
      }),
      funds: quotation.fund.funds.map(function(fund) {
        var fRsp = (quotation.policyOptions.rspAmount || 0) * fund.alloc / 100;
        return {
          name: fund.fundName.en,
          code: fund.fundCode,
          alloc: fund.alloc + '%',
          topUpAlloc: hasSpTopUp ? fund.topUpAlloc + '%' : '-',
          premium: getCurrency(basicPlan.premium * fund.alloc / 100, '', 2),
          annualPremium: getCurrency(basicPlan.yearPrem * fund.alloc / 100, '', 2),
          rsp: getCurrency(fRsp, '', 2),
          annualRsp: getCurrency(fRsp * 12, '', 2),
          topUp: getCurrency((quotation.policyOptions.topUpAmt || 0) * fund.topUpAlloc / 100, '', 2)
        };
      }),
      withdrawal: {
        value: hasWithdrawal ? getCurrency(quotation.policyOptions.wdAmount, '', 2) : "N/A",
        from: hasWithdrawal ? quotation.policyOptions.wdFromAge : "N/A",
        to: hasWithdrawal ? quotation.policyOptions.wdToAge : "N/A"
      }
    },
    basicPlan: {
      name: basicPlan.covName.en,
      sumInsured: getCurrency(basicPlan.sumInsured, '', 2),
      premium: getCurrency(basicPlan.premium, '', 2),
      aPremium: getCurrency(basicPlan.yearPrem, '', 2),
      sPremium: getCurrency(basicPlan.halfYearPrem, '', 2),
      qPremium: getCurrency(basicPlan.quarterPrem, '', 2),
      mPremium: getCurrency(basicPlan.monthPrem, '', 2),
      spTopUp: getCurrency(hasSpTopUp ? quotation.policyOptions.topUpAmt : 0, '', 2),
      rspTopUp: getCurrency(hasRspTopUp ? quotation.policyOptions.rspAmount : 0, '', 2),
      annualRspTopUp: getCurrency(hasRspTopUp ? quotation.policyOptions.rspAmount * 12 : 0, '', 2),
      paymentModeDesc: quotation.paymentMode === 'A' ? 'Annual' : (quotation.paymentMode === 'S' ? 'Semi-Annual' : (quotation.paymentMode === 'Q' ? 'Quarterly' : 'Monthly')),
      deathBenefit: quotation.policyOptions.deathBenefit,
      deathBenefitDesc: quotation.policyOptionsDesc.deathBenefit.en,
      insuranceChargeDesc: quotation.policyOptions.insuranceCharge ? quotation.policyOptionsDesc.insuranceCharge.en : 'N/A',
      ccy: policyCcyDisplay,
      ccySymbol: quotation.ccy === 'SGD' ? 'S$' : (quotation.ccy === 'USD' ? 'US$' : '$'),
      hasTopUp: hasSpTopUp ? 'Y' : 'N',
      hasRspTopUp: hasRspTopUp ? 'Y' : 'N',
      hasWithdrawal: hasWithdrawal ? 'Y' : 'N'
    }
  };
}