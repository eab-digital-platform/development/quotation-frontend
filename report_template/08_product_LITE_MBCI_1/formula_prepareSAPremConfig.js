function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var refSA = 25000;
  for (var p in quotation.plans) {
    var plan = quotation.plans[p];
    if (plan.sumInsured) {
      if (plan.covCode === 'LITE_CIB') {
        refSA = plan.sumInsured;
      }
    }
  }
  var bpInfo = quotation.plans[0];
  var multiFactor = quotation.policyOptions.multiFactor;
  if (multiFactor) {
    var maxValue = quotation.iAge < 18 ? 500000 : 3000000;
    if (maxValue > bpInfo.sumInsured) {
      maxValue = bpInfo.sumInsured;
    }
    if (maxValue >= refSA) {
      planDetail.inputConfig.benlim = {
        min: refSA,
        max: maxValue
      };
    }
  }
}