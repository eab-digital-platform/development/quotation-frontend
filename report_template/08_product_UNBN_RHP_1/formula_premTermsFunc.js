function(planDetail, quotation) {
  var premTermsList = [];
  var premiumTerm = quotation.plans[0].premTerm;
  if (premiumTerm == undefined) {
    return premTermsList;
  }
  if (quotation.paymentMode == 'L') {
    premTermsList.push({
      value: 1,
      title: 'Single Premium',
      default: true
    });
  } else {
    var policyTerm = 0;
    var premTerm = 0;
    var testAge = 50 - premiumTerm;
    if (quotation.iAge <= testAge) {
      policyTerm = premiumTerm;
    } else if ((premiumTerm < 25) && (quotation.iAge <= 45) && (quotation.iAge > testAge)) {
      policyTerm = 50;
    } else if ((premiumTerm == 25) && (quotation.iAge > 25) && (quotation.iAge <= 40)) {
      policyTerm = 50;
    }
    if (policyTerm == 50) {
      premTerm = 50 - quotation.iAge;
    } else {
      premTerm = policyTerm;
    }
    premTermsList.push({
      value: premTerm,
      title: premTerm + ' Years',
      default: true
    });
  }
  return premTermsList;
}