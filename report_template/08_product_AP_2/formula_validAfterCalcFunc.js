function(quotation, planInfo, planDetail) {
  quotValid.validatePlanAfterCalc(quotation, planInfo, planDetail);
  planInfo.planCode = planInfo.premium ? 'AP' : null;
  if (planInfo.premium) {
    planInfo.policyTermYr = planInfo.policyTerm.endsWith('_YR') ? Number.parseInt(planInfo.policyTerm) : Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    if (planInfo.premTerm === 'SP') {
      planInfo.premTermYr = 1;
    } else if (planInfo.premTerm.endsWith('_YR')) {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      planInfo.premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
  }
}