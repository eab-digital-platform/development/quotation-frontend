function(quotation, planInfo, planDetails, extraPara) {
  var basicPlan = quotation.plans[0];
  var hasSpTopUp = quotation.policyOptions.topUpSelect;
  var hasRspTopUp = quotation.policyOptions.rspSelect;
  var hasWithdrawal = quotation.policyOptions.wdSelect;
  var company = extraPara.company;
  return {
    footer: {
      compName: company.compName,
      compRegNo: company.compRegNo,
      compAddr: company.compAddr,
      compAddr2: company.compAddr2,
      compTel: company.compTel,
      compFax: company.compFax,
      compWeb: company.compWeb,
      sysdate: new Date(extraPara.systemDate).format(extraPara.dateFormat),
      planCode: basicPlan.planCode,
      releaseVersion: "1"
    },
    cover: {
      sameAs: quotation.sameAs,
      polType: quotation.policyOptions.policytype,
      proposer: {
        name: quotation.pFullName,
      },
      insured: {
        name: quotation.iFullName,
      },
      plans: quotation.plans.map(function(plan) {
        return {
          name: plan.covName.en,
          polTermDesc: plan.polTermDesc,
          premTermDesc: plan.premTermDesc,
        };
      }),
    }
  };
}