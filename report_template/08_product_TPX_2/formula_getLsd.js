function(planDetail, sumInsured, age) {
  var lsdSA = planDetail.rates.LSD.SA;
  var lsdIndex = 0;
  for (var i = 0; i < lsdSA.length; i++) {
    if (sumInsured >= lsdSA[i]) {
      lsdIndex = i;
    }
  }
  var maxLsdAge = 0;
  for (var i in planDetail.rates.LSD) {
    var lsdAge = Number(i);
    if (typeof age === 'number' && age >= lsdAge && lsdAge > maxLsdAge) {
      maxLsdAge = lsdAge;
    }
  }
  return planDetail.rates.LSD[maxLsdAge + ''][lsdIndex];
}