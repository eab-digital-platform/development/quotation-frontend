function(planDetail, quotation, planDetails) {
  let planInfo = quotation.plans && quotation.plans.find(p => p.covCode === planDetail.covCode);
  let polTermList = quotDriver.runFunc(planDetail.formulas.polTermsFunc, planDetail, quotation);
  if (polTermList.length === 1) {
    planInfo.policyTerm = polTermList[0].value;
  } else if (!polTermList.find((opt) => opt.value === quotation.plans[0].policyTerm)) {
    quotation.plans[0].policyTerm = null;
  }
  planDetail.inputConfig.canEditPolicyTerm = !!quotation.policyOptions.planType && !!quotation.paymentMode;
  planDetail.inputConfig.policyTermList = polTermList;
  let premTermList = quotDriver.runFunc(planDetail.formulas.premTermsFunc, planDetail, quotation);
  if (premTermList.length === 1) {
    planInfo.premTerm = premTermList[0].value;
  } else if (!premTermList.find((opt) => opt.value === quotation.plans[0].premTerm)) {
    quotation.plans[0].premTerm = null;
  }
  planDetail.inputConfig.canEditPremTerm = planDetail.inputConfig.canEditPolicyTerm && !!quotation.plans[0].policyTerm;
  planDetail.inputConfig.premTermList = premTermList;
  if (quotation.policyOptions.planType === 'toAge') {
    var po = _.find(planDetail.inputConfig.policyOptions, po => po.id === 'indexation');
    po.disable = planInfo.policyTerm && planInfo.policyTerm === planInfo.premTerm ? 'N' : 'Y';
    if (po.disable === 'Y') {
      quotation.policyOptions.indexation = 'N';
    }
  }
}