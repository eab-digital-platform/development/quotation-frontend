function(quotation, planDetail, planDetails) {
  quotDriver.prepareQuotConfigs(quotation, planDetail, planDetails);
  var planType = quotation.policyOptions && quotation.policyOptions.planType;
  var bpPolTerm = (quotation.plans && quotation.plans[0] && quotation.plans[0].policyTerm) || '';
  var payModes = [];
  _.each(planDetail.inputConfig.payModes, (payMode) => {
    if (payMode.mode === quotation.paymentMode) {
      payMode.onChange = {
        to: payMode.mode === 'L' ? ['A', 'S', 'Q', 'M'] : 'L',
        action: 'resetQuot',
        params: {
          keepPolicyOptions: true
        }
      };
    }
    if (payMode.mode !== 'L' || planType === 'renew') {
      payModes.push(payMode);
    }
  });
  planDetail.inputConfig.payModes = payModes;
  if (!_.find(planDetail.inputConfig.payModes, (payMode) => payMode.mode === quotation.paymentMode)) {
    quotation.paymentMode = null;
  }
}