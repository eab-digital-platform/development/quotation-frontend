function(quotation, planInfo, planDetail) {
  var rates = planDetail.rates;
  var countryGroup = rates.countryGroup[quotation.iResidence];
  var substandardClass = rates.substandardClass[countryGroup]['LEPX'];
  var rateKey = planInfo.planCode + quotation.iGender + (quotation.iSmoke === 'Y' ? 'S' : 'NS') + substandardClass;
  if (planDetail.rates.residentialLoading[rateKey] && planDetail.rates.residentialLoading[rateKey][quotation.iAge]) {
    return planDetail.rates.residentialLoading[rateKey][quotation.iAge];
  }
  return 0;
}