function(quotation, planInfo, planDetails) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var petRates = rates.premRate;
  var sumSa = planInfo.sumInsured;
  var occClass = quotation.iOccupationClass;
  var plancode = 'AP';
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };

  function calculateRiderPremium(curPlanInfo) {
    var annualPremium = 0;
    var premRate = petRates[occClass];
    var minSa = 20000;
    var maxSa = 1000000;
    if ((sumSa * 10) < maxSa) {
      maxSa = sumSa * 10;
    }
    if (sumSa < minSa) {
      sumSa = minSa;
    } else if (sumSa > maxSa) {
      sumSa = maxSa;
    } else { /** set sa is multiples 1000 */
      sumSa = math.divide(sumSa, 1000);
      sumSa = trunc(sumSa, 0);
      sumSa = math.multiply(sumSa, 1000);
    }
    annualPremium = math.divide(math.multiply(math.bignumber(premRate), sumSa), math.bignumber(1000));
    annualPremium = round(annualPremium, 2);
    curPlanInfo.yearPrem = math.number(annualPremium);
    curPlanInfo.halfYearPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.51)), 2)); /** Math.floor(annualPremium * 0.51 * 100)/100; */
    curPlanInfo.quarterPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.26)), 2)); /** Math.floor(annualPremium * 0.26 * 100)/100;*/
    curPlanInfo.monthPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.0875)), 2)); /** Math.floor(annualPremium * 0.0875 * 100)/100;*/
    quotation.totYearPrem += curPlanInfo.yearPrem;
    quotation.totHalfyearPrem += curPlanInfo.halfYearPrem;
    quotation.totQuarterPrem += curPlanInfo.quarterPrem;
    quotation.totMonthPrem += curPlanInfo.monthPrem;
    switch (quotation.paymentMode) {
      case 'A':
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
      case 'S':
        {
          curPlanInfo.premium = curPlanInfo.halfYearPrem;quotation.premium += curPlanInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          curPlanInfo.premium = curPlanInfo.quarterPrem;quotation.premium += curPlanInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          curPlanInfo.premium = curPlanInfo.monthPrem;quotation.premium += curPlanInfo.monthPrem;
          break;
        }
      default:
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
    }
    curPlanInfo.planCode = plancode;
  }
  calculateRiderPremium(planInfo);
}