function(planDetail, quotation) {
  return [{
    value: '10_YR',
    title: '10 Years'
  }, {
    value: '20_YR',
    title: '20 Years'
  }, {
    value: '65_TA',
    title: 'To Age 65'
  }];
}