function(quotation, planInfo, planDetails, extraPara) {
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var planDetail = planDetails[planInfo.covCode];
  var sumInsured = math.bignumber(planInfo.sumInsured);
  var renew = planInfo.policyTerm.indexOf('YR') > -1;
  var renewYr = 0;
  var illYrs;
  if (renew) {
    renewYr = Number.parseInt(planInfo.policyTerm);
    illYrs = (Math.floor((60 - quotation.iAge) / renewYr) + 1) * renewYr;
  } else {
    illYrs = 65 - quotation.iAge;
  }
  var commRates = runFunc(planDetail.formulas.getCommRates, quotation, planInfo, planDetail);
  var totYearPrems = [0];
  var totComms = [0];
  for (var i = 1; i <= illYrs; i++) {
    var rateAge = quotation.iAge;
    if (renew) {
      rateAge = quotation.iAge + Math.floor((i - 1) / renewYr) * renewYr;
    }
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, rateAge));
    var premBeforeLsd = roundDown(math.multiply(premRate, sumInsured, 0.001), 2);
    var lsd = runFunc(planDetail.formulas.getLsd, planDetail, sumInsured);
    var premAfterLsd = math.subtract(premBeforeLsd, roundDown(math.multiply(premRate, sumInsured, 0.001, lsd), 2));
    var substdCls = planDetail.rates.substdCls[quotation.iResidence];
    var emLoading = substdCls === '1B' ? math.bignumber(0.5) : 0;
    var residenceLoading = math.round(math.multiply(math.divide(premAfterLsd, sumInsured), emLoading, 1000), 2);
    premAfterLsd = math.add(premAfterLsd, roundDown(math.multiply(residenceLoading, sumInsured, 0.001), 2));
    var yearPrem = premAfterLsd;
    totYearPrems.push(math.add(totYearPrems[i - 1], yearPrem));
    var tdcYr = renew ? i % renewYr : i;
    var commRate = commRates[tdcYr - 1] || 0;
    var comm = roundDown(math.multiply(yearPrem, math.bignumber(commRate)), 2);
    totComms.push(math.add(totComms[i - 1], comm));
  }
  var illustration = [];
  for (var i = 1; i <= illYrs; i++) {
    illustration.push({
      totYearPrem: Math.floor(math.number(totYearPrems[i])),
      tdc: Math.floor(math.number(totComms[i])),
      gtdDB: 10000,
      cibReset: planInfo.sumInsured * 1.5,
      cib: planInfo.sumInsured,
      surgery: planInfo.sumInsured,
      reconstructSurgery: planInfo.sumInsured,
      supportBenefit: 25000
    });
  }
  var bieannialMap = [{
    sumInsured: 25000,
    bieannial: 100
  }, {
    sumInsured: 50000,
    bieannial: 200
  }, {
    sumInsured: 100000,
    bieannial: 400
  }];
  var mapping = _.findLast(bieannialMap, m => sumInsured >= m.sumInsured);
  var bieannialAmt = mapping ? mapping.bieannial : 0;
  var illu = illustration[0];
  illu.bieannialPolAmt = bieannialAmt * Math.round(planInfo.policyTermYr / 2) - bieannialAmt;
  illu.childCov = 5000;
  var wp = math.multiply(math.bignumber(planInfo.yearPrem), 6);
  illu.wp = Math.round(math.number(wp));
  illu.totalBenefit = Math.round(math.number(math.add(wp, illu.cibReset, illu.surgery, illu.reconstructSurgery, illu.supportBenefit, illu.bieannialPolAmt, illu.childCov, illu.gtdDB)));
  return illustration;
}