function(quotation, planInfo, planDetail) {
  var rateKey;
  switch (quotation.dealerGroup) {
    case 'SYNERGY':
      rateKey = 'AGENCY';
      break;
    case 'FUSION':
      rateKey = 'BROKER';
      break;
    default:
      rateKey = quotation.dealerGroup;
  }
  var rateTable = planDetail.rates.commRate[rateKey];
  var commRates;
  if (rateTable) {
    var maxTerm = 0;
    var lookupYr = Number.parseInt(planInfo.policyTermYr);
    _.each(rateTable, (rates, key) => {
      var rateTerm = Number(key);
      if (lookupYr >= rateTerm && rateTerm > maxTerm) {
        commRates = rates;
        maxTerm = rateTerm;
      }
    });
  }
  return commRates || [];
}