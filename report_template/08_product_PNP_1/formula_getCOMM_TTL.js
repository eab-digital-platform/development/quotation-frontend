function(quotation, planDetail, planInfo) {
  var rateKey;
  switch (quotation.dealerGroup) {
    case 'SYNERGY':
      rateKey = 'AGENCY';
      break;
    case 'FUSION':
      rateKey = 'BROKER';
      break;
    default:
      rateKey = quotation.dealerGroup;
  }
  var BasicCommTable = planInfo.rates.BasicComm[rateKey];
  var BasicComm = 0;
  if (BasicCommTable) {
    var maxTerm = 0;
    var lookupYr = Number.parseInt(planDetail.policyTerm);
    _.each(BasicCommTable, (rates, key) => {
      var rateTerm = Number(key);
      if (lookupYr >= rateTerm && rateTerm > maxTerm) {
        BasicComm = rates;
        maxTerm = rateTerm;
      }
    });
  }
  var OverridesTable = planInfo.rates.Overrides[rateKey];
  var Overrides = 0;
  if (OverridesTable) {
    var maxTerm = 0;
    var lookupYr = Number.parseInt(planDetail.policyTerm);
    _.each(OverridesTable, (rates, key) => {
      var rateTerm = Number(key);
      if (lookupYr >= rateTerm && rateTerm > maxTerm) {
        Overrides = rates;
        maxTerm = rateTerm;
      }
    });
  }
  var MktAllowTable = planInfo.rates.MktAllow[rateKey];
  var MktAllow = 0;
  if (MktAllowTable) {
    var maxTerm = 0;
    var lookupYr = Number.parseInt(planDetail.policyTerm);
    _.each(MktAllowTable, (rates, key) => {
      var rateTerm = Number(key);
      if (lookupYr >= rateTerm && rateTerm > maxTerm) {
        MktAllow = rates;
        maxTerm = rateTerm;
      }
    });
  }
  var TDCLoading = 0.18;
  var GST = 0.07;
  var commRates = BasicComm * (1 + Overrides + MktAllow) * (1 + TDCLoading);
  if (rateKey == "BROKER") {
    commRates = commRates * (1 + GST);
  }
  return commRates;
}