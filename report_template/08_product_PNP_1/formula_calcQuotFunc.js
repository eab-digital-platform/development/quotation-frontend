function(quotation, planInfo, planDetails) {
  var policyTerm = Number.parseInt(planInfo.policyTerm);
  var i = 0;
  if (policyTerm == 3) {
    i = 0;
  } else if (policyTerm == 6) {
    i = 1;
  }
  var premium = planDetails[planInfo.covCode].rates.premRate[quotation.iAge][i];
  var ivfOptMultiplier = 1;
  if (quotation.policyOptions.IVF_OPT == "Y") {
    ivfOptMultiplier = 2
  }
  planInfo.premium = premium * ivfOptMultiplier;
  planInfo.yearPrem = planInfo.premium;
  quotation.totYearPrem = planInfo.premium;
  quotation.premium = planInfo.premium;
  planInfo.noApplication = true;
  planInfo.noApplicationMsg = "Under the Mum’s Advantage package, please proceed with paper submission together with the selected Investment Linked Plans (“ILP”).";
}