function(quotation, planInfo, planDetail) {
  quotValid.validateMandatoryFields(quotation, planInfo, planDetail);
  var planCodeMap = planDetail.planCodeMapping;
  if (planInfo.policyTerm) {
    var idx = planCodeMap && planCodeMap.policyTerm ? planCodeMap.policyTerm.indexOf(planInfo.policyTerm) : -1;
    if (idx >= 0) {
      planInfo.planCode = planCodeMap.planCode[idx];
    }
  }
  if (quotation.policyOptions.IVF_OPT == "Y") {
    planInfo.planCode = planInfo.planCode + "B";
  }
}