function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var bpInfo = quotation.plans[0];
  var amount = bpInfo.sumInsured > 0 ? bpInfo.sumInsured : 0;
  var multiFactor = quotation.policyOptions.multiFactor;
  if (multiFactor) {
    planDetail.inputConfig.benlim = {
      min: 20000,
      max: 2000000
    };
  }
}