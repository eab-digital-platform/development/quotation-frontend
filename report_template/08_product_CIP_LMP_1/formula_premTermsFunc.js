function(planDetail, quotation) {
  var premTermsList = [];
  var hasTa65 = false;
  var hasTa75 = false;
  var hasTa99 = false;
  _.each([10, 15, 20, 25], (val) => {
    if (quotation.iAge + val <= 80) {
      premTermsList.push({
        value: val + '_YR',
        title: val + ' Years'
      });
      if (quotation.iAge + val === 60) {
        hasTa65 = true;
      }
      if (quotation.iAge + val === 75) {
        hasTa75 = true;
      }
      if (quotation.iAge + val === 99) {
        hasTa99 = true;
      }
    }
  });
  if (quotation.iAge < 65) {
    premTermsList.push({
      value: '65_TA',
      title: 'To Age 65'
    });
  }
  if (quotation.iAge < 75) {
    premTermsList.push({
      value: '75_TA',
      title: 'To Age 75'
    });
  }
  if (quotation.iAge < 99) {
    premTermsList.push({
      value: '99_TA',
      title: 'To Age 99'
    });
  }
  return premTermsList;
}