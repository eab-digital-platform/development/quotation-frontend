function(quotation, planDetail, planDetails) {
  quotDriver.prepareQuotConfigs(quotation, planDetail, planDetails);
  planDetail.policyOptions.forEach(function(pPolicyOption) {
    if (quotation.ccy === 'SGD') { /* nothing to do */ } else if (quotation.ccy === 'USD') {
      const ppayModes = [];
      _.each(planDetail.payModes, (payMode) => {
        if (payMode.mode !== 'M') {
          ppayModes.push(_.clone(payMode));
        }
      });
      if (quotation.paymentMode === 'M') {
        quotation.paymentMode = null;
      }
      planDetail.inputConfig.payModes = ppayModes;
    }
  });
}