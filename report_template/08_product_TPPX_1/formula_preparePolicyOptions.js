function(planDetail, quotation, planDetails) {
  quotDriver.preparePolicyOptions(planDetail, quotation, planDetails);
  var policyOptions = [];
  for (var p in planDetail.inputConfig.policyOptions) {
    var po = planDetail.inputConfig.policyOptions[p];
    if (po.id === 'planType') {
      if (quotation.policyOptions.planType === 'renew') {
        po.onChange = {
          to: 'toAge',
          action: 'resetQuot'
        };
      } else if (quotation.policyOptions.planType === 'toAge') {
        po.onChange = {
          to: 'renew',
          action: 'resetQuot'
        };
      }
    } else if (po.id === 'indexation') {
      if (quotation.policyOptions.planType !== 'toAge') {
        continue;
      }
      var bpPolicyTerm = quotation.plans[0].policyTerm;
      var bpPremTerm = quotation.plans[0].premTerm;
      po.disable = bpPolicyTerm && bpPremTerm && bpPolicyTerm === bpPremTerm ? 'N' : 'Y';
      if (po.disable === 'Y') {
        quotation.policyOptions.indexation = 'N';
      }
    }
    policyOptions.push(po);
  }
  planDetail.inputConfig.policyOptions = policyOptions;
  quotation.policyOptions.occupationClass = quotation.iOccupationClass;
}