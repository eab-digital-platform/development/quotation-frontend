function(planDetail, quotation) {
  var policyTermList = [];
  if (quotation.policyOptions.planType === 'renew') {
    var values;
    if (quotation.paymentMode === 'L') {
      values = [5, 10, 15];
    } else {
      values = [5, 10, 15, 20, 25];
      if (quotation.iAge <= 68) {
        values.push(30);
      }
    }
    for (var v in values) {
      var value = values[v];
      policyTermList.push({
        value: value + '_YR',
        title: value + ' Years'
      });
    }
  } else if (quotation.policyOptions.planType === 'toAge') {
    var values = [50, 55, 60, 65, 70, 75, 99];
    for (var v in values) {
      var value = values[v];
      if (value - quotation.iAge >= 5) {
        policyTermList.push({
          value: value + '_TA',
          title: 'To Age ' + value
        });
      }
    }
  }
  return policyTermList;
}