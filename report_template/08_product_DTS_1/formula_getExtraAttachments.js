function(agent, quotation, planDetails) {
  var files = [];
  for (var i = 0; i < quotation.plans.length; i++) {
    files.push({
      covCode: quotation.plans[i].covCode,
      fileId: 'prod_summary_1'
    });
  }
  var attachments = [{
    id: 'prodSummary',
    label: 'Product Summary',
    fileName: 'product_summary.pdf',
    files: files
  }];
  return attachments;
}