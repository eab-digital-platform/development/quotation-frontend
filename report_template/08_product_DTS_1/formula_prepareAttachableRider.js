function(planDetail, quotation, planDetails) {
  var riderList = [];
  if (quotation.paymentMode === 'L') {
    planDetail.inputConfig.riderList = riderList;
    return;
  }
  var index;
  var max_index = planDetail.riderList.length;
  var curRider;
  var policyTerm = parseInt(quotation.plans[0].policyTerm);
  for (index = 0; index < max_index; index++) {
    curRider = planDetail.riderList[index];
    if (curRider.covCode === 'DLZ') {
      curRider.condition = [{
        autoAttach: "",
        compulsory: "",
        country: "*",
        dealerGroup: "*",
        endAge: 60,
        saRate: "",
        saRule: "",
        staAge: 16
      }];
      riderList.push(curRider); /** riderList.push({ autoAttach: 'N', compulsory: 'N', covCode: 'DLZ', saRate: "", saRule: "0" });*/
    }
    if (curRider.covCode === 'AP_DTS') {
      if (quotation.iOccupationClass !== 'DCL') { /** when la occupation class is DCL, this rider will can not Choice */
        curRider.condition = [{
          autoAttach: "",
          compulsory: "",
          country: "*",
          dealerGroup: "*",
          endAge: 60,
          saRate: "",
          saRule: "",
          staAge: 16
        }];
        riderList.push(curRider); /**riderList.push({ autoAttach: 'N', compulsory: 'N', covCode: 'AP_DTS', saRate: "", saRule: "0" });*/
      }
    }
    if (quotation.sameAs === 'Y') { /** First party*/
      if (curRider.covCode === 'WPSR_DTS' && policyTerm >= 13) {
        curRider.condition = [{
          autoAttach: "",
          compulsory: "",
          country: "*",
          dealerGroup: "*",
          endAge: 55,
          saRate: "",
          saRule: "",
          staAge: 16
        }];
        riderList.push(curRider); /**riderList.push({ autoAttach: 'N', compulsory: 'N', covCode: 'WPSR_DTS', saRate: "", saRule: "0" });*/
      }
    } else { /** 3rd party*/
      if (policyTerm >= 13 && quotation.pAge >= 16 && quotation.pAge <= 55) {
        if (curRider.covCode === 'WPTN_DTS') {
          curRider.condition = [{
            autoAttach: "",
            compulsory: "",
            country: "*",
            dealerGroup: "*",
            endAge: 120,
            saRate: "",
            saRule: "",
            staAge: 0
          }];
          riderList.push(curRider); /**riderList.push({ autoAttach: 'N', compulsory: 'N', covCode: 'WPTN_DTS', saRate: "", saRule: "0" });*/
        }
        if (curRider.covCode === 'WPPT_DTS') {
          curRider.condition = [{
            autoAttach: "",
            compulsory: "",
            country: "*",
            dealerGroup: "*",
            endAge: 120,
            saRate: "",
            saRule: "",
            staAge: 0
          }];
          riderList.push(curRider); /**riderList.push({ autoAttach: 'N', compulsory: 'N', covCode: 'WPPT_DTS', saRate: "", saRule: "0" });*/
        }
      }
    }
  }
  var noattchDLZ = true;
  for (var j = 0; j < quotation.plans.length; j++) {
    if (quotation.plans[j].covCode == 'DLZ') {
      noattchDLZ = false;
    }
  }
  if (noattchDLZ) {
    riderList = riderList.filter(function(rider) {
      switch (rider.covCode) {
        case 'DLZ':
          return quotation.plans[0].sumInsured === undefined || quotation.plans[0].sumInsured <= 2000000;
        default:
          return true;
      }
    });
  } /**planDetail.inputConfig.riderList = riderList; planDetail.inputConfig.riderList = riderList;*/
  planDetail.riderList = riderList;
  quotDriver.prepareAttachableRider(planDetail, quotation, planDetails);
}