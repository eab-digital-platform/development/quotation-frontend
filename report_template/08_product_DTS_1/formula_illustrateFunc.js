function(quotation, planInfo, planDetails, extraPara) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var commissionTable = rates.comm;
  var RsaRate = rates.Rsa;
  const MAX_POLICY_YEAR = 100; /**---------------------*/
  var premTerm = parseInt(planInfo.premTerm);
  var policyTerm = parseInt(planInfo.policyTerm);
  var interRate = quotation.policyOptions.interRate;
  var yearPrem = planInfo.yearPrem;
  var sa = quotation.plans[0].sumInsured;
  var totalPrim = math.bignumber(0);
  var totaltdc = math.bignumber(0); /**--------------------*/
  var round = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var roundup = function(value, position) {
    var scale = math.pow(10, position);
    return math.divide(math.ceil(math.multiply(value, scale)), scale);
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };
  var convertBignumberToReadable = function(data) {
    for (var row in data) {
      for (var col in data[row]) {
        if (typeof(data[row][col]) !== 'string') {
          data[row][col] = math.number(data[row][col]);
        }
      }
    }
  };
  var calcIllustration = function(illustration) {
    var sustainTestResult = true;
    var rawData = [];
    var defaultRawData = {
      aPolicyYear: 0,
      lAttainedAgeEOY: quotation.iAge,
      mEndOfPolicyYearAge: "",
      premiumsPaid: math.bignumber(0),
      totalPremiumPaidToDate: math.bignumber(0),
      basicTdc: math.bignumber(0),
      totalBasicTdc: math.bignumber(0),
      dbGuaranteed: math.bignumber(0),
      surrGuaranteed: math.bignumber(0),
    };
    rawData.push(Object.assign({}, defaultRawData));
    for (var year = 1; year <= MAX_POLICY_YEAR; year++) {
      if (year <= policyTerm) {
        var ref = year + 'RSA' + interRate;
        var nextref = year + 1 + 'RSA' + interRate;
        var rsaRate = RsaRate[ref][policyTerm - 5];
        var nextrsaRate = 0;
        if ((year + 1) <= 30) {
          nextrsaRate = RsaRate[nextref][policyTerm - 5];
        }
        var row = year;
        if (row > 7) {
          row = 7;
        }
        var commRate = 0.0944; /** Single Premium Commission Rate is fixed 0.0944 */
        if (quotation.paymentMode !== 'L') {
          commRate = commissionTable[row][policyTerm - 10];
        }
        var currData = Object.assign({}, defaultRawData);
        var prevData = rawData[year - 1];
        currData.aPolicyYear = year;
        currData.lAttainedAgeEOY = quotation.iAge + year;
        currData.mEndOfPolicyYearAge = currData.aPolicyYear + '/' + (currData.lAttainedAgeEOY); /** premiums */
        if (year <= premTerm) {
          currData.premiumsPaid = math.bignumber(yearPrem);
        }
        currData.totalPremiumPaidToDate = !math.isZero(currData.premiumsPaid) ? math.add(prevData.totalPremiumPaidToDate, currData.premiumsPaid) : prevData.totalPremiumPaidToDate;
        totalPrim = math.max(totalPrim, currData.totalPremiumPaidToDate); /** TDC */
        currData.basicTdc = math.multiply(math.bignumber(commRate), currData.premiumsPaid);
        currData.totalBasicTdc = !math.isZero(currData.basicTdc) ? math.add(prevData.totalBasicTdc, currData.basicTdc) : prevData.totalBasicTdc;
        totaltdc = math.max(totaltdc, currData.totalBasicTdc); /** Death Benefit Guaranteed */
        currData.dbGuaranteed = math.divide(math.multiply(math.subtract(math.bignumber(rsaRate), math.divide(math.multiply(math.subtract(math.bignumber(rsaRate), math.bignumber(nextrsaRate)), math.bignumber(11)), math.bignumber(12))), math.bignumber(sa)), math.bignumber(1000)); /** Guaranteed Surrender Value */
        if (quotation.paymentMode === 'L') {
          currData.surrGuaranteed = math.multiply(math.bignumber(yearPrem), math.divide(math.multiply((policyTerm - year), math.bignumber(0.9)), math.bignumber(policyTerm)));
        }
      }
      var illData = illustration[year - 1];
      illData.policyYear = currData.aPolicyYear;
      illData.age = currData.lAttainedAgeEOY;
      illData.endOfPolicyYearAge = currData.mEndOfPolicyYearAge;
      illData.totalPremiumPaidToDate = math.number(round(currData.totalPremiumPaidToDate, 0));
      illData.guaranteedDeathBenefit = math.number(round(currData.dbGuaranteed, 0));
      illData.guaranteedSurrender = math.number(round(currData.surrGuaranteed, 0));
      illData.totalDistributionCost = math.number(round(currData.totalBasicTdc, 0));
      rawData.push(currData);
    }
    var covPageData = illustration[0].covPageData;
    var cover = covPageData.covPage;
    cover.totalPremiums = math.number(round(totalPrim, 0));
    cover.totaltdc = math.number(round(totaltdc, 0));
    cover.tdcPremPercen = math.number(trunc(math.multiply(math.divide(totaltdc, totalPrim), math.bignumber(100)), 2));
    convertBignumberToReadable(rawData);
    return sustainTestResult;
  };
  var illustration = [];
  var illustrationData = {
    policyYear: 0,
    age: 0,
    endOfPolicyYearAge: "",
    /** Guaranteed Death */ totalPremiumPaidToDate: 0,
    guaranteedDeathBenefit: 0,
    guaranteedSurrender: 0,
    /** Basic TDC */ totalDistributionCost: 0,
    /** Rider TDC */ suppRiderPremiumPaid: 0,
    suppTotalDistributionCost: 0
  };
  var covPageData = {
    covPage: {
      totalPremiums: 0,
      totaltdc: 0,
      tdcPremPercen: 0
    }
  };
  for (var y = 0; y < MAX_POLICY_YEAR; y++) {
    illustration.push(Object.assign({}, JSON.parse(JSON.stringify(illustrationData)), {
      covPageData
    }));
  }
  var sustainTestResult = calcIllustration(illustration);
  return illustration;
}