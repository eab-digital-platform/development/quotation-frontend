function(quotation, planInfo, planDetails) {
  var planDetail = planDetails[planInfo.covCode];
  var rates = planDetail.rates;
  var unRates = rates.PWUN_RATE;
  var sumAssuredUN = quotation.annualPremium;
  var premTrem = parseInt(quotation.plans[1].premTerm);
  var policyTerm = parseInt(quotation.plans[1].policyTerm);
  var ref = '';
  var plancode = '';
  if (policyTerm === 50) {
    ref = 'UNB50' + quotation.iGender + quotation.iSmoke;
    plancode = 'UNB50';
  } else if (premTrem < 10) {
    ref = '0' + premTrem + 'UNBN' + quotation.iGender + quotation.iSmoke;
    plancode = '0' + premTrem + 'UNBN';
  } else {
    ref = premTrem + 'UNBN' + quotation.iGender + quotation.iSmoke;
    plancode = premTrem + 'UNBN';
  }
  var getRate = function(ref, index, Rates) {
    var tempRates = Rates[ref];
    if (tempRates === null) {
      return 0;
    }
    return tempRates[index];
  };
  var trunc = function(value, position) {
    if (!value) {
      return null;
    }
    if (!position) {
      position = 0;
    }
    var sign = value < 0 ? -1 : 1;
    var scale = math.pow(10, position);
    return math.multiply(sign, math.divide(math.floor(math.multiply(math.abs(value), scale)), scale));
  };

  function calculateRiderPremiumWaiverUN(curPlanInfo) {
    var unRate = getRate(ref, quotation.iAge, unRates);
    var temp = trunc(math.divide(math.multiply(math.bignumber(unRate), math.bignumber(sumAssuredUN)), math.bignumber(100)), 2); /* Math.floor(unRate * sumAssuredUN * 100)/100; TRUNCATE to 2 decimal places*/
    var annualPremium = temp; /** Update planInfo curPlanInfo.planCode = planCode;*/
    curPlanInfo.sumAssured = sumAssuredUN; /** curPlanInfo.policyTerm = policyTerm; curPlanInfo.polTermDesc = polTermDesc; curPlanInfo.premTerm = premTerm; curPlanInfo.premTermDesc = premTerm + ' Years'; */
    curPlanInfo.yearPrem = math.number(annualPremium);
    curPlanInfo.halfYearPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.51)), 2)); /** Math.floor(annualPremium * 0.51 * 100)/100;*/
    curPlanInfo.quarterPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.26)), 2)); /**Math.floor(annualPremium * 0.26 * 100)/100;*/
    curPlanInfo.monthPrem = math.number(trunc(math.multiply(math.bignumber(annualPremium), math.bignumber(0.0875)), 2)); /**Math.floor(annualPremium * 0.0875 * 100)/100;*/
    quotation.totYearPrem += curPlanInfo.yearPrem;
    quotation.totHalfyearPrem += curPlanInfo.halfYearPrem;
    quotation.totQuarterPrem += curPlanInfo.quarterPrem;
    quotation.totMonthPrem += curPlanInfo.monthPrem;
    switch (quotation.paymentMode) {
      case 'A':
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
      case 'S':
        {
          curPlanInfo.premium = curPlanInfo.halfYearPrem;quotation.premium += curPlanInfo.halfYearPrem;
          break;
        }
      case 'Q':
        {
          curPlanInfo.premium = curPlanInfo.quarterPrem;quotation.premium += curPlanInfo.quarterPrem;
          break;
        }
      case 'M':
        {
          curPlanInfo.premium = curPlanInfo.monthPrem;quotation.premium += curPlanInfo.monthPrem;
          break;
        }
      default:
        {
          curPlanInfo.premium = curPlanInfo.yearPrem;quotation.premium += curPlanInfo.yearPrem;
          break;
        }
    }
    curPlanInfo.planCode = plancode;
  }
  calculateRiderPremiumWaiverUN(planInfo);
}