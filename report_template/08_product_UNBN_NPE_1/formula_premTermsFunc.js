function(planDetail, quotation) {
  var premTermsList = [];
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var premiumTerm = quotation.plans[0].premTerm;
  var testAge = 50 - premiumTerm;
  var policyTerm = 0;
  if (quotation.iAge <= testAge) {
    policyTerm = premiumTerm;
  } else if (quotation.iAge > testAge) {
    policyTerm = 50;
  }
  var riderpremTerm = 0;
  if (policyTerm === 50) {
    riderpremTerm = 50 - quotation.iAge;
  } else {
    riderpremTerm = policyTerm;
  }
  premTermsList.push({
    value: riderpremTerm,
    title: riderpremTerm + ' Years',
    default: true
  });
  quotation.plans[1].premTerm = riderpremTerm;
  return premTermsList;
}