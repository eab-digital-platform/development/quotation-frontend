function(planDetail, quotation, planDetails) {
  quotDriver.prepareAttachableRider(planDetail, quotation, planDetails);
  var riderList = _.cloneDeep(planDetail.riderList) || [];
  var basicPlan = _.cloneDeep(quotation.plans[0]);
  var basicBenefit = quotation.policyOptions.basicBenefit;
  var selectedPlans = [];
  var formula = planDetail.formulas,
    isAgeInRangeFunc = formula.isAgeInRange;
  var iAge = quotation.iAge;
  for (var i in quotation.plans) {
    selectedPlans.push(quotation.plans[i].covCode);
  }
  for (var i = riderList.length - 1; i >= 0; i--) {
    var rider = riderList[i],
      riderId = rider.covCode,
      riderDetail = planDetails[riderId] || {};
    var entryAge = riderDetail.entryAge[0],
      isAgeInRange = quotDriver.runFunc(isAgeInRangeFunc, quotation, planDetail);
    if (riderId == "LARB") {
      if (basicBenefit != "M" || basicPlan.sumInsured > 2000000 || !isAgeInRange) {
        riderList.splice(i, 1);
      }
    } else if (riderId == "CARB") {
      if (basicBenefit != "C" || basicPlan.sumInsured > 2000000 || !isAgeInRange) {
        riderList.splice(i, 1);
      }
    } else if (["LVT", "LTT", "ADBR"].indexOf(riderId) > -1) {
      if (riderId === "ADBR" && quotation.policyOptions.occupationClass === 'DCL') {
        riderList.splice(i, 1);
      } else if (!isAgeInRange) {
        riderList.splice(i, 1);
      }
    } else if (riderId == "WPSR") {
      let larbIdx = selectedPlans.indexOf("LARB");
      if (quotation.sameAs != "Y") {
        riderList.splice(i, 1);
      }
    } else if (riderId == "WPTN") {
      if (quotation.sameAs == "Y") {
        riderList.splice(i, 1);
      }
    } else if (riderId == "WPPT") {
      if (quotation.sameAs == "Y") {
        riderList.splice(i, 1);
      }
    } else if (riderId == "EPB") {
      if (quotation.iAge > 50) {
        riderList.splice(i, 1);
      }
    }
  }
  planDetail.inputConfig.riderList = riderList;
  return planDetail.inputConfig.riderList;
}