function(quotation, planDetail) {
  var entryAge = planDetail.entryAge[0],
    min = entryAge.iminAge,
    max = entryAge.imaxAge,
    age = quotation.iAge,
    iDob = quotation.iDob;
  if (entryAge.iminAgeUnit != "month") {
    return age >= min && age <= max;
  } else {
    var dob = new Date(iDob),
      today = new Date();
    var dy = dob.getFullYear(),
      dm = dob.getMonth() + 1,
      dd = dob.getDate();
    var ty = today.getFullYear(),
      tm = today.getMonth() + 1,
      td = today.getDate();
    var month = (12 * (ty - dy)) + tm - dm - (tm != dm && dd > td ? 1 : 0);
    return month >= min && age <= max;
  }
}