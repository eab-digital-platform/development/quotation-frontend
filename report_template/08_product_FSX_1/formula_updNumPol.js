function(policy, policyValue, min, max) {
  var value = math.number(policyValue),
    id = policy.id,
    factor = math.number(policy.factor || 1);
  if (min !== null && !isNaN(min) && value < min) {
    return math.number(min);
  }
  if (max !== null && !isNaN(max) && value > max) {
    return math.number(max);
  }
  if (value % factor !== 0) {
    return math.number(math.multiply(math.floor(math.divide(math.bignumber(value), factor)), factor));
  }
  return value;
}