function(agent, quotation, planDetails) {
  var files = [];
  for (var i in quotation.plans) {
    files.push({
      covCode: quotation.plans[i].covCode,
      fileId: 'prod_summary_1'
    });
  }
  var attachments = [{
    id: 'FPXProdSummary',
    label: 'Product Summary',
    fileName: 'prod_summary.pdf',
    files: files
  }];
  return attachments;
}