function(planDetail, quotation, planDetails) {
  quotDriver.prepareAmountConfig(planDetail, quotation);
  var aggrMax = {
    SGD: 3000000,
    USD: 2097000,
    EUR: 1948000,
    GBP: 1442000,
    AUD: 2857000
  };
  var aggrMaxChild = {
    SGD: 500000,
    USD: 349000,
    EUR: 324000,
    GBP: 240000,
    AUD: 476000
  };
  var backDate = quotation.isBackDate;
  var dateToUse;
  if (backDate == 'Y') {
    dateToUse = new Date(quotation.riskCommenDate);
  } else {
    dateToUse = new Date();
  }
  var iAttainedAge = dateUtils.getAttainedAge(dateToUse, new Date(quotation.iDob)).year;
  var maxV = (iAttainedAge >= 16 ? aggrMax : aggrMaxChild)[quotation.ccy];
  planDetail.inputConfig.benlim = {
    min: 20000,
    max: maxV
  };
}