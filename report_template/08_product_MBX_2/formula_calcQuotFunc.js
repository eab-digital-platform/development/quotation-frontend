function(quotation, planInfo, planDetails) {
  if (quotation.plans[0].premTerm) {
    planInfo.premTerm = quotation.plans[0].premTerm;
    planInfo.premTermDesc = quotation.plans[0].premTermDesc;
  }
  if (quotation.policyOptions.multiFactor && planInfo.premTerm) { /* planCode needed for prem calculation */
    var planCode = 'MBX';
    var premTermYr;
    var policyTermYr;
    if (planInfo.premTerm.indexOf('YR') > -1) {
      premTermYr = Number.parseInt(planInfo.premTerm);
    } else {
      premTermYr = Number.parseInt(planInfo.premTerm) - quotation.iAge;
    }
    if (planInfo.policyTerm.indexOf('TA') > -1) {
      policyTermYr = Number.parseInt(planInfo.policyTerm) - quotation.iAge;
    } else {
      policyTermYr = Number.parseInt(planInfo.policyTerm);
    }
    planInfo.premTermYr = premTermYr;
    planInfo.policyTermYr = policyTermYr;
    var mbCodeMap = {
      "5": "A",
      "3.5": "B",
      "2.5": "C"
    };
    var mbCodeMapOlder = {
      "2.50": "A",
      "2.75": "B",
      "1.25": "C"
    };
    var group = quotation.iAge < 71 ? mbCodeMap[quotation.policyOptions.multiFactor] : mbCodeMapOlder[quotation.policyOptions.multiFactor];
    if (planInfo.premTerm.indexOf('YR') > -1) {
      planCode = Number.parseInt(planInfo.premTerm) + planCode + group;
    } else {
      planCode = planCode + group + Number.parseInt(planInfo.premTerm);
    }
    planInfo.planCode = planCode;
    planInfo.packagedRider = true;
    var bpInfo = quotation.plans[0];
    var amount = bpInfo.sumInsured > 0 ? bpInfo.sumInsured : 0;
    var multiFactor = quotation.policyOptions.multiFactor;
    if (multiFactor) {
      planInfo.sumInsured = bpInfo.sumInsured;
    }
    quotCalc.calcQuotPlan(quotation, planInfo, planDetails);
    if (planInfo.sumInsured) {
      planInfo.multiplyBenefit = planInfo.sumInsured * quotation.policyOptions.multiFactor;
      planInfo.multiplyBenefitAfter70 = planInfo.multiplyBenefit * 0.5;
      var policyYearRefer = 0;
      if (planInfo.premTerm.indexOf('YR') > -1) {
        policyYearRefer = Number.parseInt(planInfo.premTerm);
      } else {
        policyYearRefer = Number.parseInt(planInfo.premTerm) - quotation.iAge;
      }
      if (planInfo.premium) {
        var crate;
        var channel = quotation.agent.dealerGroup.toUpperCase();
        var commission_rate = planDetails['MBX'].rates.commissionRate[channel];
        planInfo.cummComm = [0];
        var cummComm = 0;
        for (var rate in commission_rate) {
          if (policyYearRefer > 25) {
            crate = commission_rate[rate][0];
          } else if (policyYearRefer >= 20 && policyYearRefer <= 24) {
            crate = commission_rate[rate][1];
          } else {
            crate = commission_rate[rate][2];
          }
          cummComm += crate * planInfo.premium;
          planInfo.cummComm.push(crate);
        }
      }
      var SA_bundleInfo = {
        "2.5": 40000,
        "3.5": 28571.43,
        "5": 20000
      };
      var SA_policy_lookup = SA_bundleInfo[quotation.policyOptions.multiFactor];
      var bundle_lsd = 0;
      if (SA_policy_lookup < 50000) {
        bundle_lsd = 0;
      } else if (SA_policy_lookup < 100000) {
        bundle_lsd = 1.00;
      } else if (SA_policy_lookup < 200000) {
        bundle_lsd = 1.50;
      } else {
        bundle_lsd = 1.80;
      }
      var premRate = runFunc(planDetails['MBX'].formulas.getPremRate, quotation, planInfo, planDetails['MBX'], quotation.iAge);
      var process_premRate = premRate + bundle_lsd;
      var annualPrem_bundle = Math.round(process_premRate * SA_policy_lookup * 0.001 * 100) / 100;
      var modalFactor = 1;
      for (var p in planDetails['MBX'].payModes) {
        if (planDetails['MBX'].payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetails['MBX'].payModes[p].factor;
        }
      }
      var modlePrem_bundle = Math.round(annualPrem_bundle * modalFactor * 100) / 100;
      planInfo.annualPrem_bundle = annualPrem_bundle;
      planInfo.modlePrem_bundle = modlePrem_bundle;
    } else {
      planInfo.multiplyBenefit = null;
      planInfo.multiplyBenefitAfter70 = null;
      planInfo.premium = null;
    }
  }
}