function(quotation, planInfo, planDetails, extraPara) {
  var illustrations = extraPara.illustrations[planInfo.covCode];
  var deathBenefit = [];
  var deathBenefitLast = [];
  var surrenderValue = [];
  var surrenderValueLast = [];
  var deductions = [];
  var deductionsLast = [];
  var totalDistributionCost = [];
  var totalDistributionCostLast = [];
  var afterWithdrawal = [];
  var dbsvDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    guaranteed: '',
    nonGuaranteedLow: '',
    totalLow: '',
    nonGuaranteedHigh: '',
    totalHigh: ''
  };
  var deductionsDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    valueOfPremiumPaidToDateLow: '',
    effectOfDeductionToDateLow: '',
    totalSurrenderValueLow: '',
    valueOfPremiumPaidToDateHigh: '',
    effectOfDeductionToDateHigh: '',
    totalSurrenderValueHigh: ''
  };
  var totalDistributionCostDefault = {
    policyYearAge: '',
    totalPremiumPaidToDate: '',
    totalDistributionCostToDate: ''
  };
  var planDetail = planDetails[planInfo.covCode];
  var aror = {
    low: planDetail.rates.aror.aror1 * 100,
    high: planDetail.rates.aror.aror2 * 100
  };
  var reduceYieldYear = math.max(20, 65 - quotation.iAge);
  var rYieldValue = 0;
  var rYieldReducedReturnRate = 0;
  if (illustrations instanceof Array) {
    var hasWithdrawal = quotation.policyOptions.wdSelect;
    var wdStartIndex = hasWithdrawal ? illustrations.findIndex(function(illustration) {
      return illustration.age === quotation.policyOptions.wdFromAge;
    }) + 1 : illustrations.length;
    illustrations.forEach(function(illustration, i) {
      var policyYear = illustration.policyYear;
      var age = illustration.age;
      var policyYearAge = illustration.policyYear + ' / ' + illustration.age;
      var iTotalPremiumPaidToDate = illustration.totalPremiumPaidToDate;
      var iGuaranteedDeathBenefit = illustration.guaranteedDeathBenefit;
      var iNonGuaranteedDeathBenefit = illustration.nonGuaranteedDeathBenefit;
      var iTotalDeathBenefit = illustration.totalDeathBenefit;
      var iSurrenderValue = illustration.surrenderValue;
      var iAccumulatedPremPaid = illustration.accumulatedPremPaid;
      var iEffectOfDeduction = illustration.effectOfDeduction;
      var iAccumulatedTotalDistCost = illustration.accumulatedTotalDistCost;
      var iAnnualWithdrawalAmount = illustration.annualWithdrawalAmount;
      var iNonGuaranteedAccValueAfterWD = illustration.nonGuaranteedAccValueAfterWD;
      var iTotalDeathBenefitAfterWD = illustration.totalDeathBenefitAfterWD;
      var iRYieldTotalEffectDeduction = illustration.rYieldTotalEffectDeduction;
      var iRYieldReducedReturnRate = illustration.rYieldReducedReturnRate;
      var deathBenefitRow = Object.assign({}, dbsvDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(iTotalPremiumPaidToDate, '', 0),
        guaranteed: getCurrency(iGuaranteedDeathBenefit, '', 0),
        nonGuaranteedLow: getCurrency(iNonGuaranteedDeathBenefit[aror.low], '', 0),
        totalLow: getCurrency(iTotalDeathBenefit[aror.low], '', 0),
        nonGuaranteedHigh: getCurrency(iNonGuaranteedDeathBenefit[aror.high], '', 0),
        totalHigh: getCurrency(iTotalDeathBenefit[aror.high], '', 0)
      });
      var surrenderValueRow = Object.assign({}, dbsvDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(iTotalPremiumPaidToDate, '', 0),
        guaranteed: getCurrency(0, '', 0),
        nonGuaranteedLow: getCurrency(iSurrenderValue[aror.low], '', 0),
        totalLow: getCurrency(iSurrenderValue[aror.low], '', 0),
        nonGuaranteedHigh: getCurrency(iSurrenderValue[aror.high], '', 0),
        totalHigh: getCurrency(iSurrenderValue[aror.high], '', 0)
      });
      var deductionsRow = Object.assign({}, deductionsDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(iTotalPremiumPaidToDate, '', 0),
        valueOfPremiumPaidToDateLow: getCurrency(iAccumulatedPremPaid[aror.low], '', 0),
        effectOfDeductionToDateLow: getCurrency(iEffectOfDeduction[aror.low], '', 0),
        totalSurrenderValueLow: getCurrency(iSurrenderValue[aror.low], '', 0),
        valueOfPremiumPaidToDateHigh: getCurrency(iAccumulatedPremPaid[aror.high], '', 0),
        effectOfDeductionToDateHigh: getCurrency(iEffectOfDeduction[aror.high], '', 0),
        totalSurrenderValueHigh: getCurrency(iSurrenderValue[aror.high], '', 0)
      });
      var totalDistributionCostRow = Object.assign({}, totalDistributionCostDefault, {
        policyYearAge: policyYearAge,
        totalPremiumPaidToDate: getCurrency(iTotalPremiumPaidToDate, '', 0),
        totalDistributionCostToDate: getCurrency(iAccumulatedTotalDistCost[aror.high], '', 0)
      });
      if (i < 20 || i < 40 && (i + 1) % 5 === 0 || i === illustrations.length - 1) {
        deathBenefit.push(Object.assign({}, deathBenefitRow, {
          policyYearAge: policyYearAge
        }));
        surrenderValue.push(Object.assign({}, surrenderValueRow, {
          policyYearAge: policyYearAge
        }));
        deductions.push(Object.assign({}, deductionsRow, {
          policyYearAge: policyYearAge
        }));
        totalDistributionCost.push(Object.assign({}, totalDistributionCostRow, {
          policyYearAge: policyYearAge
        }));
      }
      if ([55, 60, 65].indexOf(illustration.age) >= 0) {
        deathBenefitLast.push(Object.assign({}, deathBenefitRow, {
          policyYearAge: 'AGE ' + illustration.age
        }));
        surrenderValueLast.push(Object.assign({}, surrenderValueRow, {
          policyYearAge: 'AGE ' + illustration.age
        }));
        deductionsLast.push(Object.assign({}, deductionsRow, {
          policyYearAge: 'AGE ' + illustration.age
        }));
        totalDistributionCostLast.push(Object.assign({}, totalDistributionCostRow, {
          policyYearAge: 'AGE ' + illustration.age
        }));
      }
      var first20RecordEndIndex = wdStartIndex + 20;
      if (i >= wdStartIndex && (i < first20RecordEndIndex || i > first20RecordEndIndex && i < first20RecordEndIndex + 20 && (i - wdStartIndex + 1) % 5 === 0 || i === illustrations.length - 1)) {
        afterWithdrawal.push({
          policyYear: policyYear,
          age: age,
          annualWithdrawalAmountLow: getCurrency(iAnnualWithdrawalAmount[aror.low], '', 0),
          nonGuaranteedAccValueAfterWDLow: getCurrency(iNonGuaranteedAccValueAfterWD[aror.low], '', 0),
          totalDeathBenefitAfterWDLow: getCurrency(iTotalDeathBenefitAfterWD[aror.low], '', 0),
          annualWithdrawalAmountHigh: getCurrency(iAnnualWithdrawalAmount[aror.high], '', 0),
          nonGuaranteedAccValueAfterWDHigh: getCurrency(iNonGuaranteedAccValueAfterWD[aror.high], '', 0),
          totalDeathBenefitAfterWDHigh: getCurrency(iTotalDeathBenefitAfterWD[aror.high], '', 0)
        });
      }
      if (policyYear === reduceYieldYear) {
        rYieldValue = getCurrency(iRYieldTotalEffectDeduction[aror.high], '', 0);
      }
      if (i === illustrations.length - 1) {
        rYieldReducedReturnRate = iRYieldReducedReturnRate;
      }
    });
  }
  return {
    illustration: {
      deathBenefit: deathBenefit.concat([dbsvDefault]).concat(deathBenefitLast),
      surrenderValue: surrenderValue.concat([dbsvDefault]).concat(surrenderValueLast),
      deductions: deductions.concat([deductionsDefault]).concat(deductionsLast),
      totalDistributionCost: totalDistributionCost.concat([totalDistributionCostDefault]).concat(totalDistributionCostLast),
      reductionInYield: {
        age: 65 - quotation.iAge >= 20 ? 'at age 65' : 'over 20 years',
        value: rYieldValue,
        reducedReturnRate: rYieldReducedReturnRate
      },
      afterWithdrawal: afterWithdrawal
    },
    aror: aror
  };
}