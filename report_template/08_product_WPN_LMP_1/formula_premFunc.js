function(quotation, planInfo, planDetail) {
  console.log('function premFunc');
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (planInfo.premTerm && sumInsured) {
    var residenceLoading = math.bignumber(runFunc(planDetail.formulas.getResidenceLoading, quotation, planInfo, planDetail));
    var rates = planDetail.rates;
    var countryCode = quotation.iResidence;
    var countryGroup = rates.countryGroup[countryCode];
    var substandardClass = rates.substandardClass[countryGroup]['WP'];
    var loadingFactor = math.bignumber(rates.WPRLoadingFactor[substandardClass]);
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
    var isCountryMatch = (countryGroup === "Major City of China" || countryGroup === "Other City of China" || countryGroup === "Indonesia" || countryGroup === "Thailand");
    if (isCountryMatch) {
      residenceLoading = math.round(math.multiply(residenceLoading, loadingFactor, 1), 2);
      var premAfterLsd = roundDown(math.multiply(residenceLoading, math.bignumber(sumInsured), 0.001), 2);
      var modalFactor = 1;
      for (var p in planDetail.payModes) {
        if (planDetail.payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetail.payModes[p].factor;
        }
      }
      var prem = roundDown(math.multiply(premAfterLsd, modalFactor), 2);
      premium = math.number(prem);
    } else {
      var premAfterLsd = roundDown(math.multiply(premRate, math.bignumber(sumInsured), 0.01), 2);
      var modalFactor = 1;
      for (var p in planDetail.payModes) {
        if (planDetail.payModes[p].mode === quotation.paymentMode) {
          modalFactor = planDetail.payModes[p].factor;
        }
      }
      var prem = roundDown(math.multiply(premAfterLsd, modalFactor), 2);
      premium = math.number(prem);
    }
  }
  return premium;
}