function(quotation, planInfo, planDetail) { /**ESP premFunc*/
  var roundDown = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.floor(math.multiply(value, scale)), scale);
  };
  var numberTrunc = function(value, digit) {
    value = value.toString();
    if (value.indexOf('.') === -1) {
      return Number.parseInt(value);
    } else {
      value = value.substr(0, value.indexOf('.') + digit + 1);
      return Number.parseFloat(value);
    }
  };
  var round = function(value, digit) {
    var scale = math.pow(10, digit);
    return math.divide(math.round(math.multiply(value, scale)), scale);
  };
  var premium = null;
  var sumInsured = planInfo.sumInsured;
  if (planInfo.premTerm && sumInsured && planInfo.policyTerm) {
    var premRate = math.bignumber(runFunc(planDetail.formulas.getPremRate, quotation, planInfo, planDetail, quotation.iAge));
    var lsd = math.bignumber(runFunc(planDetail.formulas.getLSD, planDetail, sumInsured, planInfo.premTerm));
    premRate = roundDown(math.subtract(premRate, lsd), 2);
    var prem = round(math.divide(numberTrunc(math.multiply(premRate, sumInsured), 2), 1000), 2);
    var modalFactor = 1;
    for (var p in planDetail.payModes) {
      if (planDetail.payModes[p].mode === quotation.paymentMode) {
        modalFactor = planDetail.payModes[p].factor;
      }
    }
    prem = numberTrunc(math.multiply(prem, math.bignumber(modalFactor)), 2);
    premium = math.number(prem);
  } else {
    premium = null;
    planInfo.sumInsured = null;
    if (quotation.plans && quotation.plans[0]) {
      quotation.plans[0].sumInsured = null;
      quotation.plans[0].premium = null;
    }
  }
  return premium;
}