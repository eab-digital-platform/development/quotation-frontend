function(quotation, planInfo, planDetail, age) {
  var rateKey;
  if (planInfo.premTerm === 5) {
    var rates = planDetail.rates.premRate['ESP_5'];
  } else if (planInfo.premTerm === 10) {
    var rates = planDetail.rates.premRate['ESP_10'];
  }
  return rates[Number.parseInt(planInfo.policyTerm)][age] || 0;
}