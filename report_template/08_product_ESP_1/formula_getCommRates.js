function(quotation, planInfo, planDetails, planType) { /** getCommRates ESP*/
  var parsePrecentageToDecimal = function(value) {
    value = parseFloat(value);
    if (!isNaN(value)) {
      value = math.divide(value, 100);
    } else {
      value = 0;
    }
    return math.bignumber(value);
  };
  var rateKey, lookupYr;
  lookupYr = planInfo.policyTerm;
  rateKey = '';
  switch (quotation.dealerGroup) {
    case 'SYNERGY':
    case 'GIAGENCY':
    case 'BANCA':
      rateKey += 'AGENCY';
      break;
    case 'SINGPOST':
      rateKey += 'POST';
      break;
    case 'FUSION':
      rateKey += 'BROKER';
      break;
    default:
      rateKey += quotation.dealerGroup;
  }
  var planCode = runFunc(planDetails[planInfo.covCode].formulas.getRatePlanCodeMapping, planInfo);
  var commRates = [];
  commRates = planDetails[planInfo.covCode].rates.TDC_COMMON[planType + '_' + rateKey][planCode];
  var MA = 0;
  var GST = 0;
  var SI = 0;
  switch (rateKey) {
    case 'AGENCY':
      var overAllRates = planDetails[planInfo.covCode].rates.TDC_COMMON['OVERALL_' + rateKey][planCode] || [];
      commRates = commRates.map((commRate, index) => {
        return math.multiply(math.bignumber(parsePrecentageToDecimal(commRate)), math.add(1, math.bignumber(parsePrecentageToDecimal(overAllRates[index]))))
      });
      break;
    case 'BROKER':
      commRates = commRates.map((commRate, index) => {
        return math.multiply(math.add(math.bignumber(parsePrecentageToDecimal(commRate)), MA), math.add(1, math.bignumber(GST)))
      });
      break;
    case 'POST':
      commRates = commRates.map((commRate, index) => {
        return (index === 0) ? math.multiply(math.add(math.bignumber(parsePrecentageToDecimal(commRate)), MA, SI), math.add(1, math.bignumber(GST))) : parsePrecentageToDecimal(commRate);
      });
      break;
    case 'DIRECT':
      commRates = commRates.map((commRate, index) => {
        return parsePrecentageToDecimal(commRate);
      });
      break;
  }
  return commRates;
}