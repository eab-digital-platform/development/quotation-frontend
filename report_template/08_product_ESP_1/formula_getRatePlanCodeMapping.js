function(planInfo) {
  if (planInfo.premTerm < 10) {
    return '0' + planInfo.premTerm + 'HS' + planInfo.policyTerm;
  } else {
    return planInfo.premTerm + 'HS' + planInfo.policyTerm;
  }
}