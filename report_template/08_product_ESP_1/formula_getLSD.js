function(planDetail, sumInsured, premTerm) {
  var lsdSA = planDetail.rates.LSD.SA;
  var lsdIndex = 0;
  for (var i = 0; i < lsdSA.length; i++) {
    if (sumInsured >= lsdSA[i]) {
      lsdIndex = i;
    }
  }
  var rateKey = 0;
  for (var i in planDetail.rates.LSD) {
    var lsdPremTerm = Number(i);
    if (typeof premTerm === 'number' && premTerm == lsdPremTerm) {
      rateKey = lsdPremTerm;
    }
  }
  return planDetail.rates.LSD[rateKey + ''][lsdIndex];
}