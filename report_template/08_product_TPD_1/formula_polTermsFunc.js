function(planDetail, quotation) {
  const PAYOUT_TERM_LIFETIME = 99;
  var policyTermList = [];
  if (quotation.policyOptions.retirementAge === undefined) {
    return null;
  }
  if (quotation.policyOptions.payoutTerm === undefined) {
    return null;
  }
  if (quotation.plans[0].premTerm === undefined) {
    return null;
  }
  var retirementAge = parseInt(quotation.policyOptions.retirementAge);
  var payoutTerm = parseInt(quotation.policyOptions.payoutTerm);
  var benefitTerm = retirementAge;
  var polTermDesc = '';
  if (payoutTerm == PAYOUT_TERM_LIFETIME) {
    benefitTerm += 99 - retirementAge;
    benefitTerm -= quotation.iAge;
  } else {
    benefitTerm += payoutTerm;
    benefitTerm -= quotation.iAge;
  }
  var policyTerm = benefitTerm;
  if (payoutTerm == PAYOUT_TERM_LIFETIME) {
    policyTerm -= (99 - retirementAge);
  } else {
    policyTerm -= payoutTerm;
  }
  polTermDesc = policyTerm + ' Years';
  quotation.plans[1].policyTerm = policyTerm;
  quotation.plans[1].polTermDesc = polTermDesc;
  policyTermList.push({
    value: policyTerm,
    title: polTermDesc,
    default: true
  });
  return policyTermList;
}