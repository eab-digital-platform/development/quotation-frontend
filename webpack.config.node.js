var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './bz/index',

  output: {
    path: __dirname + '/prod/',
    libraryTarget: 'commonjs2',
    filename: 'bz.js'
  },

  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/
    }, {
      test: /\.json$/,
      loader: 'json-loader'
    }, {
      test: /\.(jpe?g|png|gif|svg)$/i,
      loader: 'url-loader?limit=1000'
    }, {
      test: /favicon\.ico$/,
      loader: 'url-loader',
      query: { 
        limit: 1,
        name: '[name].[ext]',
      },
    }],
    noParse: [/node_modules\/json-schema\/lib\/validate\.js/, /Quote\/math2\.js/]
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
      __DEV__: false,
      __PROD__: true,
      "process.env": {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      comments: false,
      compressor: {
        screw_ie8: true,
        warnings: false
      }
    })
  ],
  externals: {
    // put your node 3rd party libraries which can't be built with webpack here
    // (mysql, mongodb, and so on..)
    'java': 'java',
    'express-mailer':'express-mailer',
    'express': 'express',
    'html-pdf': 'html-pdf',
    'phantomjs-prebuild': 'phantomjs-prebuild',
    'forever-agent': 'forever-agent',
    'tough-cookie': 'tough-cookie',
    'tunnel-agent': 'tunnel-agent',
    'buffer': 'buffer',
    'ip': 'ip',
    'crypto': 'crypto',
    'crypto-js': 'crypto-js',
    'fs': 'fs',
    'request-ip': 'request-ip',
    'jstoxml': 'jstoxml',
    'helmet-csp': 'helmet-csp',
    'file-type': 'file-type',
    'http': 'http',
    'https': 'https',
    'couchbase': 'couchbase',
    'path': 'path',
    'request': 'request',
    'stream': 'stream',
    'iconv': 'iconv',
    'nodemailer': 'nodemailer',
    'nodemailer-smtp-transport': 'nodemailer-smtp-transport',
    'file-encryptor': 'file-encryptor',
    'node-rsa': 'node-rsa'
  }
};
