module.exports = {
  invalidationProducts:{
    'ASIM': {
      'baseProductIdList': [
        '08_product_ASIM_1',
        '08_product_ASIM_2',
        '08_product_ASIM_3'
      ],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    },
    'NPE': {
      'baseProductIdList': [
        '08_product_NPE_1',
        '08_product_NPE_2'
      ],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    },
    'LMP': {
      'baseProductIdList': [
        '08_product_LMP_7',
        '08_product_LMP_6',
        '08_product_LMP_5',
        '08_product_LMP_4',
        '08_product_LMP_3',
        '08_product_LMP_2',
        '08_product_LMP_1'
      ],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    },
    'AWT': {
      'baseProductIdList': [
        '08_product_AWT_10',
        '08_product_AWT_9',
        '08_product_AWT_8',
        '08_product_AWT_7',
        '08_product_AWT_6',
        '08_product_AWT_5',
        '08_product_AWT_4',
        '08_product_AWT_3',
        '08_product_AWT_2',
        '08_product_AWT_1'
      ],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    },
    'RHP': {
      'baseProductIdList': [
        '08_product_RHP_1',
        '08_product_RHP_2',
        '08_product_RHP_3',
        '08_product_RHP_4',
        '08_product_RHP_5',
        '08_product_RHP_6',
        '08_product_RHP_7',
        '08_product_RHP_8',
        '08_product_RHP_9'
      ],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    },
    'ESC': {
      'baseProductIdList': [
        '08_product_ESC_1'
      ],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    },
    'FPX': {
      'baseProductIdList': [
        '08_product_FPX_1',
        '08_product_FPX_2',
        '08_product_FPX_3',
        '08_product_FPX_4',
        '08_product_FPX_5',
        '08_product_FPX_6',
        '08_product_FPX_7',
        '08_product_FPX_8'
      ],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    },
    'FSX': {
      'baseProductIdList': [
        '08_product_FSX_1',
        '08_product_FSX_2',
        '08_product_FSX_3',
        '08_product_FSX_4',
        '08_product_FSX_5',
        '08_product_FSX_6',
        '08_product_FSX_7',
        '08_product_FSX_8'
      ],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    }
  }
};
