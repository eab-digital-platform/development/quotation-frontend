import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createHashHistory';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import App from './containers/App';

import './app.global.css';
import './reset.css';

import injectTapEventPlugin from 'react-tap-event-plugin';
import LoginPage from './containers/HomePage';
import MainPage from './containers/Landing';
import Quotation from './components/Pages/Pos/Quotation/Quotation';
import Proposal from './components/Pages/Pos/Proposal/Proposal';

require('./utils/DateFormater.js');
require('./utils/FieldTrigger.js');
require('./utils/CommonUtils.js');
require('./utils/ServerUtils.js');

const history = createHistory();
const router = routerMiddleware(history);
const enhancer = applyMiddleware(thunk, router);
const store = createStore(rootReducer, {/* inital state */}, enhancer);

injectTapEventPlugin();
window.isMobile = require('./utils/isMobile.js');
//<Route path="/land" component={MainPage} />

render(
  <Provider store={store}>
    <Router history={history}>
      <App>
        <Route exact path="/" component={Quotation} />
        {/* <Route exact path="/goApproval" component={LoginPage} /> */}
        <Route exact path="/proposal" component={Proposal} />
      </App>
    </Router>
  </Provider>,
  document.getElementById('root')
);
