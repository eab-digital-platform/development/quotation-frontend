module.exports = {
  appVersion: 1,
  defaultLang: 'en',
  expiration: 30, //in mins
  refreshInterval: 1,//in min
  enableRSA: false
};
