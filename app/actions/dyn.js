export const CHANGE_VALUES = 'CHANGE_VALUES';
export const CHANGE_FRAME = 'CHANGE_FRAME';
export const CHANGE_SECTION = 'CHANGE_SECTION';

export function changeValues(context, newValues, actionIndex, changedFieldId) {
  let {
    store
    // , router
  } = context;

  var dyn = store.getState().dyn;
  // var values = dyn.values;
  if (!newValues) {
    newValues = dyn.changedValues;
  }

  store.dispatch({
    type: CHANGE_VALUES,
    changedFieldId: changedFieldId
  });
}

export function changeFrame(context, newFrameId) {
  let {
    store
    // , router
  } = context;

  store.dispatch({
    type: CHANGE_FRAME,
    newFrameId: newFrameId
  });
}

export function changeSection(context, newSectionId) {
  let {
    store
    // , router
  } = context;

  store.dispatch({
    type: CHANGE_SECTION,
    newSectionId: newSectionId
  });
}
