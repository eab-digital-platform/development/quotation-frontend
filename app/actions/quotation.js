export const NEW_QUOTATION = 'NEW_QUOTATION';
export const CLOSE_QUOTATION = 'CLOSE_QUOTATION';
export const QUOTATION_RELOAD = 'QUOTATION_RELOAD';
export const REQUOTE = 'REQUOTE';
export const LOAD_AVAILABLE_FUNDS = 'LOAD_AVAILABLE_FUNDS';
export const ALLOC_FUNDS = 'ALLOC_FUNDS';
export const GEN_PROPOSAL_ERROR = 'GEN_PROPOSAL_ERROR';
export const SHOW_PROPOSAL_ERROR = 'SHOW_PROPOSAL_ERROR';
export const PROPOSAL_CONFIRM = 'PROPOSAL_CONFIRM';
export const CONFIRM_CREATE_QUOTE = 'CONFIRM_CREATE_QUOTE';
export const NEW_QUOTATION_ERROR = 'NEW_QUOTATION_ERROR';
import fs from 'fs';

import * as _ from 'lodash';

import {OPEN_PROPOSAL} from './proposal';
import SystemConstants from '../constants/SystemConstants';

export function closeQuotation(context, goFNA) {
  const {store} = context;
  store.dispatch({
    type: CLOSE_QUOTATION
  });
  if (context.goTab && goFNA) {
    context.goTab(SystemConstants.FEATURES.FNA);
  }
}

export function validateProductForQuotation(context, productId, insuredCid, proposerCid, callback) {
  window.callServer(context, '/product', {
    action: 'validateProductForQuot',
    productId: productId,
    insuredCid: insuredCid,
    proposerCid: proposerCid
  }, (resp) => {
    callback({
      success: resp.success === true && resp.viewable === true
    });
  }, true);
}

export function quotProduct(context, productId, iCid, pCid, ccy, quickQuote, callback) {
  const {store, langMap} = context;

  const confirming = !!store.getState().products.confirmMsg;
  window.callServer(context, '/quot', {
    action: 'initQuotation',
    productId: productId,
    iCid: iCid,
    pCid: pCid,
    quickQuote: quickQuote,
    params: {
      ccy: ccy
    },
    confirm: confirming
  }, (resp) => {
    if (resp.success) {
      if (resp.errorMsg) {
        store.dispatch({
          type: NEW_QUOTATION_ERROR,
          errorMsg: resp.errorMsg,
          profile: resp.profile
        });
      } else if (resp.requireConfirm) {
        store.dispatch({
          type: CONFIRM_CREATE_QUOTE,
          confirmMsg: window.getLocalizedText(langMap, 'products.confirm.invalidateApplications'),
          productId: productId
        });
      } else if (resp.quotation && resp.planDetails) {
        store.dispatch({
          type: NEW_QUOTATION,
          quotation: resp.quotation,
          planDetails: resp.planDetails,
          inputConfigs: resp.inputConfigs,
          quotWarnings: resp.quotWarnings,
          quotationErrors: resp.errors,
          availableInsureds: resp.availableInsureds,
          profile: resp.profile
        });
        callback instanceof Function && callback();
      }
    }
  });
}

export function quote(context, quotation) {
  const {store} = context;
  if (quotation){
    quotation.quotStatus = 'Updated';
  }
  window.callServer(context, '/quot', {
    action: 'quote',
    quotation: quotation
  }, (resp) => {
    var backdispatch = function() {
      store.dispatch({
        type: QUOTATION_RELOAD,
        quotWarnings: resp.quotWarnings,
        quotation: resp.quotation,
        inputConfigs: resp.inputConfigs,
        quotationErrors: resp.errors
      });
      this.ALLOC_FUNDS();
    };
    setTimeout(backdispatch, 5);
  });
}

export function requote(context, quotId) {
  const {store, langMap} = context;
  window.callServer(context, '/quot', {
    action: 'requote',
    quotId: quotId
  }, (resp) => {
    if (resp.quotation){
      resp.quotation.quotStatus = 'Requote';
      resp.quotation.quotStep = 0;
    }
    if (!resp.allowRequote) {
      let messageId = resp.messageId ? `proposal.error.${resp.messageId}` : 'proposal.error.requoteInvalid';
      store.dispatch({
        type: SHOW_PROPOSAL_ERROR,
        errorMsg: window.getLocalizedText(langMap, messageId)
      });
    }
    else {
      store.dispatch({
        type: REQUOTE,
        quotation: resp.quotation,
        planDetails: resp.planDetails,
        inputConfigs: resp.inputConfigs,
        quotWarnings: resp.quotWarnings,
        availableInsureds: resp.availableInsureds
      });
    }
  });
}

export function cloneQuotation(context, quotId, confirm) {
  const {store, langMap} = context;
  window.callServer(context, '/quot', {
    action: 'cloneQuotation',
    quotId,
    confirm
  }, (resp) => {
    if (resp.quotation){
      resp.quotation.quotStatus = 'Clone';
      resp.quotation.quotStep = 0;
    }
    if (resp.success) {
      if (!resp.allowClone) {
        let messageId = resp.messageId ? `proposal.error.${resp.messageId}` : 'proposal.error.clone';
        store.dispatch({
          type: SHOW_PROPOSAL_ERROR,
          errorMsg: window.getLocalizedText(langMap, messageId)
        });
      } else if (resp.errorMsg) {
        store.dispatch({
          type: SHOW_PROPOSAL_ERROR,
          errorMsg: resp.errorMsg
        });
      } else if (resp.requireConfirm) {
        store.dispatch({
          type: PROPOSAL_CONFIRM,
          confirmMsg: window.getLocalizedText(langMap, 'products.confirm.invalidateApplications'),
          onConfirmClone: () => {
            cloneQuotation(context, quotId, true);
          }
        });
      } else {
        store.dispatch({
          type: REQUOTE,
          quotation: resp.quotation,
          planDetails: resp.planDetails,
          inputConfigs: resp.inputConfigs,
          quotWarnings: resp.quotWarnings,
          availableInsureds: resp.availableInsureds
        });
      }
    }
  });
}

export function requoteInvalid(context, quotId, confirm, onConfirm, onError) {
  const {store, langMap} = context;
  window.callServer(context, '/quot', {
    action: 'requoteInvalid',
    quotId,
    confirm
  }, (resp) => {
    if (resp.quotation){
      resp.quotation.quotStatus = 'Requote';
      resp.quotation.quotStep = 0;
    }
    if (resp.success) {
      if (resp.requireConfirm) {
        if (onConfirm) {
          onConfirm(window.getLocalizedText(langMap, 'products.confirm.invalidateApplications'));
        } else {
          store.dispatch({
            type: PROPOSAL_CONFIRM,
            confirmMsg: window.getLocalizedText(langMap, 'products.confirm.invalidateApplications'),
            onConfirm: () => {
              requoteInvalid(context, quotId, true, onConfirm, onError);
            }
          });
        }
      } else if (!resp.allowRequote) {
        let messageId = resp.messageId ? `proposal.error.${resp.messageId}` : 'proposal.error.requoteInvalid';
        if (onError) {
          onError(window.getLocalizedText(langMap, messageId));
        } else {
            store.dispatch({
              type: SHOW_PROPOSAL_ERROR,
              errorMsg: window.getLocalizedText(langMap, messageId)
            });
        }
      } else {
        if (resp && resp.allowCreateQuotResult){
          let {isCreateNewBundle, profile} = resp.allowCreateQuotResult;
          if (isCreateNewBundle && profile){
            store.dispatch({
              type: 'UPDATE_PROFILE',
              profile: profile
            });
          }
        }
        store.dispatch({
          type: REQUOTE,
          quotation: resp.quotation,
          planDetails: resp.planDetails,
          inputConfigs: resp.inputConfigs,
          quotWarnings: resp.quotWarnings,
          availableInsureds: resp.availableInsureds,
          quotationErrors: resp.errors
        });
      }
    }
  });
}

export function resetQuot(context, quotation, keepConfigs, keepPolicyOptions, keepPlans, keepFunds, callback) {
  const {store} = context;
  window.callServer(context, '/quot', {
    action: 'resetQuot',
    quotation,
    keepConfigs,
    keepPolicyOptions,
    keepPlans,
    keepFunds
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: QUOTATION_RELOAD,
        quotation: resp.quotation,
        quotWarnings: resp.quotWarnings,
        inputConfigs: resp.inputConfigs,
        quotationErrors: resp.errors
      });
      callback && callback();
    }
  });
}

export function updateRiders(context, selectedCovCodes, callback) {
  const {store} = context;
  let {quotation} = store.getState().quotation;
  window.callServer(context, '/quot', {
    action: 'updateRiderList',
    quotation: quotation,
    newRiderList: selectedCovCodes
  }, (resp) => {
    if (resp.quotation){
      resp.quotation.quotStatus = 'Updated';
      resp.quotation.quotStep = 0;
    }
    store.dispatch({
      type: QUOTATION_RELOAD,
      quotation: resp.quotation,
      quotWarnings: resp.quotWarnings,
      inputConfigs: resp.inputConfigs,
      quotationErrors: resp.errors
    });
    callback instanceof Function && callback();
  });
}

export function queryFunds(context, invOpt, callback) {
  const {store} = context;
  let {quotation} = store.getState().quotation;
  window.callServer(context, '/quot', {
    action: 'getFundDetails',
    productId: quotation.baseProductId,
    invOpt: invOpt,
    paymentMethod: quotation.policyOptions && quotation.policyOptions.paymentMethod
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: LOAD_AVAILABLE_FUNDS,
        availableFunds: resp.funds
      });
      callback && callback();
    }
  });
}

export function allocFunds(context, quotation, invOpt, portfolio, fundAllocs, hasTopUpAlloc, topUpAllocs, adjustedModel, callback) {
  const {store} = context;
  window.callServer(context, '/quot', {
    action: 'allocFunds',
    quotation: quotation,
    invOpt: invOpt,
    portfolio: portfolio,
    fundAllocs: fundAllocs,
    hasTopUpAlloc,
    topUpAllocs,
    adjustedModel
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: ALLOC_FUNDS,
        quotation: resp.quotation,
        quotationErrors: resp.errors
      });
      callback && callback();
    }
  });
}

export function genProposal(context, quotation, errorCallback, warningCallback) {
  const {store} = context;
  window.callServer(context, '/quot', {
    action: 'genProposal',
    quotation: quotation
  }, (resp) => {
    if (resp.success) {
      if (resp.errorMsg) {
        if (quotation){
          quotation.quotStatus = 'HasError';
          quotation.quotStep = 1;
        }
        errorCallback && errorCallback(resp.errorMsg);
      } else {
        const openProposal = () => {
          store.dispatch({
            type: OPEN_PROPOSAL,
            quotation: resp.quotation,
            proposal: resp.proposal,
            illustrateData: resp.illustrateData,
            planDetails: resp.planDetails,
            canRequote: resp.funcPerms && resp.funcPerms.requote,
            canRequoteInvalid: resp.funcPerms && resp.funcPerms.requoteInvalid,
            canClone: resp.funcPerms && resp.funcPerms.clone,
            canEmail: true,
            isCreateProposal: true
          });
        };
        if (resp.warningMsg && warningCallback) {
          warningCallback(resp.warningMsg, openProposal);
        } else {
          openProposal();
        }
      }
    }
  });
}

export function getProposalByQuotId(context, quotId, readonly) {
  const {store} = context;
  window.callServer(context, '/quot', {
    action: 'getProposal',
    quotId: quotId,
    readonly: readonly
  }, (resp) => {
    if (resp.success) {
      store.dispatch({
        type: OPEN_PROPOSAL,
        quotation: resp.quotation,
        proposal: resp.proposal,
        illustrateData: resp.illustrateData,
        planDetails: resp.planDetails,
        canRequote: resp.funcPerms && resp.funcPerms.requote,
        canRequoteInvalid: resp.funcPerms && resp.funcPerms.requoteInvalid,
        canClone: resp.funcPerms && resp.funcPerms.clone,
        canEmail: !readonly
      });
    }
  });
}
