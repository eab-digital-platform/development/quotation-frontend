export const OPEN_BROCHURE = 'OPEN_BROCHURE';
export const CLOSE_BROCHURE = 'CLOSE_BROCHURE';

import {getProductId} from '../../common/ProductUtils';

export function openBrochure(context, product) {
  const {store} = context;
  window.callServer(context, '/product', {
    action: 'getBrochure',
    productId: getProductId(product)
  }, (resp) => {
  store.dispatch({
      type: 'OPEN_BROCHURE',
      data: resp.data,
      product: product
    });
  });
}

export function closeBrochure(context) {
  const {store} = context;
  store.dispatch({
    type: 'CLOSE_BROCHURE'
  });
}
