export const RESET_APPROVAL_FILTER = 'RESET_APPROVAL_FILTER';
export const RESET_WORKBENCH_FILTER = 'RESET_WORKBENCH_FILTER';
export const SET_APPROVAL_TEMPLATE = 'SET_APPROVAL_TEMPLATE';
export const SET_CASE_TEMPLATE = 'SET_CASE_TEMPLATE';
export const SEARCH_APPROVAL_CASES = 'SEARCH_APPROVAL_CASES';
export const INIT_APPROVALPAGE_TEMPLATE = 'INIT_APPROVALPAGE_TEMPLATE';
export const UPDATE_CASE_BY_ID = 'SEARCH_CASE_BY_ID';
export const UPDATE_SUP_DOC = 'UPDATE_SUP_DOC';
export const OPEN_APPROVAL_PAGE = 'OPEN_APPROVAL_PAGE';
export const UPDATE_AGENT_PROFILES = 'UPDATE_AGENT_PROFILES';
export const SET_WORKBENCH_TEMPLATE = 'SET_WORKBENCH_TEMPLATE';
export const SEARCH_WORKBENCH_CASES = 'SEARCH_WORKBENCH_CASES';
export const FAADMIN = 'FAADMIN';
export const FAAGENT = 'FAAGENT';
export const ONELEVEL_SUBMISSION = 'ONELEVEL_SUBMISSION';
export const OPEN_APPROVAL_EMAIL = 'OPEN_APPROVAL_EMAIL';
export const CLOSE_APPROVAL_EMAIL = 'CLOSE_APPROVAL_EMAIL';
export const RESET_CLOSE_APPROVAL = 'RESET_CLOSE_APPROVAL';
export const RESET_CLOSE_WORKBENCH = 'RESET_CLOSE_WORKBENCH';
export const CLOSE_APPROVAL_EAPP_MISSING_DIALOG = 'CLOSE_APPROVAL_EAPP_MISSING_DIALOG';


import * as _ from 'lodash';
import isFunction from 'lodash/isFunction';
import isArray from 'lodash/isArray';
import forEach from 'lodash/forEach';

import _isEqual from 'lodash/fp/isEqual';
import _isEmpty from 'lodash/fp/isEmpty';
import _get from 'lodash/fp/get';
import _getOr from 'lodash/fp/getOr';
import _clone from 'lodash/fp/clone';

import { DateFormat, DateTimeFormat3, DefaultEmailSender, PendingToApprovalListLink, MOMENT_TIME_ZONE } from '../constants/ConfigConstants';
import {USING_PENDING_APPROVAL_TEMPLATE, USING_WORKBENCH_TEMPLATE_USED} from '../components/Pages/Pos/Approval/main';
import moment from 'moment';

import {APPRVOAL_STATUS_APPROVED, APPRVOAL_STATUS_REJECTED, APPRVOAL_STATUS_PFAFA} from '../components/Pages/Pos/Approval/approvalStatus';
import {checkSuppDocsProperties} from './supportDocuments';

export const resetFilter = function(context){
  let {store} = context;
  let {approval} = store.getState();
  if (approval.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE) {
    store.dispatch({
      type: RESET_APPROVAL_FILTER,
      approvalFilter: {}
    });
  } else if (approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED){
    store.dispatch({
      type: RESET_WORKBENCH_FILTER,
      workbenchFilter: {}
    });
  }
};

export const resetFilterAndCloseApprovalPage = function(context, openApproval) {
  let {store} = context;
  let {approval} = store.getState();
  if (approval.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE) {
    store.dispatch({
      type: RESET_CLOSE_APPROVAL,
      approvalFilter: {},
      openApproval: open
    });
  } else if (approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED){
    store.dispatch({
      type: RESET_CLOSE_WORKBENCH,
      workbenchFilter: {},
      openApproval: open
    });
  }
}

export const getApprovalTemplate = function(context) {
  let {store} = context;

  window.callServer(
  context,
  '/approval',
  {
    action: 'getApprovalTemplate'
  },
  function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: SET_APPROVAL_TEMPLATE,
          approvalTemplate: resp.approvalTemplate || {},
          currentTemplate: USING_PENDING_APPROVAL_TEMPLATE
        });
      }
    }
  );
};

export const searchApprovalCases = function(context, filter, cb) {
  let {store} = context;
  let {app: {agentProfile: {compCode, agentCode}}} = store.getState();

  window.callServer(
    context,
    '/approval',
    {
      action: 'searchApprovalCases',
      compCode: compCode,
      id: agentCode,
      filter
    },
    function(resp){
      if (resp && resp.success) {
        store.dispatch({
          type: SEARCH_APPROVAL_CASES,
          searchedApprovalCases: resp.result || [],
          approvalFilter: filter || [],
          currentTemplate: USING_PENDING_APPROVAL_TEMPLATE
        });
        if (isFunction(cb)) {
          cb(resp.result);
        }
      }
    }
  );
};

let _handleFilterDate = (date) =>{
  let filterDate = new Date(date);
  return filterDate.getFullYear() + '-' + (filterDate.getMonth() + 1) + '-' + filterDate.getDate();
}

export const searchWorkbenchCases = function(context, filter, cb) {
  let {store} = context;
  let {app: {agentProfile: {compCode, agentCode}}} = store.getState();
  let modifiedFilter = {};
  _.each(filter, (value, key) =>{
    if (key.indexOf('_DATE') > -1 && typeof value === 'number') {
      modifiedFilter[key] = _clone(_handleFilterDate(filter[key]));
    } else {
      modifiedFilter[key] = _clone(filter[key]);
    }
  })
  window.callServer(
    context,
    '/approval',
    {
      action: 'searchWorkbenchCases',
      compCode: compCode,
      id: agentCode,
      filter: modifiedFilter
    },
    function(resp){
      if (resp && resp.success) {
        store.dispatch({
          type: SEARCH_WORKBENCH_CASES,
          searchedWorkbenchCases: resp.result || [],
          workbenchFilter: filter || [],
          currentTemplate: USING_WORKBENCH_TEMPLATE_USED
        });
        if (isFunction(cb)) {
          cb(resp.result);
        }
      }
    }
  );
};

export const searchApprovalCaseById = function(context, id, canApprove) {
  let {store} = context;
  window.callServer(
    context,
    '/approval',
    {
      action: 'searchApprovalCaseById',
      id
    },
    function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: resp.foundCase || {},
          approvalCaseHasIsCrossBorder: resp.isCrossBorder,
          approvalCaseHasIndividual: resp.hasTrustedIndividual,
          canApprove: canApprove
        });
      }
    }
  );
};

export const searchApprovalCaseByIdAndLockCase = function(context, id, canApprove, cb) {
  let {store} = context;
  window.callServer(
    context,
    '/approval',
    {
      action: 'searchApprovalCaseByIdAndLockCase',
      id
    },
    function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: resp.foundCase || {},
          approvalCaseHasIsCrossBorder: resp.isCrossBorder,
          approvalCaseHasIndividual: resp.hasTrustedIndividual,
          canApprove: canApprove
        });
        if (isFunction(cb)) {
          cb(resp.foundCase);
        }
      }
    }
  );
};
export const unlockApprovalCaseById = function(context, id, cb) {
  window.callServer(
    context,
    '/approval',
    {
      action: 'unlockApprovalCaseById',
      id
    },
    function(resp){
      if (resp && resp.success){
        if (isFunction(cb)) {
          cb(resp.success);
        }
      }
    }
  );
};

export const getEmailAllDocObj = (context, appId) => {
  const {store} = context;
  const agent = store.getState().app.agentProfile;
  window.callServer(
    context,
    '/email',
    {
      action: 'getEmailObject4AllDoc',
      appId: appId
    },
    (resp) => {
      if (resp && resp.success && resp.email) {
        const {email} = resp;
        let embedImgs = {};
        _.each(email.attachments, (att) => {
          embedImgs[att.cid] = att.data;
        });
        store.dispatch({
          type: OPEN_APPROVAL_EMAIL,
          emailObj: {
            id: 'approvalEmail',
            name: agent.name,
            toAddrs: [email.to],
            subject: email.title,
            content: email.content,
            attachments: _.sortBy(resp.availableAttachments, att => att.title && att.title.toLowerCase()),
            embedImgs: embedImgs
          }
        });
      }
    }
  );
};

export const closeEmailDialog = (context) => {
  context.store.dispatch({
    type: CLOSE_APPROVAL_EMAIL
  });
};

export const closeMissingEappDialog = (context) => {
  context.store.dispatch({
    type: CLOSE_APPROVAL_EAPP_MISSING_DIALOG
  });
}

export const emailAllDoc = function(context, appId, emails) {
  const {store} = context;
  if (emails && emails.length) {
    window.callServer(
      context,
      '/email',
      {
        action: 'sendAllDoc',
        appId,
        suppDocIds: emails[0].attachmentIds
      },
      (resp) => {
        if (resp && resp.success) {
          store.dispatch({
            type: CLOSE_APPROVAL_EMAIL
          });
        }
      }
    );
  }
};

export const updateApprovalCaseById = function(context, id, approvalCase, cb){
  let {store} = context;
  window.callServer(
    context,
    '/approval',
    {
      action: 'updateApprovalCaseById',
      id,
      approvalCase
    },
    function(resp){
      if (resp && resp.success){
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: resp.updatedCase || {},
          canApprove: true
        });
        cb(resp.updatedCase);
      }
    }
  );
};


export const genSupervisorPdf = function(context, id, approverLevel){
  let { lang} = context;
  window.callServer(
    context,
    '/approval',
    {
      action: 'genSupervisorPdf',
      id,
      lang,
      approverLevel
    },
    function(resp){
      if (resp && resp.success){
        let pdfName;
        (approverLevel === FAADMIN) ? pdfName = 'faFirm_Comment' : pdfName = 'eapproval_supervisor_pdf';
        new Promise( resolve => {
          resolve(uploadSupervisorPdf(context, id, resp.pdf, pdfName));
        }).then((result)=>{
          return result;
        });
      }
    }
  );
};

export const uploadSupervisorPdf = function(context, id, base64Str, pdfName) {
  window.callServer(
    context,
    '/approval',
    {
      action: 'uploadAttachment',
      id,
      base64Str,
      fileId: pdfName
    },
    function(resp){
      if (resp && resp.success){
        return resp.success;
      } else {
        return resp.success;
      }
    }
  );
};

export const getPendingForApprovalCaseListLength = function(context, cb) {
  let {store} = context;
  let {app:{agentProfile:{compCode,agentCode}}} = store.getState();
  let {app: {agentProfile}} = store.getState();

  let role = _identifyApproveLevel(agentProfile)
  window.callServer(
    context,
    '/approval',
    {
      action: 'getPendingForApprovalCaseListLength',
      compCode,
      id: agentCode,
      role,
      iPhone: window.isMobile.apple.phone
    },
    (resp) => {
      if (resp && resp.success) {
        store.dispatch({
          type: INIT_APPROVALPAGE_TEMPLATE,
          searchedApprovalCases: resp.searchedApprovalCases || [],
          reviewPageTemplate: resp.reviewPageTemplate[0],
          approvePageTemplate: resp.reviewPageTemplate[1],
          rejectPageTemplate: resp.reviewPageTemplate[2],
          reviewPage_selectedClient_tmpl: resp.reviewPageTemplate[3],
          reviewPage_jfw_tmpl: resp.reviewPageTemplate[4],
          approvalTemplate: resp.reviewPageTemplate[5],
          workbenchTemplate: resp.reviewPageTemplate[6],
          caseTemplate: resp.reviewPageTemplate[7],
        });
        cb(_getOr(0, 'length', resp));
      }
    }
  );
};

export const getDocAction = function(context, approvalCase, cb) {
  let {store} = context;
  let {app: {agentProfile}} = store.getState();

  if (approvalCase && _identifyApproveLevel(agentProfile) !== FAADMIN) {
    window.callServer(
      context,
      '/approval',
      {
        action: 'getDoc',
        id: approvalCase.applicationId
      },
      (resp) => {
        if (resp && resp.success) {
          let mandFlag = resp.foundDoc.isMandDocsAllUploaded;
          checkSuppDocsProperties(context, approvalCase.applicationId, (resp)=>{
            let completeFlag = false;

            if (mandFlag && resp && resp.success && resp.reviewDone && !resp.showHealthDeclarationPrompt) {
              completeFlag = true;
            }

            store.dispatch({
              type: UPDATE_SUP_DOC,
              supCompleteFlag: completeFlag,
              mandataryFlag: mandFlag,
              reviewDoneFlag: resp.reviewDone,
              healthDeclarationFlag: resp.showHealthDeclarationPrompt,
              showMissingEappPrompt: resp.showMissingEappPrompt
            });
            cb(completeFlag);
          });
        }
      }
    );
  } else if (approvalCase && _identifyApproveLevel(agentProfile) === FAADMIN) {
    //FA Admin no need to review the doc
    store.dispatch({
      type: UPDATE_SUP_DOC,
      supCompleteFlag: true
    });
    cb(true);
  }else {
    cb(false);
  }

};

export const openApprovalPage = function(context, open) {
  let {store} = context;
  store.dispatch({
    type: OPEN_APPROVAL_PAGE,
    openApproval: open,
  });
};

export const getAgentProfileId = function(context, agentCode, cb) {
  let {store} = context;
  let {app:{agentProfile:{compCode}}} = store.getState();

  window.callServer(
    context,
    '/approval',
    {
      action: 'getAgentProfileId',
      compCode,
      id: agentCode
    },
    (resp) => {
      if (resp && resp.success) {
        cb(resp.result.id);
      }
    }
  );
};

export const searchApprovalCaseByAppId = (context, appId, cb) => {
  window.callServer(
    context,
    '/approval',
    {
      action: 'searchApprovalCaseByAppId',
      appId
    },
    (resp) => {
      if (resp && resp.success) {
        cb(_get('approvalCase', resp));
      }
    }
  );
};

let _identifyApproveLevel = (agentProfile) =>{
  let channelType = _getOr('', 'channel.type', agentProfile );
  //let role = _getOr('', 'rawData.faAdvisorRole', agentProfile );
  const faAdminRole = _getOr('', 'rawData.userRole', agentProfile );
  if (channelType === 'FA' && faAdminRole === 'MM1') {
    return FAADMIN;
  } else if (channelType === 'FA') {
    return FAAGENT;
  } else {
    return ONELEVEL_SUBMISSION;
  }
}
module.exports.identifyApproveLevel = _identifyApproveLevel;

let _getApproveStatusByRole = (role) =>{
  if (role === FAADMIN) {
    return APPRVOAL_STATUS_APPROVED;
  } else if (role === FAAGENT) {
    return APPRVOAL_STATUS_PFAFA;
  } else {
    return APPRVOAL_STATUS_APPROVED;
  }
}
module.exports.getApproveStatusByRole = _getApproveStatusByRole;

module.exports.pendingDocNotification = (context, appId) => {
  window.callServer(
    context,
    '/approvalNotification',
    {
      action: 'pendingDocNotification',
      id: appId
    },
    (resp) => {
      if (resp && resp.success) {
        //
      }
    }
  );
};

module.exports.additionalDocNotification = (context, appId, isHealthDeclaration, cb) => {
  window.callServer(
    context,
    '/approvalNotification',
    {
      action: 'AdditionalDocumentNotification',
      id: appId,
      isHealthDeclaration: isHealthDeclaration
    },
    (resp) => {
      if (resp && resp.success) {
        cb();
      }else {
        cb();
      }
    }
  );
};

module.exports.approveCase = function(context, approvalCase, approveChangedValues, approvalCaseHasIndividual, cb ){
  let {store} = context;
  let jfwFileProperties = [];
  let coeFileProperties = [];
  if (approveChangedValues.jfwFile && approveChangedValues.jfwFile.length > 0) {
    approveChangedValues.jfwFile.forEach(obj => {
      jfwFileProperties.push({name: obj.file.name, type: obj.file.type});
    })
  }
  if (approveChangedValues.coeFile && approveChangedValues.coeFile.length > 0) {
    let feUploadDate =  moment().format('DD/MM/YYYY HH:mm:ss');
    approveChangedValues.coeFile.forEach(obj => {
      let fileSize = parseFloat(obj.file.size/1024).toFixed(1) + 'KB';
      coeFileProperties.push({
        title: obj.file.name,
        fileType: obj.file.type,
        length:obj.file.size,
        fileSize,
        feUploadDate
      });
    });
  }
  window.callServer(
    context,
    '/approval',
    {
      action: 'approveCase',
      approvalCase,
      approveChangedValues,
      approvalCaseHasIndividual,
      jfwFileProperties,
      coeFileProperties
    },
    (resp) => {
      if (resp && resp.success) {
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: resp.approvedCaase || {},
          canApprove: true
        });
        cb();
      }else {
        cb();
      }
    }
  );
}

module.exports.rejectCase = function(context, approvalCase, rejectChangedValues, approvalCaseHasIndividual, cb ){
  let {store} = context;

  window.callServer(
    context,
    '/approval',
    {
      action: 'rejectCase',
      approvalCase,
      rejectChangedValues,
      approvalCaseHasIndividual
    },
    (resp) => {
      if (resp && resp.success) {
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: resp.rejectCase || {},
          canApprove: true
        });
        cb();
      }else {
        cb();
      }
    }
  );
}

module.exports.saveComment = function(context, approvalId, comment, cb ){
  let {store} = context;

  window.callServer(
    context,
    '/approval',
    {
      action: 'saveComment',
      id: approvalId,
      comment
    },
    (resp) => {
      if (resp && resp.success) {
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: resp.updatedCase || {},
          canApprove: true
        });
        cb();
      }else {
        cb();
      }
    }
  );
}

module.exports.editComment = function(context, approvalId, comment, index, cb ){
  let {store} = context;

  window.callServer(
    context,
    '/approval',
    {
      action: 'editComment',
      id: approvalId,
      comment,
      index
    },
    (resp) => {
      if (resp && resp.success) {
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: resp.updatedCase || {},
          canApprove: true
        });
        cb();
      }else {
        cb();
      }
    }
  );
}

module.exports.deleteComment = function(context, approvalId, index, cb ){
  let {store} = context;

  window.callServer(
    context,
    '/approval',
    {
      action: 'deleteComment',
      id: approvalId,
      index
    },
    (resp) => {
      if (resp && resp.success) {
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: resp.updatedCase || {},
          canApprove: true
        });
        cb();
      }else {
        cb();
      }
    }
  );
}

export const viewJFWFiles = function(context, docId, attId, cb) {
  let {store} = context;
  window.callServer(
    context,
    '/approval',
    {
      action: 'viewJFWFiles',
      docId,
      attId
    },
    function(resp){
      if (resp && resp.success){
        cb(resp);
      }
    }
  );
};
