export const OPEN_PROPOSAL = 'OPEN_PROPOSAL';
export const SHOW_PROPOSAL = 'SHOW_PROPOSAL';
export const CLOSE_PROPOSAL = 'CLOSE_PROPOSAL';
export const SHOW_ILLUSTRATION = 'SHOW_ILLUSTRATION';
export const OPEN_EMAIL = 'OPEN_EMAIL';
export const CLOSE_EMAIL = 'CLOSE_EMAIL';
export const CLOSE_PROPOSAL_ERROR = 'CLOSE_PROPOSAL_ERROR';
export const CLOSE_PROPOSAL_CONFIRM = 'CLOSE_PROPOSAL_CONFIRM';

import * as _ from 'lodash';

import SystemConstants from '../constants/SystemConstants';
import {openHistory} from './quickQuote';

export function getPdfToken(context, docId, attachmentId, callback) {
  window.callServer(context, '/quot', {
    action: 'getPdfToken',
    docId: docId,
    attachmentId: attachmentId
  }, (resp) => {
    if (resp && resp.success) {
      callback && callback(resp.token);
    }
  });
}

export function closeProposal(context) {
  const {store} = context;
  const {quotation, isCreateProposal} = store.getState().proposal;
  store.dispatch({
    type: CLOSE_PROPOSAL,
    isCreateProposal: isCreateProposal
  });
  if (quotation && quotation.quickQuote) {
    openHistory(context);
  } else if (context.goTab) {
    context.goTab(SystemConstants.FEATURES.EAPP);
  }
}

let getEmailContent = function (content, params, embedImgs) {
  let ret = content;
  _.each(params, (value, key) => {
    ret = ret.replaceAll('{{' + key + '}}', value);
  });
  return ret;
};

export function openEmail(context, standalone) {
  const {store, langMap, lang} = context;
  const {app, client, proposal} = context.store.getState();
  window.callServer(context, '/quot', {
    action: 'getEmailTemplate',
    standalone: standalone
  }, (resp) => {
    if (resp && resp.success) {
      let {clientSubject, clientContent, agentSubject, agentContent, embedImgs} = resp;
      let params = {
        agentName: app.agentProfile.name,
        agentContactNum: app.agentProfile.mobile,
        clientName: client.profile.fullName
      };
      let attachments = _.map(proposal.pdfData, (pdf) => {
        return {
          id: pdf.id,
          title: window.getLocalText(lang, pdf.label)
        };
      });
      let emails = [{
        id: 'agent',
        name: window.getLocalizedText(langMap, 'email.recipient.agent'),
        toAddrs: [app.agentProfile.email],
        subject: agentSubject,
        content: getEmailContent(agentContent, params, embedImgs),
        attachments: attachments,
        embedImgs: embedImgs
      }, {
        id: 'client',
        name: window.getLocalizedText(langMap, 'email.recipient.client'),
        toAddrs: [client.profile.email],
        subject: clientSubject,
        content: getEmailContent(clientContent, params, embedImgs),
        attachments: attachments,
        embedImgs: embedImgs
      }];
      store.dispatch({
        type: OPEN_EMAIL,
        emails: emails
      });
    }
  });
}

export function closeEmail(context) {
  const {store} = context;
  store.dispatch({
    type: CLOSE_EMAIL
  });
}

export function sendEmail(context, emails) {
  const {store} = context;
  let quotation = store.getState().proposal.quotation;
  window.callServer(context, '/quot', {
    action: 'emailProposal',
    emails: emails,
    quotId: quotation.id
  }, (resp) => {
    if (resp && resp.success) {
      store.dispatch({
        type: CLOSE_EMAIL
      });
    }
  });
}

export function showIllustration(context) {
  const {store} = context;
  store.dispatch({
    type: SHOW_ILLUSTRATION
  });
}

export function backToProposal(context) {
  const {store} = context;
  store.dispatch({
    type: SHOW_PROPOSAL
  });
}

export function closeErrorMsg(context) {
  const {store} = context;
  store.dispatch({
    type: CLOSE_PROPOSAL_ERROR
  });
}

export function closeConfirmMsg(context) {
  const {store} = context;
  store.dispatch({
    type: CLOSE_PROPOSAL_CONFIRM
  });
}
