export const INIT_SIGNATURE_PAGE = 'INIT_SIGNATURE_PAGE';
export const SIGN_PDF_BY_ATTACHMENT_ID = 'SIGN_PDF_BY_ATTACHMENT_ID';
export const COMPLETE_SIGN = 'COMPLETE_SIGN';
export const SIGN_ERROR = 'SIGN_ERROR';
export const CLEAR_ERROR = 'CLEAR_ERROR';
export const CLOSE_SIGNATURE_PAGE = 'CLOSE_SIGNATURE_PAGE';
export const UPDATE_APPFORM_BACKDATE = 'UPDATE_APPFORM_BACKDATE';

import { POS_CHANGE_PAGE, POS_MAIN} from './application';
import * as _ from 'lodash';

export function initSignaturePage(
  agentSignFields, clientSignFields, textFields,
  tabCount, attachments, signDoc, isFaChannel, appStep, isMandDocsAllUploaded) {
  return {
    type: INIT_SIGNATURE_PAGE,
    agentSignFields,
    clientSignFields,
    textFields,
    tabCount,
    attachments,
    signDoc,
    isFaChannel,
    appStep,
    isMandDocsAllUploaded
  };
}

export function completeSign(appStep) {
  return {
    type: COMPLETE_SIGN,
    appStep
  };
}

export function signPdfByAttachmentId(tabIdx, newAttUrls, profile, appFormTemplate, application) {
  return {
    type: SIGN_PDF_BY_ATTACHMENT_ID,
    tabIdx,
    newAttUrls,
    profile,
    appFormTemplate,
    application
  };
}

export function signError(msg) {
  return {
    type: SIGN_ERROR,
    msg
  };
}

export function getUpdatedAttachmentUrl(context, signDocId, tabIdx, callback) {
  // let {
  //   store,
  //   router
  // } = context;
  window.callServer(context, '/application', {
    action: 'getUpdatedAttachmentUrl',
    signDocId: signDocId, 
    tabIdx: tabIdx
  },function(resp) {
    callback(resp);
  });
}

export function initSign(context, docId, callback){
  let {
    store,
    // router
  } = context;

  window.callServer(context, '/application', {
    action: 'getSignatureStatusFromCb',
    docId: docId
  },function(resp) {
    callback(resp.crossAge);
    if (resp.application) {
      store.dispatch(initSignaturePage(
        (resp.application.agentSignFields? resp.application.agentSignFields: []),
        (resp.application.clientSignFields? resp.application.clientSignFields: []),
        (resp.application.textFields? resp.application.textFields: []),
        resp.application.tabCount,
        resp.application.attachments,
        resp.signDoc,
        resp.application.isFaChannel,
        resp.application.appStep,
        resp.application.isMandDocsAllUploaded
      ));
    } else {
      store.dispatch(signError(JSON.stringify(resp)));
    }
  });
}

// export function checkCrossAge(context, appId, cb) {
//   window.callServer(
//     context,
//     '/application',
//     {
//       action: 'checkCrossAge',
//       appId: appId
//     },
//     cb,
//     true
//   );
// }

export function handleSign(context, signResult, tabIdx) {
  let {
    store
  } = context;

  let {
    appForm
  } = store.getState();

  if (signResult.sdweb_result === 'success') {
    getUpdatedAttachmentUrl(context, signResult.sdweb_docid, tabIdx, (resp) =>  {
      if (resp.success) {
        let template = window.isEmpty(resp.appFormTemplate)? appForm.template: resp.appFormTemplate;
        let application = window.isEmpty(resp.application)? appForm.application: resp.application;
        store.dispatch(signPdfByAttachmentId(tabIdx, resp.newAttUrls, resp.profile, template, application));
      } else {
        store.dispatch(signError(JSON.stringify(resp)));
      }
    })
  } else {
    store.dispatch(signError(signResult.sdweb_message? signResult.sdweb_message: (signResult.sdweb_errormessage? signResult.sdweb_errormessage: 'Sign Error')));
  }
}

export function updateAppStep(context, docId, stepperIndex, callback) {
  let {
    store
  } = context;

  window.callServer(
    context,
    '/application',
    {
      action: 'updateAppStep',
      docId: docId, 
      stepperIndex: stepperIndex
    },function(resp) {
      if (_.isFunction(callback)) {
        callback();
      }
      if (resp.success) {
        store.dispatch(completeSign(resp.appStep));
      } else {
        store.dispatch(signError(resp.result));
      }
    });
}

export function clearErrorMsg(context) {
  let {
    store
  } = context;
  store.dispatch({
    type: CLEAR_ERROR
  });
}


export function closeSignaturePage(context) {
  let {
    store
  } = context;

  store.dispatch({
    type: CLOSE_SIGNATURE_PAGE
  });
}

export function updateBIBackDateTrue(context, appId) {
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'updateBIBackDateTrue',
      appId
    },
    (resp)=>{
      if (resp.success) {
        store.dispatch({
          type: UPDATE_APPFORM_BACKDATE,
          isBackDate: resp.isBackDate,
          changedValues: resp.changedValues,
          attachments: resp.attachments
        });
      }
    }
  );
}

export function invalidateApplication(context, appId) {
  let {store} = context;
  window.callServer(
    context,
    '/application',
    {
      action: 'invalidateApplication',
      appId
    },
    ()=>{
      store.dispatch({
        type: POS_CHANGE_PAGE,
        page: POS_MAIN
      });
    }
  );
}
