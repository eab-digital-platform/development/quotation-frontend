import {
  EXIST_USER_LOGIN
}  from '../actions/home';

import {
  UPDATE_FNA,
  UPDATE_FNA_ERROR,
  UPDATE_PDA,
  UPDATE_PDA_ERROR,
  UPDATE_FE,
  UPDATE_FE_ERROR,
  INIT_NEEDS,
  GENERATE_FNA_REPORT,
  OPEN_EMAIL,
  CLOSE_EMAIL,
  RESET_NEEDS,
  CLEAR_FNA_REPORT,
  CLOSE_EMAILADDR_DIALOG,
  OPEN_EMAILADDR_DIALOG
} from '../actions/needs';

import {
  LOGOUT
} from '../actions/GlobalActions';


var getInitState = function() {
  return {
    template: {},
    pda: {},
    fna: {},
    fe: {},
    emailAddrInputDialogOpen: false,
    emailOpen: false
  }
  // completed: false
}

export default function client(state = getInitState(), action) {
  switch (action.type) {
    case EXIST_USER_LOGIN:
      return Object.assign(getInitState(), {
        template: action.needTemplate
      });
    case UPDATE_PDA:
      return Object.assign({}, state,{
        pda: action.pda,
        fe: action.fe || {},
        fna: action.fna || {}
      })
    case UPDATE_PDA_ERROR:
      return Object.assign({}, state,{
        pdaError: action.pdaError
      })
    case UPDATE_FE_ERROR:
      return Object.assign({}, state,{
        feError: action.feError
      })
    case UPDATE_FE:
      return Object.assign({}, state,{
        fe: action.fe,
        fna: action.fna || {}
      })
    case UPDATE_FNA:
      return Object.assign({}, state,{
        fna: action.fna
      })
    case UPDATE_FNA_ERROR:
      return Object.assign({}, state,{
        fnaError: action.fnaError
      })
    case INIT_NEEDS:
      return Object.assign({}, state, {
        fe: action.fe || {},
        pda: action.pda || {},
        fna: action.fna || {},
        needsSummary: action.needsSummary || {}
      })
    case RESET_NEEDS:
      return Object.assign({}, state, {
        fe: action.fe || {},
        pda: action.pda || {},
        fna: action.fna || {},
        needsSummary: action.needsSummary || {}
      })
    case GENERATE_FNA_REPORT:
      return Object.assign({}, state, {
        fnaReport: action.fnaReport || {}
      })
    case CLEAR_FNA_REPORT: 
      return Object.assign({}, state, {
        fnaReport: {}
      })
    case OPEN_EMAIL:
      return Object.assign({}, state, {
        emailOpen: true,
        emails: action.emails
      })
    case CLOSE_EMAIL:
      return Object.assign({}, state, {
        emailOpen: false,
        emails: null
      });
    case OPEN_EMAILADDR_DIALOG:
      return Object.assign({}, state, {
        emailAddrInputDialogOpen: true
      })
    case CLOSE_EMAILADDR_DIALOG: 
      return Object.assign({}, state, {
        emailAddrInputDialogOpen: false
      });
    case LOGOUT:
      return getInitState()
    default:
      return state;
  }
}
