import {
    GET_MATERIAL
  } from '../actions/downloadMaterial';

  import { LOGOUT } from '../actions/GlobalActions';

  var getInitState = function() {
    return {
        materialList: [],
    }
    // completed: false
  }

  export default function downloadMaterial(state = getInitState(), action) {
    switch (action.type) {
        case GET_MATERIAL:
            return Object.assign(state, {
             materialList: action.materialList
            });
        case LOGOUT:
            return getInitState();
        default:
            return state;
    }
  }