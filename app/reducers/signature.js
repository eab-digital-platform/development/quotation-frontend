import {
  INIT_SIGNATURE_PAGE,
  SIGN_PDF_BY_ATTACHMENT_ID,
  COMPLETE_SIGN,
  SIGN_ERROR,
  CLEAR_ERROR,
  CLOSE_SIGNATURE_PAGE,
  UPDATE_APPFORM_BACKDATE
} from '../actions/signature';
import { CLOSE_SUPPORT_DOCUMENTS } from '../actions/supportDocuments';
import { LOGOUT } from '../actions/GlobalActions';


const getInitState = () => {
  return {
    isFaChannel: false,
    appStep: 1,
    signingTabIdx: -1,
    tabCount: 0,
    pdfStr:['', '', ''],
    attUrls:['', '', ''],
    isSigned: [false, false, false],
    agentSignFields:[],
    clientSignFields:[],
    textFields:[],
    signDocAuths:['', '', ''],
    signDocDocts:['', '', ''],
    signDocIds:['', '', ''],
    signDocPostUrl:'',
    signDocResultUrl:'',
    signDocDmsId:'',
    updateStepper:false,
    errorMsg:'',
    isMandDocsAllUploaded: false
  }
}

//Function: Return the next tab index based on current state
//-1 = all signed
const getNextSignatureTab = (state = getInitState()) => {
  let tabTotals = state.tabCount;
  for (let i = 1; i < tabTotals; i ++) {
    let nextTab = (state.signingTabIdx + i) % tabTotals;
    if (!state.isSigned[nextTab]) {
      return nextTab;
    }
  }
  return -1;
}

//Function: Return signing tab index based on CB Application
const getInitSignatureTab = (action) => {
  let nextSigningTab = 0;
  for (let i = 0; i < action.attachments.length - 1; i ++) {
    if (action.attachments[i].isSigned) {
      nextSigningTab ++;
    }
  }
  //handle last tab
  if (nextSigningTab === action.tabCount - 1 && action.attachments[nextSigningTab].isSigned) {
    nextSigningTab = -1;
  }
  return nextSigningTab;
}

//Function: Decompose attachments array to different array based on the key
const getInitAttachmentState = (action, key) => {
  let array = [];
  const defaultValue = {
    auth:"",
    docts:"",
    docid: "",
    pdfStr: "",
    attUrl: "",
    isSigned: false
  };

  for (let i = 0; i < action.attachments.length; i ++) {
    if (action.attachments[i].hasOwnProperty(key)) {
      array.push(action.attachments[i][key]);
    } else if (defaultValue.hasOwnProperty(key)) {
      array.push(defaultValue[key]);
    } else {
      array.push(undefined);
    }
  }
  return array;
}

//Function: replace item in an array using index
const updateItem = (array, index, value) => {
  return array.map((item, i) => {
    if(i !== index) {
      return item;
    }

    return value;
  });
}

const updateAttUrls = (array, values) => {
  let newArray = array.slice();
  for (let value of values) {
    newArray = updateItem(newArray, value.index, value.attUrl);
  }
  return newArray;
}

//Reducer
function signature(state = getInitState(), action) {
  switch(action.type) {
    case INIT_SIGNATURE_PAGE:
      return Object.assign({}, state, {
        isFaChannel: action.isFaChannel,
        appStep: action.appStep,
        signingTabIdx: getInitSignatureTab(action),
        tabCount: action.tabCount,
        isSigned: getInitAttachmentState(action, "isSigned"),
        pdfStr: getInitAttachmentState(action, "pdfStr"),
        attUrls: getInitAttachmentState(action, "attUrl"),
        agentSignFields: action.agentSignFields,
        clientSignFields: action.clientSignFields,
        textFields: action.textFields,
        signDocAuths: getInitAttachmentState(action, "auth"),
        signDocDocts: getInitAttachmentState(action, "docts"),
        signDocIds: getInitAttachmentState(action, "docid"),
        signDocPostUrl: action.signDoc.postUrl,
        signDocResultUrl: action.signDoc.resultUrl,
        signDocDmsId: action.signDoc.dmsId,
        isMandDocsAllUploaded: action.isMandDocsAllUploaded
      });
    case UPDATE_APPFORM_BACKDATE:
      return Object.assign({}, state, {
        pdfStr: getInitAttachmentState(action, "pdfStr")
      });
    case SIGN_ERROR:
      return Object.assign({}, state, {
        errorMsg: action.msg
      });
    case CLEAR_ERROR:
      return Object.assign({}, state, {
        errorMsg: ""
      });
    case SIGN_PDF_BY_ATTACHMENT_ID:
      const nextTab = getNextSignatureTab(state);
      const newPdfStr = updateItem(state.pdfStr, action.tabIdx, "");
      const newIsSigned = updateItem(state.isSigned, action.tabIdx, true);
      const newAttUrls = updateAttUrls(state.attUrls, action.newAttUrls);
      return Object.assign({}, state, {
        signingTabIdx: nextTab,
        isSigned: newIsSigned,
        pdfStr: newPdfStr,
        attUrls: newAttUrls,
        errorMsg: ""
      });
    case COMPLETE_SIGN:
      return Object.assign({}, state, {
        updateStepper: true, 
        appStep: action.appStep
      });
    case CLOSE_SUPPORT_DOCUMENTS:
      return Object.assign({}, state, {
        isMandDocsAllUploaded: action.isMandDocsAllUploaded
      });
    case CLOSE_SIGNATURE_PAGE:
    case LOGOUT:
      return getInitState();
    default:
      return state;
  }
}

export default signature;