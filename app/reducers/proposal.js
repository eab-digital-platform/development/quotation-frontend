import {
  OPEN_PROPOSAL,
  SHOW_PROPOSAL,
  CLOSE_PROPOSAL,
  SHOW_ILLUSTRATION,
  OPEN_EMAIL,
  CLOSE_EMAIL,
  CLOSE_PROPOSAL_ERROR,
  CLOSE_PROPOSAL_CONFIRM
} from '../actions/proposal';
import { REQUOTE, SHOW_PROPOSAL_ERROR, PROPOSAL_CONFIRM } from '../actions/quotation';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = () => {
  return {
    pageId: null,
    quotation: null,
    pdfData: null,
    illustrateData: null,
    planDetails: null,
    canRequote: false,
    canRequoteInvalid: false,
    canClone: false,
    canEmail: false,
    isCreateProposal: false,
    emailOpen: false,
    emails: null,
    errorMsg: null,
    confirmMsg: null,
    onConfirm: null
  };
};

export default function proposal(state = getInitState(), action) {
  switch (action.type) {
    case OPEN_PROPOSAL:
      return Object.assign({}, state, {
        pageId: 'proposal',
        quotation: action.quotation,
        pdfData: action.proposal,
        illustrateData: action.illustrateData,
        planDetails: action.planDetails,
        canRequote: action.canRequote,
        canRequoteInvalid: action.canRequoteInvalid,
        canClone: action.canClone,
        canEmail: action.canEmail,
        isCreateProposal: action.isCreateProposal
      });
    case SHOW_PROPOSAL:
      return Object.assign({}, state, {
        pageId: 'proposal'
      });
    case SHOW_ILLUSTRATION:
      return Object.assign({}, state, {
        pageId: 'illustration'
      });
    case OPEN_EMAIL:
      return Object.assign({}, state, {
        emailOpen: true,
        emails: action.emails
      });
    case CLOSE_EMAIL:
      return Object.assign({}, state, {
        emailOpen: false,
        emails: null
      });
    case SHOW_PROPOSAL_ERROR:
      return Object.assign({}, state, {
        errorMsg: action.errorMsg
      });
    case CLOSE_PROPOSAL_ERROR:
      return Object.assign({}, state, {
        errorMsg: null
      });
    case PROPOSAL_CONFIRM:
      return Object.assign({}, state, {
        confirmMsg: action.confirmMsg,
        onConfirm: action.onConfirm
      });
    case CLOSE_PROPOSAL_CONFIRM:
      return Object.assign({}, state, {
        confirmMsg: null,
        onConfirm: null
      });
    case REQUOTE:
    case CLOSE_PROPOSAL:
    case LOGOUT:
      return getInitState();  // reset the state
    default:
      return state;
  }
}
