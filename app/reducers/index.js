import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import app from './app';
import home from './home';
import sessions from './sessions';
import client from './client';
import needs from './needs';
import appList from './applications';
import products from './products';
import quotation from './quotation';
import proposal from './proposal';
import brochure from './brochure';
import quickQuote from './quickQuote';
import appForm from './appForm';
import shieldApplication from './shieldApplication';
import clientChoice from './clientChoice';
import signature from './signature';
import payment from './payment';
import submission from './submission';
import attachments from './attachments';
import dyn from './dyn';
import pos from './pos';
import supportDocuments from './supportDocuments';
import approval from './approval';
import fileUpload from './fileUpload';
import downloadMaterial from './downloadMaterial';

const rootReducer = combineReducers({
  app,
  home,
  sessions,
  client,
  routing,
  needs,
  appList,
  products,
  quotation,
  appForm,
  clientChoice,
  proposal,
  brochure,
  quickQuote,
  shieldApplication,
  signature,
  payment,
  submission,
  attachments,
  dyn,
  supportDocuments,
  pos,
  approval,
  fileUpload,
  downloadMaterial
});

export default rootReducer;
