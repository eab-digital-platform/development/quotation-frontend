import { INIT_SUBMISSION_PAGE, SERVER_ERROR, CLOSE_SUBMISSION_PAGE, SUBMISSION_WARNING } from '../actions/submission';
import { CLOSE_APPLICATION_WARNING } from '../actions/application';
import { CLOSE_SUPPORT_DOCUMENTS } from '../actions/supportDocuments'
import ConfigConstants from '../constants/ConfigConstants';
import { LOGOUT } from '../actions/GlobalActions';

const getInitState = () => {
  return {
    template:{},
    values:{},
    isMandDocsAllUploaded: false,
    isSubmitted: false,
    submissionWarning: {}
  }
}

export default function submission(state = getInitState(), action) {
  switch(action.type) {
    case INIT_SUBMISSION_PAGE:
      return Object.assign({}, state, {
        template: action.template,
        values: action.values,
        isMandDocsAllUploaded: action.isMandDocsAllUploaded,
        isSubmitted: action.isSubmitted,
        submissionWarning: {}
      });
    case CLOSE_SUPPORT_DOCUMENTS:
      return Object.assign({}, state, {
        template: action.template,
        values: action.values,
        isMandDocsAllUploaded: action.isMandDocsAllUploaded,
        isSubmitted: action.isSubmitted,
        submissionWarning: {}
      });
    case SERVER_ERROR:
      return Object.assign({}, state, {
        values: action.values,
        isMandDocsAllUploaded: action.isMandDocsAllUploaded,
        isSubmitted: action.isSubmitted,
        submissionWarning: {}
      });
    case CLOSE_SUBMISSION_PAGE:
    case LOGOUT:
      return getInitState();
    case SUBMISSION_WARNING:
      return Object.assign({}, state, {
        submissionWarning: action.warning
      });
    case CLOSE_APPLICATION_WARNING:
      return Object.assign({}, state, {
        submissionWarning: {}
      });
    default:
      return state;
  }
}


