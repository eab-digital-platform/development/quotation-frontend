// import { NEW_QUOTATION, SHOW_PROPOSAL } from '../actions/quotation';
// import { OPEN_APPLICATION} from '../actions/client';
import { SHOW_APPLICATION, /*SUBMIT_APPLICATION,*/ UPDATE_APPLICATION_LIST, DELETE_APPLICATION_LIST, GET_DIALOG_LIST} from '../actions/application';
import { LOGOUT } from '../actions/GlobalActions';
import { INITIAL_APP} from '../actions/home';
import { SHOW_APP_FORM, UPDATE_POLICY_NUMBER, DISMISS_EAPP_CROSSAGE_PROMPT, POS_CHANGE_PAGE} from '../actions/application';
import { INIT_SIGNATURE_PAGE, SIGN_PDF_BY_ATTACHMENT_ID, UPDATE_APPFORM_BACKDATE} from '../actions/signature';
import { UPDATE_PAYMENT } from '../actions/payment';
import { CLOSE_SUPPORT_DOCUMENTS } from '../actions/supportDocuments';
import * as _ from 'lodash';

var getInitState = function() {
    return {
        open: false,
        listTabId: 0,
        application: null,
        applicationsList: [],
        targetAppId: null,
        openUpload: false,
        openSingedView: false,
        isMandDocsAllUploaded: false,
        docs:null,
        attachments:{},
        crossAgeObj:{
            showCrossedAgeSignedMsg: false
        }
    };
};

export default function application(state = getInitState(), action) {
    switch (action.type) {
        case UPDATE_POLICY_NUMBER:
          return Object.assign({}, state, {
                application: action.application
            });
        case SIGN_PDF_BY_ATTACHMENT_ID:
          return Object.assign({}, state, {
            template: action.appFormTemplate,
            application: action.application
          });
        case UPDATE_PAYMENT:
          return Object.assign({}, state, {
            application: action.application
          });
        case SHOW_APP_FORM:
            return Object.assign({}, state, {
                open: true
            });
        case UPDATE_APPLICATION_LIST:
            return Object.assign({}, state, {
                applicationsList: action.applicationsList
            });
        case DELETE_APPLICATION_LIST:
            return Object.assign({}, state, {
                applicationsList: action.applicationsList
            });
        case GET_DIALOG_LIST:
            return Object.assign({}, state, {
                applicationsList: action.applicationsList
            });
        case UPDATE_APPFORM_BACKDATE:
            return Object.assign({}, state, {
                isBackDate: action.isBackDate || 'N',
                changedValues: action.changedValues || {}
            });
        case POS_CHANGE_PAGE:
            return Object.assign({}, state, {
                isBackDate: 'N',
                changedValues: {}
            });
        case SHOW_APPLICATION:
            return Object.assign({}, state, {
                application: action.application,
                template: action.template,
                open:true,
                showSignExpiryWarning: action.showSignExpiryWarning,
                isMandDocsAllUploaded: action.application.isMandDocsAllUploaded,
                crossAgeObj: action.crossAgeObj || state.crossAgeObj
            });
        case DISMISS_EAPP_CROSSAGE_PROMPT:
            return Object.assign({}, state, {
                crossAgeObj : {
                    showCrossedAgeSignedMsg: false
                }
            });
        case CLOSE_SUPPORT_DOCUMENTS:
            return Object.assign({}, state, {
                isMandDocsAllUploaded: action.isMandDocsAllUploaded
            });
        case INIT_SIGNATURE_PAGE:
          let application = _.cloneDeep(state.application);
          if (!window.isEmpty(application)) {
            application.appStep = action.appStep;
          }
          return Object.assign({}, state, {
            application: application
          });
        case INITIAL_APP:
        case LOGOUT:
            return getInitState();  // reset the state
        default:
            return state;
    }
}
