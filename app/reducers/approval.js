import {
    RESET_APPROVAL_FILTER,
    RESET_WORKBENCH_FILTER,
    SET_APPROVAL_TEMPLATE,
    SEARCH_APPROVAL_CASES,
    SET_CASE_TEMPLATE,
    INIT_APPROVALPAGE_TEMPLATE,
    UPDATE_CASE_BY_ID,
    UPDATE_SUP_DOC,
    OPEN_APPROVAL_PAGE,
    SET_WORKBENCH_TEMPLATE,
    SEARCH_WORKBENCH_CASES,
    OPEN_APPROVAL_EMAIL,
    CLOSE_APPROVAL_EMAIL,
    RESET_CLOSE_APPROVAL,
    RESET_CLOSE_WORKBENCH,
    CLOSE_APPROVAL_EAPP_MISSING_DIALOG
} from '../actions/approval';
import { LOGOUT } from '../actions/GlobalActions';
import _isEmpty from 'lodash/fp/isEmpty';

var getInitState = function() {
    return {
        approvalFilter:{}
    }
}

export default function approval(state = getInitState(), action) {
    switch (action.type) {
        case RESET_APPROVAL_FILTER:
            if (_isEmpty(action.approvalFilter) ){
                return Object.assign({}, state, {
                    approvalFilter: action.approvalFilter,
                    searchedApprovalCases: []
                });
            } else {
                return Object.assign({}, state, {
                    approvalFilter: action.approvalFilter
                });
            }
        case RESET_WORKBENCH_FILTER:
            if (_isEmpty(action.workbenchFilter) ){
                return Object.assign({}, state, {
                    workbenchFilter: action.workbenchFilter,
                    searchedWorkbenchCases: []
                });
            } else {
                return Object.assign({}, state, {
                    workbenchFilter: action.workbenchFilter
                });
            }
        case RESET_CLOSE_APPROVAL:
            if (_isEmpty(action.approvalFilter) ){
                return Object.assign({}, state, {
                    approvalFilter: action.approvalFilter,
                    searchedApprovalCases: [],
                    openApprovalPage: action.openApproval,
                    currentTemplate: 'W'
                });
            } else {
                return Object.assign({}, state, {
                    approvalFilter: action.approvalFilter,
                    openApprovalPage: action.openApproval,
                    currentTemplate: 'W'
                });
            }
        case RESET_CLOSE_WORKBENCH:
            if (_isEmpty(action.workbenchFilter) ){
                return Object.assign({}, state, {
                    workbenchFilter: action.workbenchFilter,
                    searchedWorkbenchCases: [],
                    openApprovalPage: action.openApproval
                });
            } else {
                return Object.assign({}, state, {
                    workbenchFilter: action.workbenchFilter,
                    openApprovalPage: action.openApproval
                });
            }
        case SET_APPROVAL_TEMPLATE:
            return Object.assign({}, state, {
                approvalTemplate: action.approvalTemplate,
                currentTemplate: action.currentTemplate
            });
        case SET_WORKBENCH_TEMPLATE:
            return Object.assign({}, state, {
                workbenchTemplate: action.workbenchTemplate,
                currentTemplate: action.currentTemplate
            });
        case SEARCH_APPROVAL_CASES:
            return Object.assign({}, state, {
                searchedApprovalCases: action.searchedApprovalCases,
                approvalFilter: action.approvalFilter,
                currentTemplate: action.currentTemplate
            });
        case SEARCH_WORKBENCH_CASES:
            return Object.assign({}, state, {
                searchedWorkbenchCases: action.searchedWorkbenchCases,
                workbenchFilter: action.workbenchFilter,
                currentTemplate: action.currentTemplate
            });
        case SET_CASE_TEMPLATE:
            return Object.assign({}, state, {
                caseTemplate: action.caseTemplate
            });
        case INIT_APPROVALPAGE_TEMPLATE:
            return Object.assign({}, state, {
                reviewPageTemplate: action.reviewPageTemplate,
                approvePageTemplate: action.approvePageTemplate,
                rejectPageTemplate: action.rejectPageTemplate,
                reviewPage_selectedClient_tmpl: action.reviewPage_selectedClient_tmpl,
                reviewPage_jfw_tmpl: action.reviewPage_jfw_tmpl,
                approvalTemplate: action.approvalTemplate,
                workbenchTemplate: action.workbenchTemplate,
                caseTemplate: action.caseTemplate,
                searchedApprovalCases: action.searchedApprovalCases
            });
        case UPDATE_CASE_BY_ID:
            return Object.assign({}, state, {
                approvalCase: action.approvalCase,
                approvalCaseHasIndividual: action.approvalCaseHasIndividual,
                approvalCaseHasIsCrossBorder: action.approvalCaseHasIsCrossBorder,
                canApprove: action.canApprove
            });
        case UPDATE_SUP_DOC:
            return Object.assign({}, state, {
                supCompleteFlag: action.supCompleteFlag,
                mandataryFlag: action.mandataryFlag,
                reviewDoneFlag: action.reviewDoneFlag,
                healthDeclarationFlag: action.healthDeclarationFlag,
                showMissingEappPrompt: action.showMissingEappPrompt
            });
        case OPEN_APPROVAL_PAGE:
            return Object.assign({}, state, {
                openApprovalPage: action.openApproval
            });
        case OPEN_APPROVAL_EMAIL:
            return Object.assign({}, state, {
                openEmailDialog: true,
                emailObj: action.emailObj
            });
        case CLOSE_APPROVAL_EMAIL:
            return Object.assign({}, state, {
                openEmailDialog: false,
                emailObj: null
            });
        case LOGOUT:
            return getInitState();  // reset the state
        default:
            return state;
    }
}
