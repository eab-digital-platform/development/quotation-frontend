// import { NEW_QUOTATION, SHOW_PROPOSAL } from '../actions/quotation';
// import { OPEN_APPLICATION} from '../actions/client';
import { /*SUBMIT_APPLICATION,*/ UPDATE_APPLICATION_LIST, INCOMPLETE_CLIENT_CHOICE, GET_SUPPORT_DOCUMENTS,
  UPDATE_QUOT_CHECKED_LIST, INIT_APPLICAITON_SUMMARY, POS_CHANGE_PAGE, OPEN_APPSUM_EMAIL_PAGEONE,
  CLOSE_APPSUM_EMAIL_PAGEONE, OPEN_APPSUM_EMAIL_PAGETWO, CLOSE_APPSUM_EMAIL_PAGETWO,
  DELETE_APPLICATION_LIST, FNA_NOT_COMPLETED, CLOSE_APPLICATION_WARNING} from '../actions/application';
import { CLOSE_SUPPORT_DOCUMENTS} from '../actions/supportDocuments';
import { LOGOUT } from '../actions/GlobalActions';
import { INITIAL_APP} from '../actions/home';
import {NEW_QUOTATION, CLOSE_QUOTATION, NEW_QUOTATION_ERROR, CONFIRM_CREATE_QUOTE} from '../actions/quotation';
import { APPLICATION_SUMMARY_WARNING, PREPARE_APPLICATION_SUMMARY_WARNING } from '../actions/submission';
import {QUOT_CLOSE_CONFIRM, QUOT_CLOSE_ERROR} from '../actions/products';
import { CLOSE_PROPOSAL } from '../actions/proposal';
import { UPDATE_CLIENT_CHOICE } from '../actions/clientChoice';
import { SAVE_PROFILE } from '../actions/client';

var getInitState = function() {
    var form =
    {
      "items":{
        "id": "applyTmpl",
        "type": "layout",
        "items":[
          {
            "title": {
                "en": "Personal"
            },
            "type": "section",
            "items": [
              {
                "type":"menu",
                "detailSeq": 1,
                "items": [
                  {
                    "type": "menuItemGroup",
                    "detailSeq": 1,
                    "title": {
                      "en": "Personal Details"
                    },
                    "items": [
                      {
                        "id": "personalDetails",
                        "title": "Personal Details",
                        "type": "menuItem",
                        "detailSeq": 1,
                        "items": [
                          {
                            "type": "tabs",
                            "items":[
                              {
                                "title": "Proposer",
                                "type": "tab",
                                "items": [
                                  {
                                    "id": "sectionPP",
                                    "title": "Personal Particulars",
                                    "type": "BLOCK",
                                    "detailSeq": 1,
                                    "items": [
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 1,
                                        "items": [
                                          {
                                            "type": "TEXT",
                                            "id": "pSurName",
                                            "title": "Surname (as shown in ID)",
                                            "mandatory": true,
                                            "detailSeq": 1
                                          },
                                          {
                                            "type": "TEXT",
                                            "id": "pGivenName",
                                            "title": "Given Name (as shown in ID)",
                                            "mandatory": true,
                                            "detailSeq": 2
                                          }
                                        ]
                                      },
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 2,
                                        "items": [
                                          {
                                            "type": "TEXT",
                                            "id": "otherName",
                                            "title": "English or other name",
                                            "mandatory": true,
                                            "detailSeq": 1
                                          },
                                          {
                                            "type": "TEXT",
                                            "id": "pinyinName",
                                            "title": "Han Yu Pin Yin Name",
                                            "mandatory": true,
                                            "detailSeq": 2
                                          }
                                        ]
                                      },
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 3,
                                        "items": [
                                          {
                                            "type": "TEXT",
                                            "id": "nation",
                                            "title": "Nationality",
                                            "mandatory": true,
                                            "detailSeq": 1
                                          },
                                          {
                                            "type": "TEXT",
                                            "id": "prStatus",
                                            "title": "Singapore PR Status",
                                            "mandatory": true,
                                            "detailSeq": 2
                                          }
                                        ]
                                      },
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 4,
                                        "items": [
                                          {
                                            "type": "PICKER",
                                            "id": "idType",
                                            "title": "ID Document Type",
                                            "mandatory": true,
                                            "detailSeq": 1,
                                            "value": "nric",
                                            "options": [
                                              {
                                                "value": "passport",
                                                "title": "passport"
                                              },
                                              {
                                                "value": "nric",
                                                "title": "NRIC"
                                              },
                                              {
                                                "value": "fin",
                                                "title": "FIN No"
                                              },
                                              {
                                                "value": "other",
                                                "title": "Other"
                                              }
                                            ]
                                          },
                                          {
                                            "type": "TEXT",
                                            "id": "docID",
                                            "title": "ID Number",
                                            "mandatory": true,
                                            "detailSeq": 2
                                          }
                                        ]
                                      },
                                      {
                                        "type": "TEXT",
                                        "id": "otherIdType",
                                        "title": "Other ID Type Name",
                                        "detailSeq": 5,
                                        "trigger": {
                                          "id": "idType",
                                          "type": "showIfEqual",
                                          "value": "other"
                                        }
                                      },
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 6,
                                        "items": [
                                          {
                                            "type": "DATEPICKER",
                                            "id": "dob",
                                            "title": "Date of Birth(DD/MM/YYYY)",
                                            "mandatory": true,
                                            "detailSeq": 1
                                          },
                                          {
                                            "type": "PICKER",
                                            "id": "gender",
                                            "title": "Gender",
                                            "mandatory": true,
                                            "detailSeq": 2,
                                            "options": [
                                              {
                                                "value": "F",
                                                "title": "Female"
                                              },
                                              {
                                                "value": "M",
                                                "title": "Male"
                                              }
                                            ]
                                          }
                                        ]
                                      },
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 7,
                                        "items": [
                                          {
                                            "type": "PICKER",
                                            "id": "marital",
                                            "title": "Marital Status",
                                            "mandatory": true,
                                            "detailSeq": 1
                                          },
                                          {
                                            "type": "TEXT",
                                            "id": "mobile",
                                            "title": "Mobile",
                                            "mandatory": false,
                                            "detailSeq": 2
                                          }
                                        ]
                                      },
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 7,
                                        "items": [
                                          {
                                            "type": "TEXT",
                                            "id": "homeNo",
                                            "title": "Home Number",
                                            "mandatory": true,
                                            "detailSeq": 1
                                          },
                                          {
                                            "type": "TEXT",
                                            "subType": "email",
                                            "id": "email",
                                            "title": "Email",
                                            "detailSeq": 2
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "id": "sectionRI",
                                    "title": "Professional Details",
                                    "type": "BLOCK",
                                    "detailSeq": 2,
                                    "items": [
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 1,
                                        "items": [
                                          {
                                            "type": "TEXT",
                                            "id": "workPlace",
                                            "title": "Employer/Business/School",
                                            "mandatory": true,
                                            "detailSeq": 1
                                          },
                                          {
                                            "type": "TEXT",
                                            "id": "occupation",
                                            "title": "Occupation",
                                            "mandatory": true,
                                            "detailSeq": 2
                                          }
                                        ]
                                      },
                                      {
                                        "type": "HBOX",
                                        "detailSeq": 2,
                                        "items": [
                                          {
                                            "type": "TEXT",
                                            "id": "mIncome",
                                            "title": "Monthly Income",
                                            "mandatory": true,
                                            "detailSeq": 1
                                          },
                                          {
                                            "type": "PICKER",
                                            "id": "passType",
                                            "title": "Type of Pass",
                                            "mandatory": true,
                                            "detailSeq": 2,
                                            "options": [
                                              {
                                                "value": "employ",
                                                "title": "Employment Pass"
                                              },
                                              {
                                                "value": "workPermit",
                                                "title": "Work Permit"
                                              },
                                              {
                                                "value": "dependent",
                                                "title": "Dependent Pass"
                                              },
                                              {
                                                "value": "student",
                                                "title": "Student"
                                              },
                                              {
                                                "value": "ltVisit",
                                                "title": "Long Term Visit Passt"
                                              },
                                              {
                                                "value": "sVisit",
                                                "title": "Social Visit Pass"
                                              },
                                              {
                                                "value": "other",
                                                "title": "Others"
                                              }
                                            ]
                                          }
                                        ]
                                      },
                                      {
                                        "type": "TEXT",
                                        "id": "otherPassType",
                                        "title": "Type of pass (Other)",
                                        "detailSeq": 1,
                                        "trigger": {
                                          "id": "passType",
                                          "type": "showIfEqual",
                                          "value": "other"
                                        }
                                      },
                                      {
                                        "type": "DATEPICKER",
                                        "id": "passExpDate",
                                        "title": "Pass Expiry Date (DD/MM/YYYY)",
                                        "mandatory": true,
                                        "detailSeq": 1
                                      }
                                    ]
                                  },
                                  {
                                    "id": "sectionAdds",
                                    "title": "Address",
                                    "type": "BLOCK",
                                    "detailSeq": 3,
                                    "items": [
                                      {
                                        "type": "TEXT",
                                        "id": "resiAddress",
                                        "title": "Residential Address",
                                        "mandatory": true,
                                        "detailSeq": 1
                                      },
                                      {
                                        "type": "TEXT",
                                        "id": "workAdress",
                                        "title": "Employer/Organiaztion/Scholl Address",
                                        "mandatory": true,
                                        "detailSeq": 2
                                      },
                                      {
                                        "type": "TEXT",
                                        "id": "mailAdress",
                                        "title": "Mailing Address",
                                        "mandatory": true,
                                        "detailSeq": 3
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "id": "insured",
                                "title": "Insurer",
                                  "items": [
                                    {
                                      "id": "sectionPP",
                                      "title": "Personal Particulars",
                                      "type": "BLOCK",
                                      "detailSeq": 1,
                                      "items": [
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 1,
                                          "items": [
                                            {
                                              "type": "TEXT",
                                              "id": "pSurName",
                                              "title": "Surname (as shown in ID)",
                                              "mandatory": true,
                                              "detailSeq": 1
                                            },
                                            {
                                              "type": "TEXT",
                                              "id": "pGivenName",
                                              "title": "Given Name (as shown in ID)",
                                              "mandatory": true,
                                              "detailSeq": 2
                                            }
                                          ]
                                        },
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 2,
                                          "items": [
                                            {
                                              "type": "TEXT",
                                              "id": "otherName",
                                              "title": "English or other name",
                                              "mandatory": true,
                                              "detailSeq": 1
                                            },
                                            {
                                              "type": "TEXT",
                                              "id": "pinyinName",
                                              "title": "Han Yu Pin Yin Name",
                                              "mandatory": true,
                                              "detailSeq": 2
                                            }
                                          ]
                                        },
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 3,
                                          "items": [
                                            {
                                              "type": "TEXT",
                                              "id": "nation",
                                              "title": "Nationality",
                                              "mandatory": true,
                                              "detailSeq": 1
                                            },
                                            {
                                              "type": "TEXT",
                                              "id": "prStatus",
                                              "title": "Singapore PR Status",
                                              "mandatory": true,
                                              "detailSeq": 2
                                            }
                                          ]
                                        },
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 4,
                                          "items": [
                                            {
                                              "type": "PICKER",
                                              "id": "idType",
                                              "title": "ID Document Type",
                                              "mandatory": true,
                                              "detailSeq": 1,
                                              "value": "nric",
                                              "options": [
                                                {
                                                  "value": "passport",
                                                  "title": "passport"
                                                },
                                                {
                                                  "value": "nric",
                                                  "title": "NRIC"
                                                },
                                                {
                                                  "value": "fin",
                                                  "title": "FIN No"
                                                },
                                                {
                                                  "value": "other",
                                                  "title": "Other"
                                                }
                                              ]
                                            },
                                            {
                                              "type": "TEXT",
                                              "id": "docID",
                                              "title": "ID Number",
                                              "mandatory": true,
                                              "detailSeq": 2
                                            }
                                          ]
                                        },
                                        {
                                          "type": "TEXT",
                                          "id": "otherIdType",
                                          "title": "Other ID Type Name",
                                          "detailSeq": 5,
                                          "trigger": {
                                            "id": "idType",
                                            "type": "showIfEqual",
                                            "value": "other"
                                          }
                                        },
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 6,
                                          "items": [
                                            {
                                              "type": "DATEPICKER",
                                              "id": "dob",
                                              "title": "Date of Birth(DD/MM/YYYY)",
                                              "mandatory": true,
                                              "detailSeq": 1
                                            },
                                            {
                                              "type": "PICKER",
                                              "id": "gender",
                                              "title": "Gender",
                                              "mandatory": true,
                                              "detailSeq": 2,
                                              "options": [
                                                {
                                                  "value": "F",
                                                  "title": "Female"
                                                },
                                                {
                                                  "value": "M",
                                                  "title": "Male"
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 7,
                                          "items": [
                                            {
                                              "type": "PICKER",
                                              "id": "marital",
                                              "title": "Marital Status",
                                              "mandatory": true,
                                              "detailSeq": 1
                                            },
                                            {
                                              "type": "TEXT",
                                              "id": "mobile",
                                              "title": "Mobile",
                                              "mandatory": false,
                                              "detailSeq": 2
                                            }
                                          ]
                                        },
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 7,
                                          "items": [
                                            {
                                              "type": "TEXT",
                                              "id": "homeNo",
                                              "title": "Home Number",
                                              "mandatory": true,
                                              "detailSeq": 1
                                            },
                                            {
                                              "type": "TEXT",
                                              "subType": "email",
                                              "id": "email",
                                              "title": "Email",
                                              "detailSeq": 2
                                            }
                                          ]
                                        }
                                      ]
                                    },
                                    {
                                      "id": "sectionRI",
                                      "title": "Professional Details",
                                      "type": "BLOCK",
                                      "detailSeq": 2,
                                      "items": [
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 1,
                                          "items": [
                                            {
                                              "type": "TEXT",
                                              "id": "workPlace",
                                              "title": "Employer/Business/School",
                                              "mandatory": true,
                                              "detailSeq": 1
                                            },
                                            {
                                              "type": "TEXT",
                                              "id": "occupation",
                                              "title": "Occupation",
                                              "mandatory": true,
                                              "detailSeq": 2
                                            }
                                          ]
                                        },
                                        {
                                          "type": "HBOX",
                                          "detailSeq": 2,
                                          "items": [
                                            {
                                              "type": "TEXT",
                                              "id": "mIncome",
                                              "title": "Monthly Income",
                                              "mandatory": true,
                                              "detailSeq": 1
                                            },
                                            {
                                              "type": "PICKER",
                                              "id": "passType",
                                              "title": "Type of Pass",
                                              "mandatory": true,
                                              "detailSeq": 2,
                                              "options": [
                                                {
                                                  "value": "employ",
                                                  "title": "Employment Pass"
                                                },
                                                {
                                                  "value": "workPermit",
                                                  "title": "Work Permit"
                                                },
                                                {
                                                  "value": "dependent",
                                                  "title": "Dependent Pass"
                                                },
                                                {
                                                  "value": "student",
                                                  "title": "Student"
                                                },
                                                {
                                                  "value": "ltVisit",
                                                  "title": "Long Term Visit Passt"
                                                },
                                                {
                                                  "value": "sVisit",
                                                  "title": "Social Visit Pass"
                                                },
                                                {
                                                  "value": "other",
                                                  "title": "Others"
                                                }
                                              ]
                                            }
                                          ]
                                        },
                                        {
                                          "type": "TEXT",
                                          "id": "otherPassType",
                                          "title": "Type of pass (Other)",
                                          "detailSeq": 1,
                                          "trigger": {
                                            "id": "passType",
                                            "type": "showIfEqual",
                                            "value": "other"
                                          }
                                        },
                                        {
                                          "type": "DATEPICKER",
                                          "id": "passExpDate",
                                          "title": "Pass Expiry Date (DD/MM/YYYY)",
                                          "mandatory": true,
                                          "detailSeq": 1
                                        }
                                      ]
                                    },
                                    {
                                      "id": "sectionAdds",
                                      "title": "Address",
                                      "type": "BLOCK",
                                      "detailSeq": 3,
                                      "items": [
                                        {
                                          "type": "TEXT",
                                          "id": "resiAddress",
                                          "title": "Residential Address",
                                          "mandatory": true,
                                          "detailSeq": 1
                                        },
                                        {
                                          "type": "TEXT",
                                          "id": "workAdress",
                                          "title": "Employer/Organiaztion/Scholl Address",
                                          "mandatory": true,
                                          "detailSeq": 2
                                        },
                                        {
                                          "type": "TEXT",
                                          "id": "mailAdress",
                                          "title": "Mailing Address",
                                          "mandatory": true,
                                          "detailSeq": 3
                                        }
                                      ]
                                    }
                                  ]
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "id": "residenceInfo",
                        "title": "Residency & Insurance Info",
                        "detailSeq": 2,
                        "frames": [
                          {
                            "title": "Proposer",
                            "template": {
                              "items": [
                                {
                                  "title": "Residency Questions",
                                  "type": "BLOCK",
                                  "detailSeq": 1,
                                  "items": [
                                    {
                                      "type": "QRADIOGROUP",
                                      "id": "resided",
                                      "title": "1. Have you resided in Singapore for 183 days or more in the last 12 months preceding the date of proposal?",
                                      "options": [
                                        {
                                          "value": "N",
                                          "title": "No"
                                        },
                                        {
                                          "value": "Y",
                                          "title": "Yes"
                                        }
                                      ]
                                    },
                                    {
                                      "type": "QRADIOGROUP",
                                      "id": "permanentReside",
                                      "title": "2. Do you intend to reside in Singapore on a permanent basis?",
                                      "options": [
                                        {
                                          "value": "N",
                                          "title": "No"
                                        },
                                        {
                                          "value": "Y",
                                          "title": "Yes"
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "title": "Foreigner Questions",
                                  "type": "BLOCK",
                                  "detailSeq": 2,
                                  "items": [
                                    {
                                      "type": "QTITLE",
                                      "id": "curPermResD",
                                      "title": "1. Where is your current permanent place of residence?"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "curPermRes"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "familyResD",
                                      "title": "2. Where do you immediate family member currently reside?"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "familyRes"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "resCountryD",
                                      "title": "3. In which country(ies) do you have a permanent residency status?"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "resCountry"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "arriveDateD",
                                      "title": "4. When did you first arrive Singapore(excluding holidays of less than 3 months)?"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "arriveDate"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "resDetailsD",
                                      "title": "5. Please provide details of your residence over the last 5 years"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "resDetails"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "futurePlansD",
                                      "title": "6. Please provide details of your next destination o any future residency or travel plans in the next 2 years."
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "futurePlans"
                                    }
                                  ]
                                }
                              ]
                            }
                          },
                          {
                            "id": "insured",
                            "title": "Insurer",
                            "template": {
                              "items": [
                                {
                                  "title": "Residency Questions",
                                  "type": "BLOCK",
                                  "detailSeq": 1,
                                  "items": [
                                    {
                                      "type": "QRADIOGROUP",
                                      "id": "resided",
                                      "title": "1. Have you resided in Singapore for 183 days or more in the last 12 months preceding the date of proposal?",
                                      "options": [
                                        {
                                          "value": "N",
                                          "title": "No"
                                        },
                                        {
                                          "value": "Y",
                                          "title": "Yes"
                                        }
                                      ]
                                    },
                                    {
                                      "type": "QRADIOGROUP",
                                      "id": "permanentReside",
                                      "title": "2. Do you intend to reside in Singapore on a permanent basis?",
                                      "options": [
                                        {
                                          "value": "N",
                                          "title": "No"
                                        },
                                        {
                                          "value": "Y",
                                          "title": "Yes"
                                        }
                                      ]
                                    }
                                  ]
                                },
                                {
                                  "title": "Foreigner Questions",
                                  "type": "BLOCK",
                                  "detailSeq": 2,
                                  "items": [
                                    {
                                      "type": "QTITLE",
                                      "id": "curPermResD",
                                      "title": "1. Where is your current permanent place of residence?"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "curPermRes"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "familyResD",
                                      "title": "2. Where do you immediate family member currently reside?"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "familyRes"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "resCountryD",
                                      "title": "3. In which country(ies) do you have a permanent residency status?"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "resCountry"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "arriveDateD",
                                      "title": "4. When did you first arrive Singapore(excluding holidays of less than 3 months)?"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "arriveDate"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "resDetailsD",
                                      "title": "5. Please provide details of your residence over the last 5 years"
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "resDetails"
                                    },
                                    {
                                      "type": "QTITLE",
                                      "id": "futurePlansD",
                                      "title": "6. Please provide details of your next destination o any future residency or travel plans in the next 2 years."
                                    },
                                    {
                                      "type": "TEXT",
                                      "id": "futurePlans"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]

              }
            ]
          }
        ]
      }
    }

    return {
        open: false,
        listTabId: 0,
        applicationsList: [],
        targetAppId: null,
        openUpload: false,
        openSingedView: false,
        docs:null,
        attachments:{},
        form:form,
        clientChoiceFinished: false,
        supportDocuments:[],
        signingStatusQuotationIds:[],
        bundleId: null,
        invalid: {
          fna: false,
          initApplications: false
        },
        applicationSummaryWarning: {},
        warningAlert: false
    }
}

export default function application(state = getInitState(), action) {
    switch (action.type) {
        case SAVE_PROFILE :
          return Object.assign({}, state, {
            invalid: action.invalid || state.invalid
          })
        case UPDATE_CLIENT_CHOICE:
            if (action.isDone) {
                // return Object.assign({}, state, {
                state.clientChoiceFinished = true;
                state.warningAlert = false;
                // });
                return state;
            } else {
                return state;
            }
        case INCOMPLETE_CLIENT_CHOICE:
            return Object.assign({}, state, {
                clientChoiceFinished: false,
                warningAlert: false
            });
        case UPDATE_QUOT_CHECKED_LIST:
            return Object.assign({}, state, {
                quotCheckedList: action.quotCheckedList,
                warningAlert: false
            });
        case FNA_NOT_COMPLETED:
          return Object.assign({}, state, {
            isFNACompleted: action.isFNACompleted,
            applicationsList: action.appList,
            warningAlert: false
          });
        case UPDATE_APPLICATION_LIST:
            return Object.assign({}, state, {
                applicationsList: action.applicationsList,
                needUpdateAppList: false,
                pdaMembers: action.pdaMembers,
                clientChoiceFinished: action.clientChoiceFinished,
                quotCheckedList: action.quotCheckedList,
                agentChannel: action.agentChannel,
                bundleId: action.bundleId,
                isFNACompleted: action.isFNACompleted,
                invalid: {
                  fna: false,
                  initApplications: false
                },
                warningAlert: false
            });
        case DELETE_APPLICATION_LIST:
            return Object.assign({}, state, {
                applicationsList: action.applicationsList,
                quotCheckedList: action.quotCheckedList,
                clientChoiceFinished: action.clientChoiceFinished,
                warningAlert: false
            });
        case GET_SUPPORT_DOCUMENTS:
            return Object.assign({}, state, {
                supportDocuments: action.supportDocuments,
                warningAlert: false
            });
        case INIT_APPLICAITON_SUMMARY:
            return Object.assign({}, state, {
                applicationsList: [],
                warningAlert: false
            });
        case CLOSE_SUPPORT_DOCUMENTS:
        case POS_CHANGE_PAGE:
            if (action.page != 'index') {
              return state;
            }
        case CLOSE_QUOTATION:
        case CLOSE_PROPOSAL:
          return Object.assign({}, state, {
            needUpdateAppList: true,
            isCreateProposal: action.isCreateProposal,
            warningAlert: false
          });
        case OPEN_APPSUM_EMAIL_PAGEONE:
          return Object.assign({}, state, {
            appSumEmailOpen: true,
            attKeysMap: action.attKeysMap,
            warningAlert: false
          });
        case CLOSE_APPSUM_EMAIL_PAGEONE:
          return Object.assign({}, state, {
            appSumEmailOpen: false,
            warningAlert: false
          });
        case OPEN_APPSUM_EMAIL_PAGETWO:
          return Object.assign({}, state, {
            emailOpen: true,
            emails: action.emails,
            warningAlert: false
          })
        case CLOSE_APPSUM_EMAIL_PAGETWO:
          return Object.assign({}, state, {
            emailOpen: false,
            emails: null,
            warningAlert: false
          });
        case NEW_QUOTATION_ERROR:
          return Object.assign({}, state, {
            errorMsg: action.errorMsg,
            confirmMsg: null,
            warningAlert: false
          });
        case CONFIRM_CREATE_QUOTE:
          return Object.assign({}, state, {
            confirmMsg: action.confirmMsg,
            errorMsg: null,
            warningAlert: false
          });
        case NEW_QUOTATION:
        case QUOT_CLOSE_CONFIRM:
          return Object.assign({}, state, {
            confirmMsg: null,
            errorMsg: null,
            warningAlert: false
          });
        case QUOT_CLOSE_ERROR:
          return Object.assign({}, state, {
            errorMsg: null,
            warningAlert: false
          });
        case INITIAL_APP:
        case LOGOUT:
            return getInitState();  // reset the state
        // case LOC_CHANGE:
        //     if (action.payload && (action.payload.pathname == '/' || action.payload.pathname == '/nonGN')) {
        //         return getInitState();
        //     }
        case PREPARE_APPLICATION_SUMMARY_WARNING:
          return Object.assign({}, state, {
            applicationSummaryWarning: action.warning
          });
        case APPLICATION_SUMMARY_WARNING:
          return Object.assign({}, state, {
            warningAlert: action.bool,
            applicationSummaryWarning: action.applicationSummaryWarning
          });
        case CLOSE_APPLICATION_WARNING:
          return Object.assign({}, state, {
            applicationSummaryWarning: {},
            warningAlert: false
          });
        default:
            return state;
    }
}
