import * as _ from 'lodash';
import * as _v from './Validation';
import * as _c from './client';
import { constants } from 'zlib';

export const relationshipType = {
    OWNER: 'owner',
    SPOUSE: 'spouse',
    DEPENDANTS: 'dependants'
}

/** return undefined or profile array with value
 *  Check is active in dependants owner spouse
 * */
export function isActiveProfileValues (profileValues, fnaForm, d, sd, applicant, aspect, orginalRelationship){
  let resultArr = [];

  if (profileValues &&  profileValues.isActive != false && !_.isArray(profileValues) && (_filterAspects(fnaForm, d, sd, applicant, aspect, profileValues.cid, orginalRelationship) || (orginalRelationship=='DEPENDANTS' && _filterAspects(fnaForm, d, sd, applicant, aspect, profileValues.cid, 'OWNER')) )){
    resultArr.push(profileValues)
    return resultArr;
  }else if(profileValues && _.isArray(profileValues)){
    return profileValues.filter((dependant)=>{ return dependant.isActive != false  && ((_filterAspects(fnaForm, d, sd, applicant, aspect, dependant.cid, 'DEPENDANTS') && orginalRelationship=='OWNER') || _filterAspects(fnaForm, d, sd, applicant, aspect, dependant.cid, orginalRelationship) || (orginalRelationship=='DEPENDANTS' && _filterAspects(fnaForm, d, sd, applicant, aspect, dependant.cid, 'OWNER')) ); });
  }
  return undefined;
}

//Get all used Assets
export function getAllusedFNAAssets(template, relationship, refValues, sectionChangedVal, item, changedValues, context){
  let options = [];
  let {needs, client} = context.store.getState();
  let {pda, fna, template: nTemplate} = needs;
  let {fnaForm} = nTemplate;
  let {profile, dependantProfiles} = client;
  let {dependants: d} = profile;
  let {applicant, dependants: sd} = pda;

  let orginalRelationship = relationship;

  //If is Dependants use all owner assets

 if (relationship =='DEPENDANTS') {
    relationship = 'OWNER';
 }

  //template.cnaForm.needsLandingContent.find((content, i)=>{
    _.forEach(
      _.get(_.find(
        _.get(_.find(
          _.get(_.find(_.get(template, 'items[0].items'), relation=>relation.subType === relationship), 'items'),
            item=>item.id === 'netWorth'
        ), 'items'),
        netItem=>netItem.id === 'assets'
      ), 'items'),
      option=>{
        if (option.asset) {
          options.push({
            value: option.id,
            title: option.title.en,
            assetValue: refValues.fe[relationship.toLowerCase()][option.id],
            usedAssets: 0
          });
        }
      }
    );

  //})
  //Checking and filter out with assetsarray
  let validAsp=[];
  sectionChangedVal.aspects.split(',').filter((aspect)=> {
    return !(_.isUndefined(sectionChangedVal[aspect]))
  }).forEach((validAspect) =>{
    if (!(_.isUndefined(sectionChangedVal[validAspect][_.toLower(relationship)]))){
      validAsp.push(validAspect);
    }else if( (relationship == 'OWNER') && (_.isArray(sectionChangedVal[validAspect].dependants)) ){
      validAsp.push(validAspect);
    }
  });
  //newRefVal contains all valid aspect in the current relationship
  let isActProfileArr, tempArr, tempActProfile,tempArrDep, tempArrDepend;
  isActProfileArr = tempActProfile = [];

  validAsp.map((aspect)=> {
    /**tempArr = this.isActiveProfileValues(sectionChangedVal[aspect][relationship.toLowerCase()]);
    tempArrDepend = this.isActiveProfileValues(sectionChangedVal[aspect].dependants);
    if (relationship == 'OWNER' && !_.isEmpty(tempArr) && !_.isEmpty(tempArrDepend)) {
      tempArr = tempArr.concat(tempArrDepend);
    }else if (relationship == 'OWNER' && !_.isEmpty(tempArrDepend)){
      tempArr = tempArrDepend;
    }*/

    tempArr = this.isActiveProfileValues(sectionChangedVal[aspect][relationship.toLowerCase()], fnaForm, d, sd, applicant, aspect, orginalRelationship);
    tempArrDepend = this.isActiveProfileValues(sectionChangedVal[aspect].dependants, fnaForm, d, sd, applicant, aspect, orginalRelationship);

    if (relationship == 'OWNER' && !_.isEmpty(tempArr) && !_.isEmpty(tempArrDepend)) {
      tempArr = tempArr.concat(tempArrDepend);
    } else if (relationship == 'OWNER' && !_.isEmpty(tempArrDepend)){
      tempArr = tempArrDepend;
    }

    if (!_.isUndefined(tempArr)){
      tempArr.forEach((activeProfile)=>{
        tempActProfile = _.cloneDeep(activeProfile);
        tempActProfile.aspectId = aspect;
        isActProfileArr.push(tempActProfile);
      })
    }
  })

    isActProfileArr.forEach((isActiveProfile)=>{
      if (isActiveProfile && _.isArray(isActiveProfile['assets'])){
      isActiveProfile['assets'].map((asset)=>{
        let selOption = options.find((option) => { return option.value == asset.key});
        if (selOption)
          selOption.usedAssets += asset.usedAsset || 0;
      })
      }else if (isActiveProfile && _.isArray(isActiveProfile['goals'])){
          /**
           * needsId represent the dupPChangedValues belongs to
           * needsId existed when more than one asset in 1 section, calculate the unsaved changedValues
           * dupPChangedValues come from the DuplicatePanel
           * */
          let goalNo = Number(isActiveProfile.goalNo);
          let dupPanels = isActiveProfile['goals'].filter((panel ,index)=> {return panel.assets && index < goalNo});
          if (dupPanels && item.needsId != isActiveProfile.aspectId && !(item.dupPChangedValues)){
            dupPanels.map((panel) => {
              panel.assets.map((asset) =>{
                let selOption = options.find((option) => { return option.value == asset.key});
                if (selOption)
                  selOption.usedAssets += asset.usedAsset || 0;
              })
            })
          }else if (item.needsId == isActiveProfile.aspectId && item.dupPChangedValues){
            //dupPChangedValues cal the unsaved values to be used in Duplicated Panel
            let dupP = item.dupPChangedValues.filter((dupPanel, index)=> {return dupPanel.assets  && index < goalNo});
            if (dupP){
              dupP.map((panel) => {
                panel.assets.map((asset) =>{
                  let selOption = options.find((option) => { return option.value == asset.key});
                  if (selOption)
                    selOption.usedAssets += asset.usedAsset || 0;
                })
              })
            }
          }
        }
    })
    //ChangedValues exist, minus their own assets used
    if (!_.isUndefined(changedValues) && _.isArray(changedValues['assets'])){
      changedValues['assets'].map((asset)=>{
        options.map((option)=>{
          if (option.value == asset.key){
            option.usedAssets -= asset.usedAsset;
          }
        })
      })
    }

  return options;
}

const _getAssetItem = function(template, id){
  return _.find(_.at(template,'items[0].items[0].options')[0], o=>o.id===id);
}

export const getAssetItem = _getAssetItem;

//filter aspect values
/*
  d: dependants of profile
  sd: selected dependants in personal data acknowledgement
  a: applicant
  aid: aspectId
  cid: client id
  t: OWNER, SPOUSE, DEPENDANTS
*/
const _filterAspects = function(template, d, sd, a, aid, cid, t){
  let aspect = _.at(
    _.find(_.at(template, 'items[2].items[0].items[0].items')[0], menu=>menu.id===aid),
    'items[0]'
  )[0];

  if(aspect){
    return _filterAspect(aspect.subType, aspect.filter, d, sd, a, cid, t);
  }
  return false;
}

export const filterAspects = _filterAspects;

const _filterAspect = function(relationship, filter, d, sd, a, cid, t){
  t = _.toUpper(t);
  relationship = _.toUpper(relationship);
  //is exist options
  if(relationship.indexOf(t)>-1){
    if(t === 'OWNER'){
      if(!filter || filter.indexOf(t)>-1)
        return true;
    }
    else if(t === 'SPOUSE'){
      return !filter || (filter.indexOf(t)>-1 && a=== 'joint' && _.find(d, _d=>_d.cid === cid && _d.relationship==='SPO'));
    }
    if(t === 'DEPENDANTS'){
      //is option selected in personal data acknowledge?
      if(sd && sd.indexOf(cid)>-1){
        let dependant = _.find(d, _d=>_d.cid===cid);

        if (_.get(dependant, 'relationship') === 'SPO' && a === 'single') {
          //Make Spouse visiable when spouse is in filter of the items and the application is single
          return !filter || _.indexOf(_.split(filter, ','), dependant.relationship)>-1 || false;
        }else if (_.get(dependant, 'relationship') === 'SPO' && a === 'joint'){
          //Filter out the SPO in dependants When the application is joint
          return false;
        }
        if(dependant){
          return !filter || filter.indexOf(dependant.relationship)>-1;
        }
      }
    }
    else {
      return true;
    }
  }
  return false;
}

export function filterAspect (relationship, filter, d, sd, a, cid, t){
  return _filterAspect (relationship, filter, d, sd, a, cid, t);
};

export function validatePDA(context, template, values, pdaError={}){
  let {app, client, needs} = context.store.getState();
  let {langMap, optionsMap} = app;
  let {profile, dependantProfiles, template: cTemplate} = client;
  let validRefValues = {profile, dependantProfiles, tiTemplate: cTemplate.tiTemplate};
  return _v.exec(template, optionsMap, langMap, values, values, validRefValues, pdaError);
}

export function validateFE(context, template, values, feError={}){
  let {app, client, needs} = context.store.getState();
  let {langMap, optionsMap} = app;
  let {profile, dependantProfiles} = client;
  let {pda} = needs;
  let validRefValues = {profile, dependantProfiles, pda};
  return _v.exec(template, optionsMap, langMap, values, values, validRefValues, feError);
}

export function validateFNA(context, template, values, fnaError={}){
  let {app, client, needs} = context.store.getState();
  let {langMap, optionsMap} = app;
  let {profile, dependantProfiles} = client;
  let {pda, fe, template: nTemplate} = needs;
  let {fnaForm} = nTemplate;
  let validRefValues = {profile, dependantProfiles, pda, fe, fnaForm};
  return _v.exec(template, optionsMap, langMap, values, values, validRefValues, fnaError);
}

export const concatPriorityOption = (profile, dependantProfiles, aSec, value, seperateObj, seperateId, a, re, inCompleteFlag)=> {
  let tempOptions;
  let result =[];
  tempOptions = _handlePriorityOption(profile, dependantProfiles, aSec, value, seperateObj, seperateId, a, re, inCompleteFlag);
  if (_.isArray(tempOptions)){
    result = result.concat(tempOptions)
  }else {
    result.push(tempOptions);
  }

  return result;
}

const _handlePriorityOption = function(p, dp, aSec, aValue, seperateObj = [], seperateId, a, re, inCompleteFlag){
  let result = [];
  let tempaValue = _.cloneDeep(aValue);
  let tempGoals = _.get(tempaValue, 'goals');
  let tempaSec = _.cloneDeep(aSec);
  let goalNo = _.get(aValue, 'goalNo') || 0;
  if (tempGoals) {
    _.forEach(tempGoals, (Obj, index) => {
      tempaValue.totShortfall = Obj.totShortfall;
      tempaValue.timeHorizon = Obj.timeHorizon;
      tempaSec.title = Obj.title;
      if (index < goalNo) {
        result.push(returnOptionValue(p, dp, tempaSec, tempaValue, index, inCompleteFlag));
      }
    })
    return result;
  }

  if (aSec.id === 'pcHeadstart' && !inCompleteFlag) {
    let providedFor = aValue.providedFor || '';
    if (providedFor.indexOf('P') > -1) {
      let pValue = _.cloneDeep(aValue);
      let pSec = _.cloneDeep(aSec);
      pValue.totShortfall = pValue.pTotShortfall;
      pSec.title = pSec.title.en + ' - Protection';
      result.push(returnOptionValue(p, dp, pSec, pValue, 0, inCompleteFlag));
    }

    if (providedFor.indexOf('C') > -1) {
      let cValue = _.cloneDeep(aValue);
      let cSec = _.cloneDeep(aSec);
      cValue.totShortfall = cValue.ciTotShortfall;
      cSec.title = cSec.title.en + ' - Critical Illness';
      result.push(returnOptionValue(p, dp, cSec, cValue, 1, inCompleteFlag));
    }

    if (result.length) {
      return result;
    }
  }

  // seperate option
  let _sObj = _.find(seperateObj, s => s.id === aSec.id);
  if (_sObj) {
    _.forEach(_sObj.items, (sObj, sIndex) => {
      if (window.showCondition(null, sObj, aValue, aValue, aValue)) {
        let _value = _.cloneDeep(aValue);
        let _sec = _.cloneDeep(aSec);
        _value.totShortfall = aValue[sObj.value];
        _sec.title = sObj.title;
        _sec.type = sObj.type;
        result.push(returnOptionValue(p, dp, _sec, _value, sIndex, inCompleteFlag));
      }
    });
    return result;
  }

  return returnOptionValue(p, dp, aSec, aValue, 0, inCompleteFlag);
}

const returnOptionValue = (p, dp, aSec, aValue, index, inCompleteFlag) => {
  let optionTitle = '';
  if (aSec.id === 'psGoals') {
    optionTitle = 'Planning for specific goals';
  }else if (aSec.id === 'other')
    optionTitle = 'Other needs (e.g. mortgage, pregnancy, savings)';
  return {
    id: aValue.cid + '-' + aSec.id + '-' + index, //for sortable list
    name: _c.getClientName(p, dp, aValue.cid),
    cid: aValue.cid,
    aid: aSec.id,
    aTitle: aSec.title || _.get(aValue, `goals[${index}].goalName`) || optionTitle,
    aType: _.get(aValue, `goals[${index}].typeofNeeds`),
    totShortfall: aValue.totShortfall,
    timeHorizon: aValue.timeHorizon,
    annRetireIncome: aValue.annRetireIncome,
    image: aSec.image,
    inCompleteOption: inCompleteFlag
  }
}

const _sortProfiles = function(){
  return function(a,b) {
    let sortArr = ['SON', 'DAU', 'SPO'];
    let priorityA = sortArr.indexOf(a.relationship);
    let priorityB = sortArr.indexOf(b.relationship);

    if (a.relationship === b.relationship && a.fullName > b.fullName)
      return 1;
    else if (a.relationship === b.relationship && a.fullName < b.fullName)
      return -1;
    if (priorityA > priorityB && priorityA !== -1 && priorityB !== -1)
      return -1;
    else if (priorityA < priorityB && priorityA !== -1 && priorityB !== -1)
      return 1;
    else if (priorityA > -1 && priorityB === -1)
      return -1;
    else if (priorityB > -1 && priorityA === -1)
      return 1;
    else if(a.relationship > b.relationship)
        return 1;
    else if (a.relationship < b.relationship)
      return -1;
    else return 0;
  };
}

export const sortProfiles = _sortProfiles;

export function getPriorityOptions (fnaForm, template, profile, dependantProfiles, pda, rootValues, fnaError = {}){
  let options = [];
  let cValue = rootValues[template.id];
  let {dependants: d} = profile;
  let {applicant, dependants: sd} = pda;

  let {subType, groupId, seperateObj} = template;
  let seperateId = [];
  _.forEach(seperateObj, obj => {seperateId.push(obj.id)});


    let aspects = _.split(rootValues.aspects,',');
    _.forEach(aspects, a=>{
      let aValue = rootValues[a];
      if(aValue){
        let aSec = _getAssetItem(fnaForm, a);
        let oid = _.get(aValue, 'owner.cid');
        let inCompleteFlag = false;
        let aError = fnaError[a];
        if(oid && _filterAspects(fnaForm, d, sd, applicant, a, oid, 'OWNER') && aValue.owner.isActive){
          (_.isUndefined(_.get(aError, 'owner.code'))) ? inCompleteFlag = false : inCompleteFlag = true;
          options = options.concat(concatPriorityOption(profile, dependantProfiles, aSec, aValue.owner, seperateObj, seperateId, a, 'OWNER', inCompleteFlag));
        }
        let sid = _.get(aValue, 'spouse.cid');
        if(sid && _filterAspects(fnaForm, d, sd, applicant, a, sid, 'SPOUSE') && aValue.spouse.isActive){
          (_.isUndefined(_.get(aError, 'spouse.code'))) ? inCompleteFlag = false : inCompleteFlag = true;
          options = options.concat(concatPriorityOption(profile, dependantProfiles, aSec, aValue.spouse, seperateObj, seperateId, a, 'SPOUSE', inCompleteFlag));
        }
        if(!window.isEmpty(aValue.dependants)){
          _.forEach(aValue.dependants, de=>{
            if(_filterAspects(fnaForm, d, sd, applicant, a, de.cid, 'DEPENDANTS') && de.isActive){
              (_.isUndefined(_.get(aError, 'dependants.' + de.cid + '.code')) && _.get(aError, 'dependants.' +de.cid + '.skip') !== true ) ? inCompleteFlag = false : inCompleteFlag = true;
              options = options.concat(concatPriorityOption(profile, dependantProfiles, aSec, de, seperateObj, seperateId, a, 'OWNER', inCompleteFlag));
            }
          })
        }
      }
    });

    let orderedCid = [{cid: profile.cid}];
    let orderedDepCid =[];
    let tempOrderedDepCid = _.cloneDeep(dependantProfiles) || [];
    _.forEach(Object.keys(tempOrderedDepCid || []), key =>{orderedDepCid.push(tempOrderedDepCid[key]);});
    let orderedOptions = [];
    orderedDepCid.sort(_sortProfiles());
    orderedCid = orderedCid.concat(orderedDepCid);

    if(groupId){
      let gids = groupId.split(",");
      _.forEach(gids, gid=>{
        let opts = [];
        for(let i=options.length-1; i>=0; i--){
          let opt = options[i];
          if(opt.aid===gid){
            opts.push(opt);
            options.splice(i,1);
          }
        }
        let tempOption;
        _.forEach(orderedCid, obj => {
          tempOption = _.find(opts, ['cid', obj.cid]);
          if (_.isObject(tempOption))
            orderedOptions.push(tempOption);
        });
        let cid="";
        let name="";
        let nameArray = [];
        if(orderedOptions.length){
          _.forEach(orderedOptions, opt=>{
            cid+= (cid !=""?",":"") + opt.cid;
            name+= (name!=""?", ":"") + opt.name;
            nameArray.push(opt.name);
          })
          options.push({
            id: gid,
            cid,
            name,
            nameArray,
            aid: gid,
            aTitle: orderedOptions[0].aTitle,
            image: orderedOptions[0].image
          });
        }


      })
    }
    let optionsFullLength;
    optionsFullLength = options.length;

    if(_.toUpper(subType) === 'SHORTFALL'){
      options = options.filter(o=>(!_.isNumber(o.totShortfall) || o.totShortfall<0));
    }
    else if(_.toUpper(subType) === 'SURPLUS'){
      options = options.filter(o=>o.totShortfall>=0);
    }


    if(cValue){
      let _opts = [];
      _.forEach(cValue, c=>{
        let oi=_.findIndex(options, o=>((groupId && groupId.indexOf(c.aid)>-1) || o.cid===c.cid) && o.aid===c.aid);
        if(oi!=-1){
          _opts.push(options[oi]);
          options.splice(oi, 1);
        }
      })

      _.forEach(options, opt=>{
        _opts.push(opt);
      })
      options=_opts;

    }

    return [options, optionsFullLength];
}

export function getPriorityValues(_options){
    let value = [];
    _.forEach(_options, opt=>{
      value.push({
        cid: opt.cid,
        aid: opt.aid,
        title: _.get(opt, 'aTitle.en') || _.get(opt, 'aTitle'),
        index: Number(opt.id.substring(opt.id.length-1, opt.id.length)) + 1,
        type: opt.aType
      });
    })
    return value;
}

function isPassCka (v) {
  let a1 = v.course || '', a2 = v.collectiveInvestment || '',a3 = v.insuranceInvestment || '', a4= v.profession || '';
  var result = ((a1!='notApplicable'?1:0)+(a2=='Y'?1:0)+(a3=='Y'?1:0)+(a4!='notApplicable'?1:0))>=1? 'Y': 'N';
  return result;
}

export function handleNeedMenu(field, optionsMap, langMap, values, rootValues, validRefValues, errors={}, extra={}){
  let {id, type} = field;
  var error = errors[id] = {};
  if(!values[id]){values[id]={}};
  var value = values[id];


  let valid = true;
  if(type == 'MENUITEM'){
      extra = Object.assign(extra, {menuId: id});
  }
  _.forEach(field.items, item=>{
      if(!_v.exec(item, optionsMap, langMap, value, rootValues, validRefValues, error, extra)){
          valid = false;
      }
  })
  let pt = _.at(rootValues, 'productType.prodType')[0];
  let filter = _.get(field, 'filter') || '';
  let getSelectedDep = _.get(validRefValues, 'pda.dependants')
  let dependantArr = _.filter(_.get(validRefValues, 'profile.dependants'), obj => filter.indexOf(obj.relationship) > -1 && getSelectedDep.indexOf(obj.cid) > -1);
  let defaultPT = true;

  if (_.isUndefined(pt) || _.isEmpty(pt)){
    defaultPT = false;
  }
  if(pt && pt.indexOf('investLinkedPlans')>-1){
    field.disabled = false;
    field.hints = null;
    if(error.code){
      _v.handleError(field, errors, 101);
    }

    //special if cka change
    if(id === 'raSection'){
      let isOwnerPassCka = _.get(rootValues, "ckaSection.owner.passCka");
      let isSpousePassCka = _.get(rootValues, "ckaSection.spouse.passCka");
      if(isOwnerPassCka){
        if(isOwnerPassCka != isPassCka(rootValues.ckaSection.owner)){
          if(rootValues.raSection){
            _v.handleError(field, errors[id], 101);
            _v.handleError(field, errors, 101);
            if(rootValues.raSection.owner)
              rootValues.raSection.owner.init = true;
            if(rootValues.raSection.spouse){
              rootValues.raSection.spouse.init = true;
            }
          }
        }
      }
      if(isSpousePassCka){
        if(isSpousePassCka != isPassCka(rootValues.ckaSection.spouse)){
          if(rootValues.raSection){
            _v.handleError(field, errors[id], 101);
            _v.handleError(field, errors, 101);
            if(rootValues.raSection.owner)
              rootValues.raSection.owner.init = true;
            if(rootValues.raSection.spouse){
              rootValues.raSection.spouse.init = true;
            }
          }
        }
      }
    }

    values[id].isValid = !error.code?true:false;

    return !error.code
  }else {
    if(id === 'ckaSection'){
      field.disabled = true;
      if (defaultPT){
        field.hints = {
          "en": "CKA is not required for your selected product group"
        };
      }else {
        field.hints = undefined;
      }

      values[id].isValid = false;
      return true;
    }
    else if ((id === 'ePlanning' || id === 'pcHeadstart') && dependantArr && dependantArr.length === 0) {
      field.disabled = true;
      field.hints = {
        "en": "No dependants is valid"
      }
      _v.handleError(field, errors[id], 408);
      return false;
    }
    else {
      if(pt && pt.indexOf('participatingPlans')>-1){
        field.disabled = false;
        field.hints = null;
        values[id].isValid = !error.code ? true : false;
        return !error.code;
      }
      else if(id === 'raSection'){
        field.disabled = true;
        if (defaultPT){
          field.hints = {
            "en": "Risk Profile Assessment is not required for your selected product group"
          }
        }else {
          field.hints = undefined;
        }

        values[id].isValid = false;
        return true;
      }
      else {
        values[id].isValid = !error.code?true:false;
      }
    }
  }
  field.hints = undefined;
  field.disabled = false;
  return (valid) ? true : false;
}

export function validateCashFlow (item, values, rootValues, validRefValues = {}, params = {}) {
  const incomeIds = ['mIncome', 'aBonus', 'netMIcncome'];
  const expenseIds = ['personExpenses', 'householdExpenses', 'insurancePrem', 'regSav', 'otherExpense'];

  let errCode = null;
  let { pda, profile, dependantProfiles } = validRefValues;
  const {dependants: d} = profile, {dependants: sd, applicant: a} = pda;
  let sid = _.get(rootValues, 'spouse.cid');
  if (params.type !== 'owner') {
    profile = dependantProfiles[sid];
  }

  // monthly income or cash allowance
  let mIncome = values.mIncome = _.get(profile, 'allowance', 0);

  // insurance premiums

  let result = 0;
  let permId = params.type === 'owner' ? 'pAInsPrem' : permId = 'sAInsPrem';
  result += Number(_.get(rootValues, 'owner.' + permId, 0));
  if(rootValues.spouse && filterAspect('SPOUSE', 'SPOUSE', d, sd, a, sid, 'SPOUSE')){
      result += Number(_.get(rootValues, 'spouse.' + permId, 0));
  }
  _.forEach(rootValues.dependants, vd=>{
      if(filterAspect('DEPENDANTS', 'SON,DAU', d, sd, a, vd.cid, 'DEPENDANTS')){
          result += vd[permId] || 0;
      }
  })
  values.insurancePrem = Number((result/12).toFixed(2));


  let netMIcncome = values.netMIcncome = Number(values.mIncome || 0) - Number(values.lessMCPF || 0);
  let totAIncome = values.totAIncome = netMIcncome * 12 + Number(values.aBonus || 0);
  let totMExpense = values.totMExpense = _.sumBy(expenseIds, eid=>Number(values[eid] || 0));
  let totAExpense = values.totAExpense = Number(values.totMExpense || 0) * 12;
  let aDisposeIncome = values.aDisposeIncome = totAIncome - totAExpense;
  let mDisposeIncome = values.mDisposeIncome = Number(Number((totAIncome - totAExpense) / 12).toFixed(0));

  let hasNoCFReason =
      // no income provided
      _.findIndex(incomeIds, _s=>Number(_.get(values.owner, _s, 0))) > -1 ||
      // no exspense provided
      _.findIndex(expenseIds, _s=>Number(_.get(values.owner, _s, 0))) > -1;

  if (values.otherExpense && !values.otherExpenseTitle) { errCode = 709; }
  else if (!values.otherExpense && values.otherExpenseTitle) { errCode = 710; }
  else if (params.type === 'owner' && hasNoCFReason && !_.get(values.owner, 'noCFReason')) { errCode = 711; }

  return errCode
}

export function onAInsPremChange(item, value, changedValues, rootValues, validRefValues, params = {}) {
  var setInit = true;
  if (changedValues && _.has(changedValues, item.id)) {
    setInit = changedValues[item.id] === value ?  false : true;
  }
  if (setInit) {
    if (params.type !== 'owner' && item.id === 'pAInsPrem' && rootValues.owner) {
      rootValues.owner.init = true;
    } else if (params.type !== 'spouse' && item.id === 'sAInsPrem' && rootValues.spouse) {
      rootValues.spouse.init = true;
    }
  }
}

export function calEiPorfNInsPrem (field, values, rootValues, validRefValues) {
  const isMarried = _.get(validRefValues, 'profile.marital') === 'M';
  let eiProfIds = ['lifeInsProt', 'retirePol', 'ednowSavPol', 'eduFund', 'invLinkPol', 'disIncomeProt', 'ciPort', 'personAcc'];
  let sInsPremIds = ['caInsPrem','csInsPrem','mdInsPrem','fpInsPrem'];
  let aInsPremIds = ['pAInsPrem', 'oAInsPrem'];
  if (isMarried) {
    aInsPremIds.push('sAInsPrem');
  }

  // update existing insurance portfolio of changedValues
  values.eiPorf = getSumByIdArray(values, eiProfIds);

  // update annual premium of changedValues
  values.aInsPrem = getSumByIdArray(values, aInsPremIds);

  // update other premium of changedValues
  values.sInsPrem = getSumByIdArray(values, sInsPremIds);
}

export function validateNetworth (field, values, rootValues, validRefValues, params) {
  let netWorth = values.netWorth;
  let assets = values.assets;
  let liabilities = values.liabilities;
  let otherLiabilites = values.otherLiabilites;
  let otherLiabilitesTitle = values.otherLiabilitesTitle;
  let otherAsset = values.otherAsset;
  let otherAssetTitle = values.otherAssetTitle;
  let noALReason = values.noALReason;

  if (
    // assets and liabilities are optional for spouse
    // owner need to provide reason if no assets and no liability
    (params.type === 'owner' && !assets && !liabilities && !noALReason) ||
    // otherLibailities title and otherLiabilities both should be provide if other field is fill in
    ((otherLiabilites && !otherLiabilitesTitle) || (otherLiabilitesTitle && !otherLiabilites)) ||
    // otherAsset title and otherAsset should provide both should be provide if other field is fill in
    ((otherAsset && !otherAssetTitle) || (!otherAsset && otherAssetTitle))
  ) {
    return 704;
  }
}

function checkProdTypeCondition (field, values, rootValues, validRefValues, _filterAspects, aspects, prodTypeIndex, isDisableRequired) {
  let filterAspects = _.filter(_filterAspects, aspect => _.indexOf(aspects, aspect) >- 1);
  let filterAspectsCnt = _.size(filterAspects);
  let aspectCnt = _.size(aspects);

  //sepecial handling for criticall illness for kids
  const PCHEADSTART = 'pcHeadstart';
  if (filterAspects.indexOf(PCHEADSTART) > -1 && aspects.indexOf(PCHEADSTART) > -1) {
    const { pda = {}, profile = {} } = validRefValues;
    // find kids selected in pda
    const findKids = (cid) => {
      return _.find(profile.dependants, dependant => dependant.cid === cid && ['SON','DAU'].indexOf(dependant.relationship) > -1);
    };
    const kids = _.filter(_.split(pda.dependants, ','), findKids);

    let hasCiKids = _.find(_.get(rootValues, `${PCHEADSTART}.dependants`), sectionValues => {
      return kids.indexOf(sectionValues.cid) > -1 && !sectionValues.init && sectionValues.isActive && (_.get(sectionValues, 'providedFor', '')).indexOf('CI') > -1;
    })

    if (!hasCiKids) {
      filterAspectsCnt = filterAspectsCnt - 1;
    }

  }

  let isSingleSelect = false;

  // cannot select other option
  if (filterAspectsCnt && filterAspectsCnt == aspectCnt) {
    _.forEach(field.options, (option, optionIndex) => {
      option.disabled = isDisableRequired && optionIndex === prodTypeIndex;
    });
    isSingleSelect = true;
  }

  // default value and can select other option
  if (filterAspectsCnt) {
    const optionValue = _.get(field, `options[${prodTypeIndex}].value`);
    if (isDisableRequired && isSingleSelect) {
      values[field.id] = optionValue;
    }

    // filter null option
    let filterOptions = _.filter(_.split(values[field.id], ','), value => value !== 'null');

    // push the select option and uniq the option
    filterOptions.push(optionValue);
    values[field.id] = _.uniq(filterOptions).join(',');
  }

  return isSingleSelect;
}

export function validateProdType (field, values, rootValues, validRefValues) {
  let aspects = _.split(_.get(rootValues, 'aspects', ''), ',');

  // if user select ci protection or pc headstart, product type 0 must be select
  checkProdTypeCondition(field, values, rootValues, validRefValues, ['ciProtection','pcHeadstart'], aspects, 0);

  // if user select hc protection or pa protection, product type 1 must be select
  let isProdType1Only = checkProdTypeCondition(field, values, rootValues, validRefValues, ['hcProtection','paProtection'], aspects, 1, true);

  // if only product type 1 can be selected, show tooltip
  if (isProdType1Only) {
    field.hints = {
      en: 'If you want to select other product type, please change the needs selected previously.'
    };
    field.isSingleSelect = true;
  } else {
    field.hints = null;
    field.isSingleSelect = false;
  }
}

export function calHeadstartExtIns (field, values, rootValues, validRefValues, params) {
  let dependant = _.find(validRefValues.fe.dependants, obj => obj.cid === values.cid);
  values[field.id] = _.sumBy(_.split(params, ','), _id => _.get(dependant, _id, 0));
}

export function validateAspects (field, values, rootValues, validRefValues, params) {
  const { aspects = '' } = values;
  if (aspects.indexOf('pcHeadstart') > -1 || aspects.indexOf('ePlanning') > -1) {
    const dependants = _.split(validRefValues.pda.dependants, ',');
    const profileDependants = _.get(validRefValues, 'profile.dependants', []);
    const hasKids = _.find(profileDependants,
      dependant => {
        return (
          ['SON', 'DAU'].indexOf(dependant.relationship) > -1 &&
          dependants.indexOf(dependant.cid) > -1
        );
      }
    );

    if (!hasKids) {
      return 712;
    }
  }
}

export function validateIarRate (field, values, rootValues, validRefValues, params) {
  const { aspects = '' } = rootValues;
  if (
    aspects.indexOf('fiProtection') === -1 &&
    aspects.indexOf('ciProtection') === -1 &&
    aspects.indexOf('rPlanning') === -1
  ) {
      field.mandatory = false;
  } else {
    field.mandatory = true;
  }
}

export function onIarRateChange (item, value, changedValues, rootValues, validRefValues, params = {}) {
  _.forEach(['fiProtection','ciProtection','rPlanning'], need => {
    _.forEach(['owner', 'spouse'], subType => {
      if (_.get(rootValues, `${need}.${subType}`)){
        rootValues[need][subType].init = true;
      }
      _.forEach(_.get(rootValues, `${need}.dependants`, []), dependant => {
        dependant.init = true;
      })
    });
  });
}

export function calIarRate2 (field, values, rootValues, validRefValues, params) {
  values.iarRate2 = Number(Number(rootValues.iarRate || 0).toFixed(2)) || 0;
}

function calFv (fvRate, fvNper, fvPmt, fvPv, fvType){if(fvRate == 0){return (-fvPv - (fvPmt * fvNper)) || 0}else{return ((((1 - Math.pow(1 + fvRate, fvNper)) / fvRate) * fvPmt * (1 +fvRate * 1) - 0) / Math.pow(1 + fvRate, fvNper)) || 0}};

export function calLumpSum (field, values, rootValues, validRefValues, params) {
  const { iarRate2 = 0, requireYrIncome = 0, pmt = 0 } = values;
  const iarRate = iarRate2 / 100;
  const nper = values
  values.lumpSum = calFv(iarRate, requireYrIncome, -pmt * 12, 0, 1);
}

export function calCompFv (field, values, rootValues, validRefValues, params) {
  const { avgInflatRate = 0, timeHorizon = 0, inReqRetirement = 0 } = values;
  values.compFv = (_.get(values, 'inReqRetirement', 0) * 12) * Math.pow(1 + (avgInflatRate / 100), timeHorizon);
}

export function getTotLiabilities (field, values, rootValues, validRefValues, params) {
  values.totLiabilities = _.get(validRefValues, `fe.${params}.liabilities`, 0);
}

export function validateOtherFundNeedsName (field, values, rootValues, validRefValues, params) {
  if (values.othFundNeeds && !values.othFundNeedsName){
    return 302;
  }
}

export function validateOtherFundNeeds (field, values, rootValues, validRefValues, params) {
  if (!values.othFundNeeds && values.othFundNeedsName){
    return 302;
  }
}

export function calTotRequired (field, values, rootValues, validRefValues, params) {
  values.totRequired = _.sumBy(['finExpenses', 'totLiabilities', 'lumpSum', 'othFundNeeds'], id => Number(values[id] || 0));
}

export function calExitLifeIns (field, values, rootValues, validRefValues, params) {
  values.existLifeIns = _.sumBy(['lifeInsProt', 'invLinkPol'], id => Number(_.get(validRefValues, `fe.${params}.${id}`) || 0) );
}

export function calTotShortfall (field, values, rootValues, validRefValues, params = {}) {
  const { assets } = values;
  const { id = '' } = params;
  const assetsVal = _.sumBy(assets, asset => asset.usedAsset);
  if (id === 'fiProtection') {
    values.totShortfall = Number(-values.totRequired || 0) + Number(values.existLifeIns || 0) + Number(assetsVal || 0);
  } else if (id === 'ciProtection') {
    values.totShortfall = Number(-values.totCoverage || 0) + Number(values.ciProt || 0) + Number(assetsVal || 0);
  } else if (id === 'diProtection') {
    values.totShortfall = Number(-values.pmt * 12 || 0) + Number(values.disabilityBenefit || 0) + Number(values.othRegIncome || 0);
  } else if (id === 'paProtection') {
    values.totShortfall = Number(-values.pmt || 0) + Number(values.extInsurance || 0)
  } else if (id === 'pcHeadstart') {
    values.pTotShortfall = Number(-values.sumAssuredProvided || 0) + Number(values.extInsurance || 0);
    values.ciTotShortfall = Number(-values.sumAssuredCritical || 0) + Number(values.ciExtInsurance || 0);
    values.totShortfall = 'NotShow';
  } else if (id === 'rPlanning') {
    const { assets = [], timeHorizon = 0, maturityValue = 0, compPv = 0, retireDuration = 0} = values;
    const assetsVal = _.sumBy(assets, asset => (Number(asset.usedAsset) * Math.pow((1 + (Number(asset.return) / 100)), timeHorizon)));
    if (!isNaN(assetsVal)) {
      values.totShortfall = Number(-compPv || 0) + Number(assetsVal || 0) + Number(maturityValue || 0);
      // annual retirement income
      values.annRetireIncome = (Number(values.totShortfall) / Number(retireDuration));
    }
  } else if (id === 'ePlanning') {
    const { assets = [], timeHorizon = 0, maturityValue = 0, estTotFutureCost = 0 } = values;
    const assetsVal = _.sumBy(assets, asset => (Number(asset.usedAsset) * Math.pow((1 + (Number(asset.return) / 100)), timeHorizon)));
    if (!isNaN(assetsVal)) {
      values.totShortfall = Number(-estTotFutureCost || 0) + Number(assetsVal || 0) + Number(maturityValue || 0);
    }
  } else if (id === 'psGoals') {
    if (params.type === 'goal') {
      const { assets = [], timeHorizon = 0, projMaturity = 0, compFv = 0 } = values;
      const assetsVal = _.sumBy(assets, asset => (Number(asset.usedAsset) * Math.pow((1 + (Number(asset.return) / 100)), timeHorizon)));
      if (!isNaN(assetsVal)) {
        values.totShortfall = Number(-compFv || 0) + Number(assetsVal || 0) + Number(projMaturity || 0);
      }
    } else {
      values.totShortfall = values.goalNo === 1 ? _.get(values, 'goals[0].totShortfall', 0) : 'NotShow';
    }
  } else if (id === 'other') {
    if (params.type === 'goal') {
      const { needsValue = 0, extInsuDisplay = 0 } = values;
      values.totShortfall = -(Number(needsValue || 0) - Number(extInsuDisplay || 0));
    } else {
      values.totShortfall = values.goalNo === 1 ? _.get(values, 'goals[0].totShortfall', 0) : 'NotShow';
    }
  }
}

export function calAPmt (field, values, rootValues, validRefValues, params) {
  values[field.id] =  Number(values.pmt || 0) * 12;
}

export function calTotCoverage (field, values, rootValues, validRefValues, params) {
  const { mtCost = 0, lumpSum = 0 } = values;
  values.totCoverage = mtCost + lumpSum;
}

export function getCiPort (field, values, rootValues, validRefValues, params) {
  if (params === 'dependants') {
    let _values = _.find(_.get(validRefValues, `fe.dependants`, []), d => d.cid === values.cid);
    values.ciPort = _.get(_values, `fe.${params}.ciPort`, 0);
  } else {
    values.ciPort = _.get(validRefValues, `fe.${params}.ciPort`, 0);
  }
}

export function getFeValue (field, values, rootValues, validRefValues, params) {
  const { type, id, value } = params;
  let data = type === 'dependants' ?
    _.find(_.get(validRefValues, `fe.dependants`, []), d => d.cid === values.cid) :
    _.get(validRefValues, `fe.${type}`);

  values[field.id] = _.get(data, id, value);
}

function getProfile(validRefValues = {}, values = {}, type) {
  const { profile = {}, dependantProfiles = {} } = validRefValues;
  const { cid } = values;
  return ['dependants', 'spouse'].indexOf(type) > -1 ? dependantProfiles[cid] : profile;
}

export function getProfileValue (field, values, rootValues, validRefValues, params) {
  const { type, id, value, setToProperty } = params;
  const data = getProfile(validRefValues, values, type);

  if (setToProperty) {
    field[setToProperty] = _.get(data, id, value);
  } else {
    values[field.id] = _.get(data, id, value);
  }
}

export function getAInReqRetirement (field, values, rootValues, validRefValues, params) {
  values.annualInReqRetirement = _.get(values, 'inReqRetirement', 0) * 12;
}

export function getTimeHorizon (field, values, rootValues, validRefValues, params) {
  const { type, id } = params;

  if (id === 'ePlanning') {
    const { yrtoSupport = 0, curAgeofChild = 0 } = values;
    values.timeHorizon = yrtoSupport - curAgeofChild < 0 ? 0 : (yrtoSupport - curAgeofChild);
  } else {
    const profile = getProfile(validRefValues, values, type);
    values.timeHorizon = ((values.retireAge || profile.nearAge || 0) - profile.nearAge || 0 );
  }
}

export function calFirstYrPmt (field, values, rootValues, validRefValues, params) {
  values.firstYrPMT = (values.compFv || 0) - (values.othRegIncome || 0);
}

export function calRetireDurtion (field, values, rootValues, validRefValues, params) {
  const { type } = params;
  const profile = getProfile(validRefValues, values, type);
  const avgAge = profile.gender === 'M' ? 80 : 85;

  values.retireDuration = avgAge - (values.retireAge || 0);
}

export function calCompPv (field, values, rootValues, validRefValues, params) {
  const { iarRate2 = 0, retireDuration = 0, firstYrPMT = 0 } = values;
  values.compPv = calFv((iarRate2 / 100), retireDuration, -firstYrPMT, 0, 1);
}

export function calEstToFutureCost (field, values, rootValues, validRefValues, params) {
  const { avgEduInflatRate = 0, timeHorizon = 0, costofEdu = 0 } = values;
  values.estTotFutureCost = costofEdu * Math.pow(1 + (avgEduInflatRate / 100), timeHorizon);
}

export function calMaturityValue (field, values, rootValues, validRefValues, params) {
  values.maturityValue = _.get(_.find(validRefValues.fe.dependants, obj => {return obj.cid === values.cid}), 'eduFund', 0);
}

export function validatePreiodofStudy (field, values, rootValues, validRefValues, params) {
  const { studyPeriodEndYear } = values;
  const dob = new Date(validRefValues.profile.dob);

  if (!studyPeriodEndYear) {
    return 305;
  } else if (studyPeriodEndYear <= dob.getFullYear()) {
    return 306;
  }
}
export function calPassCka (field, values, rootValues, validRefValues, params) {
  values.passCka = isPassCka(values);
}

export function isCkaValid (field, values, rootValues, validRefValues, params) {
  values.isValid = params;
}

export function calAssessedRL (field, values, rootValues, validRefValues, params) {
  const { prodType } = rootValues.productType;
  const questions = _.at(values,
    ['riskPotentialReturn', 'avgAGReturn', 'smDroped',
    'alofLosses', 'expInvTime', 'invPref']
  );
  const source = _.sumBy(questions, question=>Number(question || 1));
  values.assessedRL = source > 26 ? 5 : source > 21 ? 4 : source > 14 ? 3 : source > 9 ? 2 : 1;
}

function isJoint (validRefValues) {
  return _.get(validRefValues, 'pda.applicant') === 'joint';
}

function getSumByIdArray (values, ids) {
  return _.sumBy(ids, _id=>Number(values[_id] || 0));
}

function hasInsPrem (values) {
  return values.aInsPrem || values.sInsPrem;
}

function hasEiPorf (values) {
  return values.eiPorf || values.hospNSurg;
}

export function validateEiPorf (field, values, rootValues, validRefValues, params) {
  field.showRedStar = hasInsPrem(values) ? true : false;
  return hasInsPrem(values) && !hasEiPorf(values) ? 714 : null;
}

export function validateInsPrem (field, values, rootValues, validRefValues, params) {
  field.showRedStar = hasEiPorf(values) ? true : false;
  return !hasInsPrem(values) && hasEiPorf(values) ? 715 : null;
}

export function validateBudget (field, values, rootValues, validRefValues, params) {
  const totalBudget = getSumByIdArray(values, ['srsBudget', 'cpfMsBudget', 'cpfSaBudget', 'cpfOaBudget', 'singPrem', 'aRegPremBudget']);
  if (!totalBudget) {
    return 703;
  }

  const isBudgetOverAsset = function (budgetId, assetId) {
    const budget = values[budgetId] || 0;
    const asset = values[assetId] || 0;
    return (budget > asset) ? 706 : null;
  }

  if (field.id === 'cpfOaBudget') {
    return isBudgetOverAsset('cpfOaBudget', 'cpfOa');
  } else if (field.id === 'cpfSaBudget') {
    return isBudgetOverAsset('cpfSaBudget', 'cpfSa');
  } else if (field.id === 'cpfMsBudget') {
    return isBudgetOverAsset('cpfMsBudget', 'cpfMs');
  } else if (field.id === 'srsBudget') {
    return isBudgetOverAsset('srsBudget', 'srs');
  }
}

export function validateRpConfirm (field, values, rootValues, validRefValues, params) {
  const mDisposeIncome = values.mDisposeIncome || 0;

  if (mDisposeIncome) {
    field.disabled = true;
    field.mandatory = false;
    const aDisposeIncome = values.aDisposeIncome || 0;
    const aRegPremBudget = values.aRegPremBudget || 0;
    values[field.id] = aRegPremBudget > aDisposeIncome / 2 ? 'Y' : 'N';
  } else {
    field.disabled = false;
    field.mandatory = true;
  }
}

export function validateSpConfirm (field, values, rootValues, validRefValues, params) {
  const requiredAssets = getSumByIdArray(values, ['savAcc','fixDeposit','invest']);
  const singPrem = values.singPrem || 0;

  if (requiredAssets) {
    field.disabled = true;
    field.mandatory = false;
    values[field.id] = singPrem > requiredAssets / 2 ? 'Y' : 'N';
  } else {
    field.disabled = false;
    field.mandatory = true;
  }
}

export function validateApplicant (field, values, rootValues, validRefValues, params) {
  if (values.applicant === 'joint') {
    const spouseId = _.get(_.find(validRefValues.profile.dependants, dependant => dependant.relationship === 'SPO'), 'cid', '');
    if (spouseId) {
      let selectedDependants = _.split(values.dependants, ',');
      // remove spouse from dependants
      const spouseIndex = selectedDependants.indexOf(spouseId);
      if (spouseIndex > -1) {
        selectedDependants.splice(spouseIndex, 1)
        values.dependants = selectedDependants.join(',');
      }
    }
    if(spouseId == ""){
        values.applicant = 'single';
    }
  }

  // auto select 'Single' if client is not married
  if (validRefValues.profile.marital !== 'M') {
    values.applicant = 'single';
    field.options[1].disabled = true;
  } else {
    field.options[1].disabled = false;
  }
}

export function validateAccompaniment (field, values, rootValues, validRefValues, params) {
  const { profile } = validRefValues;
  const { age = 0, education = '', language = '', dependants = [] } = profile;
  let pcnt = 0;

  if (age >= 62) {
    pcnt++;
  }

  if (education === 'below') {
    pcnt++;
  }

  if (language.indexOf('en') === -1) {
    pcnt++;
  }

  if (pcnt > 1) {
    field.disabled = false;
    return;
  } else {
    field.disabled = true;
    values.trustedIndividual = null;
  }
}

export function validateTrustIndividual (field, values, rootValues, validRefValues, params) {
  field.disabled = values.trustedIndividual === 'Y' ? false : true;
}
