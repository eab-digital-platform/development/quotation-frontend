import React from 'react';
import * as _ from 'lodash';
import * as _n from '../utils/needs';
import * as _c from '../utils/client';
import * as _a from '../utils/application';
import moment from 'moment';

const numType = ['number', 'integer', 'integercurrency', 'currency', 'float', 'nonzeromintxtfield', 'allownegativepercentage','allownegative'];
const mandatoryType = {
  MANDATORY_WITH_DATA: 0,
  MANDATORY_WITHOUT_DATA: 1,
  NOT_MANDATORY: 2,
  INIT: 3
}

const skipCheckTypes = ["READONLY", "QTITLE", "PARAGRAPH"];

const ERROR_MSG = [
  {code: 100, msg: {en:""}},
  {code: 101, msg: {en: "Please complete the form"}},
  {code: 102, msg: {en: "Please view all the tabs"}},
  {code: 103, msg: {en: "Please fill in the table"}},
  {code: 104, msg: {en: "Changes will affect the existing client. Do you confirm?"}},
  {code: 105, msg: {en: "PI generated will become invalid. Do you want to proceed?"}},
  {code: 106, msg: {en: "This change will reset your application progress. Do you want to proceed?"}},
  {code: 107, msg: {en: "Changes will affect the data, Do you confirm?"}},
  {code: 108, msg: {en: "This change will remove the signatures from the documents. Do you want to proceed?"}},
  {code: 109, msg: {en: "Any In progress cases will become invalid. Do you want to proceed?"}},
  {code: 110, msg: {en: "You have a signed case, any changes will invalidate the case. Do you want to proceed?"}},
  {code: 111, msg: {en: "The client has one/more in progress application(s) that may be invalidated. Do you wish to proceed?"}},
  {code: 112, msg: {en: "Your signed cases will become invalid. Do you want to proceed?"}},
  {code: 301, msg: {en: "Please check at least one box"}},
  {code: 302, msg: {en: "This field is required"}},
  {code: 303, msg: {en: "Please input at least one of Life, TPD, CI, PA/ADB"}},
  {code: 304, msg: {en: "Error occur in calculation, please check"}},
  {code: 305, msg: {en: "Please fill in years"}},
  {code: 306, msg: {en: "Year of Graduation should be later than your birth year"}},
  {code: 307, msg: {en: "Please complete the question"}},
  {code: 401, msg: {en: "Invalid email address"}},
  {code: 402, msg: {en: "Invalid telephone number"}},
  {code: 403, msg: {en: "Invalid number"}},
  {code: 404, msg: {en: "Amount should be less than or equal to {1}"}},
  {code: 405, msg: {en: "Amount should be larger than or equal to {1}"}},
  {code: 406, msg: {en: "Length should be shorter than or equal to {1}"}},
  {code: 407, msg: {en: "Length should be longer than or equal to {1}"}},
  {code: 408, msg: {en: "Cannot proceed with this selection"}},
  {code: 409, msg: {en: "Invalid date"}},
  {code: 410, msg: {en: "Age should be less than or equal to {1}"}},
  {code: 411, msg: {en: "Age should be larger than or equal to {1}"}},
  {code: 412, msg: {en: "Length should be equal to {1}"}},
  {code: 413, msg: {en: "Account Number must be equal to {1} characters"}},
  {code: 414, msg: {en: "Value must be greater than {1}"}},
  {code: 415, msg: {en: "Length should be between {1} to {2}"}},
  {code: 501, msg: {en: "No matching options"}},
  {code: 601, msg: {en: "Gender does not match selection"}},
  {code: 602, msg: {en: "Only one spouse is allowed"}},
  {code: 603, msg: {en: "Please fill in given name first"}},
  {code: 604, msg: {en: "Input against Singapore NRIC directory"}},
  {code: 605, msg: {en: "Gender does not match selection"}},
  {code: 606, msg: {en: "Gender does not match selection"}},
  {code: 607, msg: {en: "Input against Singapore FIN directory"}},
  {code: 608, msg: {en: "Myself has indicated marital status other than 'Married'"}},
  {code: 701, msg: {en: "Please fill in personal data acknowledgement first"}},
  {code: 702, msg: {en: "Amount should not be greater than current value"}},
  {code: 703, msg: {en: "At least input one budget."}},
  {code: 704, msg: {en: "This section is not completed"}},
  {code: 705, msg: {en: "Please provide amount for at least one expense"}},
  {code: 706, msg: {en: "Please check your budget is not more than your CPF balances."}},
  {code: 707, msg: {en: "The start date should not be later than the end date"}},
  {code: 708, msg: {en: "AXA Band Aid minimum sum assured is 50,000"}},
  {code: 709, msg: {en: "Missing Other Title"}},
  {code: 710, msg: {en: "Missing amount in other expenses"}},
  {code: 711, msg: {en: "Missing policy number"}},
  {code: 712, msg: {en: "No dependant is valid for the selected need"}},
  {code: 713, msg: {en: "occupation does not match selection"}},
  {code: 714, msg: {en: "Please provide Existing Insurance portfolio"}},
  {code: 715, msg: {en: "Please provide annual insurance premium(s)"}},
  {code: 801, msg: {en: "Please fill in at least one text field (Life, TPD, CI, PA/ADB)"}},
  {code: 802, msg: {en: "Please fill in at least one text field (Life, TPD, CI, PA/ADB) and Total Annual Premium"}},
  {code: 803, msg: {en: "Please fill in at least one field (Life, TPD, CI, PA/ADB)"}},
  {code: 804, msg: {en: ""}},
  {code: 901, msg: {en: "Please change the needs selected previously"}},
]

export const getErrorMsg = function(field, code, lang){
  if (field && field.id && field.id === 'saTable'){
    if (code && _.isArray(code)) {
      let msgArr = [];
      code.forEach(codeNum => {
        let msg =  getLocalText(lang, _.at(_.find(ERROR_MSG, msg=>msg.code === codeNum), 'msg')[0]) || "";
        msgArr.push(msg);
      });
      return msgArr;
    }
    return null;
  }
  let msg =  getLocalText(lang, _.at(_.find(ERROR_MSG, msg=>msg.code === code), 'msg')[0]) || "";
  if([404,406,410].indexOf(code)>-1){
    let {subType, max, maxLength} = field;
    msg = msg.replace("{1}", subType==='currency'?getCurrency(max, '$', 2):max  || maxLength)
  }
  else if([405,407,411, 412].indexOf(code)>-1){
    let {subType, min} = field;
    msg = msg.replace("{1}", subType==='currency'?getCurrency(min, '$', 2):min)
  } else if ([414].indexOf(code)>-1) {
    let {subType, min} = field;
    msg = msg.replace("{1}", subType==='currency'?getCurrency(min-1, '$', 2):min-1)
  } else if ([415].indexOf(code)>-1) {
    let {subType, min, max} = field;
    msg = msg.replace("{1}", subType==='currency'?getCurrency(min, '$', 2):min);
    msg = msg.replace("{2}", subType==='currency'?getCurrency(max, '$', 2):max);
  }
  else if ([413].indexOf(code) > -1) {
    let {min, max, passValues} = field;

    let replaceMsg = '';
    if (_.isArray(passValues)) {
      _.forEach(passValues, (item, i) => {
        replaceMsg += (i > 0 ? ' or ' : '') + item;
      });
    } else {
      replaceMsg = min + (min === max ? '' : ' or ' + max);
    }

    msg = msg.replace('{1}', replaceMsg);
  }
  return msg;
}

const _handleError = function(field, error, code){
  error.code = code;
}

export const handleError = _handleError;

const _isInit = function(id, value, defaultValue, error={}){
  if(value == null){
    if(defaultValue){
      value = defaultValue;
      return false;
    }
    else{
      error.init = true;
      return true;
    }
  }
  return false;
}

const _isSkipByTrigger = function(field, optionsMap, values={}, rootValues, validRefValues, error={}){
  if(field.mandatoryTrigger){
    setMandatory(field, values);
  }
  if(field.trigger && !showCondition(validRefValues, field, null, values, rootValues, null, {optionsMap})){
    return true;
  }
  return false;
}

const _execItemValidation = function(field, values={}, rootValues, validRefValues, error={}, optionsMap){
  let {validation, type} = field;
  type = _.toUpper(type);

  if (_.isString(validation)) {
    let errCode;
    try {
      eval(`var execFunc = ${validation}; errCode = execFunc(field, values, rootValues, validRefValues)`);
      if (errCode) {
        _handleError(field, error, errCode);
      }
    } catch (e) {
      _handleError(field, error, 304);
    }
  } else if (_.isPlainObject(validation)) {
    let errCode;
    if (_.toUpper(validation.type) === 'PASSIFEQUAL' && !window.isEqual(values[field.id], validation.value)){
      _handleError(field, error, 409);
    } else if (validation.module) {
      let funcModule =
        validation.module === 'needs' ? _n :
          validation.module === 'client' ? _c :
            validation.module === 'application' ? _a :
              null;

      let validateFunc = _.get(funcModule, validation.type);
      if (_.isFunction(validateFunc)) {
        if (validation.putOptionsMap !== false) {
          if (validation.optionsMap) {
            validRefValues.optionsMap = _.get(optionsMap, validation.optionsMap);
          }
        }
        errCode = validateFunc(field, values, rootValues, validRefValues, validation.value);

        if(validRefValues.optionsMap){
          delete validRefValues.optionsMap;
        }
      }
    }

    if (errCode) {
      _handleError(field, error, errCode);
    }
  }
}
export const execItemValidation = _execItemValidation;

const _validateEmpty = function(field, langMap, value, error={}){

  let {mandatory, type, allowZero} = field;
  type = _.toUpper(type);
  if(type == "BLOCK" && mandatory){
    var ajs = 1;
  }
  if(value == null && field.value && type !== 'DATEPICKER'){
    return mandatoryType.INIT;
  }
  if(!field.disabled && ((allowZero && !_.isNumber(value)) || (!allowZero && isEmpty(value)))){
    if(mandatory){
      _handleError(field, error, type === 'CHECKBOXGROUP'? 301:302);
      return mandatoryType.MANDATORY_WITHOUT_DATA;
    }
    else {
      return mandatoryType.NOT_MANDATORY;
    }
  }
  if(['TEXT', 'TEXTAREA'].indexOf(type)>-1 && mandatory){
    if (!isEmpty(value) && _.isString(value) && value.trim().length == 0) {
      return mandatoryType.MANDATORY_WITHOUT_DATA;
    }
  }
  return mandatoryType.MANDATORY_WITH_DATA;
}

const _validateCheckBox = function(field, optionsMap={}, langMap={}, values={}, rootValues={}, validRefValues={}, error={}){
  let {id, subType, options} = field;
  subType = _.toLower(subType);
  let value = values[id];

  if(field.mandatory){
    if(!value || value != "Y"){
      _handleError(field, error, 302);
      return false;
    }else{
      return true;
    }
  }else{
    return true;
  }

}

const _validateRiskSlider = function(field, optionsMap={}, langMap={}, values={}, rootValues={}, validRefValues={}, error={}){
  let {id, subType, options} = field;
  let value = values[id];
  if(value === 0 || !value) {
    _handleError(field, error, 302);
    return false;
  }else {
    return true;
  }
}


const _validatePicker = function(field, optionsMap={}, langMap={}, values={}, rootValues={}, validRefValues={}, error={}){
  let {id, subType, options} = field;
  subType = _.toLower(subType);
  let value = values[id];

  let ve = _validateEmpty(field, langMap, value, error);
  if(ve === mandatoryType.MANDATORY_WITHOUT_DATA){
    return false;
  }
  else if([mandatoryType.NOT_MANDATORY, mandatoryType.INIT].indexOf(ve)>-1){
    return true;
  }

  if(subType==='limit'){
    if(_.isString(options)){
      options=_.at(optionsMap, `${options}.options`)[0];
    }
    if(_.findIndex(options,option=>option.value===value)===-1){
      _handleError(field, error, 501);
      return false;
    }

  }
  return true;
}

const _validateDatePicker = function(field, optionsMap={}, langMap={}, values={}, rootValues={}, validRefValues={}, error={}){
  let {id, options} = field;
  let value = values[id];

  let ve = _validateEmpty(field, langMap, value, error);
  if(ve === mandatoryType.MANDATORY_WITHOUT_DATA){
    _handleError(field, error, 302);
    return false;
  }

  if(field.validation && field.validation.type){
    let validation = field.validation;
    let type = validation.type.toLowerCase();
    if (type === 'month_greater'){
      const beforeDate = moment(values[validation.id]);
      const afterDate = moment(value);
      if (afterDate.isBefore(beforeDate, 'month')) {
        _handleError(field, error, 409);
        return false;
      }
      // let lookupValue = values[validation.id];
      // let valueDate = new Date(value); // after
      // let lkValueDate = new Date(lookupValue); // before
      // let valueYr = valueDate.getFullYear();
      // let lkValueYr = lkValueDate.getFullYear();
      // if(valueYr < lkValueYr){
      //     _handleError(field, error, 409);
      //     return false;
      // }else{
      //     let valueMth = valueDate.getMonth();
      //     let lkValueMth = lkValueDate.getMonth();
      //     if(valueMth < lkValueMth){
      //         _handleError(field, error, 409);
      //         return false;
      //     }
      // }
    }
  }
  return true;
};

const _validateText = function(field, optionsMap, langMap, values={}, rootValues, validRefValues, error={}){
  let {id, subType, options, min, max, errorType, passValues, numberType} = field;
  subType = _.toLower(subType), min=Number(min), max=Number(max);
  let value = values[id];
  let validResult = true;
  let ve = _validateEmpty(field, langMap, value, error);
  if(ve === mandatoryType.MANDATORY_WITHOUT_DATA){
    _handleError(field, error, 302);
    return false;
  }
  else if([mandatoryType.NOT_MANDATORY, mandatoryType.INIT].indexOf(ve)>-1){
    return true;
  }

  if (subType === 'email') {
    if(!window.validateEmailAddress(value)){
      _handleError(field, error, 401);
      return false;
    }
  }
  else if(subType === 'phone'){
    if(!(/^\d+$/.test(value) && value.length<=15)){
      _handleError(field, error, 402);
      return false;
    }
  }
  else if (subType === 'accountno') {
    if(value){
      if (!isNaN(min) && !isNaN(max) && min === max && value.length < min) {
        _handleError(field, error, 412);
        return false;
      } else {
        if (_.isArray(passValues)) {
          if (passValues.indexOf(value.length) >= 0) {
            return true;
          } else {
            _handleError(field, error, 413);
            return false;
          }
        } else {
          if (!isNaN(min) && value.length < min) {
            _handleError(field, error, 407);
            return false;
          }
          else if (!isNaN(max) && value.length > max) {
            _handleError(field, error, 406);
            return false;
          }
        }
      }
    }
  }
  else if (subType === 'accountno2') {
    if(value){
      if (!isNaN(min) && !isNaN(max) && min === max && value.length < min) {
        _handleError(field, error, 412);
        return false;
      } else {
        if (_.isArray(passValues)) {
          if (passValues.indexOf(value.length) >= 0) {
            return true;
          } else {
            _handleError(field, error, 413);
            return false;
          }
        } else {
          if (!isNaN(min) && value.length < min || !isNaN(max) && value.length > max) {
            _handleError(field, error, 415);
            return false;
          }
        }
      }
    }
  }
  else if (numType.indexOf(subType)>-1) {
    value = Number(value);
    if (isNaN(value)) {
      _handleError(field, error, 403);
      return false;
    }
    else if (!isNaN(max) && value > max) {
      (_.toLower(errorType) === 'age') ? _handleError(field, error, 410) : _handleError(field, error, 404);
      return false;
    }
    else if (!isNaN(min) && value < min){
      (_.toLower(errorType) === 'age') ? _handleError(field, error, 411) :
        (_.toLower(numberType) === 'value') ? _handleError(field, error, 414) : _handleError(field, error, 405);
      return false;
    }
  }
  else {
    if (!isNaN(min) && !isNaN(max) && min === max && value.length < min) {
      _handleError(field, error, 412);
      return false;
    } else {
      if(!isNaN(min) && value.length < min){
        _handleError(field, error, 407);
        return false;
      }
      else if(!isNaN(max) && value.length > max){
        _handleError(field, error, 406);
        return false;
      }
    }
  }
  return true;
}

const _validateAssets = function(field, langMap, values={}, rootValues, validRefValues, error={}){
  let sn = rootValues.aspects && rootValues.aspects.split(",");//selected needs
  let {cid} = values;
  let valid = true;
  //find total amount of specific asset of specific person
  //find the index first
  let {pda, profile} = validRefValues;
  let {applicant, dependants} = pda;
  let assetValue = {};
  if(isEmpty(pda)){
    _handleError(field, error, 701);
    return false;
  }

  //check have values first, no values = true, values but no key = false, otherwise continue validate
  if((!values.assets || !values.assets.length) && !values.goals){
    return true;
  }
  else if(values.assets.length){
    for(var i in values.assets){
      if(!values.assets[i].key || !values.assets[i].usedAsset || (field.showStepper && !values.assets[i].return)){
        _handleError(field, error, 702);
        return false;
      }
      else {
        assetValue[values.assets[i].key]=0;
      }
    }
  }
  else if(values.goals){
    let allEmpty = true;
    for(let z=0; z<Number(values.goalNo); z++) {
      if(!values.goals[z].assets.length){
        allEmpty = false;
      }
      else if(!values.goals[z].assets.length){
        for(var i in values.goals[z].assets){
          if(!values.goals[z].assets[i].key){
            _handleError(field, error, 702);
            return false;
          }
          else {
            assetValue[values.goals[z].assets[i].key]=0;
          }
        }
      }
    }
    if(allEmpty){
      return true;
    }
  }

  let relationship = cid===profile.cid?"owner": _.at(_.find(profile.dependants, d=>d.cid===cid), 'relationship')[0];
  let rType = applicant==='joint' && _.findIndex(dependants, d=>{return d.cid===cid &&d.relationship==='SPO'}) && relationship === 'SPO' ?'spouse':'owner';

  const addValue = function(p={}, key, _v){
    let _a = _.find(p.assets, as=>as.key===key);
    if(_a){
      _v[key] += Number(_a.usedAsset) || 0;
    }
  }
  _.forEach(sn, n=>{
    let np = rootValues[n] || [];
    let oActive = _n.filterAspects(validRefValues.fnaForm, profile.dependants, dependants, applicant, n, profile.cid, 'owner');
    let sActive = _n.filterAspects(validRefValues.fnaForm, profile.dependants, dependants, applicant, n, _.at(np, 'spouse.cid')[0]||'', 'spouse');
    let dActive = {};
    _.forEach(np.dependants, d=>{
      dActive[d.cid] = _n.filterAspects(validRefValues.fnaForm, profile.dependants, dependants, applicant, n, d.cid, 'dependants');
    })
    if(!isEmpty(np)){
      _.forEach(assetValue, (a,v)=>{
        if(rType === 'spouse'){
          let sv = np[rType];
          if(sv && sv.assets && sv.isActive && sActive){
            addValue(sv, v, assetValue);
          }
          else if(sv && sv.goals && sv.goalNo && sv.isActive && sActive){
            for(let z=0; z<Number(sv.goalNo);z++){
              addValue(sv.goals[z], v, assetValue)
            }
          }

        }
        else {
          let sv = np[rType];
          if(sv && sv.assets && oActive && sv.isActive){
            addValue(sv, v, assetValue);
          }
          else if(sv && sv.goals && sv.goalNo && oActive && sv.isActive){
            for(let z=0; z<Number(sv.goalNo);z++){
              addValue(sv.goals[z], v, assetValue)
            }
          }
          _.forEach(np['dependants'], i=>{
            if(i.assets && i.isActive && dActive[i.cid] && i.isActive){
              addValue(i, v, assetValue);
            }
            else if(i.goals && i.goalNo && dActive[i.cid] && i.isActive){
              for(let z=0; z<Number(i.goalNo);z++){
                addValue(i.goals[z], v, assetValue)
              }
            }
          })
        }
      })
    }

  })
  let fep = validRefValues.fe[rType==='spouse'?rType:'owner'];
  _.forEach(assetValue, (a,v)=>{
    let fepv = (fep && fep[v]) || 0;
    if(a>fepv){
      _handleError(field, error, 702);
      valid = false;
    }
  })

  return valid;
}

const _validatePriority = function(field, langMap, values={}, rootValues, validRefValues, error={}){
  let {fnaForm, profile, dependantProfiles, pda} = validRefValues;
  let options = _n.getPriorityOptions(fnaForm, field, profile, dependantProfiles, pda, rootValues)[0];
  values[field.id] = _n.getPriorityValues(options);
  let ve = _validateEmpty(field, langMap, values[field.id], error);
  return  ve !== mandatoryType.MANDATORY_WITHOUT_DATA;
}

const _validateHbox = function(field, langMap, values={}, rootValues, validRefValues, error={}){
  if(field.mandatory && field.max){
    let hItems = field.items;
    let max = 0;
    for(let hi = 0; hi < hItems.length; hi++){
      let hItem = hItems[hi];
      if(hItem.id && values[hItem.id]){
        max = max + values[hItem.id].length;
      }
      if(hItem.type.toLowerCase() == "text" && hItem.mandatory){
        let valid = _validateText(hItem, {}, langMap, values, rootValues, validRefValues, error);
        if(!valid){
          error[hItem.id] = {code: 302};
        }
      }
    }
    if(max > field.max){
      error[field.key] = {code: 100};
    }
    return (max <= field.max);
  }else{
    return true;
  }
}

const _validateAppFormTabs = function(field, optionsMap, langMap, values, rootValues, validRefValues, error={}, extra={}, valid=true){
  let {subType, items, validation} = field;
  subType = _.toLower(subType);

  let value = values[subType];
  if(subType == "proposer"){
    error[subType] = {};
    _execItemValidation(field, value, rootValues, validRefValues, error[subType], optionsMap);
    if(error[subType].code){
      _handleError(field, error, 101);
    }

    _.forEach(items, (item, index)=>{
      let itemValidate = _validate(item, optionsMap, langMap, value, rootValues, validRefValues[subType], error[subType], extra);
      let errorToCheck = item.id ? error[subType][item.id] : error[subType]
      //see if tab or children of tab have error
      if(!itemValidate || errorToCheck.code){
        _handleError(field, error, 101);
        _handleError(field, error[subType], 101);
        valid = false;
      }
    })
  }else{
    let dError = error[subType] = {};
    _.forEach(value,(v, i)=>{
        let {cid} = v.personalInfo;
        dError[cid]={};
        _execItemValidation(field, v, rootValues, validRefValues, dError[cid], optionsMap);
        if(dError[cid].code){
          _handleError(field, error, 101);
        }
        _.forEach(items, (item, index)=>{
          if(!_validate(item, optionsMap, langMap, v, rootValues, v, dError[cid], extra)){
            _handleError(field, dError[cid], 101);
            _handleError(field, error, 101);
            valid = false;
          }
        })
      }
    )
  }
  return valid;

}

const _validateTabs = function(field, optionsMap, langMap, values={}, rootValues, validRefValues, error={}, extra={}){

  let {subType, items, validation} = field,
    {tabType, menuId, relationship, filter} = extra,
    valid = true,
    {relationshipType: rType} = _n;
  subType = _.toLower(subType);

  if(subType && [rType.OWNER, rType.SPOUSE, rType.DEPENDANTS].indexOf(subType)>-1){
    let {profile: p, pda} = validRefValues,
      {dependants: d, dependantProfiles: dp} = p,
      {applicant: a, dependants: sd} = pda,
      value = values[subType];

    if(rType.DEPENDANTS === subType){
      let dError = error.dependants = {};
      if(isEmpty(value) && !isEmpty(sd)){
        let isValid = true;
        _.forEach(sd.split(","), s=>{
          let dependant = _.find(d, _d=>_d.cid === s);
          if(_n.filterAspect(relationship, filter, d, sd, a, s, subType)){
            isValid = false;
          }
        });
        if(!isValid){
          _handleError(field, error, 102);
          return false;
        }
        else {
          return true;
        }
      }
      _.forEach(value,(v, i)=>{
        let {cid} = v;
        dError[cid]={};
        if(_n.filterAspect(relationship, filter, d, sd,a ,cid, subType)){
          if(tabType === 'cbtabs' && !_.at(v, 'isActive')[0]){
            dError[cid].skip = true;
            return true;
          }
          else if(isEmpty(v) || _.at(v, 'init')[0]){
            _handleError(field, dError[cid], 102);
            _handleError(field, error, 102);
            return true;
          }

          _execItemValidation(field, v, rootValues, validRefValues, dError[cid], optionsMap);
          if(dError[cid].code){
            _handleError(field, error, 101);
          }
          _.forEach(items, (item, index)=>{
            if(!_validate(item, optionsMap, langMap, v, rootValues, validRefValues, dError[cid], extra)){
              _handleError(field, dError[cid], 101);
              _handleError(field, error, 101);
              valid = false;
            }
          })
        }
        else {
          dError[cid].skip = true;
        }
      })
    }
    else {
      error[subType] = {};
      let cid = '';
      if(rType.OWNER === subType){
        cid = p.cid;
      } else {
        if (a !== 'joint'){
          error[subType].skip = true;
          return true;
        }
        if(_.find(d, _d=>_d.relationship==='SPO') === undefined){
          return true;
        }else{
          cid = _.get(_.find(d, _d=>_d.relationship==='SPO'), 'cid', '');
        }
      }

      if(!tabType || _n.filterAspect(relationship, filter, d, sd, a, cid, subType)){
        if(tabType === 'cbtabs' && !_.at(value, 'isActive')[0]){
          error[subType].skip = true;
          return true;
        }
        // validate item before handle empty value
        _execItemValidation(field, value, rootValues, validRefValues, error[subType], optionsMap);

        if(isEmpty(value) || _.at(value, 'init')[0]){
          _handleError(field, error[subType], 102);
          _handleError(field, error, 102);
          return false;
        }
        if(error[subType].code){
          _handleError(field, error, 101);
        }
        _.forEach(items, (item, index)=>{
          if(!_validate(item, optionsMap, langMap, value, rootValues, validRefValues, error[subType], extra)){
            _handleError(field, error, 101);
            _handleError(field, error[subType], 101);
            valid = false;
          }
        })

        //check if error in sub item
        for(var i in error){
          if(_.get(error[i], 'code')){
            valid = false;
          }
        }
      }
      else{
        error[subType].skip = true;
      }
    }
  }else if(subType && ["proposer", "insured"].indexOf(subType) > -1){
    valid =  _validateAppFormTabs(field, optionsMap, langMap, values, rootValues, validRefValues, error, extra);
  }

  return valid;
}

const _validateSumOfAppFormSaTableItem = function(values,AXA,Other,Total){
  let sumValid = true;
  for (let i = 0; i < 5; i++) {
    let keyAXA = AXA[i], keyOther = Other[i], keyTotal = Total[i];
    if(values.hasOwnProperty(keyAXA) &&  values.hasOwnProperty(keyOther) && values.hasOwnProperty(keyTotal)){
      let valueAXA = Number(values[keyAXA]);
      let valueOther = Number(values[keyOther]);
      let valueTotal = Number(values[keyTotal]);
      if(valueAXA + valueOther != valueTotal){
        sumValid = false;
        break;
      }
    }
  }
  return sumValid;
}

const _validateAppFormSaTable = function(field, optionsMap, langMap, values, rootValues, validRefValues, error={}, extra={}, valid=true){
  let { havExtPlans, havAccPlans, havPndinApp, isProslReplace } = values;

  let allValid = false;
  let exValid = false;
  let pendValid = false;
  let rpValid = false;
  let errorCodes = [0,0,0];

  let isFA = false;
  if(validRefValues && validRefValues.extra && validRefValues.extra.channel){
    if(validRefValues.extra.channel !=='AGENCY'){
      isFA = true;
    }
  }

  if (havExtPlans && havExtPlans == 'Y' && field.presentation.indexOf('havExtPlans') > -1 || havAccPlans && havAccPlans == 'Y' && field.presentation.indexOf('havAccPlans') > -1){
    let exArr = ['existLife', 'existCi', 'existTpd', 'existPaAdb'];
    for (let i = 0; i < exArr.length; i ++) {
      exValid = exValid || (values[exArr[i]] > 0)
    }
    if(!exValid){
      //hide this error message for Non-FA
      errorCodes[0] = !isFA ? 804 :803;
    }
    if(exValid){
      let AXA = ["existLifeAXA", "existTpdAXA", "existCiAXA", "existPaAdbAXA","existTotalPremAXA"];
      let Other = ["existLifeOther", "existTpdOther", "existCiOther", "existPaAdbOther","existTotalPremOther"];
      let Total = ["existLife", "existTpd", "existCi", "existPaAdb", "existTotalPrem"];
      let sumValid = _validateSumOfAppFormSaTableItem(values,AXA,Other,Total);
      exValid = exValid && sumValid;
      if(isFA && Number(values["existTotalPrem"]) === 0){
        exValid = false;
      }
      if(!exValid){
        errorCodes[0] = 804;
      }
    }
  } else {
    exValid = true;
  }

  if (havPndinApp && havPndinApp == 'Y' && field.presentation.indexOf('havPndinApp') > -1) {
    let pendArr = ['pendingLife', 'pendingTpd', 'pendingCi', 'pendingPaAdb'];
    for (let i = 0; i < pendArr.length; i ++) {
      pendValid = pendValid || (values[pendArr[i]] > 0)
    }
    if(!pendValid){
      errorCodes[1] = 803;
    }
    if(pendValid){
      let AXA = ["pendingLifeAXA", "pendingTpdAXA", "pendingCiAXA", "pendingPaAdbAXA", "pendingTotalPremAXA"];
      let Other = ["pendingLifeOther", "pendingTpdOther", "pendingCiOther", "pendingPaAdbOther", "pendingTotalPremOther"];
      let Total = ["pendingLife", "pendingTpd", "pendingCi", "pendingPaAdb", "pendingTotalPrem"];
      let sumValid = _validateSumOfAppFormSaTableItem(values,AXA,Other,Total);
      pendValid = pendValid && sumValid;
      if(Number(values["pendingTotalPrem"]) === 0){
        pendValid = false;
      }
      if(!pendValid){
        errorCodes[1] = 804;
      }
    }
  } else {
    pendValid = true;
  }

  if (isProslReplace && isProslReplace == 'Y' && field.presentation.indexOf('isProslReplace') > -1) {
    let rpArry = ['replaceLife', 'replaceCi', 'replaceTpd', 'replacePaAdb'];
    for (let i = 0; i < rpArry.length; i ++) {
      rpValid = rpValid || (values[rpArry[i]] > 0)
    }
    if(!rpValid){
      //hide this error message for non-FA
      errorCodes[2] = !isFA ? 804 :803;
    }
    if(rpValid){
      let AXA = ["replaceLifeAXA", "replaceTpdAXA", "replaceCiAXA", "replacePaAdbAXA", "replaceTotalPremAXA"];
      let Other = ["replaceLifeOther", "replaceTpdOther", "replaceCiOther", "replacePaAdbOther", "replaceTotalPremOther"];
      let Total = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
      let sumValid = _validateSumOfAppFormSaTableItem(values,AXA,Other,Total);
      rpValid = rpValid && sumValid;
      if(isFA && Number(values["replaceTotalPrem"]) === 0){
        rpValid = false;
      }
      if(values['replacePolTypeAXA'] === '' || values['replacePolTypeOther'] === '' ){
        rpValid = false;
      }
      if(!rpValid){
        errorCodes[2] = 804;
      }
    }
  } else {
    rpValid = true;
  }

  allValid = exValid && pendValid && rpValid;

  if (!allValid) {
    _handleError(field, error, errorCodes);
  }
  return allValid;
}

const _validate = function(field, optionsMap, langMap, values={}, rootValues={}, validRefValues={}, errors={}, extra={}){
  let {id, key, type, subType, items, validation, masked, mandatory, trigger, defaultValue, tabType, filter} = field;
  type = _.toUpper(type), subType = _.toLower(subType);
  let value = values, error = errors, empty;


  if(id == 'ROADSHOW01'){
    var ac = value;
  }
  //handle needs section
  if(type === 'MENUITEM' && ['raSection', 'ckaSection'].indexOf(id)>-1){
    return _n.handleNeedMenu(field, optionsMap, langMap, values, rootValues, validRefValues, errors, extra);
  }

  //no validation of following types
  if(type === 'NOTES'){
    return true;
  }

  var skipValidation = _isSkipByTrigger(field, optionsMap, values, rootValues, validRefValues, error) || setDisable(field, values, validRefValues, optionsMap);

  // if it is hidden Field and type is skip does not initial the errors
  if(field.hiddenField && field.hiddenField.type == 'skip' && skipValidation) {
    return true;
  }

  //initial error
  if(['LAYOUT'].indexOf(type) === -1 && id){
    error = errors[id] = {};
    value = values[id];
  }
  else if(['BLOCK', 'MENUITEM', 'TABLE-BLOCK', 'TABLE-BLOCK-T'].indexOf(type) > -1 && key){
    error = errors[key] = {};
  }

  //skip the checking if skip by trigger
  if(skipValidation){
    return true;
  }

  //even fail in validation, keep on checking for the items
  //tab call validation in _validateTabs
  if(type !== 'TAB'){
    _execItemValidation(field, values, rootValues, validRefValues, error, optionsMap);
    // update value again seens the value may be changed by validation
    value = ['LAYOUT'].indexOf(type) === -1 && id ? values[id] : values;
    if(error.code && id){
      _handleError(field, errors, 100)
    }
  }

  if(type == 'TABLE'){
    if(id && mandatory){
      if(value instanceof Array && value.length > 0){
        return true;
      }else{
        _handleError(field, error, 103)
        return false;
      }
    }else{
      return true;
    }
  }
  if (type == 'SATABLE') {
    return _validateAppFormSaTable(field, optionsMap, langMap, values, rootValues, validRefValues, error, extra);
  }
  if((type == 'BLOCK' || type == "TABLE-BLOCK" || type == "TABLE-BLOCK-T") && field.mandatory){
    //when to use: check mandatory of checkbox group
    //see if any fields inputed
    //return true if yes

    let key = field.id||field.key;
    if(!key)
      return true;

    let checkItems = function(items, _value, bkError){
      let _hasErr = 1;
      for(let i = 0; i < items.length; i++){
        let bkItem = items[i];
        let itmId = bkItem.id;
        if(bkItem.items){
          let hasItmError = checkItems(bkItem.items, _value, bkError);
          if(!hasItmError){
            _hasErr = 0;
          }
        }
        else{
          if(itmId){
            let itemErr = bkError[itmId] = {}
            if(bkItem.type.toLowerCase() == 'checkbox'){
              if(blkValue[itmId] == "Y")
                _hasErr = 0;
            }else{
              if(!_hasErr && showCondition(validRefValues, bkItem, null, value, validRefValues, null, {optionsMap})){
                if(blkValue[itmId]){
                  if(!_validate(bkItem, optionsMap, langMap, value, rootValues, validRefValues, itemErr, extra)){
                    _handleError(bkItem, itemErr, 302);
                    _hasErr = 2;
                  }
                }else{
                  _handleError(bkItem, itemErr, 302);
                  _hasErr = 2;
                }
              }
            }
          }
        }
      }
      return _hasErr;
    }

    let hasErr = true;
    let blkValue = id ? value : values;
    let lbkItms = field.items;
    let bkError = error;
    hasErr = checkItems(items, blkValue, bkError);

    if(hasErr){
      if(field.key){
        _handleError(field, bkError, hasErr == 1 ? 301 : 1101 );
      }
      return false;
    }else{
      return true;
    }

  }

  if(type === 'ASSETS'){
    return _validateAssets(field, langMap, values, rootValues, validRefValues, error);
  }
  else if(type === 'TIEDITOR'){
    if(!value){
      value = validRefValues.profile.trustedIndividuals;
    }
    if(!value){
      return false;
    }
    return _validate(validRefValues.tiTemplate, optionsMap, langMap, value, rootValues, validRefValues, error, extra);
  }
  else if(type === 'PRIORITY'){
    return _validatePriority(field, langMap, values, rootValues, validRefValues, error);
  }
  else if(type === 'HBOX'){
    if (field.mandatory && field.max) {
      return _validateHbox(field, langMap, values, rootValues, validRefValues, error);
    } else if (field.key && field.validation) {
      error[field.key] = {};
      _execItemValidation(field, values, rootValues, validRefValues, error[field.key], optionsMap);
    }
  }
  else if(!masked){
    if(type === 'PICKER'){
      return _validatePicker(field, optionsMap, langMap, values, rootValues, validRefValues, error);
    }
    if(type === 'DATEPICKER' && field.validation){
      return _validateDatePicker(field, optionsMap, langMap, values, rootValues, validRefValues, error);
    }
    else if(['TEXT', 'TEXTAREA'].indexOf(type)>-1){
      return _validateText(field, optionsMap, langMap, values, rootValues, validRefValues, error);
    }
    else if(type == "CHECKBOX"){
      return _validateCheckBox(field, optionsMap, langMap, values, rootValues, validRefValues, error);
    }
    else if(type === 'RISKSLIDER'){
      return _validateRiskSlider(field, optionsMap, langMap, values, rootValues, validRefValues, error);
    }
    else {
      empty = _validateEmpty(field, langMap, value, error);
      if(empty === mandatoryType.MANDATORY_WITHOUT_DATA){
        _handleError(field, errors, 101);
        return false;
      }
    }
    /*
    else if(skipCheckTypes.indexOf(type) == -1){
        let ve = _validateEmpty(field, langMap, value, error);
        if(ve === mandatoryType.MANDATORY_WITHOUT_DATA){
            return false;
        }
    }
    else{
        return true;
    }*/

  }


  if(items){
    let valid = true;
    if(type === 'STEP'){
      let sIdx = 0, skip = field.disabled;
      while(_.at(error, `sections[${sIdx}]`)[0]){
        sIdx++;
      }
      error.sections=Object.assign({}, error.sections, {[sIdx]: {skip}});
      let err = error.sections[sIdx];

      if(!skip){
        _.forEach(items, item=>{
          if(!_validate(item, optionsMap, langMap, value, rootValues, validRefValues, error, extra)){
            _handleError(field, error, 101);
            _handleError(field, err, 101);
            valid = false;
          }
        })
      }
    }
    else if(type === 'DUPLICATEPANEL'){
      let {id: tid} = trigger;
      let v = Number(values[tid]);
      if(!isNaN(v)){
        for(let i=v-1;i >=0; i--){
          _.forEach(items, item=>{
            if (value[i]) {
              value[i].cid = values.cid;
            }
            if(!_validate(item, optionsMap, langMap, value[i], rootValues, validRefValues, error)){
              _handleError(field, error, 101);
              valid = false;
              error.code = 101;
            }
          })
          error[i+1] = _.cloneDeep(error);
        }
        _.forEach(items, item=>{
          if (item && item.id) {
            error[item.id] = undefined;
          }
        });
      }
    }
    else if(['INCOMESECTION', 'EXPENSESECTION'].indexOf(type)>-1){
      _.forEach(items, section=>{
        _.forEach(section, item=>{
          if(!_validate(item, optionsMap, langMap, values, rootValues, validRefValues, errors)){
            _handleError(field, error, 101);
            valid = false;
          }
        })
      })
    }
    else if(type =='TAB'){
      return _validateTabs(field, optionsMap, langMap, values, rootValues, validRefValues, error, extra);
    }
    else if(['TABLEVIEW'].indexOf(type)>-1){
      _.forEach(items, item=>{
        if(!_validate(item, optionsMap, langMap, value, rootValues, validRefValues, error, extra)){
          _handleError(field, errors, 303);
          valid = false;
        }
      })
    }
    //tabType only for needs
    else if(type === 'TABS'){
      extra = Object.assign(extra, {tabType, relationship: subType, filter});
      _.forEach(items, item=>{
        if(!_validate(item, optionsMap, langMap, value, rootValues, validRefValues, error, extra)){
          _handleError(field, errors, 101);
          valid = false;
        }
      })
      if(valid && tabType){
        let isAllSkip = true;
        let {relationshipType: rType} = _n;
        if((subType.indexOf(rType.SPOUSE)>-1 && error.spouse && !error.spouse.skip) || (subType.indexOf(rType.OWNER)>-1 && error.owner && !error.owner.skip)){
          isAllSkip = false;
        }
        else if(subType.indexOf(rType.DEPENDANTS)>-1 && error.dependants){
          _.forEach(error.dependants, id=>{
            if(!id.skip){
              isAllSkip = false;
            }
          })
        }
        if(isAllSkip){
          _handleError(field, error, 101);
          _handleError(field, errors, 101);
          valid = false;
        }
      }
    }
    else{
      if(type === 'MENUITEM' && ['ePlanning', 'pcHeadstart'].indexOf(id)>-1){
        return _n.handleNeedMenu(field, optionsMap, langMap, values, rootValues, validRefValues, errors, extra);
      }

      _.forEach(items, item=>{
        if(type==='HBOX' && mandatory){
          item.mandatory = true;
        }
        if(!_validate(item, optionsMap, langMap, value, rootValues, validRefValues, error, extra)){
          _handleError(field, error, 101);
          valid = false;
        }
      })

      if (type == 'MENUITEM'){
        extra = Object.assign(extra, {menuId: id || key});
        if(isEmpty(value)){
          _handleError(field, error, 101);
          _handleError(field, errors, 101);
        }
      }

      if(type == 'MENUITEM' && id && value){
        value.isValid = error.code?false: true;
      }
    }
  }

  return error.skip || !error.code?true:false;
}

export const validateText = _validateText;
export const exec = _validate;
