//@ sourceURL= application.js

export function populateBankACHolderName(field, values, changedValues, rootValues, validRefValues) {
  if (values === 'Payout') {
    changedValues.FUND_DIVIDEND_OPTION_AC_HOLDER_NAME = validRefValues.proposer.personalInfo.fullName;
  } else {
    if(changedValues.FUND_DIVIDEND_OPTION_AC_HOLDER_NAME){
      delete changedValues.FUND_DIVIDEND_OPTION_AC_HOLDER_NAME;
    }
  }
}

export function onChangeBankName(field, values, changedValues, rootValues, validRefValues) {
  var _objBank = validRefValues.optionsMap.options.filter((obj) => {
    return obj.condition === values;
  });

  if (_objBank && _objBank.length === 1) {
    changedValues.FUND_DIVIDEND_OPTION_BRANCH_NAME = _objBank[0].value;
  }else{
    changedValues.FUND_DIVIDEND_OPTION_BRANCH_NAME = "";
  }
  changedValues.FUND_DIVIDEND_OPTION_AC_NUMBER = "";
}

export function onChangeAccountNumber(field, values, changedValues, rootValues, validRefValues) {
  var _objBank = validRefValues.optionsMap.options.filter((obj) => {
    return obj.value === validRefValues.proposer.declaration.FUND_DIVIDEND_OPTION_BANK_NAME;
  });

  if (_objBank && _objBank.length > 0) {
    field.max = parseInt(_objBank[0].acNumDigitsMax);
    field.min = parseInt(_objBank[0].acNumDigitsMin);
  } else {
    field.max = 30;
    field.min = 1;
  }
}

export function validationAccountNumber (field, values, rootValues, validRefValues) {
  if (field.id === 'FUND_DIVIDEND_OPTION_BRANCH_NAME' || field.id === 'FUND_DIVIDEND_OPTION_AC_NUMBER') {
    if(values.FUND_DIVIDEND_OPTION_BANK_NAME){
      var _objBank = validRefValues.optionsMap.options.filter((obj) => {
        return obj.condition === values.FUND_DIVIDEND_OPTION_BANK_NAME;
      });

      if (_objBank && _objBank.length > 0) {
        field.disabled = false;
        field.mandatory = true;
      } else {
        field.disabled = true;
        field.mandatory = false;
        if (field.id === 'FUND_DIVIDEND_OPTION_AC_NUMBER') {
          field.max = 30;
          field.min = 0;
        }
      }
    }else if(values.FUND_DIVIDEND_OPTION_TYPE === 'Payout'){
      field.disabled = true;
      field.mandatory = false;
      if (field.id === 'FUND_DIVIDEND_OPTION_AC_NUMBER') {
        field.max = 30;
        field.min = 0;
      }
    }
  }
}
