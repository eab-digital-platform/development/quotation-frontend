import React from 'react';
import {Dialog, FlatButton, Divider, RaisedButton} from 'material-ui';
import * as _ from 'lodash';
import * as _c from '../../utils/client'
import * as _n from '../../utils/needs';
import * as _v from '../../utils/Validation';

import Constants from '../../constants/SystemConstants';

import {getIcon} from '../Icons/index';

import ErrorMessage from '../CustomViews/ErrorMessage.js'
import EABTextField from '../CustomViews/TextField.js'
import EABMenu from '../CustomViews/Menu';
import EABTabs from '../CustomViews/Tabs';
import EABDatePickerField from '../CustomViews/DatePickerField';
import EABPickerField from '../CustomViews/PickerField';
import IconBoxSelection from '../CustomViews/IconBoxSelection';
import EABStepper from '../CustomViews/Stepper.js';
import EABAvatar from '../CustomViews/Avatar';
import PriorityList from '../CustomViews/PriorityList';
import NormalSlider from '../CustomViews/NormalSlider';
import EABCheckBoxGroup from '../CustomViews/CheckBoxGroup';
import TextSelection from '../CustomViews/TextSelection';
import DependantBox  from '../CustomViews/DependantBox';
import NetworthChart from '../CustomViews/NetWorthChart';
import EABTextArea from '../CustomViews/TextArea';
import HorizontalBarSection from '../CustomViews/HorizontalBarSection';
import ShowValue from '../CustomViews/ShowValue';
import Assets from '../CustomViews/Assets';
import HeaderTitle from '../CustomViews/HeaderTitle';
import LintClientDialog from '../Dialogs/LintClientDialog';
import DynColumn  from './DynColumn';
import ShowRisk from '../CustomViews/ShowRisk';
import RiskSlider from '../CustomViews/RiskSlider';
import EABRadioBoxGroup from '../CustomViews/RadioButtonGroup';
import ClientProfile from '../Dialogs/ClientProfile';
import DuplicatePanel from '../CustomViews/DuplicatePanel';
import ShortFall from '../CustomViews/ShortFall';
import EABCBTabs from '../CustomViews/CbTabs';
import ToolTips from '../CustomViews/ToolTips';
import NeedsSummaryList from '../CustomViews/NeedsSummaryList';
import RiskProfileTable from '../CustomViews/RiskProfileTable';
import TwoSection from '../CustomViews/TwoSection';

import styles from '../Common.scss';

import EABComponent from '../Component';

class DynNeedPage extends EABComponent {
   constructor(props) {
    super(props);

    this.state = Object.assign({}, this.state, {
      values: props.values,
      changedValues: props.changedValues,
      error: props.error,
      template: this.props.template,
      showSpouseDialog: false,
      shortFall: undefined
    })
  }

  static runningId = 0

  getStyle = () =>{
    return {
      hboxItemStyle: {
        display:"inline-block",
        width:"25%"
      },
      hboxItemStyleFullWidth: {
        display:"inline-block",
        flex: '1 0 auto'
      },
      hboxItemContainerStyle: {
        display: "flex",
        justifyContent: "center"
      },
      shortenSpacehboxItemContainerStyle: {
        display: "flex",
        justifyContent: "center",
        width: "80%",
        margin: '0 auto',
        flexWrap: 'wrap'
      },
      pickerFieldStyle: {
        display: "flex",
        width: "initial",
        justifyContent: "center"
      }
    }
  }

  componentWillReceiveProps(nextProps){
    let {template, values, changedValues, error} = this.state;
    let {template: nTemplate, values: nValues, changedValues: nChangedValues, error: nError} = nextProps;
    let isUpd = false;

    if (!window.isEqual(template, nTemplate)) {
      template = nTemplate;
      isUpd = true;
    }
    if (!window.isEqual(values, nValues) || !window.isEqual(changedValues, nChangedValues)) {
      values = nValues;
      changedValues = nChangedValues;
      isUpd = true;
    }

    if (!window.isEqual(error, nError)) {
      error = nError;
      isUpd = true;
    }

    if (isUpd) {
      this.setState({
        template,
        values,
        changedValues,
        error,
        showSpouseDialog: false
      });
    }
  }

  validate=()=>{
    this.props.validate(this.state.changedValues);
  }

  renderItems = (items, template, values = {}, changedValues, error = {}, key, onItemChange, relationship, qNum = 1) =>{
    if (!window.checkExist(template, 'items')) {
      return;
    }
    template.items.map((item, index)=>{
      qNum = this.renderItem(items, _.cloneDeep(item), values, changedValues, error, onItemChange, relationship, index, key, qNum) || qNum;
    })

  }

  renderItem=(items, item, values = {}, changedValues, error = {}, onItemChange, relationship, i, key, qNum = 1)=>{
    let {lang, muiTheme, store, validRefValues} = this.context;
    let {client, needs} = store.getState();
    let {docId} = this.props;
    let {profile, dependantProfiles} = client;
    let {pda, template} = needs;
    let {fnaForm} = template;

    if (_.isUndefined(validRefValues)) {
      validRefValues = {profile, dependantProfiles, pda};
    }

    let cid = profile.cid;
    let {id, type, subType, title, hints, disabled, trigger, validation} = item;
    type = _.toUpper(type);
    //update template if template have validation
    let errorMsg = null, errorCode;
    if (_.isString(validation)) {
      try {
        errorCode = _v.execItemValidation(item, changedValues, this.state.changedValues, validRefValues);
      } catch (e) {
        errorCode = 304;
      }
    }
    
    if (errorCode) {
      _v.getErrorMsg(item. errorCode, lang);
    }

    if (_.get(error, id + '.code') === 302 && !window.isEmpty(_.get(changedValues, id))) {
      this.validate();
    }

    //update value if template have value or formula
    if (item.value && type !== 'NOTES') {
      changedValues[id] = item.value;
    } else if (type === 'NOTES') {
      changedValues[id]=null;
    } else if (item.formula) {
      try {
        eval(`var execFunc = ${item.formula}; changedValues[id] = execFunc(cid, changedValues, this.state.changedValues, validRefValues)`);
      } catch(e) {
        //do nothing
      }
    }

    //change title to client name if neccessary
    if (_.isString(item.title) && (item.title === '@OWNER' || item.title === '@SPOUSE')) {
      let name = _c.getClientName(profile, dependantProfiles, item.title === '@OWNER' ? profile.cid : _c.getSpouseId(this.context));
      item.title = {[lang]:name};
    }

    //add number to title
    if (item.hasNum) {
      for (var z in item.title) {
        item.title[z] = qNum + '. ' + item.title[z];
      }
      qNum = qNum + 1;
    }


    if (trigger && !window.showCondition(validRefValues, item, values, changedValues, this.state.changedValues, null, this.context)) {
      //remove values if hidden
      return;
    }

    let handleChange = (id, value)=> {
      if (_.isFunction(this.props.onChange)) {
        this.props.onChange();
      }

      //handle special case
      if (id === 'applicant') {
        let { profile: _profile, dependantProfiles: _dependantProfiles } = validRefValues;
        let { marital, dependants } = _profile;
        if (marital==='M' && value === 'joint') {
          let spouse = _.find(dependants, dependant=>dependant.relationship === 'SPO');
          if (spouse) {
            var sProfile = _dependantProfiles[spouse.cid];
            sProfile.relationship = spouse.relationship;
          } else {
            var sProfile = {relationship: 'SPOUSE', marital: 'M'};
          }

          if (!spouse || !this.spouseProfile.validateValues(sProfile)) {
            this.spouseProfile.openDialog(sProfile, !spouse);
            return;
          }
        }
      }

      if (item.formula) {
        try {
          eval(`var execFunc = ${item.formula}; changedValues[id] = execFunc(cid, changedValues, validRefValues)`);
        } catch(e) {
          //do nothing
        }
      }

      //Trigger by tabs, id will return as object and put the isActive in the correct profile
      else if (_.isObject(id)) {
        let r = _.toLower(id.relationship);
        if (r !== 'dependants'){
          changedValues[r].isActive = value;
        } else {
          let index = _.findIndex(changedValues[r], d=>d.cid===id.id);
          if (index > -1) {
            changedValues[r][index].isActive = value;
          }
        }
      } else {
        changedValues[id] = value;
      }

      if (_.isFunction(onItemChange)) {
        onItemChange();
      }

      if (_.isFunction(this.props.validate)){
        this.props.validate(this.state.changedValues);
      }
    }

    let __id = key + '-' + i + '-' + id;

    if (type === 'TEXTSELECTION') {
      items.push(
        <div key={__id} style={{padding: (item.noPadding) ? undefined : '0px 24px'}}>
          <TextSelection
            id={id}
            ref={ref=>this[id]=ref}
            template={item}
            values={values}
            changedValues={changedValues}
            error={error}
            handleChange = { handleChange }
          />
        </div>
      )
    }
    else if (type === "READONLY"){

      // need to pass the title to showValue
      let title = window.getLocalText(lang, item.title);

      items.push(
        <ShowValue
          id={id}
          ref={ref=>this[id]=ref}
          key={__id}
          title={title}
          template={item}
          values={values}
          changedValues={changedValues}
          error={error}
          disabled={disabled}
          handleChange={handleChange}
        />
      );
    }
    else if (type === 'DEPENDANTBOX') {
      items.push(
        <DependantBox
          id={id}
          ref={ref=>this[id]=ref}
          key={__id}
          template={item}
          values={values}
          changedValues={changedValues}
          error={error}
          disabled={disabled}
          handleChange={handleChange}
        />
      );
    }
    else if (type === 'IMAGESELECTION') {
      items.push(
        <div key={__id} style={{padding: '0px 24px'}}>
          <IconBoxSelection
            ref={ref=>this[id]=ref}
            template={item}
            values={values}
            changedValues={changedValues}
            error={error}
            docId={docId}
            handleChange={handleChange}
          />
        </div>
      );
    } else if (type === 'TEXT') {
      items.push(
        <div className={styles.MuiItemPagePadding}>
          <EABTextField
            id={id}
            ref={ref=>this[id]=ref}
            key={__id}
            template={item}
            values={values}
            changedValues={changedValues}
            rootValues={this.state.changedValues}
            inputStyle={{textAlign: _.get(item, 'align', 'left')}}
            error={error}
            disabled={disabled}
            handleChange={handleChange}
          />
        </div>
      );
    } else if (type === 'TEXTAREA') {
      items.push(
        <EABTextArea
          id = {id}
          ref={ref=>this[id]=ref}
          key={__id}
          template={item}
          values={values}
          changedValues={changedValues}
          error={error}
          disabled={disabled}
          handleChange={handleChange}
        />
      );
    } else if (type === 'PRIORITY') {
      items.push(
        <PriorityList
          key={__id}
          ref={ref=>this[id]=ref}
          template={item}
          values={values}
          changedValues={changedValues}
          error={error}
          rootValues={this.state.changedValues}
          docId={docId}
          handleChange={handleChange}
          validate={this.validate}
        />
      );
    } else if (type === 'NEEDSSUMMARY') {
      items.push(
        <NeedsSummaryList
          key={__id}
          ref={ref=>this['needsSummary']=ref}
          template={item}
          values={values}
          changedValues={changedValues}
          fnaForm={fnaForm}
          docId={docId}
        />
      );
    } else if (type === 'STEPPER') {
      items.push(
        <EABStepper
          key={__id}
          ref={ref=>this[id]=ref}
          template={item}
          values={values}
          changedValues={changedValues}
          error={error}
          rootValues={this.state.changedValues}
          handleChange={handleChange}
        />
      );
    } else if (type === 'NOTES') {
      let paragClassName;
      let headerClassName;
      if (item.value && item.paragraphType && item.paragraphType.toLowerCase() === 'warning') {
        paragClassName = styles.alignCenter + ' ' + styles.ShowAlertColor + ' ' + styles.bold;
        item.value = item.value.replaceAll('<p>', '<p id="warningnote">')
      } else if (item.value && item.paragraphType && item.paragraphType.toLowerCase() === 'header'){
        headerClassName = styles.headerTitleStyle;
      }

      items.push(
        <div key={__id} style={{margin: (_.toLower(_.get(item, 'paragraphType'))  === 'header') ? '0 auto' : undefined }}>
          {title ?
            <HeaderTitle key={"label"+key+item.id} title={title} hints={hints} mandatory={item.showRedStar || item.mandatory}/>:
            null
          }
          {
            (item.value) ?
            <div
              ref={ref=>this[id]=ref}
              className={styles.SectionPadding + ' ' + styles.alignCenter + ' ' + paragClassName + ' ' + headerClassName}
              style={{textAlign: (item.align) ? item.align : 'center', padding: (_.toLower(_.get(item, 'paragraphType'))  === 'header') ? '0px' : undefined }}
              dangerouslySetInnerHTML={{__html: window.getLocalText(lang, item.value)}}
            />
            :
            undefined
          }
        </div>
      );
    }
    else if (type === 'BLOCK') {
      this.renderItems(items, item, values, changedValues, error, `block-${key}-${i}`, onItemChange, relationship, qNum);
    }
    else if (type === 'MENU') {
      let menuTemplate = [];
      item.items.map((menuSection)=>{
        let msObj = {title: menuSection.title};
        let msItems = [];

        //add validate function to items;
        menuSection.items.map((menuItem, index)=>{
          if (!(menuItem.trigger && !window.showCondition(validRefValues, menuItem, values, changedValues, this.state.changedValues))) {
            msItems.push({
              id: menuItem.id,
              title: menuItem.title,
              template: menuItem
            });
          }
        });
        msObj.items = msItems;
        menuTemplate.push(msObj)
      });

      items.push(
        <EABMenu
          id={item.key}
          key={item.key}
          ref={ref=>this[id]=ref}
          template={menuTemplate}
          renderItems={this.renderItems}
          changedValues={changedValues}
          error={error}
          values={values}
          handleChange={onItemChange}
          skipCheck={true}
          hasFixedBottom
          onMenuItemChange={this.props.onSave}
        />
      );
    }
    else if (type === 'TABS') {
      let tabsTemplate = [];
      let hasOwner = subType.toUpperCase().indexOf("OWNER")>-1;
      let hasSpouse = subType.toUpperCase().indexOf("SPOUSE")>-1;
      let hasDependants = subType.toUpperCase().indexOf("DEPENDANTS")>-1;

      let options = _c.getPdaMember(this.context, hasOwner, hasSpouse, hasDependants);

      let {dependants:sd, applicant} = validRefValues.pda;
      let cnt = 0, _member, cValue;

      options.map((member, index)=>{
          //initial changedValues
        if (_n.filterAspect(subType, item.filter, profile.dependants, sd, applicant, member.cid, member.relationship)) {
          cnt++;
          _member = _.cloneDeep(member);
          let templateItem = _.cloneDeep(item.items.find(templateItem=>templateItem.subType == member.relationship));
          cValue = (id && changedValues[id]) || changedValues;
          if (id && !window.checkExist(changedValues, id)) {
            cValue = changedValues[id] = {};
          }

          if (member.relationship === 'OWNER') {
            if (!window.checkExist(cValue, 'owner')) {
              cValue.owner = {cid: member.cid, init: true, isActive: false};
            }
          } else if (member.relationship === 'SPOUSE') {
            if (!window.checkExist(cValue, 'spouse')) {
              cValue.spouse = {cid: member.cid, init: true, isActive: false};
            }
          } else if (member.relationship === 'DEPENDANTS') {
            if (!window.checkExist(cValue, 'dependants')) {
              cValue.dependants = [];
            }

            let dIdx = _.findIndex(cValue.dependants, d=>d.cid==member.cid);
            let tValue;
            if (dIdx > -1) {
              tValue = cValue.dependants[dIdx];
            }
            templateItem.error = dIdx > -1 ? _.get(templateItem, `error.invalid[${dIdx}]`) : null;


            if (!tValue) {
              cValue.dependants.push({cid: member.cid, init: true, isActive: false});
              cValue.dependants[cValue.dependants.length - 1];
            }
          }


          tabsTemplate.push({
            id: member.cid,
            title: _c.getClientName(profile, dependantProfiles, member.cid),
            relationship: member.relationship,
            template: templateItem
          });
        }
      });

      if (item.tabType === 'cbtabs') {
        //auto select if only 1 option
        if (cnt === 1) {
          if (_member.relationship === 'OWNER') {
            cValue.owner.isActive = true;
          } else if (_member.relationship === 'DEPENDANTS') {
            let _d = _.find(cValue.dependants, d=>d.cid === _member.cid);
            if (_d) {
              _d.isActive = true;
            }
          }
        }

        if (!window.isEmpty(tabsTemplate)) {
          items.push(
            <EABCBTabs
              key={__id}
              ref={ref=>this[id]=ref}
              template={tabsTemplate}
              renderItems={this.renderItems}
              changedValues={changedValues}
              error={error}
              values={values}
              handleChange={onItemChange}
              tabCheckedChange={handleChange}
              menuId={item.menuId}
              skipCheck
              onChange={this.props.onChange}
              onTabChange={this.props.onSave}
              validate={this.validate}
            />
          );
        }
      } else {
        items.push(
          <EABTabs
            key={__id}
            ref={ref=>this[id]=ref}
            template={tabsTemplate}
            renderItems={this.renderItems}
            changedValues={changedValues}
            error={error}
            values={values}
            handleChange={onItemChange}
            skipCheck={true}
            onChange={this.props.onChange}
            onTabChange={this.props.onSave}
            validate={this.validate}
          />
        );
      }
    } else if (type === 'HEADER') {
      items.push(
        <div key={__id+'-header'} className={styles.PageSectionTitle + ' ' + styles.NeedsPanelTitleColor}>
          {_.toUpper(window.getLocalText(lang, item.value))}
        </div>
      );

      items.push(
        <div key={__id+'-divider'} className={styles.Divider}/>
      );
    } else if (type === 'DIVIDER') {
      items.push(
        <div key={__id + '-divider'} className={styles.Divider}/>
      );
    } else if (type === 'HEADERONLY') {
      items.push(
        <HeaderTitle key={__id + '-header'} title={item.title} mandatory={item.showRedStar || item.mandatory} hints={item.hints}/>
      );
    } else if (type === 'HEADERWITHERROR') {
      items.push(
        <HeaderTitle key={__id + '-header'} title={item.title} mandatory={item.showRedStar || item.mandatory} hints={item.hints}/>
      );

      items.push(
        <div key={__id + '-error-msg'} style={{color: 'red', fontSize: '12px', height: '24px', textAlign: 'center'}}>
          {_v.getErrorMsg(item, _.get(error, `${item.id}.code`,''), 'en')}
        </div>
      );
    }
    else if (type === 'NORMALSLIDER'){
      if (window.checkExist(item, 'id')) {
        if (!window.checkExist(changedValues, item.id)) {
          changedValues[id] = item.value || item.min || 0;
        }
      };

      items.push(
        <NormalSlider
          key={__id}
          ref={ref=>this[id]=ref}
          template={item}
          values={values}
          changedValues={changedValues}
          error={error}
          rootValues={this.state.changedValues}
          handleChange={handleChange}
        />
      );
    } else if (type === 'HBOX' ) {
      let titleNode = null;

      const itemError = _.get(error, item.id + '.code');
      if (itemError) {
        errorMsg = _v.getErrorMsg({}, itemError, lang);
      }

      if (item.title) {
        titleNode = (
          <HeaderTitle key={`${__id}-label`} title={item.title} hints={hints} mandatory={item.showRedStar}/>
        );
       }

      let subItems = [];
      this.renderItems(subItems, item, values, changedValues, error, key, relationship);

      let style = this.getStyle();
      let _subItems = [];

      for (let j in subItems) {
        _subItems.push(<div key={subItems[j]['key'] + "wrap"} style={(item.fullWidth) ? style.hboxItemStyleFullWidth : style.hboxItemStyle}>{subItems[j]}</div>)
      }

      items.push(
        <div key= {`${__id}-hbox`} className={"hBoxContainer"+"column1"}>
            {titleNode}
            <div key={`${__id}-hbox-items`} style={(item.lessSpace) ? style.shortenSpacehboxItemContainerStyle : style.hboxItemContainerStyle}>
              {_subItems}
            </div>
            {_.isString(errorMsg)?<ErrorMessage message={errorMsg} style={{position: 'relative', textAlign: 'center'}}/>: null}
        </div>
      );
    } else if (type === 'PICKER') {
      items.push(
        <EABPickerField
          id={id}
          ref={ref=>this[id]=ref}
          key={__id}
          template={item}
          values={values}
          changedValues={changedValues}
          error={error}
          handleChange={handleChange}
          style={this.getStyle().pickerFieldStyle}
          pickerStyle={true}
          />
      );
    } else if (type === 'DATEPICKER') {
      items.push(
        <EABDatePickerField
          id={id}
          ref={ref=>this[id]=ref}
          key={__id}
          template={item}
          values={values}
          mode={muiTheme.isPortrait?'portrait':'landscape'}
          disabled={disabled}
          changedValues={changedValues}
          error={error}
          handleChange={(id, value)=>{
            handleChange(id, value);
            this.forceUpdate();
          }}
        />
      );
    } else if (type === 'CHECKBOXGROUP') {
      items.push(
        <div key={__id} style={{padding: '0px 24px'}}>
          <EABCheckBoxGroup
            id={id}
            ref={ref=>this[id]=ref}
            template={item}
            values={values}
            changedValues={changedValues}
            error={error}
            disabled={disabled}
            handleChange={handleChange}
          />
        </div>
      );
    } else if (type === "NETWORTHCHART" ) {
      if (window.checkExist(item, 'id')) {
        if (!window.checkExist(changedValues, item.id)) {
          changedValues[id] = 0;
        }
      }

      let sectionIds = [];
      if (window.checkExist(item, 'items')) {
        item.items.map((__item)=>{
          sectionIds.push(__item.id);
          if (!window.checkExist(changedValues, __item.id)) {
            changedValues[__item.id] = 0;
          }
        });
      }

      items.push(
        <NetworthChart
          id={id}
          ref={ref=>this[id]=ref}
          key={__id}
          template={item}
          values={values}
          changedValues={changedValues}
          error={error}
          disabled={disabled}
          handleChange={handleChange}
        />
      );
    } else if (type === 'HORIZONTALBARSECTION') {
      if (window.checkExist(item, 'id')) {
        if (!window.checkExist(changedValues, item.id)) {
          changedValues[id] = 0;
        }
      }

      items.push(
        <div key={__id} style={{padding: '0px 24px'}}>
          <HorizontalBarSection
            id={id}
            ref={ref=>this[id]=ref}
            key={__id}
            template={item}
            values={values}
            changedValues={changedValues}
            rootValues={this.state.changedValues}
            error={error}
            disabled={disabled}
            handleChange={handleChange}
          />
        </div>
      );
    }
    else if(type==="TWOSECTION"){
      items.push(
        <TwoSection
          key={__id}
          template={item}
          values={values}
          changedValues={changedValues}
          validRefValues={validRefValues}
          rootValues={this.state.changedValues}
          error={error}
          disabled={disabled}
          handleChange={handleChange}
        />
      );
    } else if (type === 'ASSETS') {
      let {
        fe,
        template
      } = this.context.store.getState().needs;

      let options = [];

      options = _n.getAllusedFNAAssets(template.feForm, relationship, validRefValues, this.state.changedValues, item, changedValues, this.context);

      items.push(
        <Assets
          id = {id}
          ref={ref=>this[id]=ref}
          key={__id}
          template = { item }
          values = { values }
          changedValues = { changedValues }
          error={error}
          disabled = {disabled}
          handleChange = { handleChange }
          relationship = { relationship }
          options ={options}
          onItemChange={onItemChange}
          yrofInvest={(item.trigger) ? changedValues[item.trigger.id] : undefined}
        />
      );
    } else if (type === 'TIEDITOR') {
      //let {trustIndividualsEditDialog} = this.context.store.getState().client.template;
      let refKey = i + '-' + id + '-client';
      let _value = changedValues.tiInfo;
      if (!_value) {
        _value = changedValues.tiInfo = _.cloneDeep(profile.trustedIndividuals) || {};
      }

      let {tiTemplate} = this.context.store.getState().client.template;

      items.push(
        <div key={__id}>
          <HeaderTitle key={"label"+key+item.id} title={title}/>
          <LintClientDialog
            ref={ref=>{this[refKey]=ref}}
            onSubmit={(profile)=>{
              changedValues.tiInfo = {
                firstName: profile.firstName,
                lastName: profile.lastName,
                fullName: profile.fullName,
                nameOrder: profile.nameOrder,
                mobileCountryCode: profile.mobileCountryCode,
                mobileNo: profile.mobileNo,
                idDocType: profile.idDocType,
                idCardNo: profile.idCardNo
              }
              this.validate();
              //this.forceUpdate();
            }}
          />
          <DynColumn
            template={tiTemplate}
            changedValues={_value}
            values={changedValues.tiInfo}
            error={error.tiInfo}
            onChange={this.props.onChange}
            validate={this.validate}
            customStyle={{padding: '0px 24px'}}
          />
          <div className={styles.SectionPadding  + ' ' + styles.alignCenter}>
            <RaisedButton
              label={window.getLocalText(lang, "PICK FROM CONTACTS")}
              primary={true}
              onTouchTap={()=>{
                this[refKey].openDialog();
              }}
              style={{margin: '0 auto'}}
              />
          </div>
        </div>
      );
    } else if (type === 'RISKSLIDER') {
        items.push(
          <RiskSlider
            key={__id}
            ref={ref=>this[id]=ref}
            template={item}
            values={values}
            changedValues={changedValues}
            error={error}
            rootValues={this.state.changedValues}
            handleChange={ handleChange}
          />
        );
      } else if (type === 'RADIOGROUP') {
        items.push(
            <EABRadioBoxGroup
              style={{display: 'flex', minHeight: '0px', flexDirection: 'column', alignItems: 'flex-start'}}
              ref={ref=>this[id]=ref}
              key={__id}
              template={item}
              values={values}
              options={item.options}
              changedValues={changedValues}
              error={error}
              disabled={disabled}
              handleChange={handleChange}
            />
        );
      } else if (type === 'DUPLICATEPANEL') {
        items.push(
          <DuplicatePanel
            id = {id}
            ref={ref=>this[id]=ref}
            key={__id}
            template = { item }
            values = { values }
            changedValues = { changedValues }
            error={error}
            renderItems={this.renderItems}
            disabled = {disabled}
            handleChange = { handleChange }
            relationship = { relationship }
            qNum={qNum}
          />
        );
      } else if (type === 'SHORTFALL') {
        items.push(
          <ShortFall
            id={id}
            ref={ref=>this[id]=ref}
            key={__id}
            title={title}
            template={item}
            values={values}
            changedValues={changedValues}
            error={error}
            disabled={disabled}
          />
        );
      } else if (type === 'RISK') {
        let {options} = item;
        let v = changedValues[item.id] || 0;
        let {title: oTitle = '', hints: oHints =''} = options[v-1] || {};
        items.push(
          <div key={__id} style={{display: 'flex', minHeight: '0px', alignItems: 'center'}}>
            {window.getLocalText(lang,title)} 
            <span style={{/*note: lucia */ textTransform: 'uppercase', fontSize: '24px', fontWeight: 600, color: '#1cc54e', padding: '0 0 0 10px'}}>
              {window.getLocalText(lang, oTitle)}
            </span>
          </div>
        );
      } else if (type === 'RISKTABLE') {
        items.push(<RiskProfileTable key={__id}/>);
      }

    return qNum;
  }

  render(){

	  let { muiTheme, langMap, lang } = this.context;
    let { style, title, template={},changedValues, error, values, showSpouseDialog } = this.state;

    let fields = [];
    this.runningId = 0;

    this.renderItems(fields, template, values, changedValues, error, template.id);

    return (
      <div style={(template.style) ? template.style : {display: 'flex', minHeight: '0px', flexDirection: 'column', height: '100%', flex: 1, minWidth: '1024px'}}>
        <ClientProfile
          mandatoryId={_c.genProfileMandatory(Constants.FEATURES.FNA, true, false, true)}
           ref={ref=>this.spouseProfile=ref} isFamily isSpouse
           onSubmit={() => {changedValues['applicant'] = 'joint'; this.setState({changedValues}); this.validate(); }}/>
        <Dialog
          style={{zIndex:1000}}
          open={showSpouseDialog}
          actions={
            <FlatButton
              label={window.getLocalizedText(langMap, 'BUTTON.OK', 'OK')}
              onTouchTap={()=>{this.setState({showSpouseDialog:false})}}
            />
          }
        >
          {window.getLocalizedText(langMap, 'NEED.SPOUSE_MANDATORY', 'Spouse data is missing, please complete the information first.')}
        </Dialog>
        {fields}
        <div style={{alignSelf: 'flex-end'}}>{ }</div>
      </div>

    );

  }
}
export default DynNeedPage;
