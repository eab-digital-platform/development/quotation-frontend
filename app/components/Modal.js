import React, {Component, PropTypes} from 'react';
import { unmountComponentAtNode, unstable_renderSubtreeIntoContainer } from 'react-dom';

export default class Modal extends Component {

  static propTypes = {
    children: PropTypes.element
  };

  componentDidMount() {
    this.el = document.createElement('div');
    document.body.appendChild(this.el);
    this._render();
  }

  componentDidUpdate() {
    this._render();
  }

  componentWillUnmount() {
    unmountComponentAtNode(this.el);
    document.body.removeChild(this.el);
  }

  _render() {
    unstable_renderSubtreeIntoContainer(this, this.props.children, this.el);
  }

  render() {
    return null;
  }

}
