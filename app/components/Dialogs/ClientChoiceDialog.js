import React, { PropTypes } from 'react';
import { Dialog, Toolbar, ToolbarGroup, IconButton, ToolbarTitle, FlatButton, Checkbox, Card } from 'material-ui';
import * as _ from 'lodash';

import styles from '../Common.scss';
import EABInputComponent from '../CustomViews/EABInputComponent';
import { getIcon } from '../Icons/index';
import { rplists } from '../Pages/Pos/Quotation/rpPlanList';

export default class ClientChoiceDialog extends EABInputComponent {

  static propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      data: [],
      selectedPlan: {},
      openRPplanDialog: false
    });
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      data: nextProps.data
    });
  }

  closeDialog() {
    let { onClose } = this.props;
    onClose();
  }

  renderCheckList() {
    const { data } = this.state;
    let { muiTheme, langMap } = this.context;
    const style = {
      header: {
        color: muiTheme.palette.primary2Color,
        paddingBottom: '5px',
        fontWeight: 500,
        flex: 10
      },
      text: {
        color: muiTheme.palette.primary2Color,
        fontSize: '14px',
        paddingBottom: '5px'
      },
      premium: {
        fontSize: '16px',
        paddingTop: '10px',
        paddingBottom: '10px'
      },
      card: {
        width: '100%',
        padding: '10px',
        marginTop: '10px'
      },
      dimCard: {
        width: '100%',
        padding: '10px',
        backgroundColor: 'lightgrey',
        marginTop: '10px'
      }
    };

    const calTotalYrPremium = (plans) => {
      let totPremium = 0;
      plans.forEach(plan => {
        if (plan.yearPrem) {
          totPremium = totPremium + plan.yearPrem;
        }
      });
      return totPremium;
    };

    const calPremiumType = (item) => {
      switch (item.paymentMode) {
        case 'A': return {
          premium: (item.plan) ? calTotalYrPremium(item.plan) : 0,
          paymentMode: window.getLocalizedText(langMap, 'application_list.payMode_A')
        };
        case 'S': return {
          premium: (item.plan) ? item.plan[0].halfYearPrem : 0,
          paymentMode: window.getLocalizedText(langMap, 'application_list.payMode_S')
        };
        case 'Q': return {
          premium: (item.plan) ? item.plan[0].quarterPrem : 0,
          paymentMode: window.getLocalizedText(langMap, 'application_list.payMode_Q')
        };
        case 'M': return {
          premium: (item.plan) ? item.plan[0].monthPrem : 0,
          paymentMode: window.getLocalizedText(langMap, 'application_list.Monthly')
        };
        case 'L': return {
          premium: (item.plan) ? item.plan[0].premium : 0,
          paymentMode: window.getLocalizedText(langMap, 'application_list.payMode_L')
        };
        case '': return {
          premium: item.totPremium,
          paymentMode: ''
        };
        default: return {
          premium: 0,
          paymentMode: ''
        };
      }
    };

    const resetCheckboxes = () => {
      data && data.forEach(item => {
        if (item.checked) {
          item.checked = false;
        }
      });
    };

    return (
      <div>
        {data && data.map(item => {
          const premiumType = calPremiumType(item);
          return (
            <Card style={item.isDim ? style.dimCard : style.card}>
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <div style={style.header}>
                  {!_.isEmpty(item.plan) ? item.plan[0].covName.en + ' (' + item.paymentMethod + ') ' + ' (' + item.quotId + ')' : ''}
                </div>

                {!item.isDim && <Checkbox
                  style={{ flex: 1 }}
                  label={''}
                  labelStyle={{ color: muiTheme.palette.primary2Color }}
                  checked={item.checked}
                  onCheck={(event, value) => {

                    if (!_.isEmpty(item.errorMsg)) {
                      this.setState({ errorMsg: item.errorMsg, warningDialogOpen: true });
                      return;
                    }
                    resetCheckboxes();
                    item.checked = value;
                    if (item.checked) {
                      this.setState({ selectedPlan: item });
                    } else {
                      this.setState({ selectedPlan: {}});
                    }
                  }}
                />}
              </div>
              <div style={style.text}>
                Life Assured: {item.iName}
              </div>
              <div style={style.text}>
                Paymode: {premiumType.paymentMode}
              </div>
              <div style={style.text}>
                Currency: {item.currency}
              </div>
              <div style={style.premium}>
                {'Total ' + premiumType.paymentMode + ' Premium: ' + window.getCurrencyByCcy(premiumType.premium, 'SGD', 2)}
              </div>
            </Card>
          );
        })}
      </div>
    );
  }

  isDoneDisabled() {
    // Keep for future
    // let isDoneDisabled = true;
    // const { data } = this.state;
    // if (!_.isEmpty(data)) {
    //   data.forEach(item => {
    //     if (item.checked) {
    //       isDoneDisabled = false;
    //     }
    //   });
    // return isDoneDisabled;
    // }
    return false;
  }

  _closeRPListDialog() {
    this.setState({
      openRPplanDialog: false
    });
  }

  _getRPListDialog() {
    const {muiTheme} = this.context;
    let fontColor = muiTheme.palette.primary1Color;
    let { openRPplanDialog } = this.state;
    let title = (
      <div style={{padding: 0, margin: 0, background: '#FAFAFA' }}>
        <Toolbar className={styles.Toolbar} style={{
            borderBottom: '1px solid #cac4c4', boxShadow: '0px 2px 4px #cac4c4', backgroundColor: '#FAFAFA'
          }}
        >
          <ToolbarGroup style={{ paddingLeft: '16px' }}>
            <IconButton onTouchTap={() => this._closeRPListDialog()}>
              {getIcon('close', fontColor)}
            </IconButton>
          </ToolbarGroup>
        </Toolbar>
        <div className={styles.Divider} />
      </div>
    );

    return (
      <Dialog
          autoScrollBodyContent
          repositionOnUpdate={false}
          autoDetectWindowHeight={false}
          modal={false}
          bodyStyle={{ overflow: 'auto', padding: 0 }}
          open={openRPplanDialog}
          title={title}
      >
        <div
          style={{textAlign: 'center', color: fontColor, padding: '10px'}}
          dangerouslySetInnerHTML={{__html: rplists}}
        />
      </Dialog>
    );
  }

  render() {
    const { muiTheme } = this.context;
    const isAllDim = () => {
      const { data } = this.props;
      for (let i = 0; i < data.length; i++) {
        if (data[i].isDim === false) {
          return false;
        }
      }
      return true;
    };

    const titleText = (_.isEmpty(this.props.data) || isAllDim()) ? "No eligible Regular Premium plans" : "";
    const title = (
      <div style={{ padding: 0, margin: 0, background: '#FAFAFA' }}>
        <Toolbar className={styles.Toolbar} style={this.getStyles().toolBar}>
          <ToolbarGroup style={{ paddingLeft: '16px' }}>
            <IconButton onTouchTap={() => this.closeDialog()}>
              {getIcon('close', muiTheme.palette.primary1Color)}
            </IconButton>
            <ToolbarTitle text={titleText} style={this.getStyles().toolBarTitle} />
          </ToolbarGroup>
          <ToolbarGroup style={{ display: 'flex', minHeight: '0px', paddingRight: 16, paddingTop: 2 }}>
            <FlatButton label={'Eligible RP Plans'} onTouchTap={() => this.setState({openRPplanDialog: true})} labelStyle={this.getStyles().toolBarActionFlatBtn} />
            <FlatButton disabled={this.isDoneDisabled()} label={'Next'} onTouchTap={() => this.props.done(this.state.selectedPlan)} labelStyle={this.isDoneDisabled() ? this.getStyles().toolBarActionDisabledFlatBtn : this.getStyles().toolBarActionFlatBtn} />
          </ToolbarGroup>
        </Toolbar>
        <div className={styles.Divider} />
      </div>
    );

    return (
      <div>
        <Dialog
          open={this.props.open}
          title={title}
          contentStyle={{ width: '90%', height: '90%', maxWidth: 'none', maxHeight: '90%' }}
          autoScrollBodyContent
        >
          <div style={{
            color: muiTheme.palette.primary2Color,
            fontSize: '16px',
            paddingTop: '15px',
            paddingBottom: '15px'
          }}>
            Payment Frequency of Bundled Product must be Regular Premium (Annual). Currency of Bundled Product must be in SGD.
            <br />
            Only one Regular Premium product can be bundled with AXA Quick Saver.
        </div>
        {this.renderCheckList()}
        {this._getRPListDialog()}
        </Dialog>

        {this.state.warningDialogOpen && <Dialog
          title="Warning"
          actions={[
            <FlatButton
              label="OK"
              primary
              onTouchTap={() => {
                this.setState({ warningDialogOpen: false });
              }}
            />
          ]}
          modal
          open={this.state.warningDialogOpen}
        >
          {this.state.errorMsg}
        </Dialog>}
      </div>
    );
  }
}

