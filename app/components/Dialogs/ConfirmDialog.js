import React, { PropTypes } from 'react';
import { Dialog, FlatButton } from 'material-ui';

import EABComponent from '../Component';

export default class ConfirmDialog extends EABComponent {

  static propTypes = {
    open: PropTypes.bool,
    confirmMsg: PropTypes.string,
    onClose: PropTypes.func,
    onConfirm: PropTypes.func
  };

  render() {
    const {langMap} = this.context;
    const {open, confirmMsg, onClose, onConfirm} = this.props;
    return (
      <Dialog
        open={open}
        title={window.getLocalizedText(langMap, 'alert.confirm')}
        actions={[
          <FlatButton
            primary
            label={window.getLocalizedText(langMap, 'app.no')}
            onTouchTap={() => onClose && onClose()}
          />,
          <FlatButton
            primary
            label={window.getLocalizedText(langMap, 'app.yes')}
            onTouchTap={() => onConfirm && onConfirm()}
          />
        ]}
      >
        {confirmMsg}
      </Dialog>
    );
  }

}
