import React, {PropTypes} from 'react';
import {Dialog, Toolbar, ToolbarGroup, IconButton, ToolbarTitle, FlatButton, Checkbox, List, ListItem, Divider} from 'material-ui';
import * as _ from 'lodash';

import styles from '../Common.scss';
import EABInputComponent from '../CustomViews/EABInputComponent';
import {getIcon} from '../Icons/index';
import EABTextField from '../CustomViews/TextField.js';

const emailTemplate = {
  'id': 'emailAddr',
  'title': {
    'en': 'Email',
    'zh-Hant': '電子郵件'
  },
  'type': 'text',
  'subType': 'email',
  'maxLength': 50
};

export default class EmailDialog extends EABInputComponent {

  static propTypes = {
    open: PropTypes.bool,
    onSave: PropTypes.func,
    onClose: PropTypes.func,
    onSkip: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      emailAddr: ''
    });
  }

  componentDidMount() {
    this.init(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.open, nextProps.open)) {
      this.init(nextProps);
    }
  }

  init(props) {
    this.setState({
      emailAddr: props.emailAddr || ''
    });
  }

  closeDialog() {
    let {onClose} = this.props;
    onClose();
  }

  validateEmail(emailAddr){
    return window.validateEmailAddress(emailAddr);
  }

  onSave() {
    const {onSave} = this.props;
    const {emailAddr} = this.state;
    onSave(emailAddr);
  }

  onSkip() {
    const {onSkip} = this.props;
    onSkip();
  }

  textfieldOnChange(value) {
    if (this.state.emailAddr !== value) {
      this.setState({
        emailAddr: value
      });
    }
  }

  textfieldOnBlur(value) {
    if (this.state.emailAddr !== value) {
      this.setState({
        emailAddr: value
      });
    }
  }

  getErrorCode(invalidEmail) {
    return (invalidEmail) ? 401 : undefined;
  }

  render() {
    const {muiTheme, langMap} = this.context;
    const {emailAddr} = this.state;
    let fontColor = muiTheme.palette.primary1Color;
    let disabledSaveBtn = !this.validateEmail(emailAddr);
    let errorCode = emailAddr ? this.getErrorCode(disabledSaveBtn) : 302;
    let title = (
      <div style={{padding: 0, margin: 0, background: '#FAFAFA' }}>
        <Toolbar className={styles.Toolbar} style={this.getStyles().toolBar}>
          <ToolbarGroup style={{ paddingLeft: '16px' }}>
            <IconButton onTouchTap={() => this.closeDialog()}>
              {getIcon('close', fontColor)}
            </IconButton>
            <ToolbarTitle text={window.getLocalizedText(langMap, 'enterEmail')} style={this.getStyles().toolBarTitle} />
          </ToolbarGroup>
          <ToolbarGroup style={{ display: 'flex', minHeight: '0px', paddingRight: 16, paddingTop: 2}}>
            <FlatButton label={window.getLocalizedText(langMap, 'emailaddr.skip')} onTouchTap={() => this.onSkip()} labelStyle={this.getStyles().toolBarActionFlatBtn} style={{margin: '0px'}}/>
            <FlatButton 
              disabled={disabledSaveBtn}
              label={window.getLocalizedText(langMap, 'emailaddr.save')}
              labelStyle={(disabledSaveBtn) ? this.getStyles().toolBarActionDisabledFlatBtn : this.getStyles().toolBarActionFlatBtn}
              style={{margin: '0px'}}
              onTouchTap={() => this.onSave()}
            />
          </ToolbarGroup>
        </Toolbar>
        <div className={styles.Divider} />
      </div>
    );
    return (
      <Dialog
        open={this.props.open}
        autoScrollBodyContent
        title={title}
      >
        <EABTextField
          key = {'EMAIL_ADDR_INPUT_TEXTFIELD'}
          template = {emailTemplate}
          values = {{emailAddr: emailAddr}}
          changedValues = {{emailAddr: emailAddr}}
          error = {{emailAddr: {code: errorCode}}}
          handleChange = {(id, value) => {this.textfieldOnChange(value);}}
          handleBlur = {(value) => {this.textfieldOnBlur(value);}}
        />
      </Dialog>
    );
  }

}
