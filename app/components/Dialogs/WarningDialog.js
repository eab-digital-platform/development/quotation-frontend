import React, { PropTypes } from 'react';
import { Dialog, FlatButton } from 'material-ui';

import EABComponent from '../Component';

export default class WarningDialog extends EABComponent {

  static propTypes = {
    open: PropTypes.bool,
    isError: PropTypes.bool,
    msg: PropTypes.string,
    msgElement: PropTypes.element,
    onClose: PropTypes.func,
    hasTitle: PropTypes.bool,
    isAutoScrollBodyContent: PropTypes.bool
  };

  static defaultProps = {
    isError: false,
    hasTitle: true,
    isAutoScrollBodyContent: false
  };

  render() {
    const {langMap} = this.context;
    const {open, isError, msg, onClose, 
      style,
      hasTitle, msgElement, title, isAutoScrollBodyContent} = this.props;
    return (
      <Dialog
        title={hasTitle || hasTitle === null ? window.getLocalizedText(langMap, isError ? 'app.error' : 'app.warning', isError ? 'ERROR' : 'WARNING') : (title || "")}
        modal
        actions={[
          <FlatButton
            label={window.getLocalizedText(langMap, 'app.ok', 'OK')}
            primary
            onTouchTap={(e) => onClose && onClose(e)}
          />
        ]}
        open={!!open}
        onRequestClose={(e) => onClose && onClose(e)}
        style={style}
        autoScrollBodyContent={isAutoScrollBodyContent}
      >
        {msg ?
          <div dangerouslySetInnerHTML={{__html:msg}} />
          : msgElement}
      </Dialog>
    );
  }

}
