import React from 'react';
import {TextField, Toolbar, ToolbarTitle, ToolbarGroup,  Dialog, Divider, IconButton, FlatButton} from 'material-ui';

import * as _ from 'lodash';
import * as _v from '../../utils/Validation';

import DynColumn from '../DynViews/DynColumn';
import EABComponent from '../Component';
import Appbar from '../CustomViews/AppBar';

import {getIcon} from '../Icons/index';

import {
  saveProfile,
  saveFamilyMember,
  updateContactList
} from '../../actions/client';

import styles from '../Common.scss';

class Main extends EABComponent {

  constructor(props, context) {
      super(props);
      let {values={}, template} = props;

      this.cids = [];

      this.state = Object.assign({}, this.state, {
        values: _.cloneDeep(values),
        changedValues: _.cloneDeep(values),
        error: {},
        template: {},
        showWarnDialog: false,
        appbar: {
          menu: {
            icon: 'close',
            action: ()=>{
              this.closeDialog();
            }
          },
          title: 'Client Profile',
          itemsIndex: 0,
          items:[
            [{
              type: 'flatButton',
              title: {
                "en": "SAVE"
              },
              disabled: true
            }],
            [{
              type: 'flatButton',
              title: {
                "en": "SAVE"
              },
              action: ()=>{
                this.submit(false, true);
              }
            }]
          ]
        }
      })
  };

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
  }

  componentWillUnmount() {
    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
      if(this.unsubscribe){
        let values = this.getValues(this.cids);
        if(!window.isEqual(values, this.state.values)){
          this.setState({
            template: this.handleTemplate(this.cids),
            values,
            changedValues: _.cloneDeep(values)
          })
        }
      }
  }

  openDialog=(cids)=>{
    let newState = {
      open:true,
      template: this.handleTemplate(cids),
      showWarnDialog: false
    };
    this.cids = cids;
    let {client} = this.context.store.getState();
    let {profile, dependantProfiles} = client;
    newState.values = this.getValues(cids);
    newState.changedValues = _.cloneDeep(newState.values);
    this.validate(newState.changedValues, newState);
  }

  closeDialog=(callback, hasError)=>{
    this.setState({open: false, showWarnDialog: false},()=>{
        if(_.isFunction(callback)){
            callback(hasError)
        }
    });
  }

  addMandatory=(template, mandatoryId)=>{
    if(template.id && mandatoryId.indexOf(template.id)>-1){
      template.mandatory = true
    }
    else if(template.key && mandatoryId.indexOf(template.key)>-1){
      template.mandatory = true
    }
    if(template.items){
      _.forEach(template.items, item=>this.addMandatory(item, mandatoryId));
    }
  }

  getValues=(cids)=>{
    let {client} = this.context.store.getState();
    let {profile, dependantProfiles} = client;
    let values = {[profile.cid]: _.cloneDeep(profile)};
    _.forEach(cids, cid=>{
        if(cid != profile.cid){
            values[cid] = _.cloneDeep(dependantProfiles[cid]);
        }
    })
    return values;
  }

  handleTemplate=(cids)=>{
    let {store} = this.context;
    let {pMandatoryId, iMandatoryId} = this.props;
    let {client} = store.getState();
    let {profile, dependantProfiles} = client;
    let {editDialog} = store.getState().client.template;
    let proposerTemplate = _.cloneDeep(editDialog);
    let insuredTemplate = _.cloneDeep(editDialog);
    
    if(!isEmpty(pMandatoryId)){
      this.addMandatory(proposerTemplate, pMandatoryId);
    }
    if(!isEmpty(iMandatoryId)){
      this.addMandatory(insuredTemplate, iMandatoryId);
    }
    let proposerItems=[], insuredItems = [];
    _.forEach(cids, cid=>{
        if(cid===profile.cid){
          proposerItems.push({
            id: cid,
            title: {
                en: profile.fullName
            },
            type: "menuItem",
            items: proposerTemplate.items
          })
        }
        else {
          insuredItems.push({
            id: cid,
            title: {
                en: dependantProfiles[cid].fullName
            },
            type: "menuItem",
            items: insuredTemplate.items
          })
        }

    });
    let template = {
        type: "layout",
        items: [{
            type: "menu",
            key: "client-menu",
            items: [{
                type: "menuItemGroup",
                title: {
                    "en": "Client"
                },
                items: _.concat(proposerItems, insuredItems)
            }]
        }]
    }
    
    return template;
  }

  validateValues=(cids)=>{
    let {langMap, optionsMap} = this.context;

    let template = this.handleTemplate(cids);

    this.cids = cids;
    let {client} = this.context.store.getState();
    let {profile} = client;
    let changedValues = this.getValues(cids);

    let error = {};
    let validRefValues = {isApp: this.props.isApp};
    if (this.props.isFamily){
      validRefValues.profile = _.cloneDeep(profile);
    }
    _v.exec(template, optionsMap, langMap, changedValues, null, validRefValues, error);
    return !error.code;
  }

  validate=(changedValues, newState={})=>{
    let {langMap, optionsMap, store} = this.context;
    let {profile: cProfile} = store.getState().client;
    let {isAllowSave = false} = this.props;
    if(isEmpty(newState)){
      newState.template = this.state.template;
      newState.values = _.cloneDeep(this.state.values);
    }
    let {template, values} = newState;

    let error = newState.error = {};
    let validRefValues = {isApp: this.props.isApp};
    if(this.props.isFamily){
      validRefValues.profile = cProfile;
    }
    _v.exec(template, optionsMap, langMap, changedValues, null, validRefValues, error)
    let valid = false;
    for(var i in this.cids){
      if(!_.get(error, `${this.cids[i]}.code`)){
        valid = true;
      }
    }
    let {appbar} = this.state;
    appbar.itemsIndex = valid || isAllowSave? 1: 0;
    newState.appbar = appbar;
    this.setState(newState);
    return valid;
  }

  submit=(confirm, isClose, changeMenu)=>{
    let {changedValues, error = {}} = this.state;
    let {profile} = this.context.store.getState().client;
    let {onSubmit} = this.props;

    let callback = ()=>{
      if(isClose){
        this.closeDialog(onSubmit, error.code);
      }
      else {
          changeMenu();
      }
    }

    if(changedValues.selectedMenuId == profile.cid){
        saveProfile(this.context, changedValues[profile.cid], false, confirm, (code)=>{
            if(code){
                this.setState({
                    errorCode: code,
                    showWarnDialog: true,
                    isClose,
                    changeMenu
                })
            }
            else {
                updateContactList(this.context, false, callback);
            }
        });
    }
    else {
        saveFamilyMember(this.context, changedValues.selectedMenuId, changedValues[changedValues.selectedMenuId], confirm, (code)=>{
            if(code){
                this.setState({
                    errorCode: code,
                    showWarnDialog: true,
                    isClose,
                    changeMenu
                })
            }
            else {
                updateContactList(this.context, false, callback);
            }
        });
    }
  }

  render() {
    let {muiTheme, langMap, lang} = this.context;
    let {open, values, changedValues, error, template, appbar, errorCode, showWarnDialog} = this.state;
    let {onDelete} = this.props;

    let iconColor = muiTheme.palette.alternateTextColor;
    let _style = {paddingLeft: '16px'}

    const updWarnActions = [
      (
        <FlatButton
          key="profileDialog-updWarn-cancel-btn"
          label={getLocalizedText(langMap, "BUTTON.CANCEL", "Cancel")}
          onTouchTap={()=>{
            this.setState({
              showWarnDialog: false,
              changedValues: _.cloneDeep(this.state.values),
            })
          }}
        />
      ),
      (
        <FlatButton
          key="profileDialog-updWarn-confirm-btn"
          label={getLocalizedText(langMap, "BUTTON.CONFIRM", "Confirm")}
          onTouchTap={()=>{
            this.submit(true, this.state.isClose, this.state.changeMenu);
          }}
        />
      )
    ]

    return (
      <Dialog open={!!open}
        className={styles.FixWrongDialogPadding}
        bodyClassName={styles.MultiClientProfileDialog}
        contentClassName={styles.DialogPaperShield}
        style={{maxWidth: '1080px !important'}}
        title={
         <div style={{padding: '0px'}}>
         <Appbar ref={ref=>{this.appbar=ref}} showShadow={false} template={appbar} style={{background: '#FAFAFA'}}/>
           <div className={styles.Divider}/>
          </div>
        }
        autoScrollBodyContent={true}
        autoDetectWindowHeight={true}
        onDrop={(e)=>{e.preventDefault()}}
        onDragOver={(e)=>{e.preventDefault()}}>
        <Dialog open={showWarnDialog} actions={updWarnActions}>
            {_v.getErrorMsg(null, errorCode, lang)}
          </Dialog>
          <DynColumn
            ref={ref=>{this.dynColumn=ref}}
            template={template}
            values={values}
            changedValues={changedValues}
            error={error}
            validate={this.validate}
            customStyle={{maxHeight: '-webkit-fill-available'}}
            onMenuItemChange={(callback)=>{this.submit(false, false, callback)}}
          />
      </Dialog>
    );
  }
}

export default Main;
