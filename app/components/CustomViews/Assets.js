import React from 'react';
import PropTypes from 'prop-types';import EABComponent from '../Component';
import EABInputComponent from './EABInputComponent';
import {IconButton, SelectField, MenuItem} from 'material-ui';
import {getIcon} from '../Icons/index';
import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';
import HorizontalBarSection from './HorizontalBarSection';
import HorizontalBar from './HorizontalBar';
import EABStepper from '../CustomViews/Stepper.js';
import * as _ from 'lodash';
import styles from '../Common.scss';
const assetKey = 'key';
const assetValKey = 'usedAsset';
const assetReturnKey = 'return';
const filterOptions = {
      rPlanning: ['cpfMs'],
      fiProtection: ['cpfMs'],
      ciProtection: ['cpfOa', 'cpfSa', 'srs', 'cpfMs'],
      psGoals: ['cpfOa', 'cpfSa', 'srs', 'cpfMs'],
      ePlanning: ['cpfOa', 'cpfSa', 'srs', 'cpfMs']
    }

const getOptions=(options, existedOption, cKey, sid)=>{
  return options.filter((option)=>{
    let {value, assetValue, usedAssets} = option;
    return (existedOption.indexOf(value)==-1 || value == cKey) 
      && (assetValue - usedAssets > 0 || existedOption.indexOf(value) > -1 || value == cKey)
  })
}

class AssetSection extends EABComponent {
  constructor(props){
    super(props);
    let {sectionId, changedValues, existedOption, options} = props;
    let selectedId = changedValues[assetKey];
    this.state = Object.assign({}, this.state, {
      selectedId,
      options: getOptions(options, existedOption, selectedId, sectionId),
      assetValue: changedValues[assetValKey] || 0,
      calRate: props.changedValues[assetReturnKey] || 0
    })
  }

  selOptionbyId(selectedId, options){
    return options.filter((option)=>{ return option.value == selectedId})[0];
  }

  componentWillReceiveProps(nextProps){  
    let {sectionId, changedValues, existedOption, options} = nextProps;
    let selectedId = changedValues[assetKey];
    this.setState({
      selectedId:nextProps.changedValues[assetKey],
      options: getOptions(options, existedOption, selectedId, sectionId),
      assetValue: nextProps.changedValues[assetValKey] || 0,
      calRate: nextProps.changedValues[assetReturnKey] || 0
    })
  }

  render(){
    let {changedValues, options, id, values, assetthis, existedOption, onItemChange, yrofInvest} = this.props;
    let {selectedId} = this.state;
    let {lang, muiTheme} = this.context;
    let {
      template
    } = this.context.store.getState().needs;
    var self = this;
    let iconColor = muiTheme.palette.alertColor;

    let handleChange = (id, value)=> {
      
      changedValues[id] = value;
 
      let assetValue =0;
      let calRate =0;
      //assetValue = value;

      Object.keys(changedValues).map((key) =>{
        if (key == assetValKey) {
          assetValue = changedValues[key];
        } else if (key == assetReturnKey) {
          calRate = changedValues[key];
        }
      })
      
      if (calRate && calRate !==0 && this.props.yrofInvest && this.props.yrofInvest !==0) {
        changedValues['calAsset'] = assetValue * Math.pow((1 + (calRate / 100)), this.props.yrofInvest);
      } else {
        changedValues['calAsset'] = assetValue;
      }
    
      this.setState({assetValue, calRate, changedValues});
      if(_.isFunction(onItemChange))
        onItemChange();
      
      if(_.isFunction(assetthis.updateDynNeedPage))
        assetthis.updateDynNeedPage();      
    }

    let updateAssetSection = (value, newOptions) =>{
      /**
       * Chnage the state of Options minus the used Assets to itself, as it changed the selected id
       * Used precentage set to zero
       */
      
      /**if (_.isUndefined(changedValues[assetKey])){
        changedValues[assetKey] = value;
        this.setState({selectedId: value, changedValues: changedValues});
        return;
      }*/
      if (changedValues[assetKey] == value) return;
      newOptions.map((option)=>{ if (option.value == changedValues[assetKey]) option.usedAssets -= changedValues[assetValKey] });
      changedValues[assetValKey] = 0;
      changedValues[assetKey] = value;
      this.setState({
        selectedId: value,
        changedValues: changedValues,
        options: newOptions
      })

      if(_.isFunction(onItemChange))
        onItemChange();

      if(_.isFunction(assetthis.updateExistedAsset))
        assetthis.updateExistedAsset(value, this.state.selectedId);

    }

    let tempOptions = this.state.options;
    let selOption = this.state.options.find((option)=>{ return option.value == this.state.selectedId});
    let hbitem ={
								id: assetValKey,
								type: "horizontalBar",
								subType: "currency",
								min: 0,
								max: (selOption) ? selOption.assetValue :  0,
                valuetoValidate: (selOption) ? (selOption.assetValue - selOption.usedAssets) : undefined,
                usedPrecentage: (selOption) ? (selOption.usedAssets) : 0,
            };
    let selectField =  <SelectField id={'selectfield' + id} hintText='Please select an Asset' value={this.state.selectedId} onChange= { function(e, index, value) {updateAssetSection(value, tempOptions);}}>
                          {
                            this.state.options.map((option, index)=>{
                              return (
                                <MenuItem key={index} value={option.value} primaryText={getLocalText(lang, option.title)} />
                              )
                            })
                          }
                        </SelectField>;

              let stepperitem ={
                id: assetReturnKey,
								type: "stepper",
								title: {
									"en": "Rate of return (%)",
								},
                mandatory: true,
                subType: "allowNegativepercentage",
                min: -100,
                max: 100,
                interval: 0.5,
                decimalNeeded: 2,
                allowNegative: true
            };
    let _assetValue = getCurrency(Number(this.state.assetValue), '$', 2) || getCurrency(0, '$', 2);
    if(this.state.calRate && this.state.calRate != 0 && this.state.assetValue){
      changedValues['calAsset'] = (Number( (yrofInvest && yrofInvest != 0) ? this.state.assetValue * Math.pow((1 + (this.state.calRate / 100)), yrofInvest) : this.state.assetValue)) || 0;
      _assetValue = getCurrency(changedValues['calAsset'], '$', 2);
    }
    return (
      <div className={styles.FlexColumnNoWrapContainer}>
        <div className={styles.alignRight} style={{alignSelf: 'flex-end', marginBottom: '-10px'}}>Current Value (PV): { (selOption) ? getCurrency(Number(selOption.assetValue), '$', 0)  : getCurrency(0, '$', 0) }</div>
        <div className={styles.FlexNoWrapContainer}>
          <IconButton key={'removebtn' + id} id={'AssetremoveBtn'} style={{alignSelf: 'center', zIndex: 0, bottom: (selOption && this.state.assetValue > selOption.assetValue) ? '12px': undefined }} onTouchTap={(e) => {e.preventDefault(); assetthis.removeAsset(id)}}>{getIcon('RemoveCircleOutline', iconColor)}</IconButton>
          <HorizontalBar
            key={'horizontalbar' + id}
            template={hbitem}
            changedValues={changedValues}
            handleChange={handleChange}
            values={values}
            selectField = {selectField}
            errorMsg={this.props.errorMsg}
          />
        </div>
        {(this.props.showStepper) ?
          <div className={styles.FlexNoWrapContainer}>
            <EABStepper
              key={'stepper'+id}
              template={stepperitem}
              values={values}
              changedValues={changedValues}
              rootValues={values}
              handleChange = { handleChange }
              headerContainerStyle = {styles.NoPaddingheaderTitleContainer }
              headerStyle = {styles.NoPaddingheaderTitle}
            />
          </div>
          :
          undefined
        }
        <div className={styles.FlexNoWrapContainer}>
           <div className={styles.NonInputValue + ' ' + styles.alignCenter + ' ' + styles.marginTop}>Asset value: {_assetValue}</div>
        </div>
      </div>
    )
  }
}

AssetSection.propTypes={
  changedValues: PropTypes.object,
  options: PropTypes.array
}

class Asset extends EABInputComponent{
  constructor(props) {
      super(props);
      let cValue = _.cloneDeep(this.getValue(_.at(this.props.changedValues, 'assets')[0]));
      this.state = Object.assign( {}, this.state, {
        options: props.options,
        eOptions: this.getExistedOption(cValue)
      });
  }

  componentWillReceiveProps(nextProps){
    if (!_.isEqual(this.props, nextProps)){
      let cValue = _.cloneDeep(_.at(nextProps, 'changedValues.assets')[0]);
      this.setState({
        options: nextProps.options,
        eOptions: (cValue) ? this.getExistedOption(cValue) : []
      })
    }
  }

  removeAsset = (id) =>{
    let cValue = this.getValue(_.at(this.props.changedValues, 'assets')[0] || []);
    cValue.splice(id, 1);
    this.requestChange(cValue);
  }

  removeMultiAsset = (cV) => {
    this.requestChange(cV);
  }

  updateDynNeedPage = () =>{
    let cValue = this.getValue(_.at(this.props.changedValues, 'assets')[0] || []);
    this.requestChange(cValue);
  }

  addAsset = (options) => {
    let cValue = this.getValue(_.at(this.props.changedValues, 'assets')[0] || []);
    let tempOption = _.filter(options, option => {return _.isNumber(option.assetValue) && option.assetValue > 0}).length;
    if (cValue.length < tempOption) {
      cValue.push({});
      this.requestChange(cValue);
    }
  }

  getExistedOption = (cValue) =>{
    let eOption = [];
    _.forEach(cValue, (val)=> {
      if (val.key) eOption.push(val.key);
    })
    return eOption;
  }

  updateExistedAsset = (optionId, oldValue) =>{
    let exOption = Object.assign([], this.state.eOptions);
    let tempArr;
    if (!_.isEqual(optionId, oldValue)){
      tempArr = _.filter(exOption, (option) =>{
        return option != oldValue
      })
      exOption = tempArr;
    }
    
    if(_.isUndefined(_.find(exOption, (o) => {return o.id == optionId})))
      exOption.push(optionId);
    this.setState({eOptions: exOption});

    let cValue = this.getValue(_.at(this.props.changedValues, 'assets')[0] || []);
    this.requestChange(cValue);
  }

  renderItems = (items, values, cValue, relationship,onItemChange, handleChange, errorMsg, filteredOptions=[]) => {
    var self = this;
    let {options, eOptions=[]} = this.state;
    let {needsId:sId, showStepper, needsId} = this.props.template;
    let removeInvalidcValue =false;
    let modifiedcVal = [];

    _.map(cValue, (value, index, cVal)=>{
      if (eOptions.length < filteredOptions.length || (eOptions.length === filteredOptions.length && !_.isUndefined(value.key))  ){
        items.push(
          <AssetSection sectionId={sId} key={'AssetSection'+index} values={values} 
            changedValues={value} options={filteredOptions} id={index} assetthis={self} 
            existedOption={eOptions} onItemChange={onItemChange} yrofInvest={this.props.yrofInvest} 
            showStepper={showStepper} errorMsg={errorMsg}/>
        )
      } else {
        removeInvalidcValue = true;
      } 
    })
    
    if (removeInvalidcValue){
      modifiedcVal = _.filter(cValue, obj => !_.isUndefined(obj.key))
      this.removeMultiAsset(modifiedcVal);
    }
    
  }
  
  render() {
    let {lang, langMap, muiTheme, store, validRefValues} = this.context;
    let {template,rootValues, values, changedValues, handleChange, relationship,id, options, onItemChange} = this.props;
    let {title,  mandatory, hints, needsId} = template;

    let cValue = this.getValue(_.at(changedValues, 'assets')[0]);

    let items = [];
    let errorMsg = this.getErrMsg();
    let filteredOptions = _.filter(this.state.options, (option)=> {
      return _.indexOf(filterOptions[needsId], option.value) == -1
    });

    this.renderItems(items, values, cValue, relationship, onItemChange, handleChange, errorMsg, filteredOptions);
   
    let iconColor = muiTheme.palette.primary2Color;

    let hasOptions = _.get(getOptions(filteredOptions, this.state.eOptions, '' ,''), 'length') > 0;
    //if (!hasOptions && !items.length) {
      hints = 'Please add the assets under Financial Evaluation.';
    //}
    
    return (
      <div className={styles.FlexColumnNoWrapContainer}>
        <HeaderTitle title={title} mandatory={mandatory} hints={hints}/>
        {items}
        {hasOptions ? 
        <div className={styles.FlexNoWrapContainer}>
          <IconButton key={'addbutton'} style={{alignSelf: 'center', zIndex:0}} onTouchTap={(e) => {e.preventDefault(); this.addAsset(options)}}>{getIcon('addCircleOutline', iconColor)}</IconButton>
          <div className={styles.headerTitleStyle}>{getLocalizedText(langMap, 'Add additional value of assets to be used')}</div>
        </div>
        :
        !items.length ? <span className={styles.NonInputValue + ' ' + styles.alignCenter}>$0</span> : null
        }
      </div>
    );
  }
}

Asset.propTypes={
  relationship: PropTypes.string
}

export default Asset;