import React from 'react';
import {Checkbox} from 'material-ui';
import EABInputComponent from './EABInputComponent';
import ErrorMessage from './ErrorMessage';
import styles from '../Common.scss';
import _assign from 'lodash/fp/assign';
import * as _ from 'lodash';

const checkBoxLabelStyle = {
  // overflow: 'hidden',
  // textOverflow: 'ellipsis',
  // whiteSpace: 'nowrap',
  width: 'calc(100% - 40px)',
  fontSize: 16,
  cursor: 'default',
  margin: '0px'
  // height: 36
}

class EABCheckBox extends EABInputComponent{
  render() {
    let {lang, muiTheme} = this.context;
    let {changedValues} = this.state;
    let {template, width, handleChange, requestChange, requireShowDiff, labelStyle} = this.props;
    let {id, title, content, disabled, value, mandatory, getCheckedValueFromOthers, noStar} = template;

    let cValue = (this.getValue('N') === 'Y')
    let self = this;
    if (content) {
      content = getLocalText(lang, content);
      let _arryToReplce = content.split(/[<>]/);

      let strToDisplay = content;
      let arryToReplce = [];
      let subIndex = 1;
      while(subIndex < _arryToReplce.length){
        arryToReplce.push(_arryToReplce[subIndex]);
        subIndex = subIndex + 2;
      }

      for(let sub = 0; sub < arryToReplce.length; sub ++){
        let replceStr = arryToReplce[sub];
        let _cValues = changedValues;
        let hasAt = "";
        if(replceStr.indexOf("@") != -1){
          _cValues = this.props.tabValues;
          replceStr = replceStr.replace("@", "");
          hasAt = "@";
        }


        let subValue = getValueFmRootByRef(_cValues, replceStr, null);
        if (subValue || !(replceStr == 'br/' || replceStr == 'p' || replceStr == '/p' || replceStr == 'b' || replceStr == '/b' || replceStr.indexOf('span') > -1)) {
          // if(subValue == 'en')
          //   subValue = 'English';
          let options = getFieldFmTemplate(this.props.rootTemplate, replceStr.split('/'), 0, 'options');

          if (options && options.length) {
            for (var o in options) {
              if (options[o].value == subValue) {
                subValue = getLocalText(lang, options[o].title);
                break;
              }
            }
          }

          if(!subValue)
            subValue = '';

          strToDisplay = strToDisplay.replace("<" + hasAt + replceStr + ">", subValue);
        }
      }
      if (mandatory) {
        strToDisplay += "<span class='"+styles.mandatoryStar+"'>*</span>"
      }
      title = <div dangerouslySetInnerHTML={{__html:strToDisplay}}></div>
    } else {
      if(noStar){
        title = this.getFieldTitleNoStar(mandatory, title);
      }else{
        title = this.getFieldTitle(mandatory, title);
      }
    }

    if (getCheckedValueFromOthers) {
      const {othersValue, checkedValue} = getCheckedValueFromOthers;
      const idArr = getCheckedValueFromOthers.id.split(',');
      _.forEach(idArr, otherId => {
        if (changedValues[otherId] && changedValues[otherId] !== othersValue) {
          changedValues[id] = cValue = checkedValue;
          return false;
        }
      });
    }

    var errorMsg = this.getErrMsg();
    let erMsg = (_.isString(errorMsg) && errorMsg!= '')? (<div style={{width:'100%'}}>{_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg} style={{top: '0px', left: '0px', textAlign: 'left'}}/>: null}</div>) : null;

    const _labelStyle = labelStyle ? _assign(checkBoxLabelStyle, labelStyle, {}) : checkBoxLabelStyle;
    return (
      <div className={styles.DetailsItem +' '+styles.CheckBox}>
        <Checkbox
            key={id}
            label={title}
            labelStyle={_assign(_labelStyle, _labelStyle.color ? {} : {color: '#000'})}
            className={styles.CheckBox}
            disabled={disabled}
            checked={cValue}
            inputStyle={{width: '24px', height: '24px', cursor: disabled ? 'not-allowed' : 'pointer' }}
            style={{ cursor: 'default' }}
            onCheck={
              (e, val) => {
                self.requestChange(val);
              }
            }
            />
          {erMsg}
        </div>
    )
  }
}

export default EABCheckBox;
