import React from 'react';
import {FlatButton, IconButton} from 'material-ui';
import appTheme from '../../theme/appBaseTheme.js';
import {getIcon} from '../Icons/index';

import EABInputComponent from './EABInputComponent.js';

class EABList extends EABInputComponent{

  constructor(props){
    super(props);
    this.state = Object.assign({}, this.state, {
      allowAdd: true
    })
  }


  add=()=>{
   let listValues = this.state.changedValues[this.props.template.id];
   let newListValue = {};
   listValues.push(newListValue);
   this.forceUpdate();
  }

  render() {
    let {muiTheme} = this.context;
    let {changedValues} = this.state;
    let {template, width, values, error, inDialog, handleChange, rootValues, index, renderItemsFunc} = this.props;
    //let allowAdd = template.allowAdd || this.state.allowAdd || true;
    let allowAdd = true;
    let listValues = this.getValue([{}])

    let rows = [];
    _.forEach(listValues, (listValues, index)=>{
      _.forEach(template.items, (item, iIndex)=>{
        renderItemsFunc(rows, item, listValues, listValues, error, index*10 + iIndex);
      });
    })

    if (allowAdd) {
          rows.push(
            <IconButton onClick={this.add} key={"add-bt-" + template.id}>
              {getIcon('add', muiTheme.palette.themeGreen)}
            </IconButton>
          )
    }

    return (
      <div>{rows}</div>
    )
  }    
}

export default EABList;
