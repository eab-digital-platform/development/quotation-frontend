import React from 'react';
import {RadioButton, RadioButtonGroup} from 'material-ui';
import EABInputComponent from './EABInputComponent';
import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';
import EABToolTips from './ToolTips';

import styles from '../Common.scss';
import _forEach from 'lodash/fp/forEach';
import _get from 'lodash/fp/get';
import _getOr from 'lodash/fp/getOr';
import _pipe from 'lodash/fp/pipe';
import _split from 'lodash/fp/split';

class EABRadioBoxGroup  extends EABInputComponent{
  getCheckBoxValues = (cbValues) =>{
    return cbValues.join(',');
  }

  highlightTitle=(title, tips)=>{
    return (<span>
      {
        _pipe([
          _split('{{'),
          _get(0)
        ])(title)
      }
      <span style={{textDecoration : 'underline'}}>
        {
          _pipe([
            _split('{{'),
            _get(1),
            _split('}}'),
            _get(0)
          ])(title)
        }
      </span>
      {
        _pipe([
          _split('}}'),
          _get(1)
        ])(title)
      }
      {tips?<EABToolTips hints={tips.msg} iconType={tips.icon} Iconstyle={{padding: 0, height: '24px'}}/>:""}
    </span>);
  }

  render() {
    let {validRefValues, muiTheme} = this.context;
    let {changedValues, options} = this.state
    let {template, handleChange, style, rootValues, values} = this.props;
    let {id, title, hints, mandatory, hashAfterMandatory, value, subType, placeholder, presentation, horizontal, align, useHeaderTitle, needToHighlight, tips, noPadding} = template;
    let disabled = template.disabled || this.props.disabled;

    let cValue = this.getValue('');
    let errorMsg = this.getErrMsg();
    let radios = [];
    _forEach((option, j)=>{
      const needToHighlight = _getOr(false, 'needToHighlight', option);
      radios.push(
        <RadioButton
          key = {'rb' + id + j}
          className = {styles.CheckBox}
          style={{padding: horizontal?'0 24px 0 0':'8px 0', cursor: 'default', width: horizontal?'auto':'100%'}}
          inputStyle={{width: '24px', height: '24px', margin: '0px', cursor: disabled ? 'not-allowed' : 'pointer'}}
          labelStyle={{cursor: 'default', margin: '0px', color: '#000'}}
          value={option.value}
          label={needToHighlight ? this.highlightTitle(option.title) : window.getLocalText('en', option.title)}
          disabled={disabled}
        />
      );
    }, options);

    if (template.note) {
      var noteView = <p style={{'color':'red'}}>{template.note}</p>
    }

    let customClass;
    let headerClass;
    if (align) {
      customClass = styles.RbCbheaderTitleAlignLeftContainer;
      headerClass = styles.headerTitleAlignLeftContainer+ ' ' + styles.NoPaddingheaderTitle;
    }

    if (align && horizontal) {
      headerClass = styles.RbheaderTitleAlignLeft;
    }

    let erMsg = (_.isString(errorMsg) && errorMsg!= '')? (<div style={{width:'100%'}}>{_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg} style={{top: '0px', left: '0px', textAlign: 'left'}}/>: null}</div>) : null;
    let defaultStyle = Object.assign({}, this.props.defaultStyle, horizontal ? {paddingTop : '8px'} : {});
    if (useHeaderTitle) {
      title = <HeaderTitle template={template} defaultStyle={this.props.defaultStyle} title={title} mandatory={mandatory} hashAfterMandatory={hashAfterMandatory} hints={hints} customStyleClass={customClass} customHeaderStyle={headerClass}/>;     
    } else if (needToHighlight) {
      title = this.highlightTitle(title, tips);
    } else {
      title = this.getFieldTitle(mandatory, title, tips);
    }

    // if (tips) {
    //   title = <div style={{display:'flex'}}>{title}<EABToolTips hints={tips.msg} iconType={tips.icon} Iconstyle={{padding: 0, height: '24px'}}/></div>;
    // }

    return (
      <div className={this.checkDiff()} style={{display: 'flex', minHeight: '0px', flexDirection: (horizontal) ? 'row': 'column', flexWrap: (horizontal) ? 'wrap' : 'no-wrap', alignItems: (horizontal) ? 'flex-start': undefined, padding: (noPadding ? '0px 0px 8px' : '8px 0px')}}>
        <div style={
          horizontal ? {width: 'calc(100% - 250px)', minWidth: '420px', padding: '8px 48px 8px 0', lineHeight: '24px'}
           : {width: 'calc(100% - 24px)', padding: (noPadding ? '0' : '12px 0'), lineHeight: '24px'} }>
          {title}
          {noteView}
          {erMsg}
        </div>

        <RadioButtonGroup
          key={id}
          name = {id}
          className={'RadioBoxGroup'+ (presentation?' '+presentation:'')}
          defaultSelected={cValue}
          valueSelected={cValue}
          style={Object.assign(defaultStyle, {display: 'flex', minHeight: '0px', flexDirection: (horizontal) ? 'row': 'column', alignItems: 'center', flexGrow: 1})}
          onChange={(e, value)=>{
            this.requestChange(value);
          }}
        >
          {radios}
        </RadioButtonGroup>


    </div>
    );
  }

}

export default EABRadioBoxGroup;
