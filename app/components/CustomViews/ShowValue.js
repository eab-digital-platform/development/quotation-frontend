import React from 'react';
import EABInputComponent from './EABInputComponent';
import styles from '../Common.scss';
import HeaderTitle from './HeaderTitle';
import ErrorMessage from './ErrorMessage';

class ShowValue extends EABInputComponent{
  constructor(props){
    super(props);
    this.state = Object.assign( {}, this.state, {
      isFirstInit: true
    });
  }

  render() {
    let {validRefValues} = this.context;
    let {changedValues} = this.state;
    let {
      template,
      handleChange,
      style,
      values,
      index,
      rootValues,
      labelStyle
    } = this.props;

    let {
      id,
      type,
      title,
      hints,
      mandatory,
      subType,
      disabled,
      format,
      decimalNeeded,
      hboxStyle,
      noSectionPadding
    } = template;

    let { lang } = this.context

    let cValue = changedValues[id] != undefined?changedValues[id]:this.getValue("");
    if(subType === "currency"){
      cValue = getCurrency(Number(cValue), '$', decimalNeeded || 0);
    }
    let display = format? getLocalText(lang, format).replace("%s", cValue): cValue.toString();

    let errorMsg = this.getErrMsg();

    let className = (noSectionPadding) ? styles.alignCenter : styles.SectionPadding + ' ' + styles.alignCenter;
    return(
      <div className={className} style={{lineHeight: (hboxStyle=== true) ? '62px': undefined}}>
          {!_.isEmpty(title) ? <HeaderTitle title={title} mandatory={mandatory} hints={hints}/> : undefined}
          {(_.isString(errorMsg) && !isEmpty(errorMsg))?<ErrorMessage template={template} message={errorMsg}/>: null}
          <span className={styles.NonInputValue + ' ' + styles.alignCenter}>{display}</span>
      </div>
    )

  }


}

export default ShowValue;