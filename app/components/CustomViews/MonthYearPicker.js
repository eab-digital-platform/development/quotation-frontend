import React from 'react'
import mui, {MenuItem} from 'material-ui';
import EABInputComponent from './EABInputComponent';
import SelectField from './SelectField';
import styles from '../Common.scss';
import Picker from './PickerField';

class MonthYearPicker extends EABInputComponent{

  constructor(props){
    super(props);
    var template = this.props.template;
    var thisValue = this.props.changedValues[template.id]
    if(thisValue){
      this.state={
        changedValue:{},
        thisValue: thisValue
      }
    }else{
      thisValue = this.props.changedValues[template.id] = " / ";
      var myValue = thisValue.split("/");
      var thisCvalue = {};
      thisCvalue[template.id+"MM"] = myValue[0];
      thisCvalue[template.id+"YY"] = myValue[1];
      this.state={ changedValue:thisCvalue, thisValue:thisValue}
    }

  }

  handleChange = (value, fId) =>{
    var {
      template,
      values,
      width,
      style,
      handleChange
    } = this.props;

     var {
      changedValue,
      muiTheme
    } = this.state;

    changedValue[fId] = value;

    var MY = changedValue[template.id];
    var array = MY.split("/");
    var thisValue = changedValue[template.id + "MM"] + "/" + changedValue[template.id + "YY"];
    this.props.changedValues[template.id] = thisValue;
    this.setState({changedValue: changedValue, thisValue:thisValue})
  }

  handleMChange = (fId, value) =>{
    this.handleChange(value, fId);
  }

   handleYChange = (fId, value) =>{
    this.handleChange(value, fId);
  }
  render() {
    var self = this;
    var {
      template,
      values,
      width,
      style,
      handleChange
    } = this.props;


    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      title,
      mandatory,
      disabled
    } = template;

    var mTemplate = {
      "id":template.id + "MM",
      "title":"MM",
      "type":"picker",
      "options":[
        {
          "value":"01",
          "title":"01"
        },
        {
          "value":"02",
          "title":"02"
        },
        {
          "value":"03",
          "title":"03"
        },
        {
          "value":"04",
          "title":"04"
        },
        {
          "value":"05",
          "title":"05"
        },
        {
          "value":"06",
          "title":"06"
        },
        {
          "value":"07",
          "title":"07"
        },
        {
          "value":"08",
          "title":"08"
        },
        {
          "value":"09",
          "title":"09"
        },
        {
          "value":"10",
          "title":"10"
        },
        {
          "value":"11",
          "title":"11"
        },
        {
          "value":"12",
          "title":"12"
        }

      ]
    }

    var yTemplate = {
      "id":template.id + "YY",
      "title":"YYYY",
      "type":"picker",
      "options":template.options
    }

    var changedValue =  this.state.changedValue;


    return <div
              className={ styles.tableView }
              style={style}>
              <div className = {styles.tableViewItem}>
                <Picker
                template = {mTemplate}
                changedValues ={changedValue}
                handleChange = {this.handleMChange}
                />
              </div>
              <div className = {styles.tableViewItem}>
                <Picker
                template = {yTemplate}
                changedValues ={changedValue}
                handleChange = {this.handleYChange}
                />
              </div>
        </div>;
  }
}

export default MonthYearPicker;
