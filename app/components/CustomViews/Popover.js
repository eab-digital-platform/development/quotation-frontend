import React from 'react';
import {TextField, Popover} from 'material-ui';
import EABInputComponent from './EABInputComponent';
import EABCheckboxGroup from './CheckBoxGroup.js';
import EABTextField from './TextField.js';

import * as _ from 'lodash';
import * as _v from '../../utils/Validation';

import styles from '../Common.scss';

class EABPopover extends EABInputComponent{
  constructor(props){
    super(props);
    this.state = Object.assign( {}, this.state, {
      iosKeyboard:false,
      open: false
    });
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget
    });
  }

  handleOnKeyDodwn = (event) => {
    // This prevents ghost click.
    //event.preventDefault();
    if (event.keyCode === 9){
      this.setState({
        open: false
      });
    }
  }

  handleRequestClose = () =>{
    this.setState({
      open: false,
      iosKeyboard:false
    });
  }

  renderItems = (fields, template, values, changedValues, key) =>{
    let mandatory = false;
    _.forEach(template.items, (item, index)=>{
      if(this.renderItem(fields, item, values, changedValues, index, key)){
        mandatory = true;
      }
    })
    return mandatory;
  }

  renderItem = (items, iTemplate, values, changedValues, index, key) => {
    let {muiTheme, langMap, lang, validRefValues} = this.context;
    let {id='', value, type='', mandatory} = iTemplate;

    // if it is a new change get default value if changed value is empty
    if(id && !checkExist(changedValues, id) && value){
      changedValues[id] = value
    }

    // check show condition
    if (!showCondition(validRefValues, iTemplate, values, changedValues, this.props.rootValues)) {
      return;
    }

    if(!iTemplate.type)
      return null;

    let viewType = _.toUpper(type);
    let __id = key+'_' + index + '_'+ id;

    if (viewType === 'TEXT') {
      items.push(
        <EABTextField
          ref={ref=>{this[id]=ref}}
          key={__id}
          template={ iTemplate }
          index={index}
          values={values}
          changedValues={changedValues}
          rootValues={this.state.values}
          handleChange={this.props.handleChange}
          onFocus={e=>{
            e.preventDefault();
            if (this.isPad()) {
              this.setState({
                iosKeyboard: true
              });
            }
          }}
          onBlur={e=>{
            e.preventDefault();
            if (this.isPad()) {
              this.handleRequestClose();
            }
          }}
          />

      );
    }
    else if (viewType === 'CHECKBOXGROUP') {
      items.push(
        <EABCheckboxGroup
          id={id}
          ref={id}
          key={__id}
          template={iTemplate}
          index={index}
          values={values}
          mode={"P"}
          changedValues={changedValues}
          handleChange={this.props.handleChange}
        />
      )
    }

    return mandatory;
  }

  getItemValue = (items=[], values, changedValues) => {
    let {lang, validRefValues} = this.context;
    let returnValue = []
    items.forEach((item) => {
      if (!showCondition(validRefValues, item, values, changedValues, this.props.rootValues)) {
        changedValues[item.id] = undefined;
      }else {
        let {id='', type=''} = item;
        let itemType = _.toUpper(type)
        let storedValues = changedValues[id];
        if (storedValues) {
          if (itemType == 'CHECKBOXGROUP') {
            // map the values to titles, eg 'zh, en' => 'Mandarin, English'
            // stored values is separeted by comma
            let valArr = storedValues.split(',');
            // filter 'other'
            let titleArr = valArr.filter(t=>t!='other').map((val) => {
              // find the options with the specific value
              let opt = item.options.find(option => option.value == val)
              return checkExist(opt, 'title.'+lang)?opt.title[lang]:'';
            }).filter(k=>k!='');
            if(!isEmpty(titleArr)) {
              returnValue.push(titleArr.join(', '));
            }
          } else if (itemType == 'TEXTFIELD' || itemType == 'TEXT') {
            if(!isEmpty(storedValues)) {
              returnValue.push(storedValues);
            }
          }
        }
      }
    });

    return returnValue.join(', ');
  }

  getItemErrMsg = () =>{
    let {template, error} = this.props;
    let {lang} = this.context;
    let msg = '';
    _.forEach(template.items, i=>{
      if(error[i.id] && error[i.id].code){
        msg = _v.getErrorMsg(template, error[i.id].code, lang)
      }
    })
    return msg;
  }

  isPad = ()=>{
    return /iPad/.test(navigator.platform);
  }
  getTargetOrigin = ()=>{
    if (!this.isPad()){
      return {horizontal: 'left', vertical: 'top'};
    }
    return { vertical: 'top', horizontal: 'left'};
  }

  getAnchorEl = (anchorEl) => {
    let {iosKeyboard} = this.state;
    if (!this.isPad()){
      return anchorEl;
    }
    else if (!iosKeyboard && this.isPad()){
      if (anchorEl){
        let rect = anchorEl.getBoundingClientRect();
        let pos = {
          top: rect.top,
          left: rect.left,
          width: anchorEl.offsetWidth
        };
        let sLeft = pos.left.toString() + 'px';
        let width = pos.width;
        let isPortrait = width < 350 ? true : false;
        if (!isPortrait){
          if (pos.top > 350){
            let sTop = '320px';
            var fixAnchorEl = document.getElementById('fixAnchorEl');
            if (!fixAnchorEl){
              fixAnchorEl = document.createElement('div');
              fixAnchorEl.id = 'fixAnchorEl';
              fixAnchorEl.style.position = 'relative';
              fixAnchorEl.style.width = '435px';
              fixAnchorEl.style.scrollWidth = '435px';
              document.body.appendChild(fixAnchorEl);
            }
            fixAnchorEl.style.left = sLeft;
            fixAnchorEl.style.top = sTop;
            return fixAnchorEl;
          }
        }
        else {
          if (pos.top > 620){
            let sTop = '600px';
            var fixAnchorEl = document.getElementById('fixAnchorEl');
            if (!fixAnchorEl){
              fixAnchorEl = document.createElement('div');
              fixAnchorEl.id = 'fixAnchorEl';
              fixAnchorEl.style.position = 'relative';
              fixAnchorEl.style.width = '435px';
              fixAnchorEl.style.scrollWidth = '435px';
              document.body.appendChild(fixAnchorEl);
            }
            fixAnchorEl.style.left = sLeft;
            fixAnchorEl.style.top = sTop;
            return fixAnchorEl;
          }
        }
      }
      return anchorEl;
    }
    else if (iosKeyboard && this.isPad()){
      if (anchorEl) {
        let rect = anchorEl.getBoundingClientRect();
        let pos = {
          top: rect.top,
          left: rect.left,
          width: anchorEl.offsetWidth
        };
        let sLeft = pos.left.toString() + 'px';
        let width = pos.width;
        let isPortrait = width < 350 ? true : false;
        let sTop = '340px';
        if (isPortrait) {
          if (pos.top > 620) {
            sTop = '430px';
          } else {
            sTop = (pos.top + 80).toString() + 'px';
          }
        }
        var fixAnchorEl = document.getElementById('fixAnchorEl');
        if (!fixAnchorEl){
          fixAnchorEl = document.createElement('div');
          fixAnchorEl.id = 'fixAnchorEl';
          fixAnchorEl.style.position = 'relative';
          fixAnchorEl.style.width = '435px';
          fixAnchorEl.style.scrollWidth = '435px';
          document.body.appendChild(fixAnchorEl);
        }
        fixAnchorEl.style.left = sLeft;
        fixAnchorEl.style.top = sTop;
        return fixAnchorEl;
      }
    }
    return anchorEl;
  }



  render() {
    let {lang, langMap, validRefValues} = this.context;
    let {changedValues, anchorEl,iosKeyboard} = this.state;
    let {style, template, hintText, rootValues, values, handleChange, disabled} = this.props;
    let {id, title, mandatory, value, subType, placeholder, presentation} = template;

    let valueArr = [];
    let cbGroup = []
    let cValue = this.getItemValue(template.items, values, changedValues);
    if (cValue && typeof cValue == 'string') {
      valueArr = cValue.split(',');
    }

    let errorMsg = this.getItemErrMsg();

    // gen items
    let items = []
    let _m = this.renderItems(items, template, values, changedValues, id);

    return (
      <div
        className={ styles.DetailsItem + ' ' + styles.Popover }
        style={style}
      >
        <TextField
          key = {id}
          ref = {id}
          className = { styles.ColumnField }
          hintText= { placeholder?getLocalText(lang, placeholder):null }
          hintStyle= { { bottom:0, fontSize:'12px', lineHeight:'16px' }}
          errorText= { errorMsg }
          floatingLabelText = { this.getFieldTitle(mandatory || _m, title) }
          style= {this.getStyles().columnField}
          errorStyle = {this.getStyles().errorStyle}
          floatingLabelStyle = { this.getStyles().floatingLabelStyle }
          underlineStyle = { {bottom:'22px'} }
          underlineDisabledStyle = { this.getStyles().disabledUnderline }
          value = { cValue }
          onFocus = { this.handleTouchTap }
          onTouchTap = {this.handleTouchTap}
          onKeyDown = {this.handleOnKeyDodwn}
        />

        <Popover
          style={{width:anchorEl && anchorEl.scrollWidth}}
          open={this.state.open}
          anchorEl={this.getAnchorEl(anchorEl)}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={this.getTargetOrigin()}
          onRequestClose={this.handleRequestClose}
          autoCloseWhenOffScreen = {false}
          canAutoPosition = {!this.isPad()}
        >
          {items}
        </Popover>
      </div>
    );
  }

}

export default EABPopover;
