import React from 'react';
import EABInputComponent from './EABInputComponent';
import styles from '../Common.scss';

import * as _ from 'lodash';

class ShowRisk extends EABInputComponent{
  constructor(props){
    super(props);
    this.state = Object.assign( {}, this.state, {
      isFirstInit: true
    });
  }

  render() {
    let {validRefValues} = this.context;
    let {changedValues} = this.state;
    let {
      template,
      handleChange,
      style,
      values,
      index,
      rootValues,
      labelStyle
    } = this.props;

    let {
      id,
      type,
      hints,
      mandatory,
      subType,
      disabled,
      format,
      level,
      title,
      countid
    } = template;

    let { lang } = this.context

    let riskSum = 0;
    countid.map((countValue, index) => {
      riskSum += parseInt(changedValues[countValue]) ? parseInt(changedValues[countValue]):0;
    })
    let riskLevel ="";
    level.map((item, index)=> {
      let range = item.margin.split('-');
      if (riskSum >= parseInt(range[0]) && riskSum <= parseInt(range[1])) {
        riskLevel = item.desc;
      }
    })

    changedValues[id] = riskLevel;
    let errorMsg = this.getErrMsg();

    return(
      <div style={{margin: '0px auto', paddingBottom: '12px', textAlign: 'center'}}>
          {_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg}/>: null}
          <span style={{fontSize: '20px'}}>{title.en} <font color='red'>{riskLevel}</font></span>
      </div>
    )

  }


}

export default ShowRisk;
