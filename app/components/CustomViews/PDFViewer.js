import React from 'react';
import PropTypes from 'prop-types'
import {Paper, CircularProgress} from 'material-ui';

import EABComponent from '../Component';
import styles from '../Common.scss';
import PDFJS from 'pdfjs-dist/build/pdf.js';

class PDFViewer extends EABComponent {

  constructor(props) {
      super(props);

      this.state = Object.assign({}, this.state, {
        size: 0,
        fileSize: 0,
        signCvs:''
      });
  };

  componentDidMount() {
    this.reload(this.props.pdf);
    //this.setTimeLimit(0);
  };

  componentWillReceiveProps(newProps) {
    this.reload(newProps.pdf);

    //attach signature image to signBoxes
    if(newProps.signCvs){
      let signCvs=newProps.signCvs;
      let self=this;
      var {size, width, height} = this.state;

      if(this.props.signFields&&this.props.signFields.client){
        let signFields=this.props.signFields.client;
        for( let f =0; f< signFields.length; f++){
          let signField=signFields[f];
          let signLocations=signField.signLocation;
          for( let s =0; s< signLocations.length; s++){
            let signLocation=signLocations[s];
            let signId=signLocation.id;
            //let signPage=signLocation.page;
            let signX=Number(signLocation.x)*width;
            let signY=Number(signLocation.y)*height;
            let signWidth=Number(signLocation.width)*width;
            let signHeight=Number(signLocation.height)*height;
            /////
            if( self.refs[signId] && signCvs['same']){
              var sCanvas = self.refs[signId];
              var ctx = sCanvas.getContext('2d');
              var drawCanvas=signCvs['same'];

              let gridWidth=sCanvas.width;
              let gridHeight=sCanvas.height;
              let drawWidth=drawCanvas.width;
              let drawHeight=drawCanvas.height;

              let scale=Math.min(gridWidth/drawWidth, gridHeight/drawHeight);
              let scaledWidth=drawWidth*scale;
              let scaledHeight=drawHeight*scale;
              ctx.clearRect(0, 0, gridWidth, gridHeight);

              ctx.drawImage(drawCanvas, (gridWidth-scaledWidth)/2, (gridHeight-scaledHeight)/2, scaledWidth, scaledHeight);
            }
          }
        }
      }
      self.forceUpdate();
    }
  }

  reload(data) {
    var self = this;

    if (data) {
      if (typeof data == 'string') {
        if (this.state.fileSize == data.length) {
          return;
        }
        this.state.fileSize = data.length
        data = base64ToArrayBuffer(data).buffer;
      }

      PDFJS.getDocument(data).then(function(pdf) {
        var pcount = pdf.numPages;
        var {
          muiTheme
        } = self.context;

        var renderPage = function(page, param) {
          var that = param || this;
          var viewport = page.getViewport(2.0);
          // viewport = page.getViewport(width / viewport.width);
          var canvas = document.createElement('canvas');
          // var canvas = self.refs['c'+this.index];
          canvas.width = viewport.width;
          canvas.height = viewport.height;
          // var canvas = document.createElement('canvas')
          var context = canvas.getContext('2d');
          var id = that.index;
          // canvas.height = viewport.height;
          // canvas.width = viewport.width;
          page.render({
            canvasContext: context,
            viewport: that.viewport
          }).promise.then(function() {
            var rCanvas = self.refs['c'+id];
            var ctx = rCanvas.getContext('2d');
            rCanvas.width = that.width;
            rCanvas.height = that.height;
            ctx.drawImage(canvas, 0, 0, that.width, that.height);

            // check all page rendered
            if(that.index == pcount && self.props.pdfLoadingComplete) {
              self.props.pdfLoadingComplete();
            }
          });
        }

        pdf.getPage(1).then(function(page) {
          var viewport = page.getViewport(2.0);
          //minus scroll bar width
          var width = self.props.pageWidth || ((muiTheme.windowWidth-20-2 > viewport.width?viewport.width:muiTheme.windowWidth-20-2));
          var height = self.props.pageHeight || (width?(width * viewport.height / viewport.width):viewport.height);
          self.setState({
            size: self.props.page ? 1 : pcount,
            width: width,
            height: height
          })

          setTimeout(function() {
            for (var i = 1; i <= pcount; i++) {
              if (self.props.page && self.props.page != i) {
                continue;
              }
              var param = {
                index: i,
                width: width,
                height: height,
                viewport: viewport
              }
              if (i == 1) {
                renderPage(page, param);
              } else {
                pdf.getPage(i).then(renderPage.bind(param))
              }
            }
          }, 500);
        });
      }).catch(function (ex) {
        self.setState({
          size: -1
        })
      });
    } else {
      if (data === false) {
        self.setState({
          size: -1
        })
      }
    }
  }

  handleOnScroll(f, e){
    if (this.props.onScroll) {
      this.props.onScroll(e);
    }

    if(this.props.reachEnd){
      let pdfId=this.props.pdfId;
      if(e.target.clientHeight+e.target.scrollTop>=e.target.scrollHeight){
        this.props.reachEnd(pdfId);
      }
    }
  }

  render() {
    var self = this;

    var {
      muiTheme
    } = this.context;

    var {
      size,
      width,
      height,
      pdfId,
    } = this.state;

    var {
      pdf,
      ...others
    } = this.props;

    if (size > 0) {
      var papers = [];
      for(var p = 1; p <= size; p++) {

        //creat signatures boxes

        let signBoxes=[];

        if(this.props.signFields&&this.props.signFields.client){
          let signFields=this.props.signFields.client;
          for( let f =0; f< signFields.length; f++){
            let signField=signFields[f];
            if(signField.page==p){
              let signLocations=signField.signLocation;
              for( let s =0; s< signLocations.length; s++){
                let signLocation=signLocations[s];
                let signId=signLocation.id;
                //let signPage=signLocation.page;
                let signX=Number(signLocation.x)*width;
                let signY=Number(signLocation.y)*height;
                let signWidth=Number(signLocation.width)*width;
                let signHeight=Number(signLocation.height)*height;
                signBoxes.push(
                  <div
                    style={{
                      top:signY,
                      left :signX,
                      position:'absolute',
                      zIndex: 1600,
                      width:signWidth,
                      height:signHeight,
                      border:'0px solid'}}
                     >
                    <canvas key={'s'+s} ref={signId} width={signWidth} height={signHeight}></canvas>
                  </div>
                );
              }
            }
          }
        }

        papers.push(
          <Paper key={'p'+p}
          ref={'p'+p}
          style={ this.props.paperStyle || {
            marginTop: muiTheme.baseTheme.spacing.desktopGutterMini,
            marginLeft: muiTheme.baseTheme.spacing.desktopGutterMini,
            marginRight: muiTheme.baseTheme.spacing.desktopGutterMini,
            border: '1px solid #979797',
            boxShadow: 'rgba(0, 0, 0, 0.5) 0px 2px 4px',
            display: 'inline-block',
            height: height,
            width: width + 2,
            position:'relative',
          }}
          ><canvas key={'c'+p} ref={'c'+p} width={width} height={height}/>
          {signBoxes}
          </Paper>)
      }
      return <div
        key={"pdfviewer"}
        className={ this.props.className || styles.FlexContent }
        style={this.props.containerStyle || {
          paddingBottom: '4px',
          width: '100%',
          textAlign: 'center',
          height: '100%'
        }}
        onScroll={this.handleOnScroll.bind(this, this.props.pdfId)}>
        {papers}
      </div>
    } if (size < 0) {
      return <div
        key={ "pdfviewer"}
        className={ this.props.className || styles.FlexContent }
        style={{
          marginTop: '50px',
          width: '100%',
          textAlign: 'center'
        }}>
        <span>File missing or Invalid format</span>
      </div>;
    } else {
      return <div
        key={"pdfviewer"}
        className={ this.props.className || styles.FlexContent }
        style={{
          marginTop: '50px',
          width: '100%',
          textAlign: 'center',
          height: '200px'
        }}>
        <CircularProgress key="loader" ref="loader"/>
      </div>;
    }
  }
}

PDFViewer.propTypes = Object.assign({}, PDFViewer.propTypes, {
    pdf: PropTypes.string,
    height: PropTypes.any,
    page:  PropTypes.number,
    pdfNum: PropTypes.number,
    pageWidth: PropTypes.any,
    pageHeight: PropTypes.any,
    paperStyle: PropTypes.object,
    containerStyle: PropTypes.object,
    pdfLoadingComplete: PropTypes.func,
    reachEnd: PropTypes.func,
    onScroll: PropTypes.func
})

export default PDFViewer;
