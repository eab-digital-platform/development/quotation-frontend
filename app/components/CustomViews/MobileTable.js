import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from '../Common.scss';
import { getIcon } from '../Icons/index';
import {IconButton} from 'material-ui';

import styled, {css} from 'styled-components';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';

import * as _ from 'lodash';

const StyledDiv = styled.div`
    ${
        props => props.hideTable && css`
            display: none;
        `
    }
`;

class MobileTable extends PureComponent{
  constructor(props) {
    super(props);
    this.state = Object.assign( {}, this.state, {
      openTable: false
    });
  }

  controlDisplay = () => {
    this.setState({
      openTable: !this.state.openTable
    });
  }

  generateTableRow = (title, value, isGrey) => {
    let style = (isGrey) ? {backgroundColor: '#FAFAFA'} : undefined;
    return (
      <div style={{lineHeight: '1.4em', display: 'flex', minHeight: '0px', ...style}}>
        <div style={{flex: '3', fontWeight: '600'}}>
          {title}
        </div>
        <div style={{flex: '4'}}>
          {value}
        </div>
      </div>
    );
  }

  generateTableBody = () => {
      const {changedValues ={}} = this.props;
      let tableBodyContent = [];
      _.each(changedValues.policiesMapping, policyValue => {
        tableBodyContent.push(
          this.generateTableRow('Proposal Number', policyValue.policyNumber, true)
        );
        tableBodyContent.push(
          this.generateTableRow('Life Assured', policyValue.laName, false)
        );
        tableBodyContent.push(
          this.generateTableRow('Plan Name', policyValue.covName.en, false)
        );
      });
      return (
        <div>
          {tableBodyContent}
        </div>
      );
  }

  render(){
      const {changedValues, template} = this.props;
      return (
        <div>
          <div style={{textAlign: 'center'}}>
            <div style={{display:'inline'}}>
              {template.title}
            </div>
            <div style={{display:'inline'}}>
              <IconButton onClick={() => {this.controlDisplay();}} style={{top: '6px'}}>
                {getIcon((this.state.openTable) ? 'KeyUp' : 'KeyDown', 'black')}
              </IconButton>
            </div>
          </div>
          <StyledDiv hideTable={this.state.openTable}>
            {this.generateTableBody()}
          </StyledDiv>
        </div>
      )
  }
}

MobileTable.propTypes = {

};

export default MobileTable;
