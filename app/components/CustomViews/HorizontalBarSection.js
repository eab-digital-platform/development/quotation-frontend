import React from 'react';
import PropTypes from 'prop-types';
import HorizontalBar from './HorizontalBar';
import HeaderTitle from './HeaderTitle';
import EABInputComponent from './EABInputComponent';

import styles from '../Common.scss';

class HorizontalBarSection extends EABInputComponent{ 
  getTotalValue=()=>{
    let {template, changedValues} = this.props;
    let value=0;
  
    template.items.map((item)=>{
      value+=(changedValues[item.id] || 0)
    })
    return value;
  }

  renderItems=()=>{
    let {validRefValues} = this.context;
    let {template, changedValues, values, handleChange, rootValues} = this.props;
    let {items, id: templateId} = template;

    let returnItems =[];

    items.map((itemTemplate, index)=>{
      if(!(itemTemplate.trigger && !showCondition(validRefValues, itemTemplate, values, changedValues, rootValues, []))){
        returnItems.push(
          <HorizontalBar
            key={template.id + '-'+index+'-'+itemTemplate.id}
            template={itemTemplate}
            changedValues={changedValues}
            rootValues={rootValues}
            handleChange={(id, value)=>{
              changedValues[id] = value;
              this.requestChange(this.getTotalValue())
            }}
            values={values}
          />
        )
      }
    })

    return returnItems;
  }

  render() {
    let {lang} = this.context;
    let {template} = this.props;
    let {title, hints,  mandatory} = template;

    return (
      <div className={styles.FlexColumnNoWrapContainer}>
        <HeaderTitle title={title} mandatory={mandatory} hints={hints}/>
        {this.renderItems()}
      </div>
    );
  }
}

HorizontalBarSection.propTypes = Object.assign(HorizontalBarSection.propTypes, {
  template: PropTypes.object,
  changedValues: PropTypes.object,
})

export default HorizontalBarSection;