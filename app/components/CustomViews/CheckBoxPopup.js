import React from 'react';
import * as _ from 'lodash';

import Checkbox from 'material-ui/Checkbox';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';
import ErrorMessage from './ErrorMessage';

const checkBoxLabelStyle = {
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  width: 'calc(90% - 40px)',
  fontSize: '16px'
}

class CheckBoxPopup extends EABInputComponent {
  constructor(props) {
    super(props);
    
    this.state = Object.assign({}, this.state, {
      showDialog:false
    })
  }

  handlePopupDialog = (newValue) => {
    if (newValue) {
      this.setState({showDialog: true});
    }
  }

  handleCloseDialog = () => {
    this.setState({showDialog: false});
  }

  render() {
    let {lang, muiTheme} = this.context;

    let {
      template, 
      values, 
      requestChange
    } = this.props;

    let {
      id, 
      title, 
      message, 
      disabled, 
      value
    } = template;

    let {
      showDialog
    } = this.state;

    const actions = [
      <FlatButton
        label="OK"
        primary={true}
        onTouchTap={this.handleCloseDialog}
      />,
    ];

    let cValue = (this.getValue("N") === "Y");
    let errorMsg = this.getErrMsg();

    return (
      <div> 
        <Checkbox
          key={id}
          label={getLocalText(lang, title)}
          labelStyle={checkBoxLabelStyle}
          className={styles.CheckBox}
          disabled={disabled}
          defaultChecked={cValue}
          onCheck={
            (e, val) => {
              this.requestChange(val);
              this.handlePopupDialog(val);
            }
          }
        />
        {
          _.isString(errorMsg)? (
            <div className={styles.ErrorMsgContainer}>
              <ErrorMessage template={template} message={errorMsg} style={{textAlign: template.align || 'center'}}/>
            </div>
          ) : null
        }
        {
          showDialog? (
            <Dialog
              className={styles.Dialog}
              modal={true}
              open={showDialog}
              actions={actions}
            >
              {message}
            </Dialog>
          ) : null
        }
      </div>
    )
  }
}

export default CheckBoxPopup;