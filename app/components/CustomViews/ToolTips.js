import React from 'react';
import {Popover, IconButton, FlatButton } from 'material-ui';
import {getIcon} from '../Icons/index';
import styles from '../Common.scss';
import _assign from 'lodash/fp/assign';

import EABComponent from '../Component';

class EABToolTips extends EABComponent{
  constructor(props){
    super(props);

    this.state = Object.assign( {}, this.state, {
      toolTipsOpen: false
    });
  }

  handleOnTouchTap(e) {
    let currentTargert = e.currentTarget;
    e.preventDefault();
    this.setState({
      toolTipsOpen: true,
      anchorEl: currentTargert
    });
  }

// NOTE: lucia from green to #3032c1 *** info icon should change to info-outline
  renderIconButon(iconType, Iconstyle) {
    const {muiTheme} = this.context;
    const style = _assign({ zIndex : 0 }, Iconstyle);
    switch (iconType) {
      case 'viewApplicant':
      return (<FlatButton 
                className={styles.Button}
                style={style}
                labelStyle={{paddingLeft: 0, paddingRight: 0}}
                onTouchTap={(e)=>{this.handleOnTouchTap(e);}}
                label={<span style={{color: 'red'}}>VIEW APPLICANTS</span>}
            />)
      case 'help':
      return (<IconButton
                iconClassName='material-icons'
                iconStyle={{color: muiTheme.palette.primary1Color}}
                className={styles.IconButton}
                style={style}
                onTouchTap={(e)=>{this.handleOnTouchTap(e);}}>
                help
              </IconButton>);
      default:
      return (<IconButton
                style={style}
                onTouchTap={(e)=>{this.handleOnTouchTap(e);}}>
                {getIcon('info', '#3032c1')}
              </IconButton>);
    }
  }

// NOTE: lucia maxWidth from 720 to 420; padding from 8 to 24
// NOTE: lucia newly added after 24px
  render() {
    let {lang} = this.context;
    let {hints, Iconstyle, containerStyle, iconType} = this.props;

    return (
      <div style={_.assign({height: '48px', alignSelf: 'center', display:'inline-block'}, containerStyle)}>
        {this.renderIconButon(iconType, Iconstyle)}
        <Popover
          open={this.state.toolTipsOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: "middle", vertical: "bottom"}}
          targetOrigin={{horizontal: "middle", vertical: "top"}}
          onRequestClose={()=>{this.setState({toolTipsOpen: false})}}
        >
          <div style={{maxWidth: '420px', padding: '24px', background: '#00008f', color: '#ffffff', lineHeight: '18px',}} dangerouslySetInnerHTML={{__html: window.getLocalText(lang, hints)}}/>
        </Popover>
      </div>
    )
  }

}

export default EABToolTips;
