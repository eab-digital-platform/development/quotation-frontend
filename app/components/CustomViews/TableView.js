let React = require('react');
import {Table, TableHeader, TableHeaderColumn, TableBody, TableRow, TableRowColumn} from 'material-ui';
import EABInputComponent from './EABInputComponent.js';
import ErrorMessage from './ErrorMessage';
import styles from '../Common.scss';
import * as _ from 'lodash';

class TableView extends EABInputComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign( {}, this.state, {
      template: props.template,
      changedValues: this.props.changedValues
    });
  }

  getDisplayText = (item, value) => {
    if(!item.type)
      return value;
    var viewType = item.type.toUpperCase();
    if( viewType== "PICKER" || item.type && item.type.toUpperCase() == 'QRADIOGROUP')
      return getOptionsTitle(item, value);
    else if(viewType == 'IMAGE'){
      return value ? <img src={value} width="40" height="50"/> : null;
    }else if(viewType == 'CHECKBOX'){
      return (value && value == 'Y') ? item.title : "";
    }else{
      if(value instanceof Object){
        var {
          lang
        } = this.context
        return getLocalText(lang, value);
      }else{
        return value;
      }

    }
  }

  genItems = (rowValues, items) => {
    let { muiTheme } = this.context;
    let {
      renderItems,
      handleChange,
      error
    } = this.props;
    let genRows = [];
    if(!rowValues || !items || typeof renderItems !== "function") {
      return null;
    }

    genRows = items.map((item, idx) => {
      if (item.type.toUpperCase() === "TABLEVIEW-SUBHEADER") {
        return (
          <TableRow key={item.id + '_' + idx} style={this.getStyles().tableSubHeaderStyle}>
            <TableRowColumn key={item.id  + '_' + idx + '_0'} colSpan={item.colSpan} style={{height: '24px'}}>{item.title}</TableRowColumn>
          </TableRow>
        );
      } else if (item.type.toUpperCase() === "TABLEVIEW-DATA") {
        let columns = [];
        let rValues = item.id? rowValues[item.id]: rowValues;
        renderItems(columns, item, rValues, rValues, error, item.title, handleChange);
        return (
          <TableRow key={item.id + '_' + idx}>
            {
              columns.map((col, index) => {
                return (
                  <TableRowColumn key={item.id + '_' + idx + '_' + index} style={{lineHeight: '72px', textOverflow: 'initial'}}>
                    {col}
                  </TableRowColumn>
                );
              })
            }
          </TableRow>
        );
      } else {
        return null;
      }
    })

    return genRows;
  }

  render() {
    let {langMap} = this.context;
    let {
      template,
      values,
      handleChange,
      ...others
    } = this.props;
    var {changedValues} = this.state;
    if(!this.props.changedValues[template.id] || this.props.changedValues[template.id] == null){
      changedValues[template.id] = template.values;
    }

    let errorMsg = this.getErrMsg();

    let {
      id,
      type,
      title,
      items
    } = template;

    let tableHeaders = items.map((item, idx) => {
      return item.type.toUpperCase() === "TABLEVIEW-HEADER"? (
        item.items.map((hItem, index) => {
          let hTitle = hItem.title;
          //special handle for different payment mode
          // if (hTitle.startsWith('application_list.payMode_')) {
          //   hTitle = getLocalizedText(langMap, 'application_list.premium').replace('[@@payMode]', getLocalizedText(langMap, hTitle));
          // }

          return (
            <TableHeaderColumn key={item.id + "_" + hItem.id} style={this.getStyles().tableHeaderStyle}>
              {hTitle}
            </TableHeaderColumn>
          )
        }))
        : null;
    })

    let tableRows = this.genItems(changedValues[id], items);

    return <div
      className={"DetailsItem"} style={{boxShadow: '0px 2px 10px grey', maxWidth: '880px', margin: '50px auto' }}>
      <Table 
        key={"tb"+id} 
        selectable={false}
       //className={styles.MuTableView}
      >
        <TableHeader 
          displaySelectAll={false}
          adjustForCheckbox={false}
        >
          <TableRow>{tableHeaders}</TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {tableRows}
        </TableBody>
      </Table>
      <div className={styles.ErrorMsgContainer}>{_.isString(errorMsg)?<ErrorMessage template={template} message={errorMsg} style={{position:'relative', left:0, top: 0}}/>: null}</div>
    </div>
  }
}

export default TableView;
