import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from '../Common.scss';

import styled, {css} from 'styled-components';

const StyledDiv = styled.div`
    display: inline-block;
    padding: 0 24px;
    maxWidth: calc(100% - 280px);
    width: calc(100% - 280px);

    ${
        props => props.iphone && css`
            padding: 0px;
            maxWidth: 100%;
            width: 100%;
        `
    }
`;

class CardBlock extends PureComponent{
    render(){
        let {children, id} = this.props;
        return (
            <StyledDiv iphone={window.isMobile.apple.phone} key={`${id}-key`}>
                {children}
            </StyledDiv>
        );
    }
}

CardBlock.propTypes = {
    children: PropTypes.array,
    id: PropTypes.id
};

export default CardBlock;
