import React, { Component, PropTypes}  from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import DropZone from './DropZone.jsx';
import {getIcon} from '../Icons/index';
import {upsertFile, isShowDragDropZone} from '../../actions/supportDocuments';
import _getOr from 'lodash/fp/getOr';
import _cloneDeep from 'lodash/fp/cloneDeep';

import EABInputComponent from './EABInputComponent';
import styles from '../Common.scss';
import forEach from 'lodash/forEach';
import EmbedPDFViewer from './EmbedPDFViewer';

class DynColFileUpload extends EABInputComponent{

    constructor(props){
        super(props);
        this.state = Object.assign({},this.state,{
            showFileSizeLargeDialog: false,
            showFileTypeUnacceptable: false,
            showTotalFilesSizeLargeDialog: false,
            filesArr: [],
            openPreviewDialog: false,
            previewObj: ''
        });
    }

    dropZoneOnDrop = (res) => {
        let fileType = res.file.type;
        let {filesArr} = this.state;
        let totalSize = 0;
        forEach(filesArr, obj =>{
            totalSize += obj.file.size;
        });

        const clonedfilesArr = _cloneDeep(filesArr);
        if (!this.checkTotalSize(totalSize, res.file.size)) {
            if (fileType == 'image/png' || fileType == 'image/jpg' || fileType == 'image/jpeg' || fileType == 'application/pdf' ){
                if (!this.exceedMaxFileSize(res)){
                    clonedfilesArr.push(res);
                    this.requestChange(clonedfilesArr);
                    this.setState({
                        filesArr: clonedfilesArr
                    });
                }
            } else {
                this.setState({showFileTypeUnacceptable: true});
            }
        }
    }

    closeDropZone = ()=>{
        isShowDragDropZone(this.context, false, {});
    }

    checkTotalSize = (totalSize, values) => {
        //const MAXSIZE = 20 * 1048676;
        let {filesArr}  = this.state;
        let {maxSize} = this.props.template;

        totalSize += values;

        if (totalSize > maxSize * 1048676) {
            this.setState({showTotalFilesSizeLargeDialog:true});
            return true;
        } else {
            return false;
        }
    }

    exceedMaxFileSize = (res) => {
        let {fileMaxSize} = this.props.template
        let maxSize = fileMaxSize * 1048576;
        if (res.file.size > maxSize){
            this.setState({showFileSizeLargeDialog:true});
            return true;
        }
        return false;
    }

    fileSizeLargeDialog = () => {
        return(
            <Dialog open={this.state.showFileSizeLargeDialog}
                    title={"NOTICE"}
                    actions={
                        [<FlatButton
                            label="OK"
                            primary={true}
                            onTouchTap={()=>{
                                this.setState({showFileSizeLargeDialog: false});
                            }}
                        />]
                        }
            >
                File too large!
            </Dialog>
        );
    }

    fileTypeUnacceptable = () => {
        return(
            <Dialog open={this.state.showFileTypeUnacceptable}
                    title={"NOTICE"}
                    actions={
                        [<FlatButton
                            label="OK"
                            primary={true}
                            onTouchTap={()=>{
                                this.setState({showFileTypeUnacceptable: false});
                            }}
                        />]
                        }
            >
                Invalid File Format.
            </Dialog>
        );
    }

    totalFilesSizeLargeDialog = () => {
        return(
            <Dialog open={this.state.showTotalFilesSizeLargeDialog}
                    title={"NOTICE"}
                    actions={
                        [<FlatButton
                            label="OK"
                            primary={true}
                            onTouchTap={()=>{
                                this.setState({showTotalFilesSizeLargeDialog: false});
                            }}
                        />]
                    }
            >
                You have exceeded the maximum file size!
            </Dialog>
        );
    }

    dialogViews = () => {
        return(
            <div>
            {this.fileSizeLargeDialog()}
            {this.fileTypeUnacceptable()}
            {this.totalFilesSizeLargeDialog()}
            </div>
        );
    }

    genPreviewFile = () =>{
        let {filesArr} = this.state;
        let items =[];
        let fileName;
        const clonedfilesArr = _cloneDeep(filesArr);
        forEach(clonedfilesArr, (obj, index) =>{
            fileName = obj.file.name.split('.');
            items.push(
                <div>
                    {this.gentPreviewIcon(obj, fileName, index)}
                    <div style={{paddingLeft: '12px', display: 'inline'}}>{obj.file.name}</div>
                    <FlatButton
                        label="Delete"
                        primary={true}
                        onTouchTap={()=>{
                            clonedfilesArr.splice(index, 1);
                            this.setState({
                                filesArr: clonedfilesArr
                            });
                            this.requestChange(clonedfilesArr);
                        }}
                    />
                </div>
            )
        });
        return items;
    }

    previewAttachment = (obj) => {
        let iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        let android = /android/i.test(navigator.userAgent) && !window.MSStream;
        if (!iOS && !android) {
            let base64Str = obj.imageUrl.split(',')[1];
            let embedObj;
            let previewObj
            if (obj && obj.file && obj.file.type.indexOf('image') > -1) {
              previewObj = <object data={base64Str} type={obj.file.type} style={{width: '700px', height: '500px'}}>
              <img src={obj.imageUrl} style={{maxWidth: '100%', height: 'auto'}}/>
              </object>
            } else {
              previewObj = <object data={obj.imageUrl} type={obj.file.type} style={{width: '700px', height: '500px'}}>
              <embed src={obj.imageUrl} type={obj.file.type} style={{width: '700px', height: '500px'}}/>;
              </object>
            }
    
    
            this.setState({
                openPreviewDialog: true,
                previewObj: previewObj
            })
        }
    }

    gentPreviewIcon = (obj, fileName, index) => {
        let {muiTheme} = this.context;
        let objType = _getOr('', 'file.type', obj);
        if (objType.indexOf('image') > -1) {
            return <img key={`img-${fileName[0]}-${index}`}
                src={obj.imageUrl} style={{width: '25px', height: '25px'}}
                onClick={ () => {this.previewAttachment(obj)} }/>
        } else if (objType.indexOf('pdf') > -1) {
            let pdfIcon = getIcon('pdf', muiTheme.palette.primary2Color);
            return (<IconButton
                        key={`pdf-${fileName[0]}-${index}`}
                        id={`pdf-${fileName[0]}-${index}`}
                        onTouchTap={() => {this.previewAttachment(obj)}}
                    >
                        {pdfIcon}
                    </IconButton>);
        } else {
            return;
        }
    }

    render(){
        var {
            template,
            changedValues,
            values,
            allFilesValues,
            ...others
        } = this.props;
        var {
            muiTheme
        } = this.context;

        let { openPreviewDialog, previewObj } = this.state;

        let applicationId = values.id;
        let title, subTitle;
        if (template && template.title){
            title = template.title;
        }

        if (template && template.subTitle){
            subTitle = template.subTitle;
        }

        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onClick={()=>{
                    this.setState({
                        openPreviewDialog: false
                    });
                }}
            />,
        ];

        let dropZone = <DropZone
                onDrop={(res)=>{this.dropZoneOnDrop(res)}}
                {...others}
                accept="image/png,image/jpg,image/jpeg,application/pdf"
                >
                {this.dialogViews()}
                <div style={{width:'100%', height:'48px', background:'lightGray', textAlign:'center'}}
                    id='dragdropzone'>
                    {(window.isMobile.apple.phone) ? 'Click to add file(s) ' : 'Please drag your file(s) here'}
                </div>
            </DropZone>

        return (
            (title) ?
                <div style={{paddingTop: '12px'}}>
                    <div>{title}{template.mandatory && title ? <span key="s" className={styles.HeadermandatoryStar}>&nbsp;*</span>: null}</div>
                    <div className={styles.subTextTitle + ' ' + styles.StandardLineHeight} style={template.subTitleStyle}>{subTitle}</div>
                    {dropZone}
                    {this.genPreviewFile()}
                    <Dialog
                      className={styles.FixWrongDialogPadding}
                        actions={actions}
                        modal={true}
                        open={openPreviewDialog}
                    >
                        {previewObj}
                    </Dialog>
                </div>
                :
                dropZone
        )
    }
}

export default DynColFileUpload;

