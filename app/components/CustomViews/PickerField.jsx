import React from 'react'
import {MenuItem} from 'material-ui';

import EABInputComponent from './EABInputComponent';
import SelectField from './SelectField';
import styles from '../Common.scss';

class EABPickerField extends EABInputComponent{

  render() {
    let {lang} = this.context;
    let {changedValues} = this.state;
    let {template, values, error, width, style, pickerStyle, handleChange} = this.props;
    let {id, title, mandatory, placeholder, disabled, isCustom, subType, presentation} = template;
    if (pickerStyle === true) {
        pickerStyle = {width:'40%', height:'80px', minWidth: "350px"}
    }

    let styleClass;
    if (template.fullLengthInMobile && window.isMobile.apple.phone) {
      styleClass = styles.Picker + (presentation ? ' ' + styles[presentation] : '');
    } else {
      styleClass = styles.DetailsItem + ' ' + styles.Picker + ( presentation ? ' ' + styles[presentation] : '');
    }

    return (  
      <div className={styleClass} style={style}>
        <SelectField
          key={id}
          ref={id}
          id={id}
          template={template}
          disabled={disabled}
          changedValues={changedValues}
          error={error}
          title={template.title ? this.getFieldTitle(mandatory, title) : null}
          style={pickerStyle}
          errorStyle={this.getStyles().errorStyle}
          hintText= { placeholder?getLocalText(lang, placeholder):null }
          hintStyle= { this.getStyles().hintStyle }
          floatingLabelStyle={this.getStyles().floatingLabelStyle}
          floatingLabelShrinkStyle = {this.getStyles().floatingShrinkStyle}
          selectedMenuItemStyle={this.getStyles().selectedMenuItemStyle}
          fixfloatingLabel= {isCustom}
          labelStyle={this.getStyles().pickerLabel}
          iconStyle={this.getStyles().pickerLabel}
          underlineStyle = { {bottom:'22px'} }
          underlineDisabledStyle = { this.getStyles().disabledUnderline }          
          handleChange={(id, value)=>{
            this.requestChange(value);
          }}
          onBlur={(e)=>{
            this.handleBlur(e.target.value);
          }}
          />
      </div>
    );
  }
}

EABPickerField.defaultProps = Object.assign({}, EABPickerField.defaultProps, {
  pickerStyle: {width:'100%', height:'80px', minWidth: "20%"}
})

export default EABPickerField