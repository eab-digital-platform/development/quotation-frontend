import React from 'react'
import { render } from 'react-dom';
// Import React-JSS
import styled, {css} from 'styled-components';
let { Card } = require('material-ui');
import EABInputComponent from './EABInputComponent';

const StyledCard = styled(Card)`
  && {
    background: black;
    padding: 100px;

    ${
      props => props.cardStyle && css `
        ${props.cardStyle}
      `
    }

    ${
      props => props.isMobile && props.isSearchMenu && css `
        color: red;
      `
    }
  }
  `;

class ResponsiveCard extends EABInputComponent {
  render() {
    let {children, style, isSearchMenu} = this.props;
    return (<StyledCard cardStyle={style} isMobile isSearchMenu={isSearchMenu}>{children}</StyledCard>);
  }
}

export default ResponsiveCard;
