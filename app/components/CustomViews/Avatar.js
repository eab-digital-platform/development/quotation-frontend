import React from 'react';
import PropTypes from 'prop-types';
import {Avatar} from 'material-ui';
import DropZone from './DropZone';
import {getIcon} from '../Icons/index';
import EABInputComponent from './EABInputComponent';
import ErrorMessage from './ErrorMessage';

import {addAttachment} from '../../actions/attachments';
import styles from '../Common.scss';

class EABAvatar extends EABInputComponent {
  constructor(props){
    super(props);
    this.state = Object.assign({}, this.state, {
      lastModified: null,
      loadError: false,
      image:'',
      attachImg: '',
      errorMsg: ''
    });
  }

  componentWillReceiveProps(newProps, newState) {
    const nextState = {
      changedValues: newProps.changedValues
    };
    if (this.props.lastUpdateDate !== newProps.lastUpdateDate){
      nextState.loadError = false;
   }
    this.setState(nextState);
  }

  updateAvatar=(resp)=>{
    var self = this;
    window.imageResize(resp.imageUrl, 100, 100, 'image/png', function(result) {
      if (result.success) {
        let {id} = self.props.template;
        self.setState({attachImg: result.imageUrl, errorMsg: ''});
        addAttachment(self.context, id, result.imageUrl, 'image/png', ()=>{self.requestChange(resp.file.lastModified)});
      } else {
        self.setState({errorMsg: result.error});
      }
    });
  }

  getImage=()=>{
    let {store} = this.context;
    let {image, lastModified, attachImg} = this.state;
    let {docId, template, origin, agentProfilePicRev} = this.props;
    let {id} = template;
    let root = null;
    if (origin && agentProfilePicRev) {
      root = {
        origin: origin,
        revpos: agentProfilePicRev
      };
    }

    let cValue = this.getValue('');
    if (!isEmpty(cValue) && cValue !== 'N') {
      //special handle for trusted individual, cValue = cid
      if (id === 'tiPhoto' && _.isString(cValue) && cValue.indexOf('ACP') > -1) {
        return window.genFilePath(store.getState().app, cValue, 'photo', null, root);
      } else if (attachImg) {
        return attachImg;
      } else {
        if (image && lastModified === cValue) {
          return image;
        } else {
          return window.genFilePath(store.getState().app, docId, id, null, root);
        }
      }
    }
    return '';
  }

  render(){
    let self = this;
    let {muiTheme} = this.context;
    let {template, width, iconSize, handleChange, style, requireShowDiff, values} = this.props;
    let {id, value, align, styleClass} = template;
    let initial = (values) ? values.initial : "N/A"

    let disabled = this.props.disabled || template.disabled;
    let cValue = this.getImage();

    let combineStyle = style? Object.assign(style, { verticalAlign:'top'}) : {verticalAlign:'top'}

    let imageObj = null;

    if (!this.state.loadError && !isEmpty(cValue)) {
        imageObj = (<Avatar
        src={cValue}
        size={parseInt(width)}
        backgroundColor="RGBA(0,0,0,0.1)"
        onError={function(e) {
          e.target.src = ''
          self.setState({
            loadError: true
          })
        }}
      ></Avatar>
      )
    } else if (values.initial) {
      imageObj = (<Avatar
        src=""
        size={parseInt(width)}
        backgroundColor="RGBA(0,0,0,0.1)"
     >{initial}</Avatar>
      )
    } else {
      imageObj = (<Avatar
        src=""
        size={parseInt(width)}
        backgroundColor="RGBA(0,0,0,0.1)"
        icon={getIcon('person', "#FFFFFF")}
      ></Avatar>)
    }

    let dropzone = (
      <DropZone
        onDrop={this.updateAvatar} style={{
          textAlign: (align) ? align : 'right',
          lineHeight: '23px',
          color: muiTheme.baseTheme.palette.textColor,
          fontWeight: 400,
          width: width,
          height: width}} >
        {imageObj}
      </DropZone>
    )

    return (
      <div className={styles[styleClass]} style={{...style, display:"inline-block", maxHeight: '60px'}} >
        {disabled? imageObj: dropzone}
        {this.state.errorMsg?<ErrorMessage message={this.state.errorMsg}/>: null}
      </div>
    )
  }
}

EABAvatar.propTypes = Object.assign({}, EABAvatar.propTypes, {
  width: PropTypes.number,
  iconSize: PropTypes.number,
  docId: PropTypes.string,
  lastUpdateDate:PropTypes.object
})

export default EABAvatar;
