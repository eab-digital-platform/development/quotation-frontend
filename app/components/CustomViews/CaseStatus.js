let React = require('react');
   

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';

import styled, {css} from 'styled-components';

import {
  STATUS,
  APPRVOAL_STATUS_APPROVED,
  APPRVOAL_STATUS_REJECTED,
  APPRVOAL_STATUS_EXPIRED,
  APPRVOAL_STATUS_SUBMITTED,
  APPRVOAL_STATUS_PFAFA,
  APPRVOAL_STATUS_PDOC,
  APPRVOAL_STATUS_PDIS,
  APPRVOAL_STATUS_PDOCFAF,
  APPRVOAL_STATUS_PDISFAF,
  INPROGRESS_BI,
  INPROGRESS_APPLICATION
} from '../Pages/Pos/Approval/approvalStatus';

const StyledDiv = styled.div`
  position: absolute;
  right: 24px;
  width: 200px;
  text-align: center;
  height: 24px;
  line-height: 24px;

  ${
    props => props.iphone && css`
      position: relative;
      right: 0px;
      margin-bottom: 10px;
    `
  }
`;

class CaseStatus extends EABInputComponent {
  constructor(props) {
    super(props);
  }

  transferStatusValue(value) {
    return STATUS[value];
  }

  transformStatusColor = (cValue) => {
    if (cValue === APPRVOAL_STATUS_APPROVED) {
      return styles.showGreenBackgroundColor;
    } else if (cValue === APPRVOAL_STATUS_REJECTED || cValue === APPRVOAL_STATUS_EXPIRED) {
      return styles.showRedBackgroundColor;
    } else if ([APPRVOAL_STATUS_SUBMITTED, APPRVOAL_STATUS_PFAFA].indexOf(cValue) > -1) {
      return styles.showPendingApprovalColor;
    } else if ([APPRVOAL_STATUS_PDOC, APPRVOAL_STATUS_PDIS, APPRVOAL_STATUS_PDOCFAF, APPRVOAL_STATUS_PDISFAF].indexOf(cValue) > -1) {
      return styles.showPendingDocDisColor;
    } else if ([INPROGRESS_BI, INPROGRESS_APPLICATION].indexOf(cValue) > -1) {
      return styles.showInProgressColor;
    } else {
      return styles.showOrangeBackgroundColor;
    }
  }

  render() {
    let {
      template
    } = this.props;


    let {
      id
    } = template;

    let cValue = this.getValue('');

    return (
        <StyledDiv className={this.transformStatusColor(cValue) + ' ' + styles.whitefont} iphone={window.isMobile.apple.phone} key={id}>
            {this.transferStatusValue(cValue)}
        </StyledDiv>
    );
  }
}

export default CaseStatus;