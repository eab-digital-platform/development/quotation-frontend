import React from 'react';
import * as _ from 'lodash';

import HeaderTitle from './HeaderTitle';
import TwoSectionItem from './TwoSectionItem';
import EABInputComponent from './EABInputComponent';

import styles from '../Common.scss';

const expensesColor = 'red';
const incomeColor = 'green';

class TwoSection extends EABInputComponent{
  renderItems = (template, values, changedValues, error) =>{
    const { lang } = this.context;
    let returnItems = [];
    let sectionItems = [];

    template.items.map((item, index)=>{
      let { type, id, title } = item;

      if (type === 'showValues') {
        let cValue = changedValues[id] || 0;
        returnItems.push(
          <div key={`cash-flow-${id}-${index}`} style={{flexBasis: '100%', fontSize: '20px'}}>
            <div className={styles.FlexNoWrapContainer}>
              <div styles={{ padding: '10px'}}>{window.getLocalText(lang, title)}</div>
              <div style={{color: cValue >0? incomeColor: expensesColor, alignSelf: 'flex-end', padding: '10px'}}>{window.getCurrency(cValue, '$', 0)}</div>
            </div>
          </div>
        )
      } else {
        sectionItems.push(
          <TwoSectionItem
            key={'ann-ins-prem-' + index}
            template={item}
            changedValues={changedValues}
            values={values}
            validRefValues={this.props.validRefValues}
            rootValues={this.props.rootValues}
            error={error}
            handleChange={(id, value)=>{
              this.props.handleChange(id, value);
              this.forceUpdate();
            }}
          />
        );
      }
      
    });
    returnItems.push(<div style={{width: '100%', maxWidth: '1000px',margin: '0px auto'}}>{sectionItems}</div>);
    return returnItems;
  }


  render() {
    let { changedValues } = this.state;
    let { template, values, error } = this.props;
    let { title } = template;

    let items = this.renderItems(template, values, changedValues, error);
    return (
      <div>
        {title ? <HeaderTitle title={title}/> : null}
        {items}
      </div>
    );
  }

}

export default TwoSection;