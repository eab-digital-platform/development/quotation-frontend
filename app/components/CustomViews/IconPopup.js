import React, {PropTypes} from 'react';
import {IconButton, Dialog} from 'material-ui';

import EABComponent from '../Component';
import {getIcon} from '../Icons/index';

export default class IconPopup extends EABComponent {

  static propTypes = {
    icon: PropTypes.string,
    color: PropTypes.string,
    title: PropTypes.string,
    content: PropTypes.element,
    style: PropTypes.object
  };

  static defaultProps = {
    icon: 'info'
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      openDialog: false
    });
  }

  render() {
    const {muiTheme} = this.context;
    const {icon, color, title, content, style} = this.props;
    const {openDialog} = this.state;
    let iconColor = color || muiTheme.palette.primary2Color;
    return (
      <div style={{ display: 'flex', minHeight: '0px', ...style }}>
        <IconButton
          style={{ padding: 0, width: '24px', height: '24px' }}
          onTouchTap={() => this.setState({ openDialog: true })}
        >
          {getIcon(icon, iconColor)}
        </IconButton>
        <Dialog
          open={openDialog}
          bodyStyle={{ padding: '32px 24px' }}
          title={title}
          onRequestClose={() => this.setState({ openDialog: false })}
        >
          {content}
        </Dialog>
      </div>
    );
  }

}
