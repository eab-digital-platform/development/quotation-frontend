/** In this file, we create a React component which incorporates components provided by material-ui */

import React from 'react';
import EABComponent from '../Component';

var idx=3;
var self;
var cvs;
var cvsWidth=1600;
var cvsHeight=300;

class SignDialog extends EABComponent{
	componentDidMount() {
        // Instantiate the paperScope with the canvas element
		var   flag = false,
		prevX = 0,
		currX = 0,
		prevY = 0,
		currY = 0,
		dot_flag = false;

		var x = "black",
		y = 3;

		var canvas = document.getElementById('drawSurface');
		var context = canvas.getContext('2d');

		canvas.addEventListener('touchstart',draw, false);
		canvas.addEventListener('touchmove',draw, false);
		canvas.addEventListener('touchend',draw, false);

		canvas.addEventListener("mousemove", function (e) {
			findxy('move', e)
		}, false);
		canvas.addEventListener("mousedown", function (e) {
			findxy('down', e)
		}, false);
		canvas.addEventListener("mouseup", function (e) {
			findxy('up', e)
		}, false);
		canvas.addEventListener("mouseout", function (e) {
			findxy('out', e)
		}, false);

		var drawer = {
			isDrawing: false,
			touchstart: function(coors){
				context.beginPath();
				context.moveTo(coors.x, coors.y);
				this.isDrawing = true;
			},
			touchmove: function(coors){
				if (this.isDrawing) {
					context.lineTo(coors.x, coors.y);
          context.strokeStyle = x;
    			context.lineWidth = y;
					context.stroke();
				}
			},
			touchend: function(coors){
				if (this.isDrawing) {
					this.touchmove(coors);
					this.isDrawing = false;
				}
			}
		};
		// create a function to pass touch events and coordinates to drawer
		function draw(event){
					// get the touch coordinates
			var coors = {
					x:event.targetTouches[0].pageX- event.target.getBoundingClientRect().left,
					y: event.targetTouches[0].pageY- event.target.getBoundingClientRect().top
			};
		// pass the coordinates to the appropriate handler
					drawer[event.type](coors);
		}


		var   flag = false,
		prevX = 0,
		currX = 0,
		prevY = 0,
		currY = 0,
		dot_flag = false;

		var x = "black",
		y = 3;
		function findxy(res, e) {

			if (res == 'down') {
				prevX = currX;
				prevY = currY;
				currX = e.clientX - e.target.getBoundingClientRect().left;
				currY = e.clientY - e.target.getBoundingClientRect().top;

				flag = true;
				dot_flag = true;
				if (dot_flag) {
					context.beginPath();
					context.fillStyle = x;
					context.fillRect(currX, currY, 2, 2);
					context.closePath();
					dot_flag = false;
				}
			}
			if (res == 'up' || res == "out") {
				flag = false;
			}
			if (res == 'move') {
				if (flag) {
					prevX = currX;
					prevY = currY;
					currX = e.clientX - e.target.getBoundingClientRect().left;
					currY = e.clientY - e.target.getBoundingClientRect().top;
					mouseDraw();

				}
			}
		}

		function mouseDraw() {
		
			context.beginPath();
			context.moveTo(prevX, prevY);
			context.lineTo(currX, currY);
			context.strokeStyle = x;
			context.lineWidth = y;
			context.stroke();
			context.closePath();
		}
    }

	render() {

    var self = this;
    let initial = 0;
    let {
      canvasWidth,
      canvasHeight,
      canvasStyle
    }=this.props;
    if(!canvasWidth){
      canvasWidth='700px'
    }
    if(!canvasHeight){
      canvasHeight='141px'
    }
    if(!canvasStyle){
      canvasStyle={display: 'inline'};
    }
    return (
      	<div id="canvas-container" style={{width: canvasWidth, height:canvasHeight,  textAlign:'center', borderBottom:'1px solid rgba(0,0,0,.12)'}}>
          <canvas id="drawSurface" width={canvasWidth} height={canvasHeight} style={canvasStyle}/>
        </div>
    );
  }
}

export default SignDialog;