let React = require('react');
let {RaisedButton} = require('material-ui');

import styles from '../Common.scss';
import EABInputComponent from './EABInputComponent';


class Button extends EABInputComponent {
  constructor(props) {
    super(props);
  }

  handleButtonClick=(e)=>{
    let {
      template,
      handleChange
    } = this.props;

    let {
      subType,
      value
    } = template;

    if (subType && subType.toUpperCase() == "URL") {
      window.open(value);
    }

    handleChange(e);
  }

  render() {
    let {
      template,
      values,
      style,
      handleChange
    } = this.props;


    let {
      id,
      title,
      disabled,
      subType,
      value,
      action
    } = template;

    let button = (
      <RaisedButton
        key={id}
        ref={id}
        id={id}
        label={title}
        disabled={disabled}
        primary={true}
        onTouchTap={action || this.handleButtonClick}
      >
      </RaisedButton>
    );

    return (
      <div className={styles.DetailsItem}
           style={Object.assign({}, style, template.style)}>
        {button}
      </div>
    );
  }
}

export default Button;
