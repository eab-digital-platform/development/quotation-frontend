import React from 'react';
import appTheme from '../../theme/appBaseTheme.js';
import EABTextField from './TextField.js';
import ErrorMessage from './ErrorMessage';
import * as _ from 'lodash';

import EABInputComponent from './EABInputComponent.js';
const thStyle = {
  borderBottom: '1px solid ' + appTheme.palette.borderColor,
  lineHeight: '24px',
  textAlign: 'center',
  fontWeight: 'bold'
};

const tdStyle = {
  textAlign: 'left',
  padding: '4px 0'
};

class SATable extends EABInputComponent{

  constructor(props) {
    super(props);
  }


  getStItems = (index, array,rowIndex) =>{
    let { changedValues, validRefValues, readonly } = this.props;
    let channel = changedValues.channel || validRefValues.extra.channel;

    let disabled = false;
    if (rowIndex === 3 || rowIndex === 6 || rowIndex === 9) {
      disabled = true;
    }
    //FA = BROKER
    if ((channel === 'FA' || channel === 'BROKER')){
      return this.genTextViewRows(array, index, readonly || disabled,rowIndex);
    } else if (channel === 'AGENCY'){
      return this.genTextViewRows(array, 1, readonly || disabled,rowIndex);
    } else {
      return null;
    }
  }

  getSATableItemMapping = (itemId) => {
    let keys = ['AXA', 'Other'];
    let keyIndex = 0;
    let totalItemId = '', mappingItemId = '';
    keys.every((key, i) => {
        if (itemId.indexOf(key) >= 0) {
            totalItemId = itemId.split(key)[0];
            keyIndex = i;
            return false;
        }
        return true;
    });
    mappingItemId = (totalItemId) ? totalItemId + keys[(keyIndex + 1) % 2] : '';
    let returnMapping = (mappingItemId) ? {
        itemId,
        mappingItemId,
        totalItemId
    } : null;
    return returnMapping;
  }

  getShowRedLine(changedValues,itemId,isFA){
    if (itemId.indexOf('AXA') >= 0 || itemId.indexOf('Other') >= 0) {
      let mapping = this.getSATableItemMapping(itemId);
      if (mapping){

        let {mappingItemId,totalItemId} = mapping;
        let itemValue = Number(changedValues[itemId]);
        let mappingItemValue = Number(changedValues[mappingItemId]);
        let totalItemValue = Number(changedValues[totalItemId]);
        if (isFA){
          if (itemId.indexOf('TotalPrem') >= 0){
            if (itemValue === 0 && mappingItemValue === 0 && totalItemValue === 0){
              return true;
            }
          }
          else {
            let exArr = ['existLife', 'existCi', 'existTpd', 'existPaAdb'];
            let pendArr = ['pendingLife', 'pendingTpd', 'pendingCi', 'pendingPaAdb'];
            let rpArry = ['replaceLife', 'replaceCi', 'replaceTpd', 'replacePaAdb'];
            let itemRow = [];
            if (exArr.indexOf(totalItemId) >= 0){
              itemRow = exArr;
            }
            else if (pendArr.indexOf(totalItemId) >= 0){
              itemRow = pendArr;
            }
            else if (rpArry.indexOf(totalItemId) >= 0){
              itemRow = rpArry;
            }
            for (let i = 0; i < 4; i++) {
              if (changedValues[itemRow[i]] > 0){
                return false;
              }
            }
            return true;
          }
        }
        else {
          if (itemId.indexOf('exist') >= 0 || itemId.indexOf('replace') >= 0){
            if (itemValue === 0 && mappingItemValue === 0 && totalItemValue !== 0){
              return true;
            }
          }
          else if (itemId.indexOf('pending') >= 0){
            if (itemId.indexOf('TotalPrem') >= 0){
              if (itemValue === 0 && mappingItemValue === 0 && totalItemValue === 0){
                return true;
              }
            }
            else {
              let pendArr = ['pendingLife', 'pendingTpd', 'pendingCi', 'pendingPaAdb'];
              for (let i = 0; i < 4; i++) {
                if (changedValues[pendArr[i]] > 0){
                  return false;
                }
              }
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  handleReplaceAllZero = (itemId, changedValues) => {
    if (itemId.indexOf('replace') >= 0) {

      let rpArrayAXA = ["replaceLifeAXA", "replaceTpdAXA", "replaceCiAXA", "replacePaAdbAXA", "replaceTotalPremAXA"];
      let rpArrayOther = ["replaceLifeOther", "replaceTpdOther", "replaceCiOther", "replacePaAdbOther", "replaceTotalPremOther"];
      if (rpArrayAXA.indexOf(itemId) >= 0 || rpArrayOther.indexOf(itemId) >= 0) {
        let isAXAAllZero = true;
        let isOtherAllZero = true;
        for (let i = 0; i < 5; i++) {
          const key = rpArrayAXA[i];
          if (changedValues[key]){
            isAXAAllZero = false;
            break;
          }
        }
        for (let i = 0; i < 5; i++) {
          const key = rpArrayOther[i];
          if (changedValues[key]){
            isOtherAllZero = false;
            break;
          }
        }
        if (isAXAAllZero){
          this.props.handleChange('replacePolTypeAXA', '-');
        }
        else if (!isAXAAllZero && changedValues['replacePolTypeAXA'] === '-'){
          this.props.handleChange('replacePolTypeAXA', '');
        }
        if (isOtherAllZero){
          this.props.handleChange('replacePolTypeOther', '-');
        }
        else if (!isOtherAllZero && changedValues['replacePolTypeOther'] === '-'){
          this.props.handleChange('replacePolTypeOther', '');
        }
      }
    }
  }

  onSATableItemChange = (value,itemId) =>{
    let { changedValues, validRefValues } = this.props;
    let channel = changedValues.channel || validRefValues.extra.channel;

    if (typeof (value) === 'string') {
      value = Number(value.replace('.', ''));
    }
    if ((channel === 'FA' || channel === 'BROKER')){
      this.props.handleChange(itemId, value);
      let mapping = this.getSATableItemMapping(itemId);
      if (mapping){
        let {mappingItemId,totalItemId} = mapping;
        let mappingItemValue = 0;
        let totalItemValue = 0;
        if (itemId.indexOf('exist') >= 0 || itemId.indexOf('pending') >= 0 || itemId.indexOf('replace') >= 0){
          mappingItemValue = changedValues[mappingItemId];
          totalItemValue = Number(value) + Number(mappingItemValue);
          this.props.handleChange(totalItemId, totalItemValue);
        }
        this.handleReplaceAllZero(itemId,changedValues);
      }
    } else if (channel === 'AGENCY'){
      let mapping = this.getSATableItemMapping(itemId);
      if (mapping){
        let {mappingItemId,totalItemId} = mapping;
        let mappingItemValue = 0;
        let totalItemValue = 0;

        if (itemId.indexOf('exist') >= 0 || itemId.indexOf('replace') >= 0){
          totalItemValue = changedValues[totalItemId];
          if ( Number(value) <=  Number(totalItemValue)){
            this.props.handleChange(itemId, value);
            mappingItemValue = Number(totalItemValue) - Number(value);
            this.props.handleChange(mappingItemId, mappingItemValue);
          }
        } else if (itemId.indexOf('pending') >= 0){
          this.props.handleChange(itemId, value);
          mappingItemValue = changedValues[mappingItemId];
          totalItemValue = Number(value) + Number(mappingItemValue);

          this.props.handleChange(totalItemId, totalItemValue);
        }
        this.handleReplaceAllZero(itemId,changedValues);
      }
    }
  }


  genTextViewRows = (array, index, disabled,rowIndex)=>{
    let { values, changedValues ,validRefValues} = this.props;

    let channel = changedValues.channel || validRefValues.extra.channel;
    let isFA = false;
    //FA = BROKER
    if ((channel === 'FA' || channel === 'BROKER')){
      isFA = true;
    } else if (channel === 'AGENCY'){
      isFA = false;
    }

    //handle old data for exist / replace
    if (values.rop) {
      if (array[0].indexOf('exist') >= 0 || array[0].indexOf('replace') >= 0) {
        values = values.rop;
        changedValues = changedValues.rop;
      }
    }

    let rows = [];
    let style = tdStyle;
    if (rowIndex ===3 || rowIndex ===6  || rowIndex ===9){
      style = Object.assign({},tdStyle, { fontWeight: 'bold'});
    }

    for(let i = 0; i < array.length; i++){
        let value = changedValues[array[i]];
        if(!value || value == ''){
          value = 0;
        }
        let template = {
            "id": array[i],
            "max":9999999999,
            "type":"text",
            "subType":"currency",
            "min":0,
            "isCustom":true,
            "isROP":true,
            "mandatory":true,
            "disabled":disabled
          }

        rows.push(
          <td style={style} key={"td" + index + "-" + i}>
            <EABTextField
              ref={ref => { this[array[i]] = ref }}
              key={array[i]}

              style={{ width: '110px'}}
              inputStyle={{ width: '110px'}}
              template={template}
              values={values}
              changedValues={changedValues}
              showRedLine = {this.getShowRedLine(changedValues,array[i],isFA)}
              handleChange={this.props.handleChange}
              requestChange={this.onSATableItemChange} />
          </td>)
      }
      return rows;
  }

  genPolicyTypeText(id){
    let { values, changedValues, readonly } = this.props;
    let typeOfPolFieldStyle = Object.assign({},tdStyle, { textAlign: 'center'});

    let mandatory = false, disabled = true;

    if (id === 'replacePolTypeAXA') {
      let rpArrayAXA = ["replaceLifeAXA", "replaceTpdAXA", "replaceCiAXA", "replacePaAdbAXA", "replaceTotalPremAXA"];
      for (let i = 0; i < 5; i++) {
        const key = rpArrayAXA[i];
        if (changedValues[key]){
          mandatory = true;
          disabled = false;
          break;
        }
      }
    } else if (id === 'replacePolTypeOther') {
      let rpArrayOther = ["replaceLifeOther", "replaceTpdOther", "replaceCiOther", "replacePaAdbOther", "replaceTotalPremOther"];
      for (let i = 0; i < 5; i++) {
        const key = rpArrayOther[i];
        if (changedValues[key]){
          mandatory = true;
          disabled = false;
          break;
        }
      }
    }
    let template = {
      "id": id,
      "max":30,
      "type":"text",
      "isCustom":true,
      "isROP":true,
      "mandatory":mandatory,
      "disabled":disabled || readonly,
      'style':{width: '100px',textAlign: 'center'}
    };
    let showRedLine = mandatory && (changedValues[id].trim() === '');

    return <td style={typeOfPolFieldStyle} key={"td" + "-" + id}>
      <EABTextField
        ref={ref => { this[id] = ref; }}
        key={id}
        isROP={true}
        inputStyle={{ width: '100px',textAlign: 'center'}}
        template={template}
        values={values}
        changedValues={changedValues}
        showRedLine = {showRedLine}
        handleChange={this.props.handleChange}/></td>
  }

  setZero(changedValues){
    let policies = [
      'pendingLife', 'pendingCi', 'pendingTpd', 'pendingPaAdb', 'pendingTotalPrem',
      'existLife', 'existCi', 'existTpd', 'existPaAdb', 'existTotalPrem',
      'replaceLife', 'replaceCi', 'replaceTpd', 'replacePaAdb', 'replaceTotalPrem',
      'pendingLifeAXA', 'pendingCiAXA', 'pendingTpdAXA', 'pendingPaAdbAXA', 'pendingTotalPremAXA',
      'existLifeAXA', 'existCiAXA', 'existTpdAXA', 'existPaAdbAXA', 'existTotalPremAXA',
      'replaceLifeAXA', 'replaceCiAXA', 'replaceTpdAXA', 'replacePaAdbAXA', 'replaceTotalPremAXA',
      'pendingLifeOther', 'pendingCiOther', 'pendingTpdOther', 'pendingPaAdbOther', 'pendingTotalPremOther',
      'existLifeOther', 'existCiOther', 'existTpdOther', 'existPaAdbOther', 'existTotalPremOther',
      'replaceLifeOther', 'replaceCiOther', 'replaceTpdOther', 'replacePaAdbOther', 'replaceTotalPremOther'
    ];
    let size = policies.length;
    for (let i = 0; i < size; i++) {
      const policy = policies[i];
      if (changedValues[policy] === ''){
        changedValues[policy] = 0;
      }
    }
  }

  genErrViewRow(index,errorMsg){
    let { template, presentation, changedValues } = this.props;
    let { havExtPlans, havAccPlans, havPndinApp, isProslReplace } = changedValues;
    if (index === 1 && (havExtPlans && havExtPlans === 'Y' && presentation.indexOf('havExtPlans') >= 0 || havAccPlans && havAccPlans === 'Y' && presentation.indexOf('havAccPlans') >= 0) ){
      return <tr key={"saTable-errRow1"} style={{borderBottom:'1px solid ' + appTheme.palette.borderColor}}>
        <td key={"saTable-errMsg1"} colSpan='8' style={tdStyle}>{_.isString(errorMsg) && errorMsg !== '' ? (<div style={{width:'100%',display: 'table-cell'}}>{_.isString(errorMsg) ? <ErrorMessage template={template} message={errorMsg} style={{top: '0px', left: '0px', textAlign: 'left'}}/> : null}</div>) : null}</td>
      </tr>
    }
    if (index === 2 && havPndinApp && havPndinApp === 'Y'){
      return <tr key={"saTable-errRow2"} style={{borderBottom:'1px solid ' + appTheme.palette.borderColor}}>
        <td key={"saTable-errMsg2"} colSpan='8' style={tdStyle}>{_.isString(errorMsg) && errorMsg !== '' ? (<div style={{width:'100%',display: 'table-cell'}}>{_.isString(errorMsg) ? <ErrorMessage template={template} message={errorMsg} style={{top: '0px', left: '0px', textAlign: 'left'}}/> : null}</div>) : null}</td>
      </tr>
    }
    if (index === 3 && isProslReplace && isProslReplace === 'Y'){
      return <tr key={"saTable-errRow3"} style={{borderBottom:'1px solid ' + appTheme.palette.borderColor}}>
        <td key={"saTable-errMsg3"} colSpan='8' style={tdStyle}>{_.isString(errorMsg) && errorMsg !== '' ? (<div style={{width:'100%',display: 'table-cell'}}>{_.isString(errorMsg) ? <ErrorMessage template={template} message={errorMsg} style={{top: '0px', left: '0px', textAlign: 'left'}}/> : null}</div>) : null}</td>
      </tr>
    }
    return null;
  }

  render() {

    let { presentation, changedValues } = this.props;
    let { havExtPlans, havAccPlans, havPndinApp, isProslReplace } = changedValues;
    this.setZero(changedValues);

    let totalFieldStyle = Object.assign({},tdStyle, { fontWeight: 'bold',fontStyle: 'italic',textAlign: 'center'});
    let typeOfPolFieldStyle = Object.assign({},tdStyle, { textAlign: 'center'});
    let companyFieldStyle = Object.assign({},tdStyle, { textAlign: 'center'});

    var index = 1;

    let errorMsg = this.getErrMsg();
    let errorMsg1 = '', errorMsg2 = '', errorMsg3 = '';
    if (_.isArray(errorMsg)) {
      errorMsg1 = errorMsg[0];
      errorMsg2 = errorMsg[1];
      errorMsg3 = errorMsg[2];
    }

    let ext = null;
    let ext2 = null;
    let ext3 = null;
    let extArrayAXA = ["existLifeAXA", "existTpdAXA", "existCiAXA", "existPaAdbAXA", "existTotalPremAXA"];
    let extArrayOther = ["existLifeOther", "existTpdOther", "existCiOther", "existPaAdbOther", "existTotalPremOther"];
    let extArray = ["existLife", "existTpd", "existCi", "existPaAdb", "existTotalPrem"];
    if(havExtPlans && havExtPlans == "Y" && presentation.indexOf('havExtPlans') >= 0 || havAccPlans && havAccPlans == "Y" && presentation.indexOf('havAccPlans') >= 0){

      let eRows1 = this.getStItems(index, extArrayAXA,1);
      let eRows2 = this.getStItems(index, extArrayOther,2);
      let eRows3 = this.getStItems(index, extArray,3);
      ext = <tr key={"r_"+index+"_1"}>
          <td rowSpan='3' style={tdStyle}><b>1. Existing Policies</b></td>
          <td style={companyFieldStyle}>AXA</td>
          <td style={typeOfPolFieldStyle}>-</td>
          {eRows1}
      </tr>
      ext2 = <tr key={"r_"+index+"_2"}>
          <td style={companyFieldStyle}>Other Company(ies)</td>
          <td style={typeOfPolFieldStyle}>-</td>
          {eRows2}
      </tr>
      ext3 = <tr key={"r_"+index+"_3"}>
        <td style={totalFieldStyle}>Total</td>
        <td style={typeOfPolFieldStyle}></td>
        {eRows3}
      </tr>

      index += 1;
    }else{
      for(let jj = 0; jj < extArray.length; jj++){
        changedValues[extArrayAXA[jj]] = 0;
        changedValues[extArrayOther[jj]] = 0;
        changedValues[extArray[jj]] = 0;
      }
    }

    let pend = null;
    let pend2 = null;
    let pend3 = null;
    let pendArryAXA = ["pendingLifeAXA", "pendingTpdAXA", "pendingCiAXA", "pendingPaAdbAXA", "pendingTotalPremAXA"];
    let pendArryOther = ["pendingLifeOther", "pendingTpdOther", "pendingCiOther", "pendingPaAdbOther", "pendingTotalPremOther"];
    let pendArry = ["pendingLife", "pendingTpd", "pendingCi", "pendingPaAdb", "pendingTotalPrem"];
    if(havPndinApp && havPndinApp == "Y"){
      let pRows1 = this.getStItems(index, pendArryAXA,4);
      let pRows2 = this.getStItems(index, pendArryOther,5);
      let pRows3 = this.getStItems(index, pendArry,6);
      pend = <tr key={"r_"+index}>
              <td rowSpan='3' style={tdStyle}><b>2. Pending Applications</b><br/>(excluding this application)</td>
              <td style={companyFieldStyle}>AXA</td>
              <td style={typeOfPolFieldStyle}>-</td>
              {pRows1}
             </tr>
      pend2 = <tr key={"r_"+index+"_2"}>
                <td style={companyFieldStyle}>Other Company(ies)</td>
                <td style={typeOfPolFieldStyle}>-</td>
                {pRows2}
            </tr>
      pend3 = <tr key={"r_"+index+"_3"}>
                <td style={totalFieldStyle}>Total</td>
                <td style={typeOfPolFieldStyle}></td>
                {pRows3}
              </tr>
      index += 1;
    }else{
      for(let j = 0; j < pendArry.length; j++){
        changedValues[pendArryAXA[j]] = 0;
        changedValues[pendArryOther[j]] = 0;
        changedValues[pendArry[j]] = 0;
      }
    }

    let replce = null;
    let replce2 = null;
    let replce3 = null;
    let rpArrayAXA = ["replaceLifeAXA", "replaceTpdAXA", "replaceCiAXA", "replacePaAdbAXA", "replaceTotalPremAXA"];
    let rpArrayOther = ["replaceLifeOther", "replaceTpdOther", "replaceCiOther", "replacePaAdbOther", "replaceTotalPremOther"];
    let rpArray = ["replaceLife", "replaceTpd", "replaceCi", "replacePaAdb", "replaceTotalPrem"];
    let rpPolTypeArray = ['replacePolTypeAXA','replacePolTypeOther'];
    if(isProslReplace && isProslReplace =="Y"){
      let rpRows1 = this.getStItems(index,rpArrayAXA,7);
      let rpRows2 = this.getStItems(index,rpArrayOther,8);
      let rpRows3 = this.getStItems(index,rpArray,9);
      let rpPolicyTypeAXA = this.genPolicyTypeText('replacePolTypeAXA');
      let rpPolicyTypeOther = this.genPolicyTypeText('replacePolTypeOther');
      replce = <tr key={"r_"+index}>
                <td rowSpan='3' style={tdStyle}><b>3. Policy to be Replaced</b></td>
                <td style={companyFieldStyle}>AXA</td>
                {rpPolicyTypeAXA}
                {rpRows1}
              </tr>
      replce2 = <tr key={"r_"+index+"_2"}>
                <td style={companyFieldStyle}>Other Company(ies)</td>
                {rpPolicyTypeOther}
                {rpRows2}
              </tr>
      replce3 = <tr key={"r_"+index+"_3"}>
                <td style={totalFieldStyle}>Total</td>
                <td style={typeOfPolFieldStyle}></td>
                {rpRows3}
              </tr>
    }else{
      let size = rpArray.length;
      for (let i = 0; i < size; i++) {
        changedValues[rpArray[i]] = 0;
        changedValues[rpArrayAXA[i]] = 0;
        changedValues[rpArrayOther[i]] = 0;
      }
      size = rpPolTypeArray.length;
      for (let i = 0; i < size; i++) {
        changedValues[rpPolTypeArray[i]] = '-';
      }
    }

    if(!ext && !ext2 && !ext3 && !pend && !pend2 && !pend3 && !replce && !replce2 && !replce3)
      return null;

    return (
		  <div style={{overflowX:'scroll'}}>
            <table style={{borderTop:'1px solid ' + appTheme.palette.borderColor}}>
              <thead>
                <tr key={"header1"} style={{background: 'rgba(0, 0, 143, 0.07)', color: 'rgb(0, 0, 143)'}} >

                    <td rowSpan='2' style={Object.assign({},thStyle, {width: '20%', padding: '0 8px'})}>Type of Insurance Application</td>
                    <td rowSpan='2' style={Object.assign({},thStyle, {width: '20%'})}>Company</td>
                    <td rowSpan='2' style={Object.assign({},thStyle, {width: '20%', padding: '0 8px'})}>Type of Policy(ies)</td>
                    <td colSpan='4' style={{width: '45%',textAlign:"center", fontWeight: 'bold', padding: '24px 0'}}>Total Sum Assured (S$)</td>
                    <td rowSpan='2' style={Object.assign({},thStyle, {width: '16%',textAlign:"left", padding: '0', width: '16%',})}>Total Annual Premium (S$)</td>
                </tr>
                <tr key={"header2"} style={{background: 'rgba(0, 0, 143, 0.07)', color: 'rgb(0, 0, 143)'}}>
                    <td style={Object.assign({},thStyle,{width: '14px'})}>Life</td>
                    <td style={Object.assign({},thStyle,{width: '14px'})}>TPD</td>
                    <td style={Object.assign({},thStyle,{width: '14px'})}>CI</td>
                    <td style={Object.assign({},thStyle,{width: '14px'})}>PA/ADB</td>
                </tr>
              </thead>
              <tbody>
                {ext}
                {ext2}
                {ext3}
                {this.genErrViewRow(1,errorMsg1)}
                {pend}
                {pend2}
                {pend3}
                {this.genErrViewRow(2,errorMsg2)}
                {replce}
                {replce2}
                {replce3}
                {this.genErrViewRow(3,errorMsg3)}
              </tbody>
            </table>
      </div>
    )

  }
}

export default SATable;
