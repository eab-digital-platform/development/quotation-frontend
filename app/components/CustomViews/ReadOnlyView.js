import React from 'react';
import PropTypes from 'propTypes';
import {Paper, TextField, RaisedButton, FontIcon} from 'material-ui';
import EABComponent from '../EABComponent';


class ReadOnlyView extends TextField {

  render() {

    var {label, value='-', id, style} = this.props;

    var lableStyle = {
      color: '#2696CC',
      fontWight:'normal',
      fontSize:'14px',
    };

    var valueStyle = {
      color: '#000',
      fontWeight: 'bold',
      margin: '4px 0px 15px 0px',
      fontSize: '15px',
    };
    var lbKey = id + "L";
    var pKey = id + "V";
    var divKey = id + "Box";

    return (
      <div id={divKey} key={divKey} style={style}>
        <label id={lbKey} key={lbKey} style={lableStyle}>{label}</label>
        <p id={pKey} key={pKey} style={valueStyle}>{value}</p>
      </div>
    );
  }
}

ReadOnlyView.propTypes = Object.assign({}, ReadOnlyView.propTypes, {
  label:PropTypes.string,
  value:PropTypes.string,
  id: PropTypes.string,
})

export default ReadOnlyView;
