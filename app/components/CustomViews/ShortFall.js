import React from 'react';
import EABInputComponent from './EABInputComponent';
import styles from '../Common.scss';
import ToolTips from './ToolTips';
const shortFall = '(Shortfall): ';
const surPlus = '(Surplus): ';
const shortFallHints = 'Shortfall is defined as positive Gap between (Your Need- Your Have) and can be prioritized for product recommendation.';
const surPlusHints = 'Surplus defined as negative gap between (Your Need- Your Have), hence no shortfall';
const retPlanId = 'rPlanning';
const annRetIncomeId = 'annRetireIncome';
import * as _ from 'lodash';

class ShortFall extends EABInputComponent{
  constructor(props){
    super(props);
  }

  displayMultiShortFall(Arr = []){
    let items =[];
    let numStyle;
    let hints;

    _.forEach(Arr, (obj, index) =>{
      if (obj.ind === shortFall){
        numStyle = styles.NonInputRedNum;
        hints = shortFallHints;
      } else {
        numStyle = styles.NonInputGreenNum;
        hints = surPlusHints;
      }
      let _key = "shortfall-item-" + index;
      items.push(
        <div key={_key} className={styles.shortfallItem}>
          <ToolTips hints={hints} containerStyle={{display: 'inline-block'}}/>
          <div className={styles.NonInputValue1} style={{display: 'inline-block', lineHeight: '45px'}}>
            {obj.name}
          </div>
          <div className={styles.NonInputValue1} style={{display: 'inline-block', lineHeight: '45px', paddingRight: '12px'}}>
            {obj.ind}
          </div>
          <div className={numStyle} style={{display: 'inline-block', lineHeight: '45px', paddingRight: '24px'}}>
            {obj.value}
          </div>
        </div>
      );
    });
    return items;
  }

  render() {
    let {template,handleChange,style,values,changedValues,index,rootValues,labelStyle} = this.props;

    let {id, type, mandatory, subType, disabled, format, decimalNeeded=0, hints, validation} = template;

    let { lang, langMap } = this.context
    let {value} = validation;
    let validationId = value.id ? value.id : null;
    let isrPlanning = validationId == retPlanId ? true : false;
    let cValue = changedValues[id] != undefined?changedValues[id]:this.getValue("");
    let rValue = isrPlanning ? changedValues[annRetIncomeId] : this.getValue("");
    let isShortfall = cValue < 0 ? true : false;
    let displayText;
    let displayTextRetire;
    let valueColorClass;

    if (cValue >= 0) {
      displayText = getLocalizedText(langMap, getLocalText(lang, 'Total Surplus:'));
      hints =  getLocalizedText(langMap, getLocalText(lang, surPlusHints));
      valueColorClass = styles.NonInputGreenNum + ' ' + styles.alignCenter;
    }else {
      displayText = getLocalizedText(langMap, getLocalText(lang, 'Total Shortfall:'));
      displayTextRetire = isrPlanning ? getLocalizedText(langMap, getLocalText(lang, 'Annual Retirement Income:')) : '';
      hints =  getLocalizedText(langMap, getLocalText(lang, shortFallHints));
      valueColorClass = styles.NonInputRedNum + ' ' + styles.alignCenter;
    }
    if (cValue != 'NotShow') {
      cValue = Math.abs(cValue).toFixed(decimalNeeded);
      rValue = Math.abs(rValue).toFixed(decimalNeeded);
    }
    if(subType === "currency" && cValue != 'NotShow'){
      cValue = getCurrency(Number(cValue), '$', decimalNeeded);
      rValue = getCurrency(Number(rValue), '$', decimalNeeded);
    }
    let display;
    let rDisplay;
    if (cValue != 'NotShow') {
      display = format? getLocalText(lang, format).replace("%s", cValue): cValue.toString();
      rDisplay = rValue.toString();
    }


    let errorMsg = this.getErrMsg();

    let multiShortFall =[];
    let tempTot;
    let tempdisplayText;

    let targetShortfalls = _.get(template, 'targetShortfalls', []);
    if (cValue === 'NotShow' && _.size(targetShortfalls)) {
      _.forEach(targetShortfalls, targetShortfall => {
        if (window.showCondition(null, targetShortfall, values, changedValues, this.state.changedValues, null, this.context)) {
          tempTot = _.get(changedValues, targetShortfall.value);
          tempdisplayText = (tempTot < 0) ? shortFall : surPlus;
          tempTot = window.getCurrency(Math.abs(tempTot || 0), '$', decimalNeeded);
          multiShortFall.push({name: targetShortfall.title, ind: tempdisplayText, value: tempTot});
        }
      });
    } else if (cValue === 'NotShow' && _.isObject(template.MultipleShortFall) && template.MultipleShortFall.indicator && template.MultipleShortFall.reference && template.MultipleShortFall.name) {
      for(let i =0; i < changedValues[template.MultipleShortFall.indicator]; i++){
        tempTot = _.get(changedValues, template.MultipleShortFall.reference + '[' + i + ']' + 'totShortfall');
        tempdisplayText = (tempTot < 0) ? shortFall : surPlus;
        tempTot = getCurrency(Math.abs(tempTot || 0), '$', decimalNeeded);
        multiShortFall.push({name: template.MultipleShortFall.name + ' ' + Number(i + 1), ind: tempdisplayText, value: tempTot});
      }
    }

    if(isrPlanning && isShortfall){
      return(
        (cValue == 'NotShow') ? <div className={styles.FixFooterContainer}>{this.displayMultiShortFall(multiShortFall)}</div> :
        <div className={styles.FixFooterContainer + ' ' + styles.alignRight} style={{display: 'block'}}>
            <div className={styles.FixFooterStyle}>
              {(hints) ? <ToolTips hints={hints} Iconstyle={{bottom: '12px', margin: '12px'}}/> : undefined}
              <div className={styles.ShowToolTips}></div>
              <div className={styles.NonInputValue1 + ' ' + styles.alignCenter}>{displayText}</div>
              <div className={valueColorClass}>{display}</div>
            </div>
            <div className={styles.FixFooterStyle}>       
                <div className={styles.NonInputValue1 + ' ' + styles.alignCenter}>{displayTextRetire}</div> 
                <div className={valueColorClass}>{rDisplay}</div>
            </div> 
        </div>
      ) 
    }else{
      return(
        (cValue == 'NotShow') ? <div className={styles.FixFooterContainer}>{this.displayMultiShortFall(multiShortFall)}</div> :
        <div className={styles.FixFooterContainer + ' ' + styles.alignRight}>
            <div className={styles.FixFooterStyle}>
              {(hints) ? <ToolTips hints={hints} Iconstyle={{bottom: '12px', margin: '12px'}}/> : undefined}
              <div className={styles.ShowToolTips}></div>
              <div className={styles.NonInputValue1 + ' ' + styles.alignCenter}>{displayText}</div>
              <div className={valueColorClass}>{display}</div>
            </div>
        </div>
      ) 
    }
  }

}

export default ShortFall;
