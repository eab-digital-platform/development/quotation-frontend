import React from 'react';
import {Tabs, Tab} from 'material-ui';
import EABComponent from '../Component';
import {getIcon} from '../Icons/index';
import * as _ from 'lodash';
import styles from '../Common.scss';

import * as _n from '../../utils/needs';
import * as _c from '../../utils/client';


class Item extends EABComponent {

  constructor(props, context) {
      super(props);
      let {store} = context;
      let {needs} = store.getState();
      let {pda: {applicant}} = needs;
      this.state={
        template: props.template,
        selectedIndex: 1,
        applicant
      }
  };

  componentWillReceiveProps(nextProps){
    if(!isEqual(this.props.template, nextProps.template)){
      let {selectedIndex} = this.state;
      let {id: cid} = this.getSelectedItem(selectedIndex);
      let index = _.findIndex(nextProps.template, template=>template.id==cid)+1;
      let newState = {
        template: nextProps.template,
        selectedIndex: index<1?1:index
      };
      
      if (this.refs.panel) {
        this.refs.panel.scrollTop = 0;
      }

      let isAppTemplateIndex = _.findIndex(nextProps.template, t=>(t.template && (t.template.subType=="proposer" || t.template.subType=="insured")));
      if (isAppTemplateIndex >= 0) {
        newState.selectedIndex = 1;
      }

      this.setState(newState);
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null
    }
  }

  storeListener=()=>{
      let {store} = this.context;
      let {needs} = store.getState();
      let {pda: {applicant}} = needs;

      let newState = {};

      if(!isEqual(this.state.applicant, applicant)){
        newState.applicant = _.cloneDeep(applicant);
      }

      if(!isEmpty(newState))
        this.setState(newState);
  }


  getSelectedItem=(index)=>{
    let {template} = this.state;
    if(template){
      return template[index-1];
    }
  }

  initValue=(id, value, relationship, isCValue)=>{
    let cValue = null;
    if(relationship){
      let r = _.toLower(relationship);
      let {relationshipType: rType} = _n;
      if(r === rType.OWNER){
        cValue = value.owner;
      }
      else if(r === rType.SPOUSE){
        cValue = value.spouse;
      }
      else if(r === rType.DEPENDANTS){
        let index = -1;
        _.filter(value.dependants, (dependant, i)=>{
          if(dependant.cid == id)
            index = i;
        })
        if (index !== -1){
           cValue = value.dependants[index];
        }

      }
      if(isCValue && cValue && (cValue.init || cValue.init==null)){
        cValue.init = false;
        if(_.isFunction(this.props.onChange)){
          this.props.onChange(true);
        }
        this.props.validate();
      }
    }

    return cValue;
  }

  initTabValue=(values, tabTemplate)=>{
    let {template} = this.state;
    let proposerTab = _.filter(template, (tabTmpl) => { return _.toLower(tabTmpl.template.subType) === 'proposer'; });

    var cValues = null;
    let {selectedIndex} = this.state;

    if (tabTemplate.subType === 'proposer'){
      if (!values.proposer){
        values.proposer = {};
      }
      cValues = values.proposer;
    } else if (tabTemplate.subType === 'insured'){
      let index = selectedIndex - proposerTab.length - 1;
      if (!values.insured){
        values.insured = [];
      }

      if (!values.insured[index]){
        values.insured[index] = {};
      }
      cValues = values.insured[index];
    }

    return cValues;
  }

  onChange=(index)=>{
    let {validRefValues} = this.context;
    //don't do anything if index = current index
    let {selectedIndex, changedValues} = this.state;
    let {rootValues, values, error, skipCheck, onTabChange} = this.props;

    if(index == this.state.selectedIndex)
      return;

    if (this.refs.panel) {
      this.refs.panel.scrollTop = 0;
    }

    //pass only when current item is validate
    let currentItem = this.getSelectedItem(selectedIndex);
    let scrollTop = ()=>{this.refs.panel.scrollTop = 0;};
    if(currentItem.template.error && !this.props.skipCheck)
      this.setState({selectedIndex: selectedIndex}, scrollTop);
    else{
      if(_.isFunction(onTabChange)){
        onTabChange(()=>{
          this.setState({selectedIndex: index}, () => {
            scrollTop();
            this.props.validate();
          })        })
      }else {
        this.setState({selectedIndex: index}, scrollTop)
      }
    }
  }



  getItem=()=>{
    let {
      selectedIndex
    } = this.state;

    let {
      renderItems,
      values,
      changedValues,
      error,
      showCondItems
    } = this.props;

    let item = [];
    let {
      template,
      id,
       relationship // for needs
     } = this.getSelectedItem(selectedIndex);


    let handleChange = () => {
      if (_.isFunction(this.props.handleChange)){
        this.props.handleChange();
      }
    };

    if (template.subType === 'proposer' || template.subType === 'insured'){
     var cValue = this.initTabValue(changedValues, template);
     var value = this.initTabValue(values, template);
     var err = this.getAppFormError(template, id);
    }
    else {
      var cValue = this.initValue(id, changedValues, relationship, true);
      var value = this.initValue(id, values, relationship);
      var err = this.getError(template, id);
    }

    if (typeof renderItems === 'function'){
      let extrxPara = relationship ? relationship : {values: cValue, type: 'tab'};
      renderItems(item, template, value, cValue, err, id, handleChange, extrxPara);
    }

    return item;


  }

  getError=(template, id)=>{
    let {error} = this.props;
    let {subType} = template, err;
    let {relationshipType: rType} = _n;
    subType = _.toLower(subType);
    if(subType && [rType.OWNER, rType.SPOUSE, 'proposer'].indexOf(subType)>-1){
      err = error[subType];
    }
    else if(subType && [rType.DEPENDANTS, 'insured'].indexOf(subType)>-1){
      err = error[subType] && error[subType][id];
    }
    return err;
  }

  getAppFormError=(template, id)=>{
    let {error} = this.props;
    let {subType} = template;
    subType = _.toLower(subType);

    let err = null;
    if(subType == "insured"){
      err = error[subType] && error[subType][id];
    }else{
      err = error[subType];
    }
    return err;
  }

  render(){
	  let {muiTheme, router, lang, langMap} = this.context;
    let {selectedIndex, template} = this.state;
    let {error, isApp} = this.props;

    let item = this.getItem();


    let iconColor = muiTheme.palette.alternateTextColor;
    let fontColor = muiTheme.palette.alternateTextColor;
    let warningColor = muiTheme.palette.warningColor;

    let templateTitleArr =[];
    _.forEach(template, obj => {
      templateTitleArr.push(obj.title);
    })
    let longestStrLegnth = longestStringinArray(templateTitleArr);
    let calcTabsPixel = longestStrLegnth.length * 10;
    if (calcTabsPixel < 200)
      calcTabsPixel = 200;

    let minHeight;
    if (window.matchMedia('(min-width: ' + calcTabsPixel * template.length + 'px)').matches) {
      minHeight = { minHeight: isApp ? '53px' : '48px' };
    } else {
      minHeight = { minHeight: '58px' };
    }
    return (
      <div className={styles.Page}>
        <div className={styles.Tabs} style={minHeight}>
          <Tabs
            key="profile-tabs"
            style={{paddingLeft: '64px', width: 'calc(' + calcTabsPixel + 'px * ' + template.length +')'}}
            onChange={this.onChange}
            value={selectedIndex}
            >
            {
              template.map((tab, index)=>{
                let err = null;
                if(tab.template.subType == "proposer" || tab.template.subType == "insured"){
                  err = this.getAppFormError(tab.template, tab.id);
                }else{
                  err = this.getError(tab.template, tab.id);
                }

                return (
                  <Tab key={'tab-' + tab.id} label={
                    <div style={{display: 'flex', minHeight: '0px'}}>
                       <div style={{padding: '5px'}}>{getLocalText(lang, tab.title)}</div>
                       {err && err.code ? <div>{getIcon('warning', warningColor)}</div> : undefined}
                    </div>}
                    value={Number(index)+1}
                    style={{width: calcTabsPixel + 'px'}}
                  />
                )
              })
            }
         </Tabs>
       </div>
       <div ref={"panel"} className={styles.Tab} style={{minHeight: "200px", flexGrow: 1, overflowY: 'auto', position: "static", padding: '24px 24px 300px 24px', backgroundColor: muiTheme.palette.panelBackgroundColor}}>
         {item}
       </div>
      </div>
	  )
  }
}
export default Item;
