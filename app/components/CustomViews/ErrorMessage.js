import React from 'react';
import PropTypes from 'prop-types';
import EABComponent from '../Component';
import styles from '../Common.scss';
import * as _ from 'lodash';

class ErrorMessage extends EABComponent {
    render(){
        let {muiTheme} = this.context;
        let {template, message, style} = this.props;
        const title = _.get(template, 'title');
        const errorFontSize = _.get(template, 'errorFontSize');
        if (errorFontSize) {
            style = _.assign({}, style, {fontSize: `${errorFontSize}px`, height: `${errorFontSize + 4}px`});
        }
        
        return (
            <div className={(title) ? styles.ErrorMsgWithTitle : styles.ErrorMsgNoTitle} style={style}>
                {message}
            </div>
        )
    }
}

ErrorMessage.propTypes = Object.assign({}, ErrorMessage.propTypes, {
    template: PropTypes.object, 
    message: PropTypes.any, 
    style: PropTypes.object
})

export default ErrorMessage;