import React, { Component, PropTypes } from 'react';

import EABComponent from '../../Component';
import Slider from './Slider';
import Slider1 from './Slider1';
import Slider2 from './Slider2';
import Slider3 from './Slider3';

import styles from '../../Common.scss';

class Sliders extends EABComponent {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      lastUpdateDate: null
    });
  }

  componentDidMount() {
    const {store} = this.context;
    if (!this.unsubscribe) {
      this.unsubscribe = store.subscribe(() => {
        const {app} = store.getState();
        if (this.state.lastUpdateDate !== app.lastUpdateDate) {
          this.setState({
            lastUpdateDate: app.profileUpdateDate
          });
        }
      });
    }
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
  }

  onSliderChange(index) {
    const {langMap} = this.context;
    let title;
    switch (index) {
      case 0:
        title = getLocalizedText(langMap, 'introduction.title.page_1');
        break;
      case 1:
        title = getLocalizedText(langMap, 'introduction.title.page_2');
        break;
      case 2:
        title = getLocalizedText(langMap, 'introduction.title.page_3');
        break;
    }
    this.context.updateAppbar({
      title: title
    });
  }

	render() {
		return(
			<Slider
				id="landSliderMain"
				showArrows={true}
				showNav={true}
        onSliderChange={(index) => this.onSliderChange(index)}
      >
				<Slider1 key={`main-slider-1`}/>
				<Slider2 key={`main-slider-2`}/>
				<Slider3 key={`main-slider-3`}/>
			</Slider>
		);
	}
}
export default Sliders;
