import React from "react";
import PropTypes from 'prop-types';
import {TextField, Card, Avatar} from "material-ui";

import * as _ from 'lodash';

import EABComponent from '../../Component';
import styles from "../../Common.scss";

const initState = {
  
}

class SearchPolicy extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, _.cloneDeep(initState));
  };

  init=()=>{
    this.state = {};
  }

  render() {

    return (
      <div>
        Policy
      </div>

    );
  }
}

export default SearchPolicy;
