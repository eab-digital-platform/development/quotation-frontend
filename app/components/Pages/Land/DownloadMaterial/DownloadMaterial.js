import React, {PropTypes} from 'react';

import EABComponent from '../../../Component';
import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';
import TextField from 'material-ui/TextField';
import {List, ListItem, makeSelectable} from 'material-ui';

import {getDownloadMaterial} from '../../../../actions/downloadMaterial';
import {getPdfToken} from '../../../../actions/proposal';

import MaterialSection from './MaterialSection';
import styles from '../../../Common.scss';
import eab from "../../../../img/eab.png";

const initState = {
  value: '',
  materialList:[],
  pageClosed: false
};

export default class DownloadMaterial extends EABComponent {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, _.cloneDeep(initState));
  };

  open() {
    this.floatingPage.open();
    this.init();
  }

  close() {
    this.setState({
      pageClosed: true
    });
    this.floatingPage.close();
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);  
    this.updateAppbar();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  updateAppbar() {
    const {langMap} = this.context;
    this.appbarPage.initAppbar({
      menu: {
        icon: 'back',
        action: () => {  
          this.appbarPage.updateSearchValue("");
          this.setState({
            value: ''
          });
          this.close();
        }
      },
      title: window.getLocalizedText(langMap, 'download_material.title'),
      onSearchChange: (orgValue, newValue)=>{
        this.appbarPage.updateAppbar({
          searchValue: newValue
        });

        this.setState({
          value: newValue
        })
      },
      itemsIndex: 0,
      items: [
      [{
        type: "flatButton",
        title: {
          "en": "CLEAR"
        },
        action: ()=>{
         this.appbarPage.updateSearchValue("");
          this.setState({
            value: ''
          })
        }
      }]
    ]
    });
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let state = this.context.store.getState();
      let newState = {};

      newState.materialList = state.downloadMaterial.materialList;

      if(!isEmpty(newState))
        this.setState(newState);
    }
  }

  init = () => {
    this.setState({
      pageClosed: false
    });
    getDownloadMaterial(this.context,() => {
      this.setState({});
    });     
  }

  removeDot = (docName) =>{  // to remove dot
    let linkName = "";
    linkName = docName.replaceAll(".","");
    return linkName;
  } 

  getLink = (docId,attchId,docName)=>{
    let linkName = this.removeDot(docName);
    getPdfToken(this.context, docId, attchId, (token) => {
      const link = document.createElement('a');
      link.href = window.genAttPath(token);
      link.download = linkName || 'test.pdf';
      //link.download = docId || 'test.pdf';
      link.click();
    });
  }

  createList=(mList)=>{
    let items = [];
    let materialList = mList || [];
    const masterList = {
      prodSumList : [],
      genProvList : [],
      inspireList : [],
      pulsarList  : [],
      propFormList: [],
      questionnaireList: [],
      nbFormList  : [],
      polFormList : [],
      gaahList    : [],
      physicianList   : []
    }

    _.forEach(materialList,(item)=>{
      switch (item.sectionId){
        case "prod":
          masterList.prodSumList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        case "gprov":
          masterList.genProvList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
         case "indf":
          masterList.inspireList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem2Col} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        case "pulf":
          masterList.pulsarList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem2Col} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        case "propform":
          masterList.propFormList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem2Col} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        case "ques":
          masterList.questionnaireList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem2Col} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        case "nbadmin":
          masterList.nbFormList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem2Col} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        case "polform":
          masterList.polFormList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        case "gaah":
          masterList.gaahList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        case "aps":
          masterList.physicianList.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem2Col} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />)
          break;
        default:
            break;
      }
    })  
    return masterList;
  }

  getSearchList =(mList)=>{
    let items = [];
    let {value} = this.state;
    let searchValues = _.toUpper(value).split(" ").filter(text=>!isEmpty(_.trim(text)));
    
    let materials = _.orderBy(mList, [material=>_.toUpper(mList.name)], 'asc');

    let maxSize = 6;
    let searchCnt = 0;

    _.forEach(materials, item=>{
      if (searchCnt >= maxSize) {
        return false; 
      }
      let tname = item.name;
      let found = true;

      _.forEach(searchValues, searchValue=>{
        if(!(_.toUpper(tname).indexOf(searchValue)>-1)){
             found = false;
           }
      })

      if(_.trim(value).length >= 1 && found){
        items.push(<ListItem primaryText={item.name != undefined ? item.name: null} className={styles.downloadItem} onClick={()=>{this.getLink(item.id,item.sectionId,item.name);}} />);
        searchCnt++;
      }    
    });
    return items;
  }


  render() {
    const {langMap} = this.context;
    let {pageClosed} = this.state;
    let materialList = _.cloneDeep(this.state.materialList) || [];
    let {prodSumList, genProvList, inspireList, pulsarList, propFormList, questionnaireList, nbFormList, polFormList, gaahList, physicianList} = this.createList(materialList);
    let searchList = this.getSearchList(materialList);
     
    return (
      <FloatingPage ref={ref => { this.floatingPage = ref; }}>
        <AppbarPage ref={(ref) => { this.appbarPage = ref; }} search showShadow={false}> 

        <div className={styles.Tabs} style={{minHeight: '10px'}}></div>
        
        {searchList.length >0 &&
        <div className={styles.dropdownList} >
          <List className={styles.downloadItemList}>
            {searchList}
          </List>
        </div>
        }

        <div className={styles.easeDownloadPage} onTouchTap={()=>{this.setState({value: ''});}}> 

          <div className={styles.easeDownloadContent} style={{marginTop: '30px'}}>
           <a href="https://axa.com.sg/" target="_blank">
              <div className={styles.downloadHeader}>
                <p><span className={styles.easeContent}>{window.getLocalizedText(langMap, 'download_material.sectionA')}</span></p>
              </div>
           </a>  
          </div>
          
          <MaterialSection 
            title={window.getLocalizedText(langMap, 'download_material.sectionB')}  
            mList={prodSumList}
            pageClosed = {pageClosed}
          /> 

          <MaterialSection 
            title={window.getLocalizedText(langMap, 'download_material.sectionC')}  
            mList={genProvList}
            pageClosed = {pageClosed}
          /> 

          <MaterialSection 
            title={window.getLocalizedText(langMap, 'download_material.sectionD')} 
            subTitle = {[window.getLocalizedText(langMap, 'download_material.subSectionD1'),window.getLocalizedText(langMap, 'download_material.subSectionD2')]}
            mList={[inspireList,pulsarList]}
            pageClosed = {pageClosed}
          /> 

          <MaterialSection 
            title={window.getLocalizedText(langMap, 'download_material.sectionE')} 
            subTitle = {[window.getLocalizedText(langMap, 'download_material.subSectionE1'),window.getLocalizedText(langMap, 'download_material.subSectionE2'),window.getLocalizedText(langMap, 'download_material.subSectionE3')]}
            mList={[propFormList, questionnaireList, nbFormList]}
            pageClosed = {pageClosed}
          /> 

          <MaterialSection 
            title={window.getLocalizedText(langMap, 'download_material.sectionF')}  
            mList={polFormList}
            pageClosed = {pageClosed}
          /> 

          <MaterialSection 
            title={window.getLocalizedText(langMap, 'download_material.sectionG')}  
            mList={gaahList}
            pageClosed = {pageClosed}
          /> 

          <MaterialSection 
            title={window.getLocalizedText(langMap, 'download_material.sectionH')} 
            subTitle = {[window.getLocalizedText(langMap, 'download_material.subSectionH1')]}
            mList={[physicianList]}
            pageClosed = {pageClosed}
            claimSite = {true}
          /> 

        </div>

        </AppbarPage>
      </FloatingPage>
    );
  }

}
