import React, {PropTypes} from 'react';

import EABComponent from '../../../Component';
import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';
import {List, ListItem, makeSelectable, FlatButton} from 'material-ui';

import styles from '../../../Common.scss';
import eab from "../../../../img/eab.png";

export default class MaterialSection extends EABComponent {

  constructor(props, context) {
    super(props);
    this.state = Object.assign({}, this.state,{
      expand: false
   });
  }

  componentDidUpdate(prevProps) {
    if (this.props.pageClosed !== prevProps.pageClosed) {   //page closed closed all expanded column
      this.setState({
        expand  : false
      });
    }
  }

  onHandleClick=()=>{     //to display or hide list
    let {expand} = this.state;    
      if(expand){
        this.setState({
          expand  : false
        });
      }else{
        this.setState({
          expand  : true,
          
        });
      }
  }

  getDisplayList = (mList, subTitle)=>{
    let items = [];
    if(subTitle){          // with subheader
      for(var i = 0; i <mList.length; i++){
        items.push(
          <List className={styles.downloadItemList2Col}>
            <div className={styles.downloadItem2ColHeader}><span>{subTitle[i]}</span></div>
            {mList[i]}
          </List>
        );
      }
    }else{
      items.push(       //without subheade
        <List className={styles.downloadItemList}>
          {mList}
        </List>
      );
    }
    return items;
  }

  render() {
    const {langMap} = this.context;
    let {expand} = this.state;
    let {title, subTitle, mList, pageClosed, claimSite} = this.props;
    let mlistLength = mList.length;
    let displayList = this.getDisplayList(mList,subTitle);

    return(  
      <div className={styles.easeDownloadContent}>
        <div className={styles.downloadHeader}>
          <p>
            <span className={styles.easeContent}>{title}</span>
            {claimSite &&
              <span className={styles.claimSite}>
                <a href="https://www.axa.com.sg/customer-care/file-a-claim" target="_blank" style={{color: '#fff'}}>{window.getLocalizedText(langMap, 'download_material.claimSiteLink')}</a>
              </span>
            }
          </p>
          <FlatButton
            label= {expand ? '-' : '+'}
            primary
            onClick={this.onHandleClick}
            style={{color: '#fff', height: '30px', position:'relative',bottom:'2px'}}
          />  
        </div>    

        <div style={expand ? {display: 'block'}: {display: 'none'}}>      
            {displayList}
        </div>
        </div>
    );  
  }
}

