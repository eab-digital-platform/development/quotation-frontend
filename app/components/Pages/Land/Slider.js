import React from 'react';
import {IconButton} from 'material-ui';

import EABComponent from '../../Component';
import {getIcon} from '../../Icons/index';

export default class Slider extends EABComponent {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      loop: false,
      selected: 0,
      index: 0
    });
  }

  componentWillMount() {
    const { selected } = this.props;
    this._updateSlider({
      index: selected || 0
    });
  }

  componentWillReceiveProps(nextProps) {
    const { selected } = this.props;
    if (selected !== nextProps.selected) {
      this.goToSlide(nextProps.selected, nextProps.children);
    }
  }

  getDragX(event) {
    return event.touches[0].pageX;
  }

  handleDragStart = (event) => {
    event.preventDefault();
    this.dragStart = this.getDragX(event);
    this.dragStartTime = new Date();
  };

  handleDragMove = (event) => {
    this.dragEnd = this.getDragX(event);
  };

  handleDragEnd = () => {
      const {children} = this.props;
      const {index} = this.state;

      const timeElapsed = new Date().getTime() - this.dragStartTime.getTime();
      const offset = this.dragEnd - this.dragStart;
      const velocity = Math.round(offset / timeElapsed);

      let newIndex = index;

      if (Math.abs(velocity) > 1) {
        newIndex = velocity < 0 ? index + 1 : index - 1;
      }

      if (newIndex < 0) {
        newIndex = 0;
      } else if (newIndex >= children.length) {
        newIndex = children.length - 1;
      }

      this._updateSlider({
        index: newIndex
      });
    };

  goToSlide=(index, children)=>{
    const {loop} = this.props;
    if(!children) children = this.props.children;


    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (index < 0) {
      index = loop ? children.length - 1 : 0;
    } else if (index >= children.length) {
      index = loop ? 0 : children.length - 1;
    }

    this._updateSlider({
      index: index
    });
  };

  renderNav=()=>{
    const { children } = this.props;
    const {index} = this.state;

    var self = this;
    const nav = children.map((slide, i) => {
      const buttonClasses = i === index ? 'Slider-navButton Slider-navButton--active' : 'Slider-navButton';
      return (
        <button
          className={ buttonClasses }
          key={ i }
          onClick={function(){
            self.goToSlide(this.id, null);
          }.bind({id: i})}
        />
      );
    })

    return (
      <div className='Slider-nav'>{ nav }</div>
    );
  };

  arrowEvent=(index, event)=>{
    const {children} = this.props;
    if (index >= 0 && index <= children.length - 1) {
      this.goToSlide(index, null);
    }
  };

  renderArrows = () => {
    const {children} = this.props;
    const {index} = this.state;

    if (children.length < 2) {
      return <div />;
    } else {
      return (
        <div className={'Slider-arrows'}>
            {this.state.index > 0 ? (
              <IconButton iconStyle={{width:48, height:48}} className={'Slider-arrow-left'} onTouchTap={() => this.goToSlide(index - 1, null)}>
                {getIcon('previous')}
              </IconButton>
            ) : <div></div>}
            {this.state.index !== children.length - 1 ? (
              <IconButton iconStyle={{width:48, height:48}} className={'Slider-arrow-right'} onTouchTap={() => this.goToSlide(index + 1, null)}>
                {getIcon('next')}
              </IconButton>
            ) : <div></div>}
        </div>
      );
    }
  };

  _updateSlider = (newState) =>{
    if (typeof newState.index === 'number' && this.props.onSliderChange) {
      this.props.onSliderChange(newState.index);
    }
    this.setState(newState);
  }


  render() {
    const {index} = this.state;
    const {children} = this.props;

    var wd = 100 * children.length;
    var tran = -1 * index / children.length * 100;
    const slidesStyles = {
      width: wd + "%",
      transform: 'translateX( ' + tran + '%)',
    };

    return (
      <div className='Slider' ref='slider' style={this.props.style}>
        {this.renderArrows()}
        { this.renderNav()}

        <div
          className='Slider-inner'
          onTouchStart={ (event) => this.handleDragStart(event) }
          onTouchMove={ (event) => this.handleDragMove(event) }
          onTouchEnd={ (event) => this.handleDragEnd() }>
          <div
            className={ 'Slider-slides Slider-slides--transition' }
            style={ slidesStyles }>
            { children }
          </div>
        </div>
      </div>
    );
  };
}
