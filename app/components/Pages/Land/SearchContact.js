import React from "react";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {TextField, Card, Dialog, FlatButton} from "material-ui";

import * as _ from 'lodash';

import {getIcon} from '../../Icons/index';
import {getProfile} from '../../../actions/client';
import {resetNeedstoEmpty} from '../../../actions/needs';

import ContactItem from './ContactItem';
import EABComponent from '../../Component';

import styles from "../../Common.scss";

const initMaxSize = 50;
const initState = {
  value: '',
  viewAll: false,
  showNotFindDialog: false,
  contactList:[],
  maxSize:initMaxSize
};

class SearchContact extends EABComponent {
  constructor(props, context) {
      super(props);
      this.state = Object.assign({}, this.state, _.cloneDeep(initState));
      this.handleScroll = this.handleScroll.bind(this);
  }

  handleScroll(event) {
    let {scrollTop,scrollHeight,offsetHeight} = event.currentTarget;
    if (scrollTop + offsetHeight + 1 >= scrollHeight) {
      if (this.state.contactList.length > this.state.maxSize) {
        this.setState({
          maxSize: this.state.maxSize += initMaxSize
        });
      }
    }
  }



  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    ReactDOM.findDOMNode(this).addEventListener('scroll', this.handleScroll);

    this.init();

    this.context.updateAppbar({
      title: {"en": "Search by client's name, phone number, NRIC, ID document NO., or email address"},
      onSearchChange: (orgValue, newValue)=>{
        this.context.updateAppbar({
          searchValue: newValue,
          itemsIndex: isEmpty(newValue)?0:1
        });

        this.setState({
          value: newValue,
          viewAll: false
        })
      },
      itemsIndex: 0,
      items: [[{
          type: "flatButton",
          title: {
            "en": "VIEW ALL"
          },
          action: ()=>{
            this.setState({viewAll: true});
          }
        }],
        [{
          type: "flatButton",
          title: {
            "en": "CLEAR"
          },
          action: ()=>{
            this.context.updateAppbar({itemsIndex:0, searchValue: ''});
            this.init();
          }
        }]
      ]
    })

  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let state = this.context.store.getState();
      let newState = {};

      if(!isEqual(this.state.contactList, state.client.contactList)){
        newState.contactList = state.client.contactList;
      }

      if(!isEmpty(newState))
        this.setState(newState);
    }
  }

  init=()=>{
    this.setState(Object.assign(_.cloneDeep(initState), {
      contactList:this.context.store.getState().client.contactList
    }));
  }

  viewAll=()=> {
    this.setState({
      viewAll: true
    });
  };

  renderItems=()=>{
    let items = [];
    let contactList = _.cloneDeep(this.state.contactList) || [];
    let {viewAll, value, maxSize} = this.state;
    let searchValues = _.toUpper(value).split(" ").filter(text=>!isEmpty(_.trim(text)));

    let contactListLength = contactList.length;

    let contactCnt = 0;
    let contacts = _.orderBy(contactList, [contact=>_.toUpper(contact.fullName)], 'asc');
    _.forEach(contacts, (item, index)=>{
      if (contactCnt >= maxSize) {
        return false; // temp fix: show top 50 only, better use something like virtual scroll
      }
       let {idCardNo='', fullName='', mobileNo='', email=''} = item;
       let found = true;
       _.forEach(searchValues, searchValue=>{
         if(!(_.toUpper(idCardNo).indexOf(searchValue)>-1 || _.toUpper(fullName).indexOf(searchValue)>-1 ||
            _.toUpper(mobileNo).indexOf(searchValue)>-1 || _.toUpper(email).indexOf(searchValue)>-1)){
              found = false;
            }
       })

       if(viewAll || (_.trim(value).length > 1 && found)){
          if(items.length){
            items.push(
                <div key={"search-contact-divider-"+index} className={styles.Divider} style={{width: 'calc(100% - 84px)', marginLeft: '84px'}}/>
            )
          }
          items.push(
            <ContactItem
              onItemClick={(id)=>{
                getProfile(this.context, id, (success)=>{
                  if(success){
                    resetNeedstoEmpty(this.context);
                    this.context.onContactItemClick();
                  }
                  else {
                    this.setState({showNotFindDialog: true})
                  }
                })
              }}
              key={"search-contact-"+item.id}
              withDivider={(index<contactListLength-1)?true:false}
              item={item}
            />
          )
          contactCnt++;
        }
    })
    return items;
  }


  render() {
    let {langMap} = this.context;
    let {showNotFindDialog} = this.state;
    let items = this.renderItems();
    const actions = [
      (
        <FlatButton
          key="close-dialog-button"
          label={getLocalizedText(langMap, "BUTTON.CANCEL", "Cancel")}
          onTouchTap={()=>{this.setState({showNotFindDialog: false})}}
        />
      )
    ];

    return (
      <div className={styles.CardContainer}>
        <Dialog open={showNotFindDialog} actions={actions}>
          {getLocalizedText(langMap, "CCDIALOG.NOT_FIND", "Client not find, it may caused by client is deleted.")}
        </Dialog>
        <div className={styles.Card} style={{ width: 720 }}>
          {items}
        </div>
      </div>

    );
  }
}

SearchContact.contextTypes = Object.assign(SearchContact.contextTypes, {
  onContactItemClick: PropTypes.func
})

export default SearchContact;
