import React from 'react';
import EABComponent from '../../../Component';
import styles from '../../../Common.scss';
import {getIcon} from '../../../Icons/index';
import EABAvatar from '../../../CustomViews/Avatar';

import {updateAgentProfilePic} from '../../../../actions/agent';

export default class MyProfile extends EABComponent {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      agentProfilePicRev:0
    });
  }

  componentDidMount(){
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  storeListener= ()=>{
    if (this.unsubscribe){
      let {store} = this.context;
      let {attachments} = store.getState();
      let newState = {};
      if (attachments && attachments.agentProfilePicRev) {
        if (!_.isEqual(this.state.agentProfilePicRev, attachments.agentProfilePicRev)) {
          newState.agentProfilePicRev = attachments.agentProfilePicRev;
        }
      }
      if (!window.isEmpty(newState)){
          this.setState(newState);
      }
    }
  }

  componentWillUnmount(){
      if (window.isFunction(this.unsubscribe)){
          this.unsubscribe();
          this.unsubscribe = undefined;
      }
  }

  getProfilePic() {
    const {attachments} = this.context.store.getState();
    return attachments.items.agentProfilePic;
  }

  render() {
    const {langMap, muiTheme, store} = this.context;
    const {agentProfile,lastUpdateDate} = store.getState().app;
    const {name, position} = agentProfile;
    let values = { agentProfilePic: this.getProfilePic() || 1 };
    let {agentProfilePicRev} = this.state;

    return (
      <div className={styles.CardContainer}>
        <div style={{ margin: 'auto', width: '80%', maxWidth: 720 }}>
          <div style={{ fontSize: '24px', marginTop: 24, color: muiTheme.palette.primary1Color }}>
            {window.getLocalizedText(langMap, 'settings.my_profile')}
          </div>
          <div className={styles.Card}>
            <div style={{ display: 'flex', minHeight: '0px', padding: '16px 0' }}>
              <div style={{ flexBasis: 84, display: 'flex', minHeight: '0px', justifyContent: 'center' }}>
                <EABAvatar
                  docId={'UX_' + agentProfile.profileId}
                  template={{ id: 'agentProfilePic', type: 'photo' }}
                  origin={'MyProfile'}
                  agentProfilePicRev={agentProfilePicRev}
                  width={56}
                  icon={getIcon('person', '#FFFFFF')}
                  values={values}
                  lastUpdateDate={lastUpdateDate}
                  changedValues={values}
                  handleChange={(id, lastModified) => {
                    let profilePic = this.getProfilePic();
                    let type = profilePic && profilePic.type;
                    let data = profilePic && profilePic.value;
                    updateAgentProfilePic(this.context, type, data, () => {
                      this.setState({});
                    });
                  }}
                  style={{ cursor: 'pointer' }}
                />
              </div>
              <div style={{ flexGrow: 1, display: 'flex', minHeight: '0px', flexDirection: 'column' }}>
                <div className={styles.CardRowFieldLarge}>{name}</div>
                <div className={styles.CardRowFieldSmall}>{position}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
