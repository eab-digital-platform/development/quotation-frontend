import React from 'react';
import {List, ListItem} from 'material-ui';

import EABComponent from '../../../Component';
import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';
import MyProfile from './MyProfile';

import styles from '../../../Common.scss';

export default class Settings extends EABComponent {

  open() {
    this.updateAppbar();
    this.floatingPage.open();
  }

  close() {
    this.floatingPage.close();
  }

  updateAppbar() {
    const {langMap} = this.context;
    this.appbarPage.initAppbar({
      menu: {
        icon: 'back',
        action: () => {
          this.close();
        }
      },
      title: window.getLocalizedText(langMap, 'settings.title')
    });
  }

  render() {
    const {langMap} = this.context;
    return (
      <FloatingPage ref={ref => { this.floatingPage = ref; }}>
        <AppbarPage ref={ref => { this.appbarPage = ref; }}>
          <div style={{ display: 'flex', minHeight: '0px', flexGrow: 1, zIndex: 1 }}>
            <List className={styles.Menu}>
              <ListItem primaryText={window.getLocalizedText(langMap, 'settings.my_profile')} />
            </List>
            <MyProfile />
          </div>
        </AppbarPage>
      </FloatingPage>
    );
  }

}
