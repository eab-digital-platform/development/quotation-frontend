import React from 'react';

import EABComponent from '../../Component';
const introdBg = require("../../../img/welcome.jpg");

class Main extends EABComponent {

  constructor(props) {
      super(props);
  }

  render() {

    return (
      <div
        id="landSlide1"
        style={{
          backgroundImage: 'url(' + introdBg + ')',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'bottom'
        }}
      />
    );
  }
}

export default Main;
