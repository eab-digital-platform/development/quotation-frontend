import React, {PropTypes} from 'react';
import Divider from 'material-ui/Divider';
import Dialog from 'material-ui/Dialog';
import {List, ListItem, makeSelectable} from 'material-ui/List';
import FlatButton from 'material-ui/FlatButton';
import Avatar from 'material-ui/Avatar';
import Subheader from 'material-ui/Subheader';
import {getIcon} from '../../Icons/index';
import _getOr from 'lodash/fp/getOr';
import _get from 'lodash/fp/get';
import isFunction from 'lodash/isFunction';
import {APPROVAL_ACCESS_ROLE, APPROVAL_FA_ACCESS_ROLE} from '../../../constants/ConfigConstants';
import {
  logout,
  downloadIPA
} from '../../../actions/GlobalActions';

import EABComponent from '../../Component';
import EABAvatar from '../../CustomViews/Avatar';

let SelectableList = makeSelectable(List);

class MenuItem extends EABComponent {
  render() {
    let {muiTheme} = this.context;
    let {item, selected} = this.props;
    let {icon, title, action} = item;

    let unselectedColor = 'rgba(0,0,0,0.54)';
    let selectedColor = muiTheme.palette.alternateTextColor;

    return (
      <div style={{display: 'flex', minHeight: '0px', alignItems: 'center', height: '48px', cursor: 'pointer'}} onTouchTap={action}>
        <div style={{padding: '0px 24px'}}>{getIcon(icon, selected?selectedColor:unselectedColor)}</div>
        <div style={{padingLeft: '24px', color: selected?selectedColor:'rgba(0,0,0,1)', fontFamily: 'SourceSansPro'}}>{title}</div>
      </div>
    );
  }
}


class AppMenu extends EABComponent {

  static propTypes = {
    openClientView: PropTypes.func,
    openSettings: PropTypes.func,
    openAboutEase: PropTypes.func,
    openDownloadMaterial: PropTypes.func
  };

  constructor(props) {
      super(props);
      this.state = {
        open:false,
        openDialog: false,
        dialogTitle: '',
        agentProfilePicRev:0
      };
  }

  componentDidMount(){
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  storeListener= ()=>{
    if (this.unsubscribe){
      let {store} = this.context;
      let {attachments} = store.getState();
      let newState = {};
      if (attachments && attachments.agentProfilePicRev) {
        if (!_.isEqual(this.state.agentProfilePicRev, attachments.agentProfilePicRev)) {
          newState.agentProfilePicRev = attachments.agentProfilePicRev;
        }
      }
      if (!window.isEmpty(newState)){
          this.setState(newState);
      }
    }
  }

  componentWillUnmount(){
      if (window.isFunction(this.unsubscribe)){
          this.unsubscribe();
          this.unsubscribe = undefined;
      }
  }

  open=()=>{
    this.setState({open: true});
  }

  close=()=>{
    this.setState({open:false});
  }

  onClickMyClients() {
    const {openClientView} = this.props;
    openClientView && openClientView();
    this.setState({
      open: false
    });
  }

  onClickSettings() {
    const {openSettings} = this.props;
    openSettings && openSettings();
    this.setState({
      open: false
    });
  }

  onClickAboutEase() {
    const {openAboutEase} = this.props;
    openAboutEase && openAboutEase();
    this.setState({
      open: false
    });
  }

  onClickWorkbench() {
    if(isFunction(this.props.openWorkBench)){
      this.props.openWorkBench();
    }
    this.setState({
      open: false
    })
  }

  onClickDownloadMaterial() {
    const {openDownloadMaterial} = this.props;
    openDownloadMaterial && openDownloadMaterial();
    this.setState({
      open: false
    });
  }

  onClickDownloadIPA() {
    downloadIPA(this.context);
  }

  onClickApproval(){
    const {app} = this.context.store.getState();
    const {agentProfile} = app;
    
    const faRole = _getOr('', 'rawData.faAdvisorRole', agentProfile );
    const faAdminRole = _getOr('', 'rawData.userRole', agentProfile );
    const channel = _getOr('', 'channel.type', agentProfile );
    if (isFunction(this.props.openApproval)){
      /**if (APPROVAL_ACCESS_ROLE.indexOf(agentProfile.role) > -1 || APPROVAL_FA_ACCESS_ROLE.indexOf(faRole) > -1
      || (channel === 'FA' && APPROVAL_FA_ACCESS_ROLE.indexOf(faAdminRole) > -1 )) {
        this.props.openApproval();
      } else {
        this.setState({
          dialogTitle: 'No Approval Access',
          openDialog: true
        })
      }*/
      this.props.openApproval();
    }
    this.setState({
      open: false
    })
  }

  _getAgentName() {
    const {app} = this.context.store.getState();
    return app.agentProfile.name;
  }

  render() {
    let {muiTheme, langMap, store} = this.context;
    let headerBackgroundColor = muiTheme.palette.alternateTextColor;
    let {dialogTitle, openDialog, agentProfilePicRev} = this.state;
    const {agentProfile, version, build, isFAFirm,lastUpdateDate} = store.getState().app;
    const {attachments} = this.context.store.getState();
    let profilePic = { agentProfilePic: attachments.items.agentProfilePic || 1 };

    const actions = [
      <FlatButton
          label="OK"
          primary={true}
          onClick={()=>{
              this.setState({
                  openDialog: false,
                  dialogTitle: ''
              });
          }}
      />,
    ];

    const isIPAD = () => {
      const ua = navigator.userAgent;
      if (ua.indexOf('iPad') > -1) {
        return true;
      } else if (navigator.platform && navigator.platform === 'MacIntel' && navigator.maxTouchPoints && navigator.maxTouchPoints > 1) {
        // For iOS 13, We can't see the "Download EASE App" button on left menu bar on mobile Safari.
        // But it can be shown on mobile Chrome.
        // It's because of the navigator.userAgent will not include string "iPad" on iOS 13 Safari.
        // Maybe Apple will merge iOS, macOS and watchOS in the future.
        // So we need add one more condition to check iPad device.
        // Refer to: https://stackoverflow.com/questions/58019463/how-to-detect-device-name-in-safari-on-ios-13-while-it-doesnt-show-the-correct
        return true;
      }
      return false;
    };
    
    let pendingForApproval;
    if (agentProfile.allowToEnterApproval) {
      pendingForApproval = <MenuItem item={{icon: 'contentPaste', title: getLocalizedText(langMap, 'menu.eapproval.pendingforapproval', 'Pending for Approval'), action:()=>{this.onClickApproval()}}}/>;
    }

    return (
      <div style={{
          position: 'fixed',
          left: 0,
          top: 0,
          width: '100%',
          height: '100%',
          background: 'rgba(0,0,0,0)',
          display: this.state.open?'flex':'none',
          zIndex: 1000
        }}>
        <div style={{flexBasis: '260px', height: '100%', display: 'flex', minHeight: '0px', flexDirection: 'column' , background: '#ffffff'}}>
            <Dialog
              title={dialogTitle}
              actions={actions}
              modal={true}
              open={openDialog}
              style={{zIndex: 1500}}
          />
          <div style={{flexBasis: '140px', width: '100%', background: headerBackgroundColor}}>
            <div style={{padding: '24px'}}>
              <EABAvatar
                disabled
                docId={'UX_' + agentProfile.profileId}
                template={{ id: 'agentProfilePic', type: 'photo' }}
                origin={'Menu'}
                agentProfilePicRev={agentProfilePicRev}
                width={64}
                icon={getIcon('person', '#FFFFFF')}
                values={profilePic}
                lastUpdateDate={lastUpdateDate}
                changedValues={profilePic}
              />
              <div style={{paddingTop: '8px', color: '#FFFFFF'}}>{this._getAgentName()}</div>
            </div>
          </div>
          <div style={{flexGrow: 1, overflowY: 'auto'}}>
            {(isFAFirm) ?
              undefined :
              <MenuItem item={{icon: 'people', title: getLocalizedText(langMap, 'menu.clients'), action:()=>{this.onClickMyClients()}}}/>
            }
            <MenuItem item={{icon: 'build', title: getLocalizedText(langMap, 'menu.eapproval.workbench ', 'Workbench'), action:()=>{this.onClickWorkbench()}}}/>
            {pendingForApproval}
            <MenuItem item={{icon: 'download', title: getLocalizedText(langMap, 'menu.eapproval.downloadMaterials', 'Download Material'), action:()=>{this.onClickDownloadMaterial()}}}/>
            {
             //we don't need this download link now.
             //agentProfile.channel.type == "AGENCY" && <MenuItem item={{icon: 'KeyDown', title: getLocalizedText(langMap, 'menu.downloadEASE', 'Download EASE App'), action:()=>{this.onClickDownloadIPA()}}}/>
            }
          </div>
          <Divider style={{flexBasis: '2px'}}/>
          <div style={{flexBasis: '80px', width: '100%'}}>
            <MenuItem item={{icon: 'setting', title: getLocalizedText(langMap, 'settings.title'), action: () => this.onClickSettings()}}/>
            <MenuItem item={{icon: 'info', title: getLocalizedText(langMap, 'about.title'), action: () => this.onClickAboutEase()}}/>
            <MenuItem item={{icon: 'logout', title: getLocalizedText(langMap, 'menu.logout'), action:()=>{logout(this.context)}}}/>
          </div>
          <div style={{textAlign:'center', width: '100%'}}>{version + '-' + build}</div>
        </div>
        <div style={{flexGrow: 1, height: '100vh', background: 'rgba(0,0,0,0.45)'}} onTouchTap={()=>{this.close()}}/>
      </div>
    );
  }
}

export default AppMenu;
