import React, {PropTypes} from 'react';
import {Tabs, Tab, FlatButton, Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui';
import * as _ from 'lodash';
import DynSizePDFViewer from '../../../CustomViews/DynSizePDFViewer';
import EmbedPDFAttachmentViewer from '../../../CustomViews/EmbedPDFAttachmentViewer';

import {requote, requoteInvalid, cloneQuotation} from '../../../../actions/quotation';
import {openEmail, showIllustration, getPdfToken} from '../../../../actions/proposal';
import pdfDataSample from '../Quotation/mockupData/pdfDataSample.json';

import EABComponent from '../../../Component';
import styles from '../../../Common.scss';

export default class Proposal extends EABComponent {

  static propTypes = {
    pdfData: PropTypes.array,
    canRequote: PropTypes.bool,
    canRequoteInvalid: PropTypes.bool,
    canClone: PropTypes.bool,
    canEmail: PropTypes.bool,
    canIllustrate: PropTypes.bool
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    onCloseProposal: PropTypes.func
  });

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      slideIndex: 0
    });
  }

  getAppbarData=()=> {
    const {langMap, store} = this.context;
    const {canRequote, canRequoteInvalid, canClone, canEmail, canIllustrate, pdfData} = this.props;
    const {slideIndex} = this.state;
    const {docId, attachmentId, fileName} = (pdfData && pdfData[slideIndex]) || {};
    let appbarBtns = [];
    if (!window.isIPad() && docId && attachmentId) {
      appbarBtns.push({
        type: 'flatButton',
        title: window.getLocalizedText(langMap, 'proposal.btn.download'),
        action: () => {
          getPdfToken(this.context, docId, attachmentId, (token) => {
            const link = document.createElement('a');
            link.href = window.genAttPath(token);
            link.download = fileName || 'policy_illustration.pdf';
            link.click();
          });
        }
      });
    }
    if (canRequote) {
      appbarBtns.push({
        type: 'flatButton',
        title: window.getLocalizedText(langMap, 'proposal.btn.requote'),
        action: () => {
          let quotId = store.getState().proposal.quotation.id;
          requote(this.context, quotId);
        }
      });
    }
    if (canRequoteInvalid) {
      appbarBtns.push({
        type: 'flatButton',
        title: window.getLocalizedText(langMap, 'proposal.btn.requote'),
        action: () => {
          let quotId = store.getState().proposal.quotation.id;
          requoteInvalid(this.context, quotId);
        }
      });
    }
    if (canClone) {
      appbarBtns.push({
        type: 'flatButton',
        title: window.getLocalizedText(langMap, 'proposal.btn.clone'),
        action: () => {
          let quotId = store.getState().proposal.quotation.id;
          cloneQuotation(this.context, quotId, false);
        }
      });
    }
    if (canEmail) {
      appbarBtns.push({
        type: 'flatButton',
        title: window.getLocalizedText(langMap, 'proposal.btn.email'),
        action: () => {
          let standalone = !!_.find(pdfData, (pdf) => pdf.allowSave);
          openEmail(this.context, standalone);
        }
      });
    }
    if (canIllustrate) {
      appbarBtns.push({
        type: 'flatButton',
        title: window.getLocalizedText(langMap, 'proposal.btn.illustration'),
        action: () => {
          showIllustration(this.context);
        }
      });
    }
    appbarBtns.push({
      type: 'flatButton',
      title: window.getLocalizedText(langMap, 'proposal.btn.done'),
      action: () => {
        this.context.onCloseProposal();
      }
    });
    return {
      menu: {
        icon: null,
        action: () => {
        }
      },
      title: window.getLocalizedText(langMap, 'proposal.proposal_title'),
      itemsIndex: 0,
      items: [appbarBtns]
    };
  }

  getAppbarData = (value) => {
    this.setState({
      slideIndex: value,
    }, () => {
      let item = this['tab' + value];
      if (item && _.isFunction(item.onTabChange)) {
        item.onTabChange();
      }
      this.context.initAppbar(this.getAppbarData());
    });
  }

  _getTabs(){
    let {lang} = this.context;
    var tabs = [];
    const pdfData = pdfDataSample.pdfData;
    if (pdfData && pdfData.length) {
      pdfData.forEach((pdf, index) => {
        tabs.push(
          <Tab
            key={'tab-' + pdf.id}
            label={<div className={styles.TabLabel}>{pdf.tabLabel}</div>}
            value={index}
          />
        );
      });
    }
    return tabs;
  }

  _getPDFViewer(pdfIndex) {
    const pdfData = pdfDataSample.pdfData;
    if (pdfData && pdfData[pdfIndex]) {
      const pdf = pdfData[pdfIndex];
      let viewerProps;
      if (pdf.docId && pdf.attachmentId) {
        viewerProps = {
          docId: pdf.docId,
          attachmentId: pdf.attachmentId,
          getTokenFunc: (docId, attachmentId, callback) => {
            getPdfToken(this.context, docId, attachmentId, callback);
          }
        };
      } else {
        viewerProps = { pdf: pdf.data };
      }
      if (pdf.allowSave) {
        return <EmbedPDFAttachmentViewer {...viewerProps} />;
      } else {
        return <DynSizePDFViewer {...viewerProps} />;
      }
    }
  }

  render() {
    let {slideIndex} = this.state;
    let {muiTheme} = this.context;

    const goToProposalPage = () => {
      console.log('Page');
    };
    return (
      <div>
        <Toolbar style={{"backgroundColor" : '#FFFFFF !important'}}>
            <ToolbarGroup firstChild={true}>
              <ToolbarTitle text={'Quotation'} style={{'marginLeft': '4px !important', 'color': 'rgb(48, 84, 174)'}} />
            </ToolbarGroup>
            <ToolbarGroup lastChild={true}>
            <FlatButton
                primary
                label={'REQUOTE'}
                onTouchTap={() => {window.location.href = "/#"}}
              />
              <FlatButton
                primary
                label={'DONE'}
                onTouchTap={() => {}}
              />
            </ToolbarGroup>
        </Toolbar>
        <div style={{ flex: 1, display: 'flex', minHeight: '0px', flexDirection: 'column' }}>
          <div className={styles.Tabs}>
            <Tabs
              key="proposal-tabs"
              id={'Tabs'}
              inkBarStyle={{ backgroundColor: muiTheme.palette.primary3Color }}
              onChange={this.handleChange}
              value={this.state.slideIndex}
            >
              {this._getTabs()}
            </Tabs>
          </div>
          <div style={{ flex: 1, overflowY: 'auto' }}>
            {this._getPDFViewer(slideIndex)}
          </div>
        </div>
      </div>
    );
  }

}
