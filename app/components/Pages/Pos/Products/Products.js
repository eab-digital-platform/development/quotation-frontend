import React, { PropTypes } from 'react';
import * as _ from 'lodash';

import {getProductId} from '../../../../../common/ProductUtils';

import ProductGroup from './ProductGroup';
import ProductItem from './ProductItem';
import GroupInfoDialog from './GroupInfoDialog';

import EABComponent from '../../../Component';

export default class Products extends EABComponent {

  static propTypes = {
    prodCategories: PropTypes.array
  };

  static childContextTypes = {
    openGroupInfo: PropTypes.func
  };

  getChildContext() {
    return {
      openGroupInfo: this.openGroupInfo
    };
  }

  openGroupInfo=(infoDic)=> {
    this.groupInfoDialog.openDialog(infoDic);
  }

  render() {
    const {prodCategories} = this.props;
    const {lang} = this.context;
    let groupsEl = _.map(prodCategories, (category) => {
      let itemsEl = _.map(category.products, (product) => {
        return (
          <ProductItem
            key={getProductId(product)}
            product={product}
          />
        );
      });
      let groupName = window.getLocalText(lang, category.title);
      return (
        <ProductGroup
          key={groupName}
          groupName={groupName}
          infoDic={category.infoDic}
        >
          {itemsEl}
        </ProductGroup>
      );
    });
    return (
      <div>
        {groupsEl}
        <GroupInfoDialog ref={ref => { this.groupInfoDialog = ref; }} />
      </div>
    );
  }
}
