import React, {PropTypes} from 'react';
import EABComponent from '../../../Component';
import {Dialog} from 'material-ui';
import * as _ from 'lodash';

import styles from '../../../Common.scss';

import {getObjectiveTitle} from '../../../../utils/GlobalUtils';

class GroupInfoDialog extends EABComponent {

  static propTypes = {
    infoDic: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      open: false,
      infoDic: null
    });
  }

  openDialog=(infoDic)=>{
    this.setState({
      open: true,
      infoDic: infoDic
    });
  }

  closeDialog=()=>{
    this.setState({
      open: false
    });
  }

  getProductLineTitle(value) {
    const {optionsMap, lang} = this.context;
    let option = optionsMap.productLine.options[value];
    return (option && window.getLocalText(lang, option.title)) || value;
  }

  render() {
    const {lang} = this.context;
    const {open, infoDic} = this.state;
    let infosEl = _.map(infoDic, (info, index) => {
      let items;
      if (index === 0) { // TODO assume the first info is objective
        items = _.join(_.map(info.items, (item) => getObjectiveTitle(this.context, item)), ', ');
      } else {
        items = _.join(_.map(info.items, (item) => this.getProductLineTitle(item)), ', ');
      }
      let title = window.getLocalText(lang, info.title);
      return (
        <div key={title} className={index > 0 ? styles.ProductLgMarginTop : undefined}>
          <div className={styles.ProductTitle}>{title}</div>
          <div className={styles.ProductSmMarginTop}>{items}</div>
        </div>
      );
    });
    return (
      <Dialog open={open} onRequestClose={this.closeDialog}>
        {infosEl}
      </Dialog>
    );
  }

}

export default GroupInfoDialog;
