import React, {PropTypes} from 'react';
import {IconButton} from 'material-ui';
import EABComponent from '../../../Component';
import {getIcon} from '../../../Icons/index';

import styles from '../../../Common.scss';

class ProductGroup extends EABComponent {

  render() {
    const {groupName, style, infoDic} = this.props;
    const {muiTheme, openGroupInfo} = this.context;
    let fontColor = muiTheme.palette.primary2Color;

    let infoIcon = null;
    if (infoDic) {
      infoIcon = (
        <IconButton onTouchTap={() => openGroupInfo(infoDic)}>
          {getIcon('info', fontColor)}
        </IconButton>
      );
    }

    return (
      <div className={styles.ProductGroup}>
        <div className={styles.ProductGroupTitleContainer}>
          <span className={styles.ProductTitle}>{groupName}</span>
          {infoIcon}
        </div>
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

ProductGroup.contextTypes = Object.assign(ProductGroup.contextTypes, {
  openGroupInfo: PropTypes.func
});

ProductGroup.propTypes = {
  children: PropTypes.array,
  style: PropTypes.object,
  groupName: PropTypes.string,
  infoDic: PropTypes.array
};

export default ProductGroup;
