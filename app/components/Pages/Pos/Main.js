import React from 'react';
import PropTypes from 'prop-types';
import {Tabs, Tab} from 'material-ui';
import * as _ from 'lodash';
import Profile from './Profile';
import Needs from './Needs';
import Products from './Products';
import Applications from './Applications';
import SecIndex from './SecIndex'
import EABComponent from '../../Component'
import Constants from '../../../constants/SystemConstants'
import SupportDocPage from './Applications/UploadDocument/index';
import {updateContactList} from '../../../actions/client';
import {getIcon} from '../../Icons/index';

import styles from '../../Common.scss';

class Main extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, {
        slideIndex: Constants.FEATURES.CP,
        openSuppDocsPage: false,
        showFnaInvalidFlag: false
      });
  };

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.context.updateAppbar({
      menu: {
        icon: 'close',
        action: ()=>{
          updateContactList(this.context, false, this.context.closePage);
        }
      },
      items:[[]]
    })
    this.storeListener();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
      // let {client} = this.context.store.getState();
      // let {profile={}} = client;
      // this.context.updateAppbar({
      //   title: profile.fullName
      // })
      let newState = {};
      let {pos} = this.context.store.getState();
      let {openSuppDocsPage, showFnaInvalidFlag} = this.state;
      if(!_.isEqual(openSuppDocsPage, pos.openSuppDocsPage)) {
        newState.openSuppDocsPage = pos.openSuppDocsPage;
      }

      if (!_.isEqual(showFnaInvalidFlag, pos.showFnaInvalidFlag)) {
        newState.showFnaInvalidFlag = pos.showFnaInvalidFlag;
      }

      if (!isEmpty(newState)) {
        this.setState(newState);
      }
    }

  }

  getChildContext() {
    return {
      goTab: this.handleChange
    }
  }

  init=(page)=>{
    this.setState({slideIndex: page});
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    }, () => {
    let item = this.refs[value];
      if(item && _.isFunction(item.onTabChange)){
        item.onTabChange();
      }
    });
  }

  render() {
    let { muiTheme, store, pageOpen } = this.context;
    let { showFnaInvalidFlag } = this.state;

    if (!pageOpen) {
      return <div/>
    }

    let { slideIndex } = this.state;
    let { agentProfile } = store.getState().app;

    var tabs = [];
    var page = null;

    if (agentProfile && agentProfile.features) {
      if (agentProfile.features.indexOf(Constants.FEATURES.CP)>=0) {
        tabs.push(<Tab key="tab-profile" label={<div className={styles.TabLabel}>Profile</div>} value={Constants.FEATURES.CP}/>);
      }
      if (agentProfile.features.indexOf(Constants.FEATURES.FNA)>=0) {
        tabs.push(
          <Tab 
            key="tab-needs" 
            label={
              <div className={styles.TabLabel}>
                Needs
                { showFnaInvalidFlag ? getIcon('warning', 'red') : null }
              </div>
            } 
            value={Constants.FEATURES.FNA}
          />
        );
      }
      if (agentProfile.features.indexOf(Constants.FEATURES.FQ)>=0) {
        tabs.push(<Tab key="tab-products" label={<div className={styles.TabLabel}>Products</div>} value={Constants.FEATURES.FQ}/>);
      }
      if (agentProfile.features.indexOf(Constants.FEATURES.EAPP)>=0) {
        tabs.push(<Tab key="tab-applications" label={<div className={styles.TabLabel}>Applications</div>} value={Constants.FEATURES.EAPP}/>);
      }
      if (agentProfile.features.indexOf(Constants.FEATURES.QQ)>=0) {
        tabs.push(<Tab key="tab-qq" label={<div className={styles.TabLabel}>Quick Quote</div>} value={Constants.FEATURES.QQ}/>);
      }
    }

    return (
      <div style={{width: '100%', height: '100%', display: 'flex', minHeight: '0px',  flexDirection: 'column',flexGrow: 1}}>
        <div className={styles.Tabs}>
          <Tabs key="profile-tabs"
            id={'Tabs'}
            onChange={this.handleChange}
            inkBarStyle={{
              backgroundColor: muiTheme.palette.primary3Color
            }}
            value={slideIndex}>
            {tabs}
          </Tabs>
        </div>
        <div className={styles.Tab} style={{flexGrow: 1, display: 'flex', minHeight: '0px', overflowY: 'auto'}}>
        {
           slideIndex === Constants.FEATURES.CP  ? <Profile key="context-profile" ref={Constants.FEATURES.CP}/>:
           slideIndex === Constants.FEATURES.FNA ? <Needs key="context-needs" ref={Constants.FEATURES.FNA}/>:
           slideIndex === Constants.FEATURES.FQ  ? <Products key="context-products" ref={Constants.FEATURES.FQ}/>:
           slideIndex === Constants.FEATURES.EAPP ? <Applications key="context-applications" ref={Constants.FEATURES.EAPP}/>:
           slideIndex === Constants.FEATURES.QQ  ? <Products key="context-products-qq" quickQuote={true} ref={Constants.FEATURES.QQ}/>:null
        }
        </div>
       <SecIndex />
       {this.state.openSuppDocsPage ? <SupportDocPage isShield={store.getState().pos.isShield || false}/> : null}
      </div>
    );
  }
}

Main.childContextTypes = Object.assign({}, Main.childContextTypes, {
  goTab: PropTypes.func
})

export default Main;
