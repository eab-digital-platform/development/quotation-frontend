import React, {PropTypes} from 'react';
import * as _ from 'lodash';
import {Checkbox} from 'material-ui';

import {closeHistory, deleteQuickQuotes} from '../../../../actions/quickQuote';
import {getProposalByQuotId} from '../../../../actions/quotation';
import {parseDatetime, dayDiff} from '../../../../../common/DateUtils';
import {getIdDocTypeTitle} from '../../../../utils/client';

import styles from '../../../Common.scss';
import EABCompoennt from '../../../Component';
import ConfirmDialog from '../../../Dialogs/ConfirmDialog';
import WarningDialog from '../../../Dialogs/WarningDialog';
import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';

export default class QuickQuoteHistory extends EABCompoennt {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      open: false,
      quickQuotes: null,
      deleting: false,
      searchText: null,
      selectedQuotIds: {},
      confirmDelete: false,
      warningMsg: null
    });
  }

  componentDidMount() {
    const {store} = this.context;
    this.unsubscribe = store.subscribe(() => {
      const quickQuote = store.getState().quickQuote;
      let newState = {};
      if (!_.isEqual(this.state.open, quickQuote.open)) {
        newState.open = quickQuote.open;
      }
      if (!_.isEqual(this.state.quickQuotes, quickQuote.quickQuotes)) {
        newState.quickQuotes = quickQuote.quickQuotes;
      }
      if (!_.isEmpty(newState)) {
        this.setState(newState, () => {
          if (newState.open) {
            this.updateAppbar();
            this.floatingPage.open();
          }
        });
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.deleting !== this.state.deleting) {
      this.updateAppbar();
    }
  }

  updateAppbar() {
    const {langMap} = this.context;
    const {searchText} = this.state;
    this.appbarPage.initAppbar({
      menu: {
        icon: 'close',
        action: () => {
          const {deleting} = this.state;
          if (deleting) {
            this.setState({
              deleting: false,
              selectedQuotIds: {}
            });
          } else {
            closeHistory(this.context);
          }
        }
      },
      title: this.state.deleting ? null : window.getLocalizedText(langMap, 'products.quickQuote.searchHint'),
      maxSearchLength: 100,
      onSearchChange: (ordValue, newValue) => {
        this.setState({ searchText: newValue });
      },
      itemsIndex: 0,
      items: [[
        {
          type: 'flatButton',
          title: window.getLocalizedText(langMap, this.state.deleting ? 'products.quickQuote.deleteSelected' : 'products.quickQuote.deleteBtn'),
          action: () => {
            const {deleting, selectedQuotIds} = this.state;
            if (deleting && _.filter(selectedQuotIds, selected => selected).length > 0) {
              this.setState({ confirmDelete: true });
            } else {
              this.setState({ deleting: true });
            }
          }
        }
      ]]
    });
    this.appbarPage.updateSearchValue(searchText);
  }

  confirmDelete() {
    const {selectedQuotIds} = this.state;
    let quotIds = [];
    _.each(selectedQuotIds, (selected, quotId) => {
      if (selected) {
        quotIds.push(quotId);
      }
    });
    deleteQuickQuotes(this.context, quotIds, () => {
      this.setState({
        deleting: false,
        selectedQuotIds: {},
        confirmDelete: false
      });
    });
  }

  getIdDocTypeTitle() {
    const {lang} = this.context;
    const {template, profile} = this.context.store.getState().client;
    return (profile.idDocType === 'other' && profile.idDocTypeOther) || getIdDocTypeTitle(template.editDialog, profile, lang);
  }

  getQuotList() {
    const {lang, langMap, store} = this.context;
    const {client, app} = store.getState();
    const {deleting, searchText, quickQuotes, selectedQuotIds} = this.state;
    let items = [];
    let searchValues = (searchText || '').split(' ').map(v => v.toLowerCase());
    _.each(quickQuotes, (qq, index) => {
      let covName = window.getLocalText(lang, qq.covName);
      if (searchText) {
        let matchedValues = _.filter(searchValues, (v) => {
          return qq.id.toLowerCase().indexOf(v) > -1 || client.profile.idCardNo.toLowerCase().indexOf(v) > -1 || qq.pFullName.toLowerCase().indexOf(v) > -1 || covName.toLowerCase().indexOf(v) > -1;
        });
        if (matchedValues.length !== searchValues.length) {
          return;
        }
      }
      items.push(
        <div
          key={qq.id}
          style={{ padding: '18px 24px', display: 'flex', minHeight: '0px', cursor: deleting ? null : 'pointer' }}
          onTouchTap={() => {
            if (!deleting) {
              if (dayDiff(new Date(), parseDatetime(qq.createDate)) > 90) { // check if BI is older than 90 days
                this.setState({ warningMsg: window.getLocalizedText(langMap, 'products.quickQuote.error.expired') });
              } else {
                getProposalByQuotId(this.context, qq.id);
              }
            }
          }}
        >
          <div style={{ width: '40px' }}>
            {deleting ? (
              <Checkbox
                checked={selectedQuotIds[qq.id]}
                onCheck={(e, value) => {
                  selectedQuotIds[qq.id] = value;
                  this.setState({});
                }}
              />
            ) : null}
          </div>
          <div>
            <div className={styles.CardRowFieldLarge}>{covName} ({qq.id})</div>
            {qq.pCid !== qq.iCid ? (
              <div className={styles.CardRowFieldSmall}>{window.getLocalizedText(langMap, 'quotation.role_I')}: {qq.iFullName}</div>
            ) : null}
            <div className={styles.CardRowFieldSmall}>{window.getLocalizedText(langMap, 'quotation.role_P')}: {qq.pFullName}</div>
            <div className={styles.CardRowFieldSmall}>{this.getIdDocTypeTitle()}: {client.profile.idCardNo}</div>
            <div className={styles.CardRowFieldSmall}>{new Date(qq.lastUpdateDate).format(app.companyInfo.datetimeFormat)}</div>
          </div>
        </div>
      );
      items.push(
        <div key={'divider-' + index} className={styles.Divider} style={{ width: '100%' }} />
      );
    });
    if (items.length) {
      items.pop();
    }
    return (
      <div className={styles.CardContainer}>
        <div className={styles.Card} style={{ width: 720 }}>
          {items}
        </div>
      </div>
    );
  }

  render() {
    const {langMap} = this.context;
    const {deleting, confirmDelete, warningMsg} = this.state;
    return (
      <FloatingPage ref={ref => {this.floatingPage = ref;}}>
        <AppbarPage ref={ref => {this.appbarPage = ref;}} search={!deleting}>
          {this.getQuotList()}
          <ConfirmDialog
            open={confirmDelete}
            confirmMsg={window.getLocalizedText(langMap, 'products.quickQuote.confirmDelete')}
            onClose={() => this.setState({ confirmDelete: false })}
            onConfirm={() => this.confirmDelete()}
          />
          <WarningDialog
            open={!!warningMsg}
            isError
            msg={warningMsg}
            onClose={() => this.setState({ warningMsg: false })}
          />
        </AppbarPage>
      </FloatingPage>
    );
  }

}
