import React from 'react';
import PropTypes from 'prop-types';
import EABComponent from '../../../Component';

import {Toolbar, ToolbarTitle, ToolbarGroup,  Dialog, IconButton, FlatButton} from 'material-ui';
import _isEqual from 'lodash/fp/isEqual';
import SearchMenu from './searchMenu';
import styles from '../../../Common.scss';

export default class MobileSearchMenu extends EABComponent{
    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.state, {
          open: false
        });
    }

    componentWillReceiveProps(nextProps){
        if (!_isEqual(nextProps.open, this.state.open)){
            this.setState(Object.assign({}, this.state, nextProps));
        }
    }

    generateAppBar() {
        let { muiTheme, langMap } = this.context;
        const iconColor = muiTheme.palette.alternateTextColor;
      
        return (
            <Toolbar className= {styles.Toolbar} style={{background: '#FAFAFA', boxShadow: 'none'}}>
                <ToolbarGroup style={{padding: '16px'}}>
                    <FlatButton
                        label={window.getLocalizedText(langMap, 'alert.cancel', 'Cancel')}
                        style={{margin: 0}}
                        labelStyle={{ fontWeight: 'bold', color: iconColor}}
                        onTouchTap={this.props.closeMobileSearchMenu}
                    />
                </ToolbarGroup>
                <ToolbarGroup style={{display:'flex', marginRight:20, paddingTop: 2}}>
                    {window.getLocalizedText(langMap, 'menu.eapproval.mobilefiltertitle', 'Filters')}
                </ToolbarGroup>
                <ToolbarGroup style={{display:'flex', marginRight:20, paddingTop: 2}}>
                    <FlatButton
                        label={"Reset"}
                        style={{margin: 0}}
                        labelStyle={{ fontWeight: 'bold', color: iconColor}}
                        onTouchTap={()=>{this.props.resetFilter();}}
                    />
                </ToolbarGroup>
            </Toolbar>
        );
    }

    render(){
        return (
            <Dialog
                key='Mobile-search-menu-key'
                id='Mobile-search-menu-id'
                repositionOnUpdate={false}
                autoDetectWindowHeight={false}
                open={this.state.open}
                title={this.generateAppBar()}
                contentStyle={{width:'100%', maxWidth:'none', transform: 'translate(0, 0)'}}
                bodyStyle = {{padding: 0}}
                style={{paddingTop: 0}}
            >
                {this.props.searchMenuContent}
            </Dialog>
        );
    }
}

MobileSearchMenu.propTypes = {

};
