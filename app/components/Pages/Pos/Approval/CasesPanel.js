import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import styled, {css} from 'styled-components';

const Container = styled.div`
    max-width: calc(100% - 360px);
    overflow-x: auto;
    width: 100%
    ${
      props => props.iphone && css `
        max-width: 100%;
      `
    }
`;

const ContentBody = styled.div`
    overflow-y: auto;
    height: calc(100% - 100px);
    overflow-x: auto;
    padding-bottom: 20px;
`;

class CasesPanel extends PureComponent{
    render(){
        let { headerItems, items} = this.props;
        return (
            <Container iphone={window.isMobile.apple.phone}>
                {headerItems}
                <ContentBody>
                    {items}
                </ContentBody>
            </Container>
        );
    }
}

CasesPanel.propTypes = {
    headerItems: PropTypes.array,
    items: PropTypes.array
};

export default CasesPanel;
