import React from 'react';
import EABComponent from '../../../Component';

import FloatingPage from '../../FloatingPage';
import AppbarPage from '../../AppbarPage';

import {emailAllDoc, searchApprovalCaseByIdAndLockCase, unlockApprovalCaseById, searchApprovalCases, genSupervisorPdf, searchWorkbenchCases} from '../../../../actions/approval';

import DynColumn from '../../../DynViews/DynColumn';
import EmailDialog from '../../../Dialogs/EmailDialog';
import WarningDialog from '../../../Dialogs/WarningDialog';
import MobileTable from '../../../CustomViews/MobileTable';

import styles from '../../../Common.scss';

import * as _v from '../../../../utils/Validation';

import SupportDocPage from '../Applications/UploadDocument/index';

import moment from 'moment';

import cloneDeep from 'lodash/cloneDeep';
import clone from 'lodash/clone';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import get from 'lodash/get';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import {formatDateWithTime} from '../../../../../common/DateUtils';

import { DateFormat, DefaultEmailSender, DateTimeFormat3, MOMENT_TIME_ZONE } from '../../../../constants/ConfigConstants';
import _get from 'lodash/fp/get';
import _getOr from 'lodash/fp/getOr';
import _isEqual from 'lodash/fp/isEqual';
import _isEmpty from 'lodash/fp/isEmpty';
import * as _ from 'lodash';
import isFunction from 'lodash/isFunction';
import forEach from 'lodash/forEach';
import {APPRVOAL_STATUS_EXPIRED, APPRVOAL_STATUS_APPROVED, APPRVOAL_STATUS_REJECTED, APPRVOAL_STATUS_PFAFA, INPROGRESS_APPLICATION} from './approvalStatus';

import { getDocAction, FAADMIN, FAAGENT, ONELEVEL_SUBMISSION, getApproveStatusByRole, identifyApproveLevel, approveCase, rejectCase, getEmailAllDocObj, sendEmail, closeEmailDialog, UPDATE_CASE_BY_ID, closeMissingEappDialog} from '../../../../actions/approval';

import {USING_WORKBENCH_TEMPLATE_USED, USING_PENDING_APPROVAL_TEMPLATE} from './main';
const DISABLE_ALL_INDEX = 3;
const DISABLE_ALL_INDEX_WITHOUTEMAIL = 4;
export default class ReviewPage extends EABComponent{
    constructor(props, context){
        super(props);
        let {approval} = context.store.getState();
        this.state = Object.assign({}, this.state,{
                open: false,
                openReadOnlyMsg: false,
                changedValues: props.changedValues || {},
                reviewPageTemplate: approval.reviewPageTemplate || {},
                approvePageTemplate: approval.approvePageTemplate || {},
                rejectPageTemplate: approval.rejectPageTemplate || {},
                reviewPage_selectedClient_tmpl: approval.reviewPage_selectedClient_tmpl || {},
                reviewPage_jfw_tmpl: approval.reviewPage_jfw_tmpl || {},
                openApprovePage: false,
                openRejectPage: false,
                rejectChangedValues: /**get(approval, 'approvalCase.reject') ||*/ {},
                approveChangedValues: /**get(approval, 'approvalCase.accept') ||*/ {},
                openCompleteDialog: false,
                openInvalidApproveDialog: false,
                dialogTitle: "",
                openSuppDocsPage: false,
                supCompleteFlag: false,
                emailSent: false,
                tabIndex: 0,
                currentTemplate: props.currentTemplate,
                langMapTitle: '',
                reviewPageTitle: '',
                openEmailDialog: false,
                emailObj: null,
                canApprove: undefined,
                approvalCaseId: '',
                approveError: {},
                rejectError: {}
            }
        );
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let {openApprovePage, openRejectPage} = this.state;
        if (_.isEmpty(this.state.approveError) && openApprovePage) {
            this.approvalValidate(this.state.approveChangedValues);
        }

        if (_.isEmpty(this.state.rejectError) && openRejectPage) {
            this.rejectValidate(this.state.rejectChangedValues);
        }
    }

    componentWillReceiveProps(nextProps) {
        const {langMap, store} = this.context;
        const {approval, app:{agentProfile}} = store.getState();
        let newState = {};

        if (!window.isEqual(this.state.currentTemplate, nextProps.currentTemplate)){
            newState.currentTemplate = nextProps.currentTemplate;
        }

        if (!window.isEqual(this.state.openApprovePage, nextProps.openApprovePage)){
          newState.openApprovePage = nextProps.openApprovePage;
        }

        if (!window.isEqual(this.state.openRejectPage, nextProps.openRejectPage)){
          newState.openRejectPage = nextProps.openRejectPage;
        }

        if (nextProps.open){
            newState.changedValues = nextProps.changedValues;
            newState.open = nextProps.open;
            newState.tabIndex = 0;
            let lanMapReviewPageTitle;
            let reviewPageTitle;
            let openPeningForApprovalPage;
            let approvalLevel = identifyApproveLevel(agentProfile);
            if (nextProps.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE && approvalLevel === FAADMIN) {
                newState.langMapTitle = 'menu.eapproval.fafrimapproval';
                newState.reviewPageTitle = (window.isMobile.apple.phone) ? 'Case Details' : 'FA Firm Approval ';
                openPeningForApprovalPage = true;
            } else if (nextProps.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE ) {
                newState.langMapTitle = 'menu.eapproval.supervisorapproval';
                newState.reviewPageTitle = (window.isMobile.apple.phone) ? 'Case Details' : 'Supervisor Approval';
                openPeningForApprovalPage = true;
            }

            if (nextProps.currentTemplate === USING_WORKBENCH_TEMPLATE_USED ) {
                newState.langMapTitle = 'menu.eapproval.applicationReview';
                newState.reviewPageTitle = (window.isMobile.apple.phone) ? 'Case Details' : 'Application Review';
                openPeningForApprovalPage = false;
            }

            this.open(window.getLocalizedText(langMap, newState.langMapTitle, newState.reviewPageTitle), nextProps.changedValues.approvalCaseId,
              nextProps.changedValues.approvalStatus, this.context, nextProps.changedValues.approver, openPeningForApprovalPage, nextProps.changedValues);
        } else if (!window.isEqual(this.state.open, nextProps.open)){
            newState.open = nextProps.open;
        }

        if (!window.isEmpty(newState)) {
            this.setState(newState);
        }
    }

    componentDidMount(){
        this.unsubscribe = this.context.store.subscribe(this.storeListener);
        this.storeListener();
    }

    storeListener= ()=>{
        if(this.unsubscribe){
            let {store, langMap} = this.context;
            let {approval, pos, app:{agentProfile}} = store.getState();
            let newState = {};
            let reloadDocIcon = false;

            if (!window.isEqual(this.state.reviewPageTemplate, approval.reviewPageTemplate)){
                newState.reviewPageTemplate = Object.assign({}, approval.reviewPageTemplate);
            }

            if (!window.isEqual(this.state.approvePageTemplate, approval.approvePageTemplate)){
                newState.approvePageTemplate = Object.assign({}, approval.approvePageTemplate);
            }

            if (!window.isEqual(this.state.rejectPageTemplate, approval.rejectPageTemplate)){
                newState.rejectPageTemplate = Object.assign({}, approval.rejectPageTemplate);
            }

            if (!window.isEqual(this.state.reviewPage_selectedClient_tmpl, approval.reviewPage_selectedClient_tmpl)){
                newState.reviewPage_selectedClient_tmpl = Object.assign({}, approval.reviewPage_selectedClient_tmpl);
            }

            if (!window.isEqual(this.state.reviewPage_jfw_tmpl, approval.reviewPage_jfw_tmpl)){
                newState.reviewPage_jfw_tmpl = Object.assign({}, approval.reviewPage_jfw_tmpl);
            }

            if (!window.isEqual(this.state.changedValues, approval.approvalCase)){
                if (_.get(approval, 'approvalCase.approvalCaseId')) {
                    newState.approvalCaseId = approval.approvalCase.approvalCaseId;
                }
                newState.changedValues = Object.assign({}, approval.approvalCase);
                newState.changedValues.approvalCaseHasIndividual = approval.approvalCaseHasIndividual;
                newState.changedValues.approvalCaseHasIsCrossBorder = approval.approvalCaseHasIsCrossBorder;
                if(approval.approvalCase && approval.approvalCase.isFACase && this.state.changedValues.approvalCaseId !== approval.approvalCase.approvalCaseId) {
                    newState.approveChangedValues = Object.assign({}, get(approval, 'approvalCase.accept_FAAdmin'));
                    newState.rejectChangedValues = Object.assign({}, get(approval, 'approvalCase.reject_FAAdmin'));
                } else if(approval.approvalCase && this.state.changedValues.approvalCaseId !== approval.approvalCase.approvalCaseId){
                    newState.approveChangedValues = Object.assign({}, get(approval, 'approvalCase.accept'));
                    newState.rejectChangedValues = Object.assign({}, get(approval, 'approvalCase.reject'));
                }
            }

            if (!window.isEqual(this.state.canApprove, approval.canApprove) && approval.approvalCase && get(approval, 'approvalCase.approvalStatus')
            // if (!window.isEqual(this.state.canApprove, approval.canApprove) && approval.approvalCase && approval.approvalCase.approvalStatus
              && approval.approvalCase && approval.approvalCase.approvalCaseId
              && this.state.approvalCaseId !== approval.approvalCase.approvalCaseId
              && this.state.openApprovePage === false && this.state.openRejectPage === false
              &&  !window.isEqual(this.state.changedValues, approval.approvalCase)  ||  (approval.approvalCase && this.state.changedValues.readOnly !== approval.approvalCase.readOnly)
            ) {
              newState.canApprove = approval.canApprove;
              newState.approvalCaseId = approval.approvalCase.approvalCaseId;
              let approvalLevel = identifyApproveLevel(agentProfile);

              let applicationReviewStatus = ['A', 'R', 'E'];

              if (this.state.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE && approvalLevel === FAADMIN) {
                  newState.langMapTitle = 'menu.eapproval.fafrimapproval';
                  newState.reviewPageTitle = 'FA Firm Approval ';
              } else if (this.state.currentTemplate === USING_PENDING_APPROVAL_TEMPLATE ) {
                  newState.langMapTitle = 'menu.eapproval.supervisorapproval';
                  newState.reviewPageTitle = 'Supervisor Approval';
              }

              if (this.state.currentTemplate === USING_WORKBENCH_TEMPLATE_USED && newState.canApprove === true && approvalLevel === FAADMIN && applicationReviewStatus.indexOf(approval.approvalCase.approvalStatus) === -1
                //   && applicationReviewStatus.indexOf(approval.approvalCase.approvalStatus) === -1) {
                    && applicationReviewStatus.indexOf(get(approval, 'approvalCase.approvalStatus')) === -1) {
                newState.langMapTitle = 'menu.eapproval.fafrimapproval';
                newState.reviewPageTitle = 'FA Firm Approval ';
              } else if (this.state.currentTemplate === USING_WORKBENCH_TEMPLATE_USED && newState.canApprove === true
                //   && applicationReviewStatus.indexOf(approval.approvalCase.approvalStatus) === -1) {
                    && applicationReviewStatus.indexOf(get(approval, 'approvalCase.approvalStatus')) === -1) {
                newState.langMapTitle = 'menu.eapproval.supervisorapproval';
                newState.reviewPageTitle = 'Supervisor Approval';
              } else if (this.state.currentTemplate === USING_WORKBENCH_TEMPLATE_USED){
                newState.langMapTitle = 'menu.eapproval.applicationReview';
                newState.reviewPageTitle = 'Application Review';
              }

            //   this.updateAppbar(window.getLocalizedText(langMap, newState.langMapTitle, newState.reviewPageTitle), 0, undefined, false);

            }

            if(!_.isEqual(this.state.openSuppDocsPage, pos.openSuppDocsPage)) {
                newState.openSuppDocsPage = pos.openSuppDocsPage;
                if (pos.openSuppDocsPage === false) {
                    reloadDocIcon = true
                }
            }

            if(!_.isEqual(this.state.supCompleteFlag, approval.supCompleteFlag)) {
                newState.supCompleteFlag = approval.supCompleteFlag;
            }

            if (!_.isEqual(this.state.openEmailDialog, approval.openEmailDialog)) {
              newState.openEmailDialog = approval.openEmailDialog;
            }

            if (!_.isEqual(this.state.emailObj, approval.emailObj)) {
              newState.emailObj = approval.emailObj;
            }

            if (approval && _.get(approval, 'approvalCase.approvalCaseId') !== _.get(this.state, 'approvalCaseId')) {
                let langMapTitle = newState.langMapTitle || this.state.langMapTitle;
                let reviewPageTitle = newState.reviewPageTitle || this.state.reviewPageTitle;
                this.updateAppbar(window.getLocalizedText(langMap, langMapTitle, reviewPageTitle), 0, undefined, false);
            }

            if(!window.isEmpty(newState) && reloadDocIcon){
                this.setState(newState);
                getDocAction(this.context, approval.approvalCase, (resp)=>{
                  this.updateAppbar(window.getLocalizedText(langMap, this.state.langMapTitle, this.state.reviewPageTitle), 0, undefined, false, resp);
                  this.setState({supCompleteFlag: resp});
                });
            } else if(!window.isEmpty(newState)){
                this.setState(newState);
            }
        }
    }

    componentWillUnmount(){
        if(isFunction(this.unsubscribe)){
            this.unsubscribe();
            this.unsubscribe = undefined;
        }
    }

    open(title, id, status, context, approverList, openPeningForApprovalPage, changedValues) {
      let {store, langMap} = this.context;
      let {app:{agentProfile}} = store.getState();
      let canApprove = false;
      if((approverList && approverList.indexOf(agentProfile.agentCode) > -1) || openPeningForApprovalPage) {
        canApprove = true;
      }
      let index;
      index = this.diableEditPage_AproveRejectBtn(status, index);
      //this.updateAppbar(title, index, undefined, true);
    //   this.getReviewPageTemplate();
      if (changedValues.inProgressApp) {
        this.updateAppbar((window.isMobile.apple.phone) ? 'Case Details' : window.getLocalizedText(langMap, 'menu.eapproval.applicationReview', 'Application Review'), 4, undefined, true);
        store.dispatch({
          type: UPDATE_CASE_BY_ID,
          approvalCase: changedValues
        });
      } else {
        searchApprovalCaseByIdAndLockCase(context, id, canApprove,(approvalCase)=>{
          if (approvalCase.readOnly) {
            this.setState({
              openReadOnlyMsg: true
            });
          }
        });
      }
      this.floatingPage.open();
    }

    close() {
      let {approvalCaseId} = this.state;
      if (approvalCaseId){
        unlockApprovalCaseById(this.context, approvalCaseId);
      }
      this.floatingPage.close();
      this.props.closeReviewPage();
      this.setState({
        langMapTitle: '',
        reviewPageTitle: '',
        changedValues: {}
      });
    }

    diableEditPage_AproveRejectBtn(appStatus, index=0) {
        let {store} = this.context;
        let {app:{agentProfile}, approval} = store.getState();
        let approvalLevel = identifyApproveLevel(agentProfile);
        if (appStatus === INPROGRESS_APPLICATION) {
          return DISABLE_ALL_INDEX_WITHOUTEMAIL;
        } else if (appStatus === APPRVOAL_STATUS_APPROVED
        || appStatus === APPRVOAL_STATUS_REJECTED
        || appStatus === APPRVOAL_STATUS_EXPIRED
        || (appStatus === APPRVOAL_STATUS_PFAFA &&  approvalLevel !== FAADMIN)
        || (approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED && (approval.canApprove === false || approval.canApprove === undefined))) {
            return DISABLE_ALL_INDEX;
        } else {
            return index;
        }
    }

    generateApproveRejectBtn() {
        const {langMap, store} = this.context;
        let { supCompleteFlag } = this.state;
        let {app: {agentProfile}} = store.getState();
        let allowtoApprove = supCompleteFlag || identifyApproveLevel(agentProfile) === FAADMIN
        return (
            <div>
                <div style={{display: 'flex', minHeight: '0px', justifyContent: 'space-between'}}>
                    <FlatButton
                        key='Approval-approve-key'
                        label={window.getLocalizedText(langMap, 'menu.eapproval.approveBtn', 'Approve')}
                        disabled={!allowtoApprove}
                        onTouchTap={(e)=>{
                            e.preventDefault();
                            if (allowtoApprove) {
                                this.approvalValidate(this.state.approveChangedValues);
                                this.setState({
                                    openApprovePage: true,
                                    openRejectPage: false,
                                    approveChangedValues: this.state.approveChangedValues || {},
                                    approveError: {}
                                });
                            }
                        }}
                        backgroundColor={(allowtoApprove) ? '#103184' : 'grey'}
                        labelStyle={{color: 'white'}}
                        style={{width: '48%'}}
                    />
                    <FlatButton
                        key='Approval-reject-key'
                        label={window.getLocalizedText(langMap, 'menu.eapproval.rejectBtn', 'Reject')}
                        onTouchTap={()=>{
                            this.rejectValidate(this.state.rejectChangedValues);
                            this.setState({
                                openRejectPage: true,
                                openApprovePage: false,
                                rejectChangedValues: this.state.rejectChangedValues || {},
                                rejectError: {}
                            });
                        }}
                        backgroundColor={'rgb(170,0,0)'}
                        labelStyle={{color: 'white'}}
                        style={{width: '48%'}}
                    />
                </div>
                {
                    (identifyApproveLevel(agentProfile) === FAADMIN) ?
                        undefined :
                    <div style={{padding: '12px 0px'}}>
                        {this.incompleteDialogTitle()}
                    </div>
                }
            </div>
        );
    }

    generateAppbarBtn(allowToApprove) {
        const {langMap, store} = this.context;
        let { supCompleteFlag } = this.state;
        let {app: {agentProfile}, approval} = store.getState();

        let readOnly = approval.approvalCase ? approval.approvalCase.readOnly : false;
        let disableBtn = false;
        if (readOnly) {disableBtn = true;}

        if (window.isMobile.apple.phone) {
            return [
                {
                    id: 'emailAllDoc',
                    type: 'flatButton',
                    title: window.getLocalizedText(langMap, 'menu.eapproval.emaillalldocs', 'Email'),
                    action: ()=>{
                    const appId = _get('applicationId', this.state.changedValues);
                    getEmailAllDocObj(this.context, appId);
                    }
                }
            ];
        } else {
            return [
                {
                    id: 'emailAllDoc',
                    type: 'flatButton',

                    title: window.getLocalizedText(langMap, 'menu.eapproval.emaillalldocs', 'Email'),
                    action: ()=>{
                    const appId = _get('applicationId', this.state.changedValues);
                    getEmailAllDocObj(this.context, appId);
                    }
                },
                {
                    id: 'approveBtn',
                    type: 'flatButton',
                    title: window.getLocalizedText(langMap, 'menu.eapproval.approveBtn', 'Approve'),
                    disabled: disableBtn,
                    action: ()=>{
                        if (allowToApprove || supCompleteFlag || identifyApproveLevel(agentProfile) === FAADMIN) {
                        this.approvalValidate(this.state.approveChangedValues);
                        this.setState({
                            openApprovePage: true,
                            openRejectPage: false,
                            approveChangedValues: this.state.approveChangedValues || {},
                            approveError: {}
                        });
                        } else {

                        this.setState({
                            openInvalidApproveDialog: true,
                            dialogTitle: this.incompleteDialogTitle()
                        });
                    }
                    }
                },
                {
                    id: 'rejectBtn',
                    type: 'flatButton',
                    title: window.getLocalizedText(langMap, 'menu.eapproval.rejectBtn', 'Reject'),
                    disabled: disableBtn,
                    action: ()=>{
                        this.rejectValidate(this.state.rejectChangedValues);
                        this.setState({
                            openRejectPage: true,
                            openApprovePage: false,
                            rejectChangedValues: this.state.rejectChangedValues || {},
                            rejectError: {}
                        });
                    }
                }
            ];
        }
    }

    updateAppbar(title, index, disableBtn, isInit, allowToApprove) {
        let {langMap, store} = this.context;
        let {approval} = store.getState();
        if (!isInit) {
            // index = this.diableEditPage_AproveRejectBtn(approval.approvalCase.approvalStatus, index);
            index = this.diableEditPage_AproveRejectBtn(get(approval, 'approvalCase.approvalStatus'), index);
        }
        let menuGroup = [
            {
                icon: 'back',
                action: () => {
                  this.close();
                  this.setState({
                    canApprove: undefined,
                    rejectChangedValues: {},
                    approveChangedValues: {},
                    approveError: {},
                    rejectError: {}
                  })
                }
            },
            {
                icon: (window.isMobile.apple.phone) ? undefined : 'back',
                id: (window.isMobile.apple.phone) ? 'approveCancel' : undefined,
                type: (window.isMobile.apple.phone) ? 'flatButton' : undefined,
                title: (window.isMobile.apple.phone) ? window.getLocalizedText(langMap, 'menu.eapproval.cancel', 'Cancel') : undefined,
                action: () => {
                    this.updateAppbar(window.getLocalizedText(langMap, this.state.langMapTitle, this.state.reviewPageTitle), 0, undefined, false);
                    this.setState({
                        openApprovePage: false,
                        openRejectPage: false,
                        rejectChangedValues: {},
                        approveChangedValues: {},
                        canApprove: undefined,
                        approveError: {},
                        rejectError: {}
                    });
                }
            },
            {
                icon: (window.isMobile.apple.phone) ? undefined : 'back',
                id: (window.isMobile.apple.phone) ? 'rejectCancel' : undefined,
                type: (window.isMobile.apple.phone) ? 'flatButton' : undefined,
                title: (window.isMobile.apple.phone) ? window.getLocalizedText(langMap, 'menu.eapproval.cancel', 'Cancel') : undefined,
                action: () => {
                    this.updateAppbar(window.getLocalizedText(langMap, this.state.langMapTitle, this.state.reviewPageTitle), 0, undefined, false);
                    this.setState({
                        openApprovePage: false,
                        openRejectPage: false,
                        rejectChangedValues: {},
                        approveChangedValues: {},
                        canApprove: undefined,
                        approveError: {},
                        rejectError: {}
                    });
                }
            },
            {
                icon: 'back',
                action: () => {
                  this.close();
                  this.setState({
                    canApprove: undefined,
                    rejectChangedValues: {},
                    approveChangedValues: {},
                    approveError: {},
                    rejectError: {}
                  })
                }
            },
            {
              icon: 'back',
              action: () => {
                this.close();
                this.setState({
                  canApprove: undefined,
                  rejectChangedValues: {},
                  approveChangedValues: {},
                  approveError: {},
                  rejectError: {}
                })
              }
          }
        ];
        if (this.appbarPage) {
          this.appbarPage.initAppbar({
            menu: menuGroup[index],
            // index 1 and index 2 --> Approve Case and Reject Case (NOT to replace that title in mobile)
            title: (window.isMobile.apple.phone && index !== 1 && index !== 2) ? 'Case Details' : title || window.getLocalizedText(langMap, 'menu.eapproval.applicationReview', 'Application Review'),
            itemsIndex: index,
            titleStyle: (window.isMobile.apple.phone) ? {width: '100%', fontWeight: 500, color: 'black'} : undefined,
            items:[
                this.generateAppbarBtn(allowToApprove),
                [
                    {
                        id: 'approveSubmit',
                        type: 'flatButton',
                        title: window.getLocalizedText(langMap, 'menu.eapproval.submit', 'Submit'),
                        disabled: disableBtn,
                        action: ()=>{
                            this.approveSubmit(this.state.changedValues);
                        }
                    }
                ],
                [
                    {
                        id: 'rejectSubmit',
                        type: 'flatButton',
                        title: window.getLocalizedText(langMap, 'menu.eapproval.submit', 'Submit'),
                        disabled: disableBtn,
                        action: ()=>{
                            this.rejectSubmit(this.state.changedValues);
                        }
                    }
                ],[
                  {
                      id: 'emailAllDoc',
                      type: 'flatButton',
                      title: window.getLocalizedText(langMap, 'menu.eapproval.emaillalldocs', 'Email'),
                      action: ()=>{
                        const appId = _get('applicationId', this.state.changedValues);
                        getEmailAllDocObj(this.context, appId);
                      }
                  }
                ],
                [
                ]
            ]
            });
        }
    }

    incompleteDialogTitle() {
      let {approval={}} = this.context.store.getState();

      if (approval.showMissingEappPrompt) {
        return <div>{'There is a technical error and the case can’t be approved now. Please contact your EASE Champion or call/message to 90237577 and we will rectify it for you.'}</div>;
      } else if (approval.healthDeclarationFlag) {
        return <div>{'Please submit a “Health Declaration” form as it is more than 21 days from the proposer’s eApp signature date.  You may approve the application only after upload of the “Health Declaration” form.'}</div>;
      } else {
        if (!approval.mandataryFlag && !approval.reviewDoneFlag)  {
            return <div>{'Please check FNA, PI, Application and additional document uploaded (if any) before approving the case.'}<br/><br/>{'Please upload mandatory document(s).'}</div>
          } else if (!approval.mandataryFlag) {
            return 'Please upload mandatory document(s).'
          } else if (!approval.reviewDoneFlag)
            return 'Please check FNA, PI, Application and additional document uploaded (if any) before approving the case.'
      }
    }

    emailSentDialogView = () => {
        return (
            <Dialog open={this.state.emailSent}
                    title={'NOTICE'}
                    actions={
                    [<FlatButton
                        label='OK'
                        primary={true}
                        onTouchTap={()=>{
                            this.setState({emailSent: false});
                        }}
                    />]
                }
            >
                Email has been sent.
            </Dialog>
        );
    }

    approvalValidate = (changedValues) => {
        let {langMap, optionsMap} = this.context;
        let {approvePageTemplate, index, values, approveChangedValues} = this.state;
        let newState = {};
        let error = {};
        _v.exec(approvePageTemplate, optionsMap, langMap, approveChangedValues, approveChangedValues, approveChangedValues, error);
        newState.approveError = error;
        if (error.code === undefined) {
            this.updateAppbar('Approve Case', 1, false, false);
        } else {
            this.updateAppbar('Approve Case', 1, true, false);
        }

        if (!window.isEmpty(approveChangedValues)){
            newState.approveChangedValues = approveChangedValues;
        }

        if (!window.isEmpty(newState)){
            this.setState(newState);
        }
    }

    rejectValidate = (changedValues) => {
        let {langMap, optionsMap} = this.context;
        let {rejectPageTemplate, index, values, rejectChangedValues} = this.state;
        let newState = {};
        let error = {};
        _v.exec(rejectPageTemplate, optionsMap, langMap, rejectChangedValues, rejectChangedValues, rejectChangedValues, error);
        newState.rejectError = error;
        if (error.code === undefined) {
            this.updateAppbar('Reject Case', 2, false, false);
        } else {
            this.updateAppbar('Reject Case', 2, true, false);
        }

        if (!window.isEmpty(rejectChangedValues)){
            newState.rejectChangedValues = rejectChangedValues;
        }

        if (!window.isEmpty(newState)) {
            this.setState(newState);
        }
    }

    approveSubmit = () => {
        let {langMap} = this.context;
        let {approveChangedValues} = this.state;
        let {approval={}, app:{agentProfile}} = this.context.store.getState();

        new Promise((resolve, reject) => {
            approveCase(this.context, approval.approvalCase, approveChangedValues, approval.approvalCaseHasIndividual, () => {
                resolve();
            });
        }).then(()=>{
            this.setState({
                openCompleteDialog: true,
                dialogTitle: 'Your case has been approved.'
            })
        });
    }

    rejectSubmit = () => {
        let {lang} = this.context;
        let {rejectChangedValues} = this.state;
        let {approval={}, app:{agentProfile}}= this.context.store.getState();

        new Promise((resolve)=>{
            rejectCase(this.context, approval.approvalCase, rejectChangedValues, approval.approvalCaseHasIndividual, () => {
                resolve();
            });
        }).then(()=>{
            this.setState({
                openCompleteDialog: true,
                dialogTitle: 'Your case has been rejected.'
            })
        });
    }

    genContent = (approvePageOpened, rejectedPageOpened) => {
        if (approvePageOpened || rejectedPageOpened)
            return <div></div>

        let {reviewPageTemplate, changedValues = {}, tabIndex, reviewPage_selectedClient_tmpl, reviewPage_jfw_tmpl} = this.state;
        let {approval ={}} = this.context.store.getState();
        let contentArr = [];
        contentArr.push(this.genTabs());
        if (approval.approvalCase && this.state.changedValues.approvalCaseId === approval.approvalCase.approvalCaseId, tabIndex === 0){
            if (this.diableEditPage_AproveRejectBtn(changedValues.approvalStatus, 0) === DISABLE_ALL_INDEX || this.diableEditPage_AproveRejectBtn(changedValues.approvalStatus, 0) === DISABLE_ALL_INDEX_WITHOUTEMAIL || changedValues.readOnly) {
                this.disableTemplateItems(reviewPageTemplate, true);
                changedValues.showApproveRejectSection = false;
            } else {
                this.disableTemplateItems(reviewPageTemplate, false);
                changedValues.showApproveRejectSection = true;
            }

            let reviewPageStyleClass = (window.isMobile.apple.phone) ? styles.ReviewPageContainer : styles.SectionPadding + ' ' + styles.ReviewPageContainer;
            contentArr.push(
                <div className={reviewPageStyleClass}>
                    <DynColumn
                        template={reviewPageTemplate}
                        changedValues={changedValues}
                        values={changedValues}
                        ApprovalApproveRejectSection={this.generateApproveRejectBtn()}
                    />
                </div>
            );
        }else if (tabIndex === 1) {
            contentArr.push(this.genSelClientnJwfInfo(reviewPage_selectedClient_tmpl, changedValues.accept));
        } else if(tabIndex === 2) {
            if (changedValues.accept) {
                changedValues.accept.jfwFileName = changedValues.jfwFileName;
                if (changedValues.coeFileName){
                  changedValues.accept.coeFileName = changedValues.coeFileName;
                  changedValues.accept.coeFileExist = (changedValues.coeFileName.length > 0);
                }
                changedValues.accept.docId = changedValues.approvalCaseId;
            }

            contentArr.push(this.genSelClientnJwfInfo(reviewPage_jfw_tmpl, changedValues.accept));
        } else {
            contentArr.push(<div></div>);
        }

        return contentArr;
    }

    genSelClientnJwfInfo = (template, templateValues={}) =>{
        return (
            <div className={styles.SectionPadding + ' ' + styles.ReviewPageContainer}>
                <DynColumn
                    template={template}
                    changedValues={templateValues}
                    values={templateValues}
                />
            </div>
        )
    }

    disableTemplateItems(template, disabled) {
        if (template && template.items) {
            template.items.forEach(obj =>{
                this.disableTemplateItems(obj, disabled);
            });
        } else if (template && isArray(template)){
            template.forEach(obj =>{
               this.disableTemplateItems(obj, disabled);
            });
        } else if (template && isObject(template)) {
            template.disabled = disabled;
        }
    }

    genApproveContent = (open) => {
        if (open !== true)
            return <div></div>
        let {approvePageTemplate, approveChangedValues = {}, changedValues} = this.state;
        let {approval ={}} = this.context.store.getState();

        if (approval.approvalCase && changedValues.approvalCaseId === approval.approvalCase.approvalCaseId){
            approveChangedValues.approvalCaseHasIndividual = approval.approvalCaseHasIndividual;
            approveChangedValues.approvalCaseHasIsCrossBorder = approval.approvalCaseHasIsCrossBorder;
            const reviewPageStyleClass = (window.isMobile.apple.phone) ? styles.MobileApproveRejectPageContainer : styles.SectionPadding + ' ' + styles.ReviewPageContainer;
            return (
                <div className={reviewPageStyleClass}>
                    <DynColumn
                        template={approvePageTemplate}
                        changedValues={approveChangedValues}
                        values={approveChangedValues}
                        validate={this.approvalValidate}
                        error={this.state.approveError}
                        validateFirst
                    />
                </div>
            );
        }else {
            return <div></div>;
        }
    }

    genRejectContent = (open) => {
        if (open !== true)
            return <div></div>

        let {rejectPageTemplate, rejectChangedValues = {}, changedValues} = this.state;
        let {approval ={}} = this.context.store.getState();
        if (approval.approvalCase && changedValues.approvalCaseId === approval.approvalCase.approvalCaseId){
            const reviewPageStyleClass = (window.isMobile.apple.phone) ? styles.MobileApproveRejectPageContainer : styles.SectionPadding + ' ' + styles.ReviewPageContainer;
            return (
                <div className={reviewPageStyleClass}>
                    <DynColumn
                        template={rejectPageTemplate}
                        changedValues={rejectChangedValues}
                        values={rejectChangedValues}
                        validate={this.rejectValidate}
                        error={this.state.rejectError}
                        validateFirst
                    />
                </div>
            );
        } else return <div></div>;
    }

    handleTabChange = (value) =>{
        this.setState({
            tabIndex: value,
        });
    }

    genTabs = () =>{
        let {rejectPageTemplate, rejectChangedValues = {}, changedValues} = this.state;
        let tabArr =[];
        tabArr.push(<Tab label="Case Information" value={0} />);
        if ((changedValues.approvalStatus === APPRVOAL_STATUS_APPROVED && !changedValues.isFACase)
            || changedValues.approvalStatus === APPRVOAL_STATUS_REJECTED && !changedValues.isFACase ) {
              return  (
                <Tabs
                onChange={this.handleTabChange}
                value={this.state.tabIndex}>
                    <Tab label="Case Information" value={0} />
                    <Tab label="Selected Client" value={1} />
                    <Tab label="Joint Field Work" value={2} />
                </Tabs>);
        }
    }

    handleCloseReadOnlyMsg = () =>{
      this.setState({
        openReadOnlyMsg: false
      });
    }

    render() {
      let {langMap, store} = this.context;
      let {approval} = store.getState();
        let { dialogTitle, openCompleteDialog, openInvalidApproveDialog, openEmailDialog, emailObj, langMapTitle, reviewPageTitle, openReadOnlyMsg} = this.state;
        let actions = [
            <FlatButton
                label="OK"
                primary={true}
                onClick={()=>{
                    this.updateAppbar(window.getLocalizedText(langMap, '', ''), DISABLE_ALL_INDEX, undefined, false);
                    this.setState({
                        openCompleteDialog: false,
                        openApprovePage: false,
                        openRejectPage: false,
                        langMapTitle: '',
                        reviewPageTitle: '',
                        open: false,
                        changedValues: {},
                        rejectChangedValues: {},
                        approveChangedValues: {},
                        canApprove: undefined
                    });
                    this.floatingPage.close();
                    this.props.closeReviewPage();
                    if (approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED) {
                        searchWorkbenchCases(this.context, approval.workbenchFilter, () => {});
                    } else {
                        searchApprovalCases(this.context, approval.approvalFilter, ()=>{});
                    }
                    if (approval.currentTemplate === USING_WORKBENCH_TEMPLATE_USED) {
                        searchWorkbenchCases(this.context, approval.workbenchFilter, () => {});
                    } else {
                        searchApprovalCases(this.context, approval.approvalFilter, ()=>{});
                    }
                }}
            />,
        ];

        if (openInvalidApproveDialog){
            actions = [
                <FlatButton
                    label="OK"
                    primary={true}
                    onClick={()=>{
                        this.setState({
                            openInvalidApproveDialog: false
                        });
                    }}
                />,
            ];
        }

        let {openApprovePage, openRejectPage} = this.state
        return (
            <FloatingPage ref={ref => { this.floatingPage = ref; }} zIndex={this.props.zindex} key="review-page-key" id="review-page-id">
                <AppbarPage ref={ref => { this.appbarPage = ref; }}>
                    {this.emailSentDialogView()}
                    {this.genContent(openApprovePage, openRejectPage)}
                    {this.genApproveContent(openApprovePage)}
                    {this.genRejectContent(openRejectPage)}
                    <Dialog
                        title={dialogTitle}
                        actions={actions}
                        modal={true}
                        open={openCompleteDialog || openInvalidApproveDialog}
                    />
                    {this.state.openSuppDocsPage?<SupportDocPage isShield={this.context.store.getState().pos.isShield || false}/>:null}
                    <EmailDialog
                      open={openEmailDialog}
                      emails={emailObj ? [emailObj] : []}
                      onSend={(emails) => emailAllDoc(this.context, _get('applicationId', this.state.changedValues), emails)}
                      onClose={() => closeEmailDialog(this.context)}
                    />
                    <WarningDialog
                        open={openReadOnlyMsg}
                        hasTitle={false}
                        msg = {window.getLocalizedText(langMap, 'reviewPage.readOnlyMsg')}
                        onClose={() => this.handleCloseReadOnlyMsg()}
                        style={{whiteSpace:'pre-line'}}
                    />
                </AppbarPage>
            </FloatingPage>
        );
    }
}
