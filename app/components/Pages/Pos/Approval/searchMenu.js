import React, { PureComponent } from 'react';
import DynColumn from '../../../DynViews/DynColumn';
import PropTypes from 'prop-types';
import DefaultConstants from '../../../../../common/DefaultConstants';

export default class SearchMenu extends PureComponent{
    render(){
        let { key, template, changedValues, values, customStyleClass} = this.props;
        return (<DynColumn
                    key={key}
                    template={template}
                    changedValues={changedValues}
                    values={values}
                    customStyleClass={customStyleClass}
                    isApproval
                    isSearchMenu
                />);
    }
}

SearchMenu.propTypes = {
    key: PropTypes.string.isRequired,
    template: PropTypes.object.isRequired,
    changedValues: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired,
    customStyleClass: PropTypes.string.isRequired
};
