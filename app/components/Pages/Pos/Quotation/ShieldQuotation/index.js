import React, {PropTypes} from 'react';
import * as _ from 'lodash';
import {Divider} from 'material-ui';

import EABComponent from '../../../../Component';
import ConfirmDialog from '../../../../Dialogs/ConfirmDialog';
import WarningDialog from '../../../../Dialogs/WarningDialog';
import MultiClientProfile from '../../../../Dialogs/MultiClientProfile';
import ClientInfo from '../ClientInfo';
import ShieldInsuredInfo from './ShieldInsuredInfo';
import ShieldClientQuotation from './ShieldClientQuotation';

import styles from '../../../../Common.scss';

import {quote, closeQuotation} from '../../../../../actions/quotation';
import {updateClients, selectInsured, updateRiders} from '../../../../../actions/shieldQuotation';
import {removeInclusiveRiders, removeDisabledRiders} from '../utils';
import {genProfileMandatory} from '../../../../../utils/client';
import Constants from '../../../../../constants/SystemConstants';

export default class Quotation extends EABComponent {

  static propTypes = {
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    inputConfigs: PropTypes.object,
    quotationErrors: PropTypes.object,
    quotWarnings: PropTypes.array,
    availableInsureds: PropTypes.array
  };

  static childContextTypes = {
    handleConfigChange: PropTypes.func,
    handleRemovePlan: PropTypes.func,
    handleUpdatePlanList: PropTypes.func,
    onChangeInsuredBp: PropTypes.func,
    showErrorMsg: PropTypes.func,
    openClientProfile: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      errorMsg: null,
      onCloseErrorMsg: null,
      confirmMsg: null,
      onConfirm: null
    });
  }

  getChildContext() {
    return {
      handleConfigChange: () => {
        this.setState({}); // force rerender
        quote(this.context, this.props.quotation);
      },
      handleRemovePlan: (cid, plan) => {
        const {langMap} = this.context;
        this.setState({
          confirmMsg: window.getLocalizedText(langMap, 'quotation.riders.confirmRemove'),
          onConfirm: () => {
            const {quotation, planDetails, inputConfigs} = this.props;
            const bpDetail = planDetails[quotation.baseProductCode];
            const riderList = inputConfigs[cid][quotation.baseProductCode].riderList;
            const subQuot = quotation.insureds[cid];
            let selectedCovCodes = [];
            _.each(subQuot.plans, (p) => {
              if (p.covCode !== plan.covCode) {
                selectedCovCodes.push(p.covCode);
              }
            });
            selectedCovCodes = removeInclusiveRiders(plan.covCode, selectedCovCodes, subQuot, bpDetail, riderList);
            selectedCovCodes = removeDisabledRiders(selectedCovCodes, subQuot, bpDetail, riderList);
            updateRiders(this.context, selectedCovCodes, cid, () => {
              this.setState({
                confirmMsg: null,
                onConfirm: null
              });
            });
          }
        });
      },
      handleUpdatePlanList: (cid, covCodes, callback, errorCallback) => {
        updateRiders(this.context, covCodes, cid, callback, errorCallback);
      },
      onChangeInsuredBp: (cid, covClass) => {
        if (this.props.quotation.insureds[cid]) {
          this.setState({
            confirmMsg: window.getLocalizedText(this.context.langMap, 'quotation.shield.confirmClearLA'),
            onConfirm: () => {
              selectInsured(this.context, cid, covClass);
              this.setState({
                confirmMsg: null,
                onConfirm: null
              });
            }
          });
        } else if (_.size(this.props.quotation.insureds) >= 6) {
          this.setState({
            errorMsg: window.getLocalizedText(this.context.langMap, 'quotation.shield.maxLA')
          });
        } else {
          selectInsured(this.context, cid, covClass);
        }
      },
      showErrorMsg: (msg) => {
        this.setState({ errorMsg: msg });
      },
      openClientProfile: () => {
        const {availableInsureds} = this.props;
        const iCids = [];
        _.each(availableInsureds, (insured) => {
          if (!insured.valid) {
            iCids.push(insured.profile.cid);
          }
        });
        this.clientProfile.openDialog(iCids);
      }
    };
  }

  _onUpdateClientProfiles() {
    updateClients(this.context, (error) => {
      this.setState({
        errorMsg: error,
        onCloseErrorMsg: () => closeQuotation(this.context, true)
      });
    });
  }

  _getClientInfoSection() {
    const {quotation} = this.props;
    let proposer = {
      cid: quotation.pCid,
      fullName: quotation.pFullName,
      firstName: quotation.pFirstName,
      lastName: quotation.pLastName,
      gender: quotation.pGender,
      dob: quotation.pDob,
      age: quotation.pAge,
      residence: quotation.pResidence,
      smoke: quotation.pSmoke,
      email: quotation.pEmail,
      occupation: quotation.pOccupation
    };
    return <ClientInfo role="P" profile={proposer} singleLine />;
  }

  _getClientQuotationSections() {
    const {quotation, planDetails, inputConfigs, availableInsureds} = this.props;
    let items = [];
    _.each(availableInsureds, (insured) => {
      if (!insured.valid || insured.eligible) {
        const cid = insured.profile.cid;
        const subQuot = quotation.insureds && quotation.insureds[cid];
        items.push(
          <ShieldClientQuotation
            style={{ margin: '32px 0' }}
            key={`clientquot-${cid}`}
            profile={insured.profile}
            availableClasses={insured.availableClasses}
            validProfile={insured.valid}
            eligible={insured.eligible}
            baseProductCode={quotation.baseProductCode}
            quotation={subQuot}
            planDetails={planDetails}
            inputConfigs={inputConfigs[cid]}
          />
        );
      }
    });
    return items;
  }

  _getNonEligibleSection() {
    const {langMap} = this.context;
    const {quotation, planDetails, availableInsureds} = this.props;
    const bpDetail = planDetails[quotation.baseProductCode];
    const items = [];
    _.each(availableInsureds, (insured) => {
      if (insured.valid && !insured.eligible) {
        items.push(
          <div key={insured.profile.cid} style={{ margin: '32px 0' }}>
            <ShieldInsuredInfo
              profile={insured.profile}
              bpDetail={bpDetail}
            />
          </div>
        );
      }
    });
    if (items.length) {
      return (
        <div>
          <Divider style={{ height: 2 }} />
          <div style={{ margin: '32px 0', fontWeight: 'bold', fontStyle: 'italic', whiteSpace: 'pre-wrap' }}>
            {window.getLocalizedText(langMap, 'quotation.shield.laNotEligible')}
          </div>
          {items}
        </div>
      );
    }
  }

  render() {
    const {errorMsg, onCloseErrorMsg, confirmMsg, onConfirm} = this.state;
    const {quotation} = this.props;
    const moduleId = quotation.quickQuote ? Constants.FEATURES.QQ : Constants.FEATURES.FQ;
    const hasFNA = _.indexOf(_.get(this.context.store.getState(), 'app.agentProfile.features'), 'FNA') > -1? true: false;
    return (
      <div className={styles.Quotation}>
        <div style={{ flex: 1, overflow: 'auto', padding: '0 24px' }}>
          <div className={styles.QuotationContent}>
            {this._getClientInfoSection()}
            {this._getClientQuotationSections()}
            {this._getNonEligibleSection()}
            <ConfirmDialog
              open={!!confirmMsg}
              confirmMsg={confirmMsg}
              onClose={() => this.setState({ confirmMsg: null })}
              onConfirm={() => onConfirm && onConfirm()}
            />
            <WarningDialog
              open={!!errorMsg}
              isError
              msg={errorMsg}
              onClose={() =>
                onCloseErrorMsg ? onCloseErrorMsg() : this.setState({ errorMsg: null })
              }
            />
            <MultiClientProfile
              ref={ref => { this.clientProfile = ref; }}
              isAllowSave
              pMandatoryId={genProfileMandatory(moduleId, hasFNA, true, false)}
              iMandatoryId={genProfileMandatory(moduleId, hasFNA, false, false)}
              onSubmit={() => this._onUpdateClientProfiles()}
            />
          </div>
        </div>
      </div>
    );
  }

}
