import React, { PropTypes } from 'react';
import {Paper, DatePicker, Checkbox, SelectField, MenuItem, TextField} from 'material-ui';
import * as _ from 'lodash';

import styles from '../../../Common.scss';
import EABComponent from '../../../Component';
import WarningDialog from '../../../Dialogs/WarningDialog';
import NumericField from '../../../CustomViews/NumericField';
import TermAndCondition from './TermAndCondition';

export default class PolicyOptions extends EABComponent {

  static propTypes = {
    style: PropTypes.object,
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    inputConfigs: PropTypes.object,
    quotationErrors: PropTypes.object
  };

  static contextTypes = Object.assign({}, EABComponent.contextTypes, {
    handleConfigChange: PropTypes.func,
    handleResetQuot: PropTypes.func,
    confirmAction: PropTypes.func
  });

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      warningMsg: null,
      init: 0,
      onDismissWarning: null,
      focusOption: null
    });
  }

  _getPolicyOptions() {
    const {inputConfigs} = this.props;
    const policyOptions = [];
    _.each(inputConfigs, (inputConfig, covCode) => {
      _.each(inputConfig.policyOptions, (opt) => {
        policyOptions.push(opt);
      });
    });
    return policyOptions;
  }

  onOptionChange(policyOption, value) {
    const {quotation} = this.props;
    const {langMap, handleResetQuot, confirmAction} = this.context;
    const id = policyOption.id;
    if (policyOption.onChange) {
      if (!policyOption.onChange.to || policyOption.onChange.to === value || policyOption.onChange.to.indexOf(value) > -1) {
        if (policyOption.onChange.action === 'resetQuot') {
          confirmAction(window.getLocalizedText(langMap, 'quotation.warning.resetQuot'), () => {
            const params = policyOption.onChange.params || {};
            const {keepConfigs, keepPlans, keepFunds} = params;
            quotation.policyOptions[id] = value;
            handleResetQuot(keepConfigs, true, keepPlans, keepFunds);
          });
        }
      }
    } else {
      new Promise((resolve) => {
        if (policyOption.clearFunds === 'Y' && quotation.fund && quotation.fund.funds) {
          this.setState({
            warningMsg: window.getLocalizedText(langMap, policyOption.warningMsg || 'quotation.fund.warning.changePaymentMethod'),
            onDismissWarning: () => {
              quotation.fund = null;
              resolve();
            }
          });
        } else {
          resolve();
        }
      }).then(() => {
        return new Promise((resolve) => {
          if (policyOption.alertOnValue && policyOption.alertOnValue[value]) {
            this.setState({
              warningMsg: policyOption.alertOnValue[value],
              onDismissWarning: () => {
                resolve();
              }
            });
          } else {
            resolve();
          }
        });
      }).then(() => {
        quotation.policyOptions[id] = value;
        this.context.handleConfigChange();
      });
    }
  }

  getOptionField(policyOption, value) {
    const {quotation, quotationErrors} = this.props;
    const {lang, langMap, optionsMap} = this.context;
    const {focusOption} = this.state;
    const sign = window.getCurrencySign(quotation.compCode, quotation.ccy, optionsMap);
    let poId = policyOption.id;
    let title = window.getLocalText(lang, policyOption.title);
    let disabled = policyOption.disable === 'Y';
    let errorText = null;
    if (quotationErrors) {
      if (quotationErrors.mandatoryErrors && quotationErrors.mandatoryErrors[poId]) {
        title = <div>{title}<span className={styles.mandatoryStar}>&#42;</span></div>;
        errorText = window.getLocalizedText(langMap, 'form.err.required');
      } else if (quotationErrors.policyOptionErrors && quotationErrors.policyOptionErrors[poId]) {
        let error = quotationErrors.policyOptionErrors[poId];
        errorText = error.msg || window.getMsg(lang, langMap, error.code, error.msgPara);
      }
    }
    let hintText = focusOption === poId ? policyOption.hintMsg : null;
    if (policyOption.type === 'note' && policyOption.disable !== 'Y') {
      if (policyOption.title.en === 'title') {
        return (
        <div key={poId} style={{ margin: '0 10px', padding: 0, fontSize: 14 }}>{policyOption.note}</div>
      );
      } else if (policyOption.title.en === 'red') {
        return (
          <div key={poId} style={{ margin: '0', fontSize: 14, color: '#FF0000' }}>{policyOption.note}</div>
      )
      }
      return (
        <div key={poId} style={{ margin: '0', fontSize: 14 }}>{policyOption.note}</div>
      );
    }
    else if (policyOption.type === 'checkbox') {
      return (
        <Checkbox
          key={poId}
          disabled={disabled}
          label={title}
          checked={value}
          onCheck={(e, val) => {
            this.onOptionChange(policyOption, val);
          }}
        />
      );
    } else if (policyOption.type === 'datepicker') {
      return (
        <DatePicker
          key={poId}
          id={poId}
          disabled={disabled}
          value={value}
          onChange={(e, val) => {
            this.onOptionChange(policyOption, val);
          }}
          floatingLabelText={title}
        />
      );
    } else if (policyOption.type === 'picker') {
      if (policyOption.LeftAlignedPickerFieldTitle && policyOption.LeftAlignedPickerFieldTitle === 'Y'){
        return (
          <div style={{display: 'flex', minHeight: '0px'}}>
          <div style={{width: '50%',lineHeight: '58px',verticalAlign: 'middle'}}>{title}</div>
          <div style={{width: '25%'}}>
          <SelectField
            key={poId}
            id={poId}
            fullWidth
            disabled={disabled}
            errorText={errorText}
            hintText={policyOption.options.length ? 'Select' : '-'}
            value={value}
            underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
            onChange={(e, index, val) => {
              this.onOptionChange(policyOption, val);
            }}
            labelStyle={{ color: 'black' }}
          >
            {_.map(policyOption.options, (opt) => {
              return <MenuItem key={opt.value} value={opt.value} primaryText={opt.title[lang]} />;
            })}
          </SelectField>
          </div>
          </div>
        );
      }
      return (
        <SelectField
          key={poId}
          id={poId}
          fullWidth
          disabled={disabled}
          floatingLabelText={title}
          floatingLabelFixed
          floatingLabelStyle={{width:250}}
          errorText={errorText}
          hintText={policyOption.options.length ? 'Select' : '-'}
          value={value}
          underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
          onChange={(e, index, val) => {
            this.onOptionChange(policyOption, val);
          }}
          labelStyle={{ color: 'black' }}
        >
          {_.map(policyOption.options, (opt) => {
            return <MenuItem key={opt.value} value={opt.value} primaryText={opt.title[lang]} />;
          })}
        </SelectField>
      );
    } else if (policyOption.type === 'emptyBlock') {
      return (
        <div key={poId} id={poId} />
      );
    } else if (policyOption.type === 'text') {
      if (policyOption.subType === 'currency' || policyOption.subType === 'number') {
        return (
          <NumericField
            key={poId}
            id={poId}
            fullWidth
            disabled={disabled}
            sign={policyOption.subType === 'currency' ? sign : null}
            textTypePostfix={policyOption.textTypePostfix}
            floatingLabelText={title}
            floatingLabelFixed
            maxIntDigit={policyOption.maxLength ? policyOption.maxLength : 10}
            decimalPlace={policyOption.textDecimalPlace ? Number(policyOption.textDecimalPlace) : 0}
            errorText={hintText || errorText}
            errorStyle={hintText ? { color: '#3054ae' } : {}}
            value={value}
            onFocus={() => {
              this.setState({ focusOption: poId });
            }}
            onChange={(e, val) => {
              quotation.policyOptions[poId] = val;
            }}
            onBlur={() => {
              this.context.handleConfigChange();
              this.setState({ focusOption: null });
            }}
          />
        );
      } else {
        return (
          <TextField
            key={poId}
            id={poId}
            fullWidth
            disabled={disabled}
            errorText={policyOption.errorMsg}
            floatingLabelText={title}
            floatingLabelFixed
            value={value}
            underlineDisabledStyle={{ borderBottomColor: 'RGB(202,196,196)' }}
            onChange={(e, val) => {
              quotation.policyOptions[poId] = val;
            }}
            onBlur={() => {
              this.context.handleConfigChange();
            }}
          />
        );
      }
    } else if (policyOption.type === 'termandcondition') {
      return (
        <TermAndCondition
          key={poId}
          id={poId}
          disabled={disabled}
          title={title}
          policyOptions={quotation.policyOptions}
          baseProductId={quotation.baseProductId}
          policyOption={policyOption}
          checked={value}
          onCheck={(e, val) => {
            this.onOptionChange(policyOption, val);
          }}
        />
      );
    }
  }

  getOptionFields() {
    const {quotation} = this.props;
    const groups = [];
    const policyOptions = this._getPolicyOptions();
    _.each(policyOptions, (po) => {
      if (po.type === 'popupMsg') {
      } else if (groups.indexOf(po.groupID) === -1) {
        groups.push(po.groupID);
      }
    });
    let groupDivs = _.map(groups, (group) => {
      let options = _.filter(policyOptions, po => po.groupID === group);
      let haveTitle = false;
      let optionDivs = _.map(options, (po) => {
        let optionFieldWidth = '25%';
        if (haveTitle){
          optionFieldWidth = '20%';
        }
        if (po.type === 'note' ){
          if (po.title.en === 'title'){
            optionFieldWidth = 'auto';
            haveTitle = true;
          } else {
            optionFieldWidth = 'auto';
          }
        }
        else if (po.type === 'termandcondition'){
          optionFieldWidth = '100%';
        }
        else if (po.type === 'picker' && po.LeftAlignedPickerFieldTitle && po.LeftAlignedPickerFieldTitle === 'Y') {
          optionFieldWidth = '100%';
        } else if (po.type === 'picker' && po.widthInPercentage) {
          optionFieldWidth = po.widthInPercentage + '%';
        }
        return (
        <div key={po.id} style={{ width: optionFieldWidth }}>
          <div className={styles.policySelect}>
            {po && po.id && this.getOptionField(po, quotation.policyOptions[po.id])}
          </div>
        </div>
      )});
      return (
        <div
          key={'group-' + group}
          style={{
            display: 'flex', minHeight: '0px',
            flexWrap: 'wrap',
            alignItems: 'center'
          }}
        >
          {optionDivs}
        </div>
      );
    });
    return groupDivs;
  }

  _closeWarning() {
    const {onDismissWarning, init} = this.state;
    this.setState({
      warningMsg: null,
      onDismissWarning: null,
      init: init + 1
    }, () => {
      setTimeout(() => {
        onDismissWarning && onDismissWarning();
      }, 500); // for dismissing dialog animation
    });
  }

  render() {
    const {style, inputConfigs} = this.props;
    const {muiTheme} = this.context;
    var {warningMsg, init} = this.state;
    let optionFields = this.getOptionFields();
    // if (!optionFields.length) {
    //   return null;
    // }
    var initMsgs = [];
    var initTitleMsgs = [];
    _.each(inputConfigs, (inputConfig) => {
      _.each(inputConfig.policyOptions, (opt) => {
          if (opt.type === 'popupMsg') {
            initMsgs.push(opt.popupMsgValue);
            initTitleMsgs.push(opt.title.en);
          }
      });
    });

    if (init < initMsgs.length && initMsgs.length !== 0){
      warningMsg = initMsgs[init];
    }
    return (
      <div>
        {optionFields.length > 0 ? <Paper
          style={{
            boxShadow: muiTheme.palette.shadowColor + ' 1px 1px 4px',
            padding: '12px 0',
            backgroundColor: '#FAFAFA',
            ...style
          }}
        >
          {optionFields}
        </Paper> : null}
        <WarningDialog
          hasTitle={init < initMsgs.length ? false : true}
          title={initTitleMsgs[init]}
          open={!!warningMsg}
          msg={warningMsg}
          onClose={() => this._closeWarning()}
        />
      </div>
    );
  }

}
