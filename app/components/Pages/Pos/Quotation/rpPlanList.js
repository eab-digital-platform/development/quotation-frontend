module.exports = {
    rplists: `
    <html>

  <head>
      <style>
          .rptable {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
              color: black
          }

          .tableCellCenter {
              text-align: center !important;
          }

          .hideBorderTop {
            border-top: none !important;
          }

          .hideBorderBottom {
            border-bottom: none !important;
          }

          .rptd,
          .rpth {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
          }
      </style>
  </head>

  <body>
  <div style="text-align: left; color:black">Please note that this product must be bundled with a regular premium
  plan. The maximum single premium amount is based on the multiplier and annual
  premium of the regular premium plan.</div>
      <br />
      <table class="rptable">
          <tr>
              <th class="rpth tableCellCenter">Multiplier of Annual Premium</th>
              <th class="rpth">Selected AXA Plans Available for Bundling</th>
          </tr>
          <tr>
              <td rowspan=2 class="tableCellCenter rptd">10X</td>
              <td class="rptd hideBorderBottom">Term Protector/ Term Protector Prime</td>
          </tr>
          <tr>
              <td class="rptd hideBorderTop">AXA CritiCare For Her/ For Him</td>
          </tr>
          <tr>
              <td rowspan=4 class="tableCellCenter rptd">5X</td>
              <td class="rptd hideBorderBottom">AXA Life Treasure</td>
          </tr>
          <tr>
              <td class="rptd hideBorderBottom hideBorderTop">INSPIRE FlexiProtector/ INSPIRE FlexiSaver</td>
          </tr>
          <tr>
              <td class="rptd hideBorderBottom hideBorderTop">Pulsar</td>
          </tr>
          <tr>
              <td class="rptd hideBorderTop">AXA Wealth Accelerate</td>
          </tr>
          <tr>
              <td rowspan=2 class="tableCellCenter rptd">2X</td>
              <td class="rptd hideBorderBottom">AXA Retire Treasure II</td>
          </tr>
          <tr>
              <td class="rptd hideBorderTop">AXA Retire Happy Plus</td>
          </tr>
      </table>

  </body>

  </html>
  `
};
