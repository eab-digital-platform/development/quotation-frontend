import React, {PropTypes} from 'react';
import {List, ListItem, Checkbox} from 'material-ui';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import SaveDialog from '../../../Dialogs/SaveDialog';
import {getInclusiveRiders, getDisabledRiderList, removeDisabledRiders} from './utils';

export default class SelectRiderDialog extends EABComponent {

  static propTypes = {
    quotation: PropTypes.object,
    planDetails: PropTypes.object,
    riderList: PropTypes.array,
    onSave: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      open: false,
      selectedRiders: {}
    });
  }

  openDialog = () => {
    const {quotation, planDetails} = this.props;
    let selectedRiders = {};
    _.each(quotation.plans, (rider) => {
      selectedRiders[rider.covCode] = true;
    });
    _.each(planDetails, (plan) => {
      let covCode = plan.covCode;
      if (covCode !== quotation.baseProductCode && selectedRiders[covCode] === undefined) {
        selectedRiders[covCode] = false;
      }
    });
    this.setState({
      open: true,
      selectedRiders: selectedRiders
    });
  };

  closeDialog = () => {
    this.setState({
      open: false
    });
  };

  onUpdateRiderList = () => {
    const {onSave} = this.props;
    const {selectedRiders} = this.state;
    onSave && onSave(this._getSelectedCovCodes(selectedRiders));
  };

  _getSelectedCovCodes = (selectedRiders) => {
    const covCodes = [];
    _.each(selectedRiders, (selected, covCode) => {
      if (selected) {
        covCodes.push(covCode);
      }
    });
    return covCodes;
  };

  _getRiderListItems = () => {
    const {lang} = this.context;
    const {quotation, planDetails, riderList} = this.props;
    const {selectedRiders} = this.state;
    const bpDetail = planDetails[quotation.baseProductCode];
    const selectedCovCodes = this._getSelectedCovCodes(selectedRiders);
    const disabledRiders = getDisabledRiderList(selectedCovCodes, quotation, bpDetail, riderList);

    return _.map(riderList, (rider) => {
      let plan = planDetails[rider.covCode];
      let skipRider = false;
      if (rider.covCode) {
        skipRider = rider.hiddenInSelectRiderDialog === 'Y';
      }

      if (skipRider){
        return null;
      } else {
        let checkbox = (
          <Checkbox
            checked={!!selectedRiders[rider.covCode]}
            onCheck={(event, value) => {
              selectedRiders[rider.covCode] = value;
              const inclusiveRiders = getInclusiveRiders(rider.covCode, quotation, bpDetail);
              for (var sr in selectedRiders) {
                if (inclusiveRiders.indexOf(sr) >= 0) {
                  selectedRiders[sr] = value;
                }
              }
              let newSelectedCovCodes = this._getSelectedCovCodes(selectedRiders);
              newSelectedCovCodes = removeDisabledRiders(newSelectedCovCodes, quotation, bpDetail, riderList);
              const newSelectedRiders = {};
              _.each(newSelectedCovCodes, c => {
                newSelectedRiders[c] = true;
              });
              this.setState({
                selectedRiders: newSelectedRiders
              });
            }}
            disabled={rider.compulsory === 'Y' || disabledRiders.indexOf(rider.covCode) > -1}
          />
        );
        return (
          <ListItem key={rider.covCode} primaryText={window.getLocalText(lang, rider.covName || plan.covName)} rightToggle={checkbox} />
        );
      }
    });
  };

  render() {
    const {langMap} = this.context;
    return (
      <SaveDialog
        open={this.state.open}
        title={'Riders'}
        onSave={() => this.onUpdateRiderList()}
        onClose={() => this.closeDialog()}
      >
        <div style={{ padding: 24 }}>
          <List>
            {this._getRiderListItems()}
          </List>
        </div>
      </SaveDialog>
    );
  }

}
