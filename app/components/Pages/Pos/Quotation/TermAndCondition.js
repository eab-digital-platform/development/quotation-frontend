import React, { PropTypes } from 'react';
import {FlatButton, Dialog, Checkbox} from 'material-ui';
import * as _ from 'lodash';

import styles from '../../../Common.scss';
import EABComponent from '../../../Component';
import EmbedPDFViewer from '../../../CustomViews/EmbedPDFViewer';
import {viewedFile} from '../../../../actions/supportDocuments.js';

export default class TermAndCondition extends EABComponent {

  static propTypes = {
    style: PropTypes.object,
    policyOption: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      openPdfPreview: false,
      token:''
    });
  }

  isShowTermAndCondition(policyOption, policyOptions) {
    let {termAndConditionTriggerId,termAndConditionTriggerValue} = policyOption;
    let lookupValue = null;
    if (termAndConditionTriggerId && policyOptions[termAndConditionTriggerId]) {
      lookupValue = policyOptions[termAndConditionTriggerId];
    }
    if (lookupValue && termAndConditionTriggerValue) {
      return lookupValue === termAndConditionTriggerValue;
    }
    return false;
  }

  onLinkClick(e){
    const {baseProductId,policyOption} = this.props;
    let {termAndConditionAttId} = policyOption;
    if (baseProductId && termAndConditionAttId){
      viewedFile(this.context, baseProductId, termAndConditionAttId, false, true, (resp) => {
        if (resp.success === true) {
            this.setState({
                openPdfPreview: true,
                token: resp.token
            });
        } else {
            this.setState({openPdfPreview: true});
        }
      });
    }
  }

  getLink(linkText){
    if (!linkText) {
      return null;
    }
    return (
      <span style={{textDecoration: 'underline'}}>
        <a href='#' style={{color: 'blue'}} onClick={(e)=>{
            e.preventDefault();
            this.onLinkClick(e);
          }}>
          {linkText}
        </a>
      </span>
    );
  }

  getTitleAfterReplace(title,replaceText){
    let titleStart = '';
    let titleEnd = '';
    if (title){
      let titles = title.split(replaceText, 2);
      if (titles.length === 2){
        titleStart = titles[0];
        titleEnd = titles[1];
      }
    }
    return { titleStart,titleEnd };
  }

  render() {
    const {title, policyOption,policyOptions, disabled, checked} = this.props;
    let {termAndConditionReplacementText} = policyOption;
    let {titleStart,titleEnd} = this.getTitleAfterReplace(title,termAndConditionReplacementText);
    let {token} = this.state;

    let showTermAndCondition = this.isShowTermAndCondition(policyOption,policyOptions);
    if (showTermAndCondition) {
      return (
        <div style={{display: 'flex', minHeight: '0px'}}>
          <div>
            <Checkbox
              key={'term-and-condition-checkbox'}
              style={{width:'auto'}}
              disabled={disabled}
              checked={checked}
              onCheck={(e, val) => {
                if (this.props.onCheck){
                  this.props.onCheck(e, val);
                }
              }}
              iconStyle={{ fill: 'black' }}
            />
          </div>
          <div style={{width:'80%',lineHeight: '24px',verticalAlign: 'middle'}}>
            {titleStart}
            {this.getLink(termAndConditionReplacementText)}
            {titleEnd}
          </div>
          <Dialog
              repositionOnUpdate
              autoDetectWindowHeight
              className = {styles.FixWrongDialogPadding}
              open={this.state.openPdfPreview}
              contentStyle={{width:'100%', maxWidth:'none', transform: 'translate(0px, 64px)'}}
              actions={[
                  <FlatButton
                      label='Done'
                      primary
                      onTouchTap={()=>this.setState({openPdfPreview:false})}
                  />
              ]}
          >
              <div style={{ height:'calc(100vh - 204px)', display: 'flex', minHeight: '0px' }}>
                <EmbedPDFViewer url={window.genAttPath(token)} isHideToolBar/>
              </div>
          </Dialog>
        </div>
      );
    }
    return null;
  }

}
