import React from 'react';
import * as _ from 'lodash';

import EABComponent from '../../../Component';
import WarningDialog from '../../../Dialogs/WarningDialog';
import AppbarPage from '../../AppbarPage';
import FloatingPage from '../../FloatingPage';
import Quotation from './Quotation';
import ShieldQuotation from './ShieldQuotation';

import {closeQuotation, genProposal} from '../../../../actions/quotation';

export default class QuotationPage extends EABComponent {

  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      prevQuotation: null,
      quotation: null,
      planDetails: null,
      inputConfigs: null,
      quotationErrors: null,
      quotWarnings: null,
      availableFunds: null,
      availableInsureds: null,
      errorMsg: null,
      popMsg: null,
      onCloseMsg: null
    });
  }

  componentDidMount() {
    const {store} = this.context;
    this.unsubscribe = store.subscribe(() => {
      const quotation = store.getState().quotation;
      let newState = {};
      if (!window.isEqual(this.state.prevQuotation, quotation.quotation)) {
        newState.prevQuotation = _.cloneDeep(quotation.quotation);
        newState.quotation = _.cloneDeep(quotation.quotation);
      }
      if (!window.isEqual(this.state.planDetails, quotation.planDetails)) {
        newState.planDetails = quotation.planDetails;
      }
      if (!window.isEqual(this.state.inputConfigs, quotation.inputConfigs)) {
        newState.inputConfigs = quotation.inputConfigs || {};
      }
      if (!window.isEqual(this.state.quotationErrors, quotation.quotationErrors)) {
        newState.quotationErrors = quotation.quotationErrors;
      }
      if (!window.isEqual(this.state.quotWarnings, quotation.quotWarnings)) {
        newState.quotWarnings = quotation.quotWarnings;
      }
      if (!window.isEqual(this.state.availableFunds, quotation.availableFunds)) {
        newState.availableFunds = quotation.availableFunds;
      }
      if (!window.isEqual(this.state.availableInsureds, quotation.availableInsureds)) {
        newState.availableInsureds = quotation.availableInsureds;
      }
      if (!window.isEmpty(newState)) {
        this.setState(newState, () => {
          this.onChangePage();
        });
      }
    });
    this.init();
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
  }

  init() {
    const {store} = this.context;
    const quotation = store.getState().quotation;
    this.floatingPage.open();
    this.setState({
      prevQuotation: _.cloneDeep(quotation.quotation),
      quotation: _.cloneDeep(quotation.quotation),
      planDetails: quotation.planDetails,
      inputConfigs: quotation.inputConfigs,
      quotationErrors: quotation.quotationErrors,
      quotWarnings: quotation.quotWarnings
    }, () => {
      this.onChangePage();
    });
  }

  onChangePage() {
    const {langMap} = this.context;
    const {quotation, quotationErrors} = this.state;
    let hasFundError;
    if (quotation) {
      if(quotation.fund && quotation.fund.funds && quotation.fund.funds.length<=0){
        if(quotationErrors==undefined){
          // quotationErrors={
          //    mandatoryErrors:{fund:new Array()[0]={key:"fund",type:"mandatory"}}
          // };
          hasFundError=true;
        }
      }
      let hasError = !_.every(quotationErrors, (errors) => _.isEmpty(errors));
      this.appbarPage.initAppbar({
        menu: {
          icon: 'close',
          action: () => {
            closeQuotation(this.context);
          }
        },
        title: window.getLocalizedText(langMap, 'quotation.title'),
        itemsIndex: 0,
        items: [
          [{
            type: 'flatButton',
            title: window.getLocalizedText(langMap, 'quotation.btn.genProposal'),
            disabled: hasError || !quotation.premium || hasFundError,
            action: () => {
              genProposal(this.context, quotation, (error) => {
                this.setState({
                  errorMsg: error
                });
              }, (msg, callback) => {
                this.setState({
                  popMsg: msg,
                  onCloseMsg: callback
                });
              });
            }
          }]
        ]
      });
    }
  }

  _getContent() {
    const {quotation, planDetails, inputConfigs, quotationErrors, quotWarnings, availableFunds, availableInsureds} = this.state;
    if (quotation.quotType === 'SHIELD') {
      return (
        <ShieldQuotation
          quotation={quotation}
          planDetails={planDetails}
          inputConfigs={inputConfigs}
          quotationErrors={quotationErrors}
          quotWarnings={quotWarnings}
          availableInsureds={availableInsureds}
        />
      );
    } else {
      return (
        <Quotation
          quotation={quotation}
          planDetails={planDetails}
          inputConfigs={inputConfigs}
          quotationErrors={quotationErrors}
          quotWarnings={quotWarnings}
          availableFunds={availableFunds}
        />
      );
    }
  }

  render() {
    const {quotation, planDetails, errorMsg, popMsg, onCloseMsg} = this.state;
    return (
      <FloatingPage ref={ref=>{ this.floatingPage = ref; }}>
        <AppbarPage ref={ref=>{ this.appbarPage = ref; }}>
          {!window.isEmpty(quotation) && !window.isEmpty(planDetails) ? this._getContent() : null}
          <WarningDialog
            open={!!errorMsg}
            isError
            msg={errorMsg}
            onClose={() => this.setState({ errorMsg: null })}
            isAutoScrollBodyContent={true}
          />
          <WarningDialog
            open={!!popMsg}
            hasTitle={false}
            msg={popMsg}
            onClose={() => {
              this.setState({
                popMsg: null
              }, () => {
                onCloseMsg && onCloseMsg();
              });
            }}
          />
        </AppbarPage>
      </FloatingPage>
    );
  }
}
