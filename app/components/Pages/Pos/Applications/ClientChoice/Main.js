import React from 'react';
import EABComponent from '../../../../Component';

import DynColumn from '../../../../DynViews/DynColumn';

import * as ccActions from '../../../../../actions/clientChoice.js';
import * as appActions from '../../../../../actions/application.js';

import * as _v from '../../../../../utils/Validation';

import styles from '../../../../Common.scss'

class ClientChoiceMain extends EABComponent {
  constructor(props) {
    super(props);
    this.state = {
      template: {},
      values:{},
      changedValues: {},
      error: {},
      clientChoiceStep: 0
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  };

  componentWillUnmount(){
    this.unsubscribe();
  };

  componentDidUpdate(prevProps, prevState) {
    let {
      template, changedValues, clientChoiceStep
    } = this.state;

    if (isEmpty(prevState.template) && !isEmpty(template)) {
      this.init();
    }
    if (!isEmpty(prevState.template) && prevState.clientChoiceStep !== clientChoiceStep) {
      this.validate(changedValues, false);
    }
  }

  storeListener=()=>{
    let newState = {};
    let {
      store, validRefValues, langMap, optionsMap
    } = this.context;

    let {
      clientChoice
    } = store.getState();

    let {
      template, changedValues, values, clientChoiceStep
    } = this.state;

    if(!_.isEqual(values, clientChoice.values)) {
      newState.values = clientChoice.values;
      newState.changedValues = clientChoice.values;
    }

    if(!_.isEqual(template, clientChoice.template)) {
      newState.template = clientChoice.template;
    }

    if (clientChoiceStep != clientChoice.clientChoiceStep) {
      newState.clientChoiceStep = clientChoice.clientChoiceStep;
    }

    if (!isEmpty(newState)) {
      this.setState(newState);
    }
  }

  init=()=>{
    let {
      store
    } = this.context;

    let {
      clientChoice
    } = store.getState();

    let {
      changedValues
    } = this.state;

    let appBar = {
      title: this.getAppbarTitle(0),
      menu: {
        title: 'SAVE',
        action: ()=>{
          this.saveOnClick();
        }
      },
      items:[[{
        id: "next",
        type: "flatButton",
        title: {
          "en": "NEXT"
        },
        disabled: true
      }],[{
        id: "next",
        type: "flatButton",
        title: {
          "en": "NEXT"
        },
        action: ()=>{
          this.nextOnClick();
        }
      }],[{
        id: "done",
        type: "flatButton",
        title: {
          "en": "DONE"
        },
        action: ()=>{
          this.doneOnClick();
        }
      }]
      ]
    };

    let initIndex = 0;
    let initCompleted = -1;
    let initActive = 0;
    if (clientChoice.isRecommendationCompleted) {
      initCompleted += 1;
      initActive = initCompleted + 1;
    }
    if (clientChoice.isBudgetCompleted) {
      initCompleted += 1;
      initActive = initCompleted + 1;
    }
    if (clientChoice.isAcceptanceCompleted) {
      initCompleted += 1;
      initActive = initCompleted;
    }
    initIndex = clientChoice.clientChoiceStep;

    let stepper = {
      index: initIndex,
      completed: initCompleted,
      active: initActive,
      onGoStepComplete: this.onGoStepComplete.bind(this),
      onSaveChangeStep: this.onSaveChangeStep.bind(this)
    };

    this.validate(changedValues, true, appBar, stepper);
  }

  onSaveChangeStep=(nextIndex, callback)=>{
    let { changedValues } = this.state;
    let { stepperIndex, store } = this.context;
    let { clientChoice } = store.getState();
    let { error } = this.state;
    let completed = !_.at(error, `sections[${stepperIndex}].code`)[0];
    let allCompleted = clientChoice.isRecommendationCompleted && clientChoice.isBudgetCompleted && clientChoice.isAcceptanceCompleted;

    ccActions.updateClientChoiceCB(this.context, stepperIndex, changedValues, false, allCompleted || !(completed && nextIndex > stepperIndex), ()=>{
      if(_.isFunction(callback)){
        callback();
      }
    });
  }

  saveOnClick=()=>{
    let { stepperIndex } = this.context;
    this.onSaveChangeStep(stepperIndex, ()=>{
      this.closePage();
    })
  }

  nextOnClick=()=>{
    let { stepperIndex } = this.context;
    let nextIndex = stepperIndex + 1;
    this.context.goStep(nextIndex, true);

  }

  doneOnClick=()=>{
    let { changedValues } = this.state;
    let { stepperIndex } = this.context;
    ccActions.updateClientChoiceCB(this.context, stepperIndex, changedValues, true, false, ()=>{
      this.closePage();
    });
  }

  closePage=()=>{
    appActions.backToPosMain(this.context);
  }

  getAppbarTitle=(index)=>{
    let title = {
      "en": "Client Choice"
    };
    if (index == 0) {
      title.en = "My Financial Consultant's recommendations";
    } else if (index == 1) {
      title.en = "Budget";
    } else {
      title.en = "Client's Acceptance";
    }
    return title;
  }

  onGoStepComplete=(index)=>{
    let {error, template} = this.state;
    let {lastIndex} = this.getStepperPageIndex(error, index);
    let valid = !_.at(error, `sections[${index}].code`)[0];
    let title = this.getAppbarTitle(index);
    this.context.updateAppbar({title, itemsIndex: lastIndex===index? 2 : (valid? 1: 0)});
  }


  getStepperPageIndex=(error, currIndex)=>{
    let {template, clientChoiceStep} = this.state;
    let {items} = template;
    let len = items.length;
    let lastIndex = len - 1;
    let completed = -1;

    //lastIndex don't have input field so always no error
    for(let i=0; i<=clientChoiceStep; i++){
      let prevStepPass = i > 0 ? !_.get(error, `sections[${i - 1}].code`) : true;

      if(!_.get(error, `sections[${i}].code`) && prevStepPass){
        completed = i;
      }
    }
    return {completed, lastIndex};
  }

  validate=(changedValues, init, appbar={}, stepper={})=>{
    let {langMap, optionsMap} = this.context;
    let {template, index, values} = this.state;


    let error = {};
    _v.exec(template, optionsMap, langMap, changedValues, changedValues, changedValues, error);
    // changedValues.isCompleted = error.code?false:true;
    let currentIndex = init?stepper.index:this.context.stepperIndex;
    let {completed, lastIndex} = this.getStepperPageIndex(error, currentIndex);
    let valid = !_.at(error, `sections[${currentIndex}].code`)[0];

    let headers = [];
    _.forEach(template.items, (item, i)=>{
      let {title, disabled} = item;
      disabled = disabled || _.at(error, `sections[${i}].skip`)[0];
      headers.push({id: i, title, disabled});
    })

    let itemsIndex = {itemsIndex:(currentIndex == lastIndex? 2: (valid?1:0))};
    let title = {title: this.getAppbarTitle(currentIndex)};

    let initCompleted = init && stepper.completed > completed? stepper.completed: completed;
    let initActive = init && initCompleted == lastIndex? initCompleted: initCompleted + 1;

    let stepperChange = init? {completed: initCompleted, active: initActive}: {completed};

    this.context.updateAppbar(Object.assign(appbar, itemsIndex, title));
    this.context.updateStepper(Object.assign(stepper, {headers}, stepperChange));
    this.setState({changedValues, values, template, error});
  }

  getContent=()=>{
    let {stepperIndex} = this.context;
    let {template, changedValues, values, error} = this.state;

    if(!checkExist(template, 'items'))
      return;
    let field=template.items[stepperIndex];

    return (
      <div className={styles.PanelBackgroundColor}style={{display: 'flex', minHeight: '0px', flexGrow: 1, flexDirection: 'column', overflowY: 'auto'}}>
        <DynColumn
          template={field}
          changedValues={changedValues}
          error={error}
          values={values}
          validate={this.validate}
          customStyleClass={field.styleClass}
          customStyle={{ flex: 1, ...field.style}}
        />
      </div>
    )
  }

  render() {
    let content = this.getContent();
    return (
      <div style={{display: 'flex', minHeight: '0px', flexGrow: 1, flexDirection: 'column', height: '100%'}}>
        {content}
      </div>
    );
  }
}

export default ClientChoiceMain;
