import React from 'react';
import EABComponent from '../../../../Component';
import {Dialog, TextField, FlatButton, MenuItem, SelectField} from 'material-ui';
import Appbar from '../../../../CustomViews/AppBar';
import styles from '../../../../Common.scss';
import {updateOtherDocumentName} from '../../../../../actions/supportDocuments';
import * as _ from 'lodash';

class EditDocumentNameDialog extends EABComponent {

    constructor(props) {
        super(props);
        let documentInfo = props.documentInfo;
        let { sectionId, currentName, currentDocNameOption} = documentInfo;
        if (documentInfo) {
          if (sectionId === 'otherDoc') {
            if (!currentDocNameOption) {
              currentDocNameOption = 'Other';
            }
          }
        }
        let doneDisabled = true;
        this.state = Object.assign({}, this.state, {
            open: false,
            appbar: this.getAppBar(doneDisabled),
            currentName:currentName,
            currentDocNameOption:currentDocNameOption,
            docNameOption: currentDocNameOption ? currentDocNameOption : '',
            docName: currentName ? currentName : '',
            containsRegExp: false,
            docNameDuplicated: false
        });
    }

    componentWillReceiveProps(nextProps) {
      let documentInfo = nextProps.documentInfo;
      if (documentInfo) {
        let {
          sectionId,
          currentName,
          currentDocNameOption
        } = documentInfo;
        if (sectionId === 'otherDoc') {
          if (!currentDocNameOption) {
            currentDocNameOption = 'Other';
          }
          this.setState({
            currentName:currentName,
            currentDocNameOption:currentDocNameOption,
            docName: currentName,
            docNameOption: currentDocNameOption
          });
        }
      }
    }

    getAppBar = (doneDisabled) => {
        return ({
            menu:{
                type:'flatButton',
                title:{
                    'en':'Cancel'
                },
                action: ()=>{
                    this.closeDialog();
                }
            },
            title:'Edit Document Name',
            items:[
                [{
                    type:'flatButton',
                    title:{
                        'en': 'Done'
                    },
                    disabled: doneDisabled,
                    action: ()=>{
                        this.submit();
                    }
                }]
            ]
        });
    };
    updateAppBar = (doneDisabled) => {
        this.setState({
            appbar: this.getAppBar(doneDisabled)
        });
    }

    handleTextChange = (event)=>{
        if (event.target.value.length === 0) {
            this.updateAppBar(true);
            this.setState({
                containsRegExp:false,
                docName: event.target.value
            });
        } else {
            // Do alphanumberic checking
            let patt = /[^a-z0-9 ]/i;
            let containsRegExp = patt.test(event.target.value);
            if (containsRegExp) {
                this.updateAppBar(true);
                this.setState({
                    containsRegExp:true,
                    docName: event.target.value
                });
            } else {
                let value = event.target.value;
                if (value && typeof(value) === 'string' && value.trim() === '') {
                  this.updateAppBar(true);
                }
                else {
                  let {docNameOption,currentDocNameOption,currentName} = this.state;
                  let doneDisabled = this.checkIsDoneDisabled(docNameOption, value, currentDocNameOption, currentName);
                  this.updateAppBar(doneDisabled);
                }
                this.setState({
                    containsRegExp:false,
                    docName: event.target.value
                });
            }
        }
    }

    openDialog=(infoDic)=>{
        this.setState({
            open: true
        });
        this.updateAppBar(true);
    }

    closeDialog=()=>{
      let documentInfo = this.props.documentInfo;
      let { currentName, currentDocNameOption} = documentInfo;
      this.setState({
          open: false,
          containsRegExp: false,
          docName: currentName,
          docNameOption: currentDocNameOption
      });
    }

    submit = () =>{
        const {appId, tabId, documentId, rootValues} = this.props.documentInfo;
        let {docName,docNameOption} = this.state;

        updateOtherDocumentName(this.context, appId, tabId, documentId, docName,docNameOption, rootValues, (resp)=>{
            if (resp.duplicated) {
                this.setState({docNameDuplicated: true});
            }
            this.closeDialog();
        });
    }

    getDocOptionsView(options){
      let optionsView = [];
      _.each(options, (option) => {
        let optionId = 'otherDocEditDocPicker-' + option.title;
        optionsView.push(
          <MenuItem
            id = {optionId}
            key={option.title}
            value={option.title}
            primaryText={option.title}
          />
        );
      });
      return optionsView;
    }

    getPickerField(docNameOption,options){
      return <SelectField
        id="otherDocEditDocPicker"
        key="otherDocEditDocPicker"
        autoWidth
        value={docNameOption}
        onChange={(e, index, value) => this.onSelectionChange(value)}
        hintText='Select'
      >
        {
        (window.isMobile.apple.phone) ?
        <div style={{width: '100vw', overflowX: 'auto'}}>
        {this.getDocOptionsView(options)}
        </div>
        :
        this.getDocOptionsView(options)
        }
      </SelectField>
    }

    onSelectionChange(value){
      let {docNameOption,currentDocNameOption,currentName} = this.state;
      let doneDisabled = false;
      if (value) {
        if (docNameOption !== value) {
          if (value !== 'Other') {
            this.setState({
              docNameOption: value,
              docName: value
            });
            doneDisabled = this.checkIsDoneDisabled(value, value,currentDocNameOption,currentName);
          } else {
            this.setState({
              docNameOption: value,
              docName: ''
            });
            doneDisabled = this.checkIsDoneDisabled(value, '',currentDocNameOption,currentName);
          }
          this.updateAppBar(doneDisabled);
        }
      }
    }

    checkIsMandatory(docNameOption, docName) {
      if (typeof(docNameOption) === 'string' && typeof(docName) === 'string'){
        if (docNameOption === 'Other' && docName.trim() === '') {
          return true;
        }
      }
      return false;
    }

    showDocNameField(docNameOption){
      if (typeof(docNameOption) === 'string' && docNameOption === 'Other'){
        return true;
      }
      return false;
    }

    checkIsDoneDisabled(docNameOption, docName, currentDocNameOption, currentName) {
      if (currentDocNameOption && docNameOption) {
        if (currentDocNameOption === docNameOption && currentName === docName) {
          return true;
        }
        if (typeof (docNameOption) === 'string' && typeof (docName) === 'string') {
          if (docNameOption === 'Other' && docName.trim() === '') {
            return true;
          }
        }
      }
      return false;
    }

    render() {
        let {appbar ,open, containsRegExp,docName, docNameOption} = this.state;
        let {supportDocDetails, tabId} = this.props;

        let isMandatory = this.checkIsMandatory(docNameOption, docName);
        let isDocNameFieldShow = this.showDocNameField(docNameOption);
        let options = [];
        if (supportDocDetails && supportDocDetails.otherDocNameList){
          let otherDocNames = supportDocDetails.otherDocNameList;
          if (otherDocNames.ph && (tabId === 'policyForm' || tabId === 'proposer')) {
            options = otherDocNames.ph;
          } else if (otherDocNames.la && tabId === 'insured') {
            options = otherDocNames.la;
          } else {
            options = otherDocNames.la;
          }
        }

        return (
            <div>
                <Dialog
                    title="WARNING"
                    open={this.state.docNameDuplicated}
                    actions={[
                        <FlatButton
                            label='OK'
                            primary={true}
                            onTouchTap={()=>this.setState({docNameDuplicated: false})}
                        />
                    ]}>
                    There is already a document with the same name in this application. Please use another name.
                </Dialog>

                <Dialog titleClassName={styles.OtherDocDialogtitle} open={open} bodyStyle={{padding: '24px'}}
                    title={<div style={{padding:'0px'}}><Appbar ref={ref=>{this.appbar=ref}} showShadow={false} template={appbar} style={{background: '#FAFAFA', paddingLeft: '0px !important'}}/><div className={styles.Divider}/></div>}
                >
                    <div>
                    <div style={{display:'inline-block'}}>
                    {this.getPickerField(docNameOption,options)}</div>
                    {isDocNameFieldShow ?
                    <div style={{display:'inline-block',position:'absolute',paddingLeft:'24px'}}>
                    <TextField
                        id = 'otherDocEditDocNameText'
                        key = 'otherDocEditDocNameText'
                        defaultValue = {docName}
                        onChange = {this.handleTextChange}
                        errorText={isMandatory ? 'This field is required' : ''}
                        maxLength = '20'
                    />
                    </div> : null}
                    <div>
                    {containsRegExp ? <div style={{fontSize:'10px',color:'red'}}>Invalid letter</div> : null}
                    </div>
                    </div>

                </Dialog>
            </div>


        );
    }
}

export default EditDocumentNameDialog;
