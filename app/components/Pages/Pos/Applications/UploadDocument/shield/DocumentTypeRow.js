import React from 'react';
import * as _ from 'lodash';
import {Divider, IconButton} from 'material-ui';
import {getIcon} from '../../../../../Icons/index';
import EABComponent from '../../../../../Component';
import Dropzone from '../../../../../CustomViews/DropZone.jsx';
import FileUpload from '../../../../../CustomViews/FileUpload.js';
import OtherDocDialog from './OtherDocDialog';
import DocumentRow from './DocumentRow.js';
import styles from '../../../../../Common.scss'

class DocumentTypeRow extends EABComponent {
    constructor(props) {
        super(props);
    }

    // getFilesCount = (sectionId, values) => {
    //     let count = 0;
    //     let docTypeValues = [];

    //     if (sectionId != "sysDocs") {
    //         if (sectionId == "otherDoc") {
    //             if (values.otherDoc) {
    //                 docTypeValues = values.otherDoc.values;
    //             }
    //         } else {
    //             docTypeValues = values[sectionId];
    //         }
    //         _.forEach(docTypeValues, docValues=>{
    //             count = count + docValues.length;
    //         });
    //     }

    //     return count;
    // }

    addDoc = () => {
        this.otherDocDialog.openDialog();
    }

    deleteDoc = () => {

    }

    genItems(applicationId, section, values, tabId, appStatus,
        viewedList, reviewDisabled, isSupervisorChannel,
        isReadOnly, defaultDocNameList, supportDocDetails, pendingSubmitList){

        //TO-DO: remove input param and put the variable under following
        let {tokensMap} = this.props;
        var {muiTheme} = this.context;
        var changedSection = [];
        // let filesCount = this.getFilesCount(section.id, values);
        // const maxFilesCount = 8;

        if (section.title && section.title !== '') {
            changedSection.push(
                <div style={{
                    height: '48px',
                    lineHeight:'48px',
                    fontSize: '16px',
                    fontWeight: '500',
                    // NOTE: lucia color from 103184 to 00008f
                    color: '#00008f',
                    padding: '0 24px',
                    position: 'relative'
                }}>
                    <div style={{display:'flex', flexGrow:1}}>{section.title}</div>
                    {section.hasSubLevel === 'true' && section.disabled !== 'true' ?
                       <div style={{
                            position: 'absolute',
                            top: 0,
                            left: '24px',
                            width: 'calc(100% - 48px)',
                            textAlign: 'right'
                        }}>
                            <IconButton
                                onTouchTap={this.addDoc}
                                disabled={isReadOnly}
                            >
                                {getIcon('add', muiTheme.palette.labelColor)}
                            </IconButton>
                        </div>
                        :null
                    }
                </div>
            );
        }

        // if (filesCount >= maxFilesCount) {
        //     changedSection.push(
        //         <div style={{fontSize:'10px',color:'red',padding:'0px 0px 0px 24px'}}>
        //             {"Maximum upload " + maxFilesCount + " files"}
        //         </div>
        //     );
        // }

        if (section && section.id == 'otherDoc'){
            if (values.otherDoc && values.otherDoc.template.length>0) {
                for (var i in values.otherDoc.template){
                    changedSection.push(
                        <DocumentRow
                            key={values.otherDoc.template[i].id}
                            sectionId = {'otherDoc'}
                            template={values.otherDoc.template[i]}
                            disabled={section.disabled}
                            applicationId={applicationId}
                            values={values}
                            rootValues={this.props.rootValues}
                            dragDropZone={this.props.dragDropZone}
                            tabId={tabId}
                            appStatus={appStatus}
                            viewedList={viewedList}
                            reviewDisabled={reviewDisabled}
                            isSupervisorChannel={isSupervisorChannel}
                            isReadOnly={isReadOnly}
                            defaultDocNameList={defaultDocNameList}
                            supportDocDetails={supportDocDetails}
                            pendingSubmitList={pendingSubmitList}
                            tokensMap={tokensMap}
                        />
                    );
                }
            } else {
                changedSection.push(
                    <div style={{padding:'0px 24px'}}>No record</div>
                );
            }
        } else {
            // For Mandatory and Optional Documents, if no document then not display
            for (var i in section.items) {
                changedSection.push(
                    <DocumentRow
                        key={section.items[i].id}
                        sectionId = {section.id}
                        template={section.items[i]}
                        disabled={section.disabled}
                        applicationId={applicationId}
                        values={values}
                        rootValues={this.props.rootValues}
                        dragDropZone={this.props.dragDropZone}
                        tabId={tabId}
                        appStatus={appStatus}
                        viewedList={viewedList}
                        reviewDisabled={reviewDisabled}
                        isSupervisorChannel={isSupervisorChannel}
                        isReadOnly={isReadOnly}
                        defaultDocNameList={defaultDocNameList}
                        supportDocDetails={supportDocDetails}
                        pendingSubmitList={pendingSubmitList}
                        tokensMap={tokensMap}
                    />
                );
            }
        }
        return changedSection;
    }

    render(){
        var {template,values,applicationId, tabId, appStatus, viewedList,
            reviewDisabled, isSupervisorChannel, isReadOnly,
            defaultDocNameList, supportDocDetails,
            pendingSubmitList} = this.props;

        return(
            <div className={styles.Block}>
                <OtherDocDialog
                    ref={ref=>{this.otherDocDialog=ref}}
                    tabId={tabId}
                    values={values}
                    rootValues={this.props.rootValues}
                    appId={applicationId}
                    defaultDocNameList={defaultDocNameList}
                    supportDocDetails={supportDocDetails}
                />
                {this.genItems(applicationId, template, values, tabId, appStatus,
                    viewedList, reviewDisabled, isSupervisorChannel, isReadOnly,
                    defaultDocNameList, supportDocDetails, pendingSubmitList)}
            </div>
        );
    }
}
export default DocumentTypeRow;
