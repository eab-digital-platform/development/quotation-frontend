import React from 'react';
import * as _ from 'lodash';
import {Divider, Tabs, Tab, Dialog, FlatButton} from 'material-ui';
import SwipeableViews from 'react-swipeable-views';
import styles from '../../../../../Common.scss';
import EABComponent from '../../../../../Component';
import DocumentTypeRow from './DocumentTypeRow.js';
import {closeSuppDocs, submitSuppDocsFiles, UPDATE_PENDING_SUBMIT_LIST} from '../../../../../../actions/supportDocuments.js';
import {searchApprovalCaseByAppId, additionalDocNotification} from '../../../../../../actions/approval';
import {getIcon} from '../../../../../Icons';

import _assign from 'lodash/fp/assign';
import _get from 'lodash/fp/get';
import _isNil from 'lodash/fp/isNil';
import _isUndefined from 'lodash/fp/isUndefined';

class UploadDocument extends EABComponent {
    constructor(props) {
        super(props);
        this.inited = false;
        this.state = Object.assign({}, this.state,{
          tabSelected: 0,
          template:{},
          values:{},
          suppDocsAppView:{},
          dragDropZone:{
            isShow:false,
            valueLocation:{}
          },
          appStatus:'APPLYING',
          viewedList: {},
          isViewedListChanged: false,
          userChannel: 'agent',
          isSupervisorChannel: false,
          submitDoneDialog: false,
          isReadOnly: false,
          defaultDocNameList: [],
          supportDocDetails: {},
          pendingSubmitList: [],
          tokensMap: {}
        });
    }

    componentDidMount() {
      this.context.updateAppbar(
        Object.assign(this.getAppBar())
      );
      this.unsubscribe = this.context.store.subscribe(this.storeListener);
      this.storeListener();
    }

    getAppBar = () => {
      return (
        {
          title: 'Supporting Docs',
          menu: {
            icon: 'close',
            action: ()=>{
              this.closePage();
            }
          },
          titleStyle: (window.isMobile.apple.phone) ? {width: '100%', fontWeight: 500, color: 'black'} : undefined,
          itemsIndex: 0,
          items:[
            [],
            [
              {
                id: 'submit',
                type: 'flatButton',
                title: {
                  'en':'UPLOAD'
                },
                disabled: true
              }
            ],
            [
              {
                id: 'submit',
                type: 'flatButton',
                title: {
                  'en':'UPLOAD'
                },
                action: () => {
                  this.submitBtnOnClick();
                }
              }
            ],
          ]
        }
      );
    }

    storeListener=()=>{
      let newState = {};
      let {store} = this.context;
      let {supportDocuments} = store.getState();
      let {template, values, suppDocsAppView, dragDropZone, appStatus,
        viewedList, isViewedListChanged, userChannel, isReadOnly,
        defaultDocNameList, supportDocDetails,
        pendingSubmitList, tokensMap} = this.state;
      if (!_.isEqual(template, supportDocuments.template)) {
        newState.template = supportDocuments.template;
      }
      if (!_.isEqual(values, supportDocuments.values)) {
        newState.values = supportDocuments.values;
      }
      if (!_.isEqual(suppDocsAppView, supportDocuments.suppDocsAppView)) {
        newState.suppDocsAppView = supportDocuments.suppDocsAppView;
      }
      if (!_.isEqual(dragDropZone, supportDocuments.dragDropZone)) {
        newState.dragDropZone = supportDocuments.dragDropZone;
      }
      if (!_.isEqual(isReadOnly, supportDocuments.isReadOnly)) {
        newState.isReadOnly = supportDocuments.isReadOnly;
      }
      if (!_.isEqual(appStatus, supportDocuments.appStatus)) {
        newState.appStatus = supportDocuments.appStatus;
        appStatus = supportDocuments.appStatus;
      }
      if (!_.isEqual(viewedList, supportDocuments.viewedList)) {
        if (!_.isEmpty(viewedList)) {
          isViewedListChanged = true;
          newState.isViewedListChanged = true;
        }
        newState.viewedList = supportDocuments.viewedList;
      }
      if (!_.isEqual(userChannel, supportDocuments.userChannel)) {
        newState.userChannel = supportDocuments.userChannel;
        if (supportDocuments.userChannel === 'supervisor') {
          newState.isSupervisorChannel = true;
        }
      }
      if (!_.isEqual(defaultDocNameList, supportDocuments.defaultDocNameList)) {
        newState.defaultDocNameList = supportDocuments.defaultDocNameList;
      }
      if (!_.isEqual(supportDocDetails, supportDocuments.supportDocDetails)) {
        newState.supportDocDetails = supportDocuments.supportDocDetails;
      }
      if (!_.isEqual(pendingSubmitList, supportDocuments.pendingSubmitList)) {
        newState.pendingSubmitList = supportDocuments.pendingSubmitList;
      }
      if (!_.isEqual(tokensMap, supportDocuments.tokensMap)) {
        newState.tokensMap = supportDocuments.tokensMap;
      }

      if (!window.isEmpty(newState)) {
        this.context.updateAppbar({
          itemsIndex: appStatus === 'SUBMITTED' ? isViewedListChanged ? 2 : 1 : 0,
          menu: supportDocuments.isReadOnly ?
            {
              icon: 'close',
              action: ()=>{
                this.closePage();
              }
            } :
            {
              title: {
                'en':'Done'
              },
              action: ()=>{
                this.closePage();
              }
            }
        });
        this.setState(newState);
      }
    }

    tabViewHandleChange = (value) => {
        this.setState({tabSelected: value});
    };

    closePage = () => {
      let {template, values, suppDocsAppView} = this.state;
      closeSuppDocs(this.context, template, values, suppDocsAppView.id);
    }

    submitBtnOnClick = () => {
      let {store} = this.context;
      let {values, suppDocsAppView, viewedList, isSupervisorChannel} = this.state;
      submitSuppDocsFiles(this.context, suppDocsAppView.id, values, viewedList, isSupervisorChannel, (resp) => {
        // Only send the notifications when upload additional documents progress completed
        const approvalCase = _get('approval.approvalCase', this.context.store.getState());
        if (_isNil(approvalCase) || _isUndefined(approvalCase)) {
          const appDocId = _get('supportDocuments.suppDocsAppView.id', this.context.store.getState());
          return new Promise((resolve) => {
            searchApprovalCaseByAppId(this.context, appDocId, (_approvalCase) => resolve(_approvalCase));
          }).then((_approvalCase) => {
            return new Promise(resolve =>{
              additionalDocNotification(this.context, _approvalCase.policyNumber, resp.sendHealthDeclarationNoti, () => {
                resolve();
              });
            });
          });
        } else {
           return new Promise(resolve =>{
             additionalDocNotification(this.context, approvalCase.approvalCaseId, resp.sendHealthDeclarationNoti, () => {
              resolve();
            });
          });
        }
      });
      this.context.updateAppbar({itemsIndex: 1});
      this.setState({
        isViewedListChanged: false,
        submitDoneDialog: true
      });
      store.dispatch({
        type: UPDATE_PENDING_SUBMIT_LIST,
        pendingSubmitList: []
      });
    }

    genItems = (tabId, template, values, suppDocsAppView, appStatus, viewedList,
      isViewedListChanged, isSupervisorChannel, isReadOnly,
      defaultDocNameList, supportDocDetails,
      pendingSubmitList)=>{

      //TO-DO: remove input param and put the variable under following
      let {tokensMap} = this.state;

      if (template && values) {
        let tabValues = values[tabId];
        // let tabTemplate = template[tabId];
        let tabTemplate = _.get(template, tabId + '.tabContent');
        var DocumentTypeRowArray = [];
        if (tabTemplate && tabValues) {
          for (var i in tabTemplate) {
            if (tabTemplate[i].type === 'section'){
              DocumentTypeRowArray.push(
                <div>
                  <DocumentTypeRow
                    tabId={tabId}
                    template={tabTemplate[i]}
                    values={tabValues}
                    rootValues={values}
                    applicationId={suppDocsAppView.id}
                    dragDropZone={this.state.dragDropZone}
                    appStatus={appStatus}
                    viewedList={viewedList}
                    reviewDisabled={appStatus === 'SUBMITTED' && isViewedListChanged}
                    isSupervisorChannel={isSupervisorChannel}
                    isReadOnly={isReadOnly}
                    defaultDocNameList={defaultDocNameList}
                    supportDocDetails={supportDocDetails}
                    pendingSubmitList={pendingSubmitList}
                    tokensMap={tokensMap}
                  />
                  <Divider/>
                </div>
              );
            }
          }
        }
        return DocumentTypeRowArray;
      }
    }

    validateMandDocs = (template, values) => {
      let result = {};
      _.forEach(template, (tabTemplate, tabId)=>{
        let tabContent = _.get(tabTemplate, 'tabContent');
        let mandDocsTemplate = _.get(
          _.find(tabContent, (section)=>{
            return section.id === 'mandDocs';
          }),
          'items');
        let mandDocsValues = _.get(values, tabId + '.mandDocs', {});

        _.forEach(mandDocsTemplate, (fileTemplate)=>{
          if (_.isEmpty(_.get(mandDocsValues, _.get(fileTemplate, 'id')))) {
            result[tabId] = false;
            return false;
          }
        });
      });
      return result;
    }

    sortByTabSequence = (objTemplate) => {
      let arr = [];
      _.forEach(objTemplate, (objTab, keyTab) => {
        objTab.tabCid = keyTab;
        arr.push(objTab);
      });
      arr.sort((a , b) => a.tabSequence > b.tabSequence);
      return arr;
    };

    tabsView = () => {
      let {template, values} = this.state;
      var {muiTheme} = this.context;
      let tabStyle, personTabStyle, inactiveTabStyle;
      let tabsViewArr = [];
      let count = 0;
      let mandDocsCompleted = this.validateMandDocs(template, values);
      let copyTemplate = _.cloneDeep(template);

      let tabsTitleArr = [];
      _.forEach(copyTemplate, (objTab, keyTab) => {
        tabsTitleArr.push(_.get(objTab,'tabName', ''));
      });
      let calcTabsPixel = window.getTabWidthByTabsTitleArray(tabsTitleArr);
      let tabsMinHeight = window.getTabsMinHeightByWidth(calcTabsPixel * tabsTitleArr.length);
      tabStyle = {color: muiTheme.baseTheme.palette.primary2Color, textTransform: 'none', width: calcTabsPixel + 'px'};
      personTabStyle = {color: muiTheme.baseTheme.palette.primary2Color, textTransform: 'uppercase', width: calcTabsPixel + 'px'};
      inactiveTabStyle = {fontWeight: 400, width: calcTabsPixel + 'px'};

      if (copyTemplate.policyForm) {
        tabsViewArr.push(
          <Tab
            label= {
              <div style={{display: 'flex', minHeight: '0px'}}>
                <div style={{padding: '5px'}}>Policy Forms</div>
                {_.has(mandDocsCompleted, 'policyForm') && !mandDocsCompleted.policyForm ? <div>{getIcon('warning', muiTheme.palette.warningColor)}</div> : null}
              </div>
            }
            style={tabStyle}
            value={0}
          />
        );
        delete copyTemplate.policyForm;
        count++;
      }

      let arrCopyTemplate = this.sortByTabSequence(copyTemplate);
      _.forEach(arrCopyTemplate, (objTab) => {
        tabsViewArr.push(
          <Tab
            label={
              <div style={{display: 'flex', minHeight: '0px'}}>
                <div style={{padding: '5px'}}>{objTab.tabName}</div>
                {_.has(mandDocsCompleted, objTab.tabCid) && !mandDocsCompleted[objTab.tabCid] ? <div>{getIcon('warning', muiTheme.palette.warningColor)}</div> : null}
              </div>
            }
            style={this.state.tabSelected === count ? personTabStyle : _assign(inactiveTabStyle, personTabStyle)}
            value={count}
          />
        );
        count++;
      });

      /*
      _.forEach(copyTemplate, (objTab, keyTab) => {
        tabsViewArr.push(
          <Tab
            label={
              <div style={{display: 'flex', minHeight: '0px'}}>
                <div style={{padding: '5px'}}>{objTab.tabName}</div>
                {_.has(mandDocsCompleted, keyTab) && !mandDocsCompleted[keyTab] ? <div>{getIcon('warning', muiTheme.palette.warningColor)}</div> : null}
              </div>
            }
            style={this.state.tabSelected === count ? personTabStyle : _assign(inactiveTabStyle, personTabStyle)}
            value={count}
          />
        );
        count++;
      });
      */

      return (
        <div style={{flex: '1 0 auto', display: 'flex', minHeight: '0px', minHeight: tabsMinHeight + 'px', overflowX: 'auto'}}>
          <Tabs
            onChange={this.tabViewHandleChange}
            value={this.state.tabSelected}
            style={{ minWidth: `calc(${calcTabsPixel}px * ${tabsTitleArr.length})`, backgroundColor: '#fff'}}
          >
              {tabsViewArr}
          </Tabs>
        </div>
      );
    }

    supportDocView = () => {
      let {template, values, suppDocsAppView, appStatus, viewedList, isViewedListChanged,
        isSupervisorChannel, isReadOnly, defaultDocNameList, supportDocDetails, pendingSubmitList} = this.state;
      let supportDocViewArr = [];
      let copyTemplate = _.cloneDeep(template);

      if (copyTemplate.policyForm) {
        supportDocViewArr.push(
          <div className={`${styles.SwipeableContent} ${styles.SupportDocContent}`}>
            <div>{this.genItems('policyForm', template, values, suppDocsAppView, appStatus,
              viewedList, isViewedListChanged, isSupervisorChannel, isReadOnly, defaultDocNameList, supportDocDetails, pendingSubmitList)}
            </div>
          </div>
        );
        delete copyTemplate.policyForm;
      }

      let arrCopyTemplate = this.sortByTabSequence(copyTemplate);
      _.forEach(arrCopyTemplate, (objTab) => {
        supportDocViewArr.push(
          <div className={`${styles.SwipeableContent} ${styles.SupportDocContent}`}>
            <div>{this.genItems(objTab.tabCid, template, values, suppDocsAppView, appStatus,
              viewedList, isViewedListChanged, isSupervisorChannel, isReadOnly, defaultDocNameList, supportDocDetails, pendingSubmitList)}
            </div>
          </div>
        );
      });

      /*
      _.forEach(copyTemplate, (objTab, keyTab) => {
        supportDocViewArr.push(
          <div className={`${styles.SwipeableContent} ${styles.SupportDocContent}`}>
            <div>{this.genItems(keyTab, template, values, suppDocsAppView, appStatus,
              viewedList, isViewedListChanged, isSupervisorChannel, isReadOnly, defaultDocNameList, pendingSubmitList)}
            </div>
          </div>
        );
      });
      */

      return (
        <SwipeableViews index={this.state.tabSelected} onChangeIndex={this.tabViewHandleChange} containerStyle={{ height: 'calc(100vh - 102px)', WebkitOverflowScrolling: 'touch'}} style={{ overflowY: 'hidden' }}>
          {supportDocViewArr}
        </SwipeableViews>
      );
    }

    dialogView = () => {
      let {store} = this.context;
      return (
        <Dialog open={this.state.submitDoneDialog}
                title={'NOTICE'}
                actions={
                    [<FlatButton
                        label='OK'
                        primary={true}
                        onTouchTap={()=>{
                            this.setState({
                              submitDoneDialog: false,
                              pendingSubmitList: []
                            });
                            store.dispatch({
                              type: UPDATE_PENDING_SUBMIT_LIST,
                              pendingSubmitList: []
                            });
                        }}
                    />]
                }
        >
                Files have been submitted.
        </Dialog>
      );
    }

    render() {
      return (
        <div style={{ display: 'flex', minHeight: '0px', flexDirection: 'column' }}>
          {this.dialogView()}
          {this.tabsView()}
          <div className={styles.Divider}/>
          {this.supportDocView()}
        </div>
      );
  }
}
export default UploadDocument;
