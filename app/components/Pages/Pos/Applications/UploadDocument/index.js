import React, { Component, PropTypes } from 'react';
import mui from 'material-ui';

import AppbarPage from '../../../AppbarPage';
import UploadDocument from './UploadDocument';
import ShieldUploadDocument from './shield/UploadDocument';
import EABComponent from '../../../../Component';
import styles from '../../../../Common.scss';
import Modal from '../../../../Modal';

export default class index extends EABComponent {
  componentDidMount() {

  }

  render() {
    return (
      <Modal>
        <div className={styles.FloatingPage}>
          <AppbarPage>
            {this.props.isShield ? <ShieldUploadDocument /> : <UploadDocument ref="uploadDocument"/>}
          </AppbarPage>
        </div>
      </Modal>
    );
  }
}
