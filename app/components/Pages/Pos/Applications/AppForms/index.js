import React from 'react';
import EABComponent from '../../../../Component';

import AppbarPage from '../../../AppbarPage';
import StepperPage from '../../../StepperPage';
import Main from './Main';
import ShieldMain from './ShieldMain';

import styles from '../../../../Common.scss';

class AppForm extends EABComponent {
  render() {
    return (
        <AppbarPage>
          <StepperPage>
          {
            this.props.isShield ? <ShieldMain /> : <Main ref={(ref) => { this.main = ref; }} />
          }
          </StepperPage>
        </AppbarPage>
    );
  }
}

export default AppForm;
