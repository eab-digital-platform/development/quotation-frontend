import React, { Component, PropTypes } from 'react';
import SignatureMain from './Main';
import EABComponent from '../../../../Component';

class SignatureIndex extends EABComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <SignatureMain ref='signMain' updateAppBarTitle={this.props.updateAppBarTitle}/>
    );
  }
}

export default SignatureIndex;
