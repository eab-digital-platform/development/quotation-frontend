import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import SignatureTab from './Tab';
import SignDocViewer from '../../../../CustomViews/SignDocViewer';
import PDFViewer from '../../../../CustomViews/PDFViewer';
import EABComponent from '../../../../Component';
import SystemConstants from '../../../../../constants/SystemConstants';
import {getIcon} from '../../../../Icons/index';

import {goSupportDocuments} from '../../../../../actions/supportDocuments.js';

import * as actions from '../../../../../actions/signature';
import * as appActions from '../../../../../actions/application';
import * as cActions from '../../../../../actions/client';

import * as _ from 'lodash';

const tabIndex = { tabFNReport: 0, tabProposal: 1, tabAppForm: 2, tabCoverPage: 3 };
const A4size = { width:1731, height:2192 };

const CROSSAGE_STATUS = {
  WILL_CROSSAGE: 100,
  CROSSED_AGE_SIGNED: 200,
  CROSSED_AGE_NOTSIGNED: 300
};

class SignatureMain extends EABComponent {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this.state, {
      selectedTab:-1,
      isSigned:[],
      cid:'',
      showErrorMsg:false,
      showCrossAgeMsg: false,
      isMandDocsAllUploaded: false
    });
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    let {muiTheme} = this.context;
    let {
      appForm
    } = this.context.store.getState();

    this.props.updateAppBarTitle(SystemConstants.EAPP.STEP.SIGNATURE);
  };

  componentWillMount() {

    let {
      store
    } = this.context;

    let {
      appForm
    } = store.getState();

    let docId = appForm.application.id;

    actions.initSign(this.context, docId, (crossAgeResp)=>{
      this.handleCrossAgeCB(crossAgeResp);
    });
  }

  componentWillUnmount(){
    this.unsubscribe();
    actions.closeSignaturePage(this.context);
  }

  storeListener=()=>{
    let newState = {};
    let {
      store
    } = this.context;

    let {
      appForm,
      signature
    } = store.getState();

    let {
      selectedTab, isMandDocsAllUploaded
    } = this.state;

    // mandDocsAllUploadedFlag is used to quick fix.
    let mandDocsAllUploadedFlag = false;
    let isAllSigned = this.enableNext(signature.isSigned);
    let nextSelectedTab = (signature.signingTabIdx < 0 ? (selectedTab === signature.tabCount - 1? selectedTab: 0): signature.signingTabIdx);

    if (!isEqual(this.state.cid, signature.cid)) {
      newState.cid = signature.cid;
    }

    if (!isEqual(isMandDocsAllUploaded, signature.isMandDocsAllUploaded)) {
      newState.isMandDocsAllUploaded = signature.isMandDocsAllUploaded;
      mandDocsAllUploadedFlag = true;
      this.context.updateAppbar({itemsIndex: this.getAppBarItemsIndex(isAllSigned, signature.isMandDocsAllUploaded)});
    }

    if(!isEqual(this.state.isSigned, signature.isSigned)){
      newState.isSigned = signature.isSigned;
      newState.selectedTab = nextSelectedTab;

      this.context.updateAppbar({itemsIndex: this.getAppBarItemsIndex(isAllSigned, (mandDocsAllUploadedFlag && signature.isMandDocsAllUploaded || isMandDocsAllUploaded))});
      if (isAllSigned && signature.appStep == this.context.stepperIndex) {
        //Don't update onSave / onSaveChangeStep in updateStepper, handled in AppForm/Main
        this.context.updateStepper({
          completed: appForm.application.appStep > this.context.stepperIndex? appForm.application.appStep: this.context.stepperIndex,
          active: appForm.application.appStep > this.context.stepperIndex? appForm.application.appStep + 1: this.context.stepperIndex + 1
        });
      }
    }

    if (this.state.selectedTab < 0) {
      newState.selectedTab = nextSelectedTab;
    }

    newState.showErrorMsg = (signature.errorMsg !== '');

    if (!isEmpty(newState)) {
      this.setState(newState);
    }
  };

  getAppBarItemsIndex=(completed, isMandDocsAllUploaded)=>{
    let itemsIndex = -1;

    //See the representation of itemsIndex in ../../AppForm/Main getAppBarItems()
    if (completed) {
      itemsIndex = isMandDocsAllUploaded ? 7 : 5;
    } else {
      itemsIndex = isMandDocsAllUploaded ? 6 : 4;
    }
    return itemsIndex;
  }

  enableNext=(isSigned)=> {
    return isSigned.length > 0 && isSigned.indexOf(false) < 0;
  }

  nextOnClick=()=>{
    this.context.goStep(this.context.stepperIndex+1, true);
  }

  goSuppDocs=(appId)=>{
    goSupportDocuments(this.context, appId);
  }

  handleHideErrorMsg = () => {
    actions.clearErrorMsg(this.context);
    this.setState({showErrorMsg:false});
  }

  handleChangeTab = (selectedTabId) => {
    this.setState({selectedTab: selectedTabId});
  };

  handleSign = (resultJSON, tabIdx) => {
    actions.handleSign(this.context, resultJSON, tabIdx);
  };

  updateBackDateTrue = () => {
    let {store} = this.context;
    let {appForm} = store.getState();
    actions.updateBIBackDateTrue(this.context, appForm.application.id);
    this.setState({showCrossAgeMsg: false});
  }

  invalidateCase = () => {
    let {store} = this.context;
    let {appForm} = store.getState();
    actions.invalidateApplication(this.context, appForm.application.id);
    this.setState({showCrossAgeMsg: false});
  }

  handleCrossAgeCB = (resp) => {
    if (resp.success) {
      if (resp.insuredStatus === CROSSAGE_STATUS.WILL_CROSSAGE || resp.proposerStatus === CROSSAGE_STATUS.WILL_CROSSAGE
        || resp.insuredStatus === CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED || resp.proposerStatus === CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED){
        this.setState({
          showCrossAgeMsg : true,
          crossAgeResp: resp
        });
      }
    }
  };

  crossAgeDialog = () => {
    let {showCrossAgeMsg} = this.state;
    if (!showCrossAgeMsg) {
      return null;
    }

    const ageWillCrossText = 'age will change in less than 14 days. This may result in additional requirements. Please consider to opt for backdating option (if applicable) or regenerate the Illustration once age has changed.';
    const crossedAgeText = 'age has changed. Please consider to opt for backdating option (if applicable) or regenerate the Illustration.';

    let {insuredStatus, proposerStatus, allowBackdate} = this.state.crossAgeResp;
    let isCrossed = false;
    let text = '';
    let buttons = [];

    if (insuredStatus === proposerStatus) {
      text += 'Life Insured\'s and Proposer\'s ';
      if (insuredStatus === CROSSAGE_STATUS.WILL_CROSSAGE) {
        text += ageWillCrossText;
      } else {
        text += crossedAgeText;
        isCrossed = true;
      }
    } else {
      if (insuredStatus === CROSSAGE_STATUS.WILL_CROSSAGE || insuredStatus === CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED) {
        text += 'Life Insured\'s ';
        if (insuredStatus === CROSSAGE_STATUS.WILL_CROSSAGE) {
          text += ageWillCrossText;
        } else {
          text += crossedAgeText;
          isCrossed = true;
        }
      }

      if (proposerStatus === CROSSAGE_STATUS.WILL_CROSSAGE || proposerStatus === CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED) {
        text === '' ? null : text += '\n\n';
        text += 'Proposer\'s ';
        if (proposerStatus === CROSSAGE_STATUS.WILL_CROSSAGE) {
          text += ageWillCrossText;
        } else {
          text += crossedAgeText;
          isCrossed = true;
        }
      }
    }

    if (allowBackdate) {
      text += '\n\nDo you agree to backdate?';
    }

    if (isCrossed) {
      if (allowBackdate) {
        buttons.push(
          <FlatButton
            label='Yes'
            primary={true}
            onTouchTap={this.updateBackDateTrue}
          />
        );
        buttons.push(
          <FlatButton
            label='No'
            primary={true}
            onTouchTap={this.invalidateCase}
          />
        );
      } else {
        buttons.push(
          <FlatButton
            label='OK'
            primary={true}
            onTouchTap={this.invalidateCase}
          />
        );
      }
    } else {
      if (allowBackdate) {
        buttons.push(
          <FlatButton
            label='Yes'
            primary={true}
            onTouchTap={this.updateBackDateTrue}
          />
        );
        buttons.push(
          <FlatButton
            label='No'
            primary={true}
            onTouchTap={()=>this.setState({showCrossAgeMsg: false})}
          />
        );
      } else {
        buttons.push(
          <FlatButton
            label='OK'
            primary={true}
            onTouchTap={()=>this.setState({showCrossAgeMsg: false})}
          />
        );
      }
    }

    return (
      <Dialog
        title='REMINDER'
        open={showCrossAgeMsg}
        actions={buttons}
      >
        <div style={{whiteSpace:'pre-Line'}}>
          {text}
        </div>
      </Dialog>
    );
  };

  //Set signature field by x, y, width, height retrieved from CB
  genSignFieldCmdListByRect = (idSearch, arraySignFields) => {
    let signFieldList = [];
    for (let i = 0 ; i < arraySignFields.length; i ++) {
      const signField = arraySignFields[i];
      const arraySignLocation = signField.signLocation;

      for (let j = 0; j < arraySignLocation.length; j ++) {
        const location = arraySignLocation[j];
        if (arraySignLocation[j].id.indexOf(idSearch) !== -1) {
          signFieldList.push(
            'name=' + location.id + '|' +
            'type=formfield|subtype=signature|required=true|' +
            'page=' + (signField.page + 1) + '|' +
            'left=' + Math.round(location.x * A4size.width) + '|' +
            'bottom=' + Math.round(location.y * A4size.height) + '|' +
            'width=' + Math.round(location.width * A4size.width) + '|' +
            'height=' + Math.round(location.height * A4size.height) + '|' +
            'tooltip=Signature|friendlyname=#Signature|' +
            'archive_action=lock_all_if_signed'
          );
        }
      }
    }
    return signFieldList;
  }

  //Set signature field by search text, offset x, offset y, width, height retrieved from CB
  genSignFieldCmdListBySearchText = (idSearch, arraySignFields) => {
    let signFieldList = [];
    for (let i = 0 ; i < arraySignFields.length; i ++) {
      const signField = arraySignFields[i];
      const arraySignLocation = signField.signLocation;

      for (let j = 0; j < arraySignLocation.length; j ++) {
        const location = arraySignLocation[j];
        if (arraySignLocation[j].id.indexOf(idSearch) !== -1) {
          signFieldList.push(
            'name=' + location.id + '|' +
            'type=formfield|subtype=signature|required=true|' +
            'searchtext=' + (location.searchText) + '|' +
            'offsetx=' + Math.round(location.x * A4size.width) + '|' +
            'offsety=' + Math.round(location.y * A4size.height) + '|' +
            'width=' + Math.round(location.width * A4size.width) + '|' +
            'height=' + Math.round(location.height * A4size.height) + '|' +
            'tooltip=Signature|friendlyname=#Signature|' +
            'archive_action=lock_all_if_signed'
          );
        }
      }
    }
    return signFieldList;
  }

  //Set signature field by search tag retrieve from CB
  genSignFieldCmdListBySearchTag = (tabIndex, arraySignFields) => {
    if (arraySignFields.length == 0 || tabIndex >= arraySignFields.length) {
      return [];
    }

    let signFieldList = [];
    let signFields = arraySignFields[tabIndex];

    for(let i = 0; i < arraySignFields[tabIndex].length; i ++) {
      let signField = signFields[i];
      let searchTag = signField.searchTag;
      if (searchTag && searchTag.length > 0) {
        signFieldList.push(
          'name=' + searchTag + '|' +
          'type=formfield|subtype=signature|required=true|' +
          'searchtext=' + searchTag + '|' +
          'width=' + Math.round(signField.width * A4size.width) + '|' +
          'height=' + Math.round(signField.height * A4size.height) + '|' +
          'tooltip=Signature|friendlyname='+ signField.name + '|' +
          'archive_action=lock_all_if_signed'
        );
      }
    }
    return signFieldList;
  }

  //Set signature field by search tag retrieve from CB
  genTextFieldCmdListBySearchTag = (tabIndex, arrayTextFields) => {
    if (arrayTextFields.length == 0 || tabIndex >= arrayTextFields.length) {
      return [];
    }

    let textFieldList = [];
    let textFields = arrayTextFields[tabIndex];

    for(let i = 0; i < arrayTextFields[tabIndex].length; i ++) {
      let textField = textFields[i];
      let searchTag = textField.searchTag;
      if (searchTag && searchTag.length > 0) {
        textFieldList.push(
          'name=' + searchTag + '|' +
          'type=addtext|' +
          'text=' + (textField.value || '') + '|' +
          'fontname=Franklin Gothic Book|' +
          'searchtext=' + searchTag + '|' +
          'width=' + Math.round(textField.width * A4size.width) + '|' +
          'height=' + Math.round(textField.height * A4size.height) + '|' +
          'tooltip=Text|friendlyname=#Text|'+
          'archive_action=lock_all_if_signed'
        );
      }
    }
    return textFieldList;
  }

  getSignDocSignField = (tabIdx) => {
    let {
      store
    } = this.context;

    let {
      signature
    } = store.getState();

    const { agentSignFields, clientSignFields, textFields } = signature;

    let idSearch = "NotFound";
    if (tabIdx === tabIndex.tabFNReport) {
      idSearch = "FnaReport";
    } else if (tabIdx === tabIndex.tabProposal) {
      idSearch = "Proposal";
    } else if (tabIdx === tabIndex.tabAppForm) {
      idSearch = "AppForm";
    } else if (tabIdx === tabIndex.tabCoverPage) {
      idSearch = "Cover";
    }

    //let agentSignFieldList = this.genSignFieldCmdListByRect(idSearch, agentSignFields);
    //let clientSignFieldsList = this.genSignFieldCmdListByRect(idSearch, clientSignFields);
    // let agentSignFieldList = this.genSignFieldCmdListBySearchText(idSearch, agentSignFields);
    // let clientSignFieldsList = this.genSignFieldCmdListBySearchText(idSearch, clientSignFields);
    let agentSignFieldList = this.genSignFieldCmdListBySearchTag(tabIdx, agentSignFields);
    let clientSignFieldsList = this.genSignFieldCmdListBySearchTag(tabIdx, clientSignFields);
    let textFieldsList = this.genTextFieldCmdListBySearchTag(tabIdx, textFields);
    let signFieldList = [...agentSignFieldList, ...clientSignFieldsList, ...textFieldsList];

    return signFieldList;
  }

  render() {
    let {
      selectedTab,
      isSigned,
      showErrorMsg
    } = this.state;
    let { langMap } = this.context;
    let {
      store
    } = this.context;

    let {
      signature
    } = store.getState();

    let {
      isFaChannel,
      signingTabIdx,
      tabCount,
      pdfStr,
      attUrls,
      agentSignFields,
      clientSignFields,
      textFields,
      signDocAuths,
      signDocDocts,
      signDocIds,
      signDocPostUrl,
      signDocResultUrl,
      signDocDmsId,
      errorMsg,
    } = signature;

    let tabText = [];
    if (!isFaChannel) {
      tabText.push(window.getLocalizedText(langMap, 'signature.tab.needs', 'Financial needs analysis'));
    }
    tabText.push(window.getLocalizedText(langMap, 'pplication.proposal_tab_title', 'Policy Illustration Document(s)'));
    tabText.push(window.getLocalizedText(langMap, 'application.form_title', 'Application Form'));
    tabText.push(window.getLocalizedText(langMap, 'application.cover_page', 'Cover Page'));

    const actions = [
      <FlatButton
        label="OK"
        primary={true}
        onTouchTap={this.handleHideErrorMsg}
      />
    ];

    let tabs = [];
    for (let i = 0; i < tabCount; i ++) {
      tabs.push(
        <SignatureTab
          key={"tabBox" + i}
          name={"tabBox" + i}
          selected={(selectedTab === i)}
          checked={isSigned[i]}
          isSigningProcess={signingTabIdx === i}
          totalTabs={tabCount}
          onHandleChange={() => this.handleChangeTab(i)}
          text={tabText[i]}/>
      );
    }

    let signDocViewers = [];
    for (let i = 0; i < tabCount; i ++) {
      signDocViewers.push(
        <SignDocViewer
          key={"signDocView" + i}
          id={signDocIds[i]}
          visible={selectedTab === i && (isSigned[i] || (selectedTab === signingTabIdx && !isSigned[i]))}
          signed={isSigned[i]}
          eSignBorder={true}
          postUrl={signDocPostUrl}
          pdfUrl={attUrls[i]}
          auth={signDocAuths[i]}
          docts={signDocDocts[i]}
          dmsId={signDocDmsId}
          signFieldList={this.getSignDocSignField(i)}
          resultUrl={signDocResultUrl}
          onHandleSign={(resultJSON) => {
            this.handleSign(resultJSON, i);
          }}
        />
      );
    }

    return (
      <div style={{flexGrow: 1, display: 'flex', minHeight: '0px', flexDirection: 'column', overflowY: 'hidden'}}>
        <div key="tabsWrapper" style={{display: 'flex', minHeight: '42px', flexDirection: 'row', zIndex: '1000'}}>
          {tabs}
        </div>
        <div style={{display: 'flex', minHeight: '0px', flexDirection: 'column', flexGrow: 1}} >
          {signDocViewers}
          {
            selectedTab !== signingTabIdx && !isSigned[selectedTab] && pdfStr[selectedTab] !== ""?
              (
                <PDFViewer
                  pdf={pdfStr[selectedTab]}
                  containerStyle={{
                    width: '100%',
                    textAlign: 'center',
                    overflowY: 'auto',
                    flexGrow: 1,
                    marginTop: '40px'
                  }} />
              ): (null)
          }
          <Dialog
            actions={actions}
            modal={false}
            open={showErrorMsg}
            onRequestClose={this.handleHideErrorMsg}
          >
            {errorMsg}
          </Dialog>
          {this.crossAgeDialog()}
        </div>
      </div>
    );
  }
}

export default SignatureMain;
