import React from 'react';
import {FlatButton} from 'material-ui';

import FloatingPage from '../../../FloatingPage';
import AppbarPage from '../../../AppbarPage';
import Main from './Main';

import EABComponent from '../../../../Component';
import styles from '../../../../Common.scss';
import {initNeeds} from '../../../../../actions/needs';

class FEMain extends EABComponent {
  constructor(props, context){
    super(props);
    this.state = Object.assign({}, this.state,
      {
        open: false
      }
    )
  }

  open=()=>{
    this.main.init();
    this.floatingPage.open();
  }

  close=()=>{
    this.floatingPage.close();
  }

  validate=()=>{
    return this.main.validate();
  }

  componentDidMount=()=>{
    const {store} = this.context;
    let {needs:{pda, fe}, pos} = store.getState();
    this.unsubscribe = store.subscribe(() => {
      let {needs:{pda, fe}, pos, client: {profile}} = store.getState();

      if (pos.currentPage === 'financial_evaluation' && !isEmpty(pda) && this.state.open === false){
        this.open();
        this.setState({open: true})
      } else if (pos.currentPage !== 'financial_evaluation' && this.state.open === true) {
        this.setState({open: false})
      }
    });

    if (isEmpty(pda) && this.state.open === false){
      initNeeds(this.context);
    }else if(this.state.open === false){
      this.open();
      this.setState({open: true})
    }  
  }

  componentWillUnmount() {
    this.unsubscribe && this.unsubscribe();
  }

  render() {
    return (
      <FloatingPage ref={(ref)=>{this.floatingPage=ref;}}>
        <AppbarPage showShadow={false}>
          <Main ref={(ref)=>{this.main=ref;}}/>
        </AppbarPage>
      </FloatingPage>
    );
  }
}

export default FEMain;