import React from 'react';
import PropTypes from 'prop-types';
import {Dialog, FlatButton, Step, Stepper, StepButton} from 'material-ui';

import DynNeedPage from '../../../../DynViews/DynNeedPage';

import * as _ from 'lodash';
import * as _n from '../../../../../utils/needs';
import * as _v from '../../../../../utils/Validation';
import * as _s from '../../../../../actions/sessions';

import {saveFE, closeFE} from '../../../../../actions/needs';
import EABComponent from '../../../../Component';
import Constants from '../../../../../constants/SystemConstants'

import {getIcon} from '../../../../Icons/index';

import styles from '../../../../Common.scss';

class Main extends EABComponent {
  constructor(props, context) {
      super(props);

      let {needs} = context.store.getState();
      let template = needs.template.feForm;

      this.state = Object.assign({}, this.state, {
        template,
        changedValues: _.cloneDeep(needs.fe),
        values: _.cloneDeep(needs.fe),
        error: {},
        errorCode: null,
        showUpdDialog: false
      })
  };

  getChildContext() {
    return {
      rootTemplate: this.state.template
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null
    }
  }

  storeListener=()=>{
      let {store} = this.context;
      let {needs} = store.getState();

      let newState = {};

      if(!isEqual(this.state.values, needs.fe)){
        newState.values = _.cloneDeep(needs.fe);
        newState.changedValues = _.cloneDeep(needs.fe);
      }

      if(!isEmpty(newState))
        this.setState(newState);
  }

  init=()=>{
    this.state.hasChange=false;
    let {needs} = this.context.store.getState();
    let {template} = this.state,
      error = {},
      {fe} = needs;
    let values = _.cloneDeep(fe), changedValues = _.cloneDeep(fe);
    let valid = _n.validateFE(this.context, template, changedValues, error);

    this.context.initAppbar({
      menu: {
        title: 'SAVE',
        action: ()=>{
          this.onSave(this.onClose);
        }
      },
      title: {
        "en": "Financial Evaluation"
      },
      itemsIndex: valid?1:0,
      items:[[{
        id: "done",
        type: "flatButton",
        title: {
          "en":"DONE"
        },
        disabled: true
      }],
      [{
          id: "done",
          type: "flatButton",
          title: {
            "en":"DONE"
          },
          action: ()=>{
            this.state.changedValues.completed = true;
            this.onSave(this.onClose);
          }
        }]
      ]
    });

    this.setState({values, changedValues, error});
  }

  onClose=()=>{
    closeFE(this.context);
    this.context.goTab(Constants.FEATURES.FNA);
  }

  validate=(changedValues)=>{
    let {template, values} = this.state,
      error = {};
    let valid = _n.validateFE(this.context, template, changedValues, error);
    changedValues.isCompleted = valid;
    this.setState({changedValues, error}, ()=>{
      this.context.updateAppbar({itemsIndex: valid?1:0});
    });
    
    return valid;
  }

  onSave=(callback, confirm = false)=>{
    if(this.state.hasChange){
      saveFE(this.context, this.state.changedValues, confirm, (code)=>{
        if(code){
          this.setState({
            errorCode: code,
            showUpdDialog: true,
            callback
          })
        }
        else{
          this.setState({showUpdDialog: false, errorCode: null, hasChange: false, callback: null}, callback);
        }
      });
    }
    else {
      this.state.hasChange = false;
      _s.extendSession(this.context, callback);
    }
  }

  onChange=(revisit)=>{
    this.state.hasChange = true;
    if (!revisit){
      this.state.changedValues.revisit = false;
    }
  }

  rollback = () =>{
    let {store} = this.context;
    let {fe} = store.getState().needs;
    this.state.hasChange = false;
    this.state.showUpdDialog = false;
    this.validate(_.cloneDeep(fe), false);
  }

  render() {
    let {pageOpen, validRefValues, lang, langMap} = this.context;
    let {template, changedValues, values, error, showUpdDialog, errorCode} = this.state;

    if(!pageOpen){
      return <div/>
    }

    const updActions = [
      (
        <FlatButton
          key="profileDialog-updFM-cancel-btn"
          label="No"
          onTouchTap={this.rollback}
        />
      ),
      (
        <FlatButton
          key="profileDialog-updFM-confirm-btn"
          label="Yes"
          onTouchTap={()=>{
            this.onSave(this.state.callback, true);
          }}
        />
      )
    ]

    return (
      <div style={{display: 'flex', minHeight: '0px', flexGrow: 1, flexDirection: 'column', overflowX: 'scroll'}}>
        <Dialog open={showUpdDialog} actions={updActions}>
          {_v.getErrorMsg(null, errorCode, lang)}
        </Dialog>
        <DynNeedPage
          ref={ref=>{this.dynNeedPage=ref}}
          template={template}
          changedValues={changedValues}
          values={values}
          error={error}
          validate={this.validate}
          onChange={this.onChange}
          onSave={this.onSave}
        />
      </div>
    );
  }
}


Main.contextTypes = Object.assign(Main.contextTypes, {
  onPageClose: PropTypes.func
})

Main.childContextTypes = Object.assign({}, Main.childContextTypes, {
  rootTemplate: PropTypes.object
});

export default Main;
