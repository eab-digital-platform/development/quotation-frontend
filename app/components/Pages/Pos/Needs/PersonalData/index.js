import React, { PropTypes } from 'react';
import mui, {FlatButton} from 'material-ui';

import FloatingPage from '../../../FloatingPage';
import AppbarPage from '../../../AppbarPage';
import StepperPage from '../../../StepperPage';
import Main from './Main';
import EABComponent from '../../../../Component';

import styles from '../../../../Common.scss';

export default class PeronalData extends EABComponent {
  open=(template)=>{
    this.main.init(template);
    this.floatingPage.open();
  }

  close=()=>{
    this.floatingPage.close();
  }
  render() {
    return (
      <FloatingPage ref={ref=>{this.floatingPage=ref}}>
        <AppbarPage>
          <StepperPage>
            <Main ref={ref=>{this.main=ref}}/>
          </StepperPage>
        </AppbarPage>
      </FloatingPage>
    );
  }
}
