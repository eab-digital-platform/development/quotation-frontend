import React from 'react';
import {Dialog, FlatButton} from 'material-ui';
import PropTypes from 'prop-types';
import Slider from '../../Land/Slider';

import EABComponent from '../../../Component';
import ImageView from '../../../CustomViews/ImageView';
import ConceptSelling from './ConceptSelling/index';
import NeedsAnalysis from './NeedsAnalysis/index';
import PersonalData from './PersonalData/index';
import FinancialEvaluation from './FinancialEvaluation/index';
import ClientProfile from '../../../Dialogs/ClientProfile';
import NeedsSummary from './NeedsSummary/index';
import FNAReport from './FNAReport/index';

import {initNeeds,goFE} from '../../../../actions/needs';
import styles from '../../../Common.scss';

import * as _c from '../../../../utils/client';
import Constants from '../../../../constants/SystemConstants'

class Main extends EABComponent {
  constructor(props, context) {
      super(props);
      this.state= Object.assign({}, this.state, {
        template: context.store.getState().needs.template.cnaForm,
        showLockDialog: false,
        pdaValid: false,
        fnaValid: false,
        feValid: false,
        validRefValues:{}
      });
  };

  getChildContext() {
    return {
      onPageClose: this.onUpdate,
      validRefValues: this.state.validRefValues
    }
  }

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    let {profile} = this.context.store.getState().client;
    this.context.updateAppbar({
      title: profile.fullName
    })
    initNeeds(this.context);

  }


  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let newState={};
      let {needs, client} = this.context.store.getState();
      let {pda, fna, fe} = needs;
      let {profile, dependantProfiles, template} = client;
      let {tiTemplate} = template;


      let validRefValues = {pda, fna, fe, profile, dependantProfiles, tiTemplate};

      if(!isEqual(validRefValues, this.state.validRefValues)){
        newState.validRefValues = validRefValues;
      }

      let dependantValid = !_.find(profile.dependants, d=>!d.relationship);
      if (dependantValid != this.state.dependantValid) {
        newState.dependantValid = dependantValid;
      }
      let pdaValid = _.get(pda,'isCompleted'), fnaValid = _.get(fna,'isCompleted'), feValid = _.get(fe,'isCompleted');
      if(pdaValid != this.state.pdaValid || feValid != this.state.feValid || fnaValid != this.state.fnaValid){
        newState.pdaValid = pdaValid;
        newState.feValid = feValid;
        newState.fnaValid = fnaValid;
      }

      if(!isEmpty(newState)){
        this.setState(newState);
        let disableBtn =  (_.isEmpty(needs.pda)) ? true : false;

        this.context.updateAppbar({
          itemsIndex: isEmpty(pda)?0:1,
          items: this.genActionItem(disableBtn, disableBtn)
        })
      }
    }
  }

  genActionItem(disableFNAReport, diableNeedsSummary) {
    return [
        [],
        [
          {
            id: "btnFnaReport",
            title: {
              "en": "FNA REPORT"
            },
            disabled: disableFNAReport,
            action: () => {
              this.FNAReport.open();
            },
            type: "flatButton"
          },
          {
            id: "btnViewSummary",
            title: {
              "en": "VIEW SUMMARY"
            },
            disabled: diableNeedsSummary,
            type: "flatButton",
            action: () => {
              this.NeedsSummary.open();
            }
          }
        ]
      ];
  }

  openConceptSelling = () =>{
    this.conceptSelling.open();
  }

  genHeader=()=>{

    let {store, lang} = this.context;
    let {template, fnaValid, feValid} = this.state;

    let headers = [];    
    _.forEach(template.needsLandingTitle, h=>{
      let {id, button, title, content} = h;
      //hiden concept celling button for release 1
      let headerBtn = (
        id==="learnMore"?null:<FlatButton
          label={ getLocalText(lang, button) }
          style={{ margin: 0 }}
          onTouchTap={()=>{
            if(id==="learnMore"){
              this.openConceptSelling();
            }
            else {
              this.context.goTab(Constants.FEATURES.FQ);
            }
          }}
        />
      )
      if(!(id==="recommendProducts" && !fnaValid && !feValid)){
        headers.push(
          <div key={`cna-header-${id}`} style={{display: 'flex', minHeight: '0px', flexDirection: 'column', textAlign: 'center'}}>
            <div style={{paddingTop: 16, fontSize: 24}}>
              {getLocalText(lang, title)}
            </div>
            <div style={{paddingTop:16}}>
              {getLocalText(lang, content)}
            </div>
            <div>
              {headerBtn}
            </div>
          </div>
        )
      }
    })

    return headers;
  }

  openPage=(id)=>{
    let {profile} = this.state.validRefValues;
    if(this.clientProfile.validateValues(profile)){
      if (id ==="financialEvaluation"){
        goFE(this.context);
      }else {
        this[id].open();
      }
    }
    else{
      this.clientProfile.openDialog(profile, false);
    }
  }

  getContentStatus=(id)=>{
    if(id === 'needsAnalysis'){
      var valid = this.state.fnaValid;
    }
    else if(id === 'personalData'){
      var valid = this.state.pdaValid;
    }
    else {
      var valid = this.state.feValid;
    }

    return valid?'C':'I';
  }

  _openPage = (index, lock)=>{
    if (this.state.dependantValid && (!lock || index===0)) {
      this.openPage(index===0?'personalData': index===1?  'financialEvaluation': 'needsAnalysis');
    }
    else{
      this.setState({showLockDialog: true});
    }
  }

  genContent=()=>{
    let {store, lang} = this.context;
    let {pdaValid, feValid, validRefValues, template} = this.state;
    let {profile} = validRefValues;
    let { pda } = store.getState().needs;

    let contents = [];

    _.forEach(template.needsLandingContent, (c,i)=>{
      let {id, bgColor, bgImage, contentLastCompleted, buttonContinue, buttonStart, buttonView, content, title, template} = c;
      let contentStatus = this.getContentStatus(id);
      let isLock = (id!=='personalData' && !pdaValid) || (id==='needsAnalysis' && !feValid);

      let button = isLock?
        <FlatButton label={getLocalText(lang, "LOCK")} style={{margin: 0}} disabled={true}/>:
        contentStatus == 'I'? <FlatButton label={getLocalText(lang, "INCOMPLETE")} labelStyle={{color: 'red'}} style={{margin: 0}}/>:
        <FlatButton label={getLocalText(lang, "EDIT")} style={{margin: 0}}/>;

      let lastUpd = '';
      if(pda.lastUpd){
        let lastUpdDate = new Date(pda.lastUpd);
        lastUpd = lastUpdDate.format('dd MMM yyyy');
      }

      contents.push(
        <div  key={`cna-content-${id}`} style={{background: bgColor, display: 'flex', minHeight: '0px', position: 'relative', flexDirection: 'column', alignItems: 'stretch', margin: '16px', flexBasis: '240px', minHeight: '400px'}} onClick={()=>{this._openPage(i, isLock)}}>
          {isLock? <div style={{position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, background: 'rgba(0,0,0,0.5)'}}/>: null}
          <div style={{width: '100%', flexBasis: '150px'}}>
            <ImageView docId={'CNAForm'} attId={bgImage} style={{width: '100%', height: '150px', objectFit: 'initial'}}/>
          </div>
          <div style={{flexBasis: '48px', fontSize: 18, padding: 16, fontWeight: 'bold'}}>
            {getLocalText(lang, title)}
          </div>
          <div style={{background: bgColor, flexGrow: 1, padding: '0px 16px'}}>
            {contentStatus=='C'?(getLocalText(lang, contentLastCompleted) + ' ' + lastUpd):getLocalText(lang, content)}
          </div>
          <div style={{flexBasis:'32px', textAlign: 'right'}}>
            {button}
          </div>
        </div>
      )

    })

    return contents;
  }

  onUpdate = () =>{
    this.forceUpdate();
  }

  render() {
    let {langMap, store} = this.context
    let {showLockDialog, validRefValues, fnaValid, pdaValid, dependantValid} = this.state;
    let {profile} = validRefValues;
    let headers = this.genHeader();
    let contents = this.genContent();

    let lockActions = [(
      <FlatButton
        key="fna-dialog-lock-ok-btn"
        label={getLocalizedText(langMap, "BUTTON.OK", "OK")}
        onTouchTap={()=>{this.setState({showLockDialog: false})}}
      />
    )]
    return (
      <div style={{display: 'flex', minHeight: '0px', flex:1, flexDirection: 'column'}}>
        <Dialog open={showLockDialog} actions={lockActions}>
          {
            !dependantValid ? "Please review spouse's client profile" :
            pdaValid?
              getLocalizedText(langMap, "FNA.COMPLETE_FE_FIRST", "Please complete financial evaluation section before starting need analysis sections.")
              :getLocalizedText(langMap, "FNA.COMPLETE_PDA_FIRST", "Please complete personal data acknowledgement section before starting other FNA sections.")
          }
        </Dialog>
        <ConceptSelling ref={ref=>{this.conceptSelling=ref}}/>
        <NeedsAnalysis ref={ref=>{this.needsAnalysis=ref}}/>
        <PersonalData ref={ref=>{this.personalData=ref}}/>
        <NeedsSummary ref={ref=>{this.NeedsSummary=ref}}/>
        <FNAReport ref={ref=>{this.FNAReport=ref}}/>
        <ClientProfile
          values={profile}
          ref={ref=>{this.clientProfile=ref}}
          mandatoryId={_c.genProfileMandatory(Constants.FEATURES.FNA, true, true, false)}
          onDelete={()=>{
           this.context.closePage();
          }}
          onSubmit={() => this.personalData.open()}
        />
        <Slider selected={fnaValid?1:0} style={{flex: 1, minHeight: '148px'}}>
          {headers}
        </Slider>
        <div className={styles.FlexContainer3}>
          {contents}
        </div>
      </div>

    );
  }
}

Main.childContextTypes = Object.assign({}, Main.childContextTypes, {
  onPageClose: PropTypes.func,
  validRefValues: PropTypes.object
});

export default Main;
