import React from 'react';
import PropTypes from 'prop-types';
import {IconButton} from 'material-ui';

import {getIcon} from '../../../Icons/index';
import * as _ from 'lodash';
import EABComponent from '../../../Component';

import styles from '../../../Common.scss';
import TrustedIndividualDialog from '../../../Dialogs/TrustedIndividualDialog';
import EABAvatar from '../../../CustomViews/Avatar';
import {getFamilyProfile} from '../../../../actions/client';

import {getAvatarInitial} from '../../../../utils/client';


class TrustIndividualItem extends EABComponent{

  getRelationship=(value)=>{
    let {optionsMap, langMap, lang, muiTheme} = this.context;
    let {options} = optionsMap.relationship;

    let opt = options.find(option=>option.value === value) || '';
    return getLocalText(lang, opt && opt.title)
  }
  
  getIdDocType=(value)=>{
    let {tiTemplate: template} = this.context.store.getState().client.template;
    let idDocTypeOpts = _.get(template, "items[0].items[1].items[6].items[0].options");
    return _.get(_.find(idDocTypeOpts, opt=>opt.value === value), 'title.en') || value;
  }

  render(){
    if(isEmpty(this.props.member))
      return (<div/>)
    let {cid} = this.context.store.getState().client.profile;
    let {fullName='', idCardNo, idDocType, idDocTypeOther, relationship, tiPhoto} = this.props.member;
    let relationshipTitle = this.getRelationship(relationship);
    let {muiTheme} = this.context;
    let iconColor = muiTheme.palette.primary1Color;
    let names = fullName.split(" ");
    var initial = getAvatarInitial(this.props.member);
    idDocType = idDocType === 'other' ? idDocTypeOther : this.getIdDocType(idDocType);

    return(
      <div className={styles.CardRow} onTouchTap={this.props.onItemClick}>
        <div className={styles.Avatar}>
          <EABAvatar disabled docId={cid} changedValues={{tiPhoto, initial}} values={{tiPhoto, initial}} template={{id:'tiPhoto', type: 'photo'}} width={56} icon={getIcon('person', iconColor)}/>
        </div>
        <div className={styles.CardRowMultiColumn}>
          <div className={styles.CardRowFieldLarge}>{fullName}</div>
          <div className={styles.CardRowFieldSmall}>{relationshipTitle}</div>
        </div>
        <div className={styles.CardRowMultiColumn}>
          <div className={styles.CardRowFieldLarge}>{idDocType}</div>
          <div className={styles.CardRowFieldSmall}>{idCardNo}</div>
        </div>
      </div>
    )
  }
}

TrustIndividualItem.propTypes = Object.assign({}, TrustIndividualItem.propTypes, {
  member: PropTypes.object
})

class TrustIndividualCardView extends EABComponent {
  constructor(props) {
      super(props);
      this.state = Object.assign({}, this.state, {
        template: {},
        trustedIndividuals: {}
      })
  };

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
    this.storeListener();
  }

  componentWillUnmount() {
    if (_.isFunction(this.unsubscribe)) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener = () =>{
    if(this.unsubscribe){
      let state = this.context.store.getState();
      let newState = {};
      let isUpd = false;

      if(state.client.profile){
        if(!isEqual(this.state.trustedIndividuals, state.client.profile.trustedIndividuals)){
          newState.trustedIndividuals = state.client.profile.trustedIndividuals;
          isUpd = true;
        }
      }

      if(!isEqual(this.state.template, state.client.template.trustIndividualsEditDialog)){
        newState.template = state.client.template.trustIndividualsEditDialog;
        isUpd = true;
      }

      if(isUpd)
        this.setState(newState);
    }
  }


  openProfileDialog=()=>{
    this.trustedIndividualDialog.openDialog(this.state.trustedIndividuals);
  }

  render(){
	  let {muiTheme, langMap, lang} = this.context;
    let {template, trustedIndividuals} = this.state;
    let {style} = this.props;
    let iconColor = muiTheme.palette.primary1Color;

    return (
      <div className={styles.Card} style={style}>
        <TrustedIndividualDialog template={template} ref={ref=>{this.trustedIndividualDialog=ref}}/>
        <div className={styles.CardPadding}>
           <div className={styles.profileOnly}>
            <div className={styles.profileOnlyTitle}>
              {getLocalText(langMap, "Trusted Individual")}
            </div>
            <div>
              <IconButton onTouchTap={()=>{this.openProfileDialog(trustedIndividuals)}}>{getIcon("edit", iconColor)}</IconButton>
            </div>
          </div>
          <TrustIndividualItem member={trustedIndividuals}  onItemClick={()=>{this.openProfileDialog(trustedIndividuals)}}/>
        </div>
      </div>
	  )
  }
}
export default TrustIndividualCardView;
