import React, {Component} from 'react';
import PropTypes from 'prop-types';

import styles from '../Common.scss';

import Appbar from '../CustomViews/AppBar';

class AppbarPage extends Component {
  constructor(props) {
      super(props);
      this.state = {
        appbar: {}
      }
  };

  getChildContext() {
    return {
      appbar: this.state.appbar,
      updateAppbar: this.updateAppbar,
      initAppbar: this.initAppbar,
      updateSearchValue: this.updateSearchValue,
      focusSearchValue: this.focusSearchValue
    }
  }

  updateAppbar = (data) =>{
    this.state.appbar = this.appbar && this.appbar.updateAppbar(data);
  }

  initAppbar = (data) =>{
    this.state.appbar = this.appbar && this.appbar.initAppbar(data);
  }

  updateSearchValue = (value) => {
    this.appbar && this.appbar.updateSearchValue(value);
  }

  focusSearchValue = () => {
    this.appbar && this.appbar.focusSearchValue();
  }

  render() {
    let {children, id, search, showShadow} = this.props;
    return (
      <div className={styles.Page}>
        <Appbar id={id} ref={ref=>{this.appbar=ref}} search={search} showShadow={showShadow}/>
        {children}
      </div>
    );
  }
}

AppbarPage.propTypes = {
  id: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]),
  search: PropTypes.bool,
  showShadow: PropTypes.bool
};

AppbarPage.childContextTypes = {
  appbar: PropTypes.object,
  updateAppbar: PropTypes.func,
  initAppbar: PropTypes.func,
  updateSearchValue: PropTypes.func,
  focusSearchValue: PropTypes.func
}

export default AppbarPage;
