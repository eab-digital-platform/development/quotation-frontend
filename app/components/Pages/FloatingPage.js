import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = require('../Common.scss');
import Modal from '../Modal';

class FloatingPage extends Component {
  constructor(props) {
      super(props);
      this.state = {
        open: false
      };
  };

  close=(callback)=>{
    this.setState({open: false});
    if(typeof callback == "function"){
      callback();
    }
  }

  open=()=>{
    this.setState({open:true});
  }

  getChildContext() {
    return {
      closePage: this.close,
      pageOpen: this.state.open
    }
  }

  render() {

    let {
      open
    } = this.state;

    let {
      zIndex
    } = this.props;

    return (
      <Modal>
        <div className={styles.FloatingPage}
          style={{display: open?'flex':'none', zIndex: zIndex || 500}}
          onDrop={(e)=>{e.preventDefault()}}
          onDragOver={(e)=>{e.preventDefault()}}>
          {this.props.children}
        </div>
      </Modal>
    );
  }
}

FloatingPage.childContextTypes = {
  closePage: PropTypes.func,
  pageOpen: PropTypes.bool
}

FloatingPage.propTypes = {
  zIndex: PropTypes.number
}

export default FloatingPage;
