
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Comps from '../components/Home';

function mapStateToProps(state) {
  return {
    app: state.app,
    inited: state.app.inited
  };
}
export default connect(mapStateToProps)(Comps);
