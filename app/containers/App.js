import React, { Component, PropTypes } from 'react';
import Loading from './Loading';
import SessionAlert from '../components/CustomViews/SessionAlert';
const appTheme = require('../theme/appBaseTheme.js');
import {hideErrorMsg, logout} from '../actions/GlobalActions';
import WarningDialog from '../components/Dialogs/WarningDialog';

export default class App extends Component {
  constructor(props) {
      super(props);
      this.state = Object.assign( {}, this.state, {
        muiTheme: appTheme.getTheme(),
        showErrorMsg: false,
        msg: ''
      });
  };

  componentDidMount() {
    this.unsubscribe = this.context.store.subscribe(this.storeListener);
  }

  componentWillUnmount() {
    if (typeof this.unsubscribe == 'function') {
      this.unsubscribe();
      this.unsubscribe = null;
    }
  }

  storeListener=()=>{
    if(this.unsubscribe){
      let {store} = this.context;
      let {showErrorMsg, errorMsg, logout} = this.context.store.getState().app;
      let newState = {};

      if(!isEqual(showErrorMsg, this.state.showErrorMsg)){
        newState.showErrorMsg = showErrorMsg;
      }

      if(!isEqual(errorMsg, this.state.errorMsg)){
        newState.errorMsg = errorMsg;
      }

      if(!isEmpty(newState)){
        this.setState(newState);
      }
    }
  }

  static propTypes = {
    children: PropTypes.object.isRequired
  };

  static childContextTypes = {
    muiTheme: PropTypes.object
  }

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme
    }
  }

  static contextTypes = {
   store: PropTypes.object,
   router: PropTypes.object
  }

  handleDialogAction=()=>{
    // let {logout: isLogout} = this.context.store.getState().app;
    // if(isLogout){
    //   logout(this.context, true);
    // }else{
      hideErrorMsg(this.context)
    // }
  }

  render() {
    let {router} = this.context;
    let {showErrorMsg, errorMsg} = this.state;

    let hasTitle = true;
    let isError = true;
    let dialogAction = this.handleDialogAction;
    let msg = errorMsg;

    if (typeof errorMsg == 'object') {
      hasTitle = !!errorMsg.hasTitle;
      isError = !!errorMsg.isError;
      dialogAction = errorMsg.action || dialogAction;
      msg = errorMsg.errorMsg;
    }

    return (
      <div>
        <Loading />
        <WarningDialog 
          hasTitle = {hasTitle}
          isError = {isError}
          open= {showErrorMsg}
          onClose = {dialogAction}
          style={{zIndex:99999999}}
          msg= {msg}
        />
        <SessionAlert/>
        {this.props.children}
      </div>
    );
  }
}
